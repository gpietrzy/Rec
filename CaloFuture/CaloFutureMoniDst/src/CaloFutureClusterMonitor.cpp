/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>

/** @class CaloFutureClusterMonitor CaloFutureClusterMonitor.cpp
 *
 *  The algorithm for trivial monitoring of "CaloFutureCluster" containers.
 *  The algorithm produces 8 histograms:
 *
 *  <ol>
 *  <li> @p CaloCluster multiplicity                    </li>
 *  <li> @p CaloCluster size (number of cells)          </li>
 *  <li> @p CaloCluster energy distribution             </li>
 *  <li> @p CaloCluster transverse energy distribution  </li>
 *  <li> @p CaloCluster x-distribution                  </li>
 *  <li> @p CaloCluster y-distribution                  </li>
 *  <li> @p CaloCluster x vs y-distribution             </li>
 *  </ol>
 *
 *  Histograms reside in the directory @p /stat/"Name" , where
 *  @ "Name" is the name of the algorithm
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   02/11/2001
 */

using Input = LHCb::Event::Calo::v2::Clusters;

class CaloFutureClusterMonitor final
    : public LHCb::Algorithm::Consumer<void( const Input&, const DeCalorimeter& ),
                                       LHCb::DetDesc::usesBaseAndConditions<CaloFutureMoniAlg, DeCalorimeter>> {
public:
  StatusCode initialize() override;
  void       operator()( const Input&, const DeCalorimeter& ) const override;

  CaloFutureClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

private:
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoNClusters;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoNDigits;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoNDigits_forE;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoEnergy;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoET;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoX;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>>         m_histoY;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>>         m_histoXY;
  mutable std::optional<Gaudi::Accumulators::WeightedHistogram<2>> m_histoXY_eW;
  mutable Gaudi::Accumulators::StatCounter<>                       m_clusters{this, "# clusters over threshold"};
};

DECLARE_COMPONENT( CaloFutureClusterMonitor )

CaloFutureClusterMonitor::CaloFutureClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name )},
                 KeyValue{"Detector", LHCb::Calo::Utilities::DeCaloFutureLocation( name )}} ) {}

StatusCode CaloFutureClusterMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    // define range of new histograms from properties
    using axis1D = Gaudi::Accumulators::Axis<decltype( m_histoNClusters )::value_type::AxisArithmeticType>;
    m_histoNClusters.emplace( this, "nClusters", "# of Clusters " + inputLocation(),
                              axis1D{m_multBin, m_multMin, m_multMax} );
    m_histoNDigits.emplace( this, "nDigits", "Cluster digit multiplicity " + inputLocation(),
                            axis1D{m_sizeBin, m_sizeMin, m_sizeMax} );
    m_histoNDigits_forE.emplace( this, "nDigits_forE", "Cluster digit used for Energy multiplicity " + inputLocation(),
                                 axis1D{m_sizeBin, m_sizeMin, m_sizeMax} );
    m_histoEnergy.emplace( this, "energy", "Cluster Energy " + inputLocation(),
                           axis1D{m_energyBin, m_energyMin, m_energyMax} );
    m_histoET.emplace( this, "ET", "Cluster Et " + inputLocation(), axis1D{m_etBin, m_etMin, m_etMax} );
    m_histoX.emplace( this, "x", "Cluster x " + inputLocation(), axis1D{m_xBin, m_xMin, m_xMax} );
    m_histoY.emplace( this, "y", "Cluster y " + inputLocation(), axis1D{m_yBin, m_yMin, m_yMax} );
    m_histoXY.emplace( this, "x-y", "Cluster barycenter position x vs y " + inputLocation(),
                       axis1D{m_xBin, m_xMin, m_xMax}, axis1D{m_yBin, m_yMin, m_yMax} );
    m_histoXY_eW.emplace( this, "x-y-eW", "Energy-weighted cluster barycenter position x vs y " + inputLocation(),
                          axis1D{m_xBin, m_xMin, m_xMax}, axis1D{m_yBin, m_yMin, m_yMax} );
  } );
}

void CaloFutureClusterMonitor::operator()( const Input& clusters, const DeCalorimeter& calo ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << " Producing histo " << produceHistos() << endmsg;
  // produce histos ?
  if ( !produceHistos() ) return;
  if ( clusters.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty cluster in " << inputLocation() << endmsg;
    return;
  }

  // for multiplicity histogram
  uint nClusters = 0;

  for ( const auto& cluster : clusters.scalar() ) {
    const double e  = cluster.energy();
    const double x  = cluster.position().x();
    const double y  = cluster.position().y();
    const double z  = cluster.position().z();
    const double et = e * sqrt( x * x + y * y ) / sqrt( x * x + y * y + z * z );
    if ( e < m_eFilter ) continue;
    if ( et < m_etFilter ) continue;
    ++nClusters;
    const auto  id      = cluster.cellID();
    const auto& entries = cluster.entries();
    ++m_histoNDigits.value()[entries.size().cast()];
    ++m_histoEnergy.value()[e];
    ++m_histoET.value()[et];
    ++m_histoX.value()[x];
    ++m_histoY.value()[y];
    ++m_histoXY.value()[{x, y}];
    m_histoXY_eW.value()[{x, y}] += e;

    int iuse = std::count_if( entries.begin(), entries.end(), []( const auto& e ) {
      return e.status().test( LHCb::CaloDigitStatus::Mask::UseForEnergy );
    } );
    ++m_histoNDigits_forE.value()[iuse];

    // TODO: use thread-safe histos. Need variable binning.
    if ( doHisto( "position2D" ) )
      fillCaloFuture2D( "position2D", id, 1., calo, "Cluster position 2Dview " + inputLocation() );
    if ( doHisto( "position2D-eW" ) )
      fillCaloFuture2D( "position2D-eW", id, e, calo, "Cluster Energy 2Dview " + inputLocation() );
  }
  ++m_histoNClusters.value()[nClusters];
  m_clusters += nClusters;
}
