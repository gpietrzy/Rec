/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureMoniAlg.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloDigits_v2.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/PropertyFmt.h"
#include "LHCbAlgs/Consumer.h"

#include "GaudiKernel/IRndmEngine.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"

#include "Event/ODIN.h"
#include "GaudiKernel/IEventTimeDecoder.h"

#include <Gaudi/Accumulators/Histogram.h>

// =============================================================================
/** @class CaloFutureLEDMonitor CaloFutureLEDMonitor.cpp
 *  Algorithm for LED monitoring
 */

using Input = LHCb::Event::Calo::Digits;

namespace {
  using ODIN = LHCb::ODIN;
}

class CaloFutureLEDMonitor final
    : public LHCb::Algorithm::Consumer<void( const ODIN&, const Input&, const DeCalorimeter& ),
                                       LHCb::DetDesc::usesBaseAndConditions<CaloFutureMoniAlg, DeCalorimeter>> {

public:
  StatusCode initialize() override;
  void       operator()( const ODIN&, const Input&, const DeCalorimeter& ) const override;

  CaloFutureLEDMonitor( const std::string& name, ISvcLocator* pSvcLocator );

private:
  mutable Gaudi::Accumulators::Histogram<1> m_hTime{this, "time", "time", {3600, 0, 3600}};

  Gaudi::Property<bool> m_spectrum{this, "Spectrum", false, "activate spectrum per channel histogramming"};

  Gaudi::Property<bool>             m_useEmpty{this, "UseEmptyEvent", true};
  Gaudi::Property<std::vector<int>> m_febs{this, "SkipPinFEBs"}; // Expert usage (Hcal : MUST REMOVE THE UNFIRED PIN

  Gaudi::Property<int>    m_led{this, "DefaultLED", 0}; // look only to LED m_led when several (<0 -> check all LEDs)
  Gaudi::Property<double> m_pedRate{this, "PedestalRate", 100};   // percent
  Gaudi::Property<double> m_checkRate = {this, "CheckRate", 0.5}; // Full check rate

  Gaudi::Property<std::vector<double>> m_rangesSignal{this, "RangesSignal", {75., 3840., 100.}};
  Gaudi::Property<std::vector<double>> m_rangesPedestal{this, "RangesPedestal", {-15.5, 20.5, 36.}};
  Gaudi::Property<std::vector<double>> m_rangesRatio{this, "RangesRatio", {0., 20., 20.}};
  Gaudi::Property<std::vector<double>> m_rangesAll{this, "RangesAll", {-256, 3840., 256.}};
  Gaudi::Property<std::vector<double>> m_windowsSignal{this, "WindowsSignal", {75., 3840.}};
  Gaudi::Property<std::vector<double>> m_windowsPedestal{this, "WindowsPedestal", {-15., 20.}};
  Gaudi::Property<std::vector<double>> m_windowsRatio{this, "WindowsRatio", {75., 3840.}};
  Gaudi::Property<std::vector<double>> m_windowsAll{this, "WindowsAll", {-15, 20., 100, 3840.}};
  Gaudi::Property<bool>                m_invertSignal{this, "InvertWindowSignal", false};
  Gaudi::Property<bool>                m_invertPedestal{this, "InvertWindowPedestal", false};
  Gaudi::Property<bool>                m_invertRatio{this, "InvertWindowRatio", false};
  Gaudi::Property<bool>                m_invertAll{this, "InvertWindowAll", true};
  Gaudi::Property<std::string>         m_summaryNameSignal{this, "SummaryNameSignal", "LED"};
  Gaudi::Property<std::string>         m_summaryNamePedestal{this, "SummaryNamePedestal", "Pedestal"};
  Gaudi::Property<std::string>         m_summaryNameRatio{this, "SummaryNameRatio", "Ratio"};
  Gaudi::Property<std::string>         m_summaryNameAll{this, "SummaryNameAlll", "Desert"};
  Gaudi::Property<double>              m_summaryRate{this, "SummaryRate", 100.};

  Gaudi::Property<double> m_histoRateSignal{this, "HistoRateSignal", 6.};
  Gaudi::Property<double> m_histoRatePedestal{this, "HistoRatePedestal", 50.};
  Gaudi::Property<double> m_histoRateRatio{this, "HistoRateRatio", 6.};
  Gaudi::Property<double> m_histoRateAll{this, "HistoRateAll", 5.};

  inline bool isInBankList( const LHCb::Detector::Calo::CellID& id, const DeCalorimeter& calo ) const;
  inline int  whichLedIsFired( const std::vector<int>& leds, const Input& digits, const DeCalorimeter& calo ) const;
  inline int  ledIndex( const LHCb::Detector::Calo::CellID& id, int led, const DeCalorimeter& calo ) const;

  bool firedNeighbor( const LHCb::Detector::Calo::CellID& id, const Input& digits, const DeCalorimeter& calo,
                      bool skipFEB = false ) const;

  void fillHisto( const LHCb::Detector::Calo::CellID& id, int led, double val, double ref, bool fired, bool addRatio,
                  const DeCalorimeter& calo ) const;
  void fillChannelHisto( const DeCalorimeter& calo, double val, const LHCb::Detector::Calo::CellID& id,
                         std::string type, double norm = 0. ) const;
  void fillSummaryHisto( const DeCalorimeter& calo, const LHCb::Detector::Calo::CellID& id, double val,
                         std::string type, double norm = 0. ) const;
  bool isInWindow( double value, std::vector<double> windows, bool invert = false ) const;

  typedef std::vector<LHCb::Detector::Calo::CellID> CellIds;

  // downscaling
  mutable bool m_ped;
  mutable bool m_det;
  mutable bool m_check;

  Gaudi::Property<int>              m_cellCheck{this, "CheckNCell", 7};
  Gaudi::Property<std::vector<int>> m_banks{this, "Banks"}; // Active when not pinDriven only

  Gaudi::Property<double> m_sbSel{this, "StandBySel", 20};
  Gaudi::Property<bool>   m_xt{this, "KillXTalk", true};
  Gaudi::Property<double> m_threshold{this, "ADCThreshold", 50};

  mutable double        m_shoot;
  mutable Rndm::Numbers m_rnd;

  double m_detRate = 50;

  GaudiAlg::HistoID m_signalProfileID = GaudiAlg::HistoID( fmt::format( "Profile/{}/Profile2D", m_summaryNameSignal ) );
  std::string m_signalProfileTitle    = fmt::format( "{} Summary/{} data 'profile'", detData(), m_summaryNameSignal );
  GaudiAlg::HistoID m_pedestalProfileID =
      GaudiAlg::HistoID( fmt::format( "Profile/{}/Profile2D", m_summaryNamePedestal ) );
  std::string m_pedestalProfileTitle = fmt::format( "{} Summary/{} data 'profile'", detData(), m_summaryNamePedestal );
  GaudiAlg::HistoID m_ratioProfileID = GaudiAlg::HistoID( fmt::format( "Profile/{}/Profile2D", m_summaryNameRatio ) );
  std::string       m_ratioProfileTitle = fmt::format( "{} Summary/{} data 'profile'", detData(), m_summaryNameRatio );
  GaudiAlg::HistoID m_allProfileID      = GaudiAlg::HistoID( fmt::format( "Profile/{}/Profile2D", m_summaryNameAll ) );
  std::string       m_allProfileTitle   = fmt::format( "{} Summary/{} data 'profile'", detData(), m_summaryNameAll );
};

// =============================================================================

DECLARE_COMPONENT_WITH_ID( CaloFutureLEDMonitor, "CaloFutureLEDMonitor" )

// =============================================================================

CaloFutureLEDMonitor::CaloFutureLEDMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"InputODIN", LHCb::ODINLocation::Default},
                 KeyValue{"Input", LHCb::CaloFutureAlgUtils::CaloFutureDigitLocation( name )},
                 KeyValue{"Detector", LHCb::Calo::Utilities::DeCaloFutureLocation( name )}} ) {}

// =============================================================================
// standard initialize method
// =============================================================================

StatusCode CaloFutureLEDMonitor::initialize() {

  return Consumer::initialize().andThen( [&] {
    // get the random number generator
    IRndmGenSvc* rndm = svc<IRndmGenSvc>( "RndmGenSvc", true );
    StatusCode   sc   = m_rnd.initialize( rndm, Rndm::Flat( 0.0, 100 ) );
    if ( sc.isFailure() )
      throw GaudiException{"Unable to initialize Flat distribution", "addConditionDerivation", StatusCode::FAILURE};

    bookCaloFuture2D( m_signalProfileID, m_signalProfileTitle, detData(), -1 );
    bookCaloFuture2D( m_pedestalProfileID, m_pedestalProfileTitle, detData(), -1 );
    bookCaloFuture2D( m_ratioProfileID, m_ratioProfileTitle, detData(), -1 );
    bookCaloFuture2D( m_allProfileID, m_allProfileTitle, detData(), -1 );
  } );
}

// ============================================================================
// standard execution method
// ============================================================================

void CaloFutureLEDMonitor::operator()( const ODIN& odin, const Input& digits, const DeCalorimeter& calo ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << name() << " execute " << endmsg;

  // === TIMING HISTO ===
  // get odin time
  longlong eventTime = (longlong)odin.eventTime().ns() * 1 / 1e9;
  ++m_hTime[eventTime % 3600];

  // Init
  unsigned int fire = 0; // for info only
  // prepare downscalings
  m_ped   = false;
  m_det   = false;
  m_check = false;

  m_shoot = m_rnd.shoot();
  if ( m_pedRate > 0 && m_rnd.shoot() < m_pedRate ) m_ped = true;
  if ( m_detRate > 0 && m_shoot < m_detRate ) m_det = true;
  if ( m_checkRate > 0 && m_shoot < m_checkRate ) m_check = true;

  // Init tables
  // ===========
  debug() << " Init table " << endmsg;
  std::vector<LHCb::Detector::Calo::CellID> m_pinChannels;
  m_pinChannels.clear();

  const std::vector<CardParam>& cards = calo.cardParams();
  for ( auto iCard = cards.begin(); iCard != cards.end(); ++iCard ) {
    if ( !( *iCard ).isPinCard() ) continue;
    bool keep = true;
    for ( auto ifeb = m_febs.begin(); m_febs.end() != ifeb; ++ifeb ) {
      if ( *ifeb == ( *iCard ).number() ) {
        keep = false;
        break;
      }
    }
    if ( !keep ) { continue; }
    debug() << "Channels of FE-board number " << ( *iCard ).number() << " is added to PIN readout channels" << endmsg;
    CardParam                                        card  = *iCard;
    const std::vector<LHCb::Detector::Calo::CellID>& chans = card.ids();
    for ( std::vector<LHCb::Detector::Calo::CellID>::const_iterator iChan = chans.begin(); iChan != chans.end();
          ++iChan ) {
      if ( calo.valid( *iChan ) && isInBankList( *iChan, calo ) ) {
        m_pinChannels.push_back( *iChan );
        debug() << "add pin channels " << *iChan << endmsg;
      }
    }
  }

  debug() << "added " << m_pinChannels.size() << " pin channels" << endmsg;

  // produce histos ?
  if ( msgLevel( MSG::DEBUG ) ) debug() << " Producing histo " << produceHistos() << endmsg;
  if ( !produceHistos() ) return;

  if ( digits.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty container in " << inputLocation() << endmsg;
    return;
  }

  // Decode PIN channels
  int nStandBy = 0;
  for ( auto iChan : m_pinChannels ) {
    LHCb::Detector::Calo::CellID pinId = iChan;

    auto dig = digits.find( pinId );
    if ( !dig ) continue;
    double pin        = dig.value().adc();
    bool   pinIsfired = ( pin >= m_threshold );

    if ( msgLevel( MSG::DEBUG ) )
      debug() << " pin " << pinId << " adc = " << pin << " Is fired ? " << pinIsfired << endmsg;

    // connected leds
    const std::vector<int>& leds = calo.pinLeds( pinId );
    // test
    int firedLed = -1;
    // check whether the pin is dead at low rate (full decoding)
    if ( pinIsfired || m_check ) firedLed = whichLedIsFired( leds, digits, calo );

    if ( pinIsfired && firedLed < 0 ) {
      pinIsfired = false; // noisy PIN-diode or X-talk or PMT in StandBy !!
      nStandBy++;
    }

    if ( msgLevel( MSG::DEBUG ) ) debug() << " fired pin ? " << pinIsfired << "  fired LED ? " << firedLed << endmsg;

    if ( !pinIsfired && firedLed < 0 ) {
      if ( m_useEmpty ) continue;            // use ONLY empty event for pedestal survey
      if ( !m_useEmpty && !m_ped ) continue; // Downscale pedestal (and dead PIN)
    } else
      fire++;

    fillHisto( pinId, firedLed, pin, 1., pinIsfired, false, calo ); // fill histo for PIN-diode

    // loop over connected LEDs
    for ( std::vector<int>::const_iterator led = leds.begin(); led != leds.end(); ++led ) {
      const std::vector<LHCb::Detector::Calo::CellID>& cells      = calo.ledChannels( *led ); // !! reference
      bool                                             ledIsfired = ( *led == firedLed );

      if ( m_useEmpty && !ledIsfired ) continue; // keep only fired LED and PINs
      if ( !ledIsfired && !m_ped ) continue;     // downscale pedestal histogramming

      // loop over connected PMTs
      for ( CellIds::const_iterator cell = cells.begin(); cell != cells.end(); ++cell ) {
        const LHCb::Detector::Calo::CellID& id  = *cell;
        auto                                dig = digits.find( *cell );
        if ( !dig ) continue;
        if ( id.isPin() ) continue;
        double pmt = (double)dig.value().adc();
        if ( !isInBankList( id, calo ) ) continue;
        if ( m_xt && !ledIsfired && firedNeighbor( id, digits, calo, true ) )
          continue;                                      // reject if any FEB channel is flashed (X-talk cleaning)
        if ( pinIsfired && pmt < m_threshold ) continue; // DEAD PMT or connection
        fillHisto( id, *led, pmt, pin, ledIsfired, pinIsfired, calo );
        // Event dump
        if ( msgLevel( MSG::VERBOSE ) ) {
          std::string txt = "Pedestal";
          if ( ledIsfired ) txt = "Signal";
          verbose() << txt << " => PMT : [" << id << "," << pmt << "] PIN : [" << pinId << "," << pin << "]" << endmsg;
        }
      }
    }
  }

  // Empty event -> PEDESTAL
  bool standBy = ( nStandBy > m_sbSel );
  if ( m_useEmpty && fire == 0 && m_ped && !standBy ) {
    counter( "empty led flash event" ) += 1;
    for ( auto dig : digits ) {
      int                          adc = dig.adc();
      LHCb::Detector::Calo::CellID id  = dig.cellID();
      if ( id.isPin() ) continue;
      fillHisto( id, -1, (double)adc, 1., false, false, calo ); // FILL PEDESTAL ...
    }
  }

  return;
}

//-------------------------------------------------------
inline bool CaloFutureLEDMonitor::isInBankList( const LHCb::Detector::Calo::CellID& id,
                                                const DeCalorimeter&                calo ) const {

  if ( m_banks.size() == 0 ) return true;
  if ( m_banks.size() == 1 && *( m_banks.begin() ) < 0 ) return true;
  int  feb   = calo.cellParam( id ).cardNumber();
  int  tell1 = calo.cardToTell1( feb );
  bool ok    = false;
  for ( auto ib : m_banks ) {
    if ( ib == tell1 || ib == -1 ) {
      ok = true;
      break;
    }
  }
  return ok;
}

//-------------------------------------------------------
inline int CaloFutureLEDMonitor::whichLedIsFired( const std::vector<int>& leds, const Input& digits,
                                                  const DeCalorimeter& calo ) const {

  // majority voting
  int maxLed = -1;
  int maxSum = 0;
  int nFlash = 0;
  if ( msgLevel( MSG::DEBUG ) ) debug() << " whichLed ? : #led " << leds.size() << " -> " << leds << endmsg;
  for ( std::vector<int>::const_iterator led = leds.begin(); led != leds.end(); ++led ) {
    int            sum   = 0;
    CaloLed        cLed  = calo.caloLed( *led );
    const CellIds& cells = cLed.cells();

    for ( CellIds::const_iterator cell = cells.begin(); cell != cells.end(); ++cell ) {
      if ( cell - cells.begin() >= m_cellCheck ) break;
      auto dig = digits.find( *cell );
      if ( !dig ) continue;
      int adc = dig.value().adc();
      if ( msgLevel( MSG::DEBUG ) ) debug() << " Led " << *led << " adc : " << adc << " id = " << *cell << endmsg;
      if ( adc > m_threshold ) sum++;
    }
    if ( sum > maxSum ) {
      maxLed = *led;
      maxSum = sum;
    }
    if ( sum == maxSum && maxSum > 0 ) nFlash++;
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " Led " << *led << "maxSum / sum / maxLed " << maxSum << " / " << sum << " / " << maxLed << endmsg;
  }
  if ( nFlash > 1 ) {
    int     led  = *( leds.begin() );
    CaloLed cLed = calo.caloLed( led );
    debug() << nFlash << " LEDs connected to PIN-diode " << cLed.pin() << " are flashed simultaneously" << endmsg;
    counter( "Unexpected simultaneous LED flashes" ) += 1;
  }

  return maxLed;
}

//-------------------------------------------------------
inline int CaloFutureLEDMonitor::ledIndex( const LHCb::Detector::Calo::CellID& id, int led,
                                           const DeCalorimeter& calo ) const {

  // Find the led index in the vector of leds connected to cell id
  int              index = -1;
  std::vector<int> leds  = calo.cellParam( id ).leds();
  for ( std::vector<int>::iterator iLed = leds.begin(); iLed != leds.end(); ++iLed ) {
    if ( led == *iLed ) {
      index = iLed - leds.begin();
      break;
    }
  }
  if ( index < 0 ) { error() << "the LED " << led << " is NOT found to be connected to cell : " << id << endmsg; }
  return index;
}

//-------------------------------------------------------
bool CaloFutureLEDMonitor::firedNeighbor( const LHCb::Detector::Calo::CellID& id, const Input& digits,
                                          const DeCalorimeter& calo, bool skipFEB ) const {
  int feb = calo.cardNumber( id );

  if ( skipFEB ) {
    const std::vector<LHCb::Detector::Calo::CellID>& cells = calo.cardChannels( feb );
    for ( std::vector<LHCb::Detector::Calo::CellID>::const_iterator it = cells.begin(); cells.end() != it; ++it ) {
      auto dig = digits.find( *it );
      if ( !dig ) continue;
      if ( dig.value().adc() > m_threshold ) return true;
    }
  } else {
    const std::vector<LHCb::Detector::Calo::CellID>& cells = calo.neighborCells( id );
    for ( std::vector<LHCb::Detector::Calo::CellID>::const_iterator it = cells.begin(); cells.end() != it; ++it ) {
      if ( feb != calo.cardNumber( *it ) ) continue;
      auto dig = digits.find( *it );
      if ( !dig ) continue;
      if ( dig.value().adc() > m_threshold ) return true;
    }
  }
  return false;
}

//-------------------------------------------------------
void CaloFutureLEDMonitor::fillHisto( const LHCb::Detector::Calo::CellID& id, int led, double val, double ref,
                                      bool fired, bool addRatio, const DeCalorimeter& calo ) const {

  int index = 0;
  if ( led >= 0 ) index = ledIndex( id, led, calo );
  if ( index < 0 ) return;
  if ( m_led >= 0 && m_led != index ) return;

  // -------------------
  // Fill Summary histos
  // -------------------
  if ( fired ) {
    fillSummaryHisto( calo, id, val, "Signal" );
    if ( addRatio ) {
      fillSummaryHisto( calo, id, val - calo.pedestalShift(), "Ratio", ref - calo.pinPedestalShift() );
    }
  } else {
    fillSummaryHisto( calo, id, val, "Pedestal" );
  }
  fillSummaryHisto( calo, id, val, "All" );

  // --------------------
  // Fill detailed histos
  // --------------------
  if ( m_spectrum ) {
    if ( !m_det ) return;
    if ( fired ) {
      fillChannelHisto( calo, val, id, "Signal", 0. );
      if ( addRatio ) fillChannelHisto( calo, val - calo.pedestalShift(), id, "Ratio", ref - calo.pinPedestalShift() );
    } else {
      fillChannelHisto( calo, val, id, "Pedestal" );
    }
    fillChannelHisto( calo, val, id, "All" );
  }
  return;
}

// Filling ------------------ //
void CaloFutureLEDMonitor::fillSummaryHisto( const DeCalorimeter& calo, const LHCb::Detector::Calo::CellID& id,
                                             double val, std::string type, double norm ) const {
  double rate = m_summaryRate;
  if ( rate < m_shoot ) return;

  std::vector<double> windows = {75., 3840};
  bool                invert  = false;
  std::string         name    = type;
  if ( type == "Signal" ) {
    windows = m_windowsSignal;
    name    = m_summaryNameSignal;
    invert  = m_invertSignal;
  }
  if ( type == "Pedestal" ) {
    windows = m_windowsPedestal;
    name    = m_summaryNamePedestal;
    invert  = m_invertPedestal;
  }
  if ( type == "Ratio" ) {
    windows = m_windowsRatio;
    name    = m_summaryNameRatio;
    invert  = m_invertRatio;
  }
  if ( type == "All" ) {
    windows = m_windowsAll;
    name    = m_summaryNameAll;
    invert  = m_invertAll;
  };
  if ( !isInWindow( val, windows, invert ) ) return;

  double value = ( norm != 0 ) ? val / norm : val;

  if ( type == "Signal" ) fillCaloFuture2D( m_signalProfileID, id, value, calo, m_signalProfileTitle );
  if ( type == "Ratio" ) fillCaloFuture2D( m_ratioProfileID, id, value, calo, m_ratioProfileTitle );
  if ( type == "Pedestal" ) fillCaloFuture2D( m_pedestalProfileID, id, value, calo, m_pedestalProfileTitle );
  if ( type == "All" ) fillCaloFuture2D( m_allProfileID, id, value, calo, m_allProfileTitle );

  profile1D( id.index(), value, "Profile/" + name + "/1",
             fmt::format( "{} Summary/{} data 'profile'", detData(), name ), 0, 13800, 13800 );
}

//-------------------------------------------------------
void CaloFutureLEDMonitor::fillChannelHisto( const DeCalorimeter& calo, double val,
                                             const LHCb::Detector::Calo::CellID& id, std::string type,
                                             double norm ) const {
  double      rate  = 100;
  std::string name  = type;
  double      min   = -256.;
  double      max   = 3840.;
  double      nbins = 256.;
  if ( type == "Signal" ) {
    rate  = m_histoRateSignal;
    min   = m_rangesSignal[0];
    max   = m_rangesSignal[1];
    nbins = m_rangesSignal[2];
  }
  if ( type == "Pedestal" ) {
    rate  = m_histoRatePedestal;
    min   = m_rangesPedestal[0];
    max   = m_rangesPedestal[1];
    nbins = m_rangesPedestal[2];
  }
  if ( type == "Ratio" ) {
    rate  = m_histoRateRatio;
    min   = m_rangesRatio[0];
    max   = m_rangesRatio[1];
    nbins = m_rangesRatio[2];
  }
  if ( type == "All" ) {
    rate  = m_histoRateAll;
    min   = m_rangesAll[0];
    max   = m_rangesAll[1];
    nbins = m_rangesAll[2];
  }
  if ( rate < m_shoot ) return;

  double value = ( norm != 0 ) ? val / norm : val;
  plot1D( value,
          fmt::format( "{}/crate{:02d}/feb{:02d}/{}", name, calo.cardCrate( calo.cardNumber( id ) ),
                       calo.cardSlot( calo.cardNumber( id ) ),
                       calo.cardColumn( id ) + nColCaloCard * calo.cardRow( id ) ),
          fmt::format( "{} {} data for channel [crate={:02d},feb={:02d},readout={}] => {} (index={})", detData(), name,
                       calo.cardCrate( calo.cardNumber( id ) ), calo.cardSlot( calo.cardNumber( id ) ),
                       calo.cardColumn( id ) + nColCaloCard * calo.cardRow( id ), Gaudi::Utils::toString( id ),
                       id.index() ),
          min, max, nbins );
}

//-------------------------------------------------------
bool CaloFutureLEDMonitor::isInWindow( double value, std::vector<double> windows, bool invert ) const {
  if ( windows.size() % 2 != 0 ) return false;
  std::vector<double>::const_iterator it = windows.begin();
  for ( unsigned int i = 0; i < windows.size() / 2; ++i ) {
    int ind = i * 2;
    if ( value >= *( it + ind ) && value <= *( it + ind + 1 ) ) return invert ? false : true;
  }
  return invert ? true : false;
}
