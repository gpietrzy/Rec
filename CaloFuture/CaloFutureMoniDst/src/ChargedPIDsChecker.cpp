/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloChargedInfo_v1.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"

/** @class ChargedPIDsChecker ChargedPIDsChecker.cpp
 *
 * For MC versus Calo charged PID
 *  - as checker
 *  - for producing DLL tables from the ntuple
 *
 * For now using v1 tracks (no MC relations yet for v3 tracks?)
 */

namespace LHCb::Calo::Algorithms {

  namespace {
    using Track      = LHCb::Event::v1::Track;
    using ChargedPID = LHCb::Event::Calo::v1::CaloChargedPID;
    using BremInfo   = LHCb::Event::Calo::v1::BremInfo;

    using Tracks      = LHCb::Event::v1::Tracks;
    using ChargedPIDs = ChargedPID::Container;
    using BremInfos   = BremInfo::Container;

    template <typename PID>
    using Track2PIDsMap = std::map<Track const*, PID const*>;

    // functions
    template <typename PID>
    Track2PIDsMap<PID> getTrack2PIDsMap( typename PID::Container const& pids ) {
      auto map = Track2PIDsMap<PID>{};
      for ( PID const* pid : pids ) {
        if ( pid->idTrack() ) map[pid->idTrack()] = pid;
      }
      return map;
    }

  } // namespace

  class ChargedPIDsChecker
      : public LHCb::Algorithm::Consumer<void( Tracks const&, ChargedPIDs const&, BremInfos const&,
                                               LHCb::LinksByKey const&, LHCb::MCParticles const&,
                                               DeCalorimeter const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>> {
  public:
    using base_type =
        LHCb::Algorithm::Consumer<void( Tracks const&, ChargedPIDs const&, BremInfos const&, LHCb::LinksByKey const&,
                                        LHCb::MCParticles const&, DeCalorimeter const& ),
                                  LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>>;
    using KeyValue = base_type::KeyValue;

    // standard constructor
    ChargedPIDsChecker( std::string const& name, ISvcLocator* pSvc )
        : base_type{name,
                    pSvc,
                    {KeyValue{"Tracks", ""}, KeyValue{"ChargedPIDs", ""}, KeyValue{"BremInfos", ""},
                     KeyValue{"MCLinks", ""}, KeyValue{"MCParticles", LHCb::MCParticleLocation::Default},
                     KeyValue{"Detector", DeCalorimeterLocation::Ecal}}} {}

    // main execution
    void operator()( Tracks const&, ChargedPIDs const&, BremInfos const&, LHCb::LinksByKey const&,
                     LHCb::MCParticles const&, DeCalorimeter const& ) const override;

  private:
    Gaudi::Property<bool>               m_checktype{this, "CheckTrackType", false};
    Gaudi::Property<LHCb::Track::Types> m_type{this, "TrackType", LHCb::Track::Types::Unknown};

    // monitoring
    mutable Gaudi::Accumulators::StatCounter<float> m_clchi2_el{this, "Track-cluster match chi2 for electrons"};
    mutable Gaudi::Accumulators::StatCounter<float> m_clchi2_pi{this, "Track-cluster match chi2 for pions"};
    mutable Gaudi::Accumulators::StatCounter<float> m_eop_ecal_el{this, "Ecal electron shower E/p for electrons"};
    mutable Gaudi::Accumulators::StatCounter<float> m_eop_ecal_pi{this, "Ecal electron shower E/p for pions"};
    mutable Gaudi::Accumulators::StatCounter<float> m_eop_hcal_el{this, "Hcal E/p for electrons"};
    mutable Gaudi::Accumulators::StatCounter<float> m_eop_hcal_pi{this, "Hcal E/p for pions"};
    mutable Gaudi::Accumulators::StatCounter<float> m_breme_res{this, "Brem reco reso"};
    mutable Gaudi::Accumulators::StatCounter<float> m_breme_cl_res{this, "Brem reco reso (cluster)"};
    mutable Gaudi::Accumulators::StatCounter<float> m_breme_tr_res{this, "Brem reco reso (track-based)"};
  };

  DECLARE_COMPONENT_WITH_ID( ChargedPIDsChecker, "CaloChargedPIDsChecker" )

  //// implementation /////

  // main execution
  void ChargedPIDsChecker::operator()( Tracks const& tracks, ChargedPIDs const& pids, BremInfos const& brems,
                                       LHCb::LinksByKey const& links, LHCb::MCParticles const& mcparts,
                                       DeCalorimeter const& ) const {
    // get map
    auto pidsmap = getTrack2PIDsMap<ChargedPID>( pids );
    auto bremmap = getTrack2PIDsMap<BremInfo>( brems );

    // setup ntuple
    auto tuple = this->nTuple( "TrackTuple" );

    // fill for all tracks
    for ( auto const track : tracks ) {
      // check type if asked
      if ( m_checktype && m_type != track->type() ) continue;
      // get MC truth
      double                  maxWeight{0};
      const LHCb::MCParticle* mcp{nullptr};
      links.applyToLinks( track->key(),
                          [&maxWeight, &mcp, &mcparts]( unsigned int, unsigned int mcPartKey, float weight ) {
                            if ( weight > maxWeight ) {
                              maxWeight = weight;
                              mcp       = static_cast<const LHCb::MCParticle*>( mcparts.containedObject( mcPartKey ) );
                            }
                          } );
      //// fill ntuple with info
      std::string base = "Track_";
      // track info
      auto sc = tuple->column( base + "Type", (int)track->type() );
      sc &= tuple->column( base + "P", track->p() );
      sc &= tuple->column( base + "PT", track->pt() );
      sc &= tuple->column( base + "eta", track->pseudoRapidity() );
      sc &= tuple->column( base + "phi", track->phi() );
      // mc truth
      int mc_id  = mcp ? mcp->particleID().pid() : 0;
      int mc_mid = mcp ? ( mcp->mother() ? mcp->mother()->particleID().pid() : 0 ) : 0;
      int mc_gmid =
          mcp ? ( mcp->mother() ? ( mcp->mother()->mother() ? mcp->mother()->mother()->particleID().pid() : 0 ) : 0 )
              : 0;
      bool  mc_el_good = ( abs( mc_id ) == 11 ) && ( abs( mc_mid ) != 211 ) && ( abs( mc_mid ) != 321 );
      bool  mc_mu_good = ( abs( mc_id ) == 13 ) && ( abs( mc_mid ) != 211 ) && ( abs( mc_mid ) != 321 );
      bool  mc_pion    = abs( mc_id ) == 211;
      float mc_mom     = mcp ? mcp->momentum().P() : 0.f;
      sc &= tuple->column( base + "TRUEID", mc_id );
      sc &= tuple->column( base + "MC_MOTHER_ID", mc_mid );
      sc &= tuple->column( base + "MC_GD_MOTHER_ID", mc_gmid );
      sc &= tuple->column( base + "MC_electron_signal", mc_el_good );
      sc &= tuple->column( base + "MC_electron_fromConv", mc_mid == 22 );
      sc &= tuple->column( base + "MC_muon_signal", mc_mu_good );
      sc &= tuple->column( base + "MC_pion", mc_pion );
      sc &= tuple->column( base + "TRUE_P", mc_mom );
      sc &= tuple->column( base + "TRUE_PT", mcp ? mcp->momentum().Pt() : 0.f );
      sc &= tuple->column( base + "TRUE_eta", mcp ? mcp->momentum().Eta() : 0.f );
      sc &= tuple->column( base + "TRUE_phi", mcp ? mcp->momentum().Phi() : 0.f );
      // charged pids
      auto              charged_rel = pidsmap.find( track );
      ChargedPID const* pids        = ( charged_rel != pidsmap.end() ) ? charged_rel->second : nullptr;
      sc &= tuple->column( base + "InEcal", pids ? pids->InEcal() : 0 );
      sc &= tuple->column( base + "ClusterID", pids ? pids->ClusterID().all() : 0 );
      sc &= tuple->column( base + "ClusterMatch", pids ? pids->ClusterMatch() : -1 );
      sc &= tuple->column( base + "ElectronID", pids ? pids->ElectronID().all() : 0 );
      sc &= tuple->column( base + "ElectronMatch", pids ? pids->ElectronMatch() : -1 );
      sc &= tuple->column( base + "ElectronEnergy", pids ? pids->ElectronEnergy() : -1 );
      sc &= tuple->column( base + "ElectronShowerEoP", pids ? pids->ElectronShowerEoP() : -1.f );
      sc &= tuple->column( base + "ElectronShowerDLL", pids ? pids->ElectronShowerDLL() : 0.f );
      sc &= tuple->column( base + "EcalPIDe", pids ? pids->EcalPIDe() : 0.f );
      sc &= tuple->column( base + "EcalPIDmu", pids ? pids->EcalPIDmu() : 0.f );
      sc &= tuple->column( base + "InHcal", pids ? pids->InHcal() : 0 );
      sc &= tuple->column( base + "HcalEoP", pids ? pids->HcalEoP() : -1.f );
      sc &= tuple->column( base + "HcalPIDe", pids ? pids->HcalPIDe() : 0.f );
      sc &= tuple->column( base + "HcalPIDmu", pids ? pids->HcalPIDmu() : 0.f );
      // brem pids
      auto            brem_rel = bremmap.find( track );
      BremInfo const* brems    = ( brem_rel != bremmap.end() ) ? brem_rel->second : nullptr;
      sc &= tuple->column( base + "InBrem", brems ? brems->InBrem() : 0 );
      sc &= tuple->column( base + "BremHypoID", brems ? brems->BremHypoID().all() : 0 );
      sc &= tuple->column( base + "BremHypoMatch", brems ? brems->BremHypoMatch() : -1.f );
      sc &= tuple->column( base + "BremHypoEnergy", brems ? brems->BremHypoEnergy() : -1.f );
      sc &= tuple->column( base + "BremHypoDeltaX", brems ? brems->BremHypoDeltaX() : -1000.f );
      sc &= tuple->column( base + "BremBendingCorrection", brems ? brems->BremBendingCorrection() : 1.f );
      sc &= tuple->column( base + "BremTrackBasedEnergy", brems ? brems->BremTrackBasedEnergy() : -1.f );
      sc &= tuple->column( base + "BremEnergy", brems ? brems->BremEnergy() : 0.f );
      sc &= tuple->column( base + "BremPIDe", brems ? brems->BremPIDe() : 0.f );
      // monitoring
      if ( mc_el_good && pids ) {
        m_clchi2_el += pids->ClusterMatch();
        m_eop_ecal_el += pids->ElectronShowerEoP();
        m_eop_hcal_el += pids->HcalEoP();
      }
      if ( mc_pion && pids ) {
        m_clchi2_pi += pids->ClusterMatch();
        m_eop_ecal_pi += pids->ElectronShowerEoP();
        m_eop_hcal_pi += pids->HcalEoP();
      }
      auto res = []( float ptrack, float pbrem, float ptrue ) {
        float reso = ( ptrack + pbrem - ptrue ) / ptrue;
        return ( reso < 1.f ) ? reso : 1.f;
      };
      if ( mc_el_good && ( mc_mid != 22 ) && brems && brems->BremEnergy() > 0.f ) {
        auto res_clu = res( track->p(), brems->BremHypoEnergy(), mc_mom );
        auto res_tra = res( track->p(), brems->BremTrackBasedEnergy(), mc_mom );
        auto res_gen = res( track->p(), brems->BremEnergy(), mc_mom );
        if ( res_clu < 1. ) m_breme_cl_res += res_clu;
        if ( res_tra < 1. ) m_breme_tr_res += res_tra;
        if ( res_gen < 1. ) m_breme_res += res_gen;
      }
      // write result
      sc.andThen( [&] { return tuple->write(); } ).ignore();
    }
  }

} // namespace LHCb::Calo::Algorithms
