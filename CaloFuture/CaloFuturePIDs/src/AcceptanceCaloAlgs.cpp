/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/

#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "Event/SOACollection.h"
#include "Gaudi/Accumulators.h"
#include "Kernel/meta_enum.h"
#include "LHCbAlgs/Transformer.h"

/** @class AcceptanceCaloAlg AcceptanceCaloAlg.h
 *
 *  Algorithms that produce calorimeter acceptance tables for a set of tracks (v3 Tracks):
 *   - AcceptanceBremAlg: for bremsstrahlung from pre-magnet track states (recovery)
 *   - AcceptanceEcalAlg: track in Ecal
 *   - AcceptanceHcalAlg: track in Hcal
 *
 *  Note: saves CellID from extrapolation as extra info in table,
 *        the ones with a relation are in acceptance.
 *
 */

// ============================================================================
/** @file
 *
 *  Implementation file for class AcceptanceCaloAlg
 *
 */

namespace LHCb::Calo {

  // acceptance types
  meta_enum_class( CaloAcceptance, int, Unknown = -1, Brem, Ecal, Hcal );

  namespace {

    using namespace LHCb::Calo::TrackUtils;

    // miscellaneous definitions
    using MyState = LHCb::Event::v3::detail::FittedState<
        Tracks::FittedProxy<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous, const LHCb::Event::v3::Tracks>>;

    // calorimeter acceptance, linear from given state to calorimeter plane
    int acceptance( LHCb::StateVector& state, MyState const& ref_state, DeCalorimeter const& calo,
                    Gaudi::Plane3D const& plane, bool const fiducial ) {
      // linear extrapolation to calo
      if ( !propagateToCalo( state, ref_state, plane ) ) return 0;
      // check position
      const auto cell = calo.Cell_( state.position() );
      if ( !cell || !cell->valid() ) { return 0; }
      // check for fiducial volume ?
      if ( !fiducial ) { return cell->cellID().all(); }
      // check for neighbours
      const auto& neighbours = cell->neighbors();
      // regular cell: >= 8 valid neighbours
      if ( 8 <= neighbours.size() ) { return cell->cellID().all(); }
      // incomplete neibours: border of 2 zones ?
      return std::any_of( std::next( neighbours.begin() ), neighbours.end(),
                          [area = cell->cellID().area()]( const auto& n ) { return n.area() != area; } )
                 ? cell->cellID().all()
                 : 0;
    }

    // acceptance type to extrapolation state
    // also taking into account which type of tracks are allowed
    std::optional<SL> extrapolation_stateloc_acc( CaloAcceptance acceptance, Tracks const& tracks ) {
      if ( acceptance == CaloAcceptance::Unknown ) return std::nullopt;
      // type to state location (if not avaible, not valid type)
      switch ( tracks.type() ) {
      case TT::Long:
        return acceptance == CaloAcceptance::Brem ? SL::ClosestToBeam : SL::EndRich2;
      case TT::Downstream:
        return acceptance == CaloAcceptance::Brem ? SL::FirstMeasurement : SL::EndRich2;
      case TT::Velo:
      case TT::Upstream:
        return acceptance == CaloAcceptance::Brem ? std::optional{SL::ClosestToBeam} : std::nullopt;
      case TT::Ttrack:
        return ( acceptance == CaloAcceptance::Ecal ) || ( acceptance == CaloAcceptance::Hcal )
                   ? std::optional{SL::LastMeasurement}
                   : std::nullopt;
      default:
        return std::nullopt;
      }
    }

  } // namespace

  // main class
  template <typename OutputData>
  class AcceptanceCaloAlg : public Algorithm::Transformer<OutputData( Tracks const&, DeCalorimeter const& ),
                                                          DetDesc::usesConditions<DeCalorimeter>> {
  public:
    // standard constructor
    AcceptanceCaloAlg( const std::string& name, ISvcLocator* pSvc );

    // main function/operator
    OutputData operator()( Tracks const&, DeCalorimeter const& ) const override;

  private:
    // properties
    Gaudi::Property<CaloAcceptance>   m_acc_type{this, "AcceptanceType", CaloAcceptance::Unknown, "Acceptance type."};
    Gaudi::Property<CaloPlane::Plane> m_plane{this, "CalorimeterPlane", CaloPlane::ShowerMax,
                                              "Calorimeter plane to where track state is extrapolated."};
    Gaudi::Property<bool>             m_fiducial{this, "UseFiducial", true,
                                     "Only consider area where calo-track intersection is in fiducial volume of calo."};

    // statistics
    mutable Gaudi::Accumulators::StatCounter<> m_nTracks{this, "#total tracks"};
    mutable Gaudi::Accumulators::StatCounter<> m_nAccept{this, "#tracks in acceptance"};
  };

  // ============================= IMPLEMENTATION ===============================

  // ============================================================================
  /*  Standard constructor
   *  @param name algorithm name
   *  @param pSvc service locator
   */
  // ============================================================================
  template <typename OutputData>
  AcceptanceCaloAlg<OutputData>::AcceptanceCaloAlg( const std::string& name, ISvcLocator* pSvc )
      : AcceptanceCaloAlg<OutputData>::Transformer(
            name, pSvc,
            // Inputs
            {typename AcceptanceCaloAlg<OutputData>::KeyValue( "Tracks", "" ),
             typename AcceptanceCaloAlg<OutputData>::KeyValue( "Calorimeter", "" )},
            // Output
            {typename AcceptanceCaloAlg<OutputData>::KeyValue( "Output", "" )} ) {}

  // ============================================================================
  //   Algorithm execution
  // ============================================================================
  template <typename OutputData>
  OutputData AcceptanceCaloAlg<OutputData>::operator()( Tracks const& tracks, DeCalorimeter const& calo ) const {
    // declare output
    OutputData output_table( &tracks );
    output_table.reserve( tracks.size() );

    // track state from where to extrapolate (linearly) to calo from
    auto const state_loc = extrapolation_stateloc_acc( m_acc_type.value(), tracks );
    if ( !state_loc.has_value() ) {
      throw GaudiException( "Not a valid combination of track type and calo acceptance type.",
                            "LHCb::Event::Enum::Track::Type and LHCb::Calo::CaloAcceptance", StatusCode::FAILURE );
    }

    // miscellaneous info
    LHCb::StateVector state;
    auto const        plane   = calo.plane( m_plane );
    int               nAccept = 0;

    // loop over input tracks
    for ( auto const& track : tracks.scalar() ) {
      auto   ref_state = track.state( state_loc.value() );
      auto   cellid    = acceptance( state, ref_state, calo, plane, m_fiducial );
      mask_v incalo    = ( cellid != 0 ) && track.loop_mask();
      nAccept += popcount( incalo );
      // save result only for accepted tracks (with index from original container)
      output_table.template compress_back<SIMDWrapper::InstructionSet::Scalar>( incalo ).set( track.indices(), cellid );
    }

    // statistics
    m_nTracks += tracks.size();
    m_nAccept += nAccept;

    return output_table;
  }

  // ========================= SPECIFIC IMPLEMENTATION ==========================

  // ============================================================================
  // instances of calorimeter acceptance for: brem, ecal, hcal, respectively
  // ============================================================================
  struct AcceptanceBremAlg : AcceptanceCaloAlg<TracksInBrem> {
    // constructor
    AcceptanceBremAlg( const std::string& name, ISvcLocator* pSvc ) : AcceptanceCaloAlg<TracksInBrem>( name, pSvc ) {
      setProperty( "AcceptanceType", CaloAcceptance::Brem ).orThrow( "Wrong calorimeter acceptance type" );
      setProperty( "CalorimeterPlane", CaloPlane::ShowerMax ).ignore();
      updateHandleLocation( *this, "Calorimeter", CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) );
    }
  };

  struct AcceptanceEcalAlg : AcceptanceCaloAlg<TracksInEcal> {
    // constructor
    AcceptanceEcalAlg( const std::string& name, ISvcLocator* pSvc ) : AcceptanceCaloAlg<TracksInEcal>( name, pSvc ) {
      setProperty( "AcceptanceType", CaloAcceptance::Ecal ).orThrow( "Wrong calorimeter acceptance type" );
      setProperty( "CalorimeterPlane", CaloPlane::ShowerMax ).ignore();
      updateHandleLocation( *this, "Calorimeter", CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" ) );
    }
  };

  struct AcceptanceHcalAlg : AcceptanceCaloAlg<TracksInHcal> {
    // constructor
    AcceptanceHcalAlg( const std::string& name, ISvcLocator* pSvc ) : AcceptanceCaloAlg<TracksInHcal>( name, pSvc ) {
      setProperty( "AcceptanceType", CaloAcceptance::Hcal ).orThrow( "Wrong calorimeter acceptance type" );
      setProperty( "CalorimeterPlane", CaloPlane::Middle ).ignore();
      updateHandleLocation( *this, "Calorimeter", CaloFutureAlgUtils::DeCaloFutureLocation( "Hcal" ) );
    }
  };

} // namespace LHCb::Calo

DECLARE_COMPONENT_WITH_ID( LHCb::Calo::AcceptanceBremAlg, "AcceptanceBremAlg" )
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::AcceptanceEcalAlg, "AcceptanceEcalAlg" )
DECLARE_COMPONENT_WITH_ID( LHCb::Calo::AcceptanceHcalAlg, "AcceptanceHcalAlg" )
