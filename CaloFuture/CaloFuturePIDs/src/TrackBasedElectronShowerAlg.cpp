/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
 \*****************************************************************************/
#include "AIDA/IHistogram3D.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/TrackUtils.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloDigits_v2.h"
#include "Gaudi/Accumulators.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ServiceHandle.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/FastMaths.h"
#include "SelectiveMatchUtils.h"
#include "TH3D.h"
#include "TMemFile.h"

namespace LHCb::Calo {

  using namespace LHCb::Calo::TrackUtils;
  using namespace LHCb::Calo::SelectiveMatchUtils;
  using namespace LHCb::Math::Approx;

  using Digits     = LHCb::Event::Calo::Digits;
  using OutputData = TracksElectronShower;

  // local helper objects / functions for class
  namespace {
    // some useful constants
    namespace Constants {
      constexpr float pi      = float( M_PI );
      constexpr float piover2 = float( M_PI / 2. );
      constexpr float eps     = 1e-6;
      constexpr float inv_gev = 1.f / Gaudi::Units::GeV;

      const float log10     = log( 10.f * Gaudi::Units::GeV );
      const float log100    = log( 100.f * Gaudi::Units::GeV );
      const float midmom    = exp( ( log100 - log10 ) / 2.f );
      const float dlog_inv  = 1.f / log( 10.f );
      const float norm_fmin = log( 1.f / 0.01f - 1.f );
    } // namespace Constants

    // bookkeeping
    enum HistType { Height10, Height100, Mid, Rate, DLL };
    enum Region { All = -1, Outer = 0, Middle = 1, Inner = 2 };

    struct HistIndex {
      HistType    type;
      Region      region;
      friend bool operator<( HistIndex const& lhs, HistIndex const& rhs ) {
        return std::pair{lhs.type, lhs.region} < std::pair{rhs.type, rhs.region};
      }
    };

    //   for expected fraction and measured cell energy
    struct ShowerEntry {
      float energy   = 0.;
      float fraction = 0.;
    };

    //   for parametrization of shower versus cell
    struct ShowerParams {
      float  d        = 0.;
      float  theta    = 0.;
      float  txy      = 0.;
      float  lbar     = 0.;
      Region region   = Region::All;
      float  momentum = 0.;
    };

    //   for parametrization of sigmoid function for calculating
    //   energy fraction from distance to cell
    struct SigmoidParams {
      float height = 0.;
      float mid    = 0.;
      float rate   = 0.;
    };

    // parametrizes cell energy fraction for closest distance from cell center to track projection (xy)
    float Sigmoid( const float distance, const SigmoidParams pars ) {
      return pars.height / ( 1.f + vapprox_exp( -pars.rate * ( distance - pars.mid ) ) );
    }

    // conditions holder (parametrization histograms, from CondDB or THS)
    using CellEnergyParams = HistoStore<HistIndex, TH3D, AIDA::IHistogram3D>;

    // mapping from type to location in store
    CellEnergyParams::HistoMap getHistoMap() {
      auto map = CellEnergyParams::HistoMap{};
      // config
      const std::map<Region, std::string> regs = {
          {Region::Outer, "outer"}, {Region::Middle, "middle"}, {Region::Inner, "inner"}};
      const std::map<HistType, std::string> hnames = {
          {HistType::Height10, "h10"}, {HistType::Height100, "h100"}, {HistType::Mid, "mid"}, {HistType::Rate, "rate"}};
      for ( auto const& reg : regs ) {
        for ( auto const& hname : hnames ) {
          map[{hname.first, reg.first}] = "hist_" + hname.second + "_" + reg.second;
        }
      }
      map[{HistType::DLL, Region::All}] = "hist_dll";
      return map;
    }

    // obtains the sigmoid parameters obtain from simulation
    void getSigmoidParams( SigmoidParams& sigpars, const ShowerParams& showpars, const CellEnergyParams& ceps,
                           int iplevel ) {
      // for momentum dependence is logarithmic, interpolate accordingly
      // interpolate with other shower parameters if asked
      switch ( iplevel ) {
      case 1: {
        // hybrid version: only interpolate for height if close enough, rest binned
        const int iGlobalBin = ceps.hist( {HistType::Height10, showpars.region} )
                                   ->FindFixBin( showpars.theta, showpars.txy, showpars.lbar );
        sigpars.mid  = ceps.hist( {HistType::Mid, showpars.region} )->GetBinContent( iGlobalBin );
        sigpars.rate = ceps.hist( {HistType::Rate, showpars.region} )->GetBinContent( iGlobalBin );
        // max distance coincides with min fraction of 0.01 for height 1
        if ( showpars.d < sigpars.mid - Constants::norm_fmin / sigpars.rate ) {
          bool  switch_h = showpars.momentum > Constants::midmom;
          float height   = ceps.hist( {( switch_h ) ? HistType::Height100 : HistType::Height10, showpars.region} )
                             ->Interpolate( showpars.theta, showpars.txy, showpars.lbar );
          float delta_h = ceps.hist( {HistType::Height100, showpars.region} )->GetBinContent( iGlobalBin ) -
                          ceps.hist( {HistType::Height10, showpars.region} )->GetBinContent( iGlobalBin );
          sigpars.height = delta_h * std::max( ( LHCb::Math::Approx::approx_log( showpars.momentum ) -
                                                 ( switch_h ? Constants::log100 : Constants::log10 ) ) *
                                                   Constants::dlog_inv,
                                               ( switch_h ? -1.f : 0.f ) ) +
                           height;
        } else {
          float h10      = ceps.hist( {HistType::Height10, showpars.region} )->GetBinContent( iGlobalBin );
          float h100     = ceps.hist( {HistType::Height100, showpars.region} )->GetBinContent( iGlobalBin );
          sigpars.height = ( h100 - h10 ) *
                               std::max( LHCb::Math::Approx::approx_log( showpars.momentum ) - Constants::log10, 0.f ) *
                               Constants::dlog_inv +
                           h10;
        }
        return;
      }
      case 2: {
        // interpolate all
        float h10 = ceps.hist( {HistType::Height10, showpars.region} )
                        ->Interpolate( showpars.theta, showpars.txy, showpars.lbar );
        float h100 = ceps.hist( {HistType::Height100, showpars.region} )
                         ->Interpolate( showpars.theta, showpars.txy, showpars.lbar );
        sigpars.height = ( h100 - h10 ) *
                             std::max( LHCb::Math::Approx::approx_log( showpars.momentum ) - Constants::log10, 0.f ) *
                             Constants::dlog_inv +
                         h10;
        sigpars.mid =
            ceps.hist( {HistType::Mid, showpars.region} )->Interpolate( showpars.theta, showpars.txy, showpars.lbar );
        sigpars.rate =
            ceps.hist( {HistType::Rate, showpars.region} )->Interpolate( showpars.theta, showpars.txy, showpars.lbar );
        return;
      }
      default: {
        // no interpolation
        const int iGlobalBin = ceps.hist( {HistType::Height10, showpars.region} )
                                   ->FindFixBin( showpars.theta, showpars.txy, showpars.lbar );
        float h10      = ceps.hist( {HistType::Height10, showpars.region} )->GetBinContent( iGlobalBin );
        float h100     = ceps.hist( {HistType::Height100, showpars.region} )->GetBinContent( iGlobalBin );
        sigpars.height = ( h100 - h10 ) *
                             std::max( LHCb::Math::Approx::approx_log( showpars.momentum ) - Constants::log10, 0.f ) *
                             Constants::dlog_inv +
                         h10;
        sigpars.mid  = ceps.hist( {HistType::Mid, showpars.region} )->GetBinContent( iGlobalBin );
        sigpars.rate = ceps.hist( {HistType::Mid, showpars.region} )->GetBinContent( iGlobalBin );
        return;
      }
      }
    }

    // for histogram interpolation, forced range is needed
    constexpr auto forceinrange = []( const float value, const std::pair<float, float>& range ) {
      return std::min( std::max( value, range.first ), range.second );
    };

    // obtain histogram ranges
    std::array<std::pair<float, float>, 3> getHist3DRanges( const TH3D* hist ) {
      std::array<std::pair<float, float>, 3> range;
      range[0] = {(float)hist->GetXaxis()->GetBinCenter( 1 ) + Constants::eps,
                  (float)hist->GetXaxis()->GetBinCenter( hist->GetXaxis()->GetNbins() ) - Constants::eps};
      range[1] = {(float)hist->GetYaxis()->GetBinCenter( 1 ) + Constants::eps,
                  (float)hist->GetYaxis()->GetBinCenter( hist->GetYaxis()->GetNbins() ) - Constants::eps};
      range[2] = {(float)hist->GetZaxis()->GetBinCenter( 1 ) + Constants::eps,
                  (float)hist->GetZaxis()->GetBinCenter( hist->GetZaxis()->GetNbins() ) - Constants::eps};
      return range;
    }

    // scan along track in z for intersect cells
    void scanForCells( std::vector<Detector::Calo::CellID>& cellids, Gaudi::XYZPoint scanpos,
                       const Gaudi::XYZVector& slopes, const double deltaz, const int nscans,
                       const DeCalorimeter& calo ) {
      auto stepsize = deltaz / ( nscans - 1 );
      for ( int i = 0; i < nscans; i++ ) {
        scanpos += stepsize * slopes;
        const auto cpar = calo.Cell_( scanpos );
        if ( cpar && cpar->valid() ) cellids.push_back( cpar->cellID() );
      }
      // remove duplicates if there are any
      std::sort( cellids.begin(), cellids.end() );
      cellids.erase( std::unique( cellids.begin(), cellids.end() ), cellids.end() );
    }

  } // namespace

  /**
   *  Algorithm that selects calo cells using per-cell energy estimates parametrized based
   *  on track information (instead of calo clustering). Estimates obtained from ideal EM shower
   *  shapes MC. Subsequently can get selective E/p and summed per-cell DLL variables for
   *  improved PID performance over cluster/track-intersection based variables.
   *
   *  @date   2020-12
   *  @author Maarten VAN VEGHEL
   */
  class TrackBasedElectronShowerAlg
      : public LHCb::Algorithm::Transformer<OutputData( TracksInEcal const&, Digits const&, DeCalorimeter const& ),
                                            DetDesc::usesConditions<DeCalorimeter>> {
  public:
    TrackBasedElectronShowerAlg( const std::string& name, ISvcLocator* pSvc );
    StatusCode initialize() override;
    OutputData operator()( TracksInEcal const&, Digits const&, DeCalorimeter const& ) const override;

  private:
    // general algorithm properties
    Gaudi::Property<float> m_fmin{this, "minFraction", 0.1, "Minimum expected energy fraction for use in E/p"};
    Gaudi::Property<int>   m_nzplanes{this, "nPlanesInZ", 6,
                                    "Number of planes in z to scan for track-intersection cells"};
    int                    m_nmaxelements;
    Gaudi::Property<int>   m_nsquares{this,
                                    "nNeighborSquares",
                                    1,
                                    [this]( auto& ) { m_nmaxelements = std::pow( 2 * m_nsquares + 1, 2 ); },
                                    Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                    "Number of squares of neighbours added around track-intersection cells"};
    Gaudi::Property<int>   m_iplevel{
        this, "interpolationLevel", 1,
        "Level of interpolation with sigmoid parameter histograms (0 is none; 1 only height; 2 all)"};
    Gaudi::Property<std::vector<float>> m_maxcelldists{
        this,
        "maxCellDistances",
        {160 * Gaudi::Units::mm, 140 * Gaudi::Units::mm, 120 * Gaudi::Units::mm},
        "maximum cell distances at which full energy-expectation calculation is still done, per region (outer, middle, "
        "inner)"};
    // parameter origins
    Gaudi::Property<int>         m_historigin{this, "histOrigin", 1,
                                      "Method to obtain histograms: 0 is histogram service; 1 is ParamFiles"};
    Gaudi::Property<std::string> m_histo_location{this, "HistoLocation", "CALO/TRACKBASEDPIDE",
                                                  "Histogram location for histoSvc/ParamFiles"};
    Gaudi::Property<std::string> m_paramfiles_location{
        this, "ParamFilesLocation", "paramfile://data/CaloPID/eshower_trackbased_parametrization.root",
        "Location of ROOT file for parametrization histograms"};

    // parameter histograms
    std::unique_ptr<CellEnergyParams const> m_cellparams;

    // services
    ServiceHandle<IFileAccess> m_filesvc{this, "FileAccessor", "ParamFileSvc",
                                         "Service used to retrieve file contents"};

    // statistics
    mutable Gaudi::Accumulators::StatCounter<>      m_nfailures{this, "#tracks that failed"};
    mutable Gaudi::Accumulators::StatCounter<>      m_nnocells{this, "#tracks with no associated core cells"};
    mutable Gaudi::Accumulators::StatCounter<float> m_eop{this, "average E/p"};
    mutable Gaudi::Accumulators::StatCounter<float> m_dll{this, "average DLL"};
  };

  DECLARE_COMPONENT_WITH_ID( TrackBasedElectronShowerAlg, "TrackBasedElectronShowerAlg" )

  // ============================= IMPLEMENTATION ===============================

  TrackBasedElectronShowerAlg::TrackBasedElectronShowerAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     // Inputs
                     {KeyValue( "TracksInCalo", "" ), KeyValue( "Digits", {CaloDigitLocation::Ecal} ),
                      KeyValue( "Detector", {CaloFutureAlgUtils::DeCaloFutureLocation( "Ecal" )} )},
                     // Outputs
                     {KeyValue( "Output", "" )} ) {}

  StatusCode TrackBasedElectronShowerAlg::initialize() {
    return Transformer::initialize().andThen( [&] {
      switch ( m_historigin ) {
      case 0: {
        // use the HistogramSvc
        IHistogramSvc* hSvc = histoSvc();
        std::string    hLoc = m_histo_location.value();
        info() << "getting parametrization histograms from HistogramSvc at: " << hLoc << endmsg;
        auto hMap    = getHistoMap();
        m_cellparams = std::make_unique<CellEnergyParams>( hSvc, hLoc, hMap );
        break;
      }
      case 1: {
        // use ParamFiles directly
        std::string hLoc = m_histo_location.value();
        info() << "getting parametrization histograms from " << m_paramfiles_location.value() << endmsg;
        auto buffer  = m_filesvc->read( m_paramfiles_location );
        m_cellparams = std::make_unique<CellEnergyParams>( TMemFile::ZeroCopyView_t{buffer->data(), buffer->size()},
                                                           m_histo_location.value(), getHistoMap() );
        break;
      }
      }
    } );
  }

  OutputData TrackBasedElectronShowerAlg::operator()( TracksInEcal const& tracksincalo, Digits const& digits,
                                                      DeCalorimeter const& calo ) const {
    // declare output
    OutputData output_table( tracksincalo.from() );
    output_table.reserve( tracksincalo.size() );

    // track state from where to extrapolate (linearly) to calo from
    auto const state_loc = extrapolation_stateloc( *tracksincalo.from() );
    if ( !state_loc.has_value() ) {
      throw GaudiException( "Not a valid track type for this calo energy type.", "LHCb::Event::Enum::Track::Type",
                            StatusCode::FAILURE );
    }

    // initialize info for calo scan
    const float ecal_zsize      = calo.zSize();
    const auto  plane_calofront = calo.plane( CaloPlane::Front );

    // histogram ranges
    auto range_sp  = getHist3DRanges( m_cellparams->hist( {HistType::Rate, Region::Outer} ) );
    auto range_dll = getHist3DRanges( m_cellparams->hist( {HistType::DLL, Region::All} ) );

    // objects needed inside loop
    LHCb::StateVector state_front;
    Gaudi::XYZVector  slopes;
    Gaudi::XYZPoint   cellcenter;
    Gaudi::Vector2F   track_pos, xtrack, xcell, dist;

    std::vector<Detector::Calo::CellID> cellids;
    cellids.reserve( m_nmaxelements * m_nzplanes );

    std::vector<ShowerEntry> showerentries;
    showerentries.reserve( m_nmaxelements * m_nzplanes );

    ShowerParams  showerparam;
    SigmoidParams sigpars;

    // main loop over tracks
    for ( const auto& trackincalo : tracksincalo.scalar() ) {
      // get state at front calo plane
      auto track     = trackincalo.from();
      auto ref_state = track.state( state_loc.value() );
      if ( !propagateToCalo( state_front, ref_state, plane_calofront ) ) continue;

      // obtain relevant track info
      showerparam.momentum = state_front.p();
      if ( showerparam.momentum <= 0. ) {
        m_nfailures += 1;
        continue;
      }

      slopes    = state_front.slopes();
      track_pos = Gaudi::Vector2F( state_front.x(), state_front.y() );
      xtrack    = ecal_zsize * Gaudi::Vector2F( slopes.x(), slopes.y() );

      showerparam.txy = forceinrange( approx_sqrt( (float)slopes.Mag2() - 1.f ), range_sp[1] );

      // find relevant cells:
      // - first look for track-intersection cells
      cellids.clear();
      scanForCells( cellids, state_front.position(), state_front.slopes(), ecal_zsize, m_nzplanes, calo );
      if ( !cellids.size() ) {
        m_nnocells += 1;
        continue;
      }
      // - obtain their neigbours
      getNeighborCellIDs( cellids, calo, m_nsquares );

      // calculate for each cell the expected energy fraction and obtain measured energy (digit)
      showerentries.clear();
      for ( const auto& cellid : cellids ) {
        ShowerEntry showentry;
        // cell/track difference
        cellcenter         = calo.cellCenter( cellid );
        xcell              = Gaudi::Vector2F( cellcenter.x() - track_pos[0], cellcenter.y() - track_pos[1] );
        showerparam.region = static_cast<Region>( cellid.area() );
        showerparam.lbar   = ( showerparam.txy > Constants::eps )
                               ? forceinrange( Dot( xtrack, xcell ) / Dot( xtrack, xtrack ), range_sp[2] )
                               : Constants::eps;
        dist          = xcell - showerparam.lbar * xtrack;
        showerparam.d = approx_sqrt( Dot( dist, dist ) );
        // further calculation only relevant at small distances
        if ( showerparam.d > m_maxcelldists[showerparam.region] ) {
          showentry.fraction = 0.f;
        } else {
          showerparam.theta = forceinrange(
              fmod( LHCb::Math::fast_atan2( dist[1], dist[0] ) + Constants::pi, Constants::piover2 ), range_sp[0] );
          getSigmoidParams( sigpars, showerparam, *m_cellparams, m_iplevel );
          showentry.fraction = Sigmoid( showerparam.d, sigpars );
        }
        // obtain measured energy
        auto digit       = digits( cellid );
        showentry.energy = ( digit ) ? digit->energy() : 0.f;
        // add to list of checked cells
        showerentries.push_back( showentry );
      }

      // calculate relevant variables from energy fractions:
      // - summed (but selective) energy over momentum (eop)
      // - summed per-cell eop-DLL
      float energy = 0.f;
      float dll    = 0.f;
      // some pre-caculation for DLL
      float logmom =
          forceinrange( LHCb::Math::Approx::approx_log( showerparam.momentum * Constants::inv_gev ), range_dll[2] );
      float invmom = 1.f / showerparam.momentum;
      // obtain eops and DLL
      for ( const auto& se : showerentries ) {
        if ( se.fraction >= m_fmin ) energy += se.energy;
        if ( se.fraction > range_dll[1].first ) {
          dll += m_cellparams->hist( {HistType::DLL, Region::All} )
                     ->Interpolate( forceinrange( invmom * se.energy, range_dll[0] ),
                                    forceinrange( se.fraction, range_dll[1] ), logmom );
        }
      }

      // push results to tables
      auto eop = ( energy > 0. ) ? energy / showerparam.momentum : 0.f;
      output_table.add( track, eop, dll );
    }

    // monitor statistics
    const auto nLinks = output_table.size();
    for ( auto const& proxy : output_table.scalar() ) m_eop += proxy.get<ElectronShowerEoP>().cast() / nLinks;
    for ( auto const& proxy : output_table.scalar() ) m_dll += proxy.get<ElectronShowerDLL>().cast() / nLinks;

    return output_table;
  }

} // namespace LHCb::Calo
