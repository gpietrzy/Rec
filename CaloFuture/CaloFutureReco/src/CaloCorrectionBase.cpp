/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloCorrectionBase.h"
#include "Core/FloatComparison.h"
#include "Event/ProtoParticle.h"

#include <yaml-cpp/yaml.h>

using namespace LHCb::Math::Approx;

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::Event::Calo::Hypotheses::Type>& r, const std::string& s ) {
    std::vector<std::string> sv;
    return parse( sv, s ).orThrow( "Oops", "Oops!" ).andThen( [&] {
      for ( const auto& i : sv ) {
        LHCb::Event::Calo::Hypotheses::Type t;
        parse( t, i ).andThen( [&] { r.push_back( t ); } ).orThrow( "Oops", "Oops!" ).ignore();
      }
    } );
  }
} // namespace Gaudi::Parsers

//-----------------------------------------------------------------------------
// Implementation file for class : CaloFutureCorrectionBase
//
// 2010-05-07 : Olivier Deschamps
//-----------------------------------------------------------------------------
namespace LHCb::Calo::Correction {

  Result Polynomial::correction( float var ) const {
    float cor = pars.front();
    float v   = var;
    for ( auto i = std::next( pars.begin() ); i != pars.end(); ++i ) {
      cor += ( *i ) * v;
      v *= var;
    }
    float derivative = 0.;
    v                = 1.;
    int cnt          = 1;
    for ( auto i = std::next( pars.begin() ); i != pars.end(); ++i, ++cnt ) {
      derivative += cnt * ( *i ) * v;
      v *= var;
    }
    return {cor, derivative};
  }
  Result InversePolynomial::correction( float var ) const {
    auto cor = Polynomial::correction( var );
    if ( !essentiallyZero( cor.value ) ) cor.value = 1. / cor.value;
    cor.derivative *= -cor.value * cor.value;
    return cor;
  }

  Result ExpPolynomial::correction( float var ) const {
    auto cor = Polynomial::correction( var );
    if ( !essentiallyZero( cor.value ) ) { cor.value = ( essentiallyZero( var ) ? cached : vapprox_exp( cor.value ) ); }
    cor.derivative *= cor.value;
    return cor;
  }

  Result ReciprocalPolynomial::correction( float var ) const {
    if ( essentiallyZero( var ) ) return {pars[0], 0.}; // CHECKME
    auto cor = Polynomial::correction( 1.0 / var );
    cor.derivative *= -1.0 / ( var * var );
    return cor;
  }

  Result Sigmoid::correction( float var ) const {
    auto mytanh = []( float x ) {
      const float y = vapprox_exp( -2 * x );
      return ( 1 - y ) / ( 1 + y );
    };
    float mytanh_val = mytanh( c * ( var + d ) );
    return {a + b * mytanh_val, b * c * ( 1 - mytanh_val * mytanh_val )};
  }

  Result Sshape::correction( float var ) const {
    if ( b > 0 ) {
      float arg  = cache * var / delta;
      float sapo = approx_sqrt( arg * arg + 1.f );
      return {b * LHCb::Math::Approx::approx_log( arg + sapo ), b / delta * cache / sapo};
    }
    return {0, 0}; // CHECKME
  }

  Result Sinusoidal::correction( float var ) const {
    constexpr float twopi = 2 * M_PI;
    float           sin_val, cos_val;
    LHCb::Math::fast_sincos( twopi * var, sin_val, cos_val );
    return {A * sin_val, A * twopi * cos_val};
  }

  Result ShowerProfile::correction( float var ) const {
    if ( var > 0.5 ) {
      auto tmp1 = pars[0] * vapprox_exp( -pars[1] * var );
      auto tmp2 = pars[2] * vapprox_exp( -pars[3] * var );
      auto tmp3 = pars[4] * vapprox_exp( -pars[5] * var );
      return {tmp1 + tmp2 + tmp3, -pars[1] * tmp1 - pars[3] * tmp2 - pars[5] * tmp3};
    } else {
      auto tmp1 = pars[6] * vapprox_exp( -pars[7] * var );
      auto tmp2 = pars[8] * vapprox_exp( -pars[9] * var );
      return {2 - tmp1 - tmp2, pars[7] * tmp1 + pars[9] * tmp2};
    }
  }

  DECLARE_COMPONENT_WITH_ID( Base, "CaloFutureCorrectionBase" )

  Base::Base( const std::string& type, const std::string& name, const IInterface* parent )
      : ConditionAccessorHolder( type, name, parent ) {
    declareInterface<Base>( this );
  }

  StatusCode Base::initialize() {
    return ConditionAccessorHolder::initialize().andThen( [&]() -> StatusCode {
      if ( m_hypos.empty() ) return Error( "Empty vector of allowed Calorimeter Hypotheses!" );
      addConditionDerivation( {m_conditionName}, m_params.key(), [&]( YAML::Node const& cond ) {
        Parameters params;
        for ( auto it = cond.begin(); it != cond.end(); it++ ) {
          auto type = accept( it->first.as<std::string>() );
          if ( !type ) continue;
          const auto& parVec = it->second.as<std::vector<double>>();
          if ( parVec.size() < 2 ) {
            error() << " # of parameters is insufficient!!" << endmsg;
            continue;
          }
          constructParams( params[static_cast<int>( *type )], parVec );
        }
        return params;
      } );
      return StatusCode::SUCCESS;
    } );
  }

  StatusCode Base::finalize() {
    if ( m_corrections.size() > 1 || *( m_corrections.begin() ) != "All" ) {
      for ( const auto& c : m_corrections ) { info() << "Accepted corrections :  '" << c << "'" << endmsg; }
    }
    if ( m_corrections.empty() ) warning() << "All corrections have been disabled for " << name() << endmsg;
    m_hypos.clear();
    return ConditionAccessorHolder::finalize(); // must be called after all other actions
  }

  void Base::constructParams( Functions& fun, LHCb::span<const double> params ) {
    // NOTE: conversion from double (params) to float (ParamVector)
    const auto func = static_cast<FunctionType>( std::lround( params[0] ) );
    const auto dim  = static_cast<int>( std::lround( params[1] ) );
    if ( func == FunctionType::GlobalParamList ) {
      if ( dim + 2 != static_cast<int>( params.size() ) ) {
        warning() << "o Size of DB parameter vector does not match the expectation for the DB specified type."
                  << endmsg;
      }
      for ( auto& i : fun ) i = ParamVector{{std::next( params.begin(), 2 ), params.end()}};
    } else {
      if ( 3 * dim + 2 != static_cast<int>( params.size() ) ) {
        warning() << "o Size of DB parameter vector does not match the expectation for the DB specified type."
                  << endmsg;
      }
      for ( unsigned int area = 0; area < fun.size(); ++area ) {
        // transpose the parameters into the right order...
        std::vector<float> p;
        p.reserve( dim );
        int pos = 2 + area;
        for ( int i = 0; i < dim; ++i ) {
          p.push_back( params[pos] );
          pos += 3;
        }

        switch ( func ) {
        case FunctionType::Polynomial:
          if ( !p.empty() ) {
            fun[area] = Polynomial{{p.begin(), p.end()}};
          } else {
            error() << "Inconsistent # of parameters for " << func << endmsg;
          }
          break;
        case FunctionType::InversPolynomial:
          if ( !p.empty() ) {
            fun[area] = InversePolynomial{p};
          } else {
            error() << "Inconsistent # of parameters for " << func << endmsg;
          }
          break;
        case FunctionType::ExpPolynomial:
          if ( !p.empty() ) {
            fun[area] = ExpPolynomial{p};
          } else {
            error() << "Inconsistent # of parameters for " << func << endmsg;
          }
          break;
        case FunctionType::ReciprocalPolynomial:
          if ( !p.empty() ) {
            fun[area] = ReciprocalPolynomial{p};
          } else {
            error() << "Inconsistent # of parameters for " << func << endmsg;
          }
          break;
        case FunctionType::Sigmoid:
          if ( p.size() == 4 ) {
            fun[area] = Sigmoid{LHCb::make_span( p ).first<4>()};
          } else {
            error() << "Inconsistent # of parameters for sigmoid" << endmsg;
          }
          break;
        case FunctionType::Sshape:
          if ( p.size() == 1 ) {
            fun[area] = Sshape{p[0]};
          } else {
            error() << "Inconsistent # of parameters for Sshape" << endmsg;
          }
          break;
        case FunctionType::SshapeMod:
          if ( p.size() == 1 ) {
            fun[area] = SshapeMod{p[0]};
          } else {
            error() << "Inconsistent # of parameters for SshapeMod" << endmsg;
          }
          break;
        case FunctionType::ShowerProfile:
          if ( p.size() == 10 ) {
            fun[area] = ShowerProfile{LHCb::make_span( p ).first<10>()};
          } else {
            error() << "Inconsistent # of parameters for ShowerProfile" << endmsg;
          }
          break;
        case FunctionType::Sinusoidal:
          if ( p.size() == 1 ) {
            fun[area] = Sinusoidal{p[0]};
          } else {
            error() << "Inconsistent # of parameters for Sinusoidal" << endmsg;
          }
          break;
        case FunctionType::ParamList:
          assert( !p.empty() );
          fun[area] = ParamVector{{p.begin(), p.end()}};
          break;
        default:
          error() << "got unknown function" << endmsg;
          break;
        }
      }
    }
  }

  std::optional<Result> Base::getCorrectionAndDerivative( Parameters const& params, const Type type,
                                                          const LHCb::Detector::Calo::CellID id,
                                                          const float                        var ) const {
    return std::visit(
        Gaudi::overload( [&]( const auto& data ) -> std::optional<Result> { return data.correction( var ); },
                         []( const ParamVector& ) -> std::optional<Result> {
                           return std::nullopt; // FIXME: throw exception instead? (and drop the use of optional)
                         } ),
        params[static_cast<int>( type )][id.area()] );
  }

  std::optional<float> Base::getCorrection( Parameters const& params, const Type type,
                                            const LHCb::Detector::Calo::CellID id, const float var ) const {
    return std::visit(
        Gaudi::overload( [&]( const auto& data ) -> std::optional<float> { return data.correction( var ).value; },
                         []( const ParamVector& ) -> std::optional<float> {
                           return std::nullopt; // FIXME: throw exception instead? (and drop the use of optional)
                         } ),
        params[static_cast<int>( type )][id.area()] );
  }

} // namespace LHCb::Calo::Correction
