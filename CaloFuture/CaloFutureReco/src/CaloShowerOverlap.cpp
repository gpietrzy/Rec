/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CellID.h"
#include "ICaloShowerOverlapTool.h"
#include "LHCbAlgs/Transformer.h"
#include "SubClusterSelectorTool.h"
#include <vector>

/** @class CaloFutureShowerOverlap CaloFutureShowerOverlap.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2014-06-02
 */

namespace {
  auto neighbour_searcher( LHCb::Detector::Calo::CellID id, int distanceThreshold ) {
    return [=]( auto i, auto last ) {
      return std::find_if( i, last, [=]( const auto& e ) {
        auto id2 = e->cellID();
        return id.area() == id2.area() &&
               std::abs( static_cast<int>( id.col() ) - static_cast<int>( id2.col() ) ) <= distanceThreshold &&
               std::abs( static_cast<int>( id.row() ) - static_cast<int>( id2.row() ) ) <= distanceThreshold;
      } );
    };
  }
} // namespace

namespace LHCb::Calo {

  class ShowerOverlap final
      : public Algorithm::Transformer<Event::Calo::Clusters( Event::Calo::Clusters const&, DeCalorimeter const&,
                                                             DetectorElement const& ),
                                      DetDesc::usesConditions<DeCalorimeter, DetectorElement>> {

  public:
    /// Standard constructor
    ShowerOverlap( const std::string& name, ISvcLocator* pSvcLocator );

    ///< Algorithm execution
    Event::Calo::Clusters operator()( Event::Calo::Clusters const&, DeCalorimeter const&,
                                      DetectorElement const& ) const override;

  private:
    void correctForOverlap(
        SubClusterSelectorTool::Tags const& tags, DeCalorimeter const& calo,
        LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c1,
        LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c2,
        IGeometryInfo const& geometry ) const;

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_cluster_tagging_failed{
        this, "Cluster tagging failed - keep the initial 3x3 tagging"};
    mutable Gaudi::Accumulators::StatCounter<> m_negative_E{this, "negative E clusters (removed)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterDeltaX{this, "Delta(X)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterDeltaY{this, "Delta(Y)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterDeltaZ{this, "Delta(Z)"};
    mutable Gaudi::Accumulators::StatCounter<> m_counterDeltaE{this, "Delta(E)"};

    Gaudi::Property<int>   m_dMin{this, "DistanceThreshold", 4};
    Gaudi::Property<float> m_etMin{this, "MinEtThreshold", 50., "( ET1 > x && ET2 > x)"};
    Gaudi::Property<float> m_etMin2{this, "MaxEtThreshold", 150., "( ET2 > y || ET2 > y)"};
    Gaudi::Property<int>   m_iter{this, "Iterations", 5,
                                "Max iterations in ShowerOverlapTool. If negative, stop when tolerance is reached."};
    Gaudi::Property<bool>  m_applyLSCorr{this, "ApplyLSCorr", false};

    // following properties are inherited by the selector tool when defined :
    Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags"};
    Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags"};

    ToolHandle<Interfaces::IShowerOverlap> m_oTool{this, "OverlapTool",
                                                   "CaloFutureShowerOverlapTool/PhotonShowerOverlap"};
    ToolHandle<SubClusterSelectorTool>     m_tagger{this, "SubClusterSelector",
                                                "FutureSubClusterSelectorTool/EcalClusterTag"};
  };
  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( ShowerOverlap, "CaloFutureShowerOverlap" )

  //=============================================================================
  // Standard constructor, initializes variables
  //=============================================================================
  ShowerOverlap::ShowerOverlap( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{
            name,
            pSvcLocator,
            {KeyValue{"InputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalRaw" )},
             KeyValue{"Detector", LHCb::CaloFutureAlgUtils::DeCaloFutureLocation( name )},
             KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
            KeyValue{"OutputData", LHCb::CaloFutureAlgUtils::CaloFutureClusterLocation( name, "EcalOverlap" )}} {}

  //=============================================================================
  // Main execution
  //=============================================================================
  LHCb::Event::Calo::Clusters ShowerOverlap::operator()( LHCb::Event::Calo::Clusters const& clusters,
                                                         DeCalorimeter const&               det,
                                                         DetectorElement const&             geometry ) const {

    // sort clusters, as this algorithm depends on the ordering
    auto                        eT                   = LHCb::CaloDataFunctor::EnergyTransverse{&det};
    LHCb::Event::Calo::Clusters outputClusters       = clusters.sorted( eT );
    auto                        outputClustersScalar = outputClusters.scalar();

    // neglect overlap from/to low ET clusters
    auto last =
        std::partition_point( outputClustersScalar.begin(), outputClustersScalar.end(),
                              [&eT, m = std::min( m_etMin, m_etMin2 )]( const auto& c ) { return eT( &c ) > m; } );
    auto last2 =
        std::partition_point( outputClustersScalar.begin(), last,
                              [&eT, m = std::max( m_etMin, m_etMin2 )]( const auto& c ) { return eT( &c ) > m; } );

    // get tags from m_tagger, this is basically a local cache
    // of conditions so that we are not retrieving them n times
    auto const& tags = m_tagger->getTags();

    // loop over new clusters and correct them
    auto counterDeltaZ = m_counterDeltaZ.buffer();
    auto counterDeltaX = m_counterDeltaX.buffer();
    auto counterDeltaY = m_counterDeltaY.buffer();
    auto counterDeltaE = m_counterDeltaE.buffer();
    for ( auto i1 = outputClustersScalar.begin(); i1 != last2; ++i1 ) {
      auto find_neighbour = neighbour_searcher( i1->cellID(), m_dMin );
      for ( auto i2 = find_neighbour( std::next( i1 ), last ); i2 != last;
            i2      = find_neighbour( std::next( i2 ), last ) ) {
        auto position1 = i1->position();
        auto position2 = i2->position();
        auto e1        = i1->e();
        auto e2        = i2->e();
        correctForOverlap( tags, det, *i1, *i2, geometry );
        counterDeltaX += i1->position().x() + i2->position().x() - position1.x() - position2.x();
        counterDeltaY += i1->position().y() + i2->position().y() - position1.y() - position2.y();
        counterDeltaZ += i1->position().z() + i2->position().z() - position1.z() - position2.z();
        counterDeltaE += i1->e() + i2->e() - e1 - e2;
      }
    }

    // sanity check: rm negative E clusters
    outputClustersScalar.filterInPlace( []( const auto& cl ) { return ( cl->e() > 0 ); } );
    m_negative_E += clusters.size() - outputClusters.size();

    return outputClusters;
  }

  void ShowerOverlap::correctForOverlap(
      SubClusterSelectorTool::Tags const& tags, DeCalorimeter const& calo,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c1,
      LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c2,
      IGeometryInfo const& geometry ) const {
    // initial weights for shared cells
    for ( auto e1 : c1.entries() ) {
      auto entries2 = c2.entries();
      auto e2       = std::find_if( entries2.begin(), entries2.end(),
                              [id = e1.cellID()]( const auto& e2 ) { return e2.cellID() == id; } );
      if ( e2 != entries2.end() ) {
        const auto totE = c1.e() + c2.e();
        e1.setFraction( c1.e() / totE );
        ( *e2 ).setFraction( c2.e() / totE );
      }
    }

    // tag the cluster position to have correct corrections
    m_tagger->tagPosition( tags, calo, c1.cellID(), c1.entries() )
        .andThen( [&] { return m_tagger->tagPosition( tags, calo, c2.cellID(), c2.entries() ); } )
        .orElse( [&] { ++m_cluster_tagging_failed; } )
        .ignore();
    // correct entry weight for shower overlap (assuming EM cluster)
    m_oTool->process( calo, c1, c2, geometry, m_iter, Interfaces::IShowerOverlap::propagateInitialWeights{false},
                      Interfaces::IShowerOverlap::applyCorrections{m_applyLSCorr.value()} );
  }

} // namespace LHCb::Calo
