/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Property.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/GenericMatrixTypes.h"
#include "GaudiKernel/GenericVectorTypes.h"
#include "GaudiKernel/IAlgTool.h"
#include "GaudiKernel/StatusCode.h"
#include "GaudiKernel/SymmetricMatrixTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbMath/LHCbMath.h"
#include <cmath>

namespace LHCb::Calo {
  inline double round( const double value, const double step ) {
    const double _aux1 = value / step;
    const double _aux2 = std::floor( _aux1 );
    return ( _aux1 - _aux2 < 0.5 ) ? _aux2 * step : ( _aux2 + 1 ) * step;
  }

  class ClusterSpreadTool : public extends<GaudiTool, IAlgTool> {
  public:
    // Standard constructor
    using extends::extends;

    /** standard finalization method
     *  @return status code
     */
    StatusCode finalize() override;

    /** The main processing method (functor interface)
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     *
     *  Error codes:
     *   - 221 - invalid source of detector information
     *   - 222 - the seed cell was not found
     *   - 223 - strange combination
     *   - 224 - energy is not positive
     */

    template <typename ClusterType>
    [[gnu::always_inline]] inline StatusCode compute( const DeCalorimeter& calo, ClusterType c ) const {
      auto et = LHCb::CaloDataFunctor::EnergyTransverse{&calo};

      if ( c.size() == 0 ) {
        ++m_skipped_size;
        return StatusCode::FAILURE;
      }
      if ( c.e() <= 0 ) {
        ++m_skipped_energy;
        return StatusCode::FAILURE;
      }

      constexpr double x_prec  = 0.2 * Gaudi::Units::mm;
      constexpr double x_prec2 = x_prec * x_prec;
      constexpr double e_prec  = 0.2 * Gaudi::Units::MeV;

      Gaudi::SymMatrix2x2 cov{};
      Gaudi::Vector2      mean{0., 0.};
      double              etot = 0;

      unsigned int ncells = 0;

      std::optional<double>         cellsize;
      std::optional<Gaudi::Vector2> xy0;

      for ( const auto& entry : c.entries() ) {
        const double           fraction = entry.fraction();
        const double           weight   = entry.energy() * fraction;
        const Gaudi::XYZPoint& pos      = calo.cellCenter( entry.cellID() );

        if ( entry.status().test( LHCb::CaloDigitStatus::Mask::SeedCell ) ) {
          cellsize = calo.cellSize( entry.cellID() );
        }

        // use positive energy cells only
        if ( weight <= 0 ) continue;

        ++ncells;
        const Gaudi::Vector2 xy = {pos.x(), pos.y()};

        // accumulate the energy
        etot += weight;

        // the first good cell?
        if ( !xy0 ) {
          xy0 = xy; // adjust the bias
          continue; // can skip since everything propto dx and/or dy
        }

        const Gaudi::Vector2 dxy = xy - *xy0; // temporary shift

        mean += weight * dxy;

        cov( 0, 0 ) += dxy[0] * dxy[0] * weight;
        cov( 1, 0 ) += dxy[1] * dxy[0] * weight;
        cov( 1, 1 ) += dxy[1] * dxy[1] * weight;
      }

      // strange combinations
      if ( ncells <= 0 ) {
        ++m_skipped_ncells;
        return StatusCode::FAILURE;
      }
      // energy is not positive
      if ( etot <= 0 ) {
        ++m_skipped_etot;
        return StatusCode::FAILURE;
      }
      // seed cell was not found
      if ( !cellsize ) {
        ++m_skipped_seed;
        return StatusCode::FAILURE;
      }

      mean /= etot;
      cov /= etot;

      cov( 0, 0 ) -= mean[0] * mean[0];
      cov( 1, 0 ) -= mean[1] * mean[0];
      cov( 1, 1 ) -= mean[1] * mean[1];

      mean += *xy0; // shift back

      const double uniform = *cellsize * *cellsize / 12.;
      /// could do nothing else for 1 cell "clusters"
      if ( 1 == ncells ) {
        cov( 0, 0 ) = uniform;
        cov( 1, 1 ) = uniform;
        cov( 1, 0 ) = 0.0;
      } else {
        const double trace = cov( 0, 0 ) + cov( 1, 1 );
        const double diff  = cov( 0, 0 ) - cov( 1, 1 );
        const double disc  = std::sqrt( diff * diff + 4 * ( cov( 1, 0 ) * cov( 1, 0 ) ) );

        // eigen values:
        const double lambda1 = 0.5 * ( trace - disc );
        const double lambda2 = 0.5 * ( trace + disc );

        // minimal eigenvalue
        const double lambdaMin = std::min( lambda1, lambda2 ) / uniform;
        const double covMin    = std::min( cov( 0, 0 ), cov( 1, 1 ) ) / uniform;

        constexpr double       s_Cut   = 0.5;
        constexpr unsigned int s_Cells = 4;

        if ( s_Cells >= ncells ||  // small amout of cells wth e>0
             s_Cut >= lambdaMin || // small eigenvalue
             s_Cut >= covMin )     // small eigenvalue
        {
          // the matrix must be modified a bit:
          const double eT = round( et( &c ), e_prec );
          m_ratio += (float)lambdaMin;
          m_energy += (float)eT;
          m_cells += ncells;

          // construct the matrix of eigen vectors

          const double diff1 = cov( 0, 0 ) - lambda1;
          const double diff2 = cov( 0, 0 ) - lambda2;

          constexpr auto absLess = LHCb::Math::abs_less<double>{};

          const double phi =
              // the matrix is numerically diagonal? => trivial eigenvectors
              absLess( cov( 1, 0 ), x_prec2 ) ? 0.0 :
                                              // find "the best non-zero" eigenvector
                  absLess( diff1, diff2 ) ? std::atan2( cov( 1, 0 ), diff2 ) : std::atan2( diff1, -cov( 1, 0 ) );

          const double cphi = std::cos( phi );
          const double sphi = std::sin( phi );

          const double s2 = sphi * sphi;
          const double c2 = cphi * cphi;
          const double cs = cphi * sphi;

          const double newLambda1 = std::max( lambda1, uniform );
          const double newLambda2 = std::max( lambda2, uniform );

          // recalculate the matrix using new eigenvalues
          cov( 0, 0 ) = c2 * newLambda1 + s2 * newLambda2;
          cov( 1, 0 ) = cs * ( newLambda1 - newLambda2 );
          cov( 1, 1 ) = s2 * newLambda1 + c2 * newLambda2;
        }
      }

      // Note: the mean of the spread is NOT the cluster position!
      c.setCenter( mean );
      c.setSpread( cov );

      return StatusCode::SUCCESS;
    }

  private:
    mutable Gaudi::Accumulators::Counter<>          m_skipped_size{this, "# clusters skipped - cluster size"};
    mutable Gaudi::Accumulators::Counter<>          m_skipped_ncells{this, "# clusters skipped - no positive cells"};
    mutable Gaudi::Accumulators::Counter<>          m_skipped_energy{this, "# clusters skipped - negative energy"};
    mutable Gaudi::Accumulators::Counter<>          m_skipped_etot{this, "# clusters skipped - negative E tot"};
    mutable Gaudi::Accumulators::Counter<>          m_skipped_seed{this, "# clusters skipped - seed not found"};
    mutable Gaudi::Accumulators::StatCounter<int>   m_cells{this, "Corrected Clusters: # cells "};
    mutable Gaudi::Accumulators::StatCounter<float> m_ratio{this, "Corrected Clusters: size ratio"};
    mutable Gaudi::Accumulators::StatCounter<float> m_energy{this, "Corrected Clusters: ET"};
  };
} // namespace LHCb::Calo
