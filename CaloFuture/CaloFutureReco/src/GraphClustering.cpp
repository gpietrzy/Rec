/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CellNeighbour.h"
#include "CellSelector.h"
#include "Detector/Calo/CaloCellID.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloDigits_v2.h"
#include "Gaudi/Algorithm.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/SIMDWrapper.h"
#include "boost/container/flat_map.hpp"
#include "boost/container/small_vector.hpp"
#include "boost/dynamic_bitset.hpp"
#include <map>
#include <vector>

/** @class GraphClustering GraphClustering.h
 *
 *
 *  @author Núria Valls Canudas
 *  @date   2021-12-16
 */

namespace LHCb::Calo {
  namespace {

    using WeightedEdge = boost::container::flat_map<int, float>;
    using boost::container::small_vector;
    using simd = SIMDWrapper::best::types;

    template <typename Container>
    auto simd_find( const Container& vec, simd::int_v value ) {
      for ( size_t i{0}; i < vec.size(); i += simd::size ) {
        if ( const auto mask = ( simd::int_v{vec.data() + i} == value ); any( mask ) )
          return any( mask && simd::loop_mask( i, vec.size() ) );
      }
      return false;
    }
    /**
     * @brief Graph representing ECAL cells
     * @details The constant graph size of 6016 is given by the total number of readout cells from the ECAL.
     */
    class Graph {
      static constexpr size_t           m_size{6016};
      std::vector<small_vector<int, 8>> m_adj_u; // Pointer to an array containing adjacency vectors
      std::vector<WeightedEdge>         m_adj_weighted;
      std::vector<WeightedEdge>         m_adj_weighted_pred;
      void DFSUtil( int v, boost::dynamic_bitset<>& visited, LHCb::span<std::vector<int>> conComp,
                    int index ) const; // A function used by DFS
    public:
      explicit Graph() : m_adj_u( m_size ), m_adj_weighted( m_size ), m_adj_weighted_pred( m_size ) {}
      void addEdge( int v, int w, float weight );
      auto connectedComponents() const;
      void calculateWeights( LHCb::span<const int> nodes, const LHCb::Event::Calo::Digits& digits );

      int                 outEdgesSize( int node ) const { return m_adj_weighted[node].size(); }
      int                 inEdgesSize( int node ) const { return m_adj_weighted_pred[node].size(); }
      size_t              size() const { return m_size; }
      WeightedEdge const& adjWeightedPred( int index ) const { return m_adj_weighted_pred[index]; }
      WeightedEdge const& adjWeighted( int index ) const { return m_adj_weighted[index]; }
    };

    void Graph::addEdge( int v, int w, float weight ) {
      m_adj_u[v].push_back( w );
      m_adj_u[w].push_back( v );
      m_adj_weighted[v][w]      = weight;
      m_adj_weighted_pred[w][v] = weight;
    }

    // Method to get connected components in an undirected graph
    auto Graph::connectedComponents() const {
      std::vector<std::vector<int>> conComp( m_size );
      boost::dynamic_bitset<>       visited( m_size ); // Mark all the vertices as not visited
      for ( size_t v = 0; v < m_size; v++ ) {
        if ( !visited[v] && !m_adj_u[v].empty() ) {
          DFSUtil( v, visited, conComp, v ); // Look all reachable vertices from v
        }
      }
      return conComp;
    }

    void Graph::DFSUtil( int v, boost::dynamic_bitset<>& visited, LHCb::span<std::vector<int>> conComp,
                         int index ) const {
      visited[v] = true; // Mark the current node as visited and print it
      conComp[index].emplace_back( v );
      // Recur for all the vertices adjacent to this vertex
      for ( auto const& i : m_adj_u[v] ) {
        if ( !visited[i] ) { DFSUtil( i, visited, conComp, index ); }
      }
    }

    void Graph::calculateWeights( LHCb::span<const int> nodes, const LHCb::Event::Calo::Digits& digits ) {
      std::map<int, float> seedToClEnergy;
      for ( auto n : nodes ) {
        const auto& edges = m_adj_weighted[n];
        if ( edges.size() < 2 ) continue;
        auto totalEnergy{0.f};
        for ( auto ed : edges ) {
          const auto e = ed.first;
          if ( const auto energy_e = seedToClEnergy.find( e ); energy_e != seedToClEnergy.end() ) {
            totalEnergy += energy_e->second;
            continue;
          }
          auto        cell  = digits.find( Detector::Calo::DenseIndex::details::toCellID( e ) );
          const auto& cells = m_adj_weighted_pred[e];
          float       energy =
              std::accumulate( cells.begin(), cells.end(), cell ? cell->energy() / edges.size() : 0.f,
                               [&]( float energy, const auto& te ) {
                                 auto cell = digits.find( Detector::Calo::DenseIndex::details::toCellID( te.first ) );
                                 return cell ? energy + cell->energy() : energy;
                               } );
          totalEnergy += energy;
          [[maybe_unused]] auto r = seedToClEnergy.emplace( e, energy );
          assert( r.second );
        }
        for ( auto ed : edges ) {
          const auto e              = ed.first;
          auto       weight         = seedToClEnergy.at( e ) / totalEnergy;
          m_adj_weighted[n][e]      = weight;
          m_adj_weighted_pred[e][n] = weight;
        }
      }
    }

    constexpr auto usedForE =
        CaloDigitStatus::Status{CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForCovariance};
    constexpr auto usedForP =
        CaloDigitStatus::Status{CaloDigitStatus::Mask::UseForPosition, CaloDigitStatus::Mask::UseForCovariance};
    constexpr auto seed =
        ( usedForP | usedForE | CaloDigitStatus::Mask::SeedCell | CaloDigitStatus::Mask::LocalMaximum );
  } // namespace

  class GraphClustering
      : public Algorithm::Transformer<LHCb::Event::Calo::Clusters( const EventContext&, const DeCalorimeter&,
                                                                   const LHCb::Event::Calo::Digits& ),
                                      LHCb::DetDesc::usesConditions<DeCalorimeter>> {

  public:
    GraphClustering( const std::string& name, ISvcLocator* pSvcLocator ); /// Standard constructor
    StatusCode                  finalize() override;                      ///< Algorithm finalization
    LHCb::Event::Calo::Clusters operator()( const EventContext& evtCtx, const DeCalorimeter&,
                                            const LHCb::Event::Calo::Digits& ) const override; ///< Algorithm execution

  private:
    Gaudi::Property<float>                  m_threshold{this, "EnergyThreshold", 50};
    Gaudi::Property<float>                  m_pi0_min_seed_e{this, "MinPi0SeedEnergyThreshold", 1000};
    Gaudi::Property<float>                  m_pi0_min_f1{this, "MinPi0F1Threshold", 25};
    Gaudi::Property<CellSelector::Selector> m_usedE{this, "CellSelectorForEnergy", CellSelector::Selector::s3x3};
    Gaudi::Property<CellSelector::Selector> m_usedP{this, "CellSelectorForPosition", CellSelector::Selector::s3x3};

    mutable Gaudi::Accumulators::StatCounter<> m_clusters{this, "# clusters"};
    mutable Gaudi::Accumulators::StatCounter<> m_negative{this, "Negative energy clusters"};
    mutable Gaudi::Accumulators::StatCounter<> m_clusterEnergy{this, "Cluster energy"};
    mutable Gaudi::Accumulators::StatCounter<> m_clusterSize{this, "Cluster size"};
  };

  // ============================================================================
  // Declaration of the Algorithm Factory
  // ============================================================================
  DECLARE_COMPONENT_WITH_ID( GraphClustering, "GraphClustering" )

  // ============================================================================
  // Standard constructor, initializes variables
  // ============================================================================
  GraphClustering::GraphClustering( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"Detector", Utilities::DeCaloFutureLocation( name )},
                      KeyValue{"InputData", Utilities::CaloFutureDigitLocation( name )}},
                     KeyValue{"OutputData", Utilities::CaloFutureClusterLocation( name, "EcalRaw" )} ) {}

  // ============================================================================
  //  Finalize
  // ============================================================================
  StatusCode GraphClustering::finalize() {
    info() << "Built <" << m_clusters.mean() << "> graph calo clustering clusters/event " << endmsg;
    return Transformer::finalize(); // must be called after all other actions
  }

  // ============================================================================
  // Main execution
  // ============================================================================
  Event::Calo::Clusters GraphClustering::operator()( const EventContext& evtCtx, const DeCalorimeter& detector,
                                                     const LHCb::Event::Calo::Digits& digits ) const ///< Algorithm
                                                                                                     ///< execution
  {
    std::vector<std::pair<float, LHCb::Detector::Calo::CellID>> sortedDigits;
    sortedDigits.reserve( digits.size() );
    std::vector<int> mergedPi0;
    mergedPi0.reserve( 1024 );

    /// Sort the digits by energy
    for ( auto digit : digits ) {
      if ( digit.energy() > m_threshold.value() ) sortedDigits.emplace_back( digit.energy(), digit.cellID() );
    }
    std::sort( sortedDigits.begin(), sortedDigits.end(), std::greater<>{} );

    /// Graph declaration and sorted digit insertion under rules
    Graph G{};
    for ( auto [energy, id] : sortedDigits ) {
      if ( const auto dense_id = denseIndex( id ); G.outEdgesSize( dense_id ) == 0 ) {
        auto& neighbors  = detector.neighborCells( id );
        bool  isLocalMax = std::none_of( begin( neighbors ), end( neighbors ), [&digits, energy = energy]( auto& n ) {
          auto cell = digits.find( n );
          return cell && ( energy < cell->energy() );
        } );

        if ( isLocalMax ) {
          for ( auto n : neighbors ) {
            auto cell = digits.find( n );
            if ( !cell ) continue;
            G.addEdge( denseIndex( n ), dense_id, 1.f );
            if ( energy > m_pi0_min_seed_e.value() && ( cell->energy() * 100 / energy ) > m_pi0_min_f1.value() ) {
              mergedPi0.emplace_back( dense_id );
              mergedPi0.emplace_back( denseIndex( n ) );
            }
          }
        }
      } else if ( simd_find( mergedPi0, simd::int_v{dense_id} ) ) {
        const auto seed = G.adjWeighted( dense_id ).begin()->first;
        for ( auto n : detector.neighborCells( id ) ) {
          auto cell = digits.find( n );
          if ( cell && energy > cell->energy() && G.outEdgesSize( denseIndex( cell->cellID() ) ) == 0 ) {
            G.addEdge( denseIndex( n ), seed, 1.f );
          }
        }
      }
    }

    Event::Calo::Clusters clustersGraph{Zipping::generateZipIdentifier(), LHCb::getMemResource( evtCtx )};
    clustersGraph.reserve( 2100 );
    auto clusterEnergy_counter = m_clusterEnergy.buffer();
    auto clusterSize_counter   = m_clusterSize.buffer();
    int  nNegative             = 0;
    auto useForE               = CellSelector{&detector, m_usedE.value()};
    auto useForP               = CellSelector{&detector, m_usedP.value()};

    /// Get the connected components:
    auto conComp = G.connectedComponents();

    /// Iterate to all the connected components:
    for ( size_t c = 0; c < G.size(); c++ ) {
      if ( conComp[c].size() > 1 ) {
        G.calculateWeights( conComp[c], digits );

        for ( auto n : conComp[c] ) {
          auto nOut = G.outEdgesSize( n );
          auto nIn  = G.inEdgesSize( n );

          if ( nIn > 1 && nOut == 0 ) {

            auto cluster = clustersGraph.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

            auto cellid = Detector::Calo::DenseIndex::details::toCellID( n );
            auto cell   = digits.find( cellid );

            if ( cell ) {
              auto entry = cluster.entries().emplace_back();
              entry.setCellID( cellid );
              entry.setEnergy( cell->energy() );
              entry.setFraction( 1.f );
              entry.setStatus( seed );
            }

            for ( auto const& e : G.adjWeightedPred( n ) ) {
              auto cell_ = digits.find( Detector::Calo::DenseIndex::details::toCellID( e.first ) );
              LHCb::CaloDigitStatus::Status status = LHCb::CaloDigitStatus::Mask::OwnedCell;
              if ( useForE( cellid, Detector::Calo::DenseIndex::details::toCellID( e.first ) ) > 0 ) status |= usedForE;
              if ( useForP( cellid, Detector::Calo::DenseIndex::details::toCellID( e.first ) ) > 0 ) status |= usedForP;
              auto entry = cluster.entries().emplace_back();
              entry.setCellID( Detector::Calo::DenseIndex::details::toCellID( e.first ) );
              entry.setEnergy( cell_->energy() );
              entry.setFraction( G.adjWeighted( e.first ).at( n ) );
              entry.setStatus( status );
            }

            auto exy = LHCb::CaloDataFunctor::calculateClusterEXY( cluster.entries(), &detector );
            if ( exy ) { //  put cluster to the output
              if ( exy->Etot <= 0 || exy->Epos <= 0 ) {
                ++nNegative;
                clustersGraph.resize( clustersGraph.size() - 1 );
                continue; // skip negative E clusters
              }

              cluster.setCellID( cellid );
              cluster.setType( Event::Calo::Clusters::Type::CellularAutomaton );
              cluster.setEnergy( exy->Etot );
              cluster.setPosition( {exy->x, exy->y, (float)detector.cellCenter( cellid ).z()} );

              clusterEnergy_counter += cluster.energy();
              clusterSize_counter += cluster.size();
            } else {
              clustersGraph.resize( clustersGraph.size() - 1 );
            }
          }
        }
      }
    }
    if ( nNegative > 0 ) m_negative += nNegative;
    m_clusters += clustersGraph.size();

    return clustersGraph;
  }
} // namespace LHCb::Calo
