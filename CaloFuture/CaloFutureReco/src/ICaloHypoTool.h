/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "CaloFutureUtils/CaloFuture2Track.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"

#include "Event/CaloClusters_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/PrimaryVertices.h"
#include "GaudiKernel/IAlgTool.h"

#include <functional>

#include "DetDesc/IGeometryInfo.h"

/**
 *  The generic interface for "CalorimeterFuture tools" , which deals with
 *  CaloClusters objects, which depend on the assumed hypothesis. Possible examples include
 *
 *    \li hypothesis processing
 *    \li dispatching
 *    \li subcomponent of CaloFutureParticle processing
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   30/10/2001
 */
namespace LHCb::Calo::Interfaces {
  // Declare the PV container used in CaloReco
  using PrimaryVertices = LHCb::Event::PV::PrimaryVertexContainer;

  struct IProcessHypos : extend_interfaces<IAlgTool> {
    /** static interface identification
     *  @see IInterface
     *  @return unique interface identifier
     */
    DeclareInterfaceID( IProcessHypos, 2, 0 );

    /** The main processing method
     *  @param  hypo   type of hypothesis to use for corrections
     *  @param  hypos  range of clusters to be processed
     *  @return status code
     */

    virtual StatusCode
    process( const DeCalorimeter&, Event::Calo::Hypotheses::Type,
             Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>&,
             IGeometryInfo const& geometry ) const = 0;

    /** process function patch -- with two additional ctable and vertex parameters that are used in the ECorrection
     *  @param  hypo   type of hypothesis to use for corrections
     *  @param  ctable to associate Clusters to Tracks
     *  @return status code
     */

    virtual StatusCode
    process( const DeCalorimeter& calo, Event::Calo::Hypotheses::Type hypo,
             Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>& range,
             CaloFuture2Track::ICluster2TrackTable2D const*, IGeometryInfo const& geometry,
             const PrimaryVertices& ) const {
      return process( calo, hypo, range, geometry );
    };

    StatusCode process( const DeCalorimeter& calo, LHCb::Event::Calo::Hypotheses::Type hypo,
                        LHCb::Event::Calo::Clusters& clusters, IGeometryInfo const& geometry ) const {
      for ( auto&& cluster : clusters.scalar() ) {
        if ( process( calo, hypo, cluster, geometry ) != StatusCode::SUCCESS ) return StatusCode::FAILURE;
      }
      return StatusCode::SUCCESS;
    }

    StatusCode process( const DeCalorimeter& calo, LHCb::Event::Calo::Hypotheses::Type hypo,
                        LHCb::Event::Calo::Clusters& clusters, CaloFuture2Track::ICluster2TrackTable2D const* ctable,
                        IGeometryInfo const& geometry, const PrimaryVertices& vertLoc ) const {
      for ( auto&& cluster : clusters.scalar() ) {
        if ( process( calo, hypo, cluster, ctable, geometry, vertLoc ) != StatusCode::SUCCESS )
          return StatusCode::FAILURE;
      }
      return StatusCode::SUCCESS;
    }
  };
} // namespace LHCb::Calo::Interfaces
