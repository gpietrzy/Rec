/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/CaloClusters_v2.h"

#include "GaudiKernel/IAlgTool.h"
#include "Kernel/TaggedBool.h"

#include "DetDesc/IGeometryInfo.h"

/**
 *  @author Olivier Deschamps
 *  @date   2014-06-03
 */
namespace LHCb::Calo::Interfaces {

  struct IShowerOverlap : extend_interfaces<IAlgTool> {
    // Return the interface ID
    DeclareInterfaceID( IShowerOverlap, 2, 0 );

    using propagateInitialWeights = LHCb::tagged_bool<struct propagateInitialWeights_tag>;

    // apply or not S and L corrections
    using applyCorrections = LHCb::tagged_bool<struct applyCorrections_tag>;

    virtual void
    process( const DeCalorimeter&                                                                              calo,
             LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c1,
             LHCb::Event::Calo::Clusters::reference<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous> c2,
             IGeometryInfo const& geometry, int niter, propagateInitialWeights, applyCorrections ) const = 0;
  };

} // namespace LHCb::Calo::Interfaces
