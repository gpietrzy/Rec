/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CellMatrix3x3.h"
#include "SubClusterSelectorBase.h"

// ============================================================================
/** @file FutureSubClusterSelector3x3.cpp
 *
 *  Implementation file for class : FutureSubClusterSelector3x3
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 07/11/2001
 */
// ============================================================================

/** @class FutureSubClusterSelector3x3 FutureSubClusterSelector3x3.h
 *
 *  The simplest concrete "subcluster selector" -
 *  it just tag/select digits which form
 *  3x3 sub-matrix centered with the seed cells
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date   07/11/2001
 */

namespace LHCb::Calo {
  class SubClusterSelector3x3 : public SubClusterSelectorBase {
  public:
    /** The main processing method
     *
     *  @see ICaloSubClusterTag
     *
     *  Error codes:
     *    - 225   cluster points to NULL
     *    - 226   empty cell/digit container for given cluster
     *    - 227   SeedCell is not found
     *    - 228   Seed points to NULL
     *
     *  @see ICaloClusterTool
     *  @see ICaloSubClusterTag
     *
     *  @param cluster pointer to CaloCluster object to be processed
     *  @return status code
     */
    StatusCode
    tag( const DeCalorimeter& calo,
         Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
             entries ) const override {
      return Functor::Cluster::tagTheSubCluster( entries, CellMatrix3x3{&calo}, modify(), mask(),
                                                 DigitStatus::Mask::ModifiedBy3x3Tagger );
    }

    /** The main method - "undo" of tagging from "tag" method
     *  @see ICaloSubClusterTag
     *  @param cluster pointer to ClaoCluster object to be untagged
     *  @return status code
     */
    StatusCode
    untag( const DeCalorimeter& calo,
           Event::Calo::Clusters::Entries<SIMDWrapper::InstructionSet::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous>
               entries ) const override {
      return Functor::Cluster::untagTheSubCluster( entries, CellMatrix3x3{&calo},
                                                   DigitStatus::Mask::ModifiedBy3x3Tagger );
    }

    /** Standard Tool Constructor
     *  @param type type of the tool (useless ? )
     *  @param name name of the tool
     *  @param parent the tool parent
     */
    using SubClusterSelectorBase::SubClusterSelectorBase;
  };

  DECLARE_COMPONENT_WITH_ID( SubClusterSelector3x3, "SubClusterSelector3x3" )

} // namespace LHCb::Calo
// ============================================================================
// The End
// ============================================================================
