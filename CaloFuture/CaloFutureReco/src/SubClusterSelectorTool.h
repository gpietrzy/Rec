/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
// from Gaudi
#include "CaloCorrectionBase.h"
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloClusters_v2.h"
#include "GaudiAlg/GaudiTool.h"
#include "ICaloSubClusterTag.h"

namespace LHCb::Calo {

  namespace CaloFutureClusterMask {
    enum Mask {
      area3x3    = 0, // MUST BE 0 FOR BACKWARD COMPATIBILITY
      area2x2    = 1,
      SwissCross = 2,
      Last
    };
    constexpr int            nMask           = Last + 1;
    inline const std::string maskName[nMask] = {"3x3", "2x2", "SwissCross", "Unknown"};
  } // namespace CaloFutureClusterMask

  static const InterfaceID IID_SubClusterSelectorTool( "SubClusterSelectorTool", 1, 0 );

  /** @class SubClusterSelectorTool SubClusterSelectorTool.h
   *
   *
   *  @author Olivier Deschamps
   *  @date   2014-06-20
   */

  class SubClusterSelectorTool : public LHCb::DetDesc::ConditionAccessorHolder<GaudiTool> {
  public:
    using Tags = std::array<std::vector<Interfaces::ISubClusterTag*>, 2>; // 0 -> Energy, 1 -> Position

    // Return the interface ID
    static const InterfaceID& interfaceID() { return IID_SubClusterSelectorTool; }

    /// Standard constructor
    SubClusterSelectorTool( const std::string& type, const std::string& name, const IInterface* parent );

    StatusCode initialize() override;

    Tags const& getTags() const { return m_tags.get(); }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    StatusCode tagEnergy( Tags const& tags, DeCalorimeter const& calo, LHCb::Detector::Calo::CellID id,
                          LHCb::Event::Calo::Clusters::Entries<simd, behaviour> entries ) const {
      // get the tagger
      auto* tagger = ( id.area() < tags[0].size() ) ? tags[0][id.area()] : nullptr;
      if ( !tagger ) {
        ++m_no_tagger;
        return StatusCode::FAILURE;
      }
      return tagger->tag( calo, entries );
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    StatusCode tagPosition( Tags const& tags, DeCalorimeter const& calo, LHCb::Detector::Calo::CellID id,
                            LHCb::Event::Calo::Clusters::Entries<simd, behaviour> entries ) const {
      // get the tagger
      auto* tagger = ( id.area() < tags[1].size() ) ? tags[1][id.area()] : nullptr;
      if ( !tagger ) {
        ++m_no_tagger;
        return StatusCode::FAILURE;
      }
      return tagger->tag( calo, entries );
    }

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
    StatusCode tag( Tags const& tags, DeCalorimeter const& calo, LHCb::Detector::Calo::CellID id,
                    LHCb::Event::Calo::Clusters::Entries<simd, behaviour> entries ) const {
      return tagEnergy( tags, calo, id, entries ).andThen( [&] { return tagPosition( tags, calo, id, entries ); } );
    }

  private:
    Gaudi::Property<std::vector<std::string>> m_taggerE{this, "EnergyTags", {}};
    Gaudi::Property<std::vector<std::string>> m_taggerP{this, "PositionTags", {}};
    ConditionAccessor<Tags>                   m_tags{this, name() + "-SubClusterSelectorToolTags"};

    Tags getParams( Correction::Parameters const& );

    // associate known cluster mask to tagger tool
    Gaudi::Property<std::map<std::string, std::string>> m_clusterTaggers{
        this,
        "ClusterTaggers",
        {
            {"", "useDB"},
            {"useDB", "useDB"},
            {"3x3", "SubClusterSelector3x3"},
            {"2x2", "SubClusterSelector2x2"},
            {"SwissCross", "SubClusterSelectorSwissCross"},
        },
        "associate known cluster mask to tagger tool"};

    Gaudi::Property<std::string> m_condition{this, "ConditionName",
#ifdef USE_DD4HEP
                                             "/world/DownstreamRegion/Ecal:EcalClusterMasks"
#else
                                             "Conditions/Reco/Calo/EcalClusterMasks"
#endif
    };
    ToolHandle<Correction::Base>  m_dbAccessor{this, "CaloFutureCorrectionBase", "CaloFutureCorrectionBase/DBAccessor"};
    LHCb::CaloDigitStatus::Status m_energyStatus = LHCb::CaloDigitStatus::Status{
        LHCb::CaloDigitStatus::Mask::UseForEnergy, LHCb::CaloDigitStatus::Mask::UseForCovariance};
    LHCb::CaloDigitStatus::Status m_positionStatus = LHCb::CaloDigitStatus::Status{
        LHCb::CaloDigitStatus::Mask::UseForPosition, LHCb::CaloDigitStatus::Mask::UseForCovariance};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_tagger{this, "Tagger not found"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_energy_update_failed{this,
                                                                                 "Cannot update energy mask from DB"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_position_update_failed{
        this, "Cannot update position mask from DB"};
  };
} // namespace LHCb::Calo
