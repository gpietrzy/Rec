###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
CaloFuture/CaloFutureTools
--------------------------
#]=======================================================================]

gaudi_add_module(CaloFutureTools
    SOURCES
        src/CaloFuture2CaloFuture.cpp
        src/CaloFuture2MCTool.cpp
        src/CaloFutureElectron.cpp
        src/CaloFutureHypo2CaloFuture.cpp
        src/CaloFutureHypoEstimator.cpp
        src/ChargedPIDsConverter.cpp
        src/FutureGammaPi0SeparationTool.cpp
        src/FutureGammaPi0XGBoostTool.cpp
        src/FutureNeutralIDTool.cpp
    LINK
        Boost::headers
        fmt::fmt
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloDetLib
        LHCb::CaloFutureInterfaces
        LHCb::CaloFutureUtils
        LHCb::DigiEvent
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
        Rec::TrackInterfacesLib
        Rec::TrackKernel
        ROOT::GenVector
        XGBoost::XGBoost
)
