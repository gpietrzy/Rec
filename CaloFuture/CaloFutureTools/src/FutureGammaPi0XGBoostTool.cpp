/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/CaloMomentum.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloHypo.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ServiceHandle.h"
#include "IGammaPi0SeparationTool.h"
#include "Kernel/STLExtensions.h"
#include "fmt/format.h"
#include "xgboost/c_api.h"
#include <cassert>
#include <cstdlib>
#include <mutex>
#include <stdexcept>
#include <string>

//-----------------------------------------------------------------------------
// Implementation file for class : FutureGammaPi0XGBoostTool
//
// 2018-03-24 : author @sayankotor
//-----------------------------------------------------------------------------
namespace {

  size_t getClusterType( const LHCb::Detector::Calo::CellID& id ) {

    constexpr auto     CaloFutureNCol = std::array{64, 32, 16, 16};
    constexpr auto     CaloFutureNRow = std::array{52, 20, 12, 12};
    constexpr auto     Granularity    = std::array{1, 2, 3};
    constexpr unsigned ClusterSize    = 5;

    int type( id.area() );
    int xOffsetOut  = std::min( int( id.col() - ( 32 - CaloFutureNCol[type] * Granularity[type] / 2 ) ), // left edge
                               int( 31 + CaloFutureNCol[type] * Granularity[type] / 2 - id.col() ) );   // right edge
    int yOffsetOut  = std::min( int( id.row() - ( 32 - CaloFutureNRow[type] * Granularity[type] / 2 ) ),
                               int( 31 + CaloFutureNRow[type] * Granularity[type] / 2 - id.row() ) );
    int innerWidth  = CaloFutureNCol[type + 1] * ( type != 2 ? Granularity[type] : 1 ); // process inner hole specially
    int innerHeight = CaloFutureNRow[type + 1] * ( type != 2 ? Granularity[type] : 1 ); // process inner hole specially

    int xOffsetIn = std::min( int( id.col() - ( 31 - innerWidth / 2 ) ), int( 32 + innerWidth / 2 - id.col() ) );
    int yOffsetIn = std::min( int( id.row() - ( 31 - innerHeight / 2 ) ), int( 32 + innerHeight / 2 - id.row() ) );
    constexpr int margin      = ( ClusterSize - 1 ) / 2;
    bool          outerBorder = xOffsetOut < margin || yOffsetOut < margin;
    bool          innerBorder = xOffsetIn > -margin && yOffsetIn > -margin;
    return innerBorder ? type + 3 : outerBorder ? type + 6 : type;
  }

  std::array<float, 25> getDigitEnergies( int cluster_type, LHCb::Detector::Calo::CellID centerID,
                                          const LHCb::Event::Calo::Digits& digits ) {

    std::array<float, 25> rawEnergyVector{0};
    auto                  rawEnergy = [&rawEnergyVector]( int i, int j ) -> float& {
      return rawEnergyVector[( i + 2 ) * 5 + ( j + 2 )];
    };

    constexpr auto offsets = std::array{-2, -1, 0, 1, 2};
    if ( cluster_type > 2 ) {
      for ( auto col_number : offsets ) {
        for ( auto row_number : offsets ) {
          const auto id = LHCb::Detector::Calo::CellID{centerID.calo(), centerID.area(), centerID.row() + row_number,
                                                       centerID.col() + col_number};
          if ( !isValid( id ) ) continue;
          if ( auto digit = digits( id ); digit ) rawEnergy( col_number, row_number ) = digit->energy();
        }
      }
    } else {
      for ( auto col_number : offsets ) {
        for ( auto row_number : offsets ) {
          const auto id = LHCb::Detector::Calo::CellID{centerID.calo(), centerID.area(), centerID.row() + row_number,
                                                       centerID.col() + col_number};
          if ( !isValid( id ) ) continue;
          if ( auto digit = digits( id ); digit ) rawEnergy( col_number, row_number ) = digit->energy();
        }
      }
    }
    return rawEnergyVector;
  }

  class XGBClassifier final {
    mutable std::mutex           m_mut;
    mutable std::array<float, 2> m_predictionsCache = {0, 0};
    mutable DMatrixHandle        m_cache_matrix, m_feature_matrix;
    mutable BoosterHandle        m_booster;

  public:
    XGBClassifier( LHCb::span<std::byte const> buffer ) {
      XGDMatrixCreateFromMat( m_predictionsCache.data(), 1, m_predictionsCache.size(), 0.5, &m_cache_matrix );
      XGBoosterCreate( &m_cache_matrix, 1, &m_booster );
      if ( XGBoosterLoadModelFromBuffer( m_booster, buffer.data(), buffer.size() ) != 0 )
        throw std::runtime_error( "Failed to load XGBModel from buffer" );
    }
    double operator()( LHCb::span<const float> features ) const {
      auto lock = std::lock_guard{m_mut};
      XGDMatrixCreateFromMat( features.data(), 1, features.size(), 0, &m_feature_matrix );
      unsigned long predictions_length = 0;
      const float*  predictions        = nullptr;
      XGBoosterPredict( m_booster, m_feature_matrix, 0, 0, &predictions_length, &predictions );
      assert( predictions != nullptr );
      assert( predictions_length > 0 );
      XGDMatrixFree( m_feature_matrix );
      return predictions[0];
    }
  };

} // namespace

/** @class FutureGammaPi0XGBoostTool FutureGammaPi0XGBoostTool.h
 *
 *
 *  @author @sayankotor
 *  @date   2018-03-24
 */
namespace LHCb::Calo {
  class GammaPi0XGBoost final : public extends<GaudiTool, Interfaces::IGammaPi0Separation> {

  public:
    using extends::extends;

    StatusCode initialize() override {
      using namespace std::literals;
      return extends::initialize().andThen( [&] {
        m_file.retrieve().ignore();
        constexpr auto names = std::array{"simpl0"sv,  "simpl1"sv,  "simpl2"sv,  "bound03"sv, "bound14"sv,
                                          "bound25"sv, "bound06"sv, "bound17"sv, "bound28"sv};
        for ( auto&& [i, xgb] : range::enumerate( m_xgb ) ) {
          auto fname = fmt::format( "paramfile://data/GammaPi0XgbTool_{}.model", names[i] );
          if ( msgLevel( MSG::VERBOSE ) ) { verbose() << "reading XGB model from " << fname << endmsg; }
          auto buffer = m_file->read( fname );
          if ( !buffer ) {
            error() << "Could not open " << fname << endmsg;
            return StatusCode::FAILURE;
          }
          xgb.emplace( as_bytes( LHCb::make_span( buffer->data(), buffer->size() ) ) );
        }
        return StatusCode::SUCCESS;
      } );
    }

    std::optional<Observables> observables( const CaloHypo&, const LHCb::Event::Calo::v2::Digits& ) const override {
      return {};
    }
    std::optional<double> isPhoton( Observables const& ) const override { return {}; };
    std::optional<double> isPhoton( const CaloHypo& hypo, const LHCb::Event::Calo::v2::Digits& ) const override {
      if ( Momentum( &hypo ).pt() < m_minPt ) return std::nullopt;
      const CaloCluster* cluster =
          CaloFutureAlgUtils::ClusterFromHypo( hypo ); // OD 2014/05 - change to Split Or Main  cluster
      return cluster ? isPhoton( cluster->seed() ) : std::nullopt;
    }

  private:
    std::optional<double> isPhoton( Detector::Calo::CellID id ) const {
      auto cluster_type = getClusterType( id );
      if ( cluster_type > m_xgb.size() ) {
        ++m_bad_type;
        return std::nullopt;
      }
      return ( *m_xgb[cluster_type] )( getDigitEnergies( cluster_type, id, *m_digits.get() ) );
    }

    Gaudi::Property<float>                          m_minPt{this, "MinPt", 2000.};
    DataObjectReadHandle<LHCb::Event::Calo::Digits> m_digits{this, "DigitLocation", CaloDigitLocation::Ecal};
    ServiceHandle<IFileAccess> m_file{this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents"};
    std::array<std::optional<XGBClassifier>, 9>           m_xgb;
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_bad_type{this, "Unsupperted cluster type"};
  };

  DECLARE_COMPONENT_WITH_ID( GammaPi0XGBoost, "FutureGammaPi0XGBoostTool" )

} // namespace LHCb::Calo
