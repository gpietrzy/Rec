/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Class: ReadMLPMiddle

/* configuration options =====================================================

#GEN -*-*-*-*-*-*-*-*-*-*-*- general info -*-*-*-*-*-*-*-*-*-*-*-

Method         : MLP::MLP
TMVA Release   : 4.2.1         [262657]
ROOT Release   : 6.24/00       [399360]
Creator        : calvom
Date           : Fri Jun 18 16:01:59 2021
Host           : Linux centos7-docker 4.18.0-193.19.1.el8_2.x86_64 #1 SMP Mon Sep 14 14:37:00 UTC 2020 x86_64 x86_64
x86_64 GNU/Linux Training events: 29900 Analysis type  : [Classification]


#OPT -*-*-*-*-*-*-*-*-*-*-*-*- options -*-*-*-*-*-*-*-*-*-*-*-*-

# Set by User:
NCycles: "600" [Number of training cycles]
HiddenLayers: "N" [Specification of hidden layer architecture]
NeuronType: "tanh" [Neuron activation function type]
V: "False" [Verbose output (short form of "VerbosityLevel" below - overrides the latter one)]
VarTransform: "N" [List of variable transformations performed before training, e.g.,
"D_Background,P_Signal,G,N_AllClasses" for: "Decorrelation, PCA-transformation, Gaussianisation, Normalisation, each for
the given class of events ('Al$ H: "True" [Print method-specific help message] TestRate: "5" [Test for overtraining
performed at each #th epochs] UseRegulator: "False" [Use regulator to avoid over-training] # Default: RandomSeed: "1"
[Random seed for initial synapse weights (0 means unique seed for each run; default value '1')] EstimatorType: "CE" [MSE
(Mean Square Estimator) for Gaussian Likelihood or CE(Cross-Entropy) for Bernoulli Likelihood] NeuronInputType: "sum"
[Neuron input function type] VerbosityLevel: "Default" [Verbosity level] CreateMVAPdfs: "False" [Create PDFs for
classifier outputs (signal and background)] IgnoreNegWeightsInTraining: "False" [Events with negative weights are
ignored in the training (but are included for testing and performance evaluation)] TrainingMethod: "BP" [Train with
Back-Propagation (BP), BFGS Algorithm (BFGS), or Genetic Algorithm (GA - slower and worse)] LearningRate: "2.000000e-02"
[ANN learning rate parameter] DecayRate: "1.000000e-02" [Decay rate for learning parameter] EpochMonitoring: "False"
[Provide epoch-wise monitoring plots according to TestRate (caution: causes big ROOT output file!)] Sampling:
"1.000000e+00" [Only 'Sampling' (randomly selected) events are trained each epoch] SamplingEpoch: "1.000000e+00"
[Sampling is used for the first 'SamplingEpoch' epochs, afterwards, all events are taken for training]
SamplingImportance: "1.000000e+00" [ The sampling weights of events in epochs which successful (worse estimator than
before) are multiplied with SamplingImportance, else they are divided.] SamplingTraining: "True" [The training sample is
sampled] SamplingTesting: "False" [The testing sample is sampled] ResetStep: "50" [How often BFGS should reset history]
Tau: "3.000000e+00" [LineSearch "size step"]
BPMode: "sequential" [Back-propagation learning mode: sequential or batch]
BatchSize: "-1" [Batch size: number of events/batch, only set if in Batch Mode, -1 for BatchSize=number_of_events]
ConvergenceImprove: "1.000000e-30" [Minimum improvement which counts as improvement (<0 means automatic convergence
check is turned off)] ConvergenceTests: "-1" [Number of steps (without improvement) required for convergence (<0 means
automatic convergence check is turned off)] UpdateLimit: "10000" [Maximum times of regulator update] CalculateErrors:
"False" [Calculates inverse Hessian matrix at the end of the training to be able to calculate the uncertainties of an
MVA value] WeightRange: "1.000000e+00" [Take the events for the estimator calculations from small deviations from the
desired value to large deviations only over the weight range]
##


#VAR -*-*-*-*-*-*-*-*-*-*-*-* variables *-*-*-*-*-*-*-*-*-*-*-*-

NVar 10
fracE1             fracE1           fracE1             fracE1 'F'
[-0.00362621294335,0.443809837103] fracE2             fracE2           fracE2
fracE2                                               'F'    [-0.00275945081376,0.464425057173]
fracE3             fracE3           fracE3             fracE3 'F'
[-0.00313531467691,0.45265597105] fracE4            fracE4          fracE4
fracE4                                              'F'    [-0.00450527248904,0.465190768242]
fracEseed            fracEseed          fracEseed            fracEseed 'F'
[0.224020346999,0.934969782829] fracE6            fracE6          fracE6
fracE6                                              'F'    [-0.00276463152841,0.479072362185]
fracE7            fracE7          fracE7            fracE7 'F'
[-0.00332471798174,0.471353650093] fracE8            fracE8          fracE8
fracE8                                              'F'    [-0.00180006388109,0.465661257505
fracE9            fracE9          fracE9            fracE9 'F'
[-0.00440689036623,0.457188129425] Et                            Et                            Et Et 'F'
[2.00030565262,28.4925460815] NSpec 0

============================================================================ */

#include "Kernel/STLExtensions.h"
#include "Kernel/TMV_utils.h"
#include <array>
#include <cmath>
#include <string_view>

namespace Data::ReadMLPMiddle {
  namespace {
    constexpr auto ActivationFnc = []( double x ) {
      // fast hyperbolic tan approximation
      if ( x > 4.97 ) return 1.f;
      if ( x < -4.97 ) return -1.f;
      float x2 = x * x;
      float a  = x * ( 135135.0f + x2 * ( 17325.0f + x2 * ( 378.0f + x2 ) ) );
      float b  = 135135.0f + x2 * ( 62370.0f + x2 * ( 3150.0f + x2 * 28.0f ) );
      return a / b;
    };
    constexpr auto OutputActivationFnc = []( double x ) {
      // sigmoid
      return 1.0 / ( 1.0 + exp( -x ) );
    };

    // Normalization transformation, initialisation
    constexpr auto fMin_1 = std::array{
        std::array{-0.00362621294335, -0.00275945081376, -0.00313531467691, -0.00450527248904, 0.246648788452,
                   -0.00276463152841, -0.00332471798174, -0.00180006388109, -0.00440689036623, 2.00030565262},
        std::array{-0.00318898749538, -0.00224394164979, -0.00181982072536, -0.00381390075199, 0.224020346999,
                   -0.00146057619713, -0.00164786248934, -0.00151467497926, -0.00248911418021, 2.00102257729},
        std::array{-0.00362621294335, -0.00275945081376, -0.00313531467691, -0.00450527248904, 0.224020346999,
                   -0.00276463152841, -0.00332471798174, -0.00180006388109, -0.00440689036623, 2.00030565262}};

    constexpr auto fMax_1 =
        std::array{std::array{0.338223308325, 0.464425057173, 0.317308604717, 0.465190768242, 0.934969782829,
                              0.479072362185, 0.306119203568, 0.465661257505, 0.401705533266, 28.4925460815},
                   std::array{0.443809837103, 0.457774341106, 0.45265597105, 0.463830649853, 0.896284222603,
                              0.467007666826, 0.471353650093, 0.461849570274, 0.457188129425, 23.836101532},
                   std::array{0.443809837103, 0.464425057173, 0.45265597105, 0.465190768242, 0.934969782829,
                              0.479072362185, 0.471353650093, 0.465661257505, 0.457188129425, 28.4925460815}};

    // weight matrix from layer 0 to 1
    constexpr auto fWeightMatrix0to1 =
        std::array{std::array{2.76038716664637, -4.04151404322157, 3.00363447574542, 0.32630807509579, 1.21735893663165,
                              0.468891930484242, 2.97364929150601, -4.50393081062551, 3.78353928213453,
                              0.117549226952447, 2.88653311389283},
                   std::array{3.66665865299493, 3.18848329623295, -2.30197628571885, 2.95330109637166, 4.80194364386187,
                              -7.56415550763011, 1.74857418140998, -6.51238814418744, 7.12400332693267,
                              -0.0750151473873144, -2.5905476275632},
                   std::array{-2.03119870964679, -5.36695952071148, 2.95668733640128, 0.31839452493056,
                              0.734390005415991, -8.9357892446043, 0.849259653846504, 0.110406189330802,
                              -0.495912457348653, 0.648425185127335, -12.0844689928302},
                   std::array{0.483662499925511, 0.0440833076353419, 0.612581277943129, 0.781799170346265,
                              3.44276313331265, -0.185666593831779, 1.78203654142531, 0.670887757460542,
                              0.61488639311317, -6.38350460427769, -2.4815103889677},
                   std::array{0.854730605215466, -3.92929893560465, -3.76171518708365, 5.70415204806725,
                              -6.01761091419949, -3.7371243329302, -7.43416448429159, 7.22907400152492,
                              0.942310722330599, 0.191582837840389, 2.13456378726523},
                   std::array{-4.49897972824554, -0.690664957083194, 0.716448249798002, 5.68486457629061,
                              -1.53343564701672, 1.3384447198659, -4.60493527150641, -0.778274420933426,
                              0.804899625987248, 0.176217668230878, 0.607393485206533},
                   std::array{-1.30446219042648, -1.27021425758672, 2.62706254945813, -3.36606182483553,
                              0.712052850508852, -0.450461790435611, -1.27197965730986, -5.34650244810965,
                              2.56681873522447, -0.904958205590199, -6.94792557699129},
                   std::array{-6.7735318221976, 5.6598846829997, 0.682707862715019, 9.1915971661894, -4.49833005051649,
                              -2.57716147876984, -1.01022884376554, -2.07227383728691, -3.32344841860545,
                              -0.0799548512018979, 3.31745772930459},
                   std::array{0.809321368915275, -1.29160053786155, -4.87882111428126, 0.86475458509803,
                              -2.25068819888714, 7.93477566732004, -1.87058591488525, -1.22228712292729,
                              -5.37394273816012, -0.134691885144756, -1.73982552982106},
                   std::array{-0.262826807588696, 4.37482666278269, -0.694797698127611, 0.135384827423615,
                              -1.46258391643328, -0.964217742135818, -1.54084589876999, -1.89149551924808,
                              -1.83676372570172, 0.377462943676275, 0.612542767222117}};
    // weight matrix from layer 1 to 2
    constexpr auto fWeightMatrix1to2 = std::array{
        -3.98421652744576, 2.62121698628654,  2.67617318859809, 2.10508119817919,  -2.32974568104083, 3.50577557902819,
        1.18458245306864,  -2.21274037187777, 3.93960416012021, -1.57074818992032, 0.677169403408848};

    // the training input variables
    constexpr auto validator =
        TMV::Utils::Validator{"ReadMLPMiddle", std::tuple{"fracE1", "fracE2", "fracE3", "fracE4", "fracEseed", "fracE6",
                                                          "fracE7", "fracE8", "fracE9", "Et"}};

    // Normalization transformation
    constexpr auto transformer = TMV::Utils::Transformer{fMin_1, fMax_1};
    constexpr auto l0To1       = TMV::Utils::Layer{fWeightMatrix0to1, ActivationFnc};
    constexpr auto l1To2       = TMV::Utils::Layer{fWeightMatrix1to2, OutputActivationFnc};
    constexpr auto MVA         = TMV::Utils::MVA{validator, transformer, 2, l0To1, l1To2};
  } // namespace
} // namespace Data::ReadMLPMiddle

struct ReadMLPMiddle final {

  // constructor
  ReadMLPMiddle( LHCb::span<const std::string_view, 10> theInputVars ) {
    Data::ReadMLPMiddle::MVA.validate( theInputVars );
  }

  // the classifier response
  // "inputValues" is a vector of input values in the same order as the
  // variables given to the constructor
  static constexpr double GetMvaValue( LHCb::span<const double, 10> inputValues ) {
    return Data::ReadMLPMiddle::MVA( inputValues );
  }
};
