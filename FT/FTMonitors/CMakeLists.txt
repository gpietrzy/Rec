###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
FT/FTMonitors
-------------
#]=======================================================================]


option(FT_FILLINGSCHEME_FROM_DIM "Use SciFi online monitoring" OFF)

if (FT_FILLINGSCHEME_FROM_DIM)
  find_package(Online)
  if (Online_FOUND)
    set(FTMonitors_Online_Deps Online::OnlineKernel Online::dim)
    add_definitions(-DUSE_FT_FILLINGSCHEME_FROM_DIM)
  else()
    message(FATAL_ERROR "FT: ONLINE libraries not found")
  endif()
endif()

gaudi_add_module(FTMonitors
    SOURCES
        src/FTClusterMonitor.cpp
        src/FTDigitMonitor.cpp
        src/FillingSchemeProvider.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiCommonSvcLib
        Gaudi::GaudiKernel
        LHCb::DAQEventLib
        LHCb::DetDescLib
        LHCb::FTDAQLib
        LHCb::FTDetLib
        LHCb::FTEvent
        LHCb::LHCbKernel
        ROOT::Hist
        ${FTMonitors_Online_Deps}
)

gaudi_add_tests(QMTest)
