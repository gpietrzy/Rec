/*****************************************************************************\
* (c) Copyright 2020-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/FTLiteCluster.h"
#include "Event/ODIN.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/SynchronizedValue.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiKernel/GaudiException.h>

#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"

#include "FTMonitoring.h"
#include "FillingSchemeProvider.h"
#include "TAEHandler.h"

#if __has_include( <Gaudi/PropertyFmt.h> )
#  include <Gaudi/PropertyFmt.h>
#endif

using FTLiteClusters  = LHCb::FTLiteCluster::FTLiteClusters;
namespace FTConstants = LHCb::Detector::FT;

/** @class FTClusterMonitor FTClusterMonitor.cpp
 *
 *
 *  @author Roel Aaij, adapted by Lex Greeven, Johannes Heuel
 *  @date   2018-12-13
 */
using FTLiteClusters  = LHCb::FTLiteCluster::FTLiteClusters;
using ODIN            = LHCb::ODIN;
namespace GA          = Gaudi::Accumulators;
namespace FTConstants = LHCb::Detector::FT;

class FTClusterMonitor : public LHCb::Algorithm::Consumer<void( const FTLiteClusters&, const ODIN& )> {
public:
  FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;

  void operator()( const FTLiteClusters&, const ODIN& ) const override;

private:
  void createHistograms();
  void createClusterHistograms( const std::optional<int> tae );

  std::optional<TAEHandler> m_TAEHandler;

  mutable LHCb::cxx::SynchronizedValue<std::map<std::uint32_t, Gaudi::Time>> m_startTimes;

  // TAE global
  mutable GA::Counter<> m_nCalls{this, "number of calls"};
  enum FillType {
    DEFAULT,
    WEIGHTED,
    NUM_FillTypes,
  };

  std::vector<std::string> BXTypeNames{
      "All",      "AllIsolated",         "AllLeading",         "AllTrailing",        "Isolated",           "Leading",
      "Trailing", "EmptyBeforeIsolated", "EmptyAfterIsolated", "EmptyBeforeLeading", "EmptyAfterTrailing",
  };

  using Histogram1D = Mutable<Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float>>;
  using Histogram2D = Mutable<Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, float>>;
  using ProfileHistogram =
      Mutable<Gaudi::Accumulators::ProfileHistogram<1, Gaudi::Accumulators::atomicity::full, float>>;
  using WeightedHistogram1D =
      Mutable<Gaudi::Accumulators::WeightedHistogram<1, Gaudi::Accumulators::atomicity::full, float>>;
  using WeightedHistogram2D =
      Mutable<Gaudi::Accumulators::WeightedHistogram<2, Gaudi::Accumulators::atomicity::full, float>>;
  using Axis   = Gaudi::Accumulators::Axis<float>;
  using H2DArg = Gaudi::Accumulators::HistoInputType<float, 2>;

  std::optional<Histogram1D>               m_clustersPerBunchCrossing;
  std::map<unsigned int, Histogram1D>      m_clustersPerBunchCrossingInSide;
  std::map<unsigned int, Histogram1D>      m_clustersAroundIsolatedBunch;
  std::optional<Histogram1D>               m_clustersPerTAE;
  std::map<std::string, Histogram1D>       m_clustersPerTAEODIN;
  std::map<unsigned int, Histogram1D>      m_clustersPerTAEInSide;
  std::optional<Histogram2D>               m_TAESiPM;
  std::optional<ProfileHistogram>          m_meanTAESiPMProfile;
  std::map<unsigned int, ProfileHistogram> m_meanTAESiPMProfileInSide;

  std::optional<Histogram2D> m_nClustersPerModulePerTime;

  std::optional<Histogram2D>      m_ClustersPerEventPerSiPM;
  std::optional<ProfileHistogram> m_ClustersPerEventPerSiPMProfile;

  std::optional<Histogram2D>      m_ClustersPerEventPerBX;
  std::optional<ProfileHistogram> m_ClustersPerEventPerBXProfile;
  std::optional<ProfileHistogram> m_ClustersPerEventPerBXProfileBB;

  using fillT    = unsigned int;
  using taeT     = std::optional<int>;
  using stationT = unsigned int;
  using layerT   = unsigned int;
  using quarterT = unsigned int;
  using moduleT  = unsigned int;
  using binT     = unsigned int;
  using stepT    = unsigned int;

  std::map<std::tuple<stationT, layerT, quarterT>, Histogram2D> m_TAESiPMinQuarter;

  std::map<taeT, ProfileHistogram> m_clustersSideProfile;
  std::map<taeT, ProfileHistogram> m_clustersQuarterProfile;
  std::map<taeT, ProfileHistogram> m_clustersModuleProfile;

  std::map<taeT, Histogram2D> m_clustersChannelModuleHistogram;
  std::map<taeT, Histogram2D> m_clustersQuarterChannelHistogram;
  std::map<taeT, Histogram2D> m_clustersQuarterModuleHistogram;
  std::map<taeT, Histogram2D> m_clustersSideHistogram;
  std::map<taeT, Histogram2D> m_clustersQuarterHistogram;
  std::map<taeT, Histogram2D> m_clustersModuleHistogram;
  std::map<taeT, Histogram1D> m_nClustersPerEvent;
  std::map<taeT, Histogram1D> m_nClustersPerEventLog10;
  std::map<taeT, Histogram1D> m_lcisipm;

  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lcps;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lcpm;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lcpsipm;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lcpc;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lcf;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lcs;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram1D> m_lciq;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram2D> m_lcsf;
  std::map<std::tuple<fillT, taeT>, WeightedHistogram2D> m_lcqsipm;

  std::deque<Histogram2D> m_correlationSiPM;

  std::optional<Histogram1D>        m_clustersPerStep;
  std::map<stepT, Histogram2D>      m_TAESiPM_step;
  std::map<stepT, ProfileHistogram> m_meanTAESiPMProfile_step;

  std::map<std::tuple<fillT, taeT, binT>, WeightedHistogram1D>                   m_PseudoChannel;
  std::map<std::tuple<fillT, taeT, binT, layerT>, WeightedHistogram1D>           m_PseudoChannelPerLayer;
  std::map<std::tuple<fillT, taeT, binT, quarterT>, WeightedHistogram1D>         m_PseudoChannelPerQuarter;
  std::map<std::tuple<fillT, taeT, binT, stationT, layerT>, WeightedHistogram1D> m_PseudoChannelPerStationAndLayer;
  std::map<std::tuple<fillT, taeT, binT, stationT, layerT, quarterT>, WeightedHistogram1D>
                                                                                              m_PseudoChannelPerStationAndLayerAndQuarter;
  std::map<std::tuple<fillT, taeT, stationT>, WeightedHistogram1D>                            m_lcpst;
  std::map<std::tuple<fillT, taeT, stationT, layerT, quarterT>, WeightedHistogram1D>          m_sipmpq;
  std::map<std::tuple<fillT, taeT, stationT, layerT, quarterT>, WeightedHistogram1D>          m_fracpq;
  std::map<std::tuple<fillT, taeT, stationT, layerT, quarterT>, WeightedHistogram1D>          m_pseusizepq;
  std::map<std::tuple<fillT, taeT, stationT, layerT, quarterT, moduleT>, WeightedHistogram1D> m_lcichpm;

  std::optional<Mutable<GA::Histogram<1, GA::atomicity::full, float>>> m_belowThreshold;
  std::optional<Mutable<GA::Histogram<1, GA::atomicity::full, float>>> m_eventsPerBunchCrossing;

  Gaudi::Property<bool> m_drawHistsPerStation{this, "DrawHistsPerStation", true, "Enable histrograms per station"};
  Gaudi::Property<bool> m_drawHistsPerQuarter{this, "DrawHistsPerQuarter", true, "Enable histrograms per quarter"};
  Gaudi::Property<bool> m_drawHistsPerModule{this, "DrawHistsPerModule", true, "Enable histograms per module"};
  Gaudi::Property<bool> m_drawCorrelationPlots{this, "DrawCorrelationPlots", true, "Enable correlation plots"};
  Gaudi::Property<bool> m_drawTimeDependentPlots{this, "DrawTimeDependentPlots", false, "Enable time dependent plots"};

  Gaudi::Property<double>       m_normalization{this, "Normalization", 1.,
                                          "Normalization factor to use in weighted histograms"};
  Gaudi::Property<std::string>  m_extraInfo{this, "ExtraInfo", "", "Extra information to add to the plot"};
  Gaudi::Property<unsigned int> m_timingStepsTotal{this, "TimingStepsTotal", 0, "Total number of timing scan steps"};
  Gaudi::Property<std::vector<unsigned int>> m_ModulesToSkip{
      this, "ModulesToSkip", {}, "a list of modules in quarters to skip"};
  Gaudi::Property<std::vector<unsigned int>>        m_SiPMsToSkip{this, "SiPMsToSkip", {}, "a list of SiPMs to skip"};
  Gaudi::Property<std::vector<std::vector<double>>> m_ChannelsToSkip{
      this, "ChannelsToSkip", {}, "a list of pseudochannels in quarters to skip"};
  Gaudi::Property<std::vector<unsigned int>> m_StepsToSkip{this, "StepsToSkip", {}, "a list of Scan Steps to skip"};
  Gaudi::Property<unsigned int>              m_maxTAEHalfWindow{this, "TAEHalfWindow", 7, "max TAE half window"};
  Gaudi::Property<unsigned int>              m_maxTAEDetailedHalfWindow{this, "TAEDetailedHalfWindow", 2,
                                                           "max TAE detailed half window"};
  Gaudi::Property<unsigned int> m_clustersPerBxMin{this, "ClustersPerBxMin", 10, "Low threshold for cluster per BX"};
  Gaudi::Property<bool>         m_enableTAE{this, "enableTAE", true, "enable TAE mode"};
  Gaudi::Property<std::string>  m_dns{this, "DNS", "mon01", "DIM node to subscribe to"};
  std::optional<FillingSchemeProvider>       m_fillingSchemeProvider;
  Gaudi::Property<std::vector<unsigned int>> m_TAEBunchIdsOverride{
      this, "TAEBunchIds", {}, "Center bunch ids of all TAE groups"};
  Gaudi::Property<std::string> m_fillingScheme{this, "FillingScheme", "", "Fill scheme"};
  Gaudi::Property<bool>        m_online{this, "Online", true, "Run online monitoring"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTClusterMonitor )

FTClusterMonitor::FTClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ClusterLocation", LHCb::FTLiteClusterLocation::Default},
                 KeyValue{"InputODIN", LHCb::ODINLocation::Default}} ) {}

StatusCode FTClusterMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    if ( m_TAEBunchIdsOverride.empty() ) {
      if ( !m_fillingScheme.empty() ) {
        m_fillingSchemeProvider.emplace( FillingSchemeProvider::FromFile{}, m_fillingScheme );
      }
#ifdef USE_FT_FILLINGSCHEME_FROM_DIM
      if ( m_online ) { m_fillingSchemeProvider.emplace( FillingSchemeProvider::FromDIM{}, m_dns.value() ); }
#endif // USE_FT_FILLINGSCHEME_FROM_DIM
    }

    if ( m_clustersPerBxMin > 0 ) {
      m_belowThreshold.emplace( this, "BelowThreshold",
                                fmt::format( "Number of events below threshold{};Clusters;Events", m_extraInfo ),
                                Axis{m_clustersPerBxMin, -0.5f, m_clustersPerBxMin - 0.5f} );
    }
    m_eventsPerBunchCrossing.emplace( this, "EventsPerBunchCrossing",
                                      fmt::format( "bunch crossing ID{}; BX ID; Events", m_extraInfo ),
                                      Axis{FTMonitoring::maxFillID, -0.5f, FTMonitoring::maxFillID - 0.5f} );

    m_TAEHandler.emplace( this, m_enableTAE, m_maxTAEHalfWindow, m_extraInfo );

    m_clustersPerBunchCrossing.emplace( this, "ClustersPerBunchCrossing",
                                        fmt::format( "bunch crossing ID{};BX ID;Clusters", m_extraInfo ),
                                        Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f} );
    m_clustersPerTAE.emplace( this, "nClustersPerTAE",
                              fmt::format( "Number of clusters per TAE{};TAE;Clusters", m_extraInfo ),
                              Axis{2 * m_maxTAEHalfWindow + 1, -0.5f - m_maxTAEHalfWindow, m_maxTAEHalfWindow + 0.5f} );

    for ( auto& bxtype : BXTypeNames ) {
      buildHistogram( this, m_clustersPerTAEODIN, bxtype, fmt::format( "ODIN/nClustersPerTAE{}", bxtype ),
                      fmt::format( "Number of clusters per TAE {}{};TAE index;Clusters", bxtype, m_extraInfo ),
                      Axis{2 * m_maxTAEHalfWindow + 1, -2 * static_cast<int>( m_maxTAEHalfWindow ) - 1 - 0.5f, -0.5f} );
    }

    m_TAESiPM.emplace(
        this, "TAESiPM", fmt::format( "TAE per SiPM{};SiPM;TAE", m_extraInfo ),
        std::tuple{Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f},
                   Axis{2 * m_maxTAEHalfWindow + 1, -0.5f - m_maxTAEHalfWindow, m_maxTAEHalfWindow + 0.5f}} );
    m_meanTAESiPMProfile.emplace( this, "meanTAESiPMProfile",
                                  fmt::format( "mean TAE per SiPM{};SiPM;mean TAE", m_extraInfo ),
                                  Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f} );
    m_nClustersPerModulePerTime.emplace(
        this, "nClustersPerModulePerTime",
        fmt::format( "Number of clusters per module{};Time since start of run / s;Module index", m_extraInfo ),
        std::tuple{Axis{500, -0.5f, 5000 - 0.5f},
                   Axis{FTConstants::nModulesTotal, -0.5f, FTConstants::nModulesTotal - 0.5f}} );

    m_ClustersPerEventPerSiPM.emplace(
        this, "ClustersPerEventPerSiPM",
        fmt::format( "Clusters Per Event per SiPM{};SiPM;Clusters / Event", m_extraInfo ),
        std::tuple{Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f},
                   Axis{50, -0.5f, 50 - 0.5f}} );
    m_ClustersPerEventPerSiPMProfile.emplace(
        this, "ClustersPerEventPerSiPMProfile",
        fmt::format( "Clusters Per Event per SiPM{};SiPM;Clusters / Event / SiPM", m_extraInfo ),
        Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f} );
    m_ClustersPerEventPerBX.emplace(
        this, "ClustersPerEventPerBX", fmt::format( "Clusters Per Event per BX{};BX ID;Clusters / BX", m_extraInfo ),
        std::tuple{Axis{FTMonitoring::maxFillID, -0.5f, FTMonitoring::maxFillID - 0.5f}, Axis{101, -50.0f, 10050.0f}} );
    m_ClustersPerEventPerBXProfile.emplace(
        this, "ClustersPerEventPerBXProfile",
        fmt::format( "Clusters Per Event per BX{};BX ID;Clusters / BX", m_extraInfo ),
        Axis{FTMonitoring::maxFillID, -0.5f, FTMonitoring::maxFillID - 0.5f} );
    m_ClustersPerEventPerBXProfileBB.emplace(
        this, "ClustersPerEventPerBXProfileBB",
        fmt::format( "Clusters Per Event per beam-beam BX{};BX ID;Clusters / BX", m_extraInfo ),
        Axis{FTMonitoring::maxFillID, -0.5f, FTMonitoring::maxFillID - 0.5f} );

    constexpr auto sideName = std::array{"C", "A"};
    for ( unsigned int side = 0; side < 2; ++side ) {

      buildHistogram( this, m_clustersPerBunchCrossingInSide, side,
                      fmt::format( "ClustersPerBunchCrossing{}Side", sideName[side] ),
                      fmt::format( "Bunch crossing ID in {}-side{};BX ID;Clusters", sideName[side], m_extraInfo ),
                      Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f} );
      buildHistogram(
          this, m_clustersPerTAEInSide, side, fmt::format( "nClustersPerTAE{}Side", sideName[side] ),
          fmt::format( "Number of clusters per TAE in {}-side{};TAE;Clusters", sideName[side], m_extraInfo ),
          Axis{2 * m_maxTAEHalfWindow + 1, -0.5f - m_maxTAEHalfWindow, m_maxTAEHalfWindow + 0.5f} );
      buildHistogram( this, m_meanTAESiPMProfileInSide, side, fmt::format( "meanTAESiPMProfile{}Side", sideName[side] ),
                      fmt::format( "mean TAE per SiPM in {}-side{};SiPM;mean TAE", sideName[side], m_extraInfo ),
                      Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f} );
    }

    if ( m_timingStepsTotal > 0 && m_maxTAEHalfWindow > 0 ) {
      m_clustersPerStep.emplace( this, fmt::format( "stepping/clustersPerStep" ),
                                 fmt::format( "Clusters per step{};Step;Clusters", m_extraInfo ),
                                 Axis{512, -0.5f, 512 - 0.5f} );
      for ( unsigned int step = 0; step < m_timingStepsTotal; step++ ) {
        buildHistogram(
            this, m_TAESiPM_step, step, fmt::format( "stepping/TAESiPM_step{}", step ),
            fmt::format( "TAE per SiPM step {}{};SiPM;TAE", step, m_extraInfo ),
            std::tuple{Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f},
                       Axis{2 * m_maxTAEHalfWindow + 1, -0.5f - m_maxTAEHalfWindow, m_maxTAEHalfWindow + 0.5f}} );

        buildHistogram( this, m_meanTAESiPMProfile_step, step, fmt::format( "stepping/meanTAESiPMProfile{}", step ),
                        fmt::format( "mean TAE per SiPM {}{};SiPM; mean TAE", step, m_extraInfo ),
                        Axis{FTConstants::nSiPMsTotal, -0.5f, FTConstants::nSiPMsTotal - 0.5f} );
      }
    }

    if ( m_drawCorrelationPlots ) {
      /// correlation plots
      for ( size_t station = 1; station < FTConstants::nStations + 1; station++ ) {
        for ( size_t layerAIdx = 0; layerAIdx < FTConstants::nLayers; layerAIdx++ ) {
          for ( size_t layerBIdx = layerAIdx + 1; layerBIdx < FTConstants::nLayers; layerBIdx++ ) {
            for ( size_t quarter = 0; quarter < FTConstants::nQuarters; quarter++ ) {
              m_correlationSiPM.emplace_back(
                  this, fmt::format( "Correlation/CorrSiPM_T{}_L{}vsL{}_Q{}", station, layerAIdx, layerBIdx, quarter ),
                  fmt::format( "SiPM Correlation T{} L{} vs L{} for Q{}{};SiPMNumber_L{};SiPM number_L{}", station,
                               layerAIdx, layerBIdx, quarter, m_extraInfo, layerAIdx, layerBIdx ),
                  std::tuple{Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f,
                                  "SiPM Number L" + std::to_string( layerAIdx )},
                             Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f,
                                  "SiPM number L" + std::to_string( layerBIdx )}} );
            }
          }
        }
      }

      // correlation between stations
      for ( size_t stationA = 1; stationA < FTConstants::nStations + 1; stationA++ ) {
        for ( size_t stationB = stationA + 1; stationB < FTConstants::nStations + 1; stationB++ ) {
          for ( size_t layerAIdx = 0; layerAIdx < FTConstants::nLayers; layerAIdx++ ) {
            for ( size_t quarter = 0; quarter < FTConstants::nQuarters; quarter++ ) {
              m_correlationSiPM.emplace_back(
                  this, fmt::format( "Correlation/CorrSiPM_T{}vsT{}_L{}_Q{}", stationA, stationB, layerAIdx, quarter ),
                  fmt::format( "SiPM Correlation T{} vs T{} for L{} Q{}{};SiPMNumber_T{};SiPM number_T{}", stationA,
                               stationB, layerAIdx, quarter, m_extraInfo, stationA, stationB ),
                  std::tuple{Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f,
                                  fmt::format( "SiPM Number T{}", stationA )},
                             Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f,
                                  fmt::format( "SiPM number T{}", stationB )}} );
            }
          }
        }
      }
    }

    for ( size_t station = 1; station < FTConstants::nStations + 1; ++station ) {
      for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
        for ( size_t quarter = 0; quarter < FTConstants::nQuarters; ++quarter ) {
          buildHistogram(
              this, m_TAESiPMinQuarter, {station, layer, quarter},
              fmt::format( "meanTAEPerSiPMInQuarter/T{}L{}Q{}", station, layer, quarter ),
              fmt::format( "Mean TAE in T{}L{}Q{}{};SiPM;mean TAE", station, layer, quarter, m_extraInfo ),
              std::tuple{Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f},
                         Axis{2 * m_maxTAEHalfWindow + 1, -0.5f - m_maxTAEHalfWindow, m_maxTAEHalfWindow + 0.5f}} );
        }
      }
    }

    createClusterHistograms( {} );
    for ( int tae = -m_maxTAEHalfWindow; tae < static_cast<int>( m_maxTAEHalfWindow ) + 1; ++tae ) {
      createClusterHistograms( {tae} );
    }

    return StatusCode::SUCCESS;
  } );
}

void FTClusterMonitor::createClusterHistograms( const std::optional<int> tae ) {
  std::string taePrefix = "nonTAE/";
  if ( tae ) {
    auto index = *tae == 0 ? "" : std::to_string( abs( *tae ) );
    taePrefix  = fmt::format( "TAE/{}{}/", *tae < 0 ? "Prev" : *tae > 0 ? "Next" : "MainEvent", index );
  }

  buildHistogram( this, m_nClustersPerEvent, tae, fmt::format( "{}nClusters", taePrefix ),
                  fmt::format( "Number of clusters{};Clusters/event; Events", m_extraInfo ),
                  Axis{1000, -0.5f, 10000 - 0.5f} );
  buildHistogram( this, m_nClustersPerEventLog10, tae, fmt::format( "{}nClustersLog10", taePrefix ),
                  fmt::format( "Number of clusters{};log_10(Clusters)/event; Events", m_extraInfo ),
                  Axis{500, 0.f, 5.f} );
  buildHistogram( this, m_lcisipm, tae, fmt::format( "{}LiteClustersInSiPM", taePrefix ),
                  fmt::format( "Lite clusters per SiPM{};Number of clusters;Number of SiPMs", m_extraInfo ),
                  Axis{21, -0.5f, 21 - 0.5f} );

  buildHistogram( this, m_clustersSideProfile, tae, fmt::format( "{}clusters/clustersSideProfile", taePrefix ),
                  fmt::format( "Average number of clusters{};Side;Average number of clusters", m_extraInfo ),
                  Axis{2, -0.5f, 1.5} );
  buildHistogram( this, m_clustersQuarterProfile, tae, fmt::format( "{}clusters/clustersQuarterProfile", taePrefix ),
                  fmt::format( "Average number of clusters{};Quarter;Average number of clusters", m_extraInfo ),
                  Axis{FTConstants::nQuartersTotal, -0.5f, FTConstants::nQuartersTotal - 0.5f} );
  buildHistogram( this, m_clustersModuleProfile, tae, fmt::format( "{}clusters/clustersModuleProfile", taePrefix ),
                  fmt::format( "Average number of clusters{};Module;Average number of clusters", m_extraInfo ),
                  Axis{FTConstants::nModulesTotal, -0.5f, FTConstants::nModulesTotal - 0.5f} );
  buildHistogram( this, m_clustersChannelModuleHistogram, tae,
                  fmt::format( +"{}clusters/clustersChannelModule", taePrefix ),
                  fmt::format( "Number of clusters per channel and module{};Channel;Module", m_extraInfo ),
                  std::tuple{Axis{FTConstants::nChannels, -0.5f, FTConstants::nChannels - 0.5f},
                             Axis{FTConstants::nModulesTotal, -0.5f, FTConstants::nModulesTotal - 0.5f}} );
  buildHistogram(
      this, m_clustersQuarterChannelHistogram, tae, fmt::format( "{}clusters/clustersQuarterChannel", taePrefix ),
      fmt::format( "Number of clusters per channel and quarter{};Quarter;Channel", m_extraInfo ),
      std::tuple{Axis{FTConstants::nQuartersTotal, -0.5f, FTConstants::nQuartersTotal - 0.5f},
                 Axis{FTConstants::nMaxChannelsPerQuarter, -0.5f, FTConstants::nMaxChannelsPerQuarter - 0.5f}} );
  buildHistogram( this, m_clustersQuarterModuleHistogram, tae,
                  fmt::format( +"{}clusters/clustersQuarterModule", taePrefix ),
                  fmt::format( "Number of clusters quarter and module{};Quarter;Module", m_extraInfo ),
                  std::tuple{Axis{FTConstants::nQuartersTotal, -0.5f, FTConstants::nQuartersTotal - 0.5f},
                             Axis{FTConstants::nModulesMax, -0.5f, FTConstants::nModulesMax - 0.5f}} );

  buildHistogram( this, m_clustersSideHistogram, tae, fmt::format( "{}clusters/clustersSide", taePrefix ),
                  fmt::format( "Clusters{};Side;Clusters", m_extraInfo ),
                  std::tuple{Axis{2, -0.5f, 1.5f}, Axis{1000, -0.5f, 1000 - 0.5f}} );
  buildHistogram( this, m_clustersQuarterHistogram, tae, fmt::format( "{}clusters/clustersQuarter", taePrefix ),
                  fmt::format( "Clusters{};Quarter;Clusters", m_extraInfo ),
                  std::tuple{Axis{FTConstants::nQuartersTotal, -0.5f, FTConstants::nQuartersTotal - 0.5f},
                             Axis{500, -0.5f, 500 - 0.5f}} );
  buildHistogram( this, m_clustersModuleHistogram, tae, fmt::format( "{}clusters/clustersModule", taePrefix ),
                  fmt::format( "Clusters{};Module;Clusters", m_extraInfo ),
                  std::tuple{Axis{FTConstants::nModulesTotal, -0.5f, FTConstants::nModulesTotal - 0.5f},
                             Axis{500, -0.5f, 500 - 0.5f}} );

  for ( unsigned int fillType = 0; fillType < FillType::NUM_FillTypes; ++fillType ) {
    if ( tae && static_cast<unsigned int>( abs( *tae ) ) > m_maxTAEDetailedHalfWindow ) { continue; }

    const std::string fprefix = ( FillType::WEIGHTED == fillType ? taePrefix + "weighted/" : taePrefix );
    buildHistogram( this, m_lciq, {fillType, tae}, fprefix + "LiteClustersPerQuarter",
                    fmt::format( "Number of clusters{};Quarter;Clusters", m_extraInfo ),
                    Axis{FTConstants::nQuartersTotal, -0.5, FTConstants::nQuartersTotal} );
    buildHistogram( this, m_lcps, {fillType, tae}, fprefix + "LiteClustersPerStation",
                    fmt::format( "Lite clusters per station{}; Station; Clusters", m_extraInfo ),
                    Axis{FTConstants::nStations, 0.5f, FTConstants::nStations - 0.5f} );
    buildHistogram( this, m_lcpm, {fillType, tae}, fprefix + "LiteClustersPerModule",
                    fmt::format( "Lite clusters per module{}; Module; Clusters", m_extraInfo ),
                    Axis{FTConstants::nModulesMax, -0.5f, FTConstants::nModulesMax - 0.5f} );
    buildHistogram( this, m_lcpsipm, {fillType, tae}, fprefix + "LiteClustersPerSiPM",
                    fmt::format( "Lite clusters per SiPM; SiPMID; Clusters", m_extraInfo ),
                    Axis{FTConstants::nSiPMsPerModule, -0.5f, FTConstants::nSiPMsPerModule - 0.5f} );
    buildHistogram( this, m_lcpc, {fillType, tae}, fprefix + "LiteClustersPerChannel",
                    fmt::format( "Lite clusters per channel; Channel; Clusters", m_extraInfo ),
                    Axis{FTConstants::nChannels, -0.5f, FTConstants::nChannels - 0.5f} );
    buildHistogram( this, m_lcf, {fillType, tae}, fprefix + "LiteClusterFraction",
                    fmt::format( "Lite cluster fraction;Fraction", m_extraInfo ), Axis{2, -0.25, 0.75} );
    buildHistogram( this, m_lcs, {fillType, tae}, fprefix + "LiteClusterSize",
                    fmt::format( "Lite cluster size{};Size;Clusters", m_extraInfo ),
                    Axis{FTConstants::RawBank::maxPseudoSize, -0.5f, FTConstants::RawBank::maxPseudoSize - 0.5f} );
    buildHistogram(
        this, m_lcsf, {fillType, tae}, fprefix + "LiteClusterSizeVsFraction",
        fmt::format( "Cluster size vs fraction{};Size;Fraction", m_extraInfo ),
        std::tuple{Axis{FTConstants::RawBank::maxPseudoSize, -0.5f, FTConstants::RawBank::maxPseudoSize - 0.5f, "Size"},
                   Axis{2, -0.25, 0.75, "Fraction"}} );

    buildHistogram(
        this, m_lcqsipm, {fillType, tae}, fprefix + "QuarterVsSiPMNumber",
        fmt::format( "Quarter ID vs SiPMNumber{}; Quarter ID; SiPM number", m_extraInfo ),
        std::tuple{
            Axis{FTConstants::nQuartersTotal, -0.5f, FTConstants::nQuartersTotal - 0.5f, "Quarter ID"},
            Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f, "SiPM number"}} );

    for ( const unsigned int channelsPerBin : {1, 64} ) {
      std::string channelsPerBinStr  = "";
      std::string channelsPerBinName = "";
      if ( channelsPerBin != 1 ) {
        channelsPerBinName = fmt::format( "_{}", channelsPerBin );
        channelsPerBinStr  = fmt::format( " / ({} channels)", channelsPerBin );
      }
      std::string post = channelsPerBinName;
      buildHistogram(
          this, m_PseudoChannel, {fillType, tae, channelsPerBin}, fprefix + "LiteClustersPerPseudoChannel" + post,
          fmt::format( "clusters per pseudo channel{};Pseudo channel;Clusters{}", m_extraInfo, channelsPerBinStr ),
          Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5,
               FTConstants::nMaxChannelsPerQuarter - 0.5f} );
      for ( size_t layer = 0; layer < FTConstants::nLayersTotal; ++layer ) {
        buildHistogram( this, m_PseudoChannelPerLayer, {fillType, tae, channelsPerBin, layer},
                        fmt::format( "{}perLayer/LiteClustersPerPseudoChannel/L{}{}", fprefix, layer, post ),
                        fmt::format( "clusters per pseudo channel in L{}{};Pseudo channel;Clusters{}", layer,
                                     m_extraInfo, channelsPerBinStr ),
                        Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5f,
                             FTConstants::nMaxChannelsPerQuarter - 0.5f} );
      }

      for ( size_t quarter = 0; quarter < FTConstants::nQuartersTotal; ++quarter ) {
        buildHistogram( this, m_PseudoChannelPerQuarter, {fillType, tae, channelsPerBin, quarter},
                        fmt::format( "{}perQuarter/LiteClustersPerPseudoChannel/Q{:02d}{}", fprefix, quarter, post ),
                        fmt::format( "clusters per pseudo channel in Q{}{};Pseudo channel;Clusters{}", quarter,
                                     m_extraInfo, channelsPerBinStr ),
                        Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5,
                             FTConstants::nMaxChannelsPerQuarter - 0.5f} );
      }

      for ( size_t station = 1; station < FTConstants::nStations + 1; ++station ) {
        for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
          buildHistogram( this, m_PseudoChannelPerStationAndLayer, {fillType, tae, channelsPerBin, station, layer},
                          fmt::format( "{}perStationAndLayer/LiteClustersPerPseudoChannel/T{}L{}{}", fprefix, station,
                                       layer, post ),
                          fmt::format( "clusters per pseudo channel in T{}L{}{};Pseudo channel;Clusters{}", station,
                                       layer, m_extraInfo, channelsPerBinStr ),
                          Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5,
                               FTConstants::nMaxChannelsPerQuarter - 0.5f} );
        }
      }

      for ( size_t station = 1; station < FTConstants::nStations + 1; ++station ) {
        for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
          for ( size_t quarter = 0; quarter < FTConstants::nQuarters; ++quarter ) {
            buildHistogram( this, m_PseudoChannelPerStationAndLayerAndQuarter,
                            {fillType, tae, channelsPerBin, station, layer, quarter},
                            fmt::format( "{}perStationAndLayerAndQuarter/LiteClustersPerPseudoChannel/T{}L{}Q{}{}",
                                         fprefix, station, layer, quarter, post ),
                            fmt::format( "clusters per pseudo channel in T{}L{}Q{}{};Pseudo "
                                         "channel;Clusters",
                                         station, layer, quarter, m_extraInfo ),
                            Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5,
                                 FTConstants::nMaxChannelsPerQuarter - 0.5f} );
          }
        }
      }
    }

    // Histograms per station/quarter/module
    for ( size_t station = 1; station < FTConstants::nStations + 1; station++ ) {
      if ( m_drawHistsPerStation ) {
        buildHistogram( this, m_lcpst, {fillType, tae, station},
                        fmt::format( "{}perStation/LiteClusters/T{}", fprefix, station ),
                        fmt::format( "Lite Clusters In Station {}{}; Module ID; Clusters", station, m_extraInfo ),
                        Axis{FTConstants::nModulesPerStation( station ), -0.5f,
                             FTConstants::nModulesPerStation( station ) - 0.5f} );
      }

      for ( size_t layer = 0; layer < FTConstants::nLayers; layer++ ) {
        for ( size_t quarter = 0; quarter < FTConstants::nQuarters; quarter++ ) {
          const auto SLQ = fmt::format( "T{}L{}Q{}", station, layer, quarter );

          if ( m_drawHistsPerQuarter ) {
            buildHistogram(
                this, m_fracpq, {fillType, tae, station, layer, quarter}, fprefix + "perQuarter/Fraction/" + SLQ,
                fmt::format( "Fraction in {}{};Fraction;Clusters", SLQ, m_extraInfo ), Axis{2, -0.25, 0.75} );
            buildHistogram(
                this, m_pseusizepq, {fillType, tae, station, layer, quarter}, fprefix + "perQuarter/PseudoSize/" + SLQ,
                fmt::format( "Pseudo size in {}{};Pseudo size; Clusters", SLQ, m_extraInfo ),
                Axis{FTConstants::RawBank::maxPseudoSize, -0.5f, FTConstants::RawBank::maxPseudoSize - 0.5f} );
            buildHistogram( this, m_sipmpq, {fillType, tae, station, layer, quarter},
                            fprefix + "perQuarter/ClustersPerSiPM/" + SLQ,
                            fmt::format( "Clusters per SiPM in {}{};SiPM number;Clusters", SLQ, m_extraInfo ),
                            Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f, FTConstants::nMaxSiPMsPerQuarter - 0.5f} );
          }
          if ( m_drawHistsPerModule ) {
            for ( size_t module = 0; module < FTConstants::nModulesPerQuarter( station ); module++ ) {
              const auto SLQM = fmt::format( "T{}L{}Q{}M{}", station, layer, quarter, module );
              buildHistogram(
                  this, m_lcichpm, {fillType, tae, station, layer, quarter, module},
                  fprefix + "perModule/LiteClustersPerChannel/" + SLQM,
                  fmt::format( "Lite Clusters Per Channel in {}{};Channel number; Clusters", SLQM, m_extraInfo ),
                  Axis{FTConstants::nChannelsPerModule, -0.5f, FTConstants::nChannelsPerModule - 0.5f} );
            }
          }
        }
      }
    }
  }
}

void FTClusterMonitor::operator()( const FTLiteClusters& clusters, const ODIN& odin ) const {
  std::optional<int> timeSinceStartOfRun;
  if ( m_drawTimeDependentPlots ) {
    timeSinceStartOfRun.emplace( m_startTimes.with_lock( [&]( std::map<std::uint32_t, Gaudi::Time>& startTimes ) {
      auto [i, _] = startTimes.emplace( odin.runNumber(), odin.eventTime() );
      return ( odin.eventTime() - i->second ).seconds();
    } ) );
  }

  std::vector<unsigned int> TAEBunchIds;
  if ( m_fillingSchemeProvider ) {
    TAEBunchIds = m_fillingSchemeProvider->get()->getIsolated( FillingSchemeProvider::FillingScheme::BXTypes::NoBeam );
  }
  if ( !m_TAEBunchIdsOverride.empty() ) { TAEBunchIds = m_TAEBunchIdsOverride; }

  if ( m_nCalls.nEntries() == 0 ) {
    info() << "isolated bunches: ";
    for ( const auto& i : TAEBunchIds ) { info() << " " << i; }
    info() << endmsg;
    info() << "eventTime: " << odin.eventTime() << endmsg;
    always() << "Filling Histograms... Please be patient!" << endmsg;
  }
  ++m_nCalls;

  if ( !m_StepsToSkip.empty() ) {
    if ( std::binary_search( m_StepsToSkip.begin(), m_StepsToSkip.end(), odin.calibrationStep() ) ) { return; }
  }

  if ( clusters.size() < m_clustersPerBxMin ) {
    ++( *m_belowThreshold )[clusters.size()];
    return;
  }

  ++( *m_eventsPerBunchCrossing )[odin.bunchId()];

  std::optional<int> tae;
  if ( odin.isTAE() ) {
    tae = m_TAEHandler->fromOdin( odin, TAEBunchIds );
    if ( !tae ) { return; }
  }

  std::array<std::array<std::array<std::vector<unsigned int>, FTConstants::nQuarters>, FTConstants::nLayers>,
             FTConstants::nMaxSiPMsPerQuarter>
                      clustersPerSiPMIdxQuarter;
  std::optional<uint> prevSiPM, prevModuleID;
  int                 clustersInSiPM = 0;

  ( *m_clustersPerBunchCrossing )[odin.bunchId()] += clusters.size();
  ++m_nClustersPerEvent.at( tae )[clusters.size()];
  ++m_nClustersPerEventLog10.at( tae )[log10( clusters.size() )];

  std::array<unsigned int, 2>                           clustersSide    = {};
  std::array<unsigned int, FTConstants::nQuartersTotal> clustersQuarter = {};
  std::array<unsigned int, FTConstants::nModulesTotal>  clustersModule  = {};
  std::array<std::array<unsigned int, FTConstants::nMaxChannelsPerQuarter>, FTConstants::nQuartersTotal>
                                                                                              clustersQuarterChannel = {};
  std::array<std::array<unsigned int, FTConstants::nModulesMax>, FTConstants::nQuartersTotal> clustersQuarterModule =
      {};
  std::array<std::array<unsigned int, FTConstants::nModulesTotal>, FTConstants::nChannels> clustersChannelModule = {};

  unsigned int clustersPerSiPM[FTConstants::nSiPMsTotal] = {0};
  unsigned int clustersPerBX                             = 0;
  unsigned int clustersPerBXBB                           = 0;

  for ( const auto& cluster : clusters.range() ) {
    LHCb::Detector::FTChannelID chanID               = cluster.channelID();
    unsigned int                globalQuarterIdx     = chanID.globalQuarterIdx();
    unsigned int                localSiPMIdx_quarter = chanID.localSiPMIdx_quarter();
    unsigned int                station              = to_unsigned( chanID.station() );
    unsigned int                stationIdx           = chanID.globalStationIdx();
    unsigned int                layerIdx             = chanID.localLayerIdx();
    unsigned int                globalLayerIdx       = chanID.globalLayerIdx();
    unsigned int                globalMatIdx     = chanID.localMatIdx() + FTConstants::nMats * chanID.globalModuleIdx();
    unsigned int                globalSiPMIdx    = chanID.localSiPMIdx() + FTConstants::nSiPM * globalMatIdx;
    unsigned int                quarterIdx       = chanID.localQuarterIdx();
    unsigned int                moduleIdx        = chanID.localModuleIdx();
    unsigned int                globalModuleIdx  = chanID.globalModuleIdx();
    unsigned int                pseudoChannelIdx = chanID.localChannelIdx_quarter();

    if ( std::find( m_ModulesToSkip.value().begin(), m_ModulesToSkip.value().end(), chanID.globalModuleIdx() ) !=
         m_ModulesToSkip.value().end() )
      continue;

    if ( std::find( m_SiPMsToSkip.value().begin(), m_SiPMsToSkip.value().end(), globalSiPMIdx ) !=
         m_SiPMsToSkip.value().end() )
      continue;

    if ( !m_ChannelsToSkip.value().empty() ) {
      if ( std::find( m_ChannelsToSkip.value()[globalQuarterIdx].begin(),
                      m_ChannelsToSkip.value()[globalQuarterIdx].end(),
                      pseudoChannelIdx ) != m_ChannelsToSkip.value()[globalQuarterIdx].end() ) {
        continue;
      }
    }

    clustersPerSiPM[globalSiPMIdx]++;
    clustersPerBX++;
    if ( odin.bunchCrossingType() == LHCb::ODIN::BXTypes::BeamCrossing ) {
      clustersPerBXBB++; // only physics beam beam collision bx
    }

    ++m_clustersPerBunchCrossingInSide.at( chanID.getSide() )[odin.bunchId()];
    ++clustersSide[chanID.getSide()];
    ++clustersQuarter[chanID.globalQuarterIdx()];
    ++clustersModule[chanID.globalModuleIdx()];
    ++clustersQuarterChannel[globalQuarterIdx][pseudoChannelIdx];
    ++clustersQuarterModule[globalQuarterIdx][moduleIdx];
    ++clustersChannelModule[chanID.localChannelIdx()][chanID.globalModuleIdx()];

    if ( m_drawTimeDependentPlots ) {
      ++( *m_nClustersPerModulePerTime )[H2DArg{*timeSinceStartOfRun, globalModuleIdx}];
    }

    if ( odin.isTAE() ) {
      ( *m_meanTAESiPMProfile )[globalSiPMIdx] += *tae;
      m_meanTAESiPMProfileInSide.at( chanID.getSide() )[globalSiPMIdx] += *tae;
      ++( *m_TAESiPM )[H2DArg{globalSiPMIdx, *tae}];
      ++m_TAESiPMinQuarter.at( {station, layerIdx, quarterIdx} )[H2DArg{localSiPMIdx_quarter, *tae}];
      ++( *m_clustersPerTAE )[*tae];

      ++m_clustersPerTAEODIN.at( "All" )[-odin.timeAlignmentEventIndex()];
      if ( isEmptyBeforeIsolatedBX( odin.eventType() ) || isIsolatedBX( odin.eventType() ) ||
           isEmptyAfterIsolatedBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "AllIsolated" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isEmptyBeforeLeadingBX( odin.eventType() ) || isLeadingBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "AllLeading" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isTrailingBX( odin.eventType() ) || isEmptyAfterTrailingBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "AllTrailing" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isIsolatedBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "Isolated" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isLeadingBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "Leading" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isTrailingBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "Trailing" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isEmptyBeforeIsolatedBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "EmptyBeforeIsolated" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isEmptyAfterIsolatedBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "EmptyAfterIsolated" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isEmptyBeforeLeadingBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "EmptyBeforeLeading" )[-odin.timeAlignmentEventIndex()];
      }
      if ( isEmptyAfterTrailingBX( odin.eventType() ) ) {
        ++m_clustersPerTAEODIN.at( "EmptyAfterTrailing" )[-odin.timeAlignmentEventIndex()];
      }

      ++m_clustersPerTAEInSide.at( chanID.getSide() )[*tae];

      if ( m_timingStepsTotal > 0 && odin.calibrationStep() < m_timingStepsTotal ) {
        m_meanTAESiPMProfile_step.at( odin.calibrationStep() )[globalSiPMIdx] += *tae;
        ++m_TAESiPM_step.at( odin.calibrationStep() )[H2DArg{globalSiPMIdx, *tae}];
        ++( *m_clustersPerStep )[odin.calibrationStep()];
      }
    }

    if ( m_drawCorrelationPlots ) {
      // save sipmIdx per quarter of each cluster for correlation plots
      clustersPerSiPMIdxQuarter[stationIdx][layerIdx][quarterIdx].push_back( localSiPMIdx_quarter );
    }
    for ( unsigned int fillType = 0; fillType < FillType::NUM_FillTypes; ++fillType ) {
      if ( tae && static_cast<unsigned int>( abs( *tae ) ) > m_maxTAEDetailedHalfWindow ) { continue; }
      double weight = 1;
      if ( FillType::WEIGHTED == fillType ) {
        weight = 1. / (double)clusters.size();
        weight /= m_normalization;
      }
      m_lcps.at( {fillType, tae} )[station] += weight;
      m_lcpm.at( {fillType, tae} )[chanID.localModuleIdx()] += weight;
      m_lcpsipm.at( {fillType, tae} )[chanID.sipmInModule()] += weight;
      m_lcpc.at( {fillType, tae} )[chanID.channel()] += weight;
      for ( const unsigned int channelsPerBin : {1, 64} ) {
        m_PseudoChannel.at( {fillType, tae, channelsPerBin} )[pseudoChannelIdx] += weight;
        m_PseudoChannelPerQuarter.at( {fillType, tae, channelsPerBin, globalQuarterIdx} )[pseudoChannelIdx] += weight;
        m_PseudoChannelPerLayer.at( {fillType, tae, channelsPerBin, globalLayerIdx} )[pseudoChannelIdx] += weight;
        m_PseudoChannelPerStationAndLayer.at( {fillType, tae, channelsPerBin, station, layerIdx} )[pseudoChannelIdx] +=
            weight;
        m_PseudoChannelPerStationAndLayerAndQuarter.at(
            {fillType, tae, channelsPerBin, station, layerIdx, quarterIdx} )[pseudoChannelIdx] += weight;
      }

      m_lcf.at( {fillType, tae} )[cluster.fraction()] += weight;
      m_lcs.at( {fillType, tae} )[cluster.pseudoSize()] += weight;
      m_lcsf.at( {fillType, tae} )[H2DArg{cluster.pseudoSize(), cluster.fraction()}] += weight;

      if ( m_drawHistsPerQuarter ) {
        m_lciq.at( {fillType, tae} )[globalQuarterIdx] += weight;
        m_lcqsipm.at( {fillType, tae} )[H2DArg{globalQuarterIdx, localSiPMIdx_quarter}] += weight;
        m_fracpq.at( {fillType, tae, station, layerIdx, quarterIdx} )[cluster.fraction()] += weight;
        m_pseusizepq.at( {fillType, tae, station, layerIdx, quarterIdx} )[cluster.pseudoSize()] += weight;
        m_sipmpq.at( {fillType, tae, station, layerIdx, quarterIdx} )[localSiPMIdx_quarter] += weight;
      }
      if ( m_drawHistsPerStation ) {
        m_lcpst.at( {fillType, tae, station} )[chanID.localModuleIdx_station()] += weight;
      }
      if ( m_drawHistsPerModule ) {
        m_lcichpm.at( {fillType, tae, station, layerIdx, quarterIdx, moduleIdx} )[chanID.localChannelIdx_module()] +=
            weight;
      }
    }
    // Count the number of clusters per SiPM
    uint thisSiPM = chanID.globalSiPMID();
    if ( prevSiPM && thisSiPM != *prevSiPM && clustersInSiPM != 0 ) {
      ++m_lcisipm.at( tae )[clustersInSiPM];
      clustersInSiPM = 0;
    }
    prevSiPM     = thisSiPM;
    prevModuleID = chanID.globalModuleIdx();
    ++clustersInSiPM;
  }
  ++m_lcisipm.at( tae )[clustersInSiPM]; // Fill this for the last time

  for ( unsigned int i = 0; i < 2; ++i ) {
    ++m_clustersSideHistogram.at( *tae )[H2DArg{i, clustersSide[i]}];
    m_clustersSideProfile.at( *tae )[i] += clustersSide[i];
  }
  for ( unsigned int i = 0; i < FTConstants::nQuartersTotal; ++i ) {
    ++m_clustersQuarterHistogram.at( *tae )[H2DArg{i, clustersQuarter[i]}];
    m_clustersQuarterProfile.at( *tae )[i] += clustersQuarter[i];
  }
  for ( unsigned int i = 0; i < FTConstants::nModulesTotal; ++i ) {
    ++m_clustersModuleHistogram.at( *tae )[H2DArg{i, clustersModule[i]}];
    m_clustersModuleProfile.at( *tae )[i] += clustersModule[i];
  }
  for ( unsigned int quarter = 0; quarter < FTConstants::nQuartersTotal; ++quarter ) {
    for ( unsigned int channel = 0; channel < FTConstants::nMaxChannelsPerQuarter; ++channel ) {
      m_clustersQuarterChannelHistogram.at( *tae )[H2DArg( quarter, channel )] +=
          clustersQuarterChannel[quarter][channel];
    }
    for ( unsigned int module = 0; module < FTConstants::nModulesMax; ++module ) {
      m_clustersQuarterModuleHistogram.at( *tae )[H2DArg( quarter, module )] += clustersQuarterModule[quarter][module];
    }
  }
  for ( unsigned int channel = 0; channel < FTConstants::nChannels; ++channel ) {
    for ( unsigned int module = 0; module < FTConstants::nModulesTotal; ++module ) {
      m_clustersChannelModuleHistogram.at( *tae )[H2DArg( channel, module )] += clustersChannelModule[channel][module];
    }
  }

  ( *m_ClustersPerEventPerBXProfile )[odin.bunchId()] += clustersPerBX;
  ++( *m_ClustersPerEventPerBX )[H2DArg( odin.bunchId(), clustersPerBX )];

  if ( odin.bunchCrossingType() == LHCb::ODIN::BXTypes::BeamCrossing ) {
    ( *m_ClustersPerEventPerBXProfileBB )[odin.bunchId()] += clustersPerBXBB;

    for ( size_t sipmindex = 0; sipmindex < FTConstants::nSiPMsTotal; sipmindex++ ) {
      ( *m_ClustersPerEventPerSiPMProfile )[sipmindex] += clustersPerSiPM[sipmindex];
      ++( *m_ClustersPerEventPerSiPM )[H2DArg( sipmindex, clustersPerSiPM[sipmindex] )];
    }
  }

  if ( m_drawCorrelationPlots ) {
    /// sort clusters by SiPM index in every layer
    for ( size_t stationIdx = 0; stationIdx < FTConstants::nStations; stationIdx++ ) {
      for ( size_t quarterIdx = 0; quarterIdx < FTConstants::nQuarters; quarterIdx++ ) {
        for ( size_t layerIdx = 0; layerIdx < FTConstants::nLayers; layerIdx++ ) {
          auto& layer = clustersPerSiPMIdxQuarter[stationIdx][layerIdx][quarterIdx];
          sort( layer.begin(), layer.end() );
        }
      }
    }

    /// correlation plots
    size_t correlationPlot = 0;
    for ( size_t stationIdx = 0; stationIdx < FTConstants::nStations; stationIdx++ ) {
      for ( size_t layerAIdx = 0; layerAIdx < FTConstants::nLayers; layerAIdx++ ) {
        for ( size_t layerBIdx = layerAIdx + 1; layerBIdx < FTConstants::nLayers; layerBIdx++ ) {
          for ( size_t quarterIdx = 0; quarterIdx < FTConstants::nQuarters; quarterIdx++ ) {
            ++correlationPlot;
            const auto layerA = clustersPerSiPMIdxQuarter[stationIdx][layerAIdx][quarterIdx];
            const auto layerB = clustersPerSiPMIdxQuarter[stationIdx][layerBIdx][quarterIdx];
            if ( layerA.size() != layerB.size() )
              continue; // try cluster matching between layers only when layers have the
                        // same number of clusters

            for ( size_t i = 0; i < layerA.size(); ++i ) {
              ++m_correlationSiPM[correlationPlot - 1][H2DArg( layerA[i], layerB[i] )];
            }
          }
        }
      }
    }
    for ( size_t stationAIdx = 0; stationAIdx < FTConstants::nStations; stationAIdx++ ) {
      for ( size_t stationBIdx = stationAIdx + 1; stationBIdx < FTConstants::nStations; stationBIdx++ ) {
        for ( size_t layerAIdx = 0; layerAIdx < FTConstants::nLayers; layerAIdx++ ) {
          for ( size_t quarterIdx = 0; quarterIdx < FTConstants::nQuarters; quarterIdx++ ) {
            ++correlationPlot;
            const auto layerA = clustersPerSiPMIdxQuarter[stationAIdx][layerAIdx][quarterIdx];
            const auto layerB = clustersPerSiPMIdxQuarter[stationBIdx][layerAIdx][quarterIdx];
            if ( layerA.size() != layerB.size() )
              continue; // try cluster matching between layers only when layers have the
                        // same number of clusters

            for ( size_t i = 0; i < layerA.size(); ++i ) {
              ++m_correlationSiPM[correlationPlot - 1][H2DArg( layerA[i], layerB[i] )];
            }
          }
        }
      }
    }
  } // end if draw correlation plots
}
