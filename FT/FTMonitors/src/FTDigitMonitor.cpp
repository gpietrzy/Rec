/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/FT/FTChannelID.h"
#include "Detector/FT/FTConstants.h"
#include "Event/FTDigit.h"
#include "Event/ODIN.h"
#include "FTDet/DeFTDetector.h"
#include "FTMonitoring.h"
#include "FillingSchemeProvider.h"
#include "GaudiAlg/Consumer.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbAlgs/Consumer.h"

#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiKernel/GaudiException.h>
#include <boost/range/irange.hpp>
#include <optional>

#include "TAEHandler.h"

#if __has_include( <Gaudi/PropertyFmt.h> )
#  include <Gaudi/PropertyFmt.h>
#endif

namespace FTConstants = LHCb::Detector::FT;

/**
 *  @author Lex Greeven, Sevda Esen, Johannes Heuel, Jan-Marc Basels
 *  @date   2020-02-17
 */
class FTDigitMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::FTDigit::FTDigits&, const DeFT&, const LHCb::ODIN& ),
                                       LHCb::Algorithm::Traits::usesBaseAndConditions<GaudiHistoAlg, DeFT>> {
public:
  FTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;
  void       operator()( const LHCb::FTDigit::FTDigits& digits, const DeFT&, LHCb::ODIN const& ) const override;

private:
  using Histo1D = Mutable<Gaudi::Accumulators::Histogram<1, Gaudi::Accumulators::atomicity::full, float>>;
  using Histo2D = Mutable<Gaudi::Accumulators::Histogram<2, Gaudi::Accumulators::atomicity::full, float>>;
  using Axis    = Gaudi::Accumulators::Axis<float>;
  using H2DArg  = Gaudi::Accumulators::HistoInputType<float, 2>;

  enum FillType {
    DEFAULT,
    ADCCOUNTS,
    ADCZERO,
    ADCONE,
    ADCTWO,
    ADCTHREE,
    NUM_FillTypes,
  };
  void fillHistograms( const std::optional<int> tae, const LHCb::FTDigit& Digit, const DeFT& deFT,
                       const LHCb::ODIN& odin ) const;
  void createHistograms();
  void createDigitHistograms( std::optional<int> tae );

  std::optional<TAEHandler> m_TAEHandler;

  // TAE global
  mutable Gaudi::Accumulators::Counter<> m_nCalls{this, "number of calls"};
  std::optional<Histo1D>                 m_eventsPerBunchCrossing;
  std::optional<Histo1D>                 m_nDigits;

  // once per TAE
  using taeT = std::optional<int>;
  std::map<taeT, Histo1D> m_ADCCounts;
  std::map<taeT, Histo1D> m_digitsPerStation;
  std::map<taeT, Histo1D> m_digitsPerModule;
  std::map<taeT, Histo1D> m_digitsPerMat;
  std::map<taeT, Histo1D> m_digitsPerSiPM;
  std::map<taeT, Histo1D> m_digitsPerChannel;
  std::map<taeT, Histo1D> m_digitsPerQuarter;
  std::map<taeT, Histo2D> m_digitsPerMatModule;
  std::map<taeT, Histo2D> m_digitsPerQuarterLayer;

  // fill type, binning, station, layer, quarter
  using fillT    = unsigned int;
  using binT     = unsigned int;
  using stationT = unsigned int;
  using layerT   = unsigned int;
  using quarterT = unsigned int;

  std::map<std::tuple<fillT, taeT>, Histo1D>                             m_BunchCrossing;
  std::map<std::tuple<fillT, taeT, stationT, layerT, quarterT>, Histo2D> m_BXIDVsSiPMPerStationAndLayerAndQuarter;
  std::map<std::tuple<fillT, taeT, binT>, Histo1D>                       m_PseudoChannel;
  std::map<std::tuple<fillT, taeT, binT, stationT, layerT, quarterT>, Histo1D>
      m_PseudoChannelPerStationAndLayerAndQuarter;

  Gaudi::Property<std::string>  m_extraInfo{this, "ExtraInfo", "", "Extra information to add to the plot"};
  Gaudi::Property<unsigned int> m_maxTAEHalfWindow{this, "TAEHalfWindow", 7, "TAE half window"};
  Gaudi::Property<unsigned int> m_maxTAEDetailedHalfWindow{this, "TAEDetailedHalfWindow", 2,
                                                           "max TAE detailed half window"};
  Gaudi::Property<bool>         m_enableTAE{this, "enableTAE", true, "enable TAE mode"};
  Gaudi::Property<bool>         m_drawHistsPerBXIDVsSiPM{
      this, "DrawHistsPerBXIDVsSiPM", false,
      "Enable 2-dim histrograms per BXID (bunch crossing ID) vs SiPM (per station/layer/quarter)"};
  Gaudi::Property<bool> m_drawHistsPerPseudochannel{this, "DrawHistsPerPseudochannel", false,
                                                    "Enable histrograms per pseudochannel (per station/layer/quarter)"};

  Gaudi::Property<std::string>               m_dns{this, "DNS", "mon01", "DIM node to subscribe to"};
  std::optional<FillingSchemeProvider>       m_fillingSchemeProvider;
  Gaudi::Property<std::vector<unsigned int>> m_TAEBunchIdsOverride{
      this, "TAEBunchIds", {}, "Center bunch ids of all TAE groups"};
  Gaudi::Property<std::string> m_fillingScheme{this, "FillingScheme", "", "Fill scheme"};
  Gaudi::Property<bool>        m_online{this, "Online", true, "Run online monitoring"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( FTDigitMonitor )

FTDigitMonitor::FTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"DigitLocation", LHCb::FTDigitLocation::Default},
                 KeyValue{"FTDetectorLocation", DeFTDetectorLocation::Default},
                 KeyValue{"InputODIN", LHCb::ODINLocation::Default}} ) {}

StatusCode FTDigitMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    if ( m_TAEBunchIdsOverride.empty() ) {
      if ( !m_fillingScheme.empty() ) {
        m_fillingSchemeProvider.emplace( FillingSchemeProvider::FromFile{}, m_fillingScheme );
      } else if ( m_online ) {
        m_fillingSchemeProvider.emplace( FillingSchemeProvider::FromDIM{}, m_dns.value() );
      }
    }
    m_eventsPerBunchCrossing.emplace( this, "EventsPerBunchCrossing",
                                      fmt::format( "bunch crossing ID{}; BX ID; Events", m_extraInfo ),
                                      Axis{4096, -0.5f, 4095 + 0.5f} );
    m_nDigits.emplace( this, "nDigits", fmt::format( "Number of digits{}; Digits/event; Events", m_extraInfo ),
                       Axis{200, -0.5f, 6e5 + 0.5f} );

    m_TAEHandler.emplace( this, m_enableTAE, m_maxTAEHalfWindow, m_extraInfo );

    createHistograms();
  } );
}

void FTDigitMonitor::createHistograms() {
  createDigitHistograms( {} );

  for ( int tae = -m_maxTAEHalfWindow; tae < static_cast<int>( m_maxTAEHalfWindow ) + 1; ++tae ) {
    createDigitHistograms( {tae} );
  }
}

void FTDigitMonitor::createDigitHistograms( std::optional<int> tae ) {
  std::string prefix = "nonTAE/";
  if ( tae ) {
    std::string name = "MainEvent";
    if ( *tae < 0 ) { name = "Prev"; }
    if ( *tae > 0 ) { name = "Next"; }
    auto index = *tae == 0 ? "" : std::to_string( abs( *tae ) );
    prefix     = fmt::format( "TAE/{}{}/", name, index );
  }
  buildHistogram( this, m_ADCCounts, tae, prefix + "ADCCounts",
                  fmt::format( "Charge in 2bit ADC{};Charge/digit [ADC];Number of digits", m_extraInfo ),
                  Axis{FTConstants::RawBank::maxAdcCharge, -0.5f, FTConstants::RawBank::maxAdcCharge - 0.5f} );
  buildHistogram( this, m_digitsPerStation, tae, prefix + "DigitsPerStation",
                  fmt::format( "Digits per station{};Station;Digits", m_extraInfo ),
                  Axis{FTConstants::nStations, 0.5f, FTConstants::nStations + 0.5f} );
  buildHistogram( this, m_digitsPerModule, tae, prefix + "DigitsPerModule",
                  fmt::format( "Digits per module{};Module;Digits", m_extraInfo ),
                  Axis{FTConstants::nModulesMax, -0.5f, FTConstants::nModulesMax - 0.5f} );
  buildHistogram( this, m_digitsPerMat, tae, prefix + "DigitsPerMat",
                  fmt::format( "Digits per mat{};Mat;Digits", m_extraInfo ),
                  Axis{FTConstants::nMats, -0.5f, FTConstants::nMats - 0.5f} );
  buildHistogram( this, m_digitsPerSiPM, tae, prefix + "DigitsPerSiPM",
                  fmt::format( "Digits per SiPM{};SiPMID;Digits", m_extraInfo ),
                  Axis{FTConstants::nSiPMsPerModule, -0.5f, FTConstants::nSiPMsPerModule - 0.5f} );
  buildHistogram( this, m_digitsPerChannel, tae, prefix + "DigitsPerChannel",
                  fmt::format( "Digits per channel{};Channel;Digits", m_extraInfo ),
                  Axis{FTConstants::nChannels, -0.5f, FTConstants::nChannels - 0.5f} );
  buildHistogram( this, m_digitsPerQuarter, tae, prefix + "DigitsPerQuarter",
                  fmt::format( "Digits per quarter{};Quarter;Digits", m_extraInfo ),
                  Axis{FTConstants::nQuarters, -0.5f, FTConstants::nQuarters - 0.5f} );
  buildHistogram( this, m_digitsPerMatModule, tae, prefix + "DigitsPerMatModule",
                  fmt::format( "Digits per mat and module{};Mat;Module", m_extraInfo ),
                  std::tuple{Axis{FTConstants::nMats, -0.5f, FTConstants::nMats - 0.5f, "Mat"},
                             Axis{FTConstants::nModulesMax, -0.5f, FTConstants::nModulesMax - 0.5f, "Module"}} );
  buildHistogram( this, m_digitsPerQuarterLayer, tae, prefix + "DigitsPerQuarterLayer",
                  fmt::format( "Digits per quarter and layer{};Quarter;Layer", m_extraInfo ),
                  std::tuple{Axis{FTConstants::nQuarters, -0.5f, FTConstants::nQuarters - 0.5f, "Quarter"},
                             Axis{FTConstants::nLayers, -0.5f, FTConstants::nLayers - 0.5f, "Layer"}} );

  // hists per fill type
  for ( unsigned int fillType = 0; fillType < FillType::NUM_FillTypes; ++fillType ) {
    std::string fprefix = "Digits";
    std::string title   = "digits";
    std::string ylabel  = "Digits";
    if ( FillType::ADCCOUNTS == fillType ) {
      fprefix = "ADCCounts";
      title   = "total charge [ADC]";
      ylabel  = "Total charge [ADC]";
    } else if ( FillType::ADCZERO <= fillType && FillType::ADCTHREE >= fillType ) {
      fprefix = fmt::format( "DigitsADC{}", fillType - FillType::ADCZERO );
      title   = fmt::format( "digts [ADC = {}]", fillType - FillType::ADCZERO );
    }

    // histograms per bunch crossing ID
    buildHistogram( this, m_BunchCrossing, {fillType, tae}, prefix + fprefix + "PerBunchCrossing",
                    fmt::format( "{}{};BX ID;{}", title, m_extraInfo, ylabel ), Axis{4096, -0.5f, 4095 + 0.5f} );

    // histograms per SiPM ID vs bunch crossing ID
    if ( m_drawHistsPerBXIDVsSiPM ) {
      for ( size_t station = 1; station < FTConstants::nStations + 1; ++station ) {
        for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
          for ( size_t quarter = 0; quarter < FTConstants::nQuarters; ++quarter ) {
            buildHistogram( this, m_BXIDVsSiPMPerStationAndLayerAndQuarter, {fillType, tae, station, layer, quarter},
                            prefix + fmt::format( "perStationAndLayerAndQuarter/{}PerBXIDVsSiPM/T{}L{}Q{}", fprefix,
                                                  station, layer, quarter ),
                            fmt::format( "{} per bunch crossing and SIPM in T{}L{}Q{}{};BX ID; SiPM ID", title, station,
                                         layer, quarter, m_extraInfo ),
                            std::tuple{Axis{4096 / 8, -0.5f, 4096 / 8 - 0.5f, "BX ID"}, // focus on first 512 BXIDs,
                                                                                        // otherwise TH2 too large
                                       Axis{FTConstants::nMaxSiPMsPerQuarter, -0.5f,
                                            FTConstants::nMaxSiPMsPerQuarter - 0.5f, "SiPM ID"}} );
          }
        }
      }
    }

    // histograms per pseudochannel
    for ( const unsigned int channelsPerBin : {1, 64} ) {
      std::string channelsPerBinStr  = "";
      std::string channelsPerBinName = "";
      if ( channelsPerBin != 1 ) {
        channelsPerBinName = fmt::format( "_{}", channelsPerBin );
        channelsPerBinStr  = fmt::format( " / ({} channels)", channelsPerBin );
      }
      std::string post = channelsPerBinName;

      buildHistogram(
          this, m_PseudoChannel, {fillType, tae, channelsPerBin}, prefix + fprefix + "PerPseudoChannel" + post,
          fmt::format( "{} per pseudo channel{};Pseudo channel;{}{}", title, m_extraInfo, ylabel, channelsPerBinStr ),
          Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5f,
               FTConstants::nMaxChannelsPerQuarter - 0.5f} );

      if ( m_drawHistsPerPseudochannel ) {
        for ( size_t station = 1; station < FTConstants::nStations + 1; ++station ) {
          for ( size_t layer = 0; layer < FTConstants::nLayers; ++layer ) {
            for ( size_t quarter = 0; quarter < FTConstants::nQuarters; ++quarter ) {
              buildHistogram( this, m_PseudoChannelPerStationAndLayerAndQuarter,
                              {fillType, tae, channelsPerBin, station, layer, quarter},
                              prefix + fmt::format( "perStationAndLayerAndQuarter/{}PerPseudoChannel/T{}L{}Q{}{}",
                                                    fprefix, station, layer, quarter, post ),
                              fmt::format( "{} per pseudo channel in T{}L{}Q{}{};Pseudo channel;{}{}", title, station,
                                           layer, quarter, m_extraInfo, ylabel, channelsPerBinStr ),
                              Axis{FTConstants::nMaxChannelsPerQuarter / channelsPerBin, -0.5f,
                                   FTConstants::nMaxChannelsPerQuarter - 0.5f} );
            }
          }
        }
      }
    }
  }
}

void FTDigitMonitor::operator()( const LHCb::FTDigit::FTDigits& DigitsCont, const DeFT& deFT,
                                 const LHCb::ODIN& odin ) const {
  if ( deFT.version() < 61 ) {
    throw GaudiException( "This version requires FTDet v6.1 or higher", "", StatusCode::FAILURE );
  };

  std::vector<unsigned int> TAEBunchIds;
  if ( !m_TAEBunchIdsOverride.empty() ) {
    TAEBunchIds = m_TAEBunchIdsOverride;
  } else {
    if ( m_fillingSchemeProvider ) {
      TAEBunchIds =
          m_fillingSchemeProvider->get()->getIsolated( FillingSchemeProvider::FillingScheme::BXTypes::NoBeam );
    }
  }
  if ( m_nCalls.nEntries() == 0 ) {
    info() << "isolated bunches: ";
    for ( const auto& i : TAEBunchIds ) { info() << " " << i; }
    info() << endmsg;
  }

  ++( *m_eventsPerBunchCrossing )[odin.bunchId()];

  std::optional<int> tae;
  if ( odin.isTAE() ) {
    tae = m_TAEHandler->fromOdin( odin, TAEBunchIds );
    if ( !tae ) { return; }
  }

  for ( const auto& Digit : DigitsCont.range() ) { fillHistograms( tae, Digit, deFT, odin ); }
  debug() << "ndigits: " << DigitsCont.size() << endmsg;
  ++( *m_nDigits )[DigitsCont.size()];
}

void FTDigitMonitor::fillHistograms( const std::optional<int> tae, const LHCb::FTDigit& Digit, const DeFT& deFT,
                                     const LHCb::ODIN& odin ) const {

  ++m_ADCCounts.at( tae )[Digit.adcCount()];

  const LHCb::Detector::FTChannelID chanID           = Digit.channelID();
  const auto                        mod              = deFT.findModule( chanID );
  unsigned int                      pseudoChannelIdx = 0;
  unsigned int                      station          = to_unsigned( chanID.station() );
  // unsigned int                      stationIdx           = station - 1;
  unsigned int layerIdx             = chanID.localLayerIdx();
  unsigned int quarterIdx           = chanID.localQuarterIdx();
  unsigned int localSiPMIdx_quarter = chanID.localSiPMIdx_quarter();
  // unsigned int globalLayerIdx       = layerIdx + FTConstants::nLayers * stationIdx;
  // unsigned int globalQuarterIdx     = chanID.globalQuarterIdx();
  // unsigned int globalMatIdx         = chanID.localMatIdx() + FTConstants::nMats * chanID.globalModuleIdx();
  // unsigned int globalSiPMIdx        = chanID.localSiPMIdx() + FTConstants::nSiPM * globalMatIdx;

  if ( mod ) { pseudoChannelIdx = mod->pseudoChannel( chanID ); }

  // 1D
  ++m_digitsPerStation.at( tae )[(unsigned int)chanID.station()];
  ++m_digitsPerModule.at( tae )[chanID.localModuleIdx()];
  ++m_digitsPerMat.at( tae )[chanID.localMatIdx()];
  ++m_digitsPerSiPM.at( tae )[chanID.sipmInModule()];
  ++m_digitsPerChannel.at( tae )[chanID.channel()];
  ++m_digitsPerQuarter.at( tae )[chanID.localQuarterIdx()];

  // 2D
  ++m_digitsPerMatModule.at( tae )[H2DArg( chanID.localMatIdx(), chanID.localModuleIdx() )];
  ++m_digitsPerQuarterLayer.at( tae )[H2DArg( chanID.localQuarterIdx(), chanID.localLayerIdx() )];

  // per fill type
  for ( unsigned int fillType = 0; fillType < FillType::NUM_FillTypes; ++fillType ) {
    unsigned int weight = 1;
    if ( FillType::ADCCOUNTS == fillType ) {
      weight = Digit.adcCount();
    } else if ( FillType::ADCZERO <= fillType && FillType::ADCTHREE >= fillType ) {
      weight = int( Digit.adcCount() == int( fillType - FillType::ADCZERO ) );
    }

    m_BunchCrossing.at( {fillType, tae} )[odin.bunchId()] += weight;

    if ( mod ) {
      for ( const unsigned int channelsPerBin : {1, 64} ) {
        m_PseudoChannel.at( {fillType, tae, channelsPerBin} )[pseudoChannelIdx] += weight;
        if ( m_drawHistsPerPseudochannel ) {
          m_PseudoChannelPerStationAndLayerAndQuarter.at(
              {fillType, tae, channelsPerBin, station, layerIdx, quarterIdx} )[pseudoChannelIdx] += weight;
        }
      }
      if ( m_drawHistsPerBXIDVsSiPM ) {
        m_BXIDVsSiPMPerStationAndLayerAndQuarter.at(
            {fillType, tae, station, layerIdx, quarterIdx} )[H2DArg( odin.bunchId(), localSiPMIdx_quarter )] += weight;
      }
    }
  }

  if ( m_nCalls.nEntries() == 0 ) { always() << "Filling Histograms... Please be patient!" << endmsg; }
  ++m_nCalls;
}
