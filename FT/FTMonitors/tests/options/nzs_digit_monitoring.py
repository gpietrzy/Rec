###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from os.path import basename
from Gaudi.Configuration import ApplicationMgr, MessageSvc, INFO, ERROR
from Configurables import LHCbApp, GaudiSequencer
from Configurables import FTNZSRawBankDecoder, FTDigitMonitor
from Configurables import Gaudi__Monitoring__JSONSink as JSONSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import createODIN, LHCb__UnpackRawEvent
from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep
from os.path import expandvars
import sys
sys.path.append(expandvars('$FTMONITORSROOT/tests/python'))
from glue import filling_scheme

app = LHCbApp(DataType="Upgrade")
if not UseDD4Hep:
    app.DDDBtag = "upgrade/dddb-20221004"
    app.CondDBtag = "upgrade/SFCAVERN_SF_20220922_142756_All-FixedOrder"
    app.Simulation = True
    from Configurables import CondDB
    CondDB().Upgrade = True
    CondDB().IgnoreHeartBeat = True

MessageSvc().OutputLevel = ERROR

files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000250946_20221026-063447-446_SCEB13.mdf",
]

output_name = basename(files[0]).replace(".mdf", "_nzs.root")

IOHelper("MDF").inputFiles(files)

monSeq = GaudiSequencer("SciFiMonitorSequence", IgnoreFilterPassed=True)

bankTypes = ['FTCluster', 'FTNZS', 'ODIN']
monSeq.Members += [
    LHCb__UnpackRawEvent(
        'UnpackRawEvent',
        BankTypes=bankTypes,
        RawEventLocation='/Event/DAQ/RawEvent',
        RawBankLocations=[
            '/Event/DAQ/RawBanks/{}'.format(i) for i in bankTypes
        ])
]

decodeODIN = createODIN(RawBanks="DAQ/RawBanks/ODIN")
monSeq.Members += [decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    monSeq.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]

monSeq.Members += [
    FTNZSRawBankDecoder(
        "DecodeFT", OutputLevel=INFO, RawBank='/Event/DAQ/RawBanks/FTNZS')
]

monSeq.Members += [
    FTDigitMonitor(
        OutputLevel=INFO,
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        TAEBunchIds=[56],
        enableTAE=True,
        Online=False,
    )
]

appMgr = ApplicationMgr(
    EvtMax=300,
    TopAlg=[monSeq],
    HistogramPersistency="ROOT",
    ExtSvc=[
        MessageSvcSink(),
        JSONSink(
            FileName='nzs_digit_monitoring.json',
            NamesToSave=[
                'TAEErrors',
                'TAEEvents',
                'TAEHalfWindows',
            ]),
    ],
)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    appMgr.ExtSvc += [DD4hepSvc(DetectorList=["/world", "FT"])]
