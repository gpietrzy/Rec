###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from os.path import basename
from Gaudi.Configuration import ApplicationMgr, MessageSvc, INFO, ERROR
from Configurables import Gaudi__Monitoring__JSONSink as JSONSink
from Configurables import Gaudi__Monitoring__MessageSvcSink as MessageSvcSink
from Configurables import LHCb__UnpackRawEvent, createODIN
from Configurables import LHCbApp, GaudiSequencer
from Configurables import FTRawBankDecoder, FTClusterMonitor
from GaudiConf import IOHelper
from DDDB.CheckDD4Hep import UseDD4Hep
from os.path import expandvars
import sys
sys.path.append(expandvars('$FTMONITORSROOT/tests/python'))
from glue import filling_scheme

app = LHCbApp(DataType="Upgrade")
if not UseDD4Hep:
    app.DDDBtag = "upgrade/dddb-20221004"
    app.CondDBtag = "upgrade/SFCAVERN_SF_20220922_142756_All-FixedOrder"
    app.Simulation = True
app.DataType = "Upgrade"

MessageSvc().OutputLevel = ERROR

files = [
    "mdf:root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/Run_0000251217_20221027-121010-887_SCEB15.mdf",
]

IOHelper("MDF").inputFiles(files)
output_name = basename(files[0]).replace(".mdf", "_zs.root")

monSeq = GaudiSequencer('SciFiMonitorSequence', IgnoreFilterPassed=True)

monSeq.Members += [
    LHCb__UnpackRawEvent(
        'UnpackRawEvent',
        BankTypes=['FTCluster', 'ODIN'],
        RawEventLocation='/Event/DAQ/RawEvent',
        RawBankLocations=[
            '/Event/DAQ/RawBanks/FTCluster', '/Event/DAQ/RawBanks/ODIN'
        ])
]

decodeODIN = createODIN(RawBanks="DAQ/RawBanks/ODIN")
monSeq.Members += [decodeODIN]

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer
    monSeq.Members += [IOVProducer("ReserveIOVDD4hep", ODIN=decodeODIN.ODIN)]

monSeq.Members += [FTRawBankDecoder("DecodeFT", OutputLevel=INFO)]

monSeq.Members += [
    FTClusterMonitor(
        OutputLevel=INFO,
        DrawHistsPerStation=True,
        DrawHistsPerQuarter=True,
        DrawHistsPerModule=True,
        FillingScheme=filling_scheme(
            "root://eoslhcb.cern.ch//eos/lhcb/wg/SciFi/Monitoring/test_files/fills/8314_fillingscheme.txt"
        ),
        enableTAE=True,
        Online=False,
    )
]

appMgr = ApplicationMgr(
    EvtMax=1000,
    TopAlg=[monSeq],
    ExtSvc=[
        MessageSvcSink(),
        JSONSink(
            FileName='zs_cluster_monitoring.json',
            NamesToSave=[
                '.*MainEvent/LiteClustersPerPseudoChannel_64',
                '.*nClustersPerTAE',
            ]),
    ],
)

if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
    appMgr.ExtSvc += [DD4hepSvc(DetectorList=["/world", "FT"])]
