/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "ICloneMCParticle.h"
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/DataObjectHandle.h>
/** @class CopySignalMCParticles CopySignalMCParticles.h
 *
 *  Clones all 'signal' MCParticles.
 *
 *  @author Chris Jones
 *  @date   2015-03-24
 */

class CopySignalMCParticles final : public Gaudi::Algorithm {
  /// MCParticle Cloner
  ToolHandle<ICloneMCParticle> m_cloner{this, "ICloneMCParticle", "MCParticleCloner"};

  /// Location of MCParticles to clone
  DataObjectReadHandle<LHCb::MCParticles> m_mcPs{this, "MCParticlesLocation", LHCb::MCParticleLocation::Default};

  /// Required when using MCParticleCloner/MCVertexCloner
  Gaudi::Property<std::string> m_outputPrefix{this, "OutputPrefix", "MicroDST"};

public:
  /// Standard constructor
  using Gaudi::Algorithm::Algorithm;

  StatusCode execute( const EventContext& ) const override {
    const auto* mcPs = m_mcPs.getIfExists();
    if ( mcPs ) {
      for ( const auto* mcP : *mcPs ) {
        if ( mcP->fromSignal() ) ( *m_cloner )( mcP );
      }
    }
    return StatusCode::SUCCESS;
  }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CopySignalMCParticles )

//=============================================================================
