/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// from MicroDST
#include "ICloner.h"

// Forward declarations
namespace LHCb {
  class MCVertex;
}

/** @class ICloneMCVertex MicroDST/ICloneMCVertex.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
struct GAUDI_API ICloneMCVertex : virtual MicroDST::ICloner<LHCb::MCVertex> {

  /// Interface ID
  DeclareInterfaceID( ICloneMCVertex, 3, 0 );
};
