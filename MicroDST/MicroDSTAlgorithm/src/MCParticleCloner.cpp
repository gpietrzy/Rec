/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "ICloneMCParticle.h"
#include "ICloneMCVertex.h"
#include "MicroDSTTool.h"
#include <Event/MCParticle.h>
#include <algorithm>
//-----------------------------------------------------------------------------
// Implementation file for class : MCParticleCloner
//
// 2007-11-30 : Juan PALACIOS
//-----------------------------------------------------------------------------
/** @class MCParticleCloner MCParticleCloner.h
 *
 *  MicroDSTTool to clone an LHCb::MCParticle and place it in a TES location
 *  parallel to that of the parent. It clones and stores the origin vertex
 *  using simple copy, and copies the origin vertex's LHCb::MCParticle products.
 *  End vertices receive a special treatment. By default, only SmartRefs
 *  are stored, so an XML catalogue to the original DST is required for access.
 *  It is possible to clone and store end vertices that are also decay
 *  vertices setting the <b>ICloneMCVertex</b> property to an existing
 *  implementation of ICloneMCVertex.
 *
 *  <b>Options</b>
 *  ICloneMCVertex: string. Implementation of the ICloneMCVertex used to clone
 *  decay end vertices, that is, end vertices with
 *  LHCb::MCVertex::isDecay() = true.
 *  Default: "NONE". No cloning of decay vertices performed. Only SmartRefs
 *  are copied.
 *
 *  @see ICloneMCVertex
 *  @see MCVertexCloner
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
class MCParticleCloner final : public extends<MicroDSTTool, ICloneMCParticle> {

public:
  /// Standard constructor
  using extends::extends;

  StatusCode initialize() override;

  LHCb::MCParticle* operator()( const LHCb::MCParticle* mcp ) const override;

private:
  LHCb::MCParticle* clone( const LHCb::MCParticle* mcp ) const;

  void cloneDecayVertices( const SmartRefVector<LHCb::MCVertex>& endVertices, LHCb::MCParticle* clonedParticle ) const;

private:
  typedef MicroDST::BasicItemCloner<LHCb::MCParticle> BasicMCPCloner;
  typedef MicroDST::BasicItemCloner<LHCb::MCVertex>   BasicVtxCloner;

private:
  ICloneMCVertex* m_vertexCloner = nullptr;

  Gaudi::Property<std::string> m_vertexClonerName{this, "ICloneMCVertex", "MCVertexCloner"};
};

//=============================================================================

StatusCode MCParticleCloner::initialize() {
  return extends::initialize().andThen( [&] {
    m_vertexCloner =
        ( m_vertexClonerName.value() == "NONE" ? nullptr : tool<ICloneMCVertex>( m_vertexClonerName, parent() ) );
  } );
}

//=============================================================================

LHCb::MCParticle* MCParticleCloner::clone( const LHCb::MCParticle* mcp ) const {
  if ( !mcp ) return nullptr;

  if ( msgLevel( MSG::DEBUG ) ) debug() << "clone() called for " << *mcp << endmsg;

  // Clone the MCParticle
  LHCb::MCParticle* clone = cloneKeyedContainerItem<BasicMCPCloner>( mcp );

  // Is there an originVertex that should be cloned?
  if ( const LHCb::MCVertex* originVertex = mcp->originVertex(); originVertex ) {

    // Has it already been cloned
    LHCb::MCVertex* originVertexClone = getStoredClone<LHCb::MCVertex>( originVertex );
    if ( !originVertexClone ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Cloning origin vertex " << *originVertex << endmsg;

      // make a clone
      originVertexClone = cloneKeyedContainerItem<BasicVtxCloner>( originVertex );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Cloned vertex " << *originVertexClone << endmsg;

      // Clear the current list of products in the cloned vertex
      originVertexClone->clearProducts();

      // Clone the origin vertex mother
      const auto* mother      = mcp->mother();
      auto*       motherClone = ( mother ? ( *this )( mother ) : nullptr );
      if ( motherClone && msgLevel( MSG::DEBUG ) ) debug() << "Cloned mother " << *motherClone << endmsg;
      originVertexClone->setMother( motherClone );
    }

    // Add the cloned origin vertex to the cloned MCP
    clone->setOriginVertex( originVertexClone );

    // Add the cloned MCP to the cloned origin vertex, if not already there
    const bool found =
        std::any_of( originVertexClone->products().begin(), originVertexClone->products().end(),
                     [&clone]( const SmartRef<LHCb::MCParticle>& mcP ) { return mcP.target() == clone; } );
    if ( !found ) { originVertexClone->addToProducts( clone ); }
    // else
    //{ warning() << "Attempt add a duplicate MCParticle product SmartRef" << endmsg; }

  } else {
    clone->setOriginVertex( nullptr );
  }

  // Clone the end vertices
  clone->clearEndVertices();
  cloneDecayVertices( mcp->endVertices(), clone );

  return clone;
}

//=============================================================================

void MCParticleCloner::cloneDecayVertices( const SmartRefVector<LHCb::MCVertex>& endVertices,
                                           LHCb::MCParticle*                     clonedParticle ) const {
  for ( const auto& endVtx : endVertices ) {
    if ( endVtx->isDecay() && !( endVtx->products().empty() ) ) {
      if ( m_vertexCloner ) {
        if ( msgLevel( MSG::VERBOSE ) )
          verbose() << "Cloning Decay Vertex " << *endVtx << " with " << endVtx->products().size() << " products!"
                    << endmsg;
        clonedParticle->addToEndVertices( ( *m_vertexCloner )( endVtx ) );
      } else {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Copying decay vertex SmartRefs" << endmsg;
        clonedParticle->addToEndVertices( endVtx );
      }
    }
  }
}

//=============================================================================

LHCb::MCParticle* MCParticleCloner::operator()( const LHCb::MCParticle* mcp ) const {
  if ( !mcp ) return nullptr;
  LHCb::MCParticle* clone = getStoredClone<LHCb::MCParticle>( mcp );
  return clone ? clone : this->clone( mcp );
}

//=============================================================================

// Declaration of the Tool Factory
DECLARE_COMPONENT( MCParticleCloner )

//=============================================================================
