/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartIF.h"
#include "GaudiKernel/StatusCode.h"
#include <GaudiAlg/GaudiAlgorithm.h>
#include <GaudiKernel/KeyedObject.h>

/** @namespace MicroDST Functors.hpp MicroDST/Functors.hpp
 *
 *
 *  Collection of useful functors satisfying the Cloner policy and
 *  dealing with the cloning and storing clones into TES locations.
 *
 *  Policy: Cloner must export a clone(const T*) method that returns
 *          a T*. If the input T* is null, Cloner must return a null
 *          pointer.
 *          A cloner must export member type T as ::Type.
 *
 *  @author Juan PALACIOS
 *  @date   2007-10-24
 */
namespace MicroDST {

  /**
   *
   * BasicItemCloner satisfying the Cloner policy.
   * Requirements on template parameter T:
   * T must export a method T* T::clone().
   *
   * @author Juan Palacios juancho@nikhef.nl
   * @date 16-10-2007
   */
  template <class T>
  struct BasicItemCloner {
    typedef T    Type;
    Type*        operator()( const Type* item ) { return clone( item ); }
    static Type* copy( const Type* item ) { return clone( item ); }
    static Type* clone( const Type* item ) { return item ? item->clone() : NULL; }
  };

  /**
   *
   * BasicCopy satisfying the Cloner policy.
   * Requirements on template parameter T:
   * T must have a copy constructor.
   *
   * @author Juan Palacios juancho@nikhef.nl
   * @date 16-10-2007
   */
  template <class T>
  struct BasicCopy {
    typedef T    Type;
    Type*        operator()( const Type* item ) { return copy( item ); }
    static Type* copy( const Type* item ) { return item ? new Type( *item ) : NULL; }
    static Type* clone( const Type* item ) { return copy( item ); }
  };
  //===========================================================================

} // namespace MicroDST

/** @class MicroDSTCommon MicroDSTCommon.h MicroDST/MicroDSTCommon.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-11-30
 */
template <class PBASE>
class MicroDSTCommon : public PBASE {

public:
  /// Standard constructor
  using PBASE::PBASE;

  StatusCode initialize() override;

  /**
   * Copy an object of type T from the TES onto another TES location.
   * Uses the copy constructor of type T for copying. Do not copy if object
   * already exists in that location.
   *
   * @param from The TES location of the object to be copied
   * @param to   The TES location where the copy should be stored
   * @return     Const pointer to the cloned object.
   *
   * @author Juan Palacios juancho@nikhef.nl
   * @author Ulrich Kerzel
   */

  template <class Cloner>
  typename Cloner::Type* cloneKeyedContainerItem( const typename Cloner::Type* item ) const;

  template <class T>
  T* getStoredClone( const KeyedObject<int>* original ) const;

  /**
   * Get the TES container in the TES location to be stored on the
   * MicroDST.
   * @param locTES The address in the original TES
   * @return pointer to the container in the MicroDST TES
   */
  template <class T>
  T* getOutputContainer( const std::string& location ) const;

protected:
  const std::string& outputPrefix() const { return m_outputPrefix; }

  const std::string outputTESLocation( const std::string& inputLocation ) const {
    std::string tmp( inputLocation );
    const auto  loc = tmp.find( m_rootInTES );
    if ( loc != std::string::npos ) { tmp.replace( loc, m_rootInTES.length(), "" ); }
    return this->outputPrefix() + "/" + tmp;
  }

  /** Returns the full location of the given DataObject in the Data Store
   *
   *  @param pObj Data object
   *
   *  @return Location of given data object
   */
  std::string objectLocation( const DataObject* pObj ) const {
    return ( !pObj ? "" : ( pObj->registry() ? pObj->registry()->identifier() : "" ) );
  }

  /// Returns the full location of the parent of a given object
  std::string objectLocation( const ContainedObject* obj ) const {
    return ( obj ? objectLocation( obj->parent() ) : "" );
  }

private:
  Gaudi::Property<std::string> m_outputPrefix{this, "OutputPrefix", "MicroDST"};
  std::string                  m_rootInTES = "/Event/";
  std::string                  m_fullOutputTESLocation;
};

//-----------------------------------------------------------------------------
// Implementation of the templated methods of class : MicroDSTCommon
//
// 2007-12-04 : Juan PALACIOS
//-----------------------------------------------------------------------------

template <class PBASE>
StatusCode MicroDSTCommon<PBASE>::initialize() {
  const StatusCode sc = PBASE::initialize();
  if ( sc.isFailure() ) return sc;

  if ( !PBASE::rootInTES().empty() ) m_rootInTES = PBASE::rootInTES();
  this->debug() << "Set rootInTES to " << m_rootInTES << endmsg;
  return sc;
}

//=============================================================================

template <class PBASE>
template <class itemCloner>
typename itemCloner::Type*
MicroDSTCommon<PBASE>::cloneKeyedContainerItem( const typename itemCloner::Type* item ) const {
  if ( !item ) {
    if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "Cannot clone a NULL pointer !" << endmsg;
    return nullptr;
  }

  if ( !item->parent() ) {
    this->Warning( "Cannot clone an object with no parent!" ).ignore();
    return nullptr;
  }

  const auto cloneLocation = outputTESLocation( objectLocation( item->parent() ) );

  auto clones = getOutputContainer<typename itemCloner::Type::Container>( cloneLocation );
  if ( !clones ) return nullptr;

  // Propagate DataObject version from original to clone container
  clones->setVersion( item->parent()->version() );

  // try and get clone
  auto clonedItem = clones->object( item->key() );
  // std::cout << "cloneKeyedContainerItem: Cloning item key " << item->key() << " in "
  //                  << item->parent()->registry()->identifier() << " to " << cloneLocation << std::endl;
  if ( !clonedItem ) {
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "cloneKeyedContainerItem: Cloning item key " << item->key() << " in "
                    << item->parent()->registry()->identifier() << " to " << cloneLocation << endmsg;
      if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << *item << endmsg;
    }

    clonedItem = itemCloner::clone( item );
    if ( clonedItem ) {
      clones->insert( clonedItem, item->key() );
      if ( this->msgLevel( MSG::VERBOSE ) ) this->verbose() << "Cloned item " << *clonedItem << endmsg;
    } else {
      if ( this->msgLevel( MSG::DEBUG ) ) this->debug() << "cloneKeyedContainerItem: Cloning FAILED" << endmsg;
    }
  }

  return clonedItem;
}

//=============================================================================

template <class PBASE>
template <class T>
T* MicroDSTCommon<PBASE>::getStoredClone( const KeyedObject<int>* original ) const {
  if ( !original || !original->parent() ) return nullptr;

  const auto cloneLocation = outputTESLocation( objectLocation( original->parent() ) );

  auto clones = this->template getIfExists<typename T::Container>( cloneLocation );

  return ( clones ? clones->object( original->key() ) : nullptr );
}

//=============================================================================

template <class PBASE>
template <class T>
T* MicroDSTCommon<PBASE>::getOutputContainer( const std::string& location ) const {
  auto t = this->template getIfExists<T>( location );
  if ( !t ) {
    t = new T();
    this->put( t, location );
  }
  return t;
}

//=============================================================================

//=============================================================================
