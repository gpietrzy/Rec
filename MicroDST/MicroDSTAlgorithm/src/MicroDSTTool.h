/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
// Include files
#include "MicroDSTCommon.h"
#include <Gaudi/Property.h>
#include <GaudiAlg/GaudiTool.h>
#include <GaudiKernel/IProperty.h>

/** @class MicroDSTTool MicroDSTTool.h MicroDST/MicroDSTTool.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2007-12-04
 */
struct GAUDI_API MicroDSTTool : MicroDSTCommon<GaudiTool> {

  /// Standard constructor
  using MicroDSTCommon::MicroDSTCommon;

  StatusCode initialize() override {
    return MicroDSTCommon::initialize().andThen( [&, name = "OutputPrefix"]() {
      auto prop = Gaudi::Utils::getProperty( parent(), name );
      return prop ? Gaudi::Utils::setProperty( this, name, *prop ) : StatusCode::FAILURE;
    } );
  }
};
