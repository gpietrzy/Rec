###############################################################################
# (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonID
-----------
#]=======================================================================]

gaudi_add_header_only_library(MuonIDLib
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbMathLib
        LHCb::MuonDAQLib
        LHCb::TrackEvent
)

gaudi_add_module(MuonID
    SOURCES
        src/component/MuonChi2MatchTool.cpp
        src/component/MuonIDHltAlg.cpp
        src/component/MVATool.cpp
        src/component/PrepareMuonHits.cpp
    LINK
        Boost::container
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::DetDescLib
        LHCb::EventBase
        LHCb::LHCbMathLib
        LHCb::MuonDAQLib
        LHCb::MuonDetLib
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::TrackInterfacesLib
        ROOT::MathCore
        ROOT::Matrix
        VDT::vdt
        MuonIDLib
        CatboostStandaloneEvaluator
)

gaudi_install(PYTHON)

