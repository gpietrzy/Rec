/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef IMVATOOL_H_
#define IMVATOOL_H_

#include "Event/PrHits.h"
#include "GaudiKernel/IAlgTool.h"
#include "Kernel/STLExtensions.h"
#include "MuonDAQ/CommonMuonHit.h"

#include "Event/Track.h"

using MuonTrackExtrapolation = std::array<std::pair<float, float>, 4>;

class IMVATool : public extend_interfaces<IAlgTool> {
public:
  struct MVAResponses {
    float CatBoost;
  };
  DeclareInterfaceID( IMVATool, 1, 0 );
  virtual bool                getRunCatBoostBatch() const noexcept                   = 0;
  virtual size_t              getNFeatures() const noexcept                          = 0;
  virtual MVAResponses        calcBDT( const float trackP, const MuonTrackExtrapolation& extrapolation,
                                       const MuonHitContainer& hitContainer, const float chi2corr,
                                       std::array<float, 4> stZ ) const              = 0;
  virtual void                compute_features( const float trackP, const MuonTrackExtrapolation& extrapolation,
                                                const MuonHitContainer& hitContainer, const float chi2corr, LHCb::span<float> result,
                                                std::array<float, 4> stZ ) const     = 0;
  virtual std::vector<double> calcBDTBatch( LHCb::span<const float> features ) const = 0;
};

#endif // IMVATOOL_H_
