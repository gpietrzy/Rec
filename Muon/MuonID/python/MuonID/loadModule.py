#! /usr/bin/env python
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
""" @namespace loadModule
@brief Makes easier use of pickle
@author Xabier Cid Vidal xabier.cid.vidal@cern.ch
@date 2009-07-27
"""


def loadFromModule(file):
    """ A shortcut to load from a module
    @param fileName File to be loaded
    @author Xabier Cid Vidal xabier.cid.vidal@cern.ch
    """

    myf = str(file)
    locals = {}
    exec("from MuonID import " + myf, {}, locals)
    return getattr(locals[myf], myf)
