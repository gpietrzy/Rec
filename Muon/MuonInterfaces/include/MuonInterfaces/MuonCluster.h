/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @class MuonPad MuonPad.h
 *
 *  Muon pad class for standalone muon track reconstruction
 *  @author Alessia
 *  @date   2022-09-29
 */
#include "Detector/Muon/TileID.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "MuonDAQ/CommonMuonHit.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonInterfaces/MuonPad.h"
#include <ostream>
#include <vector>

namespace MuonClusterContainerLocation {
  inline const std::string Default = "Muon/MuonClusters";
}

class MuonCluster final {
public:
  /// standard constructor
  MuonCluster() = default;
  /// constructor from pad
  MuonCluster( const MuonPad* pad, const DeMuonDetector::TilePosition pad_position ) {
    createFromPad( pad, pad_position );
  }

  // public member functions
  /// add a pad to this cluster
  void addPad( const MuonPad* pad, const DeMuonDetector::TilePosition pad_position );

  /// store a progressive hit number for debugging
  void setHitID( int id );

  int hitPID() const { return m_pid; }
  int hitMother() const { return m_mamy_pid; }
  /// return the logical channels used for this cluster
  std::vector<const CommonMuonHit*> getHits() const;
  /// return the MuonTileIDs of the logical channels used for this cluster
  std::vector<LHCb::Detector::Muon::TileID> getTiles() const;
  /// get the raw times of logical channels in this cluster

  std::vector<float>    getTimes() const;
  std::array<double, 3> hitTile_Size() const; /// half-sizes of the tile(s) underlying this hit
  double                hitTile_dX() const    /// dx half-size of the tile underlying this hit
  {
    return 0.5 * ( m_hit_maxx - m_hit_minx );
  }
  double hitTile_dY() const /// dy half-size of the tile underlying this hit
  {
    return 0.5 * ( m_hit_maxy - m_hit_miny );
  }
  double hitTile_dZ() const /// dz half-size of the tile underlying this hit
  {
    return 0.5 * ( m_hit_maxz - m_hit_minz );
  }
  float hitTime() const { return m_time; }       /// in TDC counts !
  float hitDeltaTime() const { return m_dtime; } /// in TDC counts !
  float hitMinTime() const { return m_mintime; } /// in TDC counts !
  float hitMaxTime() const { return m_maxtime; } /// in TDC counts !

  /// retireve the hit ID
  int hitID() const { return m_hit_ID; }
  /// return the hit station
  int station() const { return m_pads.empty() ? -1 : m_pads[0]->tile().station(); }
  /// return the hit region
  int region() const { return m_pads.empty() ? -1 : m_pads[0]->tile().region(); }
  /// return the (first) MuonTileID of the underlying logical pad
  LHCb::Detector::Muon::TileID tile() const {
    return !m_pads.empty() ? m_pads.front()->tile() : LHCb::Detector::Muon::TileID{};
  }
  /// return the most centered tile of this cluster
  LHCb::Detector::Muon::TileID centerTile() const;
  /// check if this is a true logical and not an uncrossed log. channel
  bool isTruePad() const { return !m_pads.empty() && m_pads.front()->truepad(); }

  /// return the (first) associated MuonPad object
  const MuonPad* pad() const { return !m_pads.empty() ? m_pads[0] : nullptr; }
  /// return the associated MuonPad objects
  const std::vector<const MuonPad*>& pads() const { return m_pads; }
  /// return the MuonTileIDs of the logical pads used for this cluster
  std::vector<LHCb::Detector::Muon::TileID> getPadTiles() const;
  /// number of logical pads in this hit
  int              npads() const { return m_pads.size(); }
  int              clsizeX() const { return m_xsize; } /// cluster size in X
  int              clsizeY() const { return m_ysize; } /// cluster size in Y
  double           dX() const { return m_dx; }         /// error on X position
  double           dY() const { return m_dy; }         /// error on Y position
  double           dZ() const { return m_dz; }         /// error on Z position
  double           minX() const { return m_hit_minx; }
  double           maxX() const { return m_hit_maxx; }
  double           minY() const { return m_hit_miny; }
  double           maxY() const { return m_hit_maxy; }
  double           minZ() const { return m_hit_minz; }
  double           maxZ() const { return m_hit_maxz; }
  double           X() const { return m_position.X(); }
  double           Y() const { return m_position.Y(); }
  double           Z() const { return m_position.Z(); }
  Gaudi::XYZVector pos() const { return m_position; }
  MuonCluster&     SetXYZ( double x, double y, double z ) {
    m_position = {x, y, z};
    return *this;
  }

private:
  void createFromPad( const MuonPad* pad, const DeMuonDetector::TilePosition pad_position );

  void recomputeTime();

private:
  std::vector<const MuonPad*> m_pads;
  std::vector<double>         m_padx;
  std::vector<double>         m_pady;
  std::vector<double>         m_padz;
  Gaudi::XYZVector            m_position{};
  double                      m_dx       = 0;
  double                      m_dy       = 0;
  double                      m_dz       = 0;
  double                      m_hit_minx = 0;
  double                      m_hit_maxx = 0;
  double                      m_hit_miny = 0;
  double                      m_hit_maxy = 0;
  double                      m_hit_minz = 0;
  double                      m_hit_maxz = 0;
  float                       m_time     = 0;
  float                       m_dtime    = 0;
  float                       m_mintime  = 0;
  float                       m_maxtime  = 0;
  int                         m_pid      = 0;
  int                         m_mamy_pid = 0;
  int                         m_hit_ID   = -1;
  int                         m_xsize    = 0;
  int                         m_ysize    = 0;
  int                         m_zsize    = 0;
};

using MuonClusters = std::vector<MuonCluster, LHCb::Allocators::EventLocal<MuonCluster>>;
typedef std::vector<const MuonCluster*>        ConstMuonClusters;
typedef const Gaudi::Range_<MuonClusters>      MuonClusterRange;
typedef const Gaudi::Range_<ConstMuonClusters> ConstMuonClusterRange;
