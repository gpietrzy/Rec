/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONLOGPAD_H
#define MUONLOGPAD_H 1

/** @class MuonLogPad MuonLogPad.h
 *
 *  Muon logical pad class for standalone cosmics reconstruction
 *  @author Giacomo
 *  @date   2007-10-26
 */

#include "Detector/Muon/TileID.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonInterfaces/MuonLogHit.h"
#include <ostream>
#include <vector>

class MuonLogPad final {
public:
  /// standard constructor
  MuonLogPad() = default;
  /// constructor for uncrossed pad
  MuonLogPad( MuonLogHit* hit )
      : m_tile{hit->tile()}, m_padhits{hit, nullptr}, m_time{hit->time()}, m_intime{hit->intime()} {}

  /// constructor fot crossed pad, note that hit1 must be the strip (giving X coordinate)
  ///  and hit2 must be the pad  (giving Y coordinate)
  MuonLogPad( MuonLogHit* hit1, MuonLogHit* hit2 )
      : m_crossed{true}
      , m_tile{hit1->tile().intercept( hit2->tile() )}
      , m_padhits{hit1, hit2}
      , m_truepad{true}
      , m_intime{hit1->intime() && hit2->intime()} {
    assignTimes( hit1->time(), hit2->time() );
  }

  enum class Type { NOX = 0, XONEFE = 1, XTWOFE = 2, UNPAIRED = 3 };

  /// returns the type of logical pad (uncrossed, crossed with 1 or 2 FE channels, or unpaired log. channel)
  Type type() const {
    if ( !m_truepad ) return Type::UNPAIRED;
    if ( m_tile.station() == 0 || ( m_tile.station() > 2 && m_tile.region() == 0 ) ) {
      return Type::NOX; // single log. channel
    } else {
      if ( m_tile.station() > 0 && m_tile.station() < 3 && m_tile.region() < 2 )
        return Type::XTWOFE; // double readout for M23R12
    }
    return Type::XONEFE;
  }

  LHCb::Detector::Muon::TileID tile() const { return m_tile; }

  void assignTimes( float t1, float t2 ) {
    m_time  = 0.5 * ( t1 + t2 );
    m_dtime = 0.5 * ( t1 - t2 );
  }

  void shiftTimes( float d1, float d2 ) {
    if ( !m_padhits.second ) return;
    float t1 = ( m_padhits.first )->time() + d1;
    float t2 = ( m_padhits.second )->time() + d2;
    assignTimes( t1, t2 );
  }

  void shiftTime( float delay ) { m_time += delay; }

  /// return the pad time (averaged for crossed pads), in TDC bits
  float time() const { return m_time; }
  float timeX() const {
    switch ( type() ) {
    case Type::XONEFE:
    case Type::XTWOFE:
      return m_time + m_dtime;
    default:
      return std::numeric_limits<float>::lowest();
    }
  }
  float timeY() const {
    switch ( type() ) {
    case Type::XONEFE:
    case Type::XTWOFE:
      return m_time - m_dtime;
    default:
      return std::numeric_limits<float>::lowest();
    }
  }
  /// return the half difference of the two hit times for crossed pads, in TDC bits
  float dtime() const { return m_dtime; }
  /// true if this is the crossing of two logical channels
  bool crossed() const { return m_crossed; }
  /// true if it's a real logical pad (if false, it's an unpaired logical channel)
  bool        truepad() const { return m_truepad; }
  void        settruepad() { m_truepad = true; }
  bool        intime() const { return m_intime; }
  MuonLogHit* getHit( unsigned int i ) { return i == 0 ? m_padhits.first : i == 1 ? m_padhits.second : nullptr; }
  std::vector<MuonLogHit*> getHits() const {
    return m_padhits.second ? std::vector<MuonLogHit*>{m_padhits.first, m_padhits.second}
                            : std::vector<MuonLogHit*>( 1, m_padhits.first );
  }

private:
  bool                                m_crossed = false;
  LHCb::Detector::Muon::TileID        m_tile    = {};
  std::pair<MuonLogHit*, MuonLogHit*> m_padhits;
  float                               m_time    = 0;
  float                               m_dtime   = 0;
  bool                                m_truepad = false;
  bool                                m_intime  = false;
};

#endif // MUONLOGPAD_H
