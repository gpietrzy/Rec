/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "MuonInterfaces/MuonCluster.h"
#include <GaudiKernel/Point4DTypes.h>
#include <GaudiKernel/StatusCode.h>
#include <GaudiKernel/Vector3DTypes.h>

#include "GaudiKernel/Range.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"

#include "Detector/Muon/TileID.h"
#include "MuonDet/DeMuonDetector.h"
#include <cmath>
#include <iostream>
#include <map>
#include <ostream>
#include <vector>

/** @class MuonTrack MuonTrack.h
 *
 *  A MuonTrack is an object containing a list of MuonHits belonging to a muon
 *  track. The hits are stored in a std::map<int, MuonHit> where the int key is
 *  the ID (i.e. progressive number) of the inserted MuonHit.
 *
 *  @author Giovanni Passaleva / Giacomo Graziani
 *  @date   2007-08-21
 */

namespace NNMuonTrackContainerLocation {
  inline const std::string Default = "Muon/NNMuonTracks";
}

class NNMuonTrack final {
public:
  /// Standard constructor
  NNMuonTrack() = default;

  /// public member functions
  /// insert a hit in the track
  void insert( const int id, const MuonCluster* xyz ) { m_muonTrack.emplace( id, xyz ); };
  /// store the best hit candidates in each station
  void setBestCandidate( std::vector<const MuonCluster*> bcand ) { m_bestcand = std::move( bcand ); }
  /// retrieve the array of best candidates. Best candidates
  /// are sorted by station by construction
  std::vector<const MuonCluster*> bestCandidate() const { return m_bestcand; };
  /// set the clone flag
  void setClone() { m_isClone = true; }
  /// get the Clone flag
  bool isClone() const { return m_isClone; }
  /// return a vector with hits
  std::vector<const MuonCluster*> getHits() const;
  /// get the track span i.e. the max length in station units
  int getSpan() const;
  /// get the number of stations giving a hit to this track
  int getFiringStations() const;
  // linear chi2 fit. Track is fitted separately in XZ, YZ, TZ (assuming speed of light)
  StatusCode linFit();
  // add XTalk tracks
  StatusCode AddXTalk( const MuonClusters* trackhits, float cut = 1.5, int m_skipStation = -1 );
  // linear fit to get particle speed
  StatusCode speedFit();
  double     chi2x() const { return m_chi2x; } /// chi2/dof XZ
  double     chi2y() const { return m_chi2y; } /// chi2/dof YZ
  // slope XZ
  double sx() const { return m_sx; }
  // intercept XZ
  double bx() const { return m_bx; }
  // slope YZ
  double sy() const { return m_sy; }
  // direction (+1 forward or -1 backward)
  double sz() const;
  // intercept YZ
  double by() const { return m_by; }
  // track t0
  double t0() const { return m_t0; }
  // estimated error on time
  double sigmat() const { return m_sigmat; }
  // fitted speed (in c units)
  double speed() {
    if ( !m_speedFitted ) speedFit().ignore();
    return m_speed;
  }
  // error on fitted speed (in c units)
  double sigmaspeed() const { return m_sigmaspeed; }

  // errors on the above parameters
  double errsx() const { return m_errsx; }
  double errbx() const { return m_errbx; }
  double covbsx() const { return m_covbsx; }
  double errsy() const { return m_errsy; }
  double errby() const { return m_errby; }
  double covbsy() const { return m_covbsy; }
  double errt0() const { return m_errt0; }

  // polar angles wrt beam axis (taking into account track direction)
  double theta() const { return std::acos( sz() / std::sqrt( m_sx * m_sx + m_sy * m_sy + 1 ) ); }
  double phi() const { return std::atan2( m_sy, m_sx ); }
  // polar angles wrt vertical axis (pointing down)
  double thetaVert() const { return std::acos( -( m_sy / sz() ) / std::sqrt( m_sx * m_sx + m_sy * m_sy + 1 ) ); }
  double phiVert() const { return std::atan2( m_sx, sz() ); }

  // track extrapolation
  void extrap( double Z, Gaudi::XYZTPoint& Pos, Gaudi::XYZTPoint* PosErr = nullptr );
  // residuals
  Gaudi::XYZTPoint& residuals( const MuonCluster& hit );
  // corrected time of i-th hit (i from 0)
  double correctedTime( const MuonCluster& hit ) const;
  /// cluster size (total, and in the x/y views) associated to a given hit
  /// (only if first hit of a given station, to avoid double counting)
  int clsize( const MuonCluster* hit, int& xsize, int& ysize ) const;

private:
  double                            correctTOF( double rawT, double X, double Y, double Z ) const;
  double                            correctTime( double rawT, double X, double Y, double Z ) const;
  std::map<int, const MuonCluster*> m_muonTrack;
  std::vector<const MuonCluster*>   m_bestcand;
  bool                              m_isClone     = false;
  double                            m_chi2x       = -1.;
  double                            m_chi2y       = -1.;
  double                            m_sx          = -1.;
  double                            m_bx          = -1.;
  double                            m_sy          = -1.;
  double                            m_by          = -1.;
  double                            m_errsx       = -1.;
  double                            m_errbx       = -1.;
  double                            m_covbsx      = -1.;
  double                            m_errsy       = -1.;
  double                            m_errby       = -1.;
  double                            m_covbsy      = -1.;
  double                            m_t0          = -1.;
  double                            m_errt0       = -1.;
  double                            m_sigmat      = -1.;
  double                            m_speed       = 0.;
  double                            m_sigmaspeed  = 0.;
  bool                              m_speedFitted = false;
  Gaudi::XYZTPoint                  m_point;
};

using NNMuonTracks          = std::vector<NNMuonTrack, LHCb::Allocators::EventLocal<NNMuonTrack>>;
using ConstNNMuonTracks     = std::vector<const NNMuonTrack*>;
using NNMuonTrackRange      = const Gaudi::Range_<NNMuonTracks>;
using ConstNNMuonTrackRange = const Gaudi::Range_<ConstNNMuonTracks>;
