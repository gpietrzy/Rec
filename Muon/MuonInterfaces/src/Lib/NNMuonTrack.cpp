/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/FloatComparison.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "MuonInterfaces/MuonPad.h"
#include "MuonInterfaces/NNMuonTrack.h"
#include <bitset>

namespace NNMuonTrackRec {
  double muspeed    = 468.42;                   // speed of light in mm/TDC units
  double Zref       = 17670 * Gaudi::Units::mm; // default ref is M4
  bool   PhysTiming = false;
  bool   IsCosmic   = false;
  bool   IsPhysics  = false;
} // namespace NNMuonTrackRec

std::vector<const MuonCluster*> NNMuonTrack::getHits() const {
  std::vector<const MuonCluster*> hits;
  hits.reserve( m_muonTrack.size() );
  std::transform( m_muonTrack.begin(), m_muonTrack.end(), std::back_inserter( hits ),
                  []( const std::pair<const int, const MuonCluster*>& p ) { return p.second; } );
  return hits;
}
///
int NNMuonTrack::getSpan() const {
  int smax = -9999;
  int smin = 9999;
  for ( const auto& mt : m_muonTrack ) {
    int s = mt.second->station();
    if ( s > smax ) smax = s;
    if ( s < smin ) smin = s;
  }
  return smax - smin;
}
///
int NNMuonTrack::getFiringStations() const {
  std::bitset<4> stations; // was 5
  for ( const auto& v : m_muonTrack ) stations.set( v.second->station() );
  return stations.count();
}

StatusCode NNMuonTrack::speedFit() {
  // first extract the hits
  double dof = m_muonTrack.size() - 2.;
  if ( dof < 0 ) return StatusCode::FAILURE;

  double tc11 = 0, tc12 = 0, tc22 = 0, tv1 = 0, tv2 = 0, ttt = 0;
  for ( const auto& tk : m_muonTrack ) {
    const MuonCluster* hit = tk.second;
    auto               xp  = hit->X() - m_bx;
    auto               yp  = hit->Y() - m_by;
    auto               z   = hit->Z();
    auto               d   = std::sqrt( xp * xp + yp * yp + z * z );
    auto               t   = correctedTime( *hit );

    tc11 += d * d;
    tc12 += d;
    tc22 += 1.0;
    tv1 += d * t;
    tv2 += t;
    ttt += t * t;
  }
  auto tdet = tc11 * tc22 - tc12 * tc12;
  if ( tdet == 0 ) return StatusCode::FAILURE;
  double invSpeed       = ( tv1 * tc22 - tv2 * tc12 ) / tdet;
  double sigma_invSpeed = std::sqrt( tc22 / tdet );
  double t0             = ( tv2 * tc11 - tv1 * tc12 ) / tdet;

  double sumResq = ( ttt + invSpeed * invSpeed * tc11 + t0 * t0 * tc22 - 2. * invSpeed * tv1 - 2. * t0 * tv2 +
                     2. * invSpeed * t0 * tc12 );
  sigma_invSpeed *= std::sqrt( sumResq / dof );
  m_speed      = 1. / invSpeed;
  m_sigmaspeed = m_speed * m_speed * sigma_invSpeed;
  // convert to c units
  m_speed /= NNMuonTrackRec::muspeed;
  m_sigmaspeed /= NNMuonTrackRec::muspeed;
  m_speedFitted = true;
  return StatusCode::SUCCESS;
}

double NNMuonTrack::correctTOF( double rawT, double X, double Y, double Z ) const {
  return rawT + ( std::sqrt( X * X + Y * Y + Z * Z ) - NNMuonTrackRec::Zref ) / NNMuonTrackRec::muspeed;
}

double NNMuonTrack::correctTime( double rawT, double X, double Y, double Z ) const {
  // correct for TOF applied in delays (from primary vertex)
  return NNMuonTrackRec::PhysTiming ? correctTOF( rawT, X, Y, Z ) : rawT;
}

double NNMuonTrack::correctedTime( const MuonCluster& hit ) const {
  return correctTime( hit.hitTime(), hit.X(), hit.Y(), hit.Z() );
}

/// track fitting with linear Chi^2
StatusCode NNMuonTrack::linFit() {
  // first extract the hits
  double dof = m_muonTrack.size() - 2.;
  if ( dof < 0 ) return StatusCode::FAILURE;

  double xc11, xc12, xc22, xv1, xv2, xxx;
  xc11 = xc12 = xc22 = xv1 = xv2 = xxx = 0;
  double yc11, yc12, yc22, yv1, yv2, yyy;
  yc11 = yc12 = yc22 = yv1 = yv2 = yyy = 0;

  for ( auto tk = m_muonTrack.begin(); tk != m_muonTrack.end(); tk++ ) {
    const MuonCluster* hit  = tk->second;
    auto               xerr = 2. * hit->dX();
    auto               yerr = 2. * hit->dY();

    auto x = hit->X();
    auto y = hit->Y();
    auto z = hit->Z();

    // then fit them with a min chi^2 in the 2 projections xz and yz
    // 1) XZ projection:
    xc11 += z * z / xerr / xerr;
    xc12 += z / xerr / xerr;
    xc22 += 1.0 / xerr / xerr;
    xv1 += z * x / xerr / xerr;
    xv2 += x / xerr / xerr;
    xxx += x * x / xerr / xerr;

    // 2) YZ projection:
    yc11 += z * z / yerr / yerr;
    yc12 += z / yerr / yerr;
    yc22 += 1.0 / yerr / yerr;
    yv1 += z * y / yerr / yerr;
    yv2 += y / yerr / yerr;
    yyy += y * y / yerr / yerr;
  }
  double xdet = xc11 * xc22 - xc12 * xc12;
  if ( LHCb::essentiallyZero( xdet ) ) return StatusCode::FAILURE;
  double ydet = yc11 * yc22 - yc12 * yc12;
  if ( LHCb::essentiallyZero( ydet ) ) return StatusCode::FAILURE;
  m_sx = ( xv1 * xc22 - xv2 * xc12 ) / xdet;
  m_sy = ( yv1 * yc22 - yv2 * yc12 ) / ydet;
  m_bx = ( xv2 * xc11 - xv1 * xc12 ) / xdet;
  m_by = ( yv2 * yc11 - yv1 * yc12 ) / ydet;

  m_errbx  = std::sqrt( xc11 / xdet );
  m_errsx  = std::sqrt( xc22 / xdet );
  m_covbsx = -xc12 / xdet;

  m_errby  = std::sqrt( yc11 / ydet );
  m_errsy  = std::sqrt( yc22 / ydet );
  m_covbsy = -yc12 / ydet;

  m_chi2x =
      ( xxx + m_sx * m_sx * xc11 + m_bx * m_bx * xc22 - 2. * m_sx * xv1 - 2. * m_bx * xv2 + 2 * m_sx * m_bx * xc12 ) /
      dof;
  m_chi2y =
      ( yyy + m_sy * m_sy * yc11 + m_by * m_by * yc22 - 2. * m_sy * yv1 - 2. * m_by * yv2 + 2 * m_sy * m_by * yc12 ) /
      dof;

  // 3) fit track time (assuming constant error on time and neglecting error on distance of flight)

  if ( false == NNMuonTrackRec::IsCosmic && false == NNMuonTrackRec::IsPhysics ) {
    // get particle direction from sign of fitted speed
    // (for cosmics, we can use the y slope)
    speedFit().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }

  double sumt = 0, sumd = 0, sumt2 = 0, sumd2 = 0, sumtd = 0;
  for ( auto tk = m_muonTrack.begin(); tk != m_muonTrack.end(); tk++ ) {
    const MuonCluster* hit = tk->second;
    auto               xp  = hit->X() - m_bx;
    auto               yp  = hit->Y() - m_by;
    auto               z   = hit->Z();
    auto               d   = std::sqrt( xp * xp + yp * yp + z * z ) / NNMuonTrackRec::muspeed;
    auto               t   = correctedTime( *hit );
    sumt += t;
    sumd += d;
    sumt2 += t * t;
    sumd2 += d * d;
    sumtd += t * d;
  }
  // compute t0 choosing track direction according to y slope
  m_t0     = ( sumt - sz() * sumd ) / m_muonTrack.size();
  m_sigmat = std::sqrt( ( sumt2 + sumd2 - sz() * 2. * sumtd - m_muonTrack.size() * m_t0 * m_t0 ) /
                        ( m_muonTrack.size() - 1 ) );
  m_errt0  = m_sigmat / std::sqrt( m_muonTrack.size() );
  return StatusCode::SUCCESS;
}

void NNMuonTrack::extrap( double Z, Gaudi::XYZTPoint& Pos, Gaudi::XYZTPoint* PosErr ) {

  double dX = m_sx * Z, dY = m_sy * Z;
  Pos.SetCoordinates( m_bx + dX, m_by + dY, Z,
                      m_t0 + sz() * std::sqrt( dX * dX + dY * dY + Z * Z ) / NNMuonTrackRec::muspeed );
  if ( PosErr )
    PosErr->SetCoordinates( std::sqrt( m_errbx * m_errbx + Z * Z * m_errsx * m_errsx + 2 * Z * m_covbsx ),
                            std::sqrt( m_errby * m_errby + Z * Z * m_errsy * m_errsy + 2 * Z * m_covbsy ), 0.,
                            m_errt0 );
}

Gaudi::XYZTPoint& NNMuonTrack::residuals( const MuonCluster& hit ) {
  extrap( hit.Z(), m_point );
  double t = correctedTime( hit );
  m_point.SetCoordinates( hit.X() - m_point.X(), hit.Y() - m_point.Y(), 0., t - m_point.T() );
  return m_point;
}

// Z versor giving the track direction (obtained from y slope for cosmics)
double NNMuonTrack::sz() const {
  return ( NNMuonTrackRec::IsPhysics
               ? 1.
               : ( NNMuonTrackRec::IsCosmic ? ( m_sy > 0 ? -1. : 1. ) : ( m_speed > 0 ? 1. : -1. ) ) );
}

// attach Xtalk hits (by Silvia Pozzi)
StatusCode NNMuonTrack::AddXTalk( const MuonClusters* trackhits, float cut, int m_skipStation ) {

  if ( linFit().isFailure() ) return StatusCode::FAILURE;

  // first extract the hits
  std::vector<const MuonCluster*> tt_hits; // vector containing the added MuonHits

  for ( auto ihT = trackhits->begin(); ihT != trackhits->end(); ihT++ ) { // loop on all the hits

    double z       = ihT->Z();
    double dx2_fit = std::pow( m_errsx * z, 2 ) + std::pow( m_errbx, 2 ) + 2 * m_covbsx * z;
    double dy2_fit = std::pow( m_errsy * z, 2 ) + std::pow( m_errby, 2 ) + 2 * m_covbsy * z;

    double x_fit = m_bx + m_sx * ihT->Z();
    double y_fit = m_by + m_sy * ihT->Z();

    double deltaX = std::abs( ihT->X() - x_fit ) / std::sqrt( std::pow( ihT->hitTile_dX(), 2 ) + dx2_fit );
    double deltaY = std::abs( ihT->Y() - y_fit ) / std::sqrt( std::pow( ihT->hitTile_dY(), 2 ) + dy2_fit );

    double dist = std::sqrt( std::pow( ihT->X() - x_fit, 2 ) + std::pow( ihT->Y() - y_fit, 2 ) );

    constexpr double maxRadialDistanceToFit = 50.;
    if ( ihT->station() == m_skipStation ) {
      if ( ( deltaX < cut && deltaY < cut ) || dist < maxRadialDistanceToFit ) {
        if ( std::find( tt_hits.begin(), tt_hits.end(), &*ihT ) == tt_hits.end() ) { // avoid to fill double
          tt_hits.push_back( &*ihT );
        }
      }
    } else {
      if ( deltaX < cut && deltaY < cut ) {
        for ( auto tk = m_muonTrack.begin(); tk != m_muonTrack.end(); tk++ ) { // loop on the track hits
          const MuonCluster* hit = tk->second;
          if ( hit->station() == ihT->station() && hit->hitID() != ihT->hitID() ) {
            // avoid to fill double
            if ( std::find( tt_hits.begin(), tt_hits.end(), &*ihT ) == tt_hits.end() ) { tt_hits.push_back( &*ihT ); }
          }
        } // end loop over the track hits
      }   // if (deltaX < ... && deltaY<...)
    }     // end if is not the skip station
  }       // loop on trackhits

  int idhit = 100;
  for ( auto tj = tt_hits.begin(); tj != tt_hits.end(); tj++ ) { insert( ++idhit, ( *tj ) ); }
  return StatusCode::SUCCESS;
}

int NNMuonTrack::clsize( const MuonCluster* hit, int& xsize, int& ysize ) const {
  // cluster size is computed only for the first hit of a given station, return -1 otherwise
  std::vector<const MuonCluster*> hits  = getHits();
  bool                            start = false;
  xsize = ysize = 0;
  int np        = 0;

  for ( auto itkh = hits.begin(); itkh != hits.end(); itkh++ ) {
    auto tkh = *itkh;
    if ( !start && tkh != hit && tkh->station() == hit->station() ) {
      xsize = ysize = -1;
      return -1;
    }
    if ( tkh == hit ) start = true;
    if ( start && tkh->station() == hit->station() ) {
      bool prendilax = true, prendilay = true;
      // check that this position is not already the same of a previous pad
      for ( auto iprevh = hits.begin(); iprevh < itkh; iprevh++ ) {
        auto prevh = *iprevh;
        if ( prevh->station() == tkh->station() ) {
          if ( std::abs( tkh->X() - prevh->X() ) < tkh->hitTile_dX() ) { prendilax = false; }
          if ( std::abs( tkh->Y() - prevh->Y() ) < tkh->hitTile_dY() ) { prendilay = false; }
        }
      }
      np += tkh->npads();
      if ( prendilax ) { xsize += tkh->clsizeX(); }
      if ( prendilay ) { ysize += tkh->clsizeY(); }
    }
  }
  return np;
}
