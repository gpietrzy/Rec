/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef MUONMATCH_UTILS_H
#define MUONMATCH_UTILS_H 1
// STL
#include <vector>
// LHCb
#include "Event/PrUpstreamTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/State.h"
// from MuonDAQ
#include "MuonDAQ/CommonMuonHit.h"

enum TrackType { VeryLowP, LowP, MediumP, HighP };
enum MuonChamber { M2, M3, M4, M5 };

template <MuonChamber... Chambers>
struct MuonChamberSeq {};
struct Candidate final {
  Candidate( float _slope, float _chi2ndof ) : slope{_slope}, chi2ndof{_chi2ndof} {}
  float slope;
  float chi2ndof;
};

struct State {
  using Proxy = typename LHCb::Pr::Upstream::Tracks::template proxy_type<
      SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous, LHCb::Pr::Upstream::Tracks const>;
  using VeloProxy =
      typename LHCb::Pr::Velo::Tracks::template proxy_type<SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous,
                                                           LHCb::Pr::Velo::Tracks const>;
  State( const Proxy& track, const VeloProxy& velo ) {

    const auto pos = velo.StatePos( LHCb::Event::Enum::State::Location::EndVelo );
    const auto dir = velo.StateDir( LHCb::Event::Enum::State::Location::EndVelo );

    qop = track.qOverP().cast();
    p   = std::abs( 1.f / qop );

    x  = pos.x().cast();
    y  = pos.y().cast();
    z  = pos.z().cast();
    tx = dir.x().cast();
    ty = dir.y().cast();
  }

  float p, qop, x, y, z, tx, ty;
};

struct Hit {
  /// Build a hit from a muon-hit
  Hit( const CommonMuonHit& hit )
      : x( hit.x() ), sx2( hit.dx() * hit.dx() / 3. ), y( hit.y() ), sy2( hit.dy() * hit.dy() / 3. ), z( hit.z() ) {}

  /// Build a hit extrapolating the values from a state to the given point.
  Hit( const State& state, float pz ) {
    const auto dz = pz - state.z;
    x             = state.x + dz * state.tx;
    sx2           = 0;
    y             = state.y + dz * state.ty;
    sy2           = 0;
    z             = pz;
  }

  float x, sx2, y, sy2, z;
};

using Hits = std::vector<Hit, LHCb::Allocators::EventLocal<Hit>>;

#endif
