###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonMonitors
-------------------
#]=======================================================================]

gaudi_add_module(MuonMonitors
    SOURCES
        src/MuonMonitorAlg.cpp
        src/MuonChamberMonitor.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DigiEvent
        LHCb::LinkerEvent
        LHCb::MCEvent
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::MuonDetLib
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::MuonInterfacesLib
        LHCb::LHCbAlgsLib
        LHCb::PhysEvent
        LHCb::HltEvent
        LHCb::MuonDAQLib
        Rec::TrackInterfacesLib
        Rec::TrackKernel
)
