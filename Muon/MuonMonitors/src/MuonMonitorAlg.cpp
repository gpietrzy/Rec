/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Includes
#include "Event/MuonPID.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <cmath>
#include <functional>
#include <optional>
#include <utility>

/** @class MuonMonitorAlg MuonMonitorAlg.cpp
 *
 *  The algorithm for monitoring of "MuonID" containers.
 *  The algorithm produces N histograms:
 *
 *  <ol>
 *  <li> @p MuonPID Chi2Corr                  </li>
 *  <li> @p MuonPID CatBoost                  </li>
 *  </ol>
 *
 *  @author Patrizia de Simone, Ricardo Vazquez Gomez, Erika De Lucia
 *  @date   04/28/2021
 */

namespace GA = Gaudi::Accumulators;

class MuonMonitorAlg final : public LHCb::Algorithm::Consumer<void( const LHCb::MuonPID::Range& ),
                                                              Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  /** Standard constructor */
  MuonMonitorAlg( const std::string& name, ISvcLocator* pSvcLocator );

  /** standard algorithm initialization */
  StatusCode initialize() override;

  /** Algorithm execute */
  void operator()( const LHCb::MuonPID::Range& PidMuons ) const override;

private:
  mutable GA::Histogram<1> m_muPidIsMuon{this, "muPidIsMuon", "muonPID IsMuon", {2, -0.5, 1.5}};
  mutable GA::Histogram<1> m_muPidIsMuonTight{this, "muPidIsMuonTight", "muonPID IsMuonTight", {2, -0.5, 1.5}};
  mutable GA::Histogram<1> m_muPidInAcc{this, "muPidInAcc", "muonPID InAcceptance", {2, -0.5, 1.5}};
  mutable GA::Histogram<1> m_muPidPreSelMom{this, "muPidPreSelMom", "muonPID momentum preselection", {2, -0.5, 1.5}};
  mutable GA::Histogram<1> m_muPidChi2ID{this, "muPidChi2ID", "muonPID log10(correlated chi2/ndof)", {56, -2., 5.}};
  mutable GA::Histogram<1> m_muPidCatBoostID{this, "muPidCatBoostID", "muonPID CatBoost", {100, -10., 10.}};
  mutable GA::Histogram<1> m_muPidMuLL{this, "muPidMuLL", "muonPID Mu LogLikelihood", {100, -11.5, 10.5}};
  mutable GA::Histogram<1> m_muPidBkgLL{this, "muPidBkgLL", "muonPID Bkg LogLikelihood", {100, -11.5, 10.5}};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MuonMonitorAlg )

MuonMonitorAlg::MuonMonitorAlg( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"InputLocation", ""}} {}

// =============================================================================

/// standard algorithm initialization
StatusCode MuonMonitorAlg::initialize() {
  StatusCode sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;
  setHistoTopDir( "MuonID/" );
  return StatusCode::SUCCESS;
}

// =============================================================================
// standard execution method
// =============================================================================

void MuonMonitorAlg::operator()( const LHCb::MuonPID::Range& PidMuons ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << " Producing Muon PID histo " << endmsg;

  if ( PidMuons.empty() ) {
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found empty PidMuons " << endmsg;
    return;
  }
  //
  for ( const auto& muPid : PidMuons ) {
    ++m_muPidIsMuon[(unsigned int)muPid->IsMuon()];
    ++m_muPidIsMuonTight[(unsigned int)muPid->IsMuonTight()];
    ++m_muPidInAcc[(unsigned int)muPid->InAcceptance()];
    ++m_muPidPreSelMom[(unsigned int)muPid->PreSelMomentum()];

    if ( muPid->IsMuon() == false ) continue;

    ++m_muPidChi2ID[std::log10( muPid->chi2Corr() )];
    ++m_muPidCatBoostID[muPid->muonMVA2()];
    ++m_muPidMuLL[muPid->MuonLLMu()];
    ++m_muPidBkgLL[muPid->MuonLLBg()];
  }
}
