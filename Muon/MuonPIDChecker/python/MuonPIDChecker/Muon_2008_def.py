from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
DEBUG = False
DATA = "2008"
VERSION = "def"

if DEBUG:
    print("LOADING FROM")
    print("DATA=", DATA)
    print("VERSION=", VERSION)

#-------------------------------------------------------------
#    IsMuonLoose, IsMuon, MuProb, MuProb, DLL, DLL, NShared, NShared
MonitorCutValues = [1., 1., 0.9, 0.9, 1.4, 1.4, 1., 1.]
