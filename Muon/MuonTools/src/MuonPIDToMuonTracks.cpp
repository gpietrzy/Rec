/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCb
#include "Event/MuonPID.h"
#include "Event/MuonPIDs_v2.h"
#include "Event/Track.h"

namespace LHCb {

  /**
   * Converter from MuonPIDs to MuonTracks so that alignment can work
   *
   * This has been developed as a work around after the replacement of MuonIDAlgLite by
   * MuonIDHlt2Alg, as the former one was creating a list of MuonTracks while the later
   * does not.
   */
  class MuonPIDToMuonTracks : public Algorithm::Transformer<Tracks( const MuonPIDs& )> {

  public:
    MuonPIDToMuonTracks( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"InputMuonPIDs", ""}, KeyValue{"OutputMuonTracks", ""} ) {}

    Tracks operator()( const MuonPIDs& muonPIDs ) const override {
      Tracks out;
      out.reserve( muonPIDs.size() );
      for ( auto& pid : muonPIDs ) { out.insert( const_cast<Track*>( pid->idTrack() ) ); }
      return out;
    }
  };

  DECLARE_COMPONENT_WITH_ID( MuonPIDToMuonTracks, "MuonPIDToMuonTracks" )

} // namespace LHCb
