###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Muon/MuonTrackRec
-----------------
#]=======================================================================]


gaudi_add_module(MuonTrackRec
    SOURCES
        src/component
        src/component/MuonHitsToMuonPads.cpp
        src/component/MuonPadsToMuonClusters.cpp
        src/component/MuonClustersToNNMuonTracks.cpp
    LINK
        Boost::container
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::DetDescLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::MagnetLib
        LHCb::MuonDetLib
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::MuonInterfacesLib
)
