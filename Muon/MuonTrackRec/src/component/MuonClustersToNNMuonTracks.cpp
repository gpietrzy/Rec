/*****************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/Condition.h"
#include "Event/RawBank.h"
#include "Gaudi/Accumulators.h"
#include "LHCbAlgs/Transformer.h"
#include "MuonDAQ/MuonDAQDefinitions.h"
#include "MuonDAQ/MuonHitContainer.h"
#include "MuonDet/DeMuonDetector.h"
#include "MuonDet/MuonNamespace.h"

#include "GaudiKernel/ToolHandle.h"

#include "MuonInterfaces/MuonCluster.h"
#include "MuonInterfaces/MuonPad.h"
#include <boost/numeric/conversion/cast.hpp>

#include "MuonInterfaces/NNMuonTrack.h"
#include "MuonInterfaces/Neuron.h"

#include <array>
#include <bitset>
#include <boost/container/static_vector.hpp>
#include <functional>
#include <optional>
#include <string>
#include <vector>

namespace NNMuonTrackRec {
  extern double muspeed;
  extern double Zref;
  extern bool   PhysTiming;
  extern bool   IsCosmic;
  extern bool   IsPhysics;
} // namespace NNMuonTrackRec

/**
 *  This is the muon reconstruction algorithm
 *  This just crosses the logical strips back into pads
 */
using namespace Muon::DAQ;
namespace LHCb::Muon::MuonTrackRec {

  class ClustersToNNTracks final
      : public Algorithm::Transformer<NNMuonTracks( const EventContext&, const MuonClusters&, const DeMuonDetector& ),
                                      Algorithm::Traits::usesConditions<DeMuonDetector>> {
  public:
    ClustersToNNTracks( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode   initialize() override;
    StatusCode   finalize() override;
    NNMuonTracks operator()( const EventContext&, const MuonClusters&, const DeMuonDetector& ) const override;

    void setZref( double Zref ) { NNMuonTrackRec::Zref = Zref; }
    void setPhysicsTiming( bool PhysTiming ) { NNMuonTrackRec::PhysTiming = PhysTiming; }
    void setAssumeCosmics( bool AssumeCosmics ) {
      NNMuonTrackRec::IsCosmic = AssumeCosmics;
      if ( AssumeCosmics ) NNMuonTrackRec::IsPhysics = false;
    }
    void setAssumePhysics( bool AssumePhysics ) {
      NNMuonTrackRec::IsPhysics = AssumePhysics;
      if ( AssumePhysics ) NNMuonTrackRec::IsCosmic = false;
    }

  private:
    StatusCode              muonNNetMon();
    StatusCode              trackFit( NNMuonTracks* tracks ) const;
    Gaudi::Property<float>  m_aa{this, "PosScaleFactor", 10., "head-tail weight scale factor"};
    Gaudi::Property<float>  m_bb{this, "NegScaleFactorB", 1., "head-head weight scale factor"};
    Gaudi::Property<float>  m_cc{this, "NegScaleFactorC", 1., "tail-tail weight scale factor"};
    Gaudi::Property<float>  m_slamb{this, "SlamFactorB", 6., "penalty for TT connections"};
    Gaudi::Property<float>  m_slamc{this, "SlamFactorC", 6., "penalty for HH connections"};
    Gaudi::Property<double> m_xesp1{this, "ExponentXZ", 10., "exponent for (1-sin(thxz)) weight factor"};
    Gaudi::Property<double> m_xesp2{this, "ExponentYZ", 10., "exponent for (1-sin(thyz)) weight factor"};
    Gaudi::Property<float>  m_dd{this, "Stimulus", 0., "stimulation weight term"};
    Gaudi::Property<float>  m_temp{this, "Temperature", 1., "temperature"};
    Gaudi::Property<float>  m_dsum{this, "Convergence", 1e-6, "convergence factor"};
    Gaudi::Property<float>  m_scut{this, "NeuronThreshold", 0.7, "neuron activation cut"};
    Gaudi::Property<float>  m_acut{this, "DoubleKillAngCut", 0.1, "angular cut for double length neurons killing"};
    Gaudi::Property<int>    m_span_cut{this, "TrackSpanCut", 2, "cut on span for selected tracks"};
    Gaudi::Property<int>    m_firing_cut{this, "StationFiringCut", 2, "min # of stations firing in the track"};
    Gaudi::Property<int>    m_maxNeurons{this, "MaxNeurons", 5000, "max number of possible track segments"};
    Gaudi::Property<int>    m_maxIterations{this, "MaxIterations", 100, "max number of NN iterations"};

    Gaudi::Property<int> m_skipStation{
        this, "SkipStation", -1, "station to be skipped (e.g. for eff. study): 0-4; -1 = keep all stns (default)"};
    Gaudi::Property<bool> m_allowHoles{this, "AllowHoles", true,
                                       "if <true> (default) holes of 1 station are allowed in track reconstruction"};

    Gaudi::Property<bool> m_physicsTiming{
        this, "PhysicsTiming", true,
        "if true, we assume that timing is the final one (accounting for TOF from primary vx)"};

    Gaudi::Property<bool> m_assumeCosmics{
        this, "AssumeCosmics", false, "if true (default) we assume that tracks are of cosmic origin (can be backward)"};

    Gaudi::Property<bool> m_assumePhysics{this, "AssumePhysics", true,
                                          "if true we assume that tracks have the 'right' direction (pz>0)"};

    Gaudi::Property<bool>  m_XTalk{this, "AddXTalk", true};
    Gaudi::Property<float> m_XtalkRadius{this, "XtalkRadius", 1.5};

    mutable Gaudi::Accumulators::Counter<> m_trackFitFailed{this, "nb track with Fit failure"};
  };

  DECLARE_COMPONENT_WITH_ID( ClustersToNNTracks, "MuonClustersToNNMuonTracks" )
  //=============================================================================
  // Standard constructor
  //=============================================================================
  ClustersToNNTracks::ClustersToNNTracks( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"ClusterContainerLocation", MuonClusterContainerLocation::Default},
                      KeyValue{"MuonDetectorLocation", DeMuonLocation::Default}},
                     KeyValue{"NNMuonTracksContainer", NNMuonTrackContainerLocation::Default} ) {}

  //=============================================================================
  // Initialisation
  //=============================================================================
  StatusCode ClustersToNNTracks::initialize() {
    return Transformer::initialize().andThen( [&]() {
      setPhysicsTiming( m_physicsTiming );
      setAssumeCosmics( m_assumeCosmics );
      setAssumePhysics( m_assumePhysics );
    } );
  }
  StatusCode ClustersToNNTracks::finalize() {
    return Transformer::finalize().andThen( [&]() {} );
  }
  NNMuonTracks ClustersToNNTracks::operator()( const EventContext&, const MuonClusters& muonClusters,
                                               const DeMuonDetector& det ) const {

    auto trackFitFailed = m_trackFitFailed.buffer();

    if ( msgLevel( MSG::DEBUG ) ) debug() << "\n running MuonNNetMon" << endmsg;
    unsigned int nStations = det.stations();

    boost::container::static_vector<unsigned int, 4> hit_per_station( nStations, 0 ); // was 5

    // check on maximum number of hit combinations giving a neuron

    NNMuonTracks tracks{};

    for ( const auto& coord : muonClusters ) hit_per_station[coord.station()]++;
    int ncomb = 0, nFiringS = 0;
    int minFiringS = 99;
    int maxFiringS = -99;
    for ( auto [i, nhits] : LHCb::range::enumerate( hit_per_station, 0u ) ) {
      if ( nhits > 0 ) {
        ++nFiringS;
        if ( minFiringS == 99 ) minFiringS = i;
        maxFiringS = i;
      }
      for ( unsigned j = i + 1; j < nStations && j < ( i + 3 ); j++ ) {
        if ( int( i ) != m_skipStation ) ncomb += hit_per_station[i] * hit_per_station[j];
      }
    }
    if ( nFiringS <= m_firing_cut || ( maxFiringS - minFiringS ) < m_span_cut ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "not enough firing stations to get tracks" << endmsg;
      return tracks;
    }
    if ( msgLevel( MSG::DEBUG ) )
      debug() << " max combination " << hit_per_station[0] << " " << hit_per_station[1] << " " << hit_per_station[2]
              << " " << hit_per_station[3] << " " << ncomb << endmsg;
    if ( ncomb > m_maxNeurons ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Too many hits to proceed with cosmic track finding " << ncomb << endmsg;
      return tracks;
    }

    // starts the NNet reconstruction

    // here starts the double loop over hits to build the neurons
    // Neurons are oriented segments with a tail (close to the IP) and a head
    // (far from the IP). Neurons can be connected head-head (tail-tail)
    // if they enter (exit) in (from) the same hit
    //
    // if holes are allowed (default) max neuron length is 2 else 1
    int neuronLength = m_allowHoles ? 2 : 1;
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "MAX NEURON LENGTH FOR THIS JOB IS: " << neuronLength << " Skipping Station " << m_skipStation
              << endmsg;

    std::list<Muon::Neuron*> neurons;
    int                      Nneurons = 0;
    for ( auto ihT = muonClusters.begin(); ihT != muonClusters.end(); ihT++ ) {
      MuonCluster const *head, *tail;
      // skip a station for efficiency studies
      int stT = ihT->station();
      if ( stT == m_skipStation ) continue;

      int tID = ihT->hitID(); // tail ID
      for ( auto ihH = std::next( ihT ); ihH != muonClusters.end(); ihH++ ) {

        // skip a station for efficiency studies
        int stH = ihH->station();
        if ( stH == m_skipStation ) continue;

        // cut on neuron length in terms of crossed stations

        if ( std::abs( double( ihH->station() - ihT->station() ) ) > neuronLength || ihH->station() == ihT->station() )
          continue;
        if ( ihH->station() > ihT->station() ) {
          head = &*ihH;
          tail = &*ihT;
        } else {
          head = &*ihT;
          tail = &*ihH;
        }
        int sta = nStations == 5 ? tail->station() + 1 : tail->station() + 2; // switch depends on no. of stations

        int reg = tail->region() + 1;
        int hID = head->hitID(); // head ID

        if ( m_assumePhysics ) { // tighter cuts on neurons
          double sf1, sf2, sf3, sf4;
          bool   neuron_OK = true;

          double tT  = atan2( ihT->Y(), ihT->Z() );
          double tH  = atan2( ihH->Y(), ihH->Z() );
          double pT  = atan2( ihT->X(), ihT->Z() );
          double pH  = atan2( ihH->X(), ihH->Z() );
          double dth = fabs( tT - tH );
          double dph = fabs( pT - pH );

          switch ( sta ) {
          case 1:
            sf1 = 2.0;
            sf2 = 1.5;
            sf3 = 2.0;
            sf4 = 2.0;
            switch ( reg ) {
            case 1:
              if ( dth > sf1 * 0.002 ) neuron_OK = false;
              if ( dph > sf1 * 0.0015 ) neuron_OK = false;
              break;
            case 2:
              if ( dth > sf2 * 0.004 ) neuron_OK = false;
              if ( dph > sf2 * 0.003 ) neuron_OK = false;
              break;
            case 3:
              if ( dth > sf3 * 0.008 ) neuron_OK = false;
              if ( dph > sf3 * 0.0065 ) neuron_OK = false;
              break;
            case 4:
              if ( dth > sf4 * 0.016 ) neuron_OK = false;
              if ( dph > sf4 * 0.013 ) neuron_OK = false;
              break;
            }
            break;
          case 2:
            sf1 = 3.0;
            sf2 = 3.0;
            sf3 = 6.0;
            sf4 = 3.0;
            switch ( reg ) {
            case 1:
              if ( dth > sf1 * 0.002 ) neuron_OK = false;
              if ( dph > sf1 * 0.0015 ) neuron_OK = false;
              break;
            case 2:
              if ( dth > sf2 * 0.004 ) neuron_OK = false;
              if ( dph > sf2 * 0.003 ) neuron_OK = false;
              break;
            case 3:
              if ( dth > sf3 * 0.008 ) neuron_OK = false;
              if ( dph > sf3 * 0.0065 ) neuron_OK = false;
              break;
            case 4:
              if ( dth > sf4 * 0.016 ) neuron_OK = false;
              if ( dph > sf4 * 0.013 ) neuron_OK = false;
              break;
            }
            break;
          default:
            sf1 = 5.0;
            sf2 = 3.0;
            sf3 = 2.0;
            sf4 = 2.0;
            switch ( reg ) {
            case 1:
              if ( dth > sf1 * 0.002 ) neuron_OK = false;
              if ( dph > sf1 * 0.0015 ) neuron_OK = false;
              break;
            case 2:
              if ( dth > sf2 * 0.004 ) neuron_OK = false;
              if ( dph > sf2 * 0.003 ) neuron_OK = false;
              break;
            case 3:
              if ( dth > sf3 * 0.008 ) neuron_OK = false;
              if ( dph > sf3 * 0.0065 ) neuron_OK = false;
              break;
            case 4:
              if ( dth > sf4 * 0.016 ) neuron_OK = false;
              if ( dph > sf4 * 0.013 ) neuron_OK = false;
              break;
            }
            break;
          }

          if ( !neuron_OK ) continue;
        }
        // here if everything OK. Now build the neurons.

        Nneurons++;
        // create the MuonNeuron
        Muon::Neuron* Neuron = new Muon::Neuron( head, tail, hID, tID, sta, reg );
        // store progressive neuron number for debugging
        Neuron->setNeuronID( Nneurons );
        // fill a neuron list to mainipulate them in the program
        neurons.push_back( Neuron ); // this is a std::list<MuonNeuron*>
      }
    }
    // the manipulation of neurons is made through the std::list neurons

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Created neurons: " << neurons.size() << endmsg;

    // now calculate the weights

    for ( auto nl1 = neurons.begin(); nl1 != neurons.end(); nl1++ ) {
      for ( auto nl2 = std::next( nl1 ); nl2 != neurons.end(); nl2++ ) {

        double ww = 0;
        // neurons 1 and 2 are tail-head or head-tail
        if ( ( *nl1 )->tailHead( *( *nl2 ) ) || ( *nl1 )->headTail( *( *nl2 ) ) ) {
          double thxz = ( *nl1 )->angleXZ( *( *nl2 ) );
          double thyz = ( *nl1 )->angleYZ( *( *nl2 ) );
          ww          = m_aa * ( std::pow( ( 1 - sin( thxz ) ), m_xesp1 ) * std::pow( ( 1 - sin( thyz ) ), m_xesp2 ) );

          // store neuron 2 pointer in neuron 1 and its weight
          ( *nl1 )->setWeight( &( *( *nl2 ) ), ww );
          // store neuron 1 pointer in neuron 2 and its weight
          ( *nl2 )->setWeight( &( *( *nl1 ) ), ww );
        }

        // neurons 1 and 2 are head-head
        if ( ( *nl1 )->headHead( *( *nl2 ) ) ) {
          ww = -m_slamb * m_bb;
          // store neuron 2 pointer in neuron 1 and its weight
          ( *nl1 )->setWeight( &( *( *nl2 ) ), ww );
          // store neuron 1 pointer in neuron 2 and its weight
          ( *nl2 )->setWeight( &( *( *nl1 ) ), ww );
        }
        // neurons 1 and 2 are tail-tail
        if ( ( *nl1 )->tailTail( *( *nl2 ) ) ) {

          ww = -m_slamc * m_cc;
          // store neuron 2 pointer in neuron 1 and its weight
          ( *nl1 )->setWeight( &( *( *nl2 ) ), ww );
          // store neuron 1 pointer in neuron 1 and its weight
          ( *nl2 )->setWeight( &( *( *nl1 ) ), ww );
        }
      }
    }

    if ( m_allowHoles ) {
      // kill double length neurons if there is a unit length one
      for ( auto& n : neurons ) n->killDoubleLength( m_acut );
    }

    // clean up weights. only the tail-head and the head-tail with the best
    // weight are kept
    for ( auto& n : neurons ) n->cleanupWeights();

    //
    // now starts network evolution
    //

    int    iterations = 0;
    double ssum       = 2 * m_dsum;
    while ( ssum > m_dsum ) {
      iterations++;
      ssum = 0.0;
      /// update  neuron status
      for ( auto& n : neurons ) {
        const auto& wl = n->getWeights();

        double sum =
            std::accumulate( wl.begin(), wl.end(), 0.0, []( double s, const std::pair<Muon::Neuron*, double>& p ) {
              return s + p.second * p.first->status();
            } );
        double s  = 0.5 * ( 1.0 + std::tanh( m_dd / m_temp + sum / m_temp ) ); // actual status
        double sp = n->status();                                               // previous status
        if ( sum == 0 ) s = 0;
        ssum += std::abs( s - sp ) / neurons.size();
        n->setStatus( s ); // update neuron status
      }
      if ( iterations == m_maxIterations ) break;
    }

    if ( iterations == m_maxIterations ) {

      if ( msgLevel( MSG::DEBUG ) )
        debug() << "maximum number of iterations reached" << m_maxIterations << " reached" << endmsg;
    } else {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "network convergence after " << iterations << " iterations" << endmsg;
    }

    // drop OFF neurons

    int  pip = 0;
    auto nl1 = neurons.begin();
    while ( nl1 != neurons.end() ) {
      if ( ( *nl1 )->status() <= m_scut ) {
        nl1 = neurons.erase( nl1 );
      } else {
        pip++;
        // store progressive neuron number for debugging
        ( *nl1 )->setNeuronID( pip );
        nl1++;
      }
    }
    if ( msgLevel( MSG::DEBUG ) ) debug() << "ON neurons after convergence: " << neurons.size() << endmsg;

    //
    // create the tracks
    //
    std::list<Muon::Neuron*> tmpList;
    nl1 = neurons.begin();
    tracks.reserve( neurons.size() );
    while ( ( nl1 != neurons.end() ) && ( !neurons.empty() ) ) {
      tracks.emplace_back();
      auto&                    muonTrack = tracks.back();
      std::list<Muon::Neuron*> track_neurons;
      // 1st neuron copied in temp list and deleted from primary
      tmpList.push_back( *nl1 );
      Muon::Neuron* firstNeuron = *nl1;
      neurons.pop_front();
      // now loop over all other neurons and copy in the tmp list those in contact with 1st
      // also these neurons are removed from the primary list
      while ( !tmpList.empty() ) {
        auto nl2 = neurons.begin();

        while ( ( nl2 != neurons.end() ) && ( !neurons.empty() ) ) {

          if ( ( *nl2 )->connectedTo( *firstNeuron ) ) {
            tmpList.push_back( *nl2 );
            nl2 = neurons.erase( nl2 );
          } else {
            nl2++;
          }
        }
        // the first neuron of tmp is copied into the track_neurons and removed from
        // the tmp list; then we proceed with the 1st element of the tmp list
        // and look for connected neurons that are in turn added to the tmp list

        int                hID  = tmpList.front()->headTailID().first;
        int                tID  = tmpList.front()->headTailID().second;
        const MuonCluster* head = tmpList.front()->head();
        const MuonCluster* tail = tmpList.front()->tail();

        muonTrack.insert( hID, head ); // insert the head point
        muonTrack.insert( tID, tail ); // insert the tail point

        track_neurons.push_back( tmpList.front() );
        tmpList.pop_front();
        if ( !tmpList.empty() ) firstNeuron = tmpList.front();
      }

      // here the track_neurons should be filled and we can proceed with the next remaining neuron

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "the track" << endmsg;

        auto t_hits = muonTrack.getHits();
        debug() << "track hits " << t_hits.size() << endmsg;
        for ( const auto& tk : t_hits ) { debug() << tk->X() << " " << tk->Y() << " " << tk->Z() << endmsg; }
      }

      int span = muonTrack.getSpan();

      int firedstat = muonTrack.getFiringStations();

      // fill the track vector only for those tracks where at least 3 stations are firing

      if ( span >= m_span_cut && firedstat > m_firing_cut && int( muonTrack.getHits().size() ) <= firedstat ) {

        if ( m_XTalk ) {
          StatusCode sct = muonTrack.AddXTalk( &muonClusters, m_XtalkRadius, m_skipStation );
          if ( msgLevel( MSG::DEBUG ) ) {
            if ( !sct ) debug() << "problem adding XTalk pads" << endmsg;
            debug() << " After Xtalk " << muonTrack.getHits().size() << " hits:" << endmsg;
            auto t_hits = muonTrack.getHits();
            for ( const auto& tk : t_hits ) {
              debug() << tk->X() << " " << tk->Y() << " " << tk->Z() << endmsg;
              auto const Tiles = tk->getPadTiles();
              debug() << " padTiles " << Tiles.size() << endmsg;
              for ( auto const& tile : Tiles ) debug() << tile << endmsg;
            }
          }
        }

      } else {
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Rejected track ";
          if ( int( muonTrack.getHits().size() ) > firedstat ) debug() << " (TOO MANY HITS per STATION)";
          debug() << endmsg;
        }

        tracks.pop_back();
      }

      nl1 = neurons.begin();
    }
    // here I have the list of good tracks selected by span cut.

    if ( msgLevel( MSG::DEBUG ) ) debug() << "selected tracks: " << tracks.size() << endmsg;

    // start track fitting

    StatusCode scf = trackFit( &tracks );
    if ( !scf ) {
      ++trackFitFailed;
      if ( msgLevel( MSG::DEBUG ) ) debug() << "problem in track fitting" << endmsg;
    }
    return tracks;
  }

  //=============================================================================
  //  track fitting
  //=============================================================================

  StatusCode ClustersToNNTracks::trackFit( NNMuonTracks* tracks ) const {

    for ( auto itk = tracks->begin(); itk != tracks->end(); itk++ ) {
      StatusCode tsc = itk->linFit();
      if ( !tsc ) {
        err() << "Error fitting track" << endmsg;
        return tsc;
      }

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "sx : " << itk->sx() << " bx : " << itk->bx() << endmsg;
        debug() << "sy : " << itk->sy() << " by : " << itk->by() << endmsg;
        debug() << "esx : " << itk->errsx() << " ebx : " << itk->errbx() << endmsg;
        debug() << "esy : " << itk->errsy() << " eby : " << itk->errby() << endmsg;
      }

    } // end loop on tracks

    return StatusCode::SUCCESS;
  }

} // namespace LHCb::Muon::MuonTrackRec
