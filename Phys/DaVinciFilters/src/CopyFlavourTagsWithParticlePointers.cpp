/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "LHCbAlgs/Transformer.h"

/** @brief Shallow copy an LHCb::FlavourTag::Container
 *
 */
struct CopyFlavourTagsWithParticlePointers
    : LHCb::Algorithm::Transformer<LHCb::FlavourTag::Selection( const LHCb::FlavourTag::Container& )> {

  CopyFlavourTagsWithParticlePointers( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, KeyValue{"InputFlavourTags", ""}, KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTag::Selection operator()( const LHCb::FlavourTag::Container& in ) const override {
    return LHCb::FlavourTag::Selection{in.begin(), in.end()};
  }
};

DECLARE_COMPONENT( CopyFlavourTagsWithParticlePointers )
