/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "LHCbAlgs/Transformer.h"

/** @brief All that's left of this is a shallow copy. We could as well use FilterDesktop with a passthrough filter.
 */

using out_t = LHCb::Particle::Selection;

struct CopyParticles : public LHCb::Algorithm::Transformer<out_t( const LHCb::Particle::Range& )> {
  CopyParticles( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator, {KeyValue{"InputParticles", ""}}, {KeyValue{"OutputParticles", ""}} ) {}
  out_t operator()( const LHCb::Particle::Range& particles ) const override {
    return LHCb::Particle::Selection{particles.begin(), particles.end()};
  }
};

DECLARE_COMPONENT( CopyParticles )
