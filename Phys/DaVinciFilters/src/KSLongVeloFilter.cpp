/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/FloatComparison.h"
#include "Event/Particle.h"
#include "Event/RecVertex.h"
#include "Kernel/IParticleDescendants.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/Line.h"
#include "LHCbMath/MatrixTransforms.h"
#include "LHCbMath/MatrixUtils.h"

#include <functional>
#include <string>

namespace {

  using in_pv           = LHCb::RecVertex::Range;
  using in_parts        = LHCb::Particle::Range;
  using output_t        = std::tuple<LHCb::Particle::Container>;
  using filter_output_t = std::tuple<bool, LHCb::Particle::Container>;
  using base_t          = LHCb::Algorithm::MultiTransformerFilter<output_t( in_parts const&, in_pv const& )>;

  float impactParameterSquared( const Gaudi::XYZPoint& vertex, const Gaudi::XYZPoint& point,
                                const Gaudi::XYZVector& dir ) {
    // Direction does not need to be normalised.
    const auto distance = point - vertex;
    return ( distance - dir * 1. / dir.Mag2() * ( distance.Dot( dir ) ) ).Mag2();
  }

  auto bpvAndMinImpactParameterSquared( const in_pv& pvs, const int nTracksMin, const Gaudi::XYZPoint& point,
                                        const Gaudi::XYZVector& dir ) {
    // Direction does not need to be normalised.
    return std::accumulate( pvs.begin(), pvs.end(), std::pair<LHCb::RecVertex const*, float>{nullptr, -1},
                            [&]( auto r, const auto& pv ) {
                              if ( ( pv->nDoF() + 3 ) > 2 * nTracksMin ) {
                                float ip2 = impactParameterSquared( pv->position(), point, dir );
                                if ( !r.first || ip2 < r.second ) return std::pair{pv, ip2};
                              }
                              return r;
                            } );
  }

  double transverseMomentumDir( const Gaudi::XYZVector& mom, const Gaudi::XYZVector& dir ) {
    const double dmag2 = dir.Mag2();
    if ( LHCb::essentiallyZero( dmag2 ) ) { return mom.R(); }
    const Gaudi::XYZVector perp = mom - dir * ( mom.Dot( dir ) / dmag2 );
    return perp.R();
  }

  Gaudi::XYZVector get_ip_vector( const Gaudi::XYZPoint& vertex_point, const Gaudi::XYZPoint& point,
                                  const Gaudi::XYZVector& direction ) {
    const Gaudi::Math::Line my_line( point, direction );
    return Gaudi::Math::closestPoint( vertex_point, my_line ) - vertex_point;
  }

  double get_ip_perp( const Gaudi::XYZPoint& vertex_point, const Gaudi::XYZPoint& point,
                      const Gaudi::XYZVector& direction, const Gaudi::XYZVector& momentum_vector_A,
                      const Gaudi::XYZVector& momentum_vector_B ) {
    const auto&      ipVector = get_ip_vector( vertex_point, point, direction );
    Gaudi::XYZVector perp     = momentum_vector_A.Unit().Cross( momentum_vector_B.Unit() );
    return perp.Dot( ipVector );
  }

} // namespace

class KSLongVeloFilter final : public base_t {
  constexpr static auto protonMass2 = 938.272 * 938.272 * Gaudi::Units::MeV * Gaudi::Units::MeV;
  constexpr static auto pionMass    = 139.57 * Gaudi::Units::MeV;
  constexpr static auto pionMass2   = pionMass * pionMass;
  constexpr static auto KSMass2     = 497.611 * 497.611 * Gaudi::Units::MeV * Gaudi::Units::MeV;

public:
  KSLongVeloFilter( const std::string& name, ISvcLocator* pSvc )
      : base_t( name, pSvc, {KeyValue{"InputParticle", ""}, KeyValue{"InputPVs", ""}},
                {KeyValue{"OutputParticles", ""}} ) {}

  filter_output_t operator()( const in_parts& parts, const in_pv& vertices ) const override {

    LHCb::Particle::Container out_particles;

    ++m_eventCount;
    for ( const LHCb::Particle* orig_p : parts ) {
      ++m_candidateCount;

      if ( orig_p->daughters().size() < 2 ) continue;

      const LHCb::Particle* orig_probe_particle = ( orig_p->daughters() ).at( m_probe_index );
      const LHCb::Particle* orig_tag_particle   = ( orig_p->daughters() ).at( m_tag_index );

      if ( orig_probe_particle == nullptr || orig_tag_particle == nullptr ) continue;

      if ( orig_p->endVertex() == nullptr ) continue;

      auto my_probe_momentum               = orig_probe_particle->momentum();
      auto mass_constrained_probe_momentum = orig_probe_particle->momentum();

      const float Etag   = orig_tag_particle->momentum().E();
      const float pprobe = std::sqrt( orig_probe_particle->momentum().Vect().Mag2() );
      const float ptagcostheta =
          orig_tag_particle->momentum().Vect().Dot( orig_probe_particle->momentum().Vect() ) / pprobe;
      const float DeltaMSquared = KSMass2 - pionMass2 - orig_tag_particle->momentum().M2();

      float my_new_momentum = 0.5 * ( DeltaMSquared ) / ( Etag - ptagcostheta );

      float Eprobe    = std::sqrt( 1. + ( pionMass / my_new_momentum ) * ( pionMass / my_new_momentum ) );
      my_new_momentum = DeltaMSquared / ( 2.0 * ( Etag * Eprobe - ptagcostheta ) );
      Eprobe          = std::sqrt( 1. + ( pionMass / my_new_momentum ) * ( pionMass / my_new_momentum ) );
      my_new_momentum = DeltaMSquared / ( 2.0 * ( Etag * Eprobe - ptagcostheta ) );
      Eprobe          = std::sqrt( 1. + ( pionMass / my_new_momentum ) * ( pionMass / my_new_momentum ) );
      my_new_momentum = DeltaMSquared / ( 2.0 * ( Etag * Eprobe - ptagcostheta ) );

      const float scale_MConstr = my_new_momentum / pprobe;

      mass_constrained_probe_momentum.SetPxPyPzE(
          scale_MConstr * mass_constrained_probe_momentum.x(), scale_MConstr * mass_constrained_probe_momentum.y(),
          scale_MConstr * mass_constrained_probe_momentum.z(),
          std::sqrt( pionMass2 + scale_MConstr * scale_MConstr * mass_constrained_probe_momentum.P2() ) );

      auto [bpv, ip2] = bpvAndMinImpactParameterSquared(
          vertices, 3, orig_p->endVertex()->position(),
          ( orig_p->momentum() - my_probe_momentum + mass_constrained_probe_momentum ).Vect() );
      if ( bpv == nullptr ) {
        ++m_noBPVfound;
        continue;
      }

      const double ip_perp =
          get_ip_perp( bpv->position(), orig_p->endVertex()->position(),
                       ( orig_p->momentum() - my_probe_momentum + mass_constrained_probe_momentum ).Vect(),
                       mass_constrained_probe_momentum.Vect(), orig_tag_particle->momentum().Vect() );

      Gaudi::LorentzVector PV_constrained_probe_momentum( orig_p->momentum() );

      const LHCb::VertexBase* vx         = orig_p->endVertex();
      const Gaudi::XYZVector  dir        = vx->position() - bpv->position();
      auto                    prb_p_PERP = transverseMomentumDir( orig_probe_particle->momentum().Vect(), dir );
      auto                    tag_p_perp = transverseMomentumDir( orig_tag_particle->momentum().Vect(), dir );

      if ( fabs( prb_p_PERP ) < 1 ) continue;

      if ( orig_probe_particle->proto() == nullptr ) continue;

      const auto scalePVConst = fabs( ( tag_p_perp ) / ( prb_p_PERP ) );
      PV_constrained_probe_momentum.SetPxPyPzE(
          scalePVConst * my_probe_momentum.x(), scalePVConst * my_probe_momentum.y(),
          scalePVConst * my_probe_momentum.z(),
          std::sqrt( pionMass2 + scalePVConst * scalePVConst * my_probe_momentum.P2() ) );
      Gaudi::LorentzVector PV_constrained_total_momentum =
          PV_constrained_probe_momentum + orig_tag_particle->momentum();

      // determine the p pi mass
      bool passed_reflection_veto = true;

      if ( m_apply_lambda_veto.value() ) {

        Gaudi::LorentzVector PV_constrained_proton_probe_momentum( PV_constrained_probe_momentum );
        PV_constrained_proton_probe_momentum.SetE(
            std::sqrt( protonMass2 + scalePVConst * scalePVConst * my_probe_momentum.P2() ) );
        Gaudi::LorentzVector PV_constrained_total_momentum_proton_hypothesis =
            PV_constrained_proton_probe_momentum + orig_tag_particle->momentum();

        passed_reflection_veto = PV_constrained_total_momentum_proton_hypothesis.M() > m_lambda_veto_min_mass;
      }

      if ( PV_constrained_total_momentum.M() > m_pvconstr_mass_min &&
           PV_constrained_total_momentum.M() < m_pvconstr_mass_max &&
           PV_constrained_probe_momentum.P() > m_probe_p_min && PV_constrained_probe_momentum.Pt() > m_probe_pt_min &&
           PV_constrained_probe_momentum.Pt() < m_probe_pt_max && fabs( ip_perp ) < ip_perp_max &&
           std::sqrt( ip2 ) < ip_max && passed_reflection_veto ) {

        LHCb::Particle* new_part = orig_p->clone();

        new_part->setMomentum( PV_constrained_total_momentum );

        out_particles.insert( new_part );
        ++m_newpartCounts;
      }
    }
    return {!out_particles.empty(), std::move( out_particles )};
  };

private:
  // cuts
  Gaudi::Property<float> m_probe_p_min{this, "PVConstrainedProbePMin", 6000. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_probe_pt_min{this, "PVConstrainedProbePtMin", 250. * Gaudi::Units::MeV};
  Gaudi::Property<float> m_probe_pt_max{this, "PVConstrainedProbePtMax", 100000. * Gaudi::Units::MeV};

  Gaudi::Property<float> m_pvconstr_mass_min{this, "PVConstrainedMassMin", 320 * Gaudi::Units::MeV};
  Gaudi::Property<float> m_pvconstr_mass_max{this, "PVConstrainedMassMax", 700 * Gaudi::Units::MeV};

  Gaudi::Property<float> ip_perp_max{this, "IPperpendicularMax", 2};
  Gaudi::Property<float> ip_max{this, "IPMax", 10};

  Gaudi::Property<unsigned int> m_probe_index{this, "ProbeIndex", 1};
  Gaudi::Property<unsigned int> m_tag_index{this, "TagIndex", 0};

  Gaudi::Property<bool>  m_apply_lambda_veto{this, "ApplyLambdaVeto", false};
  Gaudi::Property<float> m_lambda_veto_min_mass{this, "ReflectionVetoValue", 1140 * Gaudi::Units::MeV};

  mutable Gaudi::Accumulators::Counter<> m_eventCount{this, "Events"};
  mutable Gaudi::Accumulators::Counter<> m_candidateCount{this, "Input Particles"};
  mutable Gaudi::Accumulators::Counter<> m_newpartCounts{this, "Accepted Particles"};
  mutable Gaudi::Accumulators::Counter<> m_noBPVfound{this, "Particles without BPV"};
};

DECLARE_COMPONENT( KSLongVeloFilter )
