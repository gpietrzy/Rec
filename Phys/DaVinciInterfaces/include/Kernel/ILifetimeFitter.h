/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "Event/Vertex.h"

#include "GaudiKernel/IAlgTool.h"

#include "DetDesc/IGeometryInfo.h"

/**
 *  Interface Class for lifetime constrained vertex fit.
 *
 *  @author G. Raven
 *  @date   05/07/2002
 */
struct GAUDI_API ILifetimeFitter : extend_interfaces<IAlgTool> {

  DeclareInterfaceID( ILifetimeFitter, 3, 0 );

  /// Get lifetime
  /// inputs: Vertex corresponding to the assumed production point
  ///         LHCb::Particle itself
  /// output: resulting lifetime and error, chisq.
  ///         The lifetime is returned in nanoseconds.
  virtual StatusCode fit( LHCb::VertexBase const&, LHCb::Particle const&, double& lifetime, double& error,
                          double& chisq, IGeometryInfo const& geometry ) const = 0;
};
