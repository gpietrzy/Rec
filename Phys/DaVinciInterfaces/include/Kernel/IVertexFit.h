/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/Particle.h"
#include "Kernel/IParticleCombiner.h"
#include "Kernel/IParticleReFitter.h"

#include <string>

#include "DetDesc/IGeometryInfo.h"

/**
 *  The "concrete" vertex interface.
 *  It inherits from "basic" abstract  interfaces
 *  IParticleCombiner and IParticleReFitter
 *
 *  In includes 4 series of methods:
 *     - @c fit with creation of particle ("classical" DaVinci fit)
 *     - @c fit without creation of particle.
 *     - IParticleCombiner::combine
 *     - IParticleReFitter::reFit
 *
 *  It is expected that the method
 *  IParticleCombiner::combine is implemented throught @c fit:
 *
 *  @code
 *
 *  StatusCode combine( const LHCb::Particle::ConstVector& dauhters ,
 *                      LHCb::Vertex&          vertex   ,
 *                      LHCb::Particle&        mother   ) const
 *   {
 *      return fit( daughters , vertex, mother ) ;
 *   };
 *
 *  @endcode
 *
 *  In a similar way the
 *  IParticleReFitter::reFit methdod also
 *  needs to be implemented through @c fit :
 *
 *  @code
 *
 *  StatusCode reFit( LHCb::Particle& particle )
 *  {
 *     LHCb::Vertex* vertex = particle.endVertex() ;
 *    return fit( vertex->products().begin() ,
 *                vertex->products().end  () ,
 *                particle , *vertex       ) ;
 *  } ;
 *
 *  @endcode
 *
 *  @author Vanya BELYAEV belyaev@lapp.in2p3.fr
 *  @date   2004-12-19
 */
struct GAUDI_API IVertexFit : extend_interfaces<IParticleCombiner, IParticleReFitter> {

  /// interface machinery
  DeclareInterfaceID( IVertexFit, 4, 0 );

  /** The vertex fitting method without creation of a Particle
   *
   *  @code
   *
   *  // locate  the tool
   *  const IVertexFit* fitter = tool<IVertexFit>( ... ) ;
   *
   *  // container of particles to be fitetr into common vertex
   *  IVertexFit::LHCb::Particle::ConstVector daughters = ... ;
   *
   *  Vertex vertex ;
   *  StatusCode sc = fitter->fit( daughters , vertex )
   *  if( sc.isFailure() ) { Warning("Error in vertex fit", sc ) ; }
   *
   *  // check chi2 of common vertex fit
   *  double chi2 = vertex->chi2() ;
   *
   *  @endcode
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param vertex    (OUTPUT) result of vertex fit
   *  @param daughters (INPUT)  vector of daughter particles
   *  @return status code
   */
  virtual StatusCode fit( LHCb::Vertex& vertex, LHCb::Particle::ConstVector const& daughters,
                          IGeometryInfo const& geometry ) const = 0;

  /** the vertex fitting method without creation of a Particle,
   *  which allow to use an almost arbitrary sequence of
   *  daughter particles
   *   (E.g. following types coudl be used :
   *    - LHCb::Particle::ConstVector,
   *    - std::vector<LHCb::Particle*>
   *    - std::vector<const LHCb::Particle*>
   *    - SmartRefVector<Particle>
   *    - Particles, etc.. )
   *
   *  The elements of the sequence should be convertible to
   *  "const LHCb::Particle*"
   *
   *  @code
   *
   *  // locate  the tool
   *  const IVertexFit* fitter = tool<IVertexFit>( ... ) ;
   *
   *  // container of all selected particles:
   *  Particles* particles = get<Particle::Range>( "/Event/Phys/MyParticlesFromPV" );
   *
   *  Vertex vertex ;
   *  StatusCode sc = fitter->fit( particles->begin()  ,
   *                               particles->end()    ,
   *                               vertex              ) ;
   *  if( sc.isFailure() ) { Warning("Error in vertex fit", sc ) ; }
   *
   *  // check chi2 of common vertex fit
   *  double chi2 = vertex->chi2() ;
   *
   *  @endcode
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param begin begin-iterator for sequence of daughter particles
   *  @param end   end-iterator for sequence of daughter particles
   *  @param vertex result of vertex fit (output)
   *  @return status code
   */
  template <class DAUGHTER>
  inline StatusCode fit( LHCb::Vertex& vertex, DAUGHTER begin, DAUGHTER end, IGeometryInfo const& geometry ) const {
    return fit( vertex, LHCb::Particle::ConstVector( begin, end ), geometry );
  }

  /** Creation a vertex from two particles without
   *  creation of an output  Particle
   *
   *  @author Juan Palacios Juan.Palacios@cern.ch
   *  @date 09-05-2006
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param daughter0 first daughter particle  (input)
   *  @param daughter1 second daughter particle (input)
   *  @param vertex result of vertex fit        (output)
   *  @return status code
   *  @todo test
   */
  inline StatusCode fit( LHCb::Vertex& vertex, LHCb::Particle const& daughter0, LHCb::Particle const& daughter1,
                         IGeometryInfo const& geometry ) const {
    const LHCb::Particle::ConstVector tmp = {&daughter0, &daughter1};
    return fit( vertex, tmp, geometry );
  }

  /** Creation a vertex from three particles without
   *  creation of an output  Particle
   *
   *  @author Juan Palacios Juan.Palacios@cern.ch
   *  @date 09-05-2006
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param daughter0 first daughter particle  (input)
   *  @param daughter1 second daughter particle (input)
   *  @param daughter2 third daughter particle  (input)
   *  @param vertex result of vertex fit        (output)
   *  @return status code
   *  @todo test
   */
  inline StatusCode fit( LHCb::Vertex& vertex, LHCb::Particle const& daughter0, LHCb::Particle const& daughter1,
                         LHCb::Particle const& daughter2, IGeometryInfo const& geometry ) const {
    const LHCb::Particle::ConstVector tmp = {&daughter0, &daughter1, &daughter2};
    return fit( vertex, tmp, geometry );
  }

  /** The vertex fitting method with creation of LHCb::Particle
   *  ("classical")
   *
   *  @code
   *
   *  // locate  the tool
   *  const IVertexFit* fitter = tool<IVertexFit>( ... ) ;
   *
   *  // container of particles to be fitted into common vertex
   *  IVertexFit::LHCb::Particle::ConstVector daughters = ... ;
   *
   *  Vertex   vertex   ;
   *  LHCb::Particle particle ;
   *  StatusCode sc = fitter->fit( daughters , particle , vertex )
   *  if( sc.isFailure() ) { Warning("Error in vertex fit", sc ) ; }
   *
   *  // check chi2 of common vertex fit
   *  double chi2 = vertex->chi2() ;
   *
   *  @endcode
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param daughters vector of daughter particles  (input)
   *  @param vertex    result of vertex fit             (output)
   *  @param particle  result of vertex fit             (output)
   *  @return status code
   */
  virtual StatusCode fit( LHCb::Particle::ConstVector const& daughters, LHCb::Vertex& vertex, LHCb::Particle& particle,
                          IGeometryInfo const& geometry ) const = 0;

  /** the vertex fitting method with creation of LHCb::Particle ("classical")
   *  which allow to use almost arbotrary sequence of
   *  daughter partricles
   *  (E.g. following types could be used:
   *     - LHCb::Particle::ConstVector
   *     - std::vector<LHCb::Particle*>
   *     - std::vector<const LHCb::Particle*>
   *     - SmartRefVector<Particle>
   *     - Particles, etc.. )
   *
   *  The elements of the sequence shodul be convertible to
   *  "const LHCb::Particle*"
   *
   *  @code
   *
   *  // locate  the tool
   *  const IVertexFit* fitter = get<IVertexFit>( ... ) ;
   *
   *  // container of all selected particles:
   *  Particles* particles = get<Particle::Range>( "/Event/Phys/MyParticlesFromPV" );
   *
   *  Vertex   vertex ;
   *  LHCb::Particle particle ;
   *  StatusCode sc = fitter->fit( particles->begin()  ,
   *                               particles->end()    ,
   *                               vertex              ,
   *                               particle            ) ;
   *  if( sc.isFailure() ) { Warning("Error in vertex fit", sc ) ; }
   *
   *  @endcode
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param begin begin-iterator for sequence of daughter particles
   *  @param end   end-iterator for sequence of daughter particles
   *  @param particle result of vertex fit (output)
   *  @param vertex   result of vertex fit (output)
   *  @return status code
   */
  template <class DAUGHTER>
  inline StatusCode fit( DAUGHTER begin, DAUGHTER end, LHCb::Vertex& vertex, LHCb::Particle& particle,
                         IGeometryInfo const& geometry ) const {
    return fit( LHCb::Particle::ConstVector( begin, end ), vertex, particle, geometry );
  }

  /** Creation a Vertex and an output Particle from two Particles
   *
   *  @author Juan Palacios Juan.Palacios@cern.ch
   *  @date 09-05-2006
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param daughter0 first daughter particle  (input)
   *  @param daughter1 second daughter particle (input)
   *  @param particle result of vertex fit      (output)
   *  @param vertex result of vertex fit        (output)
   *  @return status code
   *  @todo test
   */
  inline StatusCode fit( LHCb::Particle const& daughter0, LHCb::Particle const& daughter1, LHCb::Vertex& vertex,
                         LHCb::Particle& particle, IGeometryInfo const& geometry ) const {
    const LHCb::Particle::ConstVector tmp = {&daughter0, &daughter1};
    return fit( tmp, vertex, particle, geometry );
  }

  /** Creation a Vertex and an output Particle from three Particles
   *
   *  @author Juan Palacios Juan.Palacios@cern.ch
   *  @date 09-05-2006
   *
   *  @see Particle
   *  @see Vertex
   *
   *  @param daughter0 first daughter particle  (input)
   *  @param daughter1 second daughter particle (input)
   *  @param daughter2 third daughter particle  (input)
   *  @param vertex result of vertex fit        (output)
   *  @param particle result of vertex fit      (output)
   *  @return status code
   *  @todo test
   */
  inline StatusCode fit( LHCb::Particle const& daughter0, LHCb::Particle const& daughter1,
                         LHCb::Particle const& daughter2, LHCb::Vertex& vertex, LHCb::Particle& particle,
                         IGeometryInfo const& geometry ) const {
    const LHCb::Particle::ConstVector tmp = {&daughter0, &daughter1, &daughter2};
    return fit( tmp, vertex, particle, geometry );
  }

  /** add the particle to the vertex and refit
   *
   *  @code
   *
   *  // locate  the tool
   *  const IVertexFit* fitter = get<IVertexFit>( ... ) ;
   *
   *  LHCb::Vertex*   vertex  = ... ;
   *  LHCb::Particle* oneMore = ... ;
   *
   *  StatusCode sc = fitter->add( oneMore , vertex ) ;
   *  if( sc.isFailure() ) { Warning("Error in 'add' ", sc ) ; }
   *
   *  // check chi2 of common vertex fit
   *  double chi2 = vertex->chi2() ;
   *
   *  @endcode
   *
   *  @param particle particle to be added
   *  @param vertex   vertex to be modified
   *  @return status code
   */
  virtual StatusCode add( LHCb::Particle const* particle, LHCb::Vertex& vertex,
                          IGeometryInfo const& geometry ) const = 0;

  /** remove the particle from the vertex and refit
   *
   *  @code
   *
   *  // locate  the tool
   *  const IVertexFit* fitter = get<IVertexFit>( ... ) ;
   *
   *  LHCb::Vertex*   vertex = ... ;
   *  LHCb::Particle* remove = ... ;
   *
   *  StatusCode sc = fitter->remove( remove , vertex ) ;
   *  if( sc.isFailure() ) { Warning("Error in 'remove' ", sc ) ; }
   *
   *  // check chi2 of common vertex fit
   *  double chi2 = vertex->chi2() ;
   *
   *  @endcode
   *
   *  @param particle particle to be removed
   *  @param vertex   vertex to be modified
   *  @return status code
   */
  virtual StatusCode remove( LHCb::Particle const* particle, LHCb::Vertex& vertex,
                             IGeometryInfo const& geometry ) const = 0;
};
