/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef ARROW_H
#define ARROW_H 1

#include <array>
#include <iostream>

namespace Decay {
  enum struct Arrow {
    Unknown = 0,
    Single,      /// single arrow        "->"
    LongSingle,  /// long single arrow  "-->"
    Double,      /// double arrow        "=>"
    LongDouble,  /// long double arrow  "==>"
    SingleX,     /// single arrow       "-x>"
    LongSingleX, /// long single arrow "--x>"
    DoubleX,     /// double arrow       "=x>"
    LongDoubleX  /// long double arrow "==x>"
  };

  static constexpr auto ValidArrows =
      std::array{Arrow::Single,  Arrow::LongSingle,  Arrow::Double,  Arrow::LongDouble,
                 Arrow::SingleX, Arrow::LongSingleX, Arrow::DoubleX, Arrow::LongDoubleX};

  /// string representation of the arrow
  inline std::string toString( Arrow arrow ) {
    switch ( arrow ) {
    case Arrow::Unknown:
      return "Unknown";
    case Arrow::Single:
      return "->";
    case Arrow::LongSingle:
      return "-->";
    case Arrow::Double:
      return "=>";
    case Arrow::LongDouble:
      return "==>";
    case Arrow::SingleX:
      return "-x>";
    case Arrow::LongSingleX:
      return "--x>";
    case Arrow::DoubleX:
      return "=x>";
    case Arrow::LongDoubleX:
      return "==x>";
    default:
      return "None";
    }
  }

  /// operator<<
  inline std::ostream& operator<<( std::ostream& os, Arrow arrow ) { return os << toString( arrow ); }

} // namespace Decay

#endif // ARROW_H
