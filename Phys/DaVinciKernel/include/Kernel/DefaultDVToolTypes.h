/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_DEFAULTTOOLTYPES_H
#define KERNEL_DEFAULTTOOLTYPES_H 1

#include <string>

namespace DaVinci {
  namespace DefaultTools {
    static const std::string Distance         = "LoKi::DistanceCalculator:PUBLIC";
    static const std::string VertexFitter     = "LoKi::VertexFitter:PUBLIC";
    static const std::string ParticleCombiner = "LoKi::VertexFitter:PUBLIC";
    static const std::string PVRelator =
        "GenericParticle2PVRelator<_p2PVWithIPChi2, OfflineDistanceCalculatorName>/P2PVWithIPChi2:PUBLIC";
    static const std::string ParticleReFitter = "LoKi::VertexFitter:PUBLIC";
  } // namespace DefaultTools
} // namespace DaVinci

#endif // KERNEL_DEFAULTTOOLTYPES_H
