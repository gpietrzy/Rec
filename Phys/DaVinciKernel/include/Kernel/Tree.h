/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#ifndef DECAYTREE_H
#define DECAYTREE_H 1

#include <algorithm>
#include <boost/regex.hpp>
#include <iostream>
#include <regex>
#include <sstream>
#include <stack>
#include <tuple>
#include <utility>
#include <vector>

#include "Event/Particle.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/Arrow.h"
#include "Kernel/Node.h"

namespace Decay {
  class Tree;
} // namespace Decay

// forward declare for detail namespace
namespace detail {
  struct classprops_t {
    Decay::Node              headNode;
    Decay::Arrow             first_arrow;
    std::vector<Decay::Tree> daughters;
    bool                     hasCC;
    bool                     isMarked;
  };
  struct arrowprops_t {
    Decay::Arrow first_arrow;
    unsigned int n_first_arrow;
    size_t       first_arrow_pos;
    unsigned int n_arrowtypes;
  };
  using tuple_parentdaughter_t = std::tuple<std::string, std::string>;
  using vec_str_t              = std::vector<std::string>;
  using vec_dt_t               = std::vector<Decay::Tree>;
  using vec_vec_dt_t           = std::vector<vec_dt_t>;
  template <class PARTICLE>
  using vec_ptr_t = std::vector<const PARTICLE*>;

  classprops_t parseDescriptor( std::string descriptor );
  vec_vec_dt_t cartesianProduct( const vec_vec_dt_t& vec_vec_dt );
} // namespace detail

namespace Decay {
  class Tree {
  public:
    /// constructors
    Tree();
    Tree( const std::string& descriptor );
    Tree( detail::classprops_t );
    Tree( Node );
    /// TODO: Could sort daughter subtree descriptors by their depth (also applies to constructor Tree(particle) )
    Tree( Node headNode, Arrow arrow, detail::vec_dt_t daughters, bool hasCC, bool isMarked );
    Tree( detail::ptr_of_ptrv1 particle );     // Particle constructor
    Tree( detail::ptr_of_ptrv1mc mcparticle ); // MCParticle constructor

    /// copy constructor
    Tree( const Tree& dt );
    /// copy assignment operator
    Tree& operator=( const Tree& dt );

    /// get charge conjugated node
    Tree getChargeConjugate( bool reversehasCC = false ) const;
    /// reverse the has CC flag
    void reversehasCC() { m_hasCC = !m_hasCC; }
    /// has daughters
    bool hasDaughters() const { return !m_daughters.empty(); }
    /// get descriptor
    std::string descriptor() const;
    /// get possibilities of "simple" descriptors from "complex" descriptor
    /// e.g. "B0 -> [K+]CC pi-" gives two possibilities: [B0 -> K+ pi- , B0 -> K- pi+]
    detail::vec_dt_t getPossibilities() const;
    /// Compare current tree with the reference tree.
    /// The "simple" reference tree (i.e. wih isMarked or hasCC flags to false) could have been created with
    /// reconstructed particle. Note the reference tree is non-const, since the isMarked property from current tree is
    /// transferred to the reference tree when there is match.
    /// TODO: remove in current implementation
    bool compareWithRefTree( Tree& ref_dt ) const;

    /* getters */
    /// get node
    Node headNode() const { return m_headNode; }
    /// get const reference to the daughters
    const detail::vec_dt_t& daughters() const { return m_daughters; }
    /// get hasCC
    bool hasCC() const { return m_hasCC; }
    /// get isMarked
    bool isMarked() const { return m_isMarked; }
    /// get has matched
    bool hasMatched() const;
    /// get arrow
    Arrow arrow() const { return m_Arrow; }

    /* setters */
    /// set headNode
    void setheadNode( Node headNode ) { m_headNode = headNode; }
    /// set daughters
    void setdaughters( detail::vec_dt_t daughters ) { m_daughters = daughters; }
    /// set hasCC
    void sethasCC( bool hasCC ) { m_hasCC = hasCC; }
    /// set isMarked
    void setisMarked( bool isMarked ) { m_isMarked = isMarked; }
    /// set has matched
    void sethasMatched( bool hasMatched );
    /// set arrow
    void setArrow( Arrow arrow ) { m_Arrow = arrow; }

    /// friend function for operator<<
    friend std::ostream& operator<<( std::ostream& os, const Tree& dt );
    /// friend function for operator==
    /// TODO: remove in current implementation
    friend bool operator==( const Tree& lhs, const Tree& rhs );
    /// friend function for operator!=
    /// TODO: remove in current implementation
    friend bool operator!=( const Tree& lhs, const Tree& rhs ) { return !( lhs == rhs ); }
    // friend function comparing two trees (Note: Arguments are non-const references)
    // They are non-const since the isMarked and hasMatched flags of the trees are modified.
    /// TODO: remove
    friend bool checkIfTreesMatch( Tree& lhs, Tree& rhs );

    /// DecayFinder no template implementation
    /// friend function comparing a particle and a tree note: internally makes a copy of the tree to change the
    /// m_hasMatched property of the tree to deal with identical particles
    template <class PARTICLE>
    friend bool operator==( const PARTICLE* const lhs, const Tree& rhs );
    /// friend function comparing a particle and a tree note: internally makes a copy of the tree to change the
    /// m_hasMatched property of the tree to deal with identical particles
    template <class PARTICLE>
    friend bool operator==( const Tree& lhs, const PARTICLE* const rhs );
    /// loop over the particle and the decaytree to evaluate if they match
    template <class PARTICLE>
    bool hasMatchWithParticle( const PARTICLE* const particle );
    /// finds the marked particles and fills the output vector
    template <class PARTICLE>
    void collectMarkedParticles( const PARTICLE* particle, std::vector<const PARTICLE*>& output ) const;

  private:
    /// TODO: Check that the following private functions are well documented
    /// private function that works with collectMarkedParticlesImpl
    template <class PARTICLE>
    void collectMarkedParticlesRecursive( const PARTICLE* particle, std::vector<const PARTICLE*>& output );
    /// set all the headNodes isMarked along with current and sub-trees isMarked flag
    /// TODO: remove
    void setAllisMarked( bool isMarked );
    /// Helper function to get daughter for particle and MCParticle
    template <class PARTICLE>
    auto getParticleDaughters( const PARTICLE* const particle ) const;

    /// head node
    Node m_headNode;
    /// Arrow type
    Arrow m_Arrow{Arrow::Unknown};
    /// vector of daughters
    detail::vec_dt_t m_daughters;
    /// has cc flag in decay e.g. [B0 -> K+ K-]CC
    bool m_hasCC{false};
    /// is marked flag in decay e.g. ^(B0 -> K+ K-) or ^([B0 -> K+ K-]CC)
    bool m_isMarked{false};
    /// decay tree has been matched
    std::optional<bool> m_hasMatched{std::nullopt};
    /// messaging service
    /// TODO: Need to set the verbosity of messaging service
    ServiceHandle<IMessageSvc> m_msgSvc{"MessageSvc", "MessageSvcName"};
  };

  Tree::Tree() = default;

  Tree::Tree( Node headNode ) : m_headNode{std::move( headNode )} {}

  Tree::Tree( Node headNode, Arrow arrow, detail::vec_dt_t daughters, bool hasCC, bool isMarked )
      : m_headNode{std::move( headNode )}
      , m_Arrow{arrow}
      , m_daughters{std::move( daughters )}
      , m_hasCC{hasCC}
      , m_isMarked{isMarked}
      , m_hasMatched{std::nullopt} {}

  Tree::Tree( detail::classprops_t props )
      : Tree{std::move( props.headNode ), props.first_arrow, std::move( props.daughters ), props.hasCC,
             props.isMarked} {}

  Tree::Tree( const std::string& descriptor_str ) : Tree{detail::parseDescriptor( descriptor_str )} {}

  Tree::Tree( detail::ptr_of_ptrv1 particle ) : m_headNode{Node( std::move( particle ) )}, m_Arrow{Arrow::Single} {
    // get parent particle pointer from "pointer of pointer"
    const auto* particle_ptr = *( particle.get() );
    // check if it is basic
    if ( !particle_ptr->isBasicParticle() ) {
      // if not loop over daughter particles
      for ( const auto* daug : particle_ptr->daughtersVector() ) {
        // built a decay tree for each daughter particle
        m_daughters.push_back( std::make_shared<const LHCb::Particle* const>( daug ) );
      }
    }
  }

  Tree::Tree( detail::ptr_of_ptrv1mc mcparticle )
      : m_headNode{Node( std::move( mcparticle ) )}, m_Arrow{Arrow::Single} {
    // get parent particle pointer from "pointer of pointer"
    const auto* mcparticle_ptr = *( mcparticle.get() );
    // get the good end vertex (the )
    // get MCParticle's first end-vertex that likely destroyed the particle
    const auto* goodVtx = mcparticle_ptr->goodEndVertex(); // MCVertex
    if ( goodVtx ) {
      // if not loop over daughter particles
      for ( const auto& mcdaug : goodVtx->products() ) { // SmartRef
        auto mcdaug_ptr = std::make_shared<const LHCb::MCParticle* const>( mcdaug.data() );
        // built a decay tree for each daughter particle
        m_daughters.emplace_back( mcdaug_ptr );
      }
    }
  }

  /// copy constructor
  Tree::Tree( const Tree& dt ) {
    m_headNode   = dt.m_headNode;
    m_Arrow      = dt.m_Arrow;
    m_daughters  = dt.m_daughters;
    m_hasCC      = dt.m_hasCC;
    m_isMarked   = dt.m_isMarked;
    m_hasMatched = dt.m_hasMatched;
  }

  /// copy assignment
  Tree& Tree::operator=( const Tree& dt ) {
    if ( &dt == this ) return *this;
    m_hasCC      = dt.m_hasCC;
    m_isMarked   = dt.m_isMarked;
    m_headNode   = dt.m_headNode;
    m_daughters  = dt.m_daughters;
    m_hasMatched = dt.m_hasMatched;
    m_Arrow      = dt.m_Arrow;
    return *this;
  }

  Tree Tree::getChargeConjugate( bool reversehasCC ) const { // get charge conjugated
                                                             // copy of current tree
    // create new Tree with current tree
    Tree cc_tree{*this};
    // change the headNode to the charge conjugated node (a copy with opposite pid/name)
    // It does not change the hasCC flag
    cc_tree.setheadNode( m_headNode.getChargeConjugate() );
    // change the daughters to the charge conjugated daughters
    detail::vec_dt_t cc_daughters;              // make a new vector of decay trees
    cc_daughters.reserve( m_daughters.size() ); // reserve space for the daughters
    // loop over the daughters and get the charge conjugated copy of daughters
    for ( auto& daughter : m_daughters ) cc_daughters.emplace_back( daughter.getChargeConjugate() );
    // set cc_tree daughters to cc_daughters
    cc_tree.setdaughters( cc_daughters );
    // reverse the hasCC flag of current decay tree
    if ( reversehasCC ) cc_tree.reversehasCC();
    return cc_tree;
  }

  std::string Tree::descriptor() const {
    std::stringstream ss;
    if ( !m_daughters.empty() ) { // if decay tree is not empty
      if ( m_isMarked ) ss << "^(";
      if ( m_hasCC ) ss << "[";

      ss << m_headNode << " " << m_Arrow << " ";
      unsigned int count_daug = 0;
      for ( const auto& daug : m_daughters ) {
        if ( !daug.daughters().empty() && !daug.isMarked() )
          ss << "(" << daug << ")";
        else
          ss << daug;
        if ( count_daug != m_daughters.size() - 1 ) ss << " ";
        count_daug++;
      }
    } else // if decay tree is empty
      ss << m_headNode;

    if ( m_hasCC && !m_daughters.empty() ) ss << "]CC";
    if ( m_isMarked ) ss << ")";

    return ss.str();
  }

  detail::vec_dt_t Tree::getPossibilities() const {
    // first get head node possibilities
    // e.g. "[B+]CC" -> [Node(B+), Node(B-)]
    std::vector<Node> headnode_possibilities{m_headNode.getPossibilities()};
    // if no daughters in the tree convert headnodes into decaytrees with no daughters and return it
    // e.g. "[B+]CC" -> [Tree(B+), Tree(B-)]
    detail::vec_dt_t tree_possibilities;
    if ( !hasDaughters() ) {
      for ( const auto& node : headnode_possibilities ) tree_possibilities.emplace_back( node );
      return tree_possibilities;
    }
    // if the Tree has daughters make a vector of vector of decaytrees with daughters
    // e.g. for descriptor "B0 -> [K1+]CC K2-", the daug_possibilities will be "[ [Tree(K1+), Tree(K1-)], [Tree(K2-)] ]"
    detail::vec_vec_dt_t daug_possibilities;
    for ( const auto& daug : m_daughters ) daug_possibilities.emplace_back( daug.getPossibilities() );
    // get all the possible permutations of daughter corresponding to B0
    // e.g. for descriptor "B0 -> [K1+]CC K2-" descriptor,
    // the daug_possibilities will become after cartesian product -> "[ [Tree(K1+), Tree(K2-)], [Tree(K1-), Tree(K2-)]
    // ]"
    daug_possibilities = detail::cartesianProduct( daug_possibilities );
    for ( std::size_t i = 0; i < headnode_possibilities.size(); i++ ) { // loop over head node tree possibility
      for ( std::size_t j = 0; j < daug_possibilities.size(); j++ ) {   // loop over daughter tree possibility
        // create an instance of decayTree with headnode and daughters
        bool hasCC{false}; // set hasCC false since we have "simple" decay descriptors removing hasCC
        tree_possibilities.emplace_back( headnode_possibilities[i], m_Arrow, daug_possibilities[j], hasCC, m_isMarked );
        // if the current tree hasCC to true in the decay add the conjugated tree to the possibilities
        if ( m_hasCC ) {
          Tree dt( headnode_possibilities[i], m_Arrow, daug_possibilities[j], hasCC, m_isMarked );
          tree_possibilities.emplace_back( dt.getChargeConjugate() ); // false as the DT are plain (without operators)
        }                                                             // if hasCC
      }                                                               // loop over daughter tree possibility
    }                                                                 // loop over head node tree possibility
    return tree_possibilities;
  }

  /// get has matched
  bool Tree::hasMatched() const {
    // if composite check if all daughters have matched
    if ( hasDaughters() ) { // has daughters
      for ( auto& daughter : m_daughters )
        if ( !daughter.hasMatched() ) return false; // no hasMatched in daughters
      return true;                                  // all daughters have hasMatched
    }
    // if basic return value of m_hasMatched
    return m_hasMatched.value_or( false );
  }

  void Tree::sethasMatched( bool hasMatched ) {
    // first set all daughters hasMatched flag if composite
    // the m_hasMatched flag for composite itself will be std::nullopt since its definition
    // entirely depends on if the daughters have been matched or not.
    if ( hasDaughters() )
      for ( auto& daughter : m_daughters ) daughter.sethasMatched( hasMatched );
    else
      // if basic particle, set hasMatched flag
      m_hasMatched = std::optional<bool>{hasMatched};
  }

  std::ostream& operator<<( std::ostream& os, const Tree& dt ) { return os << dt.descriptor(); }

  /////////////// implementation for DecayFinder
  template <class PARTICLE>
  bool operator==( const PARTICLE* const lhs, const Tree& rhs ) {
    if ( !lhs ) {
      throw GaudiException{"The input particle is null (nullptr). Please check!",
                           "Decay::operator==(const PARTICLE *lhs, const Decay::Tree &rhs)", StatusCode::FAILURE};
    }
    // make a copy of the decay tree
    Tree rhs_copy( rhs );
    return rhs_copy.hasMatchWithParticle( lhs );
  }

  template <class PARTICLE>
  bool operator==( const Tree& lhs, const PARTICLE* const rhs ) {
    if ( !rhs ) {
      throw GaudiException{"The input particle is null (nullptr). Please check!",
                           "Decay::operator==(const Tree& lhs, const PARTICLE* rhs)", StatusCode::FAILURE};
    }
    // make a copy of the decay tree
    Tree lhs_copy( lhs );
    return lhs_copy.hasMatchWithParticle( rhs );
  }

  template <class PARTICLE>
  auto Tree::getParticleDaughters( const PARTICLE* const particle ) const {
    if ( !particle )
      throw GaudiException{"The input particle to get daughters is null (nullptr). Please check!",
                           "Decay::Tree::getParticleDaughters(const PARTICLE *particle)", StatusCode::FAILURE};

    return particle->daughtersVector();
  }

  template <>
  auto Tree::getParticleDaughters<LHCb::MCParticle>( const LHCb::MCParticle* const particle ) const {
    if ( !particle )
      throw GaudiException{"The input MC particle to get daughters is null (nullptr). Please check!",
                           "Decay::Tree::getParticleDaughters(const PARTICLE *particle)", StatusCode::FAILURE};

    const auto* goodVtx = particle->goodEndVertex();
    if ( !goodVtx )
      return LHCb::MCParticle::ConstVector( ( goodVtx->products() ).begin(), ( goodVtx->products() ).end() );
    // goodVtx is null (could be basic particles) so just return an empty vector
    return LHCb::MCParticle::ConstVector{};
  }

  template <class PARTICLE>
  bool Tree::hasMatchWithParticle( const PARTICLE* const particle ) {
    // set messaging service
    MsgStream log( ( this->m_msgSvc ).get(), "Decay::Tree" );
    bool      is_verbose = ( log.level() == MSG::VERBOSE );
    // check for nullptr
    if ( !particle ) {
      throw GaudiException{"The input particle is null (nullptr). Please check!",
                           "Decay::Tree::hasMatchWithParticle(const PARTICLE *particle)", StatusCode::FAILURE};
    }
    // create a reference tree from the reconstructed particle (for debugging)
    auto part_ptr = std::make_shared<const PARTICLE* const>( particle );
    Tree ref_dt( part_ptr );
    // bool is_verbose = true;
    if ( is_verbose ) log << MSG::INFO << "Comparing tree: " << *this << "with particle: " << ref_dt << endmsg;
    // check if the head nodes match
    if ( m_headNode != particle ) return false;
    // head nodes have matched
    if ( is_verbose )
      log << MSG::INFO << "Head nodes are equal b/w: " << this->m_headNode << " and RHS: " << ref_dt.m_headNode
          << endmsg;
    // check if the user tree doesn't have daughters
    if ( !( this->hasDaughters() ) ) {
      this->sethasMatched( true );
      return true;
    }
    // both trees have daughters
    if ( is_verbose )
      log << MSG::INFO << "Trees: " << *this << " and particle: " << ref_dt << " both have daughters." << endmsg;
    // check if daughter size is equal
    if ( getParticleDaughters( particle ).size() != ( this->m_daughters ).size() ) return false;
    if ( is_verbose )
      log << MSG::INFO << "Trees: " << *this << " and particle: " << ref_dt << " have same daughter sizes." << endmsg;
    // both trees have equal head node and equal daughter size
    // compare the particle daughter with the tree daughter
    for ( const auto* particle_daughter : getParticleDaughters( particle ) ) {
      for ( auto& tree_daughter : this->m_daughters ) {
        // create a reference daughter tree
        auto part_daug_ptr = std::make_shared<const PARTICLE* const>( particle_daughter );
        if ( is_verbose )
          log << MSG::INFO << "Comparing tree daughter: " << tree_daughter
              << " and particle daughter: " << Tree( part_daug_ptr ) << endmsg;
        // check if tree daughter has already been matched
        if ( tree_daughter.hasMatched() ) {
          if ( is_verbose ) log << MSG::INFO << "Tree daughter has already been matched: " << tree_daughter << endmsg;
          continue;
        }
        // check if the tree daughter matches with the particle daughter
        else if ( tree_daughter.hasMatchWithParticle( particle_daughter ) ) {
          if ( is_verbose )
            log << MSG::INFO << "Match found for tree daughter: " << tree_daughter
                << " and particle daughter: " << Tree( part_daug_ptr ) << endmsg;
          break; // when there is a match
        }
        // no match has been found
        if ( is_verbose )
          log << MSG::INFO << "No match for tree daughter: " << tree_daughter
              << " and particle daughter: " << Tree( part_daug_ptr ) << endmsg;
      }
    }
    // check if all the daughters have matched
    if ( this->hasMatched() ) {
      if ( is_verbose )
        log << MSG::INFO << "Everything has matched, Tree: " << *this << " and Particle: " << ref_dt << endmsg;
      // this->sethasMatched( true );
      return true;
    } else {
      if ( is_verbose )
        log << MSG::INFO << "Not everything is matched, Tree: " << *this << " and Particle " << ref_dt << endmsg;
      // this->sethasMatched( false );
      return false;
    }
  }

  template <class PARTICLE>
  void Tree::collectMarkedParticles( const PARTICLE* particle, std::vector<const PARTICLE*>& output ) const {
    // check for nullptr
    if ( !particle ) {
      throw GaudiException{
          "The input particle is null (nullptr). Please check!",
          "Decay::Tree::collectMarkedParticlesImpl(const PARTICLE *particle, std::vector<const PARTICLE *> &output)",
          StatusCode::FAILURE};
    }
    // make a copy of the tree here
    Tree temp_tree{*this};
    // check for hat in descriptor
    if ( !std::regex_search( temp_tree.descriptor(), std::regex( "\\^" ) ) ) {
      // when there is no hat, append the head particle
      output.push_back( particle );
      return;
    }
    temp_tree.collectMarkedParticlesRecursive( particle, output );
  }

  bool Tree::compareWithRefTree( Tree& ref_dt ) const { // TODO: remove
    // make a copy of the current tree
    Tree current_dt_copy{*this};
    bool hasMatched = checkIfTreesMatch( current_dt_copy, ref_dt );
    // if there is no match set the ref_dt properties to default ones
    if ( !hasMatched ) {
      ref_dt.sethasMatched( false );
      ref_dt.setAllisMarked( false );
    }
    return hasMatched;
  }

  bool operator==( const Tree& lhs, const Tree& rhs ) { // TODO: remove
    // make a copy of the lhs and rhs
    Tree lhs_copy{lhs};
    Tree rhs_copy{rhs};
    return checkIfTreesMatch( lhs_copy, rhs_copy );
  }

  bool checkIfTreesMatch( Tree& lhs, Tree& rhs ) { // TODO: remove
    // set messaging service
    MsgStream log( lhs.m_msgSvc.get(), "Decay::Tree" );
    bool      is_verbose = ( log.level() == MSG::VERBOSE );
    // bool is_verbose = true;
    if ( is_verbose ) log << MSG::INFO << "Comparing the descriptors LHS: " << lhs << "with RHS: " << rhs << endmsg;

    // define helper lambdas
    ////////////////////////////////////////////////////////////////
    // define a helper lambda to check if there is match with CC
    auto has_match_with_cctree = [&lhs, &rhs]() -> bool {
      // The conjugate object requires hasCC flag to false or else hit infinite loop
      bool reversehasCC{true};
      // if lhs or rhs hasCC set to true check with conjugate object
      if ( lhs.m_hasCC ) {
        // Note the getChargeConjugate will return a copy of the lhs tree with conjugate properties
        return rhs == lhs.getChargeConjugate( reversehasCC );
      } else if ( rhs.m_hasCC ) {
        // Note the getChargeConjugate will return a copy of the rhs tree with conjugate properties
        return lhs == rhs.getChargeConjugate( reversehasCC );
      }             // cases where both have hasCC true should be covered above
      return false; // if both lhs and rhs do not hasCC to false return false
    };

    // define a helper lambda to set matching properties between lhs and rhs
    auto set_isMarked_hasMatched = [&log, &is_verbose]( Tree& lhs, Tree& rhs ) -> void {
      // set isMarked properties
      if ( lhs.isMarked() || lhs.headNode().isMarked() || rhs.isMarked() || rhs.headNode().isMarked() ) {
        if ( is_verbose ) log << MSG::INFO << "Tree/Node LHS: " << lhs << " or RHS: " << rhs << "is marked" << endmsg;
        lhs.m_headNode.setisMarked( true ); // modify the head node and no the
        rhs.m_headNode.setisMarked( true );
      }
      // set hasMatched properties
      lhs.sethasMatched( true );
      rhs.sethasMatched( true );
    };

    auto hasMarkedDaughter = []( const Tree& dt ) -> bool {
      // decaytree dt has daughters now check if one of its daughters is marked
      for ( const Tree& daughter : dt.m_daughters )
        if ( daughter.isMarked() || daughter.headNode().isMarked() ) return true;
      return false;
    };
    ////////////////////////////////////////////////////////////////

    // check if the head nodes match
    if ( lhs.m_headNode != rhs.m_headNode ) {
      // if not check if hasCC is true and match with CC tree otherwise return false
      if ( !has_match_with_cctree() ) return false;
    }

    if ( is_verbose )
      log << MSG::INFO << "Head nodes are equal b/w LHS: " << lhs.m_headNode << " and RHS: " << rhs.m_headNode
          << endmsg;

    // head nodes have matched now check if either of them has daughters
    // if either lhs or rhs has no daughters (unequal trees)
    // e.g. lhs: Jpsi  and rhs: Jpsi -> mu+ mu-.
    if ( !lhs.hasDaughters() || !rhs.hasDaughters() ) {

      // if the trees are un-equal and the user has marked a daughter send an error message and return false
      // e.g. ref. DT: B->Jpsi Phi; user DT: B->(Jpsi->mu+ ^mu-) Phi
      if ( !lhs.hasDaughters() && rhs.hasDaughters() && hasMarkedDaughter( rhs ) ) return false;
      if ( lhs.hasDaughters() && !rhs.hasDaughters() && hasMarkedDaughter( lhs ) ) return false;

      // if both lhs and rhs haven't matched before, set isMarked flag and hasMatched
      // flag appropriately and return true
      if ( !lhs.hasMatched() && !rhs.hasMatched() ) {
        if ( is_verbose )
          log << MSG::INFO << "None of the trees LHS: " << lhs << " and RHS: " << rhs << " were matched before."
              << endmsg;
        set_isMarked_hasMatched( lhs, rhs );
        return true;
      }

      // daughters have already been matched return false
      if ( is_verbose )
        log << MSG::INFO << "Trees LHS: " << lhs << " and RHS: " << rhs << " already have been matched." << endmsg;
      return false;
    }

    if ( is_verbose )
      log << MSG::INFO << "Trees LHS: " << lhs << " and RHS: " << rhs << " both have daughters." << endmsg;

    // both lhs and rhs have daughters, if daughter size doesn't match return false
    if ( lhs.m_daughters.size() != rhs.m_daughters.size() ) return false;
    if ( is_verbose )
      log << MSG::INFO << "Trees LHS: " << lhs << " and RHS: " << rhs << " have same daughter sizes." << endmsg;

    // now compare daughters (loops over all daughters setting hasMatched flag to true if matched)
    for ( Tree& daug_lhs : lhs.m_daughters ) {
      for ( Tree& daug_rhs : rhs.m_daughters ) {
        if ( is_verbose )
          log << MSG::INFO << "Comparing daughters of trees LHS: " << daug_lhs << " and RHS: " << daug_rhs << endmsg;
        // checks if daughters are not equal OR if daughters have already been
        // matched: in both cases continue
        if ( ( daug_lhs != daug_rhs ) || ( daug_lhs.hasMatched() && daug_rhs.hasMatched() ) ) {
          if ( is_verbose )
            log << MSG::INFO << "No match found or daughters have already been matched for LHS: " << daug_lhs
                << " and RHS: " << daug_rhs << endmsg;
          continue;
        } else { // match was found and daughters have not previously been matched
                 // set isMarked flag and hasMatched flag appropriately
          if ( is_verbose )
            log << MSG::INFO << "Match found for daughters LHS: " << daug_lhs << " and RHS: " << daug_rhs << endmsg;
          // will have to transfer the marked properties after the match
          checkIfTreesMatch( daug_lhs, daug_rhs );
          set_isMarked_hasMatched( daug_lhs, daug_rhs );
          break; // if match break
        }
      }
    }
    // check if all daughters have been matched
    // require here that both lhs and rhs have matched daughters
    if ( !lhs.hasMatched() && !rhs.hasMatched() ) {
      if ( is_verbose )
        log << MSG::INFO << "Not everything is matched, trying with CC if hasCC is true, LHS: " << lhs
            << " and RHS: " << rhs << endmsg;
      // if not check if hasCC is true and match with CC tree otherwise return false
      if ( !has_match_with_cctree() ) return false;
    }
    if ( is_verbose ) log << MSG::INFO << "Everything has matched, LHS: " << lhs << " and RHS: " << rhs << endmsg;

    // everything has matched return true
    // set isMarked flag and hasMatched flag appropriately
    set_isMarked_hasMatched( lhs, rhs );
    return true;
  }

  template <class PARTICLE>
  void Tree::collectMarkedParticlesRecursive( const PARTICLE* particle, std::vector<const PARTICLE*>& output ) {
    // check for nullptr
    if ( !particle ) {
      throw GaudiException{"The input particle is null (nullptr). Please check!",
                           "Decay::Tree::collectMarkedParticlesRecursive(const PARTICLE *particle, std::vector<const "
                           "PARTICLE *> &output)",
                           StatusCode::FAILURE};
    }
    // compare the head nodes
    if ( m_headNode != particle ) return;
    // head nodes have matched
    // check if the user tree doesn't have daughters
    if ( !( this->hasDaughters() ) ) {
      // set has matched of head node w/o daughters to true (helps with identical particles)
      this->sethasMatched( true );
      // if node or tree marked push to output vector
      if ( this->isMarked() || ( this->headNode() ).isMarked() ) output.push_back( particle );
      return;
    }
    // both trees have daughters
    // if daughter size is unequal return
    if ( getParticleDaughters( particle ).size() != ( this->daughters() ).size() ) return;
    // both trees have equal head node and equal daughter size
    // if node or tree marked push to output vector
    if ( this->isMarked() || ( this->headNode() ).isMarked() ) output.push_back( particle );
    // compare the daughters
    for ( const auto* particle_daughter : getParticleDaughters( particle ) ) {
      for ( auto& tree_daughter : this->m_daughters ) {
        if ( tree_daughter.hasMatched() )
          continue; // daughter has already been matched continue (dealing with identical particles)
        else if ( tree_daughter == particle_daughter ) { // daughters are equal, find the marked particle
          tree_daughter.collectMarkedParticlesRecursive( particle_daughter, output );
          break;
        }
      } // tree daughter loop
    }   // particle daughter loop
  }

  void Tree::setAllisMarked( bool isMarked ) { // TODO: remove
    // set the current tree and head nodes isMarked flag
    m_headNode.setisMarked( isMarked );
    m_isMarked = isMarked;
    // if daughters exist then iteratively set their properties
    if ( hasDaughters() )
      for ( auto& daughter : m_daughters ) daughter.setAllisMarked( isMarked );
  }
}; // namespace Decay

namespace detail {

  /// Cartesian product between the vectors in a vector of vectors, similar to the itertools.product in Python
  /// e.g. for a vector of vectors of ints { {1, 2}, {11, 12, 13}, {21, 22} } it would return
  /// { {1, 11, 21}, {1, 11, 22}, {1, 12, 21}, {1, 12, 22}, ... {2, 13, 22} }
  vec_vec_dt_t cartesianProduct( const vec_vec_dt_t& vec_vec_dt ) {
    // TODO: Try to clean this up a little bit
    vec_vec_dt_t result = {{}}; // TODO: Try to reserve the memory
    for ( const auto& vector1 : vec_vec_dt ) {
      vec_vec_dt_t res_tmp;
      for ( const auto& vector2 : result ) {
        for ( const auto& decayTree : vector1 ) {
          res_tmp.push_back( vector2 );
          res_tmp.back().push_back( decayTree );
        }
      }
      result = std::move( res_tmp ); // moves all elements from r to result leaving r empty
    }
    return result;
  }

  /// strips initial and trailing whitespaces from descriptor
  void pruneDescriptor( std::string& descriptor ) {
    // strips initial and trailing whitespaces from a string
    std::regex expression_1( "^\\s*" ); // finds any number of whitespaces at start of string
    descriptor = std::regex_replace( descriptor, expression_1, "" ); // removes initial whitespaces
    std::regex expression_2( "\\s*$" );                              // finds any number of whitespaces at end of string
    descriptor = std::regex_replace( descriptor, expression_2, "" ); // removes trailing whitespaces
  }

  /// check that the descriptor contains balanced brackets (any type) i.e. opening brackets have corresponding
  /// closing one ((...))
  bool hasBalancedBrackets( std::string_view str ) { // function to check if brackets are
                                                     // balanced in a descriptor
    // create a stack that holds characters
    std::stack<char> stk{};
    // iterate trough the string character by character
    for ( auto i : str ) {
      // if you encounter an opening bracket add it to the stack
      if ( i == '(' || i == '[' || i == '{' ) {
        stk.push( i );
      }
      // if you encounter a closing bracket...
      else if ( i == ')' || i == ']' || i == '}' ) {
        // ...and if the stack is empty return false
        if ( stk.empty() ) return false;
        // ...otherwise compare it to the previous bracket stored in the stack
        char ch = stk.top();
        // decrement the stack such that balanced brackets lead to an empty stack
        stk.pop();
        if ( ch == '(' && i == ')' ) continue;
        if ( ch == '[' && i == ']' ) continue;
        if ( ch == '{' && i == '}' ) continue;
        // returns false if not matched
        return false;
      }
    }
    return stk.empty();
  }

  /// strips the descriptor from [...]CC and sets m_hasCC flags (invoked in constructor)
  bool stripCCFromDescriptor( std::string& descriptor ) { // function used for descriptor
                                                          // parsing
    // define a helper lambda function to check cc
    auto checkHasCC = []( const std::string& descriptor ) -> bool {
      // output true if the expression has "CC" and false otherwise
      std::regex expression( "^\\["       // square bracket at the beginning of string
                             ".*"         // any character in between
                             "\\]\\s*CC$" // square bracket and CC at end of string
      );
      return std::regex_search( descriptor, expression );
    };

    // set hasCC to false
    bool hasCC = false;

    // prune descriptor if it has CC
    pruneDescriptor( descriptor );

    // check that we have balanced square brackets then set hasCC to true and strip square brackets
    if ( checkHasCC( descriptor ) ) {
      // if "CC" then strip it from descriptor
      //  e.g. "[[X]CC -> Y1 [Y2]CC]CC" => "[X]CC -> Y1 [Y2]CC"
      std::regex  expression( "^\\["    // opening square bracket at start of string
                             "\\s*"    // any whitespaces
                             "(.*)"    // matches what is in between i.e. striped descriptor
                             "\\]"     // closing bracket
                             "\\s*CC$" // any whitespaces followed by CC at end of string
      );
      std::smatch what;
      std::regex_search( descriptor, what, expression );
      // descriptor has been strip of outer-most [..]CC, now check if the square brackets are balanced
      //  This is required for cases where the descriptor is "[X]CC -> Y1 [Y2]CC" and
      //  the checkHassCC passes with descriptor = "X]CC -> Y1 [Y2" but this is an invalid descriptor.
      if ( hasBalancedBrackets( what.str( 1 ) ) ) { // check for balanced brackets
        hasCC      = true;
        descriptor = what.str( 1 );
        pruneDescriptor( descriptor );
      } // No need to throw an error here
    }
    return hasCC;
  }

  /// strips the descriptor from the hat ^(...) and sets m_isMarked flags (invoked in Tree constructor)
  bool stripHatFromDescriptor( std::string& descriptor ) { // function used for descriptor
                                                           // parsing

    // define a helper lambda function for checking if marked
    auto checkisMarked = []( const std::string& descriptor ) {
      std::regex expression( "^\\^"    // hat at start of string
                             "\\s*\\(" // any number of space and a bracket
                             ".*"      // any characters
                             "\\)"     // closing bracket
      );
      return std::regex_search( descriptor, expression );
    };

    // define a helper lambda function to check if there is brackets
    auto hasBrackets = []( const std::string& descriptor ) {
      std::regex expression( "^\\(" // bracket at start of string
                             ".*"   // any characters
                             "\\)"  // bracket
      );
      return std::regex_search( descriptor, expression );
    };

    // set is marked to false
    bool isMarked = false;

    // prune descriptor
    pruneDescriptor( descriptor );

    // if descriptor has mark with brackets then strip mark and brackets setting isMarked
    // i.e. "^(B->K+ K-)" -> "B->K+ K-"  and isMarked = True
    if ( checkisMarked( descriptor ) ) {
      // decay is marked
      isMarked = true;
      // strips decay descriptor from hat + outside brackets
      std::regex  expression( "^\\^"    // hat and opening bracket at start of string
                             "\\s*\\(" // any whitespaces followed by opening bracket
                             "(.*)"    // matches what is in between i.e. striped descriptor
                             "\\)$"    // bracket at end of string
      );
      std::smatch what;
      std::regex_search( descriptor, what, expression );
      descriptor = what.str( 1 );
      pruneDescriptor( descriptor );
    }

    // if no mark and contains brackets then strip brackets setting isMarked to false
    // i.e. "(B->K+ K-)" -> "B->K+ K-"
    if ( hasBrackets( descriptor ) ) { // tests the matched string for brackets
      // strips decay from outside brackets only
      std::regex  expression( "^\\(\\s*" // opening bracket at start of string
                             "(.*)"     // matches what is in between i.e. striped descriptor
                             "\\s*\\)$" // bracket at end of string
      );
      std::smatch what;
      std::regex_search( descriptor, what, expression );
      descriptor = what.str( 1 );
      pruneDescriptor( descriptor );
    }

    return isMarked;
  }

  /// get the number of arrows in the descriptor of given type
  inline unsigned int numberOfArrowsOfParticularType( std::string_view descriptor, Decay::Arrow arrow ) {
    // counts the number of arrow of a given type in a descriptor
    unsigned int           count  = 0;
    std::string::size_type pos    = 0;
    auto                   target = toString( arrow );
    while ( ( pos = descriptor.find( target, pos ) ) != descriptor.npos ) {
      ++count;
      pos += target.length();
    }
    return count;
  };

  /// Check if descriptor has arrows of any type
  bool hasArrow( const std::string& descriptor ) {
    return std::any_of( Decay::ValidArrows.begin(), Decay::ValidArrows.end(), [&descriptor]( Decay::Arrow arrow ) {
      return numberOfArrowsOfParticularType( descriptor, arrow ) != 0;
    } );
  };

  vec_str_t getDaughtersFromString( const std::string& daug_str ) { // function used for
                                                                    // descriptor parsing
    // separates the daughter string into the individual daughter particles returning a vector of daughters
    vec_str_t vec_daug;
    // regex expression that matches whitespaces conditionally, see below
    boost::regex expression(
        "(?<![\\[\\s\\^])" // "(?<!...)" means negative lookbehind
                           // Lookbehind assertions specifies how to match the next character in the
                           // regex i.e "\\s+", and can be negative (above) or positive ("(?<=...)")
                           // Inside is a character sets i.e. "[...]" which matches only one of
                           // the characters in the square bracket
                           // Since it is negative, it won't match a whitespace preceded by a bracket:
                           // "\\[", by a space: "\\s" or by a hat: "\\^"
        "\\s+"             // finds at least one or more whitespaces
        "(?![\\s\\]]|CC)"  // "(?!...)" means negative lookahead
                           // Lookahead assertions specifies how to match the previous character in
                           // the regex i.e. "\\s+", and can be negative (above) or positive
                           // ("(?=...)") Inside is a character set followed by an OR condition: "|"
                           // Since it is negative, it won't match a whitespace followed by a
                           // whitespace "\\s", by a bracket "\\]" or by a "CC"
    );
    boost::sregex_token_iterator iter( daug_str.begin(), daug_str.end(), expression,
                                       -1 // the sections of the string that is not match is
                                          // selected using the regex expression as delimiter
    );
    boost::sregex_token_iterator end; // create an end-of-reg-exp marker
    for ( ; iter != end; ++iter ) vec_daug.push_back( *iter );
    return vec_daug;
  }

  /// find string within outermost brackets e.g 1->(2->3 4) gives 2->3 4
  vec_str_t findStringWithinOuterMostBrackets( const std::string& daug_str ) { // function used for
                                                                               // descriptor parsing
    vec_str_t outer_vec_daug;
    // regex expression that matches the string in the outermost brackets
    // TODO: check the documentation here is okay
    boost::regex expression( "(" // "(...)" capturing group for
                                 // "b(?:m|(?R))*e"
                                 // matches balanced patterns or nested patterns where "b" is the beginning
                                 // of the patterns, "e" the end and "m" what can occur in the middle

                             "(?:\\^\\s*\\(|\\()" // "b" = "(?:\\^\\s*\\(|\\()"
                                                  // beginning of the pattern is either a hat "\\^"
                                                  // followed by any number of whitespaces "\\s*" and a opening bracket
                                                  // "\\(" OR: ("|"), an single opening bracket "\\("

                             "(?:[^()]+|(?R))*" // "m" = "[^()]"
                                                // character set that matches a single character not in the set
                                                // the negation is indicated by the "^" inside the character set
                                                // recursion gets incremented if a character from the set in encountered
                                                // "*" means that pattern will repeat zero or more consecutive times

                             "\\)" // "e" = "\\)"
                                   // the pattern ends with a closing bracket "\\)"

                             ")" // end of the capturing group
    );
    /* simpler recursion example: "a(?R)*z" that can match the string aaazzz to understand how recursion works.
    First "a" gets matched, then the engine reaches "(?R)" which tells it to repeat the regex (ie. look for a again) at
    the current position in the string now the second "a" is matched at recursion level = 1 then engine reaches "(?R)"
    again (increments recursion) now the third "a" is matched at recursion level = 2. When the regex repeats, the next
    character in the string is "z", so match for "a" fails at recursion level = 2 which causes the "(?R)" to fail but
    from the "*" character, "(?R)" is made optional so engine continues to the end of the expression and matches the
    first "z". Now engine has reached the end of the regex but since it is two level deep in recursion, it hasn't found
    an overall match. It exits the recursion after this successful match and matches the second "z". It is still one
    level deep from which it exits after another successful match and founds the third "z". The engine in now at the end
    of the regex and at level zero of recursion this time so it returns aaazzz

    ref: https://www.regular-expressions.info/recurse.html
    */
    boost::sregex_token_iterator iter( daug_str.begin(), daug_str.end(), expression,
                                       0 ); // selects the sequence in the string that is matched
                                            // by the regex expression
    boost::sregex_token_iterator end;       // end-of-reg-exp marker
    for ( ; iter != end; ++iter ) {
      if ( !detail::hasArrow( *iter ) ) // only stores decays
        continue;

      outer_vec_daug.emplace_back( *iter );
    }
    // replace the strings with empty strings so that only daughters with no decay trees are left ie simple nodes
    std::string daug_str_new{daug_str};
    for ( const auto& daug : outer_vec_daug ) {
      std::string::size_type pos = daug_str_new.find( daug );
      daug_str_new.replace( pos, daug.length(), "" );
    }
    detail::pruneDescriptor( daug_str_new );
    // create a new expression and iterator to separate daughters from a string of daughters
    boost::regex n_expression( "(?<![\\[\\s\\^])\\s+(?![\\s\\]]|CC)" ); // same expression as in getDaughtersFromString
                                                                        // function above
    boost::sregex_token_iterator n_iter( daug_str_new.begin(), daug_str_new.end(), n_expression,
                                         -1 ); // the sections of the string that is not match is
                                               // selected using the regex expression as delimiter
    boost::sregex_token_iterator n_end;
    for ( ; n_iter != n_end; ++n_iter ) outer_vec_daug.emplace_back( *n_iter );
    return outer_vec_daug;
  }

  // get the properties of first type of arrow in the descriptor e.g. B->(Jpsi => mu+ mu-) phi where "first arrow" is
  // "->" the properties are (Instance of Arrow, number of arrows of first arrow type, position of first arrow, number
  // of different arrow types in descriptor)
  arrowprops_t getFirstArrowProps( const std::string& descriptor ) {
    // instantiate a default first arrow
    auto first_arrow = Decay::Arrow::Unknown;
    // count number of first arrow type in descriptor
    unsigned int n_first_arrow{0};
    // instantiate a default position
    std::size_t first_arrow_pos{std::string::npos};
    // count the number of arrowtypes in string
    unsigned int n_arrowtypes{0};
    for ( const auto arrow : Decay::ValidArrows ) {                    // loop through all arrow types
      if ( descriptor.find( toString( arrow ) ) != descriptor.npos ) { // found the arrow
        if ( n_arrowtypes == 0 ) {                                     // first iteration
          // store arrow, number of its occurrences and position
          first_arrow     = arrow;
          n_first_arrow   = numberOfArrowsOfParticularType( descriptor, arrow );
          first_arrow_pos = descriptor.find( toString( arrow ) );
        } else {                                                          // second type of arrow found
          if ( descriptor.find( toString( arrow ) ) < first_arrow_pos ) { // if the position is smaller than the
                                                                          // previous one
            // store arrow, number of its occurrences and position
            first_arrow     = arrow;
            n_first_arrow   = numberOfArrowsOfParticularType( descriptor, arrow );
            first_arrow_pos = descriptor.find( toString( arrow ) );
          }
        }
        n_arrowtypes++;
      }
    }
    return arrowprops_t{first_arrow, n_first_arrow, first_arrow_pos, n_arrowtypes};
  }

  /// split the descriptor into head node and daughters string according to the first arrow and first arrow length
  tuple_parentdaughter_t getParentDaughterString( const std::string& descriptor, const std::size_t& firstArrowPos,
                                                  const std::size_t& firstArrowLength ) {
    auto parent = descriptor.substr( 0, firstArrowPos );
    auto daugs  = descriptor.substr( firstArrowPos + firstArrowLength );
    pruneDescriptor( parent );
    pruneDescriptor( daugs );
    return std::tuple( std::move( parent ), std::move( daugs ) );
  };

  /// A helper function to make a tuple of class properties (used in constructor of decaytree)
  classprops_t makeClassProps( std::string& descriptor ) {
    // set hasCC and isMarked
    auto isMarked = stripHatFromDescriptor( descriptor );
    auto hasCC    = stripCCFromDescriptor( descriptor );

    // get the properties of first type of arrow in the descriptor e.g. B->(Jpsi => mu+ mu-) (phi -> K+ K-) where "first
    // arrow" is "->" the properties are (Instance of First Arrow, number of arrows of first arrow type, position of
    // first arrow, number of different arrow types in descriptor)
    const auto [first_arrow, n_first_arrow, first_arrow_pos, n_arrowtypes] = getFirstArrowProps( descriptor );
    bool hasOneArrow = ( n_first_arrow == 1 && n_arrowtypes == 1 );

    // set head node
    // get the properties of first type of arrow in the descriptor e.g. B->(Jpsi => mu+ mu-) phi where "first arrow" is
    // "->"
    const auto& [parent_str, daugs_str] =
        getParentDaughterString( descriptor, first_arrow_pos, toString( first_arrow ).size() );
    Decay::Node headNode( parent_str );

    // set daughters
    // Get daughter string according to number of arrows
    //  - Allow for simple case with one arrow e.g. B -> K+ K-. Note the descriptor
    //    "[B->K+K-]" would fail but "(B->K+K-)" would pass. TODO: allow for both?.
    //  - Also allow for complex case e.g. B -> (Jpsi -> mu+ mu-) (phi(1020) -> K+ K-).
    vec_str_t vec_daug =
        hasOneArrow ? getDaughtersFromString( daugs_str ) : findStringWithinOuterMostBrackets( daugs_str );
    vec_dt_t daughters{vec_daug.begin(), vec_daug.end()}; // make vector of daughters

    return {headNode, first_arrow, daughters, hasCC, isMarked};
  };

  /// Master function to build decay tree setting member variables with descriptor (invoked in constructor)
  classprops_t parseDescriptor( std::string descriptor ) {
    // Check if contains balanced brackets. The brackets could be "(" or "[" or "{".
    if ( !detail::hasBalancedBrackets( descriptor ) ) {
      throw GaudiException{"Descriptor does not have balanced brackets. Please check the descriptor!",
                           "Decay::Node::parseDescriptor(descriptor)", StatusCode::FAILURE};
    }
    if ( !detail::hasArrow( descriptor ) ) {   // Case1: check if no arrow in descriptor i.e. node only
      Decay::Node      headNode( descriptor ); // construct the head node
      auto             arrow_in_descriptor = Decay::Arrow::Unknown; // Arrow::Unknown
      detail::vec_dt_t daughters{};                                 // no daughters
      bool             hasCC{false}, isMarked{false};               // properties handled at the node level
      return {headNode, arrow_in_descriptor, daughters, hasCC, isMarked};
    }
    // Build decay tree according how many arrows we have (calling parseDescriptor recursively)
    return makeClassProps( descriptor );
  }
} // namespace detail
#endif // TREE_H
