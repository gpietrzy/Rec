/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "Kernel/DaVinciStringUtils.h"

//-----------------------------------------------------------------------------
// Implementation file for class : DaVinciStringUtils
//
// 2009-10-30 : Juan PALACIOS
//-----------------------------------------------------------------------------

namespace DaVinci {
  namespace StringUtils {
    //=========================================================================
    void stripParticleName( std::string& name ) {
      if ( 0 == name.find( "anti-" ) ) { name.erase( 0, 5 ); }
      std::string::size_type pos = name.find_first_of( "+-~" );
      if ( std::string::npos == pos ) { return; }
      stripParticleName( name.erase( pos, 1 ) );
    }
    //=========================================================================
    void expandLocations( std::vector<std::string>::iterator begin, std::vector<std::string>::iterator end,
                          const std::string& prefix ) {

      for ( std::vector<std::string>::iterator loc = begin; loc != end; ++loc ) {
        if ( ( *loc ).find( "/" ) == std::string::npos ) { *loc = prefix + "/" + *loc; }
      }
    }
    //=========================================================================
    void expandLocation( std::string& location, const std::string& prefix ) {
      if ( location.find( "/" ) == std::string::npos ) { location = prefix + "/" + location; }
    }
    //=========================================================================
    void removeEnding( std::string& a, const std::string& ending ) {
      std::string::size_type pos = a.rfind( ending );
      if ( pos != std::string::npos ) { a = std::string( a, 0, pos ); }
    }

  } // namespace StringUtils

} // namespace DaVinci
//=============================================================================
