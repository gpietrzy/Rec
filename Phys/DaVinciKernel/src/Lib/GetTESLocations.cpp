/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#include "GaudiKernel/IRegistry.h"
// ============================================================================
// Local
// ============================================================================
#include "Kernel/GetTESLocations.h"
// ============================================================================
/** @file
 *  Implementation file for class DaVinci::Utils::GetTESLocations
 *
 *  @date 2016-02-16
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *
 */
// ============================================================================
// destructor
// ============================================================================
DaVinci::Utils::GetTESLocations::~GetTESLocations() {
  m_mapping.clear();
  m_locations.clear();
}
// ============================================================================
// add TES-location
// ============================================================================
std::size_t DaVinci::Utils::GetTESLocations::_collect( const DataObject* o ) {
  const IRegistry* iregistry = nullptr == o ? nullptr : o->registry();
  if ( nullptr == iregistry ) { return 0; }
  //
  const std::string& n = iregistry->identifier();
  if ( m_mapping.insert( {o, n} ).second ) {
    m_locations.push_back( n );
    return 1;
  }
  return 0;
}
// ============================================================================
//  The END
// ============================================================================
