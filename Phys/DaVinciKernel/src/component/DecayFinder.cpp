/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IDecayFinder.h" // My Interface
#include "Kernel/Tree.h"

namespace Decay {
  class DecayFinder : public extends<GaudiTool, IDecayFinder> {
  public:
    // inherit standard constructors, initalize etc.
    using extends::extends;

    /// find decays for Particles with decay descriptor
    StatusCode findDecay( const std::string& decay_descriptor, const IDecayFinder::vec_ptrv1& input_parts,
                          IDecayFinder::vec_ptrv1& out_parts ) const override {
      /// get the tree possibilities
      detail::vec_dt_t vec_dt_possibilities = getTreePossibilities( decay_descriptor );
      return findDecay( vec_dt_possibilities, input_parts, out_parts );
    };

    /// find decays for MCParticles with decay descriptor
    StatusCode findDecay( const std::string& decay_descriptor, const IDecayFinder::vec_ptrv1mc& input_parts,
                          IDecayFinder::vec_ptrv1mc& out_parts ) const override {
      detail::vec_dt_t vec_dt_possibilities = getTreePossibilities( decay_descriptor );
      return findDecay( vec_dt_possibilities, input_parts, out_parts );
    };

    /// find decays for Particles with a vector of Tree possibilities
    StatusCode findDecay( const detail::vec_dt_t& vec_dt_possibilities, const IDecayFinder::vec_ptrv1& input_parts,
                          IDecayFinder::vec_ptrv1& out_parts ) const override {
      return findDecayImpl<LHCb::Particle>( vec_dt_possibilities, input_parts, out_parts );
    };

    /// find decays for MCParticles with a vector of Tree possibilities
    StatusCode findDecay( const detail::vec_dt_t& vec_dt_possibilities, const IDecayFinder::vec_ptrv1mc& input_parts,
                          IDecayFinder::vec_ptrv1mc& out_parts ) const override {
      return findDecayImpl<LHCb::MCParticle>( vec_dt_possibilities, input_parts, out_parts );
    };

    /// get parsed descriptor (for testing purposes)
    std::vector<std::string> getParsedDescriptors( const std::vector<std::string>& decay_descriptors ) const override {
      std::vector<std::string> parsed_descriptors;
      parsed_descriptors.reserve( decay_descriptors.size() );
      std::for_each( decay_descriptors.begin(), decay_descriptors.end(),
                     [&parsed_descriptors]( const std::string& desc ) {
                       parsed_descriptors.emplace_back( Tree( desc ).descriptor() );
                     } );
      return parsed_descriptors;
    }

    /// get possibilities of decays from a "complex" d descriptor (for testing purposes)
    /// e.g. "B0 -> [K+]CC pi-" gives two possibilities: [B0 -> K+ pi- , B0 -> K- pi+]
    std::vector<std::string> getDescriptorPossibilities( const std::string& decay_descriptor ) const override {
      /// get the tree possibilities
      detail::vec_dt_t vec_dt_possibilities = getTreePossibilities( decay_descriptor );
      /// make vector of descriptor strings from vector of simple decay trees
      std::vector<std::string> descriptor_possibilities;
      descriptor_possibilities.reserve( vec_dt_possibilities.size() );
      std::for_each(
          vec_dt_possibilities.begin(), vec_dt_possibilities.end(),
          [&descriptor_possibilities]( const Tree& dt ) { descriptor_possibilities.emplace_back( dt.descriptor() ); } );
      return descriptor_possibilities;
    }

    /// set tree possibilities with decay descriptor
    detail::vec_dt_t getTreePossibilities( const std::string& decay_descriptor ) const override {
      // build user defined tree
      Tree tree( decay_descriptor );
      // raise exception if tree has an arrow that is non-single
      raiseExceptionNonSingleArrow( tree );
      // print the user specified descriptor and parsed/interpreted tree. Both should be equivalent.
      this->debug() << "User specified descriptor: " << decay_descriptor << endmsg; // TODO: set it back to debug
      this->debug() << "Parsed decay descriptor: " << tree << endmsg;
      // get the possibilities of trees from a complex decay descriptor
      // e.g. "B0 -> [K+]CC pi-" gives two possibilities: [B0 -> K+ pi- , B0 -> K- pi+]
      detail::vec_dt_t possibilities = tree.getPossibilities();
      // print them out
      if ( possibilities.size() > 1 ) {
        this->debug() << "Multiple possibilities for the specified descriptor." << endmsg;
        unsigned int count_descriptor{0};
        for ( const auto& possibility : possibilities ) {
          this->debug() << "Possibility #" << count_descriptor << " is: " << possibility << endmsg;
          count_descriptor++;
        }
      }
      return possibilities;
    }

  private:
    /// Inner templated method for finding decays
    template <class PARTICLE>
    StatusCode findDecayImpl( const detail::vec_dt_t& possibilities, const IDecayFinder::vec_ptr<PARTICLE>& input_parts,
                              IDecayFinder::vec_ptr<PARTICLE>& out_parts ) const;

    /// raise an exception if the descriptor has arrow other than non-single (i.e. other than "->")
    void raiseExceptionNonSingleArrow( const Tree& tree ) const;
  }; // DecayFinder

  // findDecay NoTemplate (no pointers)
  template <class PARTICLE>
  StatusCode DecayFinder::findDecayImpl( const detail::vec_dt_t&                possibilities,
                                         const IDecayFinder::vec_ptr<PARTICLE>& input_parts,
                                         IDecayFinder::vec_ptr<PARTICLE>&       output_parts ) const {
    // first check the output_parts container is empty
    if ( !output_parts.empty() )
      throw GaudiException( "The output_parts container passed to hold marked particles is not empty. Please check!",
                            "Decay::DecayFinder::findDecay(descriptor, input_parts, output_parts)",
                            StatusCode::FAILURE );

    // loop over reconstructed particles
    for ( const PARTICLE* particle : input_parts ) {
      if ( !particle )
        throw GaudiException( "The input_parts particle is null. Please check!",
                              "Decay::DecayFinder::findDecay(descriptor, input_parts, output_parts)",
                              StatusCode::FAILURE );

      // construct a reference tree from the reconstructed particle (for debugging)
      auto part_ptr = std::make_shared<const PARTICLE* const>( particle );
      this->debug() << "Particle decay tree: " << Tree{part_ptr} << endmsg;
      // loop through the decay possibilities and check if there is match
      // between the reconstructed particle and user possibility
      for ( const auto& possibility : possibilities ) {
        this->debug() << "Looping through the possibilities: " << possibility << endmsg;
        if ( possibility == particle ) { // operator== internally creates a copy of the possibility
          // print some info if verbosity is set
          this->debug() << "Particle tree matches descriptor: " << possibility << endmsg;
          this->debug() << "Size of output_parts before matching: " << output_parts.size() << endmsg;
          // get the marked particles
          possibility.collectMarkedParticles( particle, output_parts );
          // print some info if verbosity is set
          this->debug() << "Size of output_parts after matching: " << output_parts.size() << endmsg;
          for ( const auto* out_particle : output_parts )
            this->debug() << "PID of marked particle: " << ( out_particle->particleID() ).pid() << endmsg;
          break; // move to the next particle
        } else
          continue; // if no match continue to the next possibility
      }             // Possibility loop
    }               // Particle loop
    if ( output_parts.empty() ) this->debug() << "No match has been found with the specified descriptor." << endmsg;
    // print some info if verbosity is set
    this->debug() << "Size of output_parts outside loop: " << output_parts.size() << endmsg;
    for ( const auto* out_particle : output_parts )
      this->debug() << "PID of marked particle: " << ( out_particle->particleID() ).pid() << endmsg;
    return StatusCode::SUCCESS;
  } // findDecay

  void DecayFinder::raiseExceptionNonSingleArrow( const Tree& tree ) const {
    // if basic particle w/o arrow then do nothing
    if ( tree.arrow() == Arrow::Unknown ) return;

    // if not basic and arrow type is non-single (i.e. other than "->") throw exception
    // TODO: Add support for other arrows in the future
    if ( tree.arrow() != Arrow::Single )
      throw GaudiException( "The finding of particles with arrow '" + toString( tree.arrow() ) +
                                "' is not supported by the DecayFinder yet.",
                            "Decay::DecayFinder::findDecay(possibilities, input_parts, output_parts)",
                            StatusCode::FAILURE );

    // if tree has daughters loop through the tree and call reaiseExceptionForNonSingleArrow for each daughter
    if ( tree.hasDaughters() ) {
      for ( const auto& daughter : tree.daughters() ) raiseExceptionNonSingleArrow( daughter );
    }
  }
} // namespace Decay

DECLARE_COMPONENT_WITH_ID( Decay::DecayFinder, "DecayFinder" )
