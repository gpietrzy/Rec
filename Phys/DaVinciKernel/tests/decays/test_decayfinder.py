###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
import GaudiPython
from GaudiPython.Bindings import AppMgr
from Configurables import LHCbApp, MessageSvc
from cppyy.gbl import LHCb, std, SmartRefVector, SmartRef
import PartProp.Service  #this is required for app.ppSvc() to work

#prepare application manager
MessageSvc(OutputLevel=3)  #INFO==3, VERBOSE==1
app = LHCbApp()
app.DataType = "Upgrade"
appMgr = AppMgr()
pps = appMgr.ppSvc()
#below is required for ppSvc to work for pytest
appMgr.ExtSvc += ['LHCb::ParticlePropertySvc']
toolsvc = appMgr.toolSvc()
appMgr.initialize()


#helper class for testing the decay finder
class PseudoParticle():
    '''
    PseudoParticle helper class for testing. It builds tree of particles
    Needs to be after AppMgr definition since we use particle property service from AppMgr
    '''

    def __init__(self, name, daug=None):
        '''
        name: name of the particle
        daug: list of Particle objects of daughters
        '''
        self.name = name
        self.daug = daug  #list of Particle objects of daughters
        self.ppservice = pps  #particle property service
        self.lhcb_particle = self.build_mother()  #LHCb.Particle

    def build_mother(self):
        '''
        build mother particle and its duaghters
        '''
        mother_id = self.ppservice.find(self.name).pid().pid()
        mother_part = LHCb.Particle()
        mother_part.setParticleID(LHCb.ParticleID(mother_id))
        if self.daug is not None:
            daug_parts = [daughter.lhcb_particle for daughter in self.daug]
            daug_sref_parts = [
                SmartRef(LHCb.Particle)(daug_part) for daug_part in daug_parts
            ]
            daug_srefvec = SmartRefVector(LHCb.Particle)()
            for daug_part in daug_sref_parts:
                daug_srefvec.push_back(daug_part)
            mother_part.setDaughters(daug_srefvec)
        return mother_part


@pytest.fixture()
def tool_DecayFinder_Particles():
    decoder = toolsvc.create(
        "DecayFinder", interface=GaudiPython.gbl.IDecayFinder)
    return decoder


def make_input_particles():
    #Build: "B0 -> K+ K-"
    Bd_daug = [PseudoParticle("K+"), PseudoParticle("K-")]
    Bd = PseudoParticle("B0", Bd_daug)

    #Build: "B0 -> pi+ pi-"
    Bd2_daug = [PseudoParticle("pi+"), PseudoParticle("pi-")]
    Bd2 = PseudoParticle("B0", Bd2_daug)

    #Build: "B0 -> K+ pi-"
    Bd3_daug = [PseudoParticle("K+"), PseudoParticle("pi-")]
    Bd3 = PseudoParticle("B0", Bd3_daug)

    #Build: "B+ -> K+ K+ K-"
    #make first identical K+ and set its momentum
    K1_identical = PseudoParticle("K+")
    K1_vec4_1 = GaudiPython.gbl.Gaudi.LorentzVector(2, 1, 0, 3)
    K1_identical.lhcb_particle.setMomentum(K1_vec4_1)
    #make second identical K+ and set its momentum
    K2_identical = PseudoParticle("K+")
    K2_vec4_2 = GaudiPython.gbl.Gaudi.LorentzVector(4, 2, 0, 6)
    K2_identical.lhcb_particle.setMomentum(K2_vec4_2)
    #make B+ with two identical K+
    #Order 1 (K+, K-, K+)
    Bu_ord1_daug = [K1_identical, PseudoParticle("K-"), K2_identical]
    Bu_ord1 = PseudoParticle("B+", Bu_ord1_daug)
    #Order 2 (K-, K+, K+)
    Bu_ord2_daug = [PseudoParticle("K-"), K1_identical, K2_identical]
    Bu_ord2 = PseudoParticle("B+", Bu_ord2_daug)
    #Order 3 (K+, K+, K-)
    Bu_ord3_daug = [K1_identical, K2_identical, PseudoParticle("K-")]
    Bu_ord3 = PseudoParticle("B+", Bu_ord3_daug)

    #Build: "D0 -> pi+ pi- pi+ pi-"
    #make first identical pi- and set its momentum
    pim1_identical = PseudoParticle("pi-")
    pim1_vec4_1 = GaudiPython.gbl.Gaudi.LorentzVector(2, 1, 0, 3)
    pim1_identical.lhcb_particle.setMomentum(pim1_vec4_1)
    #make second identical pi- and set its momentum
    pim2_identical = PseudoParticle("pi-")
    pim2_vec4_2 = GaudiPython.gbl.Gaudi.LorentzVector(4, 2, 0, 6)
    pim2_identical.lhcb_particle.setMomentum(pim2_vec4_2)
    #make first identical pi+ and set its momentum
    pip1_identical = PseudoParticle("pi+")
    pip1_vec4_1 = GaudiPython.gbl.Gaudi.LorentzVector(3, 2, 0, 5)
    pip1_identical.lhcb_particle.setMomentum(pip1_vec4_1)
    #make second identical K+ and set its momentum
    pip2_identical = PseudoParticle("pi+")
    pip2_vec4_2 = GaudiPython.gbl.Gaudi.LorentzVector(6, 4, 0, 10)
    pip2_identical.lhcb_particle.setMomentum(pip2_vec4_2)
    #make D0
    D0_daug = [pim1_identical, pip1_identical, pim2_identical, pip2_identical]
    D0 = PseudoParticle("D0", D0_daug)

    #Build "B+ -> (phi(1020)-> K+ K-) (phi(1020) -> pi0 pi0 gamma) K+"
    #make phi -> K+ K-
    phi_1_daug = [PseudoParticle("K+"), PseudoParticle("K+")]
    phi_1 = PseudoParticle("phi(1020)", phi_1_daug)
    #make phi -> pi0 pi0 gamma
    #make first identical pi0 with set momenta
    pi0_1 = PseudoParticle("pi0")
    pi01_vec4_1 = GaudiPython.gbl.Gaudi.LorentzVector(2, 1, 0, 3)
    pi0_1.lhcb_particle.setMomentum(pi01_vec4_1)
    #make second identical pi0 with set momenta
    pi0_2 = PseudoParticle("pi0")
    pi02_vec4_1 = GaudiPython.gbl.Gaudi.LorentzVector(4, 2, 0, 6)
    pi0_2.lhcb_particle.setMomentum(pi02_vec4_1)
    #make phi -> pi0 pi0 gamma
    phi_2_daug = [pi0_1, PseudoParticle("gamma"), pi0_2]
    phi_2 = PseudoParticle("phi(1020)", phi_2_daug)
    #make B+
    Bu_2_daug = [phi_1, PseudoParticle("K+"), phi_2]
    Bu_2 = PseudoParticle("B+", Bu_2_daug)

    #Build: "B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> K+ pi-)"
    Kstar892_1_daug = [PseudoParticle("K+"), PseudoParticle("pi-")]
    Kstar892_1 = PseudoParticle("K*(892)0", Kstar892_1_daug)
    Kstarbar1430_1_daug = [
        Kstar892_1, PseudoParticle("pi+"),
        PseudoParticle("pi-")
    ]
    Kstarbar1430_1 = PseudoParticle("K*_0(1430)~0", Kstarbar1430_1_daug)
    Kstar892_2_daug = [PseudoParticle("K+"), PseudoParticle("pi-")]
    Kstar892_2 = PseudoParticle("K*(892)0", Kstar892_2_daug)
    Bs_daug = [Kstarbar1430_1, Kstar892_2]
    Bs = PseudoParticle("B_s0", Bs_daug)

    #Build: "B_s0 -> (K*_0(1430)~0 -> K- pi+) (K*(892)0 -> K+ pi-)"
    Kstarbar1430_2_daug = [PseudoParticle("K-"), PseudoParticle("pi+")]
    Kstarbar1430_2 = PseudoParticle("K*_0(1430)~0", Kstarbar1430_2_daug)
    Bs_daug_2 = [Kstarbar1430_2, Kstar892_2]
    Bs_2 = PseudoParticle("B_s0", Bs_daug_2)

    #put them in input vector
    input_parts = std.vector('const LHCb::Particle*')()
    input_parts.push_back(Bd.lhcb_particle)
    input_parts.push_back(Bd2.lhcb_particle)
    input_parts.push_back(Bd3.lhcb_particle)
    input_parts.push_back(Bu_ord1.lhcb_particle)
    input_parts.push_back(Bu_ord2.lhcb_particle)
    input_parts.push_back(Bu_ord3.lhcb_particle)
    input_parts.push_back(D0.lhcb_particle)
    input_parts.push_back(Bu_2.lhcb_particle)
    input_parts.push_back(Bs.lhcb_particle)
    input_parts.push_back(Bs_2.lhcb_particle)

    return input_parts


def get_marked_particles(descriptor, input_parts, decoder):
    #make container for output particles
    out_parts = std.vector('const LHCb::Particle*')()
    #fill the out_parts vector
    decoder.findDecay(descriptor, input_parts, out_parts)
    #return output particles
    return out_parts


def test_decayfinder_twobody(tool_DecayFinder_Particles):
    #simple two-body descriptor test
    descriptor = "[B0 -> [K+]CC ^pi-]CC"
    input_parts = make_input_particles()
    output_parts = get_marked_particles(descriptor, input_parts,
                                        tool_DecayFinder_Particles)
    assert output_parts.size() == 1
    assert output_parts[0].particleID().pid() == -211
    #simple two-body descriptor test (with charge conjugate particle names)
    # the ouput_parts and output_parts_cc (below) should be identical
    descriptor_cc = "[B~0 -> [K-]CC ^pi+]CC"
    output_parts_cc = get_marked_particles(descriptor_cc, input_parts,
                                           tool_DecayFinder_Particles)
    assert output_parts_cc.size() == output_parts.size()
    assert output_parts_cc[0].particleID().pid() == output_parts[0].particleID(
    ).pid()


def test_decayfinder_identical_threebody(tool_DecayFinder_Particles):
    def _assert(outparts_1, outparts_2, outparts_1_cc, outparts_2_cc):
        #size should be the same
        assert outparts_1.size() == 3
        assert outparts_1.size() == outparts_2.size()
        assert outparts_1.size() == outparts_1_cc.size()
        assert outparts_1.size() == outparts_2_cc.size()
        for i in range(
                outparts_1.
                size()):  #i represents the three orders of input particle
            #pids should match
            assert outparts_1[i].particleID().pid() == 321
            assert outparts_1[i].particleID().pid(
            ) == outparts_2[i].particleID().pid()
            assert outparts_1[i].particleID().pid(
            ) == outparts_1_cc[i].particleID().pid()
            assert outparts_1[i].particleID().pid(
            ) == outparts_2_cc[i].particleID().pid()
            #check that the 2nd kaon is always the second (has to have px = 4 MeV)
            assert abs(output_parts_1[i].momentum().X() - 4.0) < 1e-9
            #check that the 1st kaon is always the first (has to have px = 2 MeV)
            assert abs(outparts_2[i].momentum().X() - 2.0) < 1e-9
            assert abs(output_parts_1[i].momentum().X() -
                       outparts_1_cc[0].momentum().X()) < 1e-9
            assert abs(outparts_2[i].momentum().X() -
                       outparts_2_cc[0].momentum().X()) < 1e-9

    #descriptor matches order 1 of input container
    descriptor_1 = "[B+ -> K+ K- ^K+]CC"  #pick always 2nd kaon in the input_container (no matter the ordering)
    descriptor_2 = "[B+ -> ^K+ K- K+]CC"  #pick always 1st kaon in the input_container (no matter the ordering)
    input_parts = make_input_particles()
    output_parts_1 = get_marked_particles(descriptor_1, input_parts,
                                          tool_DecayFinder_Particles)
    output_parts_2 = get_marked_particles(descriptor_2, input_parts,
                                          tool_DecayFinder_Particles)
    #check if CC'd particle names also picks correct particles
    descriptor_1_cc = "[B- -> K- K+ ^K-]CC"
    descriptor_2_cc = "[B- -> ^K- K+ K-]CC"
    output_parts_1_cc = get_marked_particles(descriptor_1_cc, input_parts,
                                             tool_DecayFinder_Particles)
    output_parts_2_cc = get_marked_particles(descriptor_2_cc, input_parts,
                                             tool_DecayFinder_Particles)
    #conduct some checks
    _assert(output_parts_1, output_parts_2, output_parts_1_cc,
            output_parts_2_cc)

    #descriptor matches order 2 of input container
    descriptor_1 = "[B+ -> K- K+ ^K+]CC"  #pick always 2nd kaon in the input_container (no matter the ordering)
    descriptor_2 = "[B+ -> K- ^K+ K+]CC"  #pick always 1st kaon in the input_container (no matter the ordering)
    output_parts_1 = get_marked_particles(descriptor_1, input_parts,
                                          tool_DecayFinder_Particles)
    output_parts_2 = get_marked_particles(descriptor_2, input_parts,
                                          tool_DecayFinder_Particles)
    #check if CC'd particle names also picks correct particles
    descriptor_1_cc = "[B- -> K+ K- ^K-]CC"
    descriptor_2_cc = "[B- -> K+ ^K- K-]CC"
    output_parts_1_cc = get_marked_particles(descriptor_1_cc, input_parts,
                                             tool_DecayFinder_Particles)
    output_parts_2_cc = get_marked_particles(descriptor_2_cc, input_parts,
                                             tool_DecayFinder_Particles)
    #conduct some checks
    _assert(output_parts_1, output_parts_2, output_parts_1_cc,
            output_parts_2_cc)

    #descriptor matches order 3 of input container
    descriptor_1 = "[B+ -> K+ ^K+ K-]CC"  #pick always 2nd kaon in the input_container (no matter the ordering)
    descriptor_2 = "[B+ -> ^K+ K+ K-]CC"  #pick always 1st kaon in the input_container (no matter the ordering)
    output_parts_1 = get_marked_particles(descriptor_1, input_parts,
                                          tool_DecayFinder_Particles)
    output_parts_2 = get_marked_particles(descriptor_2, input_parts,
                                          tool_DecayFinder_Particles)
    #check if CC'd particle names also picks correct particles
    descriptor_1_cc = "[B- -> K- ^K- K+]CC"
    descriptor_2_cc = "[B- -> ^K- K- K+]CC"
    output_parts_1_cc = get_marked_particles(descriptor_1_cc, input_parts,
                                             tool_DecayFinder_Particles)
    output_parts_2_cc = get_marked_particles(descriptor_2_cc, input_parts,
                                             tool_DecayFinder_Particles)
    #conduct some checks
    _assert(output_parts_1, output_parts_2, output_parts_1_cc,
            output_parts_2_cc)


def test_decayfinder_identical_fourbody(tool_DecayFinder_Particles):
    #test finding of identical particles present in four-body decay
    descriptor_1 = "[D~0 -> ^pi- pi+ pi- pi+]CC"
    descriptor_2 = "[D~0 ->  pi- pi+ ^pi- pi+]CC"
    input_parts = make_input_particles()
    output_parts_1 = get_marked_particles(descriptor_1, input_parts,
                                          tool_DecayFinder_Particles)
    output_parts_2 = get_marked_particles(descriptor_2, input_parts,
                                          tool_DecayFinder_Particles)
    #size should be the same
    assert output_parts_1.size() == 1
    assert output_parts_1.size() == output_parts_2.size()
    #pids should match
    assert output_parts_1[0].particleID().pid() == 211
    assert output_parts_1[0].particleID().pid(
    ) == output_parts_2[0].particleID().pid()
    #should be 3 MeV for output_parts_1 and 6 MeV for output_parts_2
    assert abs(output_parts_1[0].momentum().X() - 3.0) < 1e-9
    assert abs(output_parts_2[0].momentum().X() - 6.0) < 1e-9


def test_decayfinder_identical_headdecay_and_daugterdecay(
        tool_DecayFinder_Particles):
    #test finding of identical particles present at two levels: head decay and daughter decay
    descriptor_1 = "[B+ -> phi(1020) (phi(1020) ->  gamma ^pi0 pi0) K+]CC"
    descriptor_2 = "[B+ -> phi(1020) (phi(1020) ->  pi0 gamma ^pi0) K+]CC"
    input_parts = make_input_particles()
    output_parts_1 = get_marked_particles(descriptor_1, input_parts,
                                          tool_DecayFinder_Particles)
    output_parts_2 = get_marked_particles(descriptor_2, input_parts,
                                          tool_DecayFinder_Particles)
    #size should be the same
    assert output_parts_1.size() == 1
    assert output_parts_1.size() == output_parts_2.size()
    #pids should match
    assert output_parts_1[0].particleID().pid() == 111
    assert output_parts_1[0].particleID().pid(
    ) == output_parts_2[0].particleID().pid()
    #should be 2 MeV for output_parts_1 and 4 MeV for output_parts_2
    assert abs(output_parts_1[0].momentum().X() - 2.0) < 1e-9
    assert abs(output_parts_2[0].momentum().X() - 4.0) < 1e-9


def test_decayfinder_depth_1(tool_DecayFinder_Particles):
    #descriptor that finds particles in the head node decay (depth 1)
    descriptor = "[B_s0 -> K*(892)0 ^K*_0(1430)~0]CC"
    input_parts = make_input_particles()
    output_parts = get_marked_particles(descriptor, input_parts,
                                        tool_DecayFinder_Particles)
    assert output_parts.size() == 2
    assert output_parts[0].particleID().pid() == -10311
    assert output_parts[1].particleID().pid() == -10311
    #descriptor that matches top level particles but particles names are CC'd
    # the ouput_parts and output_parts_cc (below) should be identical
    descriptor_cc = "[B_s~0 -> K*(892)~0 ^K*_0(1430)0]CC"
    output_parts_cc = get_marked_particles(descriptor_cc, input_parts,
                                           tool_DecayFinder_Particles)
    assert output_parts_cc.size() == output_parts.size()
    assert output_parts_cc[0].particleID().pid() == output_parts[0].particleID(
    ).pid()
    assert output_parts_cc[1].particleID().pid() == output_parts[1].particleID(
    ).pid()


def test_decayfinder_depth_2(tool_DecayFinder_Particles):
    #descriptor that finds particles in the immediate daughter decay (depth 2)
    descriptor = "[B_s0 -> K*(892)0 (K*_0(1430)~0 -> ^K*(892)0 pi+ pi-)]CC"
    input_parts = make_input_particles()
    output_parts = get_marked_particles(descriptor, input_parts,
                                        tool_DecayFinder_Particles)
    assert output_parts.size() == 1
    assert output_parts[0].particleID().pid() == 313
    #descriptor with depth 2 but selects with CC particle names
    # output_parts and output_parts_2_cc should be identical
    descriptor_cc = "[B_s~0 -> K*(892)~0 (K*_0(1430)0 -> ^K*(892)~0 pi- pi+)]CC"
    output_parts_cc = get_marked_particles(descriptor_cc, input_parts,
                                           tool_DecayFinder_Particles)
    assert output_parts_cc.size() == output_parts.size()
    assert output_parts_cc[0].particleID().pid() == output_parts[0].particleID(
    ).pid()


def test_decayfinder_depth_3(tool_DecayFinder_Particles):
    #descriptor that finds particles in the daughte of daughter decay (depth 3)
    descriptor = "[B_s0 -> K*(892)0 (K*_0(1430)~0 -> (K*(892)0 -> ^K+ pi-) pi+ pi-)]CC"
    input_parts = make_input_particles()
    output_parts = get_marked_particles(descriptor, input_parts,
                                        tool_DecayFinder_Particles)
    assert output_parts.size() == 1
    assert output_parts[0].particleID().pid() == 321
    #descriptor with depth 2 but selects with CC particle names
    # output_parts and output_parts_2_cc should be identical
    descriptor_cc = "[B_s~0 -> K*(892)~0 (K*_0(1430)0 -> (K*(892)~0 -> ^K- pi+) pi- pi+)]CC"
    output_parts_cc = get_marked_particles(descriptor_cc, input_parts,
                                           tool_DecayFinder_Particles)
    assert output_parts_cc.size() == output_parts.size()
    assert output_parts_cc[0].particleID().pid() == output_parts[0].particleID(
    ).pid()
