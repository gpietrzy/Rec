###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
import GaudiPython
from GaudiPython.Bindings import AppMgr
from Configurables import LHCbApp, MessageSvc
from cppyy.gbl import LHCb, std, SmartRefVector, SmartRef
import PartProp.Service  #this is required for app.ppSvc() to work

#prepare application manager
MessageSvc(OutputLevel=3)  #INFO==3, VERBOSE==1
app = LHCbApp()
app.DataType = "Upgrade"
appMgr = AppMgr()
pps = appMgr.ppSvc()
#below is required for ppSvc to work for pytest
appMgr.ExtSvc += ['LHCb::ParticlePropertySvc']
toolsvc = appMgr.toolSvc()
appMgr.initialize()


@pytest.fixture()
def tool_DecayFinder_Particles():
    decoder = toolsvc.create(
        "DecayFinder", interface=GaudiPython.gbl.IDecayFinder)
    return decoder


def test_parsing_exceptions(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    #map of wrong (key) -> allowed (value) descriptors
    map_wrong_to_correct_descriptors = {
        #exception raised for wrong particle name
        "Bs":
        "B_s0",
        #exception raised for wrong name in decay descriptor
        "Bs -> mu+ mu-":
        "B_s0 -> mu+ mu-",
        #exception raised when brackets (square or parenthesis) not balanced
        # following raises exception since no balanced square brackets
        "[[B_s0]CC -> mu+ mu-":
        "[[B_s0]CC -> mu+ mu-]CC",
        #following raises exception since no balanced parenthesis in particle name
        "B_s0 -> (J/psi(1S) -> mu+ mu-) phi(1020":
        "B_s0 -> (J/psi(1S) -> mu+ mu-) phi(1020)",
        #following raises exception since no balanced parenthesis in decay
        "B_s0 -> (J/psi(1S) -> mu+ mu- phi(1020)":
        "B_s0 -> (J/psi(1S) -> mu+ mu-) phi(1020)",
        #exception raised if hat inside CC
        "[^B_s0]CC":
        "^[B_s0]CC",
        #exception raised if parenthesis inside CC
        "[(B_s0 -> mu+ mu-)]CC":
        "([B_s0 -> mu+ mu-]CC)",
        #exception raised if square brakets specified without CC
        "[B_s0 -> mu+ mu-]":
        "B_s0 -> mu+ mu-",
        #exception raised if square brakets in inner decay instead of parenthesis
        "B_s0 -> [J/psi(1S) -> mu+ mu-] phi(1020)":
        "B_s0 -> (J/psi(1S) -> mu+ mu-) phi(1020)",
        #exception raised if hat in front of square brakets without CC
        # no hat => hat on head node
        "^[B_s0 -> mu+ mu-]":
        "B_s0 -> mu+ mu-",
        #exception raised if hat in front of square brakets with CC
        "^[B_s0 -> mu+ mu-]CC":
        "^([B_s0 -> mu+ mu-]CC)",
    }

    for desc in map_wrong_to_correct_descriptors.keys(
    ):  #each of the keys should raise exception
        with pytest.raises(GaudiPython.gbl.GaudiException):
            _ = decoder.getParsedDescriptors([desc])

    for desc in map_wrong_to_correct_descriptors.values(
    ):  #each of the keys should NOT raise exception
        try:
            _ = decoder.getParsedDescriptors([desc])
        except GaudiPython.gbl.GaudiException as exc:
            assert False, f"{desc} raised an exception {exc}"


def test_parsing_node(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    descriptors = ["^B_s0"]  #hat
    descriptors += ["[B_s0]CC"]  #CC
    descriptors += ["^[B_s0]CC"]  #hat and CC
    descriptors += ["^[K*_0(1430)~0]CC"]  #special characters
    descriptors += ["^[Pb208[0.0]]CC"]  #square brackets
    parsed_descriptors = decoder.getParsedDescriptors(descriptors)
    for descriptor, parsed_descriptor in zip(descriptors, parsed_descriptors):
        assert parsed_descriptor == descriptor.replace(" ", "")


def test_parsing_CC(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    descriptors = ["B_s0 -> mu+ mu-"]
    #simple descriptor
    descriptors += ["B_s0 -> mu+ [mu-]CC"]
    #CC in particle
    descriptors += ["[B_s0]CC -> [mu+]CC [mu-]CC"]
    #multiple CC in particle
    descriptors += ["[B_s0]CC -> [mu+]CC cc_1"]
    #cc_1 is a valid particle name
    descriptors += ["[[B_s0]CC -> mu+ [mu-]CC]CC"]
    #CC in decay and in particle
    descriptors += ["[[B_s0]CC -> mu+ [mu-]CC]CC"]
    #CC in decay and in particle
    descriptors += ["[[B_s0]CC -> [mu+]CC [cc_1]CC]CC"]
    #with cc_1 particle name
    parsed_descriptors = decoder.getParsedDescriptors(descriptors)
    for descriptor, parsed_descriptor in zip(descriptors, parsed_descriptors):
        assert parsed_descriptor == descriptor


def test_parsing_hat(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    descriptors = ["B_s0 -> mu+ mu-"]  #No hat implies head node hat implicitly
    descriptors += ["^B_s0 -> mu+ mu-"]  #hat on head node
    descriptors += ["^(B_s0 -> mu+ mu-)"]  #hat in the decay
    descriptors += ["^(^B_s0 -> mu+ mu-)"]  #hat in head node and decay
    descriptors += ["B_s0 -> ^mu+ mu-"]  #hat on daughter
    descriptors += ["B_s0 -> ^(K*(892)0 -> K+ pi-) phi(1020)"
                    ]  #hat on sub-decay
    descriptors += ["B_s0 -> (K*(892)0 -> ^K+ pi-) phi(1020)"
                    ]  #hat on daughter of sub-decay
    parsed_descriptors = decoder.getParsedDescriptors(descriptors)
    for descriptor, parsed_descriptor in zip(descriptors, parsed_descriptors):
        assert parsed_descriptor == descriptor


def test_parsing_CC_and_hat(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    descriptors = ["^[B_s0]CC -> mu+ mu-"]  #hat on CC'd head node
    descriptors += ["^([B_s0 -> mu+ mu-]CC)"]  #hat in the CC'd decay
    descriptors += ["^([^B_s0 -> mu+ mu-]CC)"
                    ]  #hat on the CC'd decay with hat on head node
    descriptors += ["^([^[B_s0]CC -> mu+ mu-]CC)"
                    ]  #hat on the CC'd decay with hat on CC'd head node
    descriptors += ["B_s0 -> ^[mu+]CC mu-"]  #hat on CC'd daughter
    descriptors += ["[B_s0 -> ^[mu+]CC mu-]CC"
                    ]  #hat on CC'd daughter with decay CC'd
    descriptors += ["B_s0 -> ^([K*(892)0 -> K+ pi-]CC) phi(1020)"
                    ]  #hat on CC'd sub-decay
    descriptors += ["B_s0 -> (K*(892)0 -> ^[K+]CC pi-) phi(1020)"
                    ]  #hat on CC'd daughter of sub-decay
    descriptors += ["B_s~0 -> ^([K*_0(1430)~0 -> K- pi+]CC) K*(892)~0"
                    ]  #hat and CC'd on particle with special characters
    descriptors += ["[Pb208[0.0] -> ^[Pb206[0.0]]CC [pi-]CC]CC"
                    ]  #had and CC'd on particle with square brackets
    parsed_descriptors = decoder.getParsedDescriptors(descriptors)
    for descriptor, parsed_descriptor in zip(descriptors, parsed_descriptors):
        assert parsed_descriptor == descriptor


def test_parsing_whitespaces(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    #map of user_descriptor (key) -> parsed_descriptor (value)
    map_user_parsed = {}
    map_user_parsed[
        "  ^  [  K*_0(1430)~0  ]  CC"] = "^[K*_0(1430)~0]CC"  #space between hat and []CC
    map_user_parsed[
        "  ^  [  Pb208[0.0]  ]  CC"] = "^[Pb208[0.0]]CC"  #space between special particle names
    #Space between particles
    map_user_parsed[
        "B_s0->mu+ mu-"] = "B_s0 -> mu+ mu-"  #no space b/w head,arrow and preceding particle
    map_user_parsed[
        "     B_s0    -> mu+       mu-"] = "B_s0 -> mu+ mu-"  #spaces all over
    #Space in []CC
    map_user_parsed[
        "B_s0 -> [    mu+  ] CC cc_1"] = "B_s0 -> [mu+]CC cc_1"  #space between []CC around particle
    map_user_parsed[
        "  [   B_s0 -> mu+ cc_1 ]     CC"] = "[B_s0 -> mu+ cc_1]CC"  #space between []CC around decay
    map_user_parsed[
        "  [   B_s0 -> mu+ [   cc_1 ]  CC ]     CC"] = "[B_s0 -> mu+ [cc_1]CC]CC"  #space between []CC around decay and particle
    #Space in hat
    map_user_parsed["B_s0 -> mu+ ^  mu-"] = "B_s0 -> mu+ ^mu-"
    map_user_parsed["   ^   B_s0 -> mu+ mu-"] = "^B_s0 -> mu+ mu-"
    #Space in hat and CC
    map_user_parsed[
        "   ^   B_s0 -> mu+ [   mu-  ] CC"] = "^B_s0 -> mu+ [mu-]CC"
    #space in hats and cc with complex particle names
    map_user_parsed[
        "[  ^  B_1(H)0 -> [Pb208[0.0]]CC [  K*_0(1430)~0  ]  CC  ]  CC"] = "[^B_1(H)0 -> [Pb208[0.0]]CC [K*_0(1430)~0]CC]CC"
    map_user_parsed[
        "[  ^  B_1(H)0 -> [  mu+ ] CC CLUSjet    CELLjet]CC"] = "[^B_1(H)0 -> [mu+]CC CLUSjet CELLjet]CC"
    map_user_parsed[
        "[  ^  B_1(H)0 -> [  mu+ ] CC [CLUSjet]CC    CELLjet]CC"] = "[^B_1(H)0 -> [mu+]CC [CLUSjet]CC CELLjet]CC"
    map_user_parsed[
        " Pb208[0.0]  -> Pb206[0.0] ^ [  Pb204[0.0]  ] CC CELLjet"] = "Pb208[0.0] -> Pb206[0.0] ^[Pb204[0.0]]CC CELLjet"
    for user_descriptor, expected_parsed_descriptor in map_user_parsed.items():
        parsed_descriptor = decoder.getParsedDescriptors([user_descriptor])[0]
        assert expected_parsed_descriptor == parsed_descriptor


def test_parsing_complex_decays(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles
    #WARNING: Fake decays
    descriptors = []
    descriptors += [
        "B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> K+ pi-) (K*_0(1430)~0 -> K+ gamma)"
    ]  #N outer decays
    descriptors += [
        "B_s0 -> (K*_4(2045)0 -> (K*_0(1430)~0 -> (K*(892)0 -> K+ pi-) pi0) rho(1700)0) phi(1020)"
    ]  #N inner decays
    descriptors += [
        "[[B_s0]CC -> ([K*_0(1430)~0 -> K*(892)0 [pi+]CC [pi-]CC]CC) phi(1020)]CC"
    ]  #CC in daughters and decays
    descriptors += [
        "[[B_s0]CC -> ^([K*_0(1430)~0 -> K*(892)0 ^[pi+]CC [pi-]CC]CC) phi(1020)]CC"
    ]  #CC in daughters and decays
    parsed_descriptors = decoder.getParsedDescriptors(descriptors)
    for descriptor, parsed_descriptor in zip(descriptors, parsed_descriptors):
        assert parsed_descriptor == descriptor


def test_decay_possibilities(tool_DecayFinder_Particles):
    decoder = tool_DecayFinder_Particles

    #Simple case
    descriptor = "[B_s0 -> [K*_0(1430)~0]CC K*(892)0]CC"
    number_of_CC = 2
    possibilities = decoder.getDescriptorPossibilities(descriptor)
    #check that number of possibilities is correct
    assert len(possibilities) == 2**number_of_CC
    #check if the returned possibilities match expected possibilities
    expected_possibilities = []
    expected_possibilities += ["B_s0 -> K*_0(1430)~0 K*(892)0"]
    expected_possibilities += ["B_s~0 -> K*_0(1430)0 K*(892)~0"]
    expected_possibilities += ["B_s0 -> K*_0(1430)0 K*(892)0"]
    expected_possibilities += ["B_s~0 -> K*_0(1430)~0 K*(892)~0"]
    for expected_possibility, possibility in zip(expected_possibilities,
                                                 possibilities):
        assert expected_possibility == possibility

    #Case with sub-decays and hats
    descriptor = "[B_s0]CC -> ([K*_0(1430)~0 -> K*(892)0 pi+ pi-]CC) (K*(892)0 -> ^[K+]CC pi-)"
    number_of_CC = 3
    possibilities = decoder.getDescriptorPossibilities(descriptor)
    #check that number of possibilities is correct
    assert len(possibilities) == 2**number_of_CC
    #check if the returned possibilities match expected possibilities
    expected_possibilities = []
    expected_possibilities += [
        "B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> ^K+ pi-)"
    ]
    expected_possibilities += [
        "B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> ^K- pi-)"
    ]
    expected_possibilities += [
        "B_s0 -> (K*_0(1430)0 -> K*(892)~0 pi- pi+) (K*(892)0 -> ^K+ pi-)"
    ]
    expected_possibilities += [
        "B_s0 -> (K*_0(1430)0 -> K*(892)~0 pi- pi+) (K*(892)0 -> ^K- pi-)"
    ]
    expected_possibilities += [
        "B_s~0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> ^K+ pi-)"
    ]
    expected_possibilities += [
        "B_s~0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> ^K- pi-)"
    ]
    expected_possibilities += [
        "B_s~0 -> (K*_0(1430)0 -> K*(892)~0 pi- pi+) (K*(892)0 -> ^K+ pi-)"
    ]
    expected_possibilities += [
        "B_s~0 -> (K*_0(1430)0 -> K*(892)~0 pi- pi+) (K*(892)0 -> ^K- pi-)"
    ]
    for expected_possibility, possibility in zip(expected_possibilities,
                                                 possibilities):
        assert expected_possibility == possibility


def test_parsing_arrows(tool_DecayFinder_Particles):
    #parsing of arrow notation
    # NOTE: the finding of particles according to arrow is not implemented yet
    decoder = tool_DecayFinder_Particles
    descriptors = []
    descriptors += ["B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) K*(892)0"]  #->
    descriptors += ["B_s0 -> (K*_0(1430)~0 --> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #-->
    descriptors += ["B_s0 --> (K*_0(1430)~0 => K*(892)0 pi+ pi-) K*(892)0"
                    ]  #=>
    descriptors += ["B_s0 => (K*_0(1430)~0 ==> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #==>
    descriptors += ["B_s0 ==> (K*_0(1430)~0 -x> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #-x>
    descriptors += ["B_s0 -x> (K*_0(1430)~0 --x> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #--x>
    descriptors += ["B_s0 --x> (K*_0(1430)~0 =x> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #=x>
    descriptors += ["B_s0 =x> (K*_0(1430)~0 ==x> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #==x>
    descriptors += ["B_s0 ==x> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) K*(892)0"
                    ]  #->
    parsed_descriptors = decoder.getParsedDescriptors(descriptors)
    for descriptor, parsed_descriptor in zip(descriptors, parsed_descriptors):
        assert parsed_descriptor == descriptor
