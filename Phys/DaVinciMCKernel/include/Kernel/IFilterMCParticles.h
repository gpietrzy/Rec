/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef KERNEL_IFILTERMCPARTICLES_H
#define KERNEL_IFILTERMCPARTICLES_H 1

// Include files
// from STL
#include <string>

// from Gaudi
#include "GaudiKernel/IAlgTool.h"

// Forward declarations
// from Event
#include "Event/MCParticle.h"

static const InterfaceID IID_IFilterMCParticles( "IFilterMCParticles", 1, 0 );

/** @class IFilterMCParticles IFilterMCParticles.h Kernel/IFilterMCParticles.h
 *
 *  Select or reject an ensemble of particles.
 *
 *  @author Juan Palacios
 *  @date   2007-07-24
 */
class IFilterMCParticles : virtual public IAlgTool {
public:
  // Return the interface ID
  static const InterfaceID& interfaceID() { return IID_IFilterMCParticles; }

  /// Test if filter is satisfied on ensemble of MCParticles
  virtual bool isSatisfied( const LHCb::MCParticle::ConstVector& ) const = 0;
  /// Test if filter is satisfied on ensemble of MCParticles
  virtual bool operator()( const LHCb::MCParticle::ConstVector& ) const = 0;

protected:
private:
};
#endif // KERNEL_IFILTERMCPARTICLES_H
