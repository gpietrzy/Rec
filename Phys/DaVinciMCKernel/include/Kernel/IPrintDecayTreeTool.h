/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef DAVINCIMCTOOLS_IPRINTDECAYTREETOOL_H
#  define DAVINCIMCTOOLS_IPRINTDECAYTREETOOL_H 1
// ============================================================================
// Include files
// ============================================================================
// STL&STL
// ============================================================================
#  include <string>
#  include <vector>
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/IAlgTool.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#  include "Kernel/IPrintDecay.h"
// ============================================================================
// Event
// ============================================================================
#  include "Event/MCParticle.h"
#  include "Event/Particle.h"
#  include "Kernel/Particle2MCLinker.h"
// ============================================================================
// Declaration of the interface ID (interface id, major version, minor version)
static const InterfaceID IID_IPrintDecayTreeTool( "IPrintDecayTreeTool", 2, 0 );
// ============================================================================
/** @class IPrintDecayTreeTool IPrintDecayTreeTool.h Kernel/IPrintDecayTreeTool.h
 *
 *
 *  @author Juan Palacios juancho@nikhef.nl
 *  @date   09/10/2007
 */
class IPrintDecayTreeTool : virtual public IPrintDecay {
public:
  // ==========================================================================
  /** Print the decay tree for a given particle
   *
   *  @code
   *
   *  IPrintDecay* tool = ... ;
   *  const LHCb::Particle* B = ... ;
   *
   *  tool -> printDecay ( B ) ;
   *
   *  @endcode
   *  @see IPrintDecay
   *  @param mother the pointer to the particle
   *  @param maxDepth the maximal depth level
   */
  void printTree( const LHCb::Particle* mother, int maxDepth = -1 ) const override = 0;
  // ==========================================================================
  /** Print side by side a decay tree for an MCParticle
   *  and it's associated particles
   *
   * @param mother The MCParticle who's tree is to be printed
   * @param assoc Associator linking mother to reconstructed LHCb::Particle::Range
   * @param maxDepth maximum depth of printing in daughter-space. Default
   *                 value of -1 is there to allow trickery in the
   *                 implementations.
   * @todo Does this print two side-=by side trees, or a tree
   * of side-by side items?
   */
  virtual void printTree( const LHCb::MCParticle* mother, Particle2MCLinker* assoc, int maxDepth = -1 ) const = 0;
  /** Print side by side a decay tree for a Particle
   * and it's associated MCParticles
   *
   * @param mother The Particle who's tree is to be printed
   * @param assoc Associator linking mother to LHCb::MCParticles
   * @param maxDepth maximum depth of printing in daughter-space. Default
   *                 value of -1 is there to allow trickery in the
   *                 implementations.
   * @todo Does this print two side-=by side trees, or a tree
   * of side-by side items?
   */
  virtual void printTree( const LHCb::Particle* mother, Particle2MCLinker* assoc, int maxDepth = -1 ) const = 0;

  /** Print the contents of an MCParticle keyed container as a decay tree.
   *  Print the associated reconstructed Particles if available.
   *
   * @param particles The keyed container of MCParticles to be printed.
   * @param assoc Associator linking mother to LHCb::MCParticles
   *
   */
  virtual void printAsTree( const LHCb::MCParticle::Range& particles, Particle2MCLinker* assoc = nullptr ) const = 0;
  /** Print the contents of a Particle::Range as a flat list.
   *  Print the associated MCParticles if available.
   *
   *  @param particles The vector of Particles to be printed.
   *  @param assoc Associator linking mother to LHCb::MCParticles
   */
  virtual void printAsList( const LHCb::Particle::Range& particles, Particle2MCLinker* assoc = nullptr ) const = 0;
  /** Print the contents of an MCParticle::ConstVector as a flat list.
   * Print the associated recunstructed Particles if available.
   *
   * @param particles The vector of MCParticles to be printed.
   * @param assoc Associator linking mother to LHCb::Particles
   */
  virtual void printAsList( const LHCb::MCParticle::Range& particles, Particle2MCLinker* assoc = nullptr ) const = 0;
  // ==========================================================================
public:
  // ==========================================================================
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_IPrintDecayTreeTool; }
  // ==========================================================================
};
// ============================================================================
// The END
// ============================================================================
#endif // DAVINCIMCTOOLS_IPRINTDECAYTREETOOL_H
// ============================================================================
