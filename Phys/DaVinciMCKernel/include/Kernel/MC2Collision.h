/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef KERNEL_MC2COLLISION_H
#  define KERNEL_MC2COLLISION_H 1
// ============================================================================
// Include files
// ============================================================================
// GaudiKernel
// ============================================================================
#  include "GaudiKernel/Kernel.h"
// ============================================================================
// Event
// ============================================================================
#  include "Event/GenCollision.h"
#  include "Event/MCVertex.h"
// ============================================================================
// forward declarations
// ============================================================================
namespace LHCb {
  class MCParticle;
  class MCVertex;
  class MCHit;
  class MCCaloHit;
  class MCRichHit;
  class MCFTDeposit;
  class MCUTDeposit;
} // namespace LHCb
// ============================================================================
template <class FROM, class TO>
class IRelation;
// ============================================================================
/** @file Kernel/MC2Collision.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-03-18
 *
 */
// ============================================================================
namespace LHCb {
  // ==========================================================================
  namespace Relations_ {
    // ========================================================================
    /// the actual type for MCVertex  --> GenCollision relations
    typedef IRelation<LHCb::MCVertex, LHCb::GenCollision> MCVertex2Collision;
    // ========================================================================
  } // namespace Relations_
  // ==========================================================================
  /// the actual type for MCVertex  --> GenCollision relations
  typedef LHCb::Relations_::MCVertex2Collision MCVertex2Collision;
  // ==========================================================================
  namespace MC2CollisionLocation {
    // ========================================================================
    const std::string Default = "Relations/" + LHCb::MCVertexLocation::Default + "2Collision";
    // ========================================================================
  } // namespace MC2CollisionLocation
  // ==========================================================================
} //                                                     end of namespace LHCb
// ============================================================================
/** @namespace MC2Collision Kernel/MC2Collision.h
 *
 *  Collection of helper utilities to determine the collision for
 *  MC objects
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date   2006-03-18
 */
// ==========================================================================
namespace MC2Collision {
  // ==========================================================================
  /** get the GenCollision object, associated with the given vertex
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCVertex* vertex = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( vertex , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCVertex
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param vertex vertex to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCVertex* vertex, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** get the GenCollision object, associated with the given particle
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCParticle* particle = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( particle , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCParticle
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param particle particle to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCParticle* particle, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** get the GenCollision object, associated with the given hit
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCHit* hit = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( hit , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCHit
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param hit    hit to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCHit* hit, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** get the GenCollision object, associated with the given hit
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCCaloHit* hit = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( hit , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCCaloHit
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param hit    hit to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCCaloHit* hit, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** get the GenCollision object, associated with the given deposit
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCFTDeposit* hit = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( hit , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCFTDeposit
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param hit    hit to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCFTDeposit* hit, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** get the GenCollision object, associated with the given deposit
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCUTDeposit* hit = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( hit , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCUTDeposit
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param hit    hit to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCUTDeposit* hit, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** get the GenCollision object, associated with the given Rich hit
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  const LHCb::MCRichHit* hit = ... ;
   *
   *  // get the GenCollision object:
   *  const LHCb::GenCollision* c = collision ( hit , table ) ;
   *
   *  @endcode
   *
   *  @see LHCb::MCRichHit
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *
   *  @param hit    hit to be inspected
   *  @param table  information provider
   *  @return the associated collision object
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::GenCollision* collision( const LHCb::MCRichHit* hit, const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given vertex
   *
   *  @param vertex vertex
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCVertex* vertex );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given particle
   *
   *  @param hit particle
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCParticle* hit );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given hit
   *
   *  @param hit hit
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCHit* hit );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given hit
   *
   *  @param hit hit
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCCaloHit* hit );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given hit
   *
   *  @param hit hit
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCRichHit* hit );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given hit
   *
   *  @param hit hit
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCFTDeposit* hit );
  // ==========================================================================
  /** Simple function wich returns the primary vertex for the given hit
   *
   *  @param hit hit
   *  @return the primary vertex
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  GAUDI_API
  const LHCb::MCVertex* primaryVertex( const LHCb::MCUTDeposit* hit );
  // ==========================================================================
  /** Helper function which checs, is the given (primary) vertex
   *  has as an origin the given collision
   *  @param vertex the vertex
   *  @param collision the collision
   *  @param table the information source
   *  @return true if the vertex comes from the collision
   */
  GAUDI_API
  bool fromCollision( const LHCb::MCVertex* vertex, const LHCb::GenCollision* collision,
                      const LHCb::MCVertex2Collision* table );
  // ==========================================================================
  /** Helper class(functor) to find check if the MC-objects
   *  originates form a given GenCollision
   *
   *  @code
   *
   *  // get the relation table
   *  const LHCb::MCVertex2Collision* table = ... ;
   *
   *  // the collision
   *  const LHCb::GenCollision* c = ... ;
   *
   *  // A) count MC-hits from the given collision:
   *
   *  // get MCHits form TES
   *  const LHCb::MCHits* hits = get<LHCb::MCHits>( ... ) ;
   *
   *  const size_t nHits =
   *   std::count_if ( hits->begin()                           ,
   *                   hits->end()                             ,
   *                   FromCollision<LHCb::MCHit>( c , table ) ) ;
   *
   *  // B) get all MC-particles form the given collison
   *
   *  // get MCParticles form TES
   *  const LHCb::MCParticles* mcps = get<LHCb::MCParticles>( ... ) ;
   *  // prepare the output container
   *  LHCb::Particle::ConstVector result ;
   *  // put all particles from the given collisinig into the output container:
   *  LoKi::Algs::copy_if ( mcps->begin() ,
   *                        mcps->end  () ,
   *                        FromCollision<LHCb::MCParticle>( c , table ) ) ;
   *
   *
   *  @endcode
   *
   *  @see LHCb::GenCollision
   *  @see LHCb::MCVertex2Collision
   *  @see MC2Collision
   *
   *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
   *  @date   2006-03-18
   */
  template <class TYPE>
  class FromCollision {
  public:
    // ========================================================================
    /// constructor from the collision and the information provider
    FromCollision( const LHCb::GenCollision* collision, const LHCb::MCVertex2Collision* table )
        : m_collision( collision ), m_table( table ) {}
    /// the only one essential method
    bool operator()( const TYPE* object ) const {
      return fromCollision( primaryVertex( object ), m_collision, m_table );
    }
    // ========================================================================
  private:
    // ========================================================================
    const LHCb::GenCollision*       m_collision;
    const LHCb::MCVertex2Collision* m_table;
    // ========================================================================
  };
  // ==========================================================================
} //                                              end of namespace MC2Collision
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // KERNEL_MC2COLLISION_H
// ============================================================================
