###############################################################################
# (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciMCTools
-------------------
#]=======================================================================]

gaudi_add_module(DaVinciMCTools
    SOURCES
        src/BackgroundCategory.cpp
        src/BackgroundCategoryViaRelations.cpp
        src/DaVinciSmartAssociator.cpp
        src/DaVinciAssociatorsWrapper.cpp
        src/P2MCPFromProtoP.cpp
        src/Particle2BackgroundCategoryRelationsAlg.cpp
        src/PrintDecayTreeTool.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::CaloFutureInterfaces
        LHCb::LHCbMathLib
        LHCb::MCAssociators
        LHCb::MCEvent
        LHCb::MCInterfaces
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
        Rec::DaVinciMCKernelLib
        Rec::LoKiPhysMCLib
)

gaudi_install(PYTHON)
