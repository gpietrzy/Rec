###############################################################################
# (c) Copyright 2022-2023 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from typing import Optional
from Gaudi.Configuration import INFO  # type: ignore[import]

from PyConf.Algorithms import MCTruthAndBkgCatAlg  # type: ignore[import]
from PyConf.Tools import DaVinciSmartAssociator, ParticleDescendants  # type: ignore[import]
from PyConf.Tools import BackgroundCategory, BackgroundCategoryViaRelations
from PyConf.Tools import P2MCPFromProtoP
from PyConf.dataflow import DataHandle  # type: ignore[import]
from PyConf.reading import get_pp2mcp_relations, get_mc_particles, tes_root  # type: ignore[import]
from PyConf.location_prefix import prefix  # type: ignore[import]
import Functors as F  # type: ignore[import]
from Functors.grammar import BoundFunctor  # type: ignore[import]


class MCTruthAndBkgCat:
    """
    Helper class to store the MC truth information and background category.
    This class configures the tools instantiated by the `MCTruthAndBkgCatAlg` algorithm.

    The tools configured are `DaVinciSmartAssociator`, `MCMatchObjP2MCRelator`,
    `BackgroundCategory`, `BackgroundCategoryViaRelations` and `P2MCPFromProtoP`.

    The configuration for Sprucing output is the same as for HLT2 output
    provided the correct `relations_locs` and `root_in_tes` arguments are set.

    The output of this algorithm is two relation tables:
    one for MC association (P->MCP) and one for background category (P->BKGCAT).
    The MC association table can be used as input to the `MAP_INPUT` functor
    and the background category table serves as input for `BKGCAT` functor.

    Args:
        input_particles (DataHandle): TES location of the reconstructed particles (Output of `Gaudi::Hive::FetchDataFromFile`).
        relations_locs (list, optional): TES location to the pre-existing relations for charged and neutral particles.
            Defaults to ["Relations/ChargedPP2MCP", "Relations/NeutralPP2MCP"].
        root_in_tes (str, optional): RootInTES location that can be different for streamed output. Defaults is None.
            If not 'None' then it is inferred from the 'input_process' option in yaml file.
        redo_neutral_assoc (bool, optional): Whether or not to redo MC association of pure neutral calorimetric basic particle,
            i.e. gamma and pi0-merged with pi0-resolved treated as composite.
            Defaults to False.
        output_level (int, optional): the standard `OutputLevel` from `Gaudi.Configuration`
            to set for all instantiated algorithms and tools. Defaults to `INFO` (=3).

    Example:
        # Create the helper class
        mctruth = MCTruthAndBkgCat(Particles_TES)

        #Store the background category of the particle
        allvariables['BKGCAT'] = mctruth.BkgCat

        # Store the TRUE momentum of the particle
        allvariables['TRUEP'] = mctruth(F.PT)
    """

    def __init__(
            self,
            input_particles: DataHandle,
            relations_locs: list[str] = [
                "Relations/ChargedPP2MCP",
                "Relations/NeutralPP2MCP",
            ],
            root_in_tes: Optional[str] = None,
            redo_neutral_assoc: bool = False,
            filter_mcp: bool = True,
            name: Optional[str] = None,
            output_level: int = INFO,
    ):
        # get the root_in_tes from the input process if not given
        if not root_in_tes:
            root_in_tes = tes_root()

        mc_parts = get_mc_particles(root_in_tes +
                                    "/MC/Particles"  # type: ignore[operator]
                                    )

        relations = [
            get_pp2mcp_relations(prefix(rel, root_in_tes))
            for rel in relations_locs
        ]

        # Tool used by DaVinciSmartAssociator
        p2mctool = P2MCPFromProtoP(
            Locations=relations,
            MCParticleDefaultLocation=mc_parts,
            RootInTES=root_in_tes,
            OutputLevel=output_level,
        )

        # Tools used by MCTruthAndBkgCatAlg
        part_desc = ParticleDescendants(
            RootInTES=root_in_tes, OutputLevel=output_level)
        bkg_cat_via_rel = BackgroundCategoryViaRelations(
            RootInTES=root_in_tes, OutputLevel=output_level)
        bkg_cat = BackgroundCategory(
            P2MCTool=p2mctool,
            BackgroundCategoryTool=bkg_cat_via_rel,
            ParticleDescendantsTool=part_desc,
            ExtraInputs=relations,
            vetoNeutralRedo=not redo_neutral_assoc,
            RootInTES=root_in_tes,
            OutputLevel=output_level,
        )
        dv_assc = DaVinciSmartAssociator(
            P2MCTool=p2mctool,
            RedoNeutral=redo_neutral_assoc,
            MCParticleDefaultLocation=mc_parts,
            RootInTES=root_in_tes,
            BackgroundCategoryTool=bkg_cat,
            OutputLevel=output_level,
        )

        self.mctruth = MCTruthAndBkgCatAlg(
            name="MCTruthAndBkgCatAlg_{hash}" if not name else name,
            Input=input_particles,
            FilterMCP=filter_mcp,
            DaVinciSmartAssociator=dv_assc,
            BackgroundCategory=bkg_cat,
            BackgroundCategoryViaRelations=bkg_cat_via_rel,
            ParticleDescendants=part_desc,
            OutputLevel=output_level,
        )

        # define the bkg category functor
        self.BkgCat = F.BKGCAT(Relations=self.mctruth.BkgCatTable)

        # make accessible the MC association table and bkg category table
        self.MCAssocTable = (
            self.mctruth.MCAssocTable
        )  # map reconstructed particle to MCParticle (best match)
        self.BkgCatTable = (
            self.mctruth.BkgCatTable
        )  # map reconstructed particle to background category

    def __call__(self, mc_functor: BoundFunctor) -> BoundFunctor:
        """
        Retrieve the truth information of a reconstructed particle using a functor acting on an associated MCParticle.

        Args:
          Functor (Functors.grammar.BoundFunctor): The MC functor to be applied.

        Returns:
          BoundFunctor: A configured `MAP_INPUT` functor that maps
              the reconstructed particle to associated MCParticle
              and applies user-defined functor on the MCParticle.

        Example:
          # Create the helper class
          mctruth = MCTruthAndBkgCat(Particles_TES)

          # Store the TRUE momentum of the particle
          allvariables['TRUEP'] = mctruth(F.PT)
        """
        return F.MAP_INPUT(
            Functor=mc_functor, Relations=self.mctruth.MCAssocTable)
