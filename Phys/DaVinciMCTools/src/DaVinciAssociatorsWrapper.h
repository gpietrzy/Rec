/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DAVINCIASSOCIATORSWRAPPER_H
#define DAVINCIASSOCIATORSWRAPPER_H 1

// Include files
// from Gaudi
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IIncidentListener.h"
#include "Kernel/IDaVinciAssociatorsWrapper.h" // Interface

/** @class DaVinciAssociatorsWrapper DaVinciAssociatorsWrapper.h
 *
 *  Wrapper around Particle2MCLinker class
 *
 *  @author V. Gligorov (based on code by P. Koppenburg)
 *  @date   2008-06-26
 */
class DaVinciAssociatorsWrapper : public GaudiTool,
                                  virtual public IIncidentListener,
                                  virtual public IDaVinciAssociatorsWrapper {
public:
  /// Standard constructor
  DaVinciAssociatorsWrapper( const std::string& type, const std::string& name, const IInterface* parent );

  ~DaVinciAssociatorsWrapper(); ///< Destructor

  StatusCode initialize() override;
  StatusCode finalize() override;

  Object2FromMC<LHCb::Particle, GaudiTool>*
  linker( const Particle2MCMethod::AssociationMethod& method,
          const std::vector<std::string>& locations = std::vector<std::string>( 1, "" ) ) override; ///< returns a
                                                                                                    ///< linker

  //-------------------------------------------------------------
  void handle( const Incident& ) override; ///< clean desktop

private:
  Object2FromMC<LHCb::Particle, GaudiTool>* m_linker; ///< linker
};
#endif // DAVINCIASSOCIATORSWRAPPER_H
