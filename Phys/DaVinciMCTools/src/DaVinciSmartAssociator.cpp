/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureInterfaces/ICaloFuture2MCTool.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IBackgroundCategory.h"
#include "Kernel/IDaVinciAssociatorsWrapper.h"
#include "Kernel/IParticle2MCWeightedAssociator.h"
#include "Kernel/MCAssociation.h"
#include "Kernel/Particle2MCAssociatorBase.h"
#include "Kernel/Particle2MCLinker.h"

namespace LHCb {

  /** @class DaVinciSmartAssociator DaVinciSmartAssociator.h
   *
   *  A ``smart'' associator for any kind of particle. Returns a vector of
   *  MCAssociation objects, each of which represents a single associated MCParticle
   *  and its association weight. The association methods used are
   *
   *  Charged stables: Protoparticle associator
   *  Neutral stables: Neutral Protoparticle associator
   *  Composite: BackgroundCategory
   *
   *  For composites, there is always at most one associated MCParticle, whose
   *  association weight is set to 1 by definition
   *
   *  Implements IParticle2MCAssociator, but does not satisfy its rules, since the
   *  retrieval of associations of stable particles interacts with the linker
   *  mechanism on the TES, which can populate the TES with linker objects
   *  when these don't already exist, and thus has direct side-effects. It also has the
   *  added constraint that the LHCb::MCParticles to be associated
   *  HAVE to be on the TES always, and the LHCb::Particles have to be on the TES if they are
   *  stable. However, the results returned to the user are self-contained and independent
   *  of the TES.
   *
   *  @author V. Gligorov
   *  @date   2009-01-13
   */
  class DaVinciSmartAssociator : public Particle2MCAssociatorBase {

  public:
    using Particle2MCAssociatorBase::Particle2MCAssociatorBase;

  private:
    Particle2MCParticle::ToVector relatedMCPsImpl( const LHCb::Particle*                particle,
                                                   const LHCb::MCParticle::ConstVector& mcParticles ) const override;

    ToolHandle<IParticle2MCWeightedAssociator> m_weightedAssociation{this, "P2MCTool",
                                                                     "P2MCPFromProtoP"}; // for stables
    mutable ToolHandle<IBackgroundCategory>    m_bkg{this, "BackgroundCategoryTool",
                                                  "BackgroundCategory"}; // for composites
    mutable ToolHandle<ICaloFuture2MCTool>     m_calo2MC{this, "Calo2MCTool",
                                                     "CaloFuture2MCTool"}; // for neutral calorimetric
    Gaudi::Property<double>                    m_caloWeight{this, "Calo2MCWeight", 0.5};
    Gaudi::Property<bool>                      m_redoNeutral{this, "RedoNeutral", true};
  };

  // Declaration of the Tool Factory
  DECLARE_COMPONENT_WITH_ID( DaVinciSmartAssociator, "DaVinciSmartAssociator" )

} // namespace LHCb

Particle2MCParticle::ToVector
LHCb::DaVinciSmartAssociator::relatedMCPsImpl( const LHCb::Particle*                particle,
                                               const LHCb::MCParticle::ConstVector& mcps ) const {
  // We associate according to the particle type: protoparticle associators
  // are used for neutral and charged stable tracks, otherwise we use BackCat
  // for composites. The associator wrapper makes sure the linkers thus created are
  // deleted in the correct manner.

  if ( !particle ) Exception( "The smart associator was asked to associate a NULL particle, exiting." );

  Particle2MCParticle::ToVector associatedParts;

  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Performing smart association on " << *particle << endmsg;

  // Now we get the association result based on the particle type

  if ( m_calo2MC->isPureNeutralCaloFuture( particle ) && m_redoNeutral.value() ) {
    // pure neutral calo object (stable like gamma/mergedPi0 or composite like eta/resolvedPi0/Ks->pi0pi0/...)
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Associating a calorimetric particle with pid = " << particle->particleID().pid() << " "
                << m_calo2MC->from( particle )->descriptor() << endmsg;

    associatedParts.push_back(
        MCAssociation( m_calo2MC->from( particle )->findMCOrBest( particle->particleID(), m_caloWeight ), 1 ) );

  } else if ( particle->isBasicParticle() ) { // if this is a stable
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Associating a basic particle with pid = " << particle->particleID().pid() << endmsg;

    associatedParts = m_weightedAssociation->relatedMCPs( particle, mcps );

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Associated a basic particle with pid = " << particle->particleID().pid() << endmsg;
  } else { // If composite use BackCat
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Associating a composite particle with pid = " << particle->particleID().pid() << endmsg;
    associatedParts.push_back( MCAssociation( m_bkg->origin( particle ), 1. ) );
    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << "Associated a composite particle with pid = " << particle->particleID().pid() << endmsg;
  }

  // check if the associated MCPs are in the input container, if not,
  // remove the association!
  return Particle2MCParticle::FilterMCAssociations( associatedParts, mcps );
}
