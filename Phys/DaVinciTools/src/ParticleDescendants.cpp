/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
// Implementation file for class : ParticleDescendants
//
// 2005-10-19 : Patrick KOPPENBURG
//-----------------------------------------------------------------------------
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IParticleDescendants.h" // Interface

/** @class ParticleDescendants ParticleDescendants.h
 *
 *  Return the descendants of a Particle
 *
 *  @author Patrick KOPPENBURG
 *  @date   2005-10-19
 */
class ParticleDescendants : public extends<GaudiTool, IParticleDescendants> {

public:
  /// Standard constructor
  using extends::extends;

  // Return all descendants of a Particle
  const LHCb::Particle::ConstVector descendants( const LHCb::Particle* ) const override;

  // Return all descendants of level i for a Particle
  const LHCb::Particle::ConstVector descendants( const LHCb::Particle*, int ) const override;

  // Return all stable descendants of a Particle
  const LHCb::Particle::ConstVector finalStates( const LHCb::Particle* ) override;

private:
  // Return all daughters of a Particle
  bool addDaughters( const LHCb::Particle::ConstVector&, LHCb::Particle::ConstVector& ) const;

  // Return all daughters of a Particle
  bool addDaughters( const LHCb::Particle*, LHCb::Particle::ConstVector& ) const;
};

// Declaration of the Tool Factory
DECLARE_COMPONENT( ParticleDescendants )

//=============================================================================
// Return all descendants of a Particle
//=============================================================================
const LHCb::Particle::ConstVector ParticleDescendants::descendants( const LHCb::Particle* P ) const {
  return descendants( P, 0 );
}
//=============================================================================
// Return all stable descendants of a Particle
//=============================================================================
const LHCb::Particle::ConstVector ParticleDescendants::finalStates( const LHCb::Particle* P ) {
  LHCb::Particle::ConstVector stables;
  for ( const auto& i : descendants( P, 0 ) ) { // get them all all
    if ( i->isBasicParticle() ) {
      stables.push_back( i );
      debug() << "Saving a " << i->particleID().pid() << endmsg;
    } else
      verbose() << "Discarding a " << i->particleID().pid() << endmsg;
  }
  return stables;
}
//=============================================================================
// Return all daughters of particles in a vector
//=============================================================================
bool ParticleDescendants::addDaughters( const LHCb::Particle* M, LHCb::Particle::ConstVector& Parts ) const {
  if ( !M || M->isBasicParticle() ) return false;
  const auto& dauts = M->daughtersVector();
  Parts.insert( Parts.end(), dauts.begin(), dauts.end() );
  verbose() << "Added " << dauts.size() << " daughters" << endmsg;
  return !dauts.empty();
}
//=============================================================================
// Return all daughters of particles in a vector
//=============================================================================
bool ParticleDescendants::addDaughters( const LHCb::Particle::ConstVector& mothers,
                                        LHCb::Particle::ConstVector&       Parts ) const {
  bool found = false;
  for ( const auto& i : mothers ) found = ( addDaughters( i, Parts ) || found );
  return found;
}
//=============================================================================
// Return all descendants of level i for a Particle
//=============================================================================
const LHCb::Particle::ConstVector ParticleDescendants::descendants( const LHCb::Particle* P, int maxlevel ) const {

  LHCb::Particle::ConstVector Parts;

  int  level = 0;
  bool found = false;

  LHCb::Particle::ConstVector mothers;

  do {
    ++level;
    LHCb::Particle::ConstVector leveldaughters;
    if ( level == 1 )
      found = addDaughters( P, leveldaughters );
    else
      found = addDaughters( mothers, leveldaughters );
    if ( level == maxlevel || maxlevel == 0 ) {
      Parts.insert( Parts.end(), leveldaughters.begin(), leveldaughters.end() );
      verbose() << "Level: " << level << " - inserted " << leveldaughters.size() << " daughters to get " << Parts.size()
                << endmsg;
    }
    verbose() << "Level " << level << " of " << maxlevel << " : " << leveldaughters.size() << " daughters " << found
              << endmsg;
    mothers = leveldaughters;
  } while ( ( maxlevel <= 0 || level <= maxlevel ) && found );

  debug() << "Reached " << level << ". Returning " << Parts.size() << " daughters" << endmsg;

  return Parts;
}
