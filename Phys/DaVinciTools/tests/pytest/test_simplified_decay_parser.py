###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

import pytest
from DaVinciTools import SimplifiedDecayParser


def test_parsing():
    test_descriptors = [
        # Basics
        "B_s0",
        "B_s0 -> mu+ mu-",
        "[[B_s0]CC -> mu+ mu-]CC",
        "B_s0 -> (J/psi(1S) -> mu+ mu-) phi(1020)",
        "^[B_s0]CC",
        "([B_s0 -> mu+ mu-]CC)",
        "B_s0 -> mu+ mu-",
        "B_s0 -> (J/psi(1S) -> mu+ mu-) phi(1020)",
        "B_s0 -> mu+ mu-",
        "^([B_s0 -> mu+ mu-]CC)",
        # Nodes
        "^B_s0",
        "[B_s0]CC",
        "^[B_s0]CC",
        "^[K*_0(1430)~0]CC",
        "^[Pb208[0.0]]CC",
        # CC
        "B_s0 -> mu+ mu-",
        "B_s0 -> mu+ [mu-]CC",
        "[B_s0]CC -> [mu+]CC [mu-]CC",
        "[B_s0]CC -> [mu+]CC cc_1",
        "[[B_s0]CC -> mu+ [mu-]CC]CC",
        "[[B_s0]CC -> [mu+]CC [cc_1]CC]CC",
        # Hat
        "B_s0 -> mu+ mu-",
        "^B_s0 -> mu+ mu-",
        "^(B_s0 -> mu+ mu-)",
        "^(^B_s0 -> mu+ mu-)",
        "B_s0 -> ^mu+ mu-",
        "B_s0 -> ^(K*(892)0 -> K+ pi-) phi(1020)",
        "B_s0 -> (K*(892)0 -> ^K+ pi-) phi(1020)",
        # CC and Hat
        "^[B_s0]CC -> mu+ mu-",
        "^([B_s0 -> mu+ mu-]CC)",
        "^([^B_s0 -> mu+ mu-]CC)",
        "^([^[B_s0]CC -> mu+ mu-]CC)",
        "B_s0 -> ^[mu+]CC mu-",
        "[B_s0 -> ^[mu+]CC mu-]CC",
        "B_s0 -> ^([K*(892)0 -> K+ pi-]CC) phi(1020)",
        "B_s0 -> (K*(892)0 -> ^[K+]CC pi-) phi(1020)",
        "B_s~0 -> ^([K*_0(1430)~0 -> K- pi+]CC) K*(892)~0",
        "[Pb208[0.0] -> ^[Pb206[0.0]]CC [pi-]CC]CC",
        # Whitespaces
        "  ^  [  K*_0(1430)~0  ]  CC",
        "  ^  [  Pb208[0.0]  ]  CC",
        "B_s0->mu+ mu-",
        "     B_s0    -> mu+       mu-",
        "B_s0 -> [    mu+  ] CC cc_1",
        "  [   B_s0 -> mu+ cc_1 ]     CC",
        "  [   B_s0 -> mu+ [   cc_1 ]  CC ]     CC",
        "B_s0 -> mu+ ^  mu-",
        "   ^   B_s0 -> mu+ mu-",
        "   ^   B_s0 -> mu+ [   mu-  ] CC",
        "[  ^  B_1(H)0 -> [Pb208[0.0]]CC [  K*_0(1430)~0  ]  CC  ]  CC",
        "[  ^  B_1(H)0 -> [  mu+ ] CC CLUSjet    CELLjet]CC",
        "[  ^  B_1(H)0 -> [  mu+ ] CC [CLUSjet]CC    CELLjet]CC",
        " Pb208[0.0]  -> Pb206[0.0] ^ [  Pb204[0.0]  ] CC CELLjet",
        # Complex decays
        "B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) (K*(892)0 -> K+ pi-) (K*_0(1430)~0 -> K+ gamma)",
        "B_s0 -> (K*_4(2045)0 -> (K*_0(1430)~0 -> (K*(892)0 -> K+ pi-) pi0) rho(1700)0) phi(1020)",
        "[[B_s0]CC -> ([K*_0(1430)~0 -> K*(892)0 [pi+]CC [pi-]CC]CC) phi(1020)]CC",
        "[[B_s0]CC -> ^([K*_0(1430)~0 -> K*(892)0 ^[pi+]CC [pi-]CC]CC) phi(1020)]CC",
        # Decay possibilities
        "[B_s0 -> [K*_0(1430)~0]CC K*(892)0]CC",
        "[B_s0]CC -> ([K*_0(1430)~0 -> K*(892)0 pi+ pi-]CC) (K*(892)0 -> ^[K+]CC pi-)",
        # Parsing arrows
        "B_s0 -> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 -> (K*_0(1430)~0 --> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 --> (K*_0(1430)~0 => K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 => (K*_0(1430)~0 ==> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 ==> (K*_0(1430)~0 -x> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 -x> (K*_0(1430)~0 --x> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 --x> (K*_0(1430)~0 =x> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 =x> (K*_0(1430)~0 ==x> K*(892)0 pi+ pi-) K*(892)0",
        "B_s0 ==x> (K*_0(1430)~0 -> K*(892)0 pi+ pi-) K*(892)0",
    ]
    for des in test_descriptors:
        try:
            parsed = SimplifiedDecayParser(des)
            parsed.expand_cc()
            parsed.list_cc_in_self_conjugate()
        except Exception as e:
            raise RuntimeError(
                f"Error: Can't parse the decay '{des}', message: {e}")
