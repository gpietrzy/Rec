###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/DaVinciTransporter
-----------------------
#]=======================================================================]

gaudi_add_module(DaVinciTransporter
    SOURCES
        src/ParticleTransporter.cpp
        src/ParticleTransporterWithStateProvider.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::TrackEvent
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
        Rec::TrackInterfacesLib
)
