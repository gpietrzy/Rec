/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "TrackInterfaces/ITrackExtrapolator.h" // TrackExtrapolator

#include "Event/Particle.h"
#include "Event/TrackTypes.h" /// @todo temporary
#include "Kernel/IParticle2State.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/IParticleTransporter.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/TransporterFunctions.h"
#include "LHCbMath/MatrixManip.h"

#include "GaudiAlg/GaudiTool.h"

/**
 *  Tool to transport an LHCb::Particle to a new Z position.
 *  For composite or neutral particles, there is a choice of
 *  simple transport or a transport plus a projeciton into
 *  track-like 5x5 space.
 *
 *  For all charged, basic particles, an ITrackExtrapolator is used. The
 *  implementation is controlled by the "TrackExtrapolator" property, it's
 *  default value being "TrackParabolicExtrapolator". The basic principle
 *  here is to obtain a 5x5 track-space state from the Particle, transport
 *  it with the track extrapolator, and project the transported state into
 *  Particle 7x7 space. For electrons and particles without an
 *  associated LHCb::ProtoParticle (assumed to be from MCParticles) the 5x5
 *  state is obtained from the Particle. For all other charged, basic particles
 *  it is obtained from the track state closest to the Z position to which
 *  the particle is to be transported.
 *  The projections between 5x5 track and 7x7 particle space are performed
 *  using the Particle2State implementation of IParticle2State.
 *
 *  @todo state method does a lot of useful stuff, yet is private. See if the
 *  funcitonality can be put in a tool or incorporated into an IParticleToState
 *  implementation.
 *
 *  @author Patrick KOPPENBURG
 *  @date   2006-01-20
 *
 *  @author Juan Palacios juan.palacios@nikhef.nl
 *  @date   2008-04-15
 */
class ParticleTransporter : public extends<GaudiTool, IParticleTransporter> {

public:
  /// Standard constructor
  ParticleTransporter( const std::string& type, const std::string& name, const IInterface* parent );

  StatusCode initialize() override; ///< Initialize

  /// Transport a Particle to specified z position.
  StatusCode transport( LHCb::Particle const*, double zNew, LHCb::Particle& transParticle,
                        IGeometryInfo const& geometry ) const override;

  StatusCode transportAndProject( LHCb::Particle const*, double zNew, LHCb::Particle& transParticle,
                                  IGeometryInfo const& geometry ) const override;

private:
  /**
   * Get or make a state from a Particle
   *
   * @author Patrick Koppenburg
   *
   */
  StatusCode state( const LHCb::Particle* P, const double zNew, LHCb::State& ) const;

  /**
   * Transport a charged, basic LHCb::Particle to a new Z position.
   * It simply obtains an LHCb::State using the state method, propagates
   * it using the ITrackExtrapolator, and transforms back to 7x7 space
   * using the IParticle2State tool.
   *
   * @param particle Input LHCb::Particle.
   * @param zNew     Z-position to which particle is to be transported to.
   * @param transParticle transported equivalent of input particle.
   * @return StatusCode FAILURE if the track state creation, track propagation
   * or particle stare creation fail. SUCCESS otherwise.
   *
   *
   * @author Juan Palacios juan.palacios@nikhef.nl
   *
   */

  StatusCode transportChargedBasic( LHCb::Particle const* particle, double zNew, LHCb::Particle& transParticle,
                                    IGeometryInfo const& geometry ) const;

  /**
   *  Check a transported Particle for infinite momentum and print
   *  an error warning if necessary.
   *
   *  @param transParticle The transported LHCb::Particle.
   *  @return StautsCode::FAILURE if the particle's momentum is infinity.
   *
   *  @author Patrick Koppenburg
   *  @author Juan Palacios
   *
   */
  StatusCode checkParticle( const LHCb::Particle& transParticle ) const;

  /** Returns the full location of the given object in the Data Store
   *
   *  @param pObj Data object
   *
   *  @return Location of given data object
   */
  inline std::string location( const DataObject* pObj ) const {
    return ( !pObj ? "Null DataObject !" : ( pObj->registry() ? pObj->registry()->identifier() : "UnRegistered" ) );
  }

private:
  ITrackExtrapolator* m_trackExtrapolator; ///< Track extrapolator for particles from tracks

  std::string m_trackExtrapolatorName; ///< Type of Track extrapolator for particles from tracks

  /// Accessor for ParticlePropertySvc
  LHCb::IParticlePropertySvc* m_ppSvc;

  /// Particle to state convertion tool
  IParticle2State* m_particle2State;

  /// Electron ID
  unsigned int m_eID;

  /// measure CPU performance ?
  bool m_timing;
};
