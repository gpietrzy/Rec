###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.Algorithms import DecayTreeFitterAlg_v1Particle, DecayTreeFitterAlg_v1Particle_BestPV
import Functors as F


def DTFAlg(Input, InputPVs=None, MassConstraints=[], OutputLevel=3):
    """
    Wrapper function for the DecayTreeFitterAlg_v1Particle and DecayTreeFitterAlg_v1Particle_v1Vertex algorithms.

    Args:
        Input: Location of input particles (to be refitted).
        InputPVs (DataHandle or None, optional): Location of primary vertices to which to constrain.
            Defaults to None.
        MassConstrains (list of str, optional): list of mass constraints to apply. Defaults to [].
        OutputLevel (int, optional): OutputLevel of algorithm. Defaults to INFO=3.

    Returns:
        Configured DecayTreeFitterAlg_v1Particle or DecayTreeFitterAlg_v1Particle_v1Vertex instance.
    """
    print(
        '#Warning: DecayTreeFitter.DTFAlg deprecated. Use DecayTreeFitter.DecayTreeFitter'
    )
    if InputPVs:
        DTF = DecayTreeFitterAlg_v1Particle_BestPV(
            Input=Input,
            InputPVs=InputPVs,
            MassConstraints=MassConstraints,
            OutputLevel=OutputLevel)
    else:
        DTF = DecayTreeFitterAlg_v1Particle(
            Input=Input,
            MassConstraints=MassConstraints,
            OutputLevel=OutputLevel)
    return DTF


def DTF_functors(DTF, functors=[F.MASS], head='DTF_'):
    """
    Helper function returning a dictionary of functors to apply to DecayTreeFitted chain

    Args:
          functors: list of Functors (default: mass)
          DTF: Configured DecayTreeTupleAlg as returned by DTFAlg
          head: header string. The default is "DTF_" and so F.PT of a refitted B will be decoded to "B_DTF_PT"

    Returns:
          Dictionary of names. By default it will return { 'DTF_Mass' : F.MAP_INPUT(Functor=F.Mass, Relations=DTF.OutputRelations) }

    Examples:
    >>>
    >>> from DecayTreeFitter import DTFAlg, DTF_functors
    >>> DTF_pv = DTFAlg(
    >>>     Input=dimuons,
    >>>     InputPVs=pvs,
    >>>     MassConstraints=["J/psi(1S)"])
    >>> variables_jpsi.update(DTF_functors(DTF_pv, functors=[F.PT,F.MASS], head='DTF_PV_'))
    >>>

    """
    print(
        '#Warning: DecayTreeFitter.DTF_functors deprecated. Use DecayTreeFitter.DecayTreeFitter'
    )
    outdict = {}
    for fct in functors:
        outdict[head + fct.name()] = F.MAP_INPUT(
            Functor=fct, Relations=DTF.OutputRelations)
    return outdict
