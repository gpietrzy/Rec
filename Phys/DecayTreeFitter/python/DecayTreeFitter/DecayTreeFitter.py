###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from PyConf.dataflow import DataHandle
from Gaudi.Configuration import INFO
from Functors.grammar import BoundFunctor
import Functors as F
from PyConf.Algorithms import DecayTreeFitterAlg_v1Particle, DecayTreeFitterAlg_v1Particle_BestPV, DecayTreeFitterAlg_v1Particle_AllPVs, RecV1ToPVConverter

# return RecV1ToPVConverter(InputVertices=get_pvs_v1()).OutputVertices
from DaVinciTools import SubstitutePID
from typing import List


class DecayTreeFitter:
    """
    Python tool to help with the configuration of the Decay Tree Fit (DTF) class
    and the creation of functors to access the necessary information.

    .. note::
        You can configure the mass hypothesis subsitution using the following syntax:
            ' Old_PID{{New_PID}} '

        The '[]cc' syntax is not yet supported in the SubstitutePID tool, you have to
        specify the substitution rule explicitly for each cc case.
        e.g.
        '[B+ -> K+ K+ K-{{pi-}}]cc' => ['B+ -> K+ K+ K-{{pi-}}', 'B- -> K- K- K+{{pi+}}']

    Args:
        name (str): name of the substitution algorithm, will printed in the Gaudi.
        input_particles (DataHandle): TES location of the reconstructed particles.
        input_pvs (DataHandle or None, optional): TES location of primary vertices to which to constrain.
            Defaults to None.
        mass_constraints (list of str, optional): list of mass constraints to apply. Defaults to [].
        substitutions (list): substitution rules, using the substitution syntax.
        output_level (int, optional): standard Gaudi Algorithm OutputLevel.
    Example:
        pvs = get_pvs()

        # Create the tool
        DTF_PhiG = DecayTreeFitter(
            name = 'DTF_PhiG',
            input_particles = B_Data,
            substitutions = [
                'B0{{B_s0}} -> ( K*(892)0{{phi(1020)}} ->  K+ K- ) gamma'
            ],
            mass_constraints=['B_s0'],
            input_pvs=pvs,
        )

        DTF_KstG = SubstitutePID(
            name = 'DTF_KstG',
            input_particles = B_Data,
            substitutions = [
                'B0 -> ( K*(892)0 -> K+ K-{{pi-}} ) gamma'
            ],
            mass_constraints=['B0'],
            input_pvs=pvs,
        )

        # Get the CHI2DOF of subtituted particle
        allvariables['CHI2DOF'] = F.CHI2DOF
        allvariables['PhiG_CHI2DOF'] = DTF_PhiG( F.CHI2DOF )
        allvariables['KstG_CHI2DOF'] = DTF_KstG( F.CHI2DOF )
    """

    def __init__(self,
                 name: str,
                 input_particles: DataHandle,
                 input_pvs: DataHandle = None,
                 mass_constraints: List[str] = [],
                 substitutions: List[str] = [],
                 constrain_to_ownpv=False,
                 fit_all_pvs=False,
                 output_level=INFO):
        if fit_all_pvs and input_pvs is None:
            raise ValueError(
                "The 'input_pvs' parameter must not be set to None when fitting all PVs."
            )

        # Status
        self.FitAllPVs = fit_all_pvs

        # Config subsitution if needed
        if substitutions:
            self.SubstitutePID = SubstitutePID(
                name='PIDSubstitution_' + name,
                input_particles=input_particles,
                substitutions=substitutions,
                output_level=output_level)
            DTF_input = self.SubstitutePID.Particles
        else:
            self.SubstitutePID = None
            DTF_input = input_particles

        # Config algorithm
        if input_pvs:
            self.HasPVConstraint = True
            gaudi_algorithm = DecayTreeFitterAlg_v1Particle_AllPVs if fit_all_pvs else DecayTreeFitterAlg_v1Particle_BestPV
            # If is v2 vertex
            if 'LHCb::Event::PV::PrimaryVertexContainer' in input_pvs.type:
                gaudi_pvs = input_pvs
            elif 'LHCb::RecVertex' in input_pvs.type:
                # Convert the v1 PVs to v2 PVs
                gaudi_pvs = RecV1ToPVConverter(
                    InputVertices=input_pvs).OutputVertices
            else:
                raise ValueError(
                    'Invalid input_pvs type, input_pvs = {input_pvs.type}.')
            self.Algorithm = gaudi_algorithm(
                name=name,
                Input=DTF_input,
                InputPVs=input_pvs,
                MassConstraints=mass_constraints,
                OutputLevel=output_level)
        else:
            self.HasPVConstraint = constrain_to_ownpv
            self.Algorithm = DecayTreeFitterAlg_v1Particle(
                name=name,
                Input=DTF_input,
                MassConstraints=mass_constraints,
                usePVConstraint=constrain_to_ownpv,
                OutputLevel=output_level)

        self.Output = self.Algorithm.Output
        self.OutputRelations = self.Algorithm.OutputRelations  #Reco particle -> DTF particle
        self.OutputParticleParams = self.Algorithm.ParticleParams
        self.OutputNIter = self.Algorithm.NIter

        # Functor to access fit result directly
        self.NITER = F.VALUE_OR(-1) @ self._apply_functor(
            functor=F.CAST_TO_INT, relation=self.OutputNIter)
        self.CHI2 = self.__call__(
            functor=F.CHI2 @ F.ENDVERTEX, apply_to_particle_params=False)
        self.NDOF = F.VALUE_OR(-1) @ self.__call__(
            functor=F.VALUE_OR(-1) @ F.NDOF @ F.ENDVERTEX,
            apply_to_particle_params=False)
        self.CHI2DOF = self.__call__(
            functor=F.CHI2DOF @ F.ENDVERTEX, apply_to_particle_params=False)
        self.MASS = self.__call__(
            functor=F.MATH_VALUE @ F.MATH_INVARIANT_MASS @ F.FOURMOMENTUM,
            apply_to_particle_params=True)
        self.MASSERR = self.__call__(
            functor=F.MATH_ERROR @ F.MATH_INVARIANT_MASS @ F.FOURMOMENTUM,
            apply_to_particle_params=True)
        self.P = self.__call__(
            functor=F.MATH_VALUE @ F.MATH_SCALAR_MOMENTUM @ F.FOURMOMENTUM,
            apply_to_particle_params=True)
        self.PERR = self.__call__(
            functor=F.MATH_ERROR @ F.MATH_SCALAR_MOMENTUM @ F.FOURMOMENTUM,
            apply_to_particle_params=True)
        self.CTAU = self.__call__(
            functor=F.MATH_VALUE @ F.PARTICLE_PARAMS_CTAU,
            apply_to_particle_params=True)
        self.CTAUERR = self.__call__(
            functor=F.MATH_ERROR @ F.PARTICLE_PARAMS_CTAU,
            apply_to_particle_params=True)
        self.FD = self.__call__(
            functor=F.MATH_VALUE @ F.PARTICLE_PARAMS_FLIGHT_DISTANCE,
            apply_to_particle_params=True)
        self.FDERR = self.__call__(
            functor=F.MATH_ERROR @ F.PARTICLE_PARAMS_FLIGHT_DISTANCE,
            apply_to_particle_params=True)

    def _apply_functor(self, functor: BoundFunctor, relation: DataHandle):
        '''
        Internal function that apply functor with certain relation
        '''

        if self.FitAllPVs and functor is None:
            DTF_functor = F.MAP(F.TO) @ F.RELATIONS.bind(
                F.TES(relation), F.FORWARDARGS)
        elif self.FitAllPVs and functor is not None:
            DTF_functor = F.MAP_INPUT_ARRAY(
                Functor=functor, Relations=relation)
        elif not self.FitAllPVs and functor is None:
            DTF_functor = F.MAP_TO_RELATED(Relations=relation)
        else:
            DTF_functor = F.MAP_INPUT(functor, relation)

        if self.SubstitutePID is not None:
            #The following maps the particle twice i.e. MAP_INPUT(MAP_INPUT(functor, P_2_DTFP), P_2_PIDSUBSTITUTEDP).
            #Go from reco particle to PID substituted particle, then to DTF particle, then apply the functor
            return self.SubstitutePID(DTF_functor)
        else:
            return DTF_functor

    def __call__(self,
                 functor: BoundFunctor,
                 apply_to_particle_params: bool = False):
        """
        Apply a specified functor to the resultant particle
        obtained from the decay tree fit or the resultant particle
        parameters.

        Args:
            functor (BoundFunctor): the functor to be applied
            apply_to_particle_params (bool, optional): apply to the resultant particle parameters. Defaults to False

        Return:
            BoundFunctor: result functor

        Example:
            variables['DTF_CHI2DOF'] = DTF(F.CHI2DOF)
        """
        if apply_to_particle_params:
            relation = self.OutputParticleParams
        else:
            relation = self.OutputRelations

        return self._apply_functor(functor=functor, relation=relation)

    def apply_functors(self,
                       functors: List[BoundFunctor] = [F.MASS],
                       apply_to_particle_params: bool = False,
                       head: str = 'DTF_'):
        """
        Helper function returning a dictionary of functors to apply to DecayTreeFitted chain

        Args:
            functors: list of Functors (default: mass)
            apply_to_particle_params (bool, optional): apply to the resultant particle parameters. Defaults to False
            head: header string. The default is "DTF_" and so F.PT of a refitted B will be decoded to "B_DTF_PT"

        Returns:
            Dictionary of names. By default it will return { 'DTF_Mass' : DTF(F.Mass) }

        Examples:
        >>>
        >>> from DecayTreeFitter import DecayTreeFitter
        >>> DTF_pv = DecayTreeFitter(
        >>>     name='DTF_dimuons',
        >>>     input_particles=dimuons,
        >>>     input_pvs=pvs,
        >>>     mass_constraints=["J/psi(1S)"])
        >>> variables_jpsi.update(DTF_pv.apply_functors(functors=[F.PT,F.MASS], head='DTF_PV_'))
        """
        outdict = {}
        for functor in functors:
            outdict[head + functor.name()] = self.__call__(
                functor, apply_to_particle_params=apply_to_particle_params)
        return outdict
