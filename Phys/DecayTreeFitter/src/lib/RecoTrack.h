/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef RECOTRACK_H
#define RECOTRACK_H

#include "Configuration.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "RecoParticle.h"

struct ITrackStateProvider;
namespace LHCb {
  class TrackTraj;
}

namespace DecayTreeFitter {

  class RecoTrack : public RecoParticle {
  public:
    RecoTrack( const LHCb::Particle& bc, const ParticleBase* mother, const Configuration& config );
    virtual ~RecoTrack();

    ErrCode initPar2( FitParams*, IGeometryInfo const& geometry ) override;
    ErrCode initCov( FitParams* ) const override;
    int     dimM() const override { return 5; }
    int     type() const override { return kRecoTrack; }

    ErrCode projectRecoConstraint( FitParams const&, Projection&, IGeometryInfo const& geometry ) const override;
    ErrCode updCache( FitParams const& fitparams, IGeometryInfo const& geometry );
    // tatic void setApplyCovCorrection(bool b=true) { gApplyCovCorrection = b ; }
    // static void correctCov(HepSymMatrix& V) ;

    int nFinalChargedCandidates() const override { return 1; }

    void addToConstraintList( constraintlist& alist, int depth ) const override {
      alist.push_back( Constraint( this, Constraint::track, depth, dimM() ) );
    }
    // ErrCode updFltToMother(const FitParams& fitparams) ;
    void               setFlightLength( double flt ) { m_flt = flt; }
    const LHCb::Track& track() const { return *m_track; }
    const LHCb::State& state() const { return m_state; }

    // return a trajectory (declared by base class)
    const LHCb::Trajectory<double>* trajectory( IGeometryInfo const& geometry ) const override;

    // return a tracktraj
    const LHCb::TrackTraj* tracktraj( IGeometryInfo const& geometry ) const;

  private:
    const Gaudi::XYZVector                   m_bfield;
    const LHCb::Track*                       m_track{nullptr};
    const ITrackStateProvider*               m_stateprovider{nullptr};
    bool                                     m_useTrackTraj;
    mutable std::unique_ptr<LHCb::TrackTraj> m_tracktraj_container;
    const LHCb::TrackTraj*                   m_tracktraj{nullptr};
    bool                                     m_ownstracktraj{false};
    bool                                     m_cached{false};
    double                                   m_flt{0};
    LHCb::State                              m_state;
    double                                   m_bremEnergy{0};
    double                                   m_bremEnergyCov{0};
    double                                   m_maxCovVeloTrack =
        75000. * 75000; // Maximum value for the initial covariance matrix' elements, only for VELO tracks
  };

} // namespace DecayTreeFitter
#endif
