/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "../TaggingClassifierTMVA.h"

#include <cmath>
#include <iostream>
#include <vector>

class OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0 : public TaggingClassifierTMVA {

public:
  OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0();

  double GetMvaValue( const std::vector<double>& featureValues ) const override;

private:
  // method-specific destructor
  void Clear();

  // input variable transformation

  double fDecTF_1[3][12][12];

  double fMin_2[3][12];
  double fMax_2[3][12];
  void   InitTransform_1();
  void   Transform_1( std::vector<double>& iv, int sigOrBgd ) const;
  void   InitTransform_2();
  void   Transform_2( std::vector<double>& iv, int sigOrBgd ) const;
  void   InitTransform();
  void   Transform( std::vector<double>& iv, int sigOrBgd ) const;

  // common member variables
  const char* fClassName = "OSVtxCh_Data_Run1_All_Bu2JpsiK_TMVA_MLP_v1r0";

  const size_t fNvars = 12;
  size_t       GetNvar() const { return fNvars; }
  char         GetType( int ivar ) const { return fType[ivar]; }

  // normalisation of input variables
  const bool   fIsNormalised = false;
  bool         IsNormalised() const { return fIsNormalised; }
  const double fVmin[12] = {-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1};
  const double fVmax[12] = {1, 1, 1, 1, 1, 0.99999988079071, 1, 1, 1, 0.99999988079071, 1, 1};

  double NormVariable( double x, double xmin, double xmax ) const {
    // normalise to output range: [-1, 1]
    return 2 * ( x - xmin ) / ( xmax - xmin ) - 1.0;
  }

  // type of input variable: 'F' or 'I'
  const char fType[12] = {'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F', 'F'};

  // initialize internal variables
  void   Initialize();
  double GetMvaValue__( const std::vector<double>& inputValues ) const;

  // private members (method specific)

  double ActivationFnc( double x ) const;
  double OutputActivationFnc( double x ) const;

  int    fLayers;
  int    fLayerSize[3];
  double fWeightMatrix0to1[18][13]; // weight matrix from layer 0 to 1
  double fWeightMatrix1to2[1][18];  // weight matrix from layer 1 to 2

  double* fWeights[3];
};
