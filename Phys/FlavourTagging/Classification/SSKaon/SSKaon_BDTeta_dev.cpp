/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "SSKaon_BDTeta_dev.h"

// hack, otherwise: redefinitions...
//
namespace MyBDTetaSpace {
#include "weights/BDTeta_SSK_dev/TMVAClassification_BDTG_BsBsb_6vars_BDTGselTCut_0.72.class.C"
}

BDTetaReaderCompileWrapper::BDTetaReaderCompileWrapper( std::vector<std::string>& names )
    : BDTetareader( new MyBDTetaSpace::ReadBDTG_BsBsb_6vars_BDTGselTCut_072( names ) ) {}

BDTetaReaderCompileWrapper::~BDTetaReaderCompileWrapper() { delete BDTetareader; }

double BDTetaReaderCompileWrapper::GetMvaValue( std::vector<double> const& values ) const {
  return BDTetareader->GetMvaValue( values );
}
