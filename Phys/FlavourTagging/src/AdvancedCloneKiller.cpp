/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Core/FloatComparison.h"
#include "Event/Particle.h"
#include "Event/Track.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "TrackKernel/TrackCloneData.h"
#include "Utils/TaggingHelper.h"
#include <Gaudi/Accumulators.h>

class AdvancedCloneKiller : public LHCb::Algorithm::MergingTransformer<LHCb::Particle::Selection(
                                const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>& inputParticles )> {
public:
  /// Standard constructor
  AdvancedCloneKiller( const std::string& name, ISvcLocator* pSvcLocator )
      : MergingTransformer( name, pSvcLocator, {"InputParticles", {""}}, {"OutputParticles", ""} ) {}

  LHCb::Particle::Selection
  operator()( const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>& inputParticles ) const override;

private:
  TaggingHelper::CloneStatus cloneCategory( const LHCb::Particle& p1, const LHCb::Particle& p2 ) const;
  /* bool areHitFractionClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areMomentumParameterClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areSlopeClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areSlopeChargeClones(const LHCb::Particle& p1, const LHCb::Particle& p2) const;
  bool areConvertedGamma(const LHCb::Particle& p1, const LHCb::Particle& p2) const; */

  Gaudi::Property<double> m_maxSharedHitFraction{this, "MaxSharedHitFraction", 0.3};

  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{this, "#InputParticles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_outCount{this, "#OutputParticles"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( AdvancedCloneKiller )

LHCb::Particle::Selection AdvancedCloneKiller::
                          operator()( const Gaudi::Functional::vector_of_const_<LHCb::Particle::Range>& inputParticles ) const {
  LHCb::Particle::Selection allParticles;

  for ( auto const& particleList : inputParticles ) { allParticles.insert( particleList.begin(), particleList.end() ); }
  m_inCount += allParticles.size();

  // sort them by quality
  auto qualitySort = []( const LHCb::Particle* p1, const LHCb::Particle* p2 ) {
    const LHCb::Track& t1 = *p1->proto()->track();
    const LHCb::Track& t2 = *p2->proto()->track();
    // prefer tracks which have more subdetectors in
    // where TT counts as half a subdetector
    const unsigned nSub1 = ( t1.hasVelo() ? 2 : 0 ) + ( t1.hasTT() ? 1 : 0 ) + ( t1.hasT() ? 2 : 0 );
    const unsigned nSub2 = ( t2.hasVelo() ? 2 : 0 ) + ( t2.hasTT() ? 1 : 0 ) + ( t2.hasT() ? 2 : 0 );
    if ( nSub2 < nSub1 ) return true;
    if ( nSub1 < nSub2 ) return false;

    // if available, prefer lower ghost probability
    const double ghProb1 = t1.ghostProbability();
    const double ghProb2 = t2.ghostProbability();
    if ( -0. <= ghProb1 && ghProb1 <= 1. && -0. <= ghProb2 && ghProb2 <= 1. ) {
      if ( ghProb1 < ghProb2 ) return true;
      if ( ghProb2 < ghProb1 ) return false;
    }

    // prefer longer tracks
    if ( t2.nLHCbIDs() < t1.nLHCbIDs() ) return true;
    if ( t1.nLHCbIDs() < t2.nLHCbIDs() ) return false;

    // for same length tracks, have chi^2/ndf decide
    const double chi1 = t1.chi2() / double( t1.nDoF() );
    const double chi2 = t2.chi2() / double( t2.nDoF() );
    if ( chi1 < chi2 ) return true;
    if ( chi2 < chi1 ) return false;

    // fall back on a pT comparison (higher is better) as last resort
    return p2->pt() < p1->pt();
  };

  std::stable_sort( allParticles.begin(), allParticles.end(), qualitySort );

  auto isClone = [&]( const LHCb::Particle* p, const LHCb::Particle::Selection& sel ) {
    const auto firstClone = std::find_if( sel.begin(), sel.end(), [&]( const LHCb::Particle* p2 ) {
      auto cc = cloneCategory( *p, *p2 );
      return cc;
    } );
    if ( firstClone != sel.end() ) return true;
    return false;
  };

  LHCb::Particle::Selection outputParticles;
  std::for_each( allParticles.begin(), allParticles.end(), [&]( const LHCb::Particle* p ) {
    if ( !isClone( p, outputParticles ) ) outputParticles.insert( p );
  } );

  m_outCount += outputParticles.size();
  return outputParticles;
}

TaggingHelper::CloneStatus AdvancedCloneKiller::cloneCategory( const LHCb::Particle& p1,
                                                               const LHCb::Particle& p2 ) const {

  if ( TaggingHelper::areHitFractionClones( p1, p2, m_maxSharedHitFraction ) )
    return TaggingHelper::CloneStatus::CloneHitContent;
  if ( TaggingHelper::areMomentumParameterClones( p1, p2 ) ) return TaggingHelper::CloneStatus::CloneTrackParams;
  if ( TaggingHelper::areSlopeClones( p1, p2 ) ) return TaggingHelper::CloneStatus::MattTxTy;
  if ( TaggingHelper::areSlopeChargeClones( p1, p2 ) ) return TaggingHelper::CloneStatus::MattTxTyQp;
  if ( TaggingHelper::areConvertedGamma( p1, p2 ) ) return TaggingHelper::CloneStatus::ConvertedGamma;

  // as far as we can tell, the two particles are different
  return TaggingHelper::CloneStatus::DifferentParticles;
}
