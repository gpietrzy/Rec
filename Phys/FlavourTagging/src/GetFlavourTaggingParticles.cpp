/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "Event/Particle.h"
#include "Kernel/IDistanceCalculator.h"
#include "Kernel/IRelatedPVFinder.h"
#include "LHCbAlgs/Transformer.h"
#include "Utils/TaggingHelperTool.h"

class GetFlavourTaggingParticles
    : public LHCb::Algorithm::Transformer<LHCb::Particle::Selection(
                                              const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                              const LHCb::PrimaryVertices&, const DetectorElement& ),
                                          LHCb::DetDesc::usesConditions<DetectorElement>> {
public:
  GetFlavourTaggingParticles( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingParticles", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"FlavourTaggingParticles", ""} ) {}

  LHCb::Particle::Selection operator()( const LHCb::Particle::Range& bCandidates,
                                        const LHCb::Particle::Range& taggingParticles,
                                        const LHCb::PrimaryVertices& primaryVertices,
                                        const DetectorElement& ) const override;

private:
  Gaudi::Property<double> m_minIpChi2{this, "MinIpChi2", 6.,
                                      "Tagging particle requirement: Minimum IPChi2 wrt B primary vertex"};

  ToolHandle<TaggingHelperTool> m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};

  mutable Gaudi::Accumulators::SummingCounter<> m_inCount{this, "#InputParticles"};
  mutable Gaudi::Accumulators::SummingCounter<> m_outCount{this, "#OutputParticles"};
};

DECLARE_COMPONENT( GetFlavourTaggingParticles )

LHCb::Particle::Selection GetFlavourTaggingParticles::operator()( const LHCb::Particle::Range& bCandidates,
                                                                  const LHCb::Particle::Range& taggingParticles,
                                                                  const LHCb::PrimaryVertices& primaryVertices,
                                                                  const DetectorElement&       lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  LHCb::Particle::Selection selectedParticles;

  for ( const auto* particle : taggingParticles ) {
    const LHCb::VertexBase* bestParticlePV = LHCb::bestPV( primaryVertices, *particle );
    bool                    hasSamePV      = false;

    for ( const auto* bCand : bCandidates ) {
      const LHCb::VertexBase* bestbCandPV = LHCb::bestPV( primaryVertices, *bCand );
      if ( bestbCandPV == bestParticlePV ) {
        selectedParticles.insert( particle );
        hasSamePV = true;
        break;
      }
    }

    if ( !hasSamePV ) {
      double minIPChi2 = m_taggingHelperTool->calcMinIPChi2( *particle, primaryVertices, geometry );
      if ( minIPChi2 > m_minIpChi2 ) { selectedParticles.insert( particle ); }
    }
  }

  m_inCount += taggingParticles.size();
  m_outCount += selectedParticles.size();
  return selectedParticles;
}
