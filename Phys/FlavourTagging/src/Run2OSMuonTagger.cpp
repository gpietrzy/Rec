/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "../Classification/OSMuon/OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0.h"
#include "DetDesc/DetectorElement.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class Run2OSMuonTagger
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                                             const LHCb::PrimaryVertices&, const DetectorElement& ),
                                          LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  Run2OSMuonTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingMuons", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particle::Range& bCandidates, const LHCb::Particle::Range& taggingKaons,
                                const LHCb::PrimaryVertices& primaryVertices, const DetectorElement& ) const override;

private:
  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingKaons,
                                              const LHCb::PrimaryVertices& primaryVertices,
                                              const IGeometryInfo&         geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::Run2_OSMuon; }

  // cut values for OSMuon selection
  Gaudi::Property<double> m_minDistPhi{this, "MinDistPhi", 0.0029,
                                       "Tagging particle requirement: Minimum phi distance to B daughters"};
  Gaudi::Property<double> m_minIpSigTagBestPV{this, "MinIpSigTagBestPV", 0.437,
                                              "Tagging particle requirement: Minimum IP significance wrt to best PV"};
  Gaudi::Property<double> m_minIpSigTagPileUpVertices{
      this, "MinIpSigPileUp", 3.91, "Tagging particle requirement: Minimum IP significance wrt to all pile-up PV"};

  Gaudi::Property<double> m_minProbNNmu{this, "MaxProbNNmu", 0.798, "Tagging particle requirement: Maximum ProbNNmu."};
  Gaudi::Property<double> m_maxProbNNpi{this, "MaxProbNNpi", 0.956, "Tagging particle requirement: Maximum ProbNNpi."};
  Gaudi::Property<double> m_maxProbNNp{this, "MaxProbNNp", 0.954, "Tagging particle requirement: Maximum ProbNNp."};
  Gaudi::Property<double> m_maxProbNNe{this, "MaxProbNNe", 0.521, "Tagging particle requirement: Maximum ProbNNe."};
  Gaudi::Property<double> m_maxProbNNk{this, "MinProbNNk", 0.952, "Tagging particle requirement: Minimum ProbNNk."};

  // values for calibration purposes
  // Gaudi::Property<double> m_averageEta{this, "AverageEta", 0.3949};
  // Gaudi::Property<double> m_minPosDecision{this, "MinPositiveTaggingDecision", 0.5,
  //                                          "Minimum value of the Eta for a tagging decision of +1"};
  // Gaudi::Property<double> m_maxNegDecision{this, "MaxNegativeTaggingDecision", 0.5,
  //                                          "Maximum value of the Eta for a tagging decision of -1"};

  mutable Gaudi::Accumulators::SummingCounter<> m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<> m_kaonCount{this, "#taggingKaons"};
  mutable Gaudi::Accumulators::SummingCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<TaggingHelperTool>    m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  ToolHandle<IParticleDescendants> m_particleDescendantTool{this, "ParticleDescendantTool", "ParticleDescendants"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<OSMuon_Data_Run2_All_Bu2JpsiK_XGB_BDT_v2r0>();
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Run2OSMuonTagger )

LHCb::FlavourTags Run2OSMuonTagger::operator()( const LHCb::Particle::Range& bCandidates,
                                                const LHCb::Particle::Range& taggingMuons,
                                                const LHCb::PrimaryVertices& primaryVertices,
                                                const DetectorElement&       lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_kaonCount += taggingMuons.size();

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingMuons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      auto flavourTag = std::make_unique<LHCb::FlavourTag>();
      flavourTag->addTagger( LHCb::Tagger{}.setType( taggerType() ) ).setTaggedB( bCand );
      flavourTags.insert( flavourTag.release() );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {

    auto tagger = performTagging( *bCand, taggingMuons, primaryVertices, geometry );
    m_FTCount += tagger.has_value();

    auto flavourTag = std::make_unique<LHCb::FlavourTag>();
    flavourTag->setTaggedB( bCand );
    // if no appropriate tagging candidate found, fill with "empty" FlavourTag object
    flavourTag->addTagger( tagger ? *tagger : LHCb::Tagger{}.setType( taggerType() ) );
    flavourTags.insert( flavourTag.release() );
  }

  return flavourTags;
}

std::optional<LHCb::Tagger> Run2OSMuonTagger::performTagging( const LHCb::Particle&        bCand,
                                                              const LHCb::Particle::Range& taggingMuons,
                                                              const LHCb::PrimaryVertices& primaryVertices,
                                                              const IGeometryInfo&         geometry ) const {
  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( !bestPV ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand);
  if(! refittedPV)
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, bestPV );

  auto b_daughters = m_particleDescendantTool->descendants( &bCand );
  b_daughters.push_back( &bCand );

  const LHCb::Particle*              bestTagCand = nullptr;
  double                             bestBDT     = -99.0;
  std::vector<const LHCb::Particle*> preselectionMuons;

  // Preselection to choose kaons which passed selections in BTaggingTool.cpp
  for ( const auto* tagCand : taggingMuons ) {
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;
    preselectionMuons.push_back( tagCand );
  }

  for ( const auto* tagCand : preselectionMuons ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    const LHCb::ProtoParticle* tagProto = tagCand->proto();
    const LHCb::Track*         tagTrack = tagProto->track();

    const bool isMuon      = tagProto->muonPID() ? tagProto->muonPID()->IsMuon() : false;
    auto const gpid        = tagProto->globalChargedPID();
    auto const tagProbNNk  = gpid ? gpid->ProbNNk() : -1000.0;
    auto const tagProbNNpi = gpid ? gpid->ProbNNpi() : -1000.0;
    auto const tagProbNNp  = gpid ? gpid->ProbNNp() : -1000.0;
    auto const tagProbNNmu = gpid ? gpid->ProbNNmu() : -1000.0;
    auto const tagProbNNe  = gpid ? gpid->ProbNNe() : -1000.0;

    if ( !isMuon ) continue;
    if ( tagProbNNk > m_maxProbNNk ) continue;
    if ( tagProbNNpi > m_maxProbNNpi ) continue;
    if ( tagProbNNp > m_maxProbNNp ) continue;
    if ( tagProbNNmu < m_minProbNNmu ) continue;
    if ( tagProbNNe > m_maxProbNNe ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithChi2( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP ) continue;
    const double ipSig = std::sqrt( refittedPVIP->second );
    const double absIp = std::abs( refittedPVIP->first );
    if ( ipSig < m_minIpSigTagBestPV ) continue;
    // if ( absIp > m_maxAbsIpTagBestPV ) continue;

    // Minimum IP significance wrt to pile up PVs
    auto minIp = m_taggingHelperTool->calcMinIPWithChi2( *tagCand, pileups, geometry );
    if ( !minIp ) continue;
    const double minIpSig = std::sqrt( minIp->second );
    if ( minIpSig < m_minIpSigTagPileUpVertices ) continue;

    const double tagGhostProb = tagTrack->ghostProbability();

    const double mvaValue =
        m_classifier->getClassifierValue( {tag_mom.P(), tag_mom.Pt(), minIpSig, tagGhostProb, tagProbNNmu, absIp,
                                           static_cast<double>( preselectionMuons.size() ), // trackCounts
                                           b_mom.Pt(), ipSig} );

    if ( mvaValue < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = mvaValue;
  }

  if ( bestBDT < 0 || bestBDT > 1 ) return std::nullopt;

  auto   tagdecision = bestTagCand->charge() > 0 ? LHCb::Tagger::TagResult::b : LHCb::Tagger::TagResult::bbar;
  double omega       = 1 - bestBDT;
  if ( bestBDT < 0.5 ) {
    tagdecision = reversed( tagdecision );
    omega       = bestBDT;
  }

  return LHCb::Tagger{}
      .setDecision( tagdecision )
      .setOmega( omega )
      .setType( taggerType() )
      .addToTaggerParts( bestTagCand )
      .setCharge( bestTagCand->charge() )
      .setMvaValue( bestBDT );
}
