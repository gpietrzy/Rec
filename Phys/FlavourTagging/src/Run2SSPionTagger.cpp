/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "../Classification/SSPion/SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"
#include "DetDesc/DetectorElement.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/Tagger.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class Run2SSPionTagger
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                                             const LHCb::PrimaryVertices&, const DetectorElement& ),
                                          LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  Run2SSPionTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingPions", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particle::Range& bCandidates, const LHCb::Particle::Range& taggingPions,
                                const LHCb::PrimaryVertices& primaryVertices, const DetectorElement& ) const override;

private:
  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingPions,
                                              const LHCb::PrimaryVertices& primaryVertices,
                                              const IGeometryInfo&         geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::Run2_SSPion; }

  Gaudi::Property<double> m_maxDistPhi{this, "MaxDistPhi", 1.1,
                                       "Tagging particle requirement: Maximum phi distance to B candidate"};
  Gaudi::Property<double> m_maxDeltaEta{this, "MaxDeltaEta", 1.2,
                                        "Tagging particle requirement: Maximum eta distance to B candidate"};
  Gaudi::Property<double> m_maxIpSigTagBestPV{this, "MaxIpSigTagBestPV", 4.0,
                                              "Tagging particle requirement: Maximum IP significance wrt to best PV"};
  Gaudi::Property<double> m_maxDQB0Pion{this, "MaxDQB0Pion", 1.2 * Gaudi::Units::GeV, "I don't know what this is"};
  Gaudi::Property<double> m_minPTB0Pion{this, "MaxPTB0Pion", 3.0 * Gaudi::Units::GeV,
                                        "Maximum transverse momentum of the B0-Pion combination"};
  Gaudi::Property<double> m_minChi2B0PionVertex{this, "MinChi2B0PionVertex", 100};
  Gaudi::Property<double> m_minPionProb{this, "MinPionProb", 0.5};

  mutable Gaudi::Accumulators::SummingCounter<>  m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_pionCount{this, "#taggingPions"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_FTCount{this, "#goodFlavourTags"};

  ToolHandle<TaggingHelperTool> m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<SSPion_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1>();
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Run2SSPionTagger )

LHCb::FlavourTags Run2SSPionTagger::operator()( const LHCb::Particle::Range& bCandidates,
                                                const LHCb::Particle::Range& taggingPions,
                                                const LHCb::PrimaryVertices& primaryVertices,
                                                const DetectorElement&       lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();

  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_pionCount += taggingPions.size();

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingPions.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      auto flavourTag = std::make_unique<LHCb::FlavourTag>();
      flavourTag->addTagger( LHCb::Tagger{}.setType( taggerType() ) ).setTaggedB( bCand );
      flavourTags.insert( flavourTag.release() );
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {
    auto tagResult = performTagging( *bCand, taggingPions, primaryVertices, geometry );
    m_FTCount += tagResult.has_value();

    auto flavourTag = std::make_unique<LHCb::FlavourTag>();
    flavourTag->setTaggedB( bCand );
    // if no appropriate tagging candidate found, fill with "empty" FlavourTag object
    flavourTag->addTagger( tagResult ? *tagResult : LHCb::Tagger{}.setType( taggerType() ) );
    flavourTags.insert( flavourTag.release() );
  }
  return flavourTags;
}

std::optional<LHCb::Tagger> Run2SSPionTagger::performTagging( const LHCb::Particle&        bCand,
                                                              const LHCb::Particle::Range& taggingPions,
                                                              const LHCb::PrimaryVertices& primaryVertices,
                                                              const IGeometryInfo&         geometry ) const {
  const LHCb::Particle* bestTagCand = nullptr;

  double bestBDT = -99.0;

  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best matches the B candidate and unbias it. (note: this returns on optional)
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;

  // The unbiasing only works if primaryVertices is of type LHCb::Event::PV::PrimaryVertexContainer. However, looking at
  // how the unbiased vertex is used, I can hardly imagine that it is useful to unbias it. This will need to be studied.
  // const auto refittedPV = LHCb::unbiasedBestPV( primaryVertices, bCand );
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  // get the pile-up vertices (those not associated to the B)
  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, bCand );

  // loop over tagging particles
  for ( const auto* tagCand : taggingPions ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    // if(std::abs(tag_mom.P()-1571.)>1) continue;
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;

    // exclude tracks too far way from the signal
    double deltaPhi = std::fabs( TaggingHelper::dPhi( b_mom, tag_mom ) );
    if ( deltaPhi > m_maxDistPhi ) continue;

    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );
    if ( deltaEta > m_maxDeltaEta ) continue;

    // calculate and cut on dQ
    const double dQ = ( b_mom + tag_mom ).M() - b_mom.M(); // NB: missing mp term as in Davide's computation (???)
    if ( dQ > m_maxDQB0Pion ) continue;

    // cut on the transverse momentum of the B - tagging particle combination
    const double btag_pT = ( b_mom + tag_mom ).Pt();
    if ( btag_pT < m_minPTB0Pion ) continue;

    // IP significance cut wrt bestPV.
    // FIXME: this is the only place where an unbiased PV is used. it seems a bit exagerated to use that just to compute
    // IP and IPCHI2. it will save a lot of CPU time if we just work with the biased PV.
    std::optional<std::pair<double, double>> refittedPVIP;
    if ( refittedPV ) refittedPVIP = m_taggingHelperTool->calcIPWithChi2( *tagCand, *refittedPV, geometry );

    if ( !refittedPVIP ) continue;
    if ( std::sqrt( refittedPVIP->second ) > m_maxIpSigTagBestPV ) continue;

    // fit BCand and pionCand to vertex and cut on chi2/ndof
    std::optional<LHCb::Vertex> tagBVtx = m_taggingHelperTool->fitVertex( *tagCand, bCand, geometry );
    if ( !tagBVtx ) continue;
    if ( tagBVtx->chi2() / tagBVtx->nDoF() > m_minChi2B0PionVertex ) continue;

    // inputs for the BDT
    const double               dR           = std::sqrt( deltaEta * deltaEta + deltaPhi * deltaPhi );
    const LHCb::ProtoParticle* tagProto     = tagCand->proto();
    auto const                 gpid         = tagProto->globalChargedPID();
    const double               tagPIDk      = gpid ? gpid->CombDLLk() : -1000.0;
    const LHCb::Track*         tagTrack     = tagProto->track();
    const double               tagTrackChi2 = tagTrack->chi2PerDoF();
    const double               tagGhostProb = tagTrack->ghostProbability();

    // MVA !!
    const double BDT = m_classifier->getClassifierValue(
        {log( tag_mom.P() / Gaudi::Units::GeV ), log( tag_mom.Pt() / Gaudi::Units::GeV ),
         log( std::sqrt( refittedPVIP->second ) ), tagGhostProb, log( deltaPhi ), dR, log( deltaEta ),
         dQ / Gaudi::Units::GeV, log( b_mom.Pt() / Gaudi::Units::GeV ),
         log( ( b_mom + tag_mom ).Pt() / Gaudi::Units::GeV ), tagPIDk, tagTrackChi2,
         static_cast<double>( refittedPV->nDoF() )} );

    if ( BDT < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = BDT;
  }

  constexpr auto pol = std::array{0.4523, -0.117, -0.122, -0.081};
  double         pn  = 1. - ( pol[0] + bestBDT * ( pol[1] + bestBDT * ( pol[2] + pol[3] * bestBDT ) ) );

  if ( pn < m_minPionProb || pn > 1 ) return std::nullopt;

  auto tagdecision = bestTagCand->charge() > 0 ? LHCb::Tagger::TagResult::bbar : LHCb::Tagger::TagResult::b;
  if ( bCand.particleID().hasUp() ) tagdecision = reversed( tagdecision );

  return LHCb::Tagger{}
      .setDecision( tagdecision )
      .setOmega( 1 - pn )
      .setType( taggerType() )
      .addToTaggerParts( bestTagCand )
      .setCharge( bestTagCand->charge() )
      .setMvaValue( bestBDT );
}
