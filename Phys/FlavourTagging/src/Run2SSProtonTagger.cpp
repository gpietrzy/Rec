/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "../Classification/SSProton/SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1.h"
#include "DetDesc/DetectorElement.h"
#include "Event/FlavourTag.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/Tagger.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"
#include "Utils/TaggingHelper.h"
#include "Utils/TaggingHelperTool.h"

class Run2SSProtonTagger
    : public LHCb::Algorithm::Transformer<LHCb::FlavourTags( const LHCb::Particle::Range&, const LHCb::Particle::Range&,
                                                             const LHCb::PrimaryVertices&, const DetectorElement& ),
                                          LHCb::Algorithm::Traits::usesConditions<DetectorElement>> {
public:
  /// Standard constructor
  Run2SSProtonTagger( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"BCandidates", ""}, KeyValue{"TaggingProtons", ""}, KeyValue{"PrimaryVertices", ""},
                      KeyValue{"StandardGeometry", LHCb::standard_geometry_top}},
                     KeyValue{"OutputFlavourTags", ""} ) {}

  LHCb::FlavourTags operator()( const LHCb::Particle::Range& bCandidates, const LHCb::Particle::Range& taggingProtons,
                                const LHCb::PrimaryVertices& primaryVertices, const DetectorElement& ) const override;

private:
  // std::optional<std::pair<const LHCb::Particle*, double>>
  // searchForTaggingParticle( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingProtons,
  std::optional<LHCb::Tagger> performTagging( const LHCb::Particle& bCand, const LHCb::Particle::Range& taggingProtons,
                                              const LHCb::PrimaryVertices& primaryVertices,
                                              const IGeometryInfo&         geometry ) const;

  LHCb::Tagger::TaggerType taggerType() const { return LHCb::Tagger::TaggerType::Run2_SSProton; }

  Gaudi::Property<double> m_maxDistPhi{this, "MaxDistPhi", 1.2,
                                       "Tagging particle requirement: Maximum phi distance to B candidate"};
  Gaudi::Property<double> m_maxDeltaEta{this, "MaxDeltaEta", 1.2,
                                        "Tagging particle requirement: Maximum eta distance to B candidate"};
  Gaudi::Property<double> m_maxIpSigTagBestPV{this, "MaxIpSigTagBestPV", 4.0,
                                              "Tagging particle requirement: Maximum IP significance wrt to best PV"};
  Gaudi::Property<double> m_maxDQB0Proton{this, "MaxDQB0Proton", 1.3 * Gaudi::Units::GeV, "I don't know what this is"};
  Gaudi::Property<double> m_minPTB0Proton{this, "MinPTB0Proton", 3.0 * Gaudi::Units::GeV,
                                          "Minimum transverse momentum of the B0-Proton combination"};
  Gaudi::Property<double> m_maxChi2B0ProtonVertex{this, "MaxChi2B0ProtonVertex", 100};
  Gaudi::Property<double> m_minProtonProb{this, "MinProtonProb", 0.5};

  mutable Gaudi::Accumulators::SummingCounter<>  m_BCount{this, "#BCandidates"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_BCount2{this, "#BCandidates2"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_0Count{this, "#Zero Bs"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_invCount{this, "#no tagresult"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_protonCount{this, "#taggingProtons"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_FTCount{this, "#goodFlavourTags"};
  mutable Gaudi::Accumulators::SummingCounter<>  m_allFTCount{this, "#allFlavourTags"};

  ToolHandle<TaggingHelperTool> m_taggingHelperTool{this, "TaggingHelper", "TaggingHelperTool"};
  // ToolHandle<IVertexFit>              m_vertexFitTool{this, "VertexFitter", "LoKi::VertexFitter"};

  std::unique_ptr<ITaggingClassifier> m_classifier = std::make_unique<SSProton_Data_Run1_All_Bd2Dpi_TMVA_BDT_v1r1>();
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( Run2SSProtonTagger )

LHCb::FlavourTags Run2SSProtonTagger::operator()( const LHCb::Particle::Range& bCandidates,
                                                  const LHCb::Particle::Range& taggingProtons,
                                                  const LHCb::PrimaryVertices& primaryVertices,
                                                  const DetectorElement&       lhcbDetector ) const {
  auto& geometry = *lhcbDetector.geometry();
  // keyed container of FlavourTag objects, one FlavourTag per B candidate
  LHCb::FlavourTags flavourTags;

  m_BCount += bCandidates.size();
  m_protonCount += taggingProtons.size();
  // always() << bCandidates.size() << "  " << taggingProtons.size() << "  " << primaryVertices.size() << endmsg;

  // is this still needed??? could be removed
  if ( bCandidates.size() == 0 || taggingProtons.size() == 0 || primaryVertices.size() == 0 ) {
    for ( const auto* bCand : bCandidates ) {
      m_BCount2 += 1;
      auto flavourTag = std::make_unique<LHCb::FlavourTag>();
      flavourTag->addTagger( LHCb::Tagger{}.setType( taggerType() ) ).setTaggedB( bCand );
      flavourTags.insert( flavourTag.release() );
      ++m_allFTCount;
      ++m_0Count;
    }
    return flavourTags;
  }

  // loop over B candidates
  for ( const auto* bCand : bCandidates ) {
    m_BCount2 += 1;

    auto tagResult = performTagging( *bCand, taggingProtons, primaryVertices, geometry );
    m_FTCount += tagResult.has_value();
    auto flavourTag = std::make_unique<LHCb::FlavourTag>();
    flavourTag->setTaggedB( bCand );
    m_allFTCount += 1;
    if ( !tagResult ) m_invCount += 1;
    // if no appropriate tagging candidate found, fill with "empty" FlavourTag object
    flavourTag->addTagger( tagResult ? *tagResult : LHCb::Tagger{}.setType( taggerType() ) );
    flavourTags.insert( flavourTag.release() );
  }
  return flavourTags;
}

std::optional<LHCb::Tagger> Run2SSProtonTagger::performTagging( const LHCb::Particle&        bCand,
                                                                const LHCb::Particle::Range& taggingProtons,
                                                                const LHCb::PrimaryVertices& primaryVertices,
                                                                const IGeometryInfo&         geometry ) const {
  const LHCb::Particle* bestTagCand = nullptr;

  double bestBDT = -99.0;

  Gaudi::LorentzVector b_mom = bCand.momentum();

  // find PV that best fits the B candidate
  const LHCb::VertexBase* bestPV = LHCb::bestPV( primaryVertices, bCand );
  if ( bestPV == nullptr ) return std::nullopt;

  /*std::optional<const LHCb::VertexBase> refittedPV = m_taggingHelperTool->refitPVWithoutB(*bestPV, bCand, geometry);
  if(! refittedPV)
    return std::nullopt;
  */
  // PV refitting isn't possible in Moore (right now) since the PV doesn't save the particles it was made of
  std::optional<const LHCb::VertexBase> refittedPV = *bestPV;

  auto pileups = TaggingHelper::pileUpVertices( primaryVertices, bestPV );

  // loop over tagging particles
  for ( const auto* tagCand : taggingProtons ) {
    Gaudi::LorentzVector tag_mom = tagCand->momentum();

    // preselection common to all taggers
    if ( !m_taggingHelperTool->passesCommonPreSelection( *tagCand, bCand, pileups, geometry ) ) continue;

    // selection specific to the ssproton tagger
    // exclude tracks too far way from the signal
    double deltaPhi = TaggingHelper::dPhi( b_mom, tag_mom );
    deltaPhi        = std::fabs( deltaPhi );
    if ( deltaPhi > m_maxDistPhi ) continue;

    const double deltaEta = std::fabs( b_mom.Eta() - tag_mom.Eta() );
    if ( deltaEta > m_maxDeltaEta ) continue;

    // calculate and cut on dQ
    const double dQ = ( b_mom + tag_mom ).M() - b_mom.M() - tag_mom.M();
    if ( dQ > m_maxDQB0Proton ) continue;

    // cut on the transverse momentum of the B - tagging particle combination
    const double btag_pT = ( b_mom + tag_mom ).Pt();
    if ( btag_pT < m_minPTB0Proton ) continue;

    // IP significance cut wrt bestPV
    std::optional<std::pair<double, double>> refittedPVIP =
        m_taggingHelperTool->calcIPWithErr( *tagCand, refittedPV.value(), geometry );
    if ( !refittedPVIP ) continue;
    if ( refittedPVIP->second > m_maxIpSigTagBestPV ) continue;

    // fit BCand and tagging particle to vertex and cut on chi2/ndof
    std::optional<LHCb::Vertex> tagBVtx = m_taggingHelperTool->fitVertex( *tagCand, bCand, geometry );
    if ( !tagBVtx ) continue;
    if ( tagBVtx->chi2() / tagBVtx->nDoF() > m_maxChi2B0ProtonVertex ) continue;

    // inputs for the BDT
    const double               dR       = std::sqrt( deltaEta * deltaEta + deltaPhi * deltaPhi );
    const LHCb::ProtoParticle* tagProto = tagCand->proto();
    auto const                 gpid     = tagProto->globalChargedPID();
    const double               tagPIDp  = gpid ? gpid->CombDLLp() : -1000.0;

    // MVA
    const double BDT = m_classifier->getClassifierValue(
        {log( tag_mom.P() / Gaudi::Units::GeV ), log( tag_mom.Pt() / Gaudi::Units::GeV ), log( refittedPVIP->second ),
         dR, log( deltaEta ), dQ / Gaudi::Units::GeV, log( ( b_mom + tag_mom ).Pt() / Gaudi::Units::GeV ),
         log( refittedPV->nDoF() ), log( tagPIDp )} );

    if ( BDT < bestBDT ) continue;

    bestTagCand = tagCand;
    bestBDT     = BDT;
  }

  // if ( bestTagCand ) return std::make_pair( bestTagCand, bestBDT);
  // return std::nullopt;

  constexpr auto pol = std::array{0.4831, -0.07304, -0.05043, -0.1749};
  double         pn  = 1. - ( pol[0] + bestBDT * ( pol[1] + bestBDT * ( pol[2] + pol[3] * bestBDT ) ) );

  if ( pn < m_minProtonProb || pn > 1 ) return std::nullopt;

  auto tagdecision = bestTagCand->charge() > 0 ? LHCb::Tagger::TagResult::b : LHCb::Tagger::TagResult::bbar;
  if ( bCand.particleID().hasUp() ) tagdecision = reversed( tagdecision );

  return LHCb::Tagger{}
      .setDecision( tagdecision )
      .setOmega( 1 - pn )
      .setType( taggerType() )
      .addToTaggerParts( bestTagCand )
      .setCharge( bestTagCand->charge() )
      .setMvaValue( bestBDT );
}
