/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
#include "Event/Particle.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/Relation1D.h"

namespace {
  using Table    = LHCb::Relation1D<LHCb::Particle, LHCb::Particle>;
  using in_parts = LHCb::Particle::Range;
} // namespace
// ============================================================================
/** @class ParticleTaggerAlg
 *  Algorithm that looks for tagging candidates in the event and stores the output
 *  in conjuction with MAP_INPUT_ARRAY functor
 *
 *  @param Input Location of particles
 *  @param TaggingContainer Location of tagging particles
 *  @returns OutputRelations location of relation table between in and out particles
 *
 *  Use:
 *   @code
 *     import Functors as F
 *     from PyConf.Algorithms import ParticleTaggerAlg
 *     from DaVinci.standard_particles import make_long_pions
 *
 *     pions = make_long_pions()
 *     bd2dspi = make_data_with_FetchDataFromFile("/Event/Spruce/SpruceB2OC_BdToDsmPi_DsmToHHH_Line/Particles")
 *
 *     tagAlg = ParticleTaggerAlg(Input=bs2dspi, TaggingContainer = pions)
 *     tagAlgRels = tagAlg.OutputRelations  # Relations
 *     functor = F.MAP_INPUT_ARRAY(Functor=MAP_RANGE(Functor=F.PT), Relations=tagAlgRels)
 *   @endcode
 *
 *  @see FunTuple
 *  @see Functors::Adapters::MapRelInputToFunArray
 *  @see Functors::Adapters::MapRange
 */
class ParticleTaggerAlg : public LHCb::Algorithm::Transformer<Table( in_parts const&, in_parts const& )> {
public:
  // ==========================================================================
  /** the standard constructor
   *  @param name algorithm instance name
   *  @param pSvc service locator
   */
  ParticleTaggerAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, {KeyValue{"Input", ""}, KeyValue{"TaggingContainer", ""}},
                     KeyValue{"OutputRelations", ""} ) {}
  // ==========================================================================
  /// the standard execution of the algorithm
  // ==========================================================================
  Table operator()( const in_parts& parts, const in_parts& parts_tagging ) const override {
    // output
    Table p2p_out;

    if ( parts_tagging.empty() ) {
      ++m_noParticleFound;
    } else {
      for ( const auto& inP : parts ) {
        for ( const auto& part : parts_tagging ) { p2p_out.relate( inP, part ).ignore(); }
      }
    }

    if ( !parts.empty() ) {
      ++m_eventCount;
      m_candidateCount += parts.size();
      m_trackCount += parts_tagging.size();
    }

    return p2p_out;
  }

private:
  mutable Gaudi::Accumulators::Counter<>                 m_eventCount{this, "Events"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_candidateCount{this, "Input Particles"};
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_trackCount{this, "Output Particles"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING>  m_noParticleFound{
      this, "No particles found at the current tagging location"};
};

/// declare the factory (needed for instantiation)
DECLARE_COMPONENT( ParticleTaggerAlg )
