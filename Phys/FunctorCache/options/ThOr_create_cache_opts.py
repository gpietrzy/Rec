###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Configurables import ApplicationMgr

ApplicationMgr().Environment['THOR_DISABLE_JIT'] = '1'
ApplicationMgr().Environment['THOR_DISABLE_CACHE'] = '1'
ApplicationMgr().Environment['THOR_JIT_EXTRA_ARGS'] = ''
ApplicationMgr().Environment['THOR_JIT_LIBDIR'] = '.'
