/*****************************************************************************\
 * (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/FlavourTag.h"
#include "Event/State.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/MatVec.h"
#include "SelKernel/Utilities.h"
#include "SelKernel/VertexRelation.h"

#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Functors/TrackLike.h"

#include <string>
#include <type_traits>

/** @file  Composite.h
 *  @brief Definitions of functors for composite-particle-like objects.
 */

/** @namespace Functors::Composite
 *
 *  Functors that make sense for composite particles (i.e. ones that have a
 *  vertex as well as a trajectory)
 */
namespace Functors::detail {
  template <typename FlavourTags, typename Data>
  std::optional<LHCb::Tagger> findMatchingFlavourTagger( FlavourTags const& ftags, Data const& d,
                                                         LHCb::Tagger::TaggerType const& inputtype ) {
    for ( auto const& ft : ftags ) { // loop over flavourtags in event (one per B candidate)
      const LHCb::Particle* bCand = ft->taggedB();
      if ( bCand->momentum() == d.momentum() &&
           bCand->referencePoint() == d.referencePoint() ) { // B matching check they're the same object
        for ( auto const& t : ft->taggers() ) {
          if ( t.type() == inputtype ) { return t; }
        }
      }
    }
    return {};
  }
} // namespace Functors::detail

namespace Functors::Composite {

  /**MTDOCACHI2**/
  template <int N>
  struct MotherTrajectoryDistanceOfClosestApproachChi2 : public Function {
    static_assert( N >= 1, "Indices start from 1 for LoKi compatibility." );
    MotherTrajectoryDistanceOfClosestApproachChi2( std::integral_constant<int, N> = {} ) {}

    void bind( TopLevelInfo& top_level ) { m_dist_calc.emplace( top_level.algorithm() ); }

    template <typename VContainer, typename Particle>
    auto operator()( VContainer const& vertices, Particle const& composite ) const {
      auto const bestPV = Sel::getBestPV( composite, vertices );

      using LHCb::Event::decayProducts;
      auto const& children = decayProducts( composite );
      auto const& pN       = children[N - 1];

      auto const& pN_state = Sel::stateVectorFromComposite( pN );

      using LHCb::Event::posCovMatrix;
      using LHCb::Event::referencePoint;
      using LHCb::Event::threeMomCovMatrix;
      using LHCb::Event::threeMomentum;
      using LHCb::Event::threeMomPosCovMatrix;

      auto const& composite_state = Sel::stateVectorComputations(
          endVertexPos( *bestPV ), threeMomentum( composite ), threeMomCovMatrix( composite ), posCovMatrix( *bestPV ),
          threeMomPosCovMatrix( composite ) );

      const auto& dist_calc = *m_dist_calc;
      return dist_calc.stateDOCAChi2( composite_state, pN_state );
    }

  private:
    std::optional<Functors::detail::DefaultDistanceCalculator_t> m_dist_calc;
  };

  /** @brief Flight distance chi2 to the given vertex.
   *
   *  Note that if the given data object contains a vertex link then that will
   *  be checked for compatibility with the given vertex container and, if it
   *  matches, be used.
   */
  struct FlightDistanceChi2ToVertex : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "FlightDistanceChi2ToVertex"; }

    template <typename Vertex_t, typename Particle>
    auto operator()( Vertex_t const& vertex, Particle const& composite ) const {
      auto const& comp = Sel::Utils::deref_if_ptr( composite );
      return Sel::Utils::flightDistanceChi2( comp, vertex );
    }
  };

  // First a helper for the momentum perpendicular to the flight vector
  template <typename Vertex_t, typename Composite>
  auto perpendicularMomentum( Vertex_t const& vtx, Composite const& comp ) {
    using LHCb::Event::endVertexPos;
    using LHCb::Event::threeMomentum;
    auto const d    = endVertexPos( comp ) - endVertexPos( vtx );
    auto const mom  = threeMomentum( comp );
    auto const perp = mom - d * ( dot( mom, d ) / d.mag2() );
    auto const pt   = perp.mag();
    return pt;
  }

  /** @brief Calculate the corrected mass for the given PV. */
  struct CorrectedMass : public Function {
    static constexpr auto name() { return "CorrectedMass"; }

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vtx, Composite const& composite ) const {

      using LHCb::Event::mass2;
      using std::sqrt;

      // Get the pT variable that we need
      auto const pt = perpendicularMomentum( vtx, composite );

      // Calculate the corrected mass
      return pt + sqrt( mass2( composite ) + pt * pt );
    }
  };

  /** @brief Calculate the corrected mass error for a given PV. */
  struct CorrectedMassError : public Function {
    static constexpr auto name() { return "CorrectedMassError"; }

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vertices, Composite const& composite ) const {

      using LHCb::Event::endVertexPos;
      using LHCb::Event::fourMomentum;
      using LHCb::Event::mass2;
      using LHCb::Event::momCovMatrix;
      using LHCb::Event::momPosCovMatrix;
      using LHCb::Event::posCovMatrix;
      using LHCb::Event::threeMomentum;
      using std::sqrt;

      auto const mom     = threeMomentum( composite );
      auto const fourmom = fourMomentum( composite );
      auto const sv      = endVertexPos( composite );
      auto const pv      = endVertexPos( vertices );
      auto const flight  = sv - pv;
      auto const pt      = perpendicularMomentum( vertices, composite );
      auto const A       = dot( mom, flight );
      auto const B       = flight.mag2();

      auto const posCov    = posCovMatrix( composite );
      auto const covFourP  = momCovMatrix( composite );
      auto const momPosCov = momPosCovMatrix( composite );

      auto const invSqrtMassPT = 1. / sqrt( mass2( composite ) + pt * pt );
      auto const dMcorrdPT     = 0.5 * invSqrtMassPT * 2 * pt + 1;

      // First calculate some derivatives
      auto const dMcd_SV = dMcorrdPT * ( 1 / pt ) * -0.5 * ( 2 * A * B * mom - A * A * 2 * flight ) / ( B * B );
      auto const dMcd_PV = dMcorrdPT * ( 1 / pt ) * -0.5 * ( -2 * A * B * mom + A * A * 2 * flight ) / ( B * B );
      auto const dMcdp   = -1 * invSqrtMassPT * A / B * flight + 1 / pt * ( mom - A / B * flight );
      auto const dMcdE   = invSqrtMassPT * E( fourmom );

      LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4> dMcdP4;
      dMcdP4( 0 ) = LHCb::Utils::as_arithmetic( dMcdp.X() );
      dMcdP4( 1 ) = LHCb::Utils::as_arithmetic( dMcdp.Y() );
      dMcdP4( 2 ) = LHCb::Utils::as_arithmetic( dMcdp.Z() );
      dMcdP4( 3 ) = LHCb::Utils::as_arithmetic( dMcdE );
      // -- the errors on the vertices
      auto const errsqVertex = similarity( dMcd_SV + dMcd_PV, posCov );

      auto const part1 = momPosCov.transpose() * dMcdP4;
      auto const part2 = dot( dMcd_SV, part1 );

      auto const errsqMom = similarity( dMcdP4, covFourP ) + 2 * part2;

      return sqrt( errsqVertex + errsqMom );
    }
  };

  /** @brief Calculate the composite mass using the given child mass hypotheses. */
  template <typename... MassInputs>
  struct MassWithHypotheses : public Function {
    /** Create a mass functor with a list of mass hypotheses that can be a mix
     *  of floating point values (in MeV) and names of particles. Particle
     *  names are translated into mass values using the ParticlePropertySvc.
     */
    MassWithHypotheses( std::tuple<MassInputs...> mass_inputs ) : m_mass_inputs{std::move( mass_inputs )} {}

    void bind( TopLevelInfo& top_level ) { bind( top_level, std::index_sequence_for<MassInputs...>{} ); }

    template <typename CombinationType>
    auto operator()( CombinationType const& comb ) const {
      // Calculate the mass from the child 3-momenta and the given
      // mass hypotheses. Start by checking we have the correct number.
      using LHCb::Event::decayProducts;
      auto children    = decayProducts( comb );
      auto NumChildren = children.size();
      if ( sizeof...( MassInputs ) != NumChildren || m_mass_values.size() != NumChildren ) {
        throw GaudiException{
            "Mismatch between number of mass values given (" + std::to_string( sizeof...( MassInputs ) ) +
                ") and the number of children in the given object (" + std::to_string( NumChildren ) + ")",
            "Functors::Composite::Mass", StatusCode::FAILURE};
      }
      using LHCb::Event::threeMomentum;
      using std::sqrt;
      using float_t = decltype( threeMomentum( children[0] ).mag2() );
      float_t E{0};
      for ( const auto& [i, child] : LHCb::range::enumerate( children ) ) {
        E += sqrt( threeMomentum( child ).mag2() + m_mass_values[i] * m_mass_values[i] );
      }
      return sqrt( E * E - threeMomentum( comb ).mag2() );
    }

  private:
    template <std::size_t... Ns>
    void bind( TopLevelInfo& top_level, std::index_sequence<Ns...> ) {
      // Avoid setting up the service if we don't need it (e.g. all hypotheses)
      // were specified numerically)
      std::optional<ServiceHandle<LHCb::IParticlePropertySvc>> pp_svc{std::nullopt};
      // Helper to convert a member of m_mass_inputs to a numeric value.
      auto const converter = [&]( auto const& mass_or_name ) {
        if constexpr ( std::is_same_v<float, std::decay_t<decltype( mass_or_name )>> ) {
          return mass_or_name;
        } else {
          if ( !pp_svc ) {
            pp_svc.emplace( top_level.algorithm(), top_level.generate_property_name(), "LHCb::ParticlePropertySvc" );
          }
          auto const* pp = pp_svc.value()->find( mass_or_name );
          if ( !pp ) {
            throw GaudiException{"Couldn't get ParticleProperty for particle '" + mass_or_name + "'",
                                 "Functors::Composite::Mass::bind()", StatusCode::FAILURE};
          }
          return pp->mass();
        }
      };
      ( ( m_mass_values[Ns] = converter( std::get<Ns>( m_mass_inputs ) ) ), ... );
    }

    std::tuple<MassInputs...>                  m_mass_inputs;
    std::array<float, sizeof...( MassInputs )> m_mass_values{};
  };

  /** @brief Return the input object's mass as defined by an accessor. */
  struct Mass : public Function {
    static constexpr auto name() { return "Mass"; }

    template <typename Particle>
    auto operator()( Particle const& particle ) const {
      using LHCb::Event::mass2;
      using std::sqrt;
      return sqrt( mass2( particle ) );
    }
  };

  /** @brief Calculate the lifetime of the particle with respect to the vertex. */
  struct Lifetime : public Function {
    static constexpr auto name() { return "Lifetime"; }

    void bind( TopLevelInfo& top_level ) { m_lifetime_calc.bind( top_level.algorithm() ); }

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vertex, Composite const& composite ) const {
      return std::get<0>(
          m_lifetime_calc.Lifetime( Sel::Utils::deref_if_ptr( vertex ), Sel::Utils::deref_if_ptr( composite ) ) );
    }

  private:
    Functors::detail::DefaultLifetimeFitter_t m_lifetime_calc;
  };

  /** @brief Calculate the lifetime of the particle with respect to the vertex. */
  struct ComputeDecayLengthSignificance : public Function {
    /** This allows some error messages to be customised. It is not critical. */
    static constexpr auto name() { return "ComputeDecayLengthSignificance"; }

    void bind( TopLevelInfo& top_level ) { m_lifetime_calc.bind( top_level.algorithm() ); }

    template <typename Vertex_t, typename Composite>
    auto operator()( Vertex_t const& vertex, Composite const& composite ) const {
      auto const& comp = Sel::Utils::deref_if_ptr( composite );
      auto const& vtx  = Sel::Utils::deref_if_ptr( vertex );
      return m_lifetime_calc.DecayLengthSignificance( vtx, comp );
    }

  private:
    Functors::detail::DefaultLifetimeFitter_t m_lifetime_calc;
  };

  /** @brief Get the tagging decision from specific FlavourTag tagger. */
  class TaggingDecision : public Function {
  public:
    /** Make some error message more informative. */
    static constexpr auto name() { return "TaggingDecision"; }

    /** Constructor */
    TaggingDecision( std::string tagger_name )
        : m_tagger_name( std::move( LHCb::Tagger::TaggerTypeToType( tagger_name ) ) ) {}
    TaggingDecision( int tagger_type ) : m_tagger_name( std::move( LHCb::Tagger::TaggerType( tagger_type ) ) ) {}
    template <typename FlavourTags, typename Data>
    auto operator()( FlavourTags const& ftags, Data const& d ) const {
      auto tagger = detail::findMatchingFlavourTagger( ftags, d, m_tagger_name );
      return tagger.has_value() ? tagger.value().decision() : LHCb::FlavourTag{}.decision();
    }

  private:
    LHCb::Tagger::TaggerType m_tagger_name;
  };

  /** @brief Get the tagging mistag rate from specific FlavourTag tagger. */
  class TaggingMistag : public Function {
  public:
    /** Make some error message more informative. */
    static constexpr auto name() { return "TaggingMistag"; }

    /** Constructor */
    TaggingMistag( std::string tagger_name )
        : m_tagger_name( std::move( LHCb::Tagger::TaggerTypeToType( tagger_name ) ) ) {}
    TaggingMistag( int tagger_type ) : m_tagger_name( std::move( LHCb::Tagger::TaggerType( tagger_type ) ) ) {}
    template <typename FlavourTags, typename Data>
    auto operator()( FlavourTags const& ftags, Data const& d ) const {
      auto tagger = detail::findMatchingFlavourTagger( ftags, d, m_tagger_name );
      return tagger.has_value() ? tagger.value().omega() : LHCb::FlavourTag{}.omega();
    }

  private:
    LHCb::Tagger::TaggerType m_tagger_name;
  };

  /** @brief Get the tagging MVA output from specific FlavourTag tagger. */
  class TaggingMVAOutput : public Function {
  public:
    /** Make some error message more informative. */
    static constexpr auto name() { return "TaggingMVAOutput"; }

    /** Constructor */
    TaggingMVAOutput( std::string tagger_name )
        : m_tagger_name( std::move( LHCb::Tagger::TaggerTypeToType( tagger_name ) ) ) {}
    TaggingMVAOutput( int tagger_type ) : m_tagger_name( std::move( LHCb::Tagger::TaggerType( tagger_type ) ) ) {}
    template <typename FlavourTags, typename Data>
    auto operator()( FlavourTags const& ftags, Data const& d ) const {
      auto tagger = detail::findMatchingFlavourTagger( ftags, d, m_tagger_name );
      return tagger.has_value() ? tagger.value().mvaValue() : LHCb::Tagger{}.mvaValue();
    }

  private:
    LHCb::Tagger::TaggerType m_tagger_name;
  };

  namespace BTracking {

    template <typename Composite2TrackRelations, typename Composite>
    auto getTrack( Composite2TrackRelations const& relations, Composite const& composite ) {
      auto const& comp        = Sel::Utils::deref_if_ptr( composite );
      auto        track_range = relations.relations( &comp );
      return track_range.empty() ? std::nullopt : Functors::Optional{track_range.front().to()};
    }

    /** @brief Get heavy flavour track associated to composite. */
    struct Track : public Function {
      static constexpr auto name() { return "BTracking::Track"; }

      template <typename Composite2TrackRelations, typename Composite>
      auto operator()( Composite2TrackRelations const& relations, Composite const& composite ) const {
        return getTrack( relations, composite );
      }
    };

    /** @brief Calculate the corrected mass using heavy flavour tracking info. */
    struct CorrectedMass : public Function {
      static constexpr auto name() { return "BTracking::CorrectedMass"; }

      template <typename Composite2TrackRelations, typename Composite>
      auto operator()( Composite2TrackRelations const& relations, Composite const& composite ) const {
        using FType = SIMDWrapper::scalar::float_v;
        using Sel::Utils::mass2;
        using Sel::Utils::threeMomentum;

        auto track = getTrack( relations, composite );
        if ( !track.has_value() ) return FType{0};

        // direction using first hit (or end vertex if not available, as saved in heavy flavour track)
        auto const state = track.value()->firstState();
        auto const d     = LHCb::LinAlg::Vec<FType, 3>{state.tx(), state.ty(), 1.};

        // Get the pT variable that we need
        auto const mom  = threeMomentum( composite );
        auto const perp = mom - d * ( dot( mom, d ) / d.mag2() );
        auto const pt   = perp.mag();

        // Calculate the corrected mass
        return pt + sqrt( mass2( composite ) + pt * pt );
      }
    };
  } // namespace BTracking

} // namespace Functors::Composite
