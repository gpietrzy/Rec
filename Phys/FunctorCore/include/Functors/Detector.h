/***************************************************************************** \
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// This code is DD4hep specific
#if USE_DD4HEP

#  include <Detector/LHCb/DeLHCb.h>
#  include <Functors/Function.h>
#  include <GaudiKernel/Algorithm.h>
#  include <GaudiKernel/DataObjID.h>
#  include <GaudiKernel/DataObjectHandle.h>
#  include <GaudiKernel/GaudiException.h>
#  include <LbDD4hep/IDD4hepSvc.h>
#  include <optional>
#  include <string>

// this is where standard_geometry_top is defined
#  include <DetDesc/IDetectorElement.h>

namespace Functors::Detector {
  struct DeLHCb final : public Function {
    DeLHCb() {}

    DeLHCb( DeLHCb const& ) {}

    /** Set up the DataHandles, attributing the data dependencies to the given
     *  algorithm, using .emplace() with the Condition location and algorithm pointer.
     */
    void bind( TopLevelInfo& top_level ) {
      m_condCtx.emplace( DataObjID{LHCb::Det::LbDD4hep::IDD4hepSvc::DefaultSliceLocation}, top_level.algorithm() );
      // This is a workaround for DataObjectReadHandle::init being protected.
      static_cast<Gaudi::DataHandle*>( &*m_condCtx )->init();
    }

    /**
     * Retrieve the data dependencies from the Condition and bake them into the
     * "prepared" functor that we return
     */
    auto prepare( EventContext const& /* evtCtx */, TopLevelInfo const& ) const {
      // Note: getting the a condition key from a string is really this complicated
      static const dd4hep::Condition::key_type delhcb_key = [] {
        auto const& path     = LHCb::standard_geometry_top;
        const auto  colonPos = path.find_first_of( ':' );
        assert( colonPos != path.npos );
        const auto det_name  = path.substr( 0, colonPos );
        const auto cond_name = path.substr( colonPos + 1 );
        return dd4hep::ConditionKey::KeyMaker{dd4hep::detail::hash32( det_name ), cond_name}.hash;
      }();

      if ( !m_condCtx ) {
        throw GaudiException{"DeLHCb Functor called without being bound to an algorithm", "Functors::Detector::DeLHCb",
                             StatusCode::FAILURE};
      }

      // Ideally we should pass EventContext to DataObjectReadHandle::get, but the API was never implemented
      auto& slice = *m_condCtx->get();
      // Get the DeLHCb instance from the current conditions slice
      // and bake this into a new lambda that we return
      return [de = LHCb::Detector::DeLHCb( slice->pool->get( delhcb_key ) )]( auto const&... ) { return de; };
    }

  private:
    std::optional<DataObjectReadHandle<LHCb::Det::LbDD4hep::IDD4hepSvc::DD4HepSlicePtr>> m_condCtx;
  };

  namespace details {
    template <auto fun>
    struct DeLHCInfoFunctor final : Function {
      auto operator()( LHCb::Detector::DeLHCb const& de ) const {
        auto info = de.lhcInfo();
        return info ? Functors::Optional{std::invoke( fun, *info )} : std::nullopt;
      }
    };

    template <auto fun>
    struct DeSMOGInfoFunctor final : Function {
      auto operator()( LHCb::Detector::DeLHCb const& de ) const {
        auto info = de.SMOG();
        return info ? Functors::Optional{static_cast<int>( std::invoke( fun, *info ) )} : std::nullopt;
      }
    };

  } // namespace details

  // Get the fill number from LHC condition
  using FillNumber = details::DeLHCInfoFunctor<&LHCb::Detector::LHCInfo::fillnumber>;

  // Get the LHC energy condition
  using LHCEnergy = details::DeLHCInfoFunctor<&LHCb::Detector::LHCInfo::lhcenergy>;

  // Get the LHCb clock phase from LHC condition
  using LHCbClockPhase = details::DeLHCInfoFunctor<&LHCb::Detector::LHCInfo::lhcbclockphase>;

  // Get the SMOG injection mode from SMOG condition
  using SMOGInjectionMode = details::DeSMOGInfoFunctor<&LHCb::Detector::SMOGInfo::injection_mode>;

  // Get the SMOG injected gas
  using SMOGInjectedGas = details::DeSMOGInfoFunctor<&LHCb::Detector::SMOGInfo::injected_gas>;

  // Get the SMOG stable injection flag
  using SMOGStableInjection = details::DeSMOGInfoFunctor<&LHCb::Detector::SMOGInfo::stable_injection>;

} // namespace Functors::Detector
#endif
