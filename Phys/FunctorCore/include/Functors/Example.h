/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Function.h"
#include <algorithm>
#include <tuple>

namespace Functors::Examples {
  struct TimesTwo : public Function {
    template <typename Data>
    auto operator()( Data const& value ) const {
      return value * 2;
    }
  };

  struct PlusN : public Function {
    PlusN( int n ) : N( n ) {}
    template <typename Data>
    auto operator()( Data const& value ) const {
      return value + N;
    }
    int N{0};
  };

  struct GreaterThan : public Predicate {
    int m_v;
    GreaterThan( int v ) : m_v( v ) {}
    template <typename Data>
    bool operator()( Data const& value ) const {
      return value > m_v;
    }
  };

  struct ThorBeatsLoki : public Predicate {
    // for reference see https://www.youtube.com/watch?v=-JPkvO1e1Gw
    template <typename... Data>
    bool operator()( Data const&... ) const {
      return true;
    }
  };

  template <typename T>
  struct OptReturn : public Function {
    OptReturn( T n ) : m_n( n ) {}
    auto operator()( bool return_opt ) const { return return_opt ? Functors::Optional{m_n} : std::nullopt; }
    T    m_n{};
  };

  // mainly to test the bind logic
  struct AddInputs : public Function {
    template <typename... Data>
    auto operator()( Data const&... input ) const {
      return ( input + ... );
    }
  };

} // namespace Functors::Examples
