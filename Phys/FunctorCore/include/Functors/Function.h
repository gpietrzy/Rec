/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/Optional.h"
#include "GaudiKernel/detected.h"
#include "SelKernel/Utilities.h"
#include "boost/mp11/algorithm.hpp"
#include "boost/mp11/bind.hpp"
// #include "boost/math/special_functions/sign.hpp"
#include "LHCbMath/MatVec.h"
#include <cmath>
#include <type_traits>
#include <utility>

namespace Functors::detail {
  template <typename... T>
  struct always_false : std::bool_constant<false> {};

  template <typename... T>
  constexpr bool always_false_v = always_false<T...>::value;

  namespace impl {
    template <typename T>
    struct is_tuple : std::false_type {};

    template <typename... Ts>
    struct is_tuple<std::tuple<Ts...>> : std::true_type {};

    template <typename T>
    struct is_integral_constant_t : std::false_type {};

    template <typename T, T value>
    struct is_integral_constant_t<std::integral_constant<T, value>> : std::true_type {};
  } // namespace impl

  template <typename T>
  struct is_tuple : impl::is_tuple<std::decay_t<T>> {};

  template <class T>
  inline constexpr bool is_tuple_v = is_tuple<T>::value;

  template <typename T>
  struct is_integral_constant : impl::is_integral_constant_t<std::decay_t<T>> {};

  template <class T>
  inline constexpr bool is_integral_constant_v = is_integral_constant<T>::value;

  template <typename T>
  struct is_optional_unwrapper : std::false_type {};

  template <typename T>
  constexpr bool is_optional_unwrapper_v = is_optional_unwrapper<T>::value;

  template <typename T, typename Tuple>
  struct is_constructible_from_tuple;

  template <typename T, typename... Args>
  struct is_constructible_from_tuple<T, std::tuple<Args...>> {
    template <typename U>
    static auto check( int ) -> decltype( std::is_constructible<U, Args...>{}, std::true_type{} );

    template <typename>
    static std::false_type check( ... );

    static constexpr bool value = decltype( check<T>( 0 ) )::value;
  };

  template <typename T, typename Tuple>
  inline constexpr bool is_constructible_from_tuple_v = is_constructible_from_tuple<T, Tuple>::value;

  template <typename Fs, typename M, typename... In>
  inline constexpr bool has_any_optional_return() {
    using func         = boost::mp11::mp_bind_back<std::invoke_result_t, mask_arg_t, M, In...>;
    using result_types = boost::mp11::mp_transform_q<func, Fs>;
    return boost::mp11::mp_to_bool<boost::mp11::mp_count_if<result_types, is_our_optional_t>>::value;
  };

} // namespace Functors::detail

/** @file  Function.h
 *  @brief Helper types and operator overloads for composition of functions.
 */

namespace Functors {
  /** @class Function
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All functors inherit from this class.
   */
  struct Function {};

  /** @class Predicate
   *  @brief Empty base class for use with std::is_base_of_v.
   *
   *  All predicates inherit from this class. A predicate is a function that
   *  returns a bool-like. A predicate can be wrapped up in the Filter functor
   *  to enable container-wise operations, and logical operations (and, or,
   *  etc.) are enabled for predicates but not for functions.
   */
  struct Predicate : public Function {};

  /** @namespace Functors::detail
   *
   *  Internal helpers for new style functors.
   */
  namespace detail {
    /** Helper to check if a given type is one of our functions (as opposed to
     *  predicates)
     */
    template <typename F>
    inline constexpr bool is_functor_predicate_v = std::is_base_of_v<Predicate, std::decay_t<F>>;
  } // namespace detail

  /** @class NumericValue
   *  @brief Trivial function that holds a numeric constant.
   *
   *  This is the type that is generated for the RHS of expressions such as
   *  "ETA > 2".
   */
  template <typename PODtype>
  struct NumericValue : public Function {
    constexpr NumericValue( PODtype value ) : m_value( value ) {}
    template <typename... Input>
    constexpr PODtype operator()( Input&&... ) const {
      return m_value;
    }

  private:
    PODtype m_value;
  };

  template <typename T>
  NumericValue( T )->NumericValue<T>;
  template <typename T, T v>
  NumericValue( std::integral_constant<T, v> )->NumericValue<T>;

  /** @class AcceptAll
   *  @brief Trivial functor that always returns true.
   *  @todo  Better handling in case sizeof...(Data) > 1
   */
  struct AcceptAll : public Predicate {
    template <typename... Data>
    constexpr auto operator()( Data&&... ) const {
      if constexpr ( sizeof...( Data ) > 0 ) {
        using FirstType = std::decay_t<std::tuple_element_t<0, std::tuple<Data...>>>;
        if constexpr ( Sel::Utils::has_static_mask_true_v<FirstType> ) {
          return FirstType::mask_true();
        } else {
          return true;
        }
      } else {
        return true;
      }
    }
  };

  /** @class AcceptNone
   *  @brief Trivial functor that always returns false.
   *  @todo  Better handling in case sizeof...(Data) > 1
   */
  struct AcceptNone : public Predicate {
    template <typename... Data>
    constexpr auto operator()( Data&&... ) const {
      if constexpr ( sizeof...( Data ) > 0 ) {
        using FirstType = std::decay_t<std::tuple_element_t<0, std::tuple<Data...>>>;
        if constexpr ( Sel::Utils::has_static_mask_true_v<FirstType> ) {
          return !FirstType::mask_true();
        } else {
          return false;
        }
      } else {
        return false;
      }
    }
  };

  /** @class Identity
   *  @brief Trivial functor that returns the same value.
   */
  struct Identity_t : public Function {
    template <typename T>
    auto operator()( const T& t ) const {
      return t;
    }
  };

  /** @class Column
  Instantiated with a "label" , when acting on object T returns T("label")
   */
  struct Column_t : public Function {
    Column_t( std::string label ) : m_label{std::move( label )} {}

    template <typename T>
    auto operator()( const T& t ) const {
      return t( m_label );
    }

  private:
    std::string m_label{""};
  };

  namespace detail {

    namespace composition {
      // tag structs to steer the composition of functors
      struct chain final {};
      struct bind  final {};
    } // namespace composition

    template <typename M, typename T, typename F, typename... Fs>
    decltype( auto ) chain_helper( M& mask, T& tuple_of_inputs, F const& f, Fs const&... funcs ) {
      auto&& tmp = chain_helper( mask, tuple_of_inputs, funcs... );
      if constexpr ( detail::is_our_optional_v<std::decay_t<decltype( tmp )>> &&
                     !is_optional_unwrapper_v<std::decay_t<typename F::Functor_t>> ) {
        if constexpr ( detail::is_tuple_v<decltype( tmp.value() )> ) {
          return !tmp.has_value() ? std::nullopt
                                  : Functors::Optional{std::apply(
                                        [&f, &mask]( auto&&... inputs ) -> decltype( auto ) {
                                          return f( mask_arg, mask, std::forward<decltype( inputs )>( inputs )... );
                                        },
                                        std::forward<decltype( tmp.value() )>( tmp.value() ) )};
        } else {
          return !tmp.has_value()
                     ? std::nullopt
                     : Functors::Optional{f( mask_arg, mask, std::forward<decltype( tmp.value() )>( tmp.value() ) )};
        }

      } else {
        if constexpr ( detail::is_tuple_v<decltype( tmp )> ) {
          return std::apply(
              [&f, &mask]( auto&&... inputs ) -> decltype( auto ) {
                return f( mask_arg, mask, std::forward<decltype( inputs )>( inputs )... );
              },
              std::forward<decltype( tmp )>( tmp ) );
        } else {
          return f( mask_arg, mask, std::forward<decltype( tmp )>( tmp ) );
        }
      }
    }

    template <typename M, typename T, typename F>
    decltype( auto ) chain_helper( M& mask, T& tuple_of_inputs, F const& f ) {
      return std::apply(
          [&f, &mask]( auto&&... inputs ) -> decltype( auto ) {
            return f( mask_arg, mask, std::forward<decltype( inputs )>( inputs )... );
          },
          std::move( tuple_of_inputs ) );
    }

    template <typename MF, std::size_t... Idx, typename F, typename M, typename... In>
    auto bind_helper_optional( MF const& main_functor, std::index_sequence<Idx...>, F const& tup, M mask,
                               In&&... inputs ) {

      // helper to check if a returned value from a bound functor is optional,
      // and if so check if it has a value
      auto check = []( auto const& val ) {
        if constexpr ( is_our_optional_v<std::decay_t<decltype( val )>> ) { return val.has_value(); }
        return true;
      };

      // unpack and fwd the different kinds of expected return types from the bound functors
      auto get_val = []( auto&& val ) -> decltype( auto ) {
        using val_t = decltype( val );
        if constexpr ( is_our_optional_v<std::decay_t<val_t>> ) {
          return std::forward_as_tuple( std::forward<val_t>( val ).value() );
        } else if constexpr ( detail::is_tuple_v<std::decay_t<val_t>> ) {
          return std::forward<val_t>( val );
        } else {
          return std::forward_as_tuple( std::forward<val_t>( val ) );
        }
      };

      // a bit weird but we create this immediately invoked lambda to work
      // around lifetime rules. The below expression:
      // std::forward_as_tuple(std::get<Idx + 1>(tup)(mask_arg, mask, inputs...)...)
      // creates a tuple of lvalue/rvalue references to the returned values
      // from the bound functors. But the lifetime of rvalue references to
      // temporaries are not extended! Thus you can't do e.g. auto a =
      // std::forward_as_tuple(...); and then do something with a;
      // But as per standard:  A temporary object bound to a reference
      // parameter in a function call (5.2.2) persists until the completion of the
      // full-expression containing the call.
      // Thus if we pass the result of forward_as_tuple into an immediately
      // invoked lambda, the objects in the tuple are valid until the lambda
      // returns.
      return [&]( auto tuple_of_results ) {
        // check if any of the contained optionals do not have a value;
        bool abort = !( check( std::get<Idx>( tuple_of_results ) ) && ... );

        // use std::move on the tuple_of_results so that the get_val sees a
        // rvalue reference and can move the objects
        return abort ? std::nullopt
                     : Functors::Optional{std::apply(
                           [mask, &main_functor]( auto&&... args ) -> decltype( auto ) {
                             return main_functor( mask_arg, mask, args... );
                           },
                           std::tuple_cat( get_val( std::get<Idx>( std::move( tuple_of_results ) ) )... ) )};
      }( std::forward_as_tuple( std::get<Idx + 1>( tup )( mask_arg, mask, inputs... )... ) );
    }

    template <typename MF, std::size_t... Idx, typename F, typename M, typename... In>
    auto bind_helper( MF const& main_functor, std::index_sequence<Idx...>, F const& tup, M mask, In&&... inputs ) {

      auto fwd_as_tuple = []( auto&& val ) -> decltype( auto ) {
        using val_t = decltype( val );
        if constexpr ( is_tuple_v<std::decay_t<val_t>> ) {
          return std::forward<val_t>( val );
        } else {
          return std::forward_as_tuple( std::forward<decltype( val )>( val ) );
        }
      };

      return std::apply(
          [mask, &main_functor]( auto&&... args ) -> decltype( auto ) {
            return main_functor( mask_arg, mask, std::forward<decltype( args )>( args )... );
          },
          std::tuple_cat( fwd_as_tuple(
              std::get<Idx + 1>( tup )( mask_arg, mask, std::forward<decltype( inputs )>( inputs )... ) )... ) );
    }

    /** @class ComposedFunctor
     *  @brief Workhorse for producing a new functor from existing ones.
     *
     *  This class is used to implement all binary function operations:
     *  @li arithmetric operators (+, -, *, /, %), two functions -> function.
     *  @li comparison operators (>, <, etc.), two functions -> predicate.
     *  @li logical operators (&, |), two predicates -> predicate.
     *
     *  It is also be used to implement functor transformations, like
     *  math::log( PT ), math::pow( PT, 2 ), math::in_range( min, PT, max )
     *  and so on. And the unary negation operators.
     */
    template <typename Operator, typename InputBase, typename NewBase, typename... F>
    struct ComposedFunctor : public NewBase {
      // Check that all owned functors inherit from the specified base class.
      static_assert( ( std::is_base_of_v<InputBase, F> && ... ) );

      template <typename... U, typename = std::enable_if_t<std::is_constructible_v<std::tuple<F...>, U...>>>
      ComposedFunctor( U&&... u ) : m_fs{std::forward<U>( u )...} {}

      /** Prepare the composite functor for use. */
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) const& {
        return prepare( evtCtx, top_level, std::index_sequence_for<F...>{} );
      }

      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level ) && {
        // FIXME the below is not a proper fix, it just makes it harder to
        // mess up. the current implementation doesn't support this behaviour
        // so we assert it here. take the following example:
        // Functors::NumericValue make1( 1 );
        // auto prepared_f = (make1 + make1).prepare(evtCtx, top_level);
        // std::cout << prepared_f(true) << std::endl;
        //
        // prepared_f is a lambda that contains references to the functors
        // owned by the ComposedFunctor created by the operator+, but that
        // object has since gone out of scope.

        static_assert( detail::always_false<InputBase>::value, "prepare() on a temporary Functor is not supported" );
        // the below is only here so the compiler doesn't first complain about
        // other type problems and the error message stays clear and directly
        // points to the above assert
        return prepare( evtCtx, top_level, std::index_sequence_for<F...>{} );
      }

      /** Contained functors may have data-dependencies that need to be
       *  associated to the owning algorithm.
       */
      void bind( TopLevelInfo& top_level ) { ::Functors::detail::bind( m_fs, top_level ); }

      /** Flag that this functor explicitly receives/handles a mask giving the
       *  current validity of the arguments.
       */
      constexpr static bool requires_explicit_mask = true;

    private:
      /** Helper to prepare all owned functors and return a new lambda that
       *  holds the prepared functors and the knowledge (encoded by Operator)
       *  of how to combine them into the result of the new (composed)
       * functor.
       */
      template <std::size_t... I>
      auto prepare( EventContext const& evtCtx, TopLevelInfo const& top_level, std::index_sequence<I...> ) const {
        // Prepare all of the functors and capture the resulting tuple
        return [fs = std::tuple{detail::prepare( std::get<I>( m_fs ), evtCtx, top_level )...}]
#ifndef __clang__
            __attribute__( ( flatten ) )
#endif
            ( auto mask, auto&&... input )
                ->decltype( auto ) {
          if constexpr ( std::is_same_v<Operator, std::logical_and<>> ) {
            // NOTE: 'and' combinations with more than 2 predicates are typically constructed by the additional
            //       overloads of operator& which combines an instance of this specialization with a predicate,
            //       or two instances.
            static_assert( sizeof...( I ) > 1 );
            using Sel::Utils::any; // for the scalar case
            // Operator{}( ... ) would not short-circuit in this case
            // Short circuit vectorised cuts as well as scalar ones -- use a fold expression do to so,
            // which has the right short-circuit behavior. Basically: as long as mask contains any valid
            // entries, update mask (by invoking the next functor for those  entries which are still valid),
            // and check again -- until mask no longer has any valid entries, i.e. any(mask) becomes false
            ( any( mask ) && ... &&
              ( mask = mask &&
                       std::invoke( std::get<I>( fs ), mask_arg, mask, std::forward<decltype( input )>( input )... ),
                any( mask ) ) );
            return mask;
          } else if constexpr ( std::is_same_v<Operator, std::logical_or<>> ) {
            static_assert( sizeof...( I ) == 2 );
            // Operator{}( ... ) would not short-circuit in this case
            // we start with 'first' set to all entries that should _not_ be checked, and then
            // we _add_ those which pass the first fs (and thus don't have to be checked anymore) to first
            auto const first =
                !mask || std::invoke( std::get<0>( fs ), mask_arg, mask, std::forward<decltype( input )>( input )... );
            // Short circuit vectorised cuts as well as scalar ones
            using Sel::Utils::all; // for the scalar case
            // if nothing left to check (i.e. all _possible_ elements passed the first fs), return the original mask
            if ( all( first ) ) return mask;
            // first now contains all elements that are either invalid, or have already passed, so !first
            // corresponds to all valid, not yet passed elements - so use that as mask to second fs.
            // We then update first with the ones that passed the second fs. At this point, first contains
            // all elements which are either invalid, or have passed one of the two fs), and then combine
            // with 'all valid' (i.e. mask) to get all valid _passed_ elements
            // Basically, mask the RHS so it only has to calculate the bits that would
            // make a difference to the return value.
            return mask && ( first || std::invoke( std::get<1>( fs ), mask_arg, !first,
                                                   std::forward<decltype( input )>( input )... ) );
          } else if constexpr ( std::is_same_v<Operator, composition::chain> ) {
            return std::apply(
                [&mask, tuple_of_inputs = std::forward_as_tuple( std::forward<decltype( input )>( input )... )](
                    auto const&... functors ) -> decltype( auto ) {
                  return chain_helper( mask, tuple_of_inputs, functors... );
                },
                fs );

          } else if constexpr ( std::is_same_v<Operator, composition::bind> ) {
            auto const& main_functor = std::get<0>( fs );

            // a couple lines of boost mp11 template magic to check if any of
            // the bound functors return an optional.
            using types_of_functors       = std::remove_cv_t<decltype( fs )>;
            using types_of_bound_functors = boost::mp11::mp_pop_front<types_of_functors>;
            constexpr bool has_optional_return =
                detail::has_any_optional_return<types_of_bound_functors, decltype( mask ), decltype( input )...>();

            // we pass the main_functor into the helper so that the helper can
            // use foroward_as_tuple the return values of the bound functors to
            // call the main_functor, thus (hopefully) perfectly forwarding the
            // output of the bound functors to the main_functor.
            if constexpr ( has_optional_return ) {
              return bind_helper_optional( main_functor, std::make_index_sequence<sizeof...( I ) - 1>{}, fs, mask,
                                           std::forward<decltype( input )>( input )... );
            } else {
              return bind_helper( main_functor, std::make_index_sequence<sizeof...( I ) - 1>{}, fs, mask,
                                  std::forward<decltype( input )>( input )... );
            }
          } else {
            // Evaluate all the functors and let the given Operator combine the
            // results.

            // Check all possible optional returns
            constexpr bool has_any_optional_input =
                detail::has_any_optional_return<std::remove_cv_t<decltype( fs )>, decltype( mask ),
                                                decltype( input )...>();

            // If one of the functor has optional return, we have to propagate it
            if constexpr ( has_any_optional_input ) {
              const auto& all_inputs = std::make_tuple(
                  std::invoke( std::get<I>( fs ), mask_arg, mask, std::forward<decltype( input )>( input )... )... );

              auto check = []( auto const& val ) {
                if constexpr ( is_our_optional_v<std::decay_t<decltype( val )>> ) {
                  return val.has_value();
                } else {
                  return true;
                }
              };
              bool has_invalid_value = !( check( std::get<I>( all_inputs ) ) && ... );

              auto get_value = []( auto const& val ) {
                if constexpr ( is_our_optional_v<std::decay_t<decltype( val )>> ) {
                  return std::forward<decltype( val.value() )>( val.value() );
                } else {
                  return std::forward<decltype( val )>( val );
                }
              };

              // Predicate functor should not return optional values.
              if constexpr ( std::is_same_v<NewBase, Predicate> ) {
                return has_invalid_value ? false : Operator{}( get_value( std::get<I>( all_inputs ) )... );
              } else {
                return has_invalid_value ? std::nullopt
                                         : Functors::Optional{Operator{}( get_value( std::get<I>( all_inputs ) )... )};
              }

            } else {
              return Operator{}(
                  std::invoke( std::get<I>( fs ), mask_arg, mask, std::forward<decltype( input )>( input )... )... );
            }
          }
        };
      }
      std::tuple<F...> m_fs;

    public:
      auto move_functors() && { return std::move( m_fs ); }
    };

    /** Helper function to make a ComposedFunctor that deduces the actual
     *  functor types from its argument types, but leaves us to specify the
     *  other details explicitly.
     */
    template <typename Operator, typename InputBase, typename NewBase, typename... Args>
    constexpr auto compose( Args&&... args ) {
      return ComposedFunctor<Operator, InputBase, NewBase, std::decay_t<Args>...>( std::forward<Args>( args )... );
    }

    /** Use our own helper to define in one place which types we allow to be
     *  promoted to NumericValue.
     */
    template <typename F>
    inline constexpr bool is_promotable_v = std::is_arithmetic_v<std::decay_t<F>> || is_integral_constant_v<F>;

    /** Helper to promote arithmetic constants to NumericValue instances while
     *  not doing anything to functors.
     */
    template <typename F>
    constexpr auto promote( F f ) {
      if constexpr ( is_promotable_v<F> ) {
        return NumericValue{f};
      } else {
        static_assert( is_functor_function_v<F>, "Functors::detail::promote( f ) got invalid input" );
        return f;
      }
    }

    /** Helper to make a ComposedFunctor from a set of arguments that can be a
     *  mix of arithmetic constants and functor expressions. Arithmetic
     *  constants are promoted to NumericValue instances using promote().
     */
    template <typename Operator, typename InputBase, typename NewBase, typename... Args>
    constexpr auto promote_and_compose( Args&&... args ) {
      return compose<Operator, InputBase, NewBase>( promote( std::forward<Args>( args ) )... );
    }

    /** Helper for checking if our overloaded operators should be considered.
     *
     *  The condition is that either both LHS and RHS are our functions, or one
     *  of them is and the other side of the expression is an arithmetic
     *  constant that we can promote to a NumericValue.
     */
    template <typename F1, typename F2>
    using enable_function_operator =
        std::enable_if_t<( is_functor_function_v<F1> && (is_promotable_v<F2> || is_functor_function_v<F2>)) ||
                             (is_promotable_v<F1> && is_functor_function_v<F2>),
                         int>;

    /** SFINAE helper for defining boolean (bitwise...) logic overloads.
     */
    template <typename F1, typename F2>
    using enable_if_both_predicates = std::enable_if_t<is_functor_predicate_v<F1> && is_functor_predicate_v<F2>, int>;

    template <typename... Fs>
    using enable_if_all_predicates =
        std::enable_if_t<std::conjunction_v<std::is_base_of<Predicate, std::decay_t<Fs>>...>, int>;

    template <typename... Fs>
    using enable_if_all_functions =
        std::enable_if_t<std::conjunction_v<std::is_base_of<Function, std::decay_t<Fs>>...>, int>;

    // enable if F1 is Predicate, F2 and F3 are Function
    template <typename F1, typename F2, typename F3>
    using enable_if_predicate_function_function = std::enable_if_t<std::is_base_of_v<Predicate, std::decay_t<F1>> &&
                                                                       std::is_base_of_v<Function, std::decay_t<F2>> &&
                                                                       std::is_base_of_v<Function, std::decay_t<F3>>,
                                                                   int>;

  } // namespace detail

  /** operator+ overload covering all three combinations of function/arithmetic
   *
   *  This turns two functions (hence the first Function) into a new
   *  function (hence the second Function). The other operator overloads
   *  defined here are similar.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator+( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::plus<>, Function, Function>( std::forward<F1>( lhs ),
                                                                         std::forward<F2>( rhs ) );
  }

  /** operator- overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator-( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::minus<>, Function, Function>( std::forward<F1>( lhs ),
                                                                          std::forward<F2>( rhs ) );
  }

  /** operator* overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator*( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::multiplies<>, Function, Function>( std::forward<F1>( lhs ),
                                                                               std::forward<F2>( rhs ) );
  }

  /** operator/ overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator/( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::divides<>, Function, Function>( std::forward<F1>( lhs ),
                                                                            std::forward<F2>( rhs ) );
  }

  /** operator% overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator%( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::modulus<>, Function, Function>( std::forward<F1>( lhs ),
                                                                            std::forward<F2>( rhs ) );
  }

  /** Unary operator- overload. */
  template <typename F, typename std::enable_if_t<detail::is_functor_function_v<F>, int> = 0>
  constexpr auto operator-( F&& f ) {
    return detail::compose<std::negate<>, Function, Function>( std::forward<F>( f ) );
  }

  /** operator| overload for turning two predicates into another one.
   *
   *  detail::promote_and_compose is not used here because we don't support
   *  logical operations between predicates and arithmetic constants.
   */
  template <typename F1, typename F2, typename detail::enable_if_both_predicates<F1, F2> = 0>
  constexpr auto operator|( F1&& lhs, F2&& rhs ) {
    return detail::compose<std::logical_or<>, Predicate, Predicate>( std::forward<F1>( lhs ), std::forward<F2>( rhs ) );
  }

  /** operator& overload for turning 2 predicates into one. */
  template <typename F1, typename F2, typename detail::enable_if_both_predicates<F1, F2> = 0>
  constexpr auto operator&( F1&& lhs, F2&& rhs ) {
    return detail::compose<std::logical_and<>, Predicate, Predicate>( std::forward<F1>( lhs ),
                                                                      std::forward<F2>( rhs ) );
  }

  /** operator~ overload, creates a new functor representing the logical
   *  negation of the given functor.
   */
  template <typename F, typename std::enable_if_t<detail::is_functor_predicate_v<F>, int> = 0>
  constexpr auto operator~( F&& obj ) {
    return detail::compose<std::logical_not<>, Predicate, Predicate>( std::forward<F>( obj ) );
  }

  /** operator> overload covering all three combinations of function/arithmetic
   *
   *  This, and the others below, transforms two Functions (hence Function)
   *  into a Predicate (hence the 3rd template argument).
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator>( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::greater<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                             std::forward<F2>( rhs ) );
  }

  /** operator< overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator<( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::less<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                          std::forward<F2>( rhs ) );
  }

  /** operator>= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator>=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::greater_equal<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                                   std::forward<F2>( rhs ) );
  }

  /** operator<= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator<=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::less_equal<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                                std::forward<F2>( rhs ) );
  }

  /** operator== overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator==( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::equal_to<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                              std::forward<F2>( rhs ) );
  }

  /** operator!= overload covering all three combinations of function/arithmetic
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto operator!=( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::not_equal_to<>, Function, Predicate>( std::forward<F1>( lhs ),
                                                                                  std::forward<F2>( rhs ) );
  }

  /** Wrapper for bitwise AND between two contained_functor_value objects.
   *
   * It's really operator& overloading, but we use that for functor composition
   * rather than arithmetic.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto bitwise_and( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::bit_and<>, Function, Function>( std::forward<F1>( lhs ),
                                                                            std::forward<F2>( rhs ) );
  }

  /** Wrapper for bitwise OR between two contained_functor_value objects.
   *
   * It's really operator| overloading, but we use that for functor composition
   * rather than arithmetic.
   */
  template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
  constexpr auto bitwise_or( F1&& lhs, F2&& rhs ) {
    return detail::promote_and_compose<std::bit_or<>, Function, Function>( std::forward<F1>( lhs ),
                                                                           std::forward<F2>( rhs ) );
  }

  /** Wrapper for function composition of functors
   *
   * given functors f and g and input x this return f(g(x))
   *
   * flattening like chain(chain(A,B),C) -> chain(A,B,C) is no longer done on
   * the C++ side but instead we already do this in python to avoid creating
   * many many intermediate types that cost alot of time and memory during
   * compliation, especially when debug info is turned on
   *
   */
  template <typename F1, typename... Fs, detail::enable_if_all_functions<Fs...> = 0>
  constexpr auto chain( F1&& func, Fs&&... funcs ) {
    // if the the last functor in the chain is a predicate then the resulting
    // composed functor should be one too.
    using NewBase_t = std::conditional_t<std::is_base_of_v<Predicate, std::decay_t<F1>>, Predicate, Function>;
    return detail::ComposedFunctor<detail::composition::chain, Function, NewBase_t, std::decay_t<F1>,
                                   std::decay_t<Fs>...>( std::forward<F1>( func ), std::forward<Fs>( funcs )... );
  }

  template <typename F1, typename... Fs, detail::enable_if_all_functions<Fs...> = 0>
  constexpr auto bind( F1&& bound_functor, Fs&&... funcs ) {
    static_assert( sizeof...( Fs ) > 0, "need at least two functors" );
    // if the functor we bind is a predicate then the resulting composed functor should be one too.
    using NewBase_t = std::conditional_t<std::is_base_of_v<Predicate, std::decay_t<F1>>, Predicate, Function>;

    return detail::ComposedFunctor<detail::composition::bind, Function, NewBase_t, std::decay_t<F1>,
                                   std::decay_t<Fs>...>( std::forward<F1>( bound_functor ),
                                                         std::forward<Fs>( funcs )... );
  }

  /** operator& overload for turning multiple predicates into one. */
  template <typename... Fs, detail::enable_if_all_predicates<Fs...> = 0>
  constexpr auto all_of( Fs&&... funcs ) {
    return detail::ComposedFunctor<std::logical_and<>, Predicate, Predicate, std::decay_t<Fs>...>(
        std::forward<Fs>( funcs )... );
  }

  namespace detail::math {
    struct pow_helper {
      template <typename... Args>
      constexpr auto operator()( Args&&... args ) const {
        return std::pow( LHCb::Utils::as_arithmetic( std::forward<Args>( args ) )... );
      }
    };

    struct log_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::log;
        return log( std::forward<Arg1>( arg1 ) );
      }
    };

    struct exp_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::exp;
        return exp( std::forward<Arg1>( arg1 ) );
      }
    };

    struct sqrt_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::sqrt;
        return sqrt( std::forward<Arg1>( arg1 ) );
      }
    };

    struct arccos_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::acos;
        return acos( LHCb::Utils::as_arithmetic( std::forward<Arg1>( arg1 ) ) );
      }
    };

    struct where_helper {
      template <typename Arg1, typename Arg2, typename Arg3>
      constexpr auto operator()( Arg1&& arg1, Arg2&& arg2, Arg3&& arg3 ) const {
        return arg1 ? arg2 : arg3;
      }
    };

    struct abs_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::abs;
        return abs( std::forward<Arg1>( arg1 ) );
      }
    };

    struct similarity_helper {
      template <typename VEC, typename COVMATRIX>
      constexpr auto operator()( VEC&& vec, COVMATRIX&& covMatrix ) const {
        return similarity( std::forward<VEC>( vec ), std::forward<COVMATRIX>( covMatrix ) );
      }
    };

    struct sign_helper {
      template <typename Arg1>
      constexpr auto operator()( Arg1&& arg1 ) const {
        using std::copysign;
        return copysign( 1., std::forward<Arg1>( arg1 ) );
      }
    };

    struct in_range_helper {
      /** This acts on the actual functor return values. */
      template <typename Min, typename Val, typename Max>
      constexpr auto operator()( Min min, Val val, Max max ) const {
        return ( val > min ) && ( val < max );
      }
    };
  } // namespace detail::math

  /** @namespace Functors::math
   *
   *  Mathematical functions that act on new style functors.
   */
  namespace math {
    /** Wrapper for std::pow( contained_functor_value, arg ).
     *
     *  e.g. math::pow( PT, 2 ) is a functor that returns the squared PT
     */
    template <typename... Args, typename = std::enable_if_t<sizeof...( Args ) == 2>>
    constexpr auto pow( Args&&... args ) {
      return detail::promote_and_compose<detail::math::pow_helper, Function, Function>( std::forward<Args>( args )... );
    }

    /** Wrapper for std::log( contained_functor_value ).
     */
    template <typename F>
    constexpr auto log( F&& f ) {
      return detail::promote_and_compose<detail::math::log_helper, Function, Function>( std::forward<F>( f ) );
    }

    /** Wrapper for std::exp( contained_functor_value ).
     */
    template <typename F>
    constexpr auto exp( F&& f ) {
      return detail::promote_and_compose<detail::math::exp_helper, Function, Function>( std::forward<F>( f ) );
    }

    /** Wrapper for std::sqrt( contained_functor_value ).
     */
    template <typename F>
    constexpr auto sqrt( F&& f ) {
      return detail::promote_and_compose<detail::math::sqrt_helper, Function, Function>( std::forward<F>( f ) );
    }

    /** Wrapper for std::acos( contained_functor_value ).
     */
    template <typename F>
    constexpr auto arccos( F&& f ) {
      return detail::promote_and_compose<detail::math::arccos_helper, Function, Function>( std::forward<F>( f ) );
    }

    /** Wrapper for (cond) ? true_val : false_val.
     */
    template <typename Pred, typename F2, typename F3,
              typename detail::enable_if_predicate_function_function<Pred, F2, F3> = 0>
    constexpr auto where( Pred&& cond, F2&& true_val, F3&& false_val ) {
      return detail::promote_and_compose<detail::math::where_helper, Function, Function>(
          std::forward<Pred>( cond ), std::forward<F2>( true_val ), std::forward<F3>( false_val ) );
    }

    /** Wrapper for std::abs( contained_functor_value ).
     */
    template <typename F>
    constexpr auto abs( F&& f ) {
      return detail::promote_and_compose<detail::math::abs_helper, Function, Function>( std::forward<F>( f ) );
    }

    /** Wrapper for ROOT::Math::Similarity( covMatrix, vec ).
     */
    template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
    constexpr auto similarity( F1&& vec, F2&& covMatrix ) {
      return detail::promote_and_compose<detail::math::similarity_helper, Function, Function>(
          std::forward<F1>( vec ), std::forward<F2>( covMatrix ) );
    }

    /** More efficient shorthand for (f > x) && (f < y) that only evaluates
     *  the value (f) once.
     */
    template <typename... Args, typename = std::enable_if_t<sizeof...( Args ) == 3>>
    constexpr auto in_range( Args&&... args ) {
      return detail::promote_and_compose<detail::math::in_range_helper, Function, Predicate>(
          std::forward<Args>( args )... );
    }

    /** Predicate returning true if bitwise AND between two contained_functor_value objects is non-zero.
     */
    template <typename F1, typename F2, typename detail::enable_function_operator<F1, F2> = 0>
    constexpr auto test_bit( F1&& lhs, F2&& rhs ) {
      return bitwise_and( std::forward<F1>( lhs ), std::forward<F2>( rhs ) ) != 0;
    }

    /* Sign function*/
    // Returns 1 if x > 0, -1 if x < 0, and 0 if x is zero.
    template <typename F>
    constexpr auto sign( F&& f ) {
      return detail::promote_and_compose<detail::math::sign_helper, Function, Function>( std::forward<F>( f ) );
    };

  } // namespace math
} // namespace Functors
