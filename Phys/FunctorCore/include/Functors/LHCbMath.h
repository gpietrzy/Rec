/*****************************************************************************\
* (c) Copyright 2023-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Functors/Adapters.h"
#include "Functors/Core.h"
#include "Functors/Function.h"
#include "Functors/Utilities.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "LHCbMath/ParticleParams.h"
#include "LHCbMath/ValueWithError.h"

#include <functional>
#include <type_traits>
#include <utility>

#include "Math/VectorUtil.h"

/*
 * Functors that access information of Gaudi::Math
 */
namespace Functors::LHCbMath {

  /**
   * @brief scalarMomentum, access the scalar momentum by input.scalarMomentum()
   */
  struct scalarMomentum : public Function {

    constexpr auto name() const { return "scalarMomentum"; }

    template <typename T>
    auto operator()( const T& input ) const -> decltype( input.scalarMomentum() ) {
      return input.scalarMomentum();
    }

    template <typename T>
    auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
      return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false<Data...>::value,
                     "The fucntor Functors::LHCbMath::scalarMomentum can only apply to object that has member function "
                     "scalarMomentum()" );
    }
  };

  /**
   * @brief normed_dot_3dim, calculates the cosine of angle between two 3-vectors. If passed vectors are 4-vectors, the
   * functor will use only coordinate part.
   */
  struct normed_dot_3dim : public Function {

    constexpr auto name() const { return "normed_dot_3dim"; }

    template <typename T1, typename T2>
    auto operator()( const T1& v1, const T2& v2 ) const {
      const auto dot = []( const auto x1, const auto y1, const auto z1, const auto x2, const auto y2, const auto z2 ) {
        return x1 * x2 + y1 * y2 + z1 * z2;
      };
      return dot( v1.x(), v1.y(), v1.z(), v2.x(), v2.y(), v2.z() ) /
             sqrt( dot( v1.x(), v1.y(), v1.z(), v1.x(), v1.y(), v1.z() ) *
                   dot( v2.x(), v2.y(), v2.z(), v2.x(), v2.y(), v2.z() ) );
    }

    template <typename T>
    auto operator()( const T& v1, const T& v2 ) const -> decltype( Functors::Optional{( *this )( *v1, *v2 )} ) {
      return ( v1 && v2 ) ? Functors::Optional{( *this )( *v1, *v2 )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false<Data...>::value,
                     "The functor Functors::LHCbMath::normed_dot_3dim can only apply to objects with implemented .x(), "
                     ".y() and .z() member functions or their pointers" );
    }
  };

  /**
   * @brief boost_to, boost the first input LHCb::LinAlg::Vec<4> to the frame, where the second input
   * LHCb::LinAlg::Vec<4> rests
   */
  struct boost_to : public Function {

    constexpr auto name() const { return "boost_to"; }

    template <typename T1, typename T2>
    auto operator()( const T1& input, const T2& ref ) const {
      const auto result =
          ( ROOT::Math::VectorUtil::boost( LHCb::LinAlg::convert( input ), LHCb::LinAlg::convert( ref ).BoostToCM() ) );
      return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>( result.X(), result.Y(), result.Z(), result.E() );
    }

    template <typename T>
    auto operator()( const T& p, const T& ref ) const -> decltype( Functors::Optional{( *this )( *p, *ref )} ) {
      return ( p && ref ) ? Functors::Optional{( *this )( *p, *ref )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert(
          detail::always_false<Data...>::value,
          "The functor Functors::LHCbMath::boost_to can only apply to LHCb::LinAlg::Vec<4> objects or their pointers" );
    }
  };

  /**
   * @brief boost_from, boost the first input LHCb::LinAlg::Vec<4> from the frame, where the second input
   * LHCb::LinAlg::Vec<4> rests, to the lab frame
   */
  struct boost_from : public Function {

    constexpr auto name() const { return "boost_from"; }

    template <typename T1, typename T2>
    auto operator()( const T1& input, const T2& ref ) const {
      const auto result = ( ROOT::Math::VectorUtil::boost( LHCb::LinAlg::convert( input ),
                                                           -LHCb::LinAlg::convert( ref ).BoostToCM() ) );
      return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>( result.X(), result.Y(), result.Z(), result.E() );
    }

    template <typename T>
    auto operator()( const T& p, const T& ref ) const -> decltype( Functors::Optional{( *this )( *p, *ref )} ) {
      return ( p && ref ) ? Functors::Optional{( *this )( *p, *ref )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false<Data...>::value, "The functor Functors::LHCbMath::boost_from can only apply "
                                                           "to LHCb::LinAlg::Vec<4> objects or their pointers" );
    }
  };

  /**
   * @brief mass, access the scalar momentum by input.invariantMass()
   */
  struct invariantMass : public Function {

    constexpr auto name() const { return "invariantMass"; }

    template <typename T>
    auto operator()( const T& input ) const -> decltype( input.invariantMass() ) {
      return input.invariantMass();
    }

    template <typename T>
    auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
      return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
    }

    template <typename... Data>
    auto operator()( Data... ) const {
      static_assert( detail::always_false<Data...>::value, "The fucntor Functors::LHCbMath::invariantMass can only "
                                                           "apply to object that has member function invariantMass()" );
    }
  };

  namespace ValueWithError {
    /**
     * @brief Value, access the value of Gaudi::Math::ValueWithError
     */
    struct Value : public Function {

      constexpr auto name() const { return "ValueWithError::Value"; }

      auto operator()( const Gaudi::Math::ValueWithError& input ) const { return input.value(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ValueWithError::Value can only apply to Gaudi::Math::ValueWithError" );
      }
    };

    /**
     * @brief Error, access the value of Gaudi::Math::ValueWithError
     */
    struct Error : public Function {

      constexpr auto name() const { return "ValueWithError::Error"; }

      auto operator()( const Gaudi::Math::ValueWithError& input ) const { return input.error(); }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ValueWithError::Error can only apply to Gaudi::Math::ValueWithError" );
      }
    };

  } // namespace ValueWithError

  namespace ParticleParams {
    /**
     * @brief flightDistance, access the decay length of Gaudi::Math::ParticleParams
     */
    struct flightDistance : public Function {

      constexpr auto name() const { return "ParticleParams::flightDistance"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.flightDistance(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert( detail::always_false<Data...>::value,
                       "The fucntor Functors::LHCbMath::ParticleParams::flightDistance can only apply to "
                       "Gaudi::Math::ParticleParams" );
      }
    };

    /**
     * @brief ctau, access the c*tau of Gaudi::Math::ParticleParams
     */
    struct ctau : public Function {

      constexpr auto name() const { return "ParticleParams::ctau"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.ctau(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ParticleParams::ctau can only apply to Gaudi::Math::ParticleParams" );
      }
    };

    /**
     * @brief lenPosCov, access the "Matrix" with correlation errors between position and decay length from
     * Gaudi::Math::ParticleParams
     */
    struct lenPosCov : public Function {

      constexpr auto name() const { return "ParticleParams::lenPosCov"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.lenPosCov(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ParticleParams::lenPosCov can only apply to Gaudi::Math::ParticleParams" );
      }
    };

    /**
     * @brief lenMomCov, access the "Matrix" with correlation errors between momentum and decay length from
     * Gaudi::Math::ParticleParams
     */
    struct lenMomCov : public Function {

      constexpr auto name() const { return "ParticleParams::lenMomCov"; }

      auto operator()( const Gaudi::Math::ParticleParams& input ) const { return input.lenMomCov(); }

      template <typename T>
      auto operator()( const T& p ) const -> decltype( Functors::Optional{( *this )( *p )} ) {
        return p ? Functors::Optional{( *this )( *p )} : std::nullopt;
      }

      template <typename... Data>
      auto operator()( Data... ) const {
        static_assert(
            detail::always_false<Data...>::value,
            "The fucntor Functors::LHCbMath::ParticleParams::lenMomCov can only apply to Gaudi::Math::ParticleParams" );
      }
    };
  } // namespace ParticleParams

} // namespace Functors::LHCbMath
