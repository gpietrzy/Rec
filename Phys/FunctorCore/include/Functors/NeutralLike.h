/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/ProtoParticle.h"
#include "Functors/Function.h"
#include "Functors/PID.h"

namespace Functors::detail {

  template <typename T>
  constexpr bool ai_always_false = false;

} // namespace Functors::detail

namespace Functors::Neutral {

  /** @brief The below functors use the information stored in NeutralPID objects
   */
  template <typename T, T ( LHCb::NeutralPID::*fun )() const>
  struct NeutralPID_Accessor_t : Function {

    Functors::Optional<T> operator()( LHCb::NeutralPID const& pid ) const { return ( pid.*fun )(); }

    Functors::Optional<T> operator()( LHCb::ProtoParticle const& pp ) const {
      auto pid = pp.neutralPID();
      return pid ? ( *this )( *pid ) : std::nullopt;
    }

    Functors::Optional<T> operator()( LHCb::Particle const& p ) const {
      return p.proto() ? ( *this )( *p.proto() ) : std::nullopt;
    }

    Functors::Optional<T> operator()( LHCb::Particle const* p ) const { return ( *this )( *p ); }
  };

  using IsPhoton_t         = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::IsPhoton>;
  using IsNotH_t           = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::IsNotH>;
  using ShowerShape_t      = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::ShowerShape>;
  using NeutralE19_t       = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::CaloNeutralE19>;
  using NeutralE49_t       = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::CaloNeutralE49>;
  using Saturation_t       = NeutralPID_Accessor_t<int, &LHCb::NeutralPID::Saturation>;
  using NeutralHcal2Ecal_t = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::CaloNeutralHcal2Ecal>;
  using NeutralEcal_t      = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::CaloNeutralEcal>;
  using ClusterMass_t      = NeutralPID_Accessor_t<float, &LHCb::NeutralPID::ClusterMass>;

  using NeutralID_t =
      Functors::PID::details::CellIDFunc<&LHCb::ProtoParticle::neutralPID, &LHCb::NeutralPID::CaloNeutralID>;

} // namespace Functors::Neutral
