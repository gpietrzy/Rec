/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/System.h"
#include <functional>
#include <optional>
#include <ostream>

namespace Functors {

  /**
   * @brief Functors::Optional - wrapper around std::optional
   *
   * Class that represents and optional output of a Functor
   * Similar to std::optional but differs in the way the comparison operators
   * work. The main reasons why this class exists and we cant use std::optional:
   * - A normal empty std::optional always compares less than. So empty
   *   optional < 3mm is true -> Bad for IP cuts etc.
   * - The comparison operators always return bool -> can't work with the
   *   SIMDWRAPPER types that return a mask_v
   */
  template <typename T>
  struct Optional {
    using value_type = T;

    Optional() = default;
    Optional( std::nullopt_t /*unused*/ ){};
    template <typename U = T,
              typename = std::enable_if_t<!std::is_same_v<Optional, std::decay_t<U>> && std::is_constructible_v<T, U>>>
    Optional( U&& val ) : m_value( std::forward<U>( val ) ) {}

    /**
     * @brief check if the Optional contains a value
     *
     * @return bool: true if Optional contains value, else false
     */
    [[nodiscard]] bool has_value() const { return m_value.has_value(); }
    /**
     * @brief const access to contained object
     *
     * @return T const&: contained object
     */
    [[nodiscard]] T const& value() const& { return m_value.value(); }
    /**
     * @brief access contained object
     *
     * @return T&: contained object
     */
    [[nodiscard]] T& value() & { return m_value.value(); }
    /**
     * @brief moveable access to contained object
     *
     * @return T&&: contained object
     */
    [[nodiscard]] T&& value() && { return std::move( m_value ).value(); }
    /**
     * @brief get contained value or default value
     *
     * @tparam U type of default value
     * @param default_value to be returned if Optional is empty
     * @return contained value if has_value() or default_value
     */
    template <typename U, typename = std::enable_if_t<std::is_constructible_v<T, U>>>
    [[nodiscard]] T value_or( U&& default_value ) const {
      return has_value() ? value() : static_cast<T>( std::forward<U>( default_value ) );
    }

    // ostream overload for easy printing
    friend std::ostream& operator<<( std::ostream& os, const Optional<T>& opt ) {
      os << "Optional<" << System::typeinfoName( typeid( T ) ) << ">{";
      if ( opt.has_value() ) {
        os << opt.value();
      } else {
        os << "std::nullopt";
      }
      return os << "}";
    }

  private:
    std::optional<T> m_value{};
  };

  // Deduction guide for the constructor
  template <typename T>
  Optional( T )->Optional<T>;

// Macro to generate the overload set. Comparison always returns false if the
// optional is unengaged and otherwise returns the comparison of the contained
// object.
#define MAKE_OVERLOAD( op )                                                                                            \
  template <typename T, typename U>                                                                                    \
  constexpr auto operator op( Optional<T> const& opt1, Optional<U> const& opt2 )                                       \
      ->decltype( std::declval<T const&>() op std::declval<U const&>() ) {                                             \
    if ( !opt1.has_value() || !opt2.has_value() ) { return false; }                                                    \
    return opt1.value() op opt2.value();                                                                               \
  }                                                                                                                    \
  template <typename T, typename U>                                                                                    \
  constexpr auto operator op( Optional<T> const& opt, U const& value )                                                 \
      ->decltype( std::declval<T const&>() op std::declval<U const&>() ) {                                             \
    if ( !opt.has_value() ) { return false; }                                                                          \
    return opt.value() op value;                                                                                       \
  }                                                                                                                    \
  template <typename T, typename U>                                                                                    \
  constexpr auto operator op( U const& value, Optional<T> const& opt )                                                 \
      ->decltype( std::declval<U const&>() op std::declval<T const&>() ) {                                             \
    if ( !opt.has_value() ) { return false; }                                                                          \
    return value op opt.value();                                                                                       \
  }

  MAKE_OVERLOAD( &&)
  MAKE_OVERLOAD( || )
  MAKE_OVERLOAD( == )
  MAKE_OVERLOAD( != )
  MAKE_OVERLOAD( < )
  MAKE_OVERLOAD( <= )
  MAKE_OVERLOAD( > )
  MAKE_OVERLOAD( >= )

#undef MAKE_OVERLOAD

  namespace detail {
    template <typename T>
    struct is_our_optional_t : std::false_type {};

    template <typename T>
    struct is_our_optional_t<Functors::Optional<T>> : std::true_type {};

    template <typename T>
    constexpr bool is_our_optional_v = is_our_optional_t<T>::value;
  } // namespace detail

  namespace detail {
    template <typename T>
    constexpr bool is_optional_v = false;
    template <typename T>
    constexpr bool is_optional_v<std::optional<T>> = true;
  } // namespace detail

  template <typename Opt, typename Fn>
  auto and_then( Opt&& opt, Fn&& fn ) {
    using R = decltype( std::invoke( fn, *std::forward<Opt>( opt ) ) );
    if constexpr ( std::is_pointer_v<R> ) {
      return opt ? std::invoke( fn, *std::forward<Opt>( opt ) ) : nullptr;
    } else if constexpr ( std::is_lvalue_reference_v<R> ) {
      return opt ? &std::invoke( fn, *std::forward<Opt>( opt ) ) : nullptr;
    } else if constexpr ( detail::is_optional_v<R> ) {
      auto r = ( opt ? std::invoke( fn, *std::forward<Opt>( opt ) ) : std::nullopt );
      return r ? Optional{*std::move( r )} : std::nullopt;
    } else if constexpr ( detail::is_our_optional_v<R> ) {
      return opt ? std::invoke( fn, *std::forward<Opt>( opt ) ) : std::nullopt;
    } else {
      return opt ? Optional{std::invoke( fn, *std::forward<Opt>( opt ) )} : std::nullopt;
    }
  }

  template <typename Opt, typename Fn, typename... Fns>
  auto and_then( Opt&& opt, Fn&& fn, Fns&&... fns ) {
    return and_then( and_then( std::forward<Opt>( opt ), std::forward<Fn>( fn ) ), std::forward<Fns>( fns )... );
  }
} // namespace Functors
