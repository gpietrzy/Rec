/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Functors/Core.h"
#include "Functors/FunctorDesc.h"
#include "Functors/IFactory.h"
#include "Functors/with_functors.h"
#include "Gaudi/Property.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/STLExtensions.h"

#include <tuple>
#include <type_traits>
#include <utility>

/** @brief Add maps of functors to an algorithm.
 */
template <typename base_t, typename... Tags>
class with_functor_maps : public Functors::detail::with_functor_factory<base_t> {
  using functor_types = boost::mp11::mp_list<Functors::Functor<typename Tags::Signature>...>;
  using FuncMapTuple  = std::tuple<std::map<std::string, Functors::Functor<typename Tags::Signature>>...>;
  using FuncProxyMap  = std::map<std::string, ThOr::FunctorDesc>;

  template <std::size_t... Is>
  void initialize( std::index_sequence<Is...> ) {
    ( decode<Is>(), ... );
  }

  template <std::size_t Idx>
  void decode() {
    // This is the {nickname: decoded_functor} map we want to populate
    auto& functor_map = std::get<Idx>( m_functors );
    // Clean it up each time in case we re-decode
    functor_map.clear();
    for ( auto const& [func_name, func_desc] : m_properties.template get<Idx>() ) {
      this->getFunctorFactory().register_functor( this, functor_map[func_name], func_desc );
    }
  }

  // Storage for the decoded functors
  FuncMapTuple m_functors;

  // Storage for the Gaudi::Property objects; helper handles duplicate names
  friend struct Functors::detail::property_holder<FuncProxyMap, Tags...>;
  Functors::detail::property_holder<FuncProxyMap, Tags...> m_properties{this, {}};

public:
  // Expose the base class constructors
  using Functors::detail::with_functor_factory<base_t>::with_functor_factory;

  // Expose the list of tag types for convenience
  using functor_map_tag_types = std::tuple<Tags...>;

  StatusCode initialize() override {
    auto sc = Functors::detail::with_functor_factory<base_t>::initialize();
    initialize( std::index_sequence_for<Tags...>{} );
    return sc;
  }

  /** @brief Get the decoded functor map corresponding to the given tag type.
   *  @todo  Extend this to despatch to getFunctor() of the parent class if it
   *         exists and we didn't know the given TagType.
   */
  template <typename Tag>
  auto const& getFunctorMap() const {
    return std::get<boost::mp11::mp_find<functor_map_tag_types, Tag>::value>( m_functors );
  }
};

/** Extra layer on top of with_functor_maps that removes duplicate
 *  entries in the pack of tag types.
 */
template <typename base_t, typename... Tags>
using with_functor_maps_deduplicated =
    boost::mp11::mp_apply<with_functor_maps, boost::mp11::mp_unique<boost::mp11::mp_list<base_t, Tags...>>>;
