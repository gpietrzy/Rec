###############################################################################
# (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Pure-python classes used as building blocks of functor expressions."""
import logging
from inspect import cleandoc
from textwrap import indent
from array import array

from Functors.common import top_level_namespace
from GaudiKernel.Configurable import Configurable
from PyConf.dataflow import DataHandle

log = logging.getLogger(__name__)


def bind_if_needed(functor):
    """Return a BoundFunctor constructed from the argument.

    If the argument is an arithmetic constant, do this by hand. Otherwise let
    the argument try and bind itself. This will result in an error if you try
    and pass an unbound functor that needs arguments.
    """
    if type(functor) == str:
        return functor
    if type(functor) in [int, float]:
        code = python_to_cpp_str(functor)
        return BoundFunctor(code=code, nice_code=str(functor))
    else:
        return functor()


def make_sphinx_link_name(functor):
    """Given a functor, create a sphinx link name in the form of :py:func:~Functors.{functor.name()} """
    if isinstance(functor, ComposedBoundFunctor):
        doc = functor._nicedoc
    else:
        doc = f":py:func:`~Functors.{functor.name()}`"
    return doc


def python_to_cpp_str(obj, in_container=False, return_type=False):
    # Reduce complex Python types down to simple Python representations
    if isinstance(obj, DataHandle):
        obj = obj.location

    # Convert simple Python types to C++ representations
    ret_val = None
    if type(obj) == int:
        if in_container:
            ret_val = 'int{{{0}}}'.format(obj)
            if return_type: return (ret_val, 'int')
        else:
            ret_val = 'std::integral_constant<int, {}>{{}}'.format(obj)
            #remove the last curly brace to get type
            if return_type: return (ret_val, ret_val[:-2])
        return ret_val
    elif type(obj) == bool:
        if in_container:
            ret_val = 'bool{{{0}}}'.format('true' if obj else 'false')
            if return_type: return (ret_val, 'bool')
        else:
            ret_val = 'std::bool_constant<{}>{{}}'.format(
                'true' if obj else 'false')
            #remove the last curly brace to get type
            if return_type: return (ret_val, ret_val[:-2])
        return ret_val
    elif type(obj) == float:
        # check if it is Not a Number
        if obj != obj:
            ret_val = 'std::numeric_limits<float>::quiet_NaN()'
            if return_type: return (ret_val, 'float')
            return ret_val
        # map python float onto C++ float, not double
        ret_val = repr(obj) + 'f'
        if return_type: return (ret_val, 'float')
        return ret_val
    elif type(obj) == str:
        # map python strings explicitly to std::string
        ret_val = 'std::string{"' + obj.replace('"', '\\"') + '"}'
        if return_type: return (ret_val, 'std::string')
        return ret_val
    elif type(obj) == dict:
        items = []
        #get the type of the keys and values
        _, key_type = python_to_cpp_str(
            list(obj.keys())[0], in_container=True, return_type=True)
        _, val_type = python_to_cpp_str(
            list(obj.values())[0], in_container=True, return_type=True)
        #make a container type
        cpp_container_type = f'std::map<{key_type}, {val_type}>'
        for key, val in list(obj.items()):
            key_code, ki_type = python_to_cpp_str(
                key, in_container=True, return_type=True)
            val_code, vi_type = python_to_cpp_str(
                val, in_container=True, return_type=True)
            items.append('{ ' + key_code + ', ' + val_code + ' }')
            if ki_type != key_type:
                raise TypeError(
                    "The keys of the dictionary must be of the same type. One is"
                    " of type {0}, whereas the other is of type {1}. Please check!"
                    .format(key_type, ki_type))

            if vi_type != val_type:
                raise TypeError(
                    "The values of the dictionary must be of the same type. One is"
                    " of type {0}, whereas the other is of type {1}. Please check!"
                    .format(val_type, vi_type))

        ret_val = cpp_container_type + '{' + ', '.join(items) + '}'
        if return_type: return (ret_val, cpp_container_type)
        return ret_val
    elif (type(obj) == list or type(obj) == tuple):
        items = [python_to_cpp_str(item, in_container=True) for item in obj]
        items_ret_type = [
            python_to_cpp_str(item, in_container=True, return_type=True)[1]
            for item in obj
        ]
        # if all elements are string type then use std::vector, if not std::tuple
        contains_same_type = all(item_ret_type == items_ret_type[0]
                                 for item_ret_type in items_ret_type)
        is_list = isinstance(obj, list)
        cpp_container_type = f'std::vector<{items_ret_type[0]}>' if (
            is_list and contains_same_type
        ) else 'std::tuple<' + ', '.join(items_ret_type) + '>'
        ret_val = cpp_container_type + '{' + ', '.join(items) + '}'
        if return_type: return (ret_val, cpp_container_type)
        return ret_val
    elif isinstance(obj, array):
        cpp_types = {
            'b': 'signed char',
            'B': 'unsigned char',
            'u': 'wchart_t',
            'h': 'signed short',
            'H': 'unsigned short',
            'i': 'signed int',
            'I': 'unsigned int',
            'l': 'signed long',
            'L': 'unsigned long',
            'q': 'signed long long',
            'Q': 'unsigned long long',
            'f': 'float',
            'd': 'double'
        }
        if obj.typecode not in cpp_types.keys():
            raise TypeError(f'Invalid array typecode: {obj.typecode}')
        vector_content = ', '.join([str(value) for value in obj.tolist()])
        return f'std::vector<{cpp_types[obj.typecode]}>{{ {vector_content} }}'
    elif isinstance(obj, BoundFunctor):
        ret_val = obj.code()
        #remove the last curly brace for the type
        if return_type: return (ret_val, ret_val[:-2])
        return ret_val
    else:
        # one last thing to try: if the argument is an unbound functor, but it
        # doesn't take any arguments, then we can convert it to a bound functor
        try:
            bound = obj()
            if isinstance(bound, BoundFunctor):
                ret_val = bound.code()
                #remove the last curly brace for the type
                if return_type: return (ret_val, ret_val[:-2])
                return ret_val
        except:
            pass
        raise TypeError("Unsupported type: " + str(type(obj)))


class FunctorBase(object):
    """Base class for all Python-side functors.

    This implements the various operator overloads.
    """

    def __call__(self):
        """Dummy implementation to keep pylint happy"""
        raise Exception("FunctorBase.__call__ should not be called!")

    def __invert__(self):
        """~FUNCTOR, negation"""
        return InvertedBoundFunctor(self())

    def __and__(self, other):
        """F1 & F2, logical and"""

        other = other()

        # flatten if functors are themselves already `&` of basic functors
        funcs = self.funcs if (isinstance(self, ComposedBoundFunctor)
                               and self.operator == '&') else (self, )
        funcs += other.funcs if (isinstance(other, ComposedBoundFunctor)
                                 and other.operator == '&') else (other, )

        return ComposedBoundFunctor('&', *funcs)

    def __or__(self, other):
        """F1 | F2, logical or"""
        # bind_if_needed not used because we don't need to support applying the
        # operator to a functor and an arithmetic constant.
        return ComposedBoundFunctor('|', self(), other())

    def __lt__(self, other):
        """F1 < F2"""
        return ComposedBoundFunctor('<', self(), bind_if_needed(other))

    def __gt__(self, other):
        """F1 > F2"""
        return ComposedBoundFunctor('>', self(), bind_if_needed(other))

    def __le__(self, other):
        """F1 <= F2"""
        return ComposedBoundFunctor('<=', self(), bind_if_needed(other))

    def __ge__(self, other):
        """F1 >= F2"""
        return ComposedBoundFunctor('>=', self(), bind_if_needed(other))

    def __eq__(self, other):
        """F1 == F2"""
        if type(other) == type(Configurable.propertyNoValue
                               ) and other == Configurable.propertyNoValue:
            return False
        else:
            return ComposedBoundFunctor('==', self(), bind_if_needed(other))

    def __ne__(self, other):
        """F1 != F2"""
        return ComposedBoundFunctor('!=', self(), bind_if_needed(other))

    def __add__(self, other):
        """F1 + F2"""
        return ComposedBoundFunctor('+', self(), bind_if_needed(other))

    def __radd__(self, other):
        """F1 + F2"""
        return ComposedBoundFunctor('+', bind_if_needed(other), self())

    def __sub__(self, other):
        """F1 - F2"""
        return ComposedBoundFunctor('-', self(), bind_if_needed(other))

    def __rsub__(self, other):
        """F1 - F2"""
        return ComposedBoundFunctor('-', bind_if_needed(other), self())

    def __mul__(self, other):
        """F1 * F2"""
        return ComposedBoundFunctor('*', self(), bind_if_needed(other))

    def __rmul__(self, other):
        """F1 * F2"""
        return ComposedBoundFunctor('*', bind_if_needed(other), self())

    def __div__(self, other):
        """F1 / F2, python 2"""
        return ComposedBoundFunctor('/', self(), bind_if_needed(other))

    def __rdiv__(self, other):
        """F1 / F2, python 2"""
        return ComposedBoundFunctor('/', bind_if_needed(other), self())

    def __truediv__(self, other):
        """F1 / F2, python 3"""
        return ComposedBoundFunctor('/', self(), bind_if_needed(other))

    def __rtruediv__(self, other):
        """F1 / F2, python 3"""
        return ComposedBoundFunctor('/', bind_if_needed(other), self())

    def __pow__(self, other):
        """F1 ** F2"""
        return WrappedBoundFunctor(top_level_namespace + 'math::pow', self,
                                   other)

    def __rpow__(self, other):
        """F1 ** F2"""
        return WrappedBoundFunctor(top_level_namespace + 'math::pow', other,
                                   self)

    def __nonzero__(self):
        """Boolean conversion, python 2"""
        raise Exception(
            "FunctorBase.__nonzero__ should not be called! This likely means that you used `and`, `or` or `not` when you should have used `&`, `|` or `~`."
        )

    def __bool__(self):
        """Boolean conversion, python 3"""
        raise Exception(
            "FunctorBase.__bool__ should not be called! This likely means that you used `and`, `or` or `not` when you should have used `&`, `|` or `~`."
        )

    def __matmul__(self, other):
        """F1 @ F2"""

        other = other()

        # flatten if functors are themselves already chained
        funcs = self.funcs if (isinstance(self, ComposedBoundFunctor)
                               and self.operator == '@') else (self, )
        funcs += other.funcs if (isinstance(other, ComposedBoundFunctor)
                                 and other.operator == '@') else (other, )

        return ComposedBoundFunctor('@', *funcs)

    def bind(self, *args):
        """Bind other functors as input arguments to self

        D = A.bind(B, C) would result in a the following call:

        D(input) == A( B(input), C(input) )

        """
        if len(args) == 0:
            raise Exception("bind call requires at least one argument")

        bound_args = [bind_if_needed(arg) for arg in args]
        return ComposedBoundFunctor('bind', self(), *bound_args)


# type() hack to ensure that the BoundFunctor is parsed correctly
# see https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/1721#note_2881432
class BoundFunctor(FunctorBase, type('Configurable', (), {})):
    """A functor that has had all its parameters fully specified."""

    def __init__(self,
                 code=None,
                 nice_code=None,
                 inputs=None,
                 nice_name=None,
                 docs=""):
        self._strrep = nice_code  # RUNNUMBER(ODIN=...)
        self._strname = nice_name  # RUNNUMBER
        self._code = code
        # Unique DataHandle objects held by this functor
        self._inputs = sorted(set(inputs or []), key=hash)
        # Assign an instance-specific docstring, allowing bound functors to
        # forward the docstring of the base functor. Note that this will *not*
        # change the docstring displayed with ``help`` as that shows the class
        # docstring
        if docs:
            self.__doc__ = f"{cleandoc(docs)}"

    def __getstate__(self):
        """Return serialisation state for pickling.

        Note:
            The pickled state **discards explicit DataHandle data
            dependencies**. This is because these can reference to a
            PyConf.Algorithm object, and PyConf make no guarantees about
            their pickle-ability.

            The implicit data dependency is retained, as encoded in the TES
            location of the DataHandle in the string encoding of the functor.
        """
        return (self._strrep, self._strname, self._code, [])

    def __setstate__(self, state):
        """Load serialisation state from pickled representation."""
        self._strrep, self._strname, self._code, self._inputs = state

    def code(self):
        return self._code

    def code_repr(self):
        return self._strrep

    def name(self):
        return self._strname

    def data_dependencies(self):
        """Return a list of DataHandle inputs to this functor.

        This implements the API which allows PyConf to deduce that a functor
        property must 'inject' dependencies into its owning Algorithm.
        """
        return self._inputs

    def __str__(self):
        return str((self.code(), self.code_repr()))

    def __repr__(self):
        return str(self)

    def to_json(self):
        return str(self)

    def __call__(self):
        """Return ourself, for compatibility with a 0-argument Functor.

        The function call operator is used to bake in parameter values. This
        has already been done for a BoundFunctor, so this is trivial.
        """
        return self


class ComposedBoundFunctor(BoundFunctor):
    """A (bound) functor made from other (bound) functors.

    This is the return type of the various binary operators.
    """

    def __init__(self, operator, *funcs):

        if not all([isinstance(func, BoundFunctor) for func in funcs]):
            raise Exception(
                "ComposedBoundFunctor constructor called with input that is not a functor"
            )

        self.operator = operator
        self.funcs = funcs
        self._doc = None

        if operator == 'bind':
            code = f'Functors::bind( {", ".join([f.code() for f in funcs]) } )'
            nice_code = f'{funcs[0].code_repr()}.bind( {", ".join([f.code_repr() for f in funcs[1:]]) } )'
        else:
            if operator == '@':
                op = 'Functors::chain'
            elif operator == '&':
                op = 'Functors::all_of'
            else:
                op = f'operator{operator}'
            code = f'{op}( {", ".join([f.code() for f in funcs]) } )'
            nice_code = f'( {f" {operator} ".join([f.code_repr() for f in funcs]) } )'

        BoundFunctor.__init__(
            self,
            code=code,
            nice_code=nice_code,
            inputs=[d for f in funcs for d in f.data_dependencies()])

    @property
    def _nicedoc(self):
        if self.operator == 'bind':
            tmp = ",\n"
            indent_doc = indent(
                make_sphinx_link_name(self.funcs[0]) + ".bind(\n" + tmp.join(
                    [make_sphinx_link_name(f)
                     for f in self.funcs[1:]]) + "\n)", '  ')
        else:
            tmp = f'\n{self.operator}\n'
            indent_doc = indent(
                tmp.join([make_sphinx_link_name(f) for f in self.funcs]), '  ')
        return f"(\n{indent_doc}\n)"

    @property
    def __doc__(self):
        if self._doc is not None:
            return self._doc
        return ("ComposedBoundFunctor:\n\n.. parsed-literal::\n" + indent(
            self._nicedoc, '  '))

    @__doc__.setter
    def __doc__(self, value):
        if not isinstance(value, str): raise TypeError("Not a string!")
        self._doc = indent(value, '  ')


class InvertedBoundFunctor(BoundFunctor):
    """The logical negation of another (bound) functor.

    This is the return type of the unary negation operator.
    """

    def __init__(self, bound_functor):
        assert isinstance(bound_functor, BoundFunctor)
        BoundFunctor.__init__(
            self,
            code='~( {f1} )'.format(f1=bound_functor.code()),
            nice_code='~{f1}'.format(f1=bound_functor.code_repr()),
            inputs=bound_functor.data_dependencies())


class WrappedBoundFunctor(BoundFunctor):
    """A (bound) functor that's fed through a transforming function.

    This is used to represent a transformation such as math::log(PT)
    or math::pow( PT, 2 ).
    """

    def __init__(self, wrapping_function, *arguments):
        bound_functors = [bind_if_needed(arg) for arg in arguments]
        inputs = []
        for bound_functor in bound_functors:
            inputs += bound_functor.data_dependencies()
        code_arguments = ', '.join(
            [bound_functor.code() for bound_functor in bound_functors])
        code = '{wrap}( {args} )'.format(
            wrap=wrapping_function, args=code_arguments)
        nice_code_arguments = ', '.join(
            [bound_functor.code_repr() for bound_functor in bound_functors])
        nice_code = '{wrap}( {args} )'.format(
            wrap=wrapping_function, args=nice_code_arguments)

        BoundFunctor.__init__(
            self, code=code, nice_code=nice_code, inputs=inputs)


def Functor(representation: str,
            cppname,
            brief_docs,
            Params=[],
            TemplateParams=[],
            AllowMultiplePositionalArguments=False):
    """Create a new Functor class and return an instance of it.

    This takes care of generating a new class inheriting from FunctorBase and
    adding some appropriate methods and doc-strings to it.
    """

    if not representation.isupper():
        raise Exception(
            f"Please follow the convention to use all caps for functor names: {representation} -> {representation.upper()}"
        )

    def call_without_args(self):
        """Trivial conversion to a BoundFunctor"""
        return BoundFunctor(
            code=self.code(),
            nice_code=representation,
            nice_name=representation,
            docs=brief_docs)

    def call_with_args(self, *args, **kwargs):
        """Convert this Functor object to a BoundFunctor, using the given
        parameter values. To encourage explicit code, all parameters must
        be specified by name, unless there is only one parameter in which
        case it can be passed as a positional argument. Passing additional
        named arguments also causes an error."""

        if len(args) and len(kwargs):
            raise Exception(
                "You may not mix keyword and positional arguments to functors."
            )
        elif len(args):
            # We only got positional arguments
            if not AllowMultiplePositionalArguments and len(args) > 1:
                raise Exception(
                    "Functors that take more >1 argument must have those arguments specified by keyword."
                )
            if len(args) != len(self._arguments):
                raise Exception(
                    "You passed {} positional arguments to a functor that needs {}"
                    .format(len(args), len(self._arguments)))
            # Dump them into `kwargs` following the order of self._arguments
            for (kwargname, _, _), value in zip(self._arguments, args):
                kwargs[kwargname] = value

        cpp_arguments = []
        repr_arguments = []
        # Input DataHandle dependencies
        inputs = []
        for argtuple in self._arguments:
            argname, argdocs, argtype = argtuple[:3]
            formatter = argtuple[3] if len(argtuple) > 3 else None
            if argname in kwargs:
                provided_value = kwargs.pop(argname)
                value_repr = provided_value
                doc_comment = '/* {comment} */ '.format(
                    comment=argdocs.replace('*', '\\*'))
                if argtype == BoundFunctor:
                    # We want a BoundFunctor. We can try to get one by binding
                    # with no arguments (a no-op if the object was already a
                    # BoundFunctor). This will work if we were given an unbound
                    # functor that doesn't expect arguments (e.g. PT)...
                    provided_value = provided_value()
                    value_repr = provided_value.code_repr()
                    # Propagate dependencies from the property to self
                    inputs += provided_value.data_dependencies()
                if isinstance(provided_value, argtype):
                    # all good
                    repr_arguments.append((argname, value_repr))
                    if formatter is not None:
                        code, value_inputs = formatter(provided_value)
                        cpp_arguments.append(doc_comment + code)
                        inputs += value_inputs
                        # TODO also format the repr
                    else:
                        try:
                            code = python_to_cpp_str(provided_value)
                            cpp_arguments.append(doc_comment + code)
                        except:
                            raise TypeError(
                                "Not sure how to express argument '{arg}' of type {type}"
                                .format(
                                    arg=provided_value,
                                    type=type(provided_value)))
                    if isinstance(provided_value, DataHandle):
                        inputs.append(provided_value)
                    elif isinstance(provided_value, list):
                        for dh in provided_value:
                            if isinstance(dh, DataHandle):
                                inputs.append(dh)

                else:
                    raise Exception(
                        "Incompatible type {provided} given, expected {expected}"
                        .format(
                            provided=str(type(provided_value)),
                            expected=str(argtype)))
            else:
                raise Exception(
                    "No value provided for compulsory argument {argname} to {name}"
                    .format(argname=argname, name=representation))

        if len(self._template_arguments):
            targs = []
            # Now handle the template arguments
            for n, argtuple in enumerate(self._template_arguments):
                targ, tdoc = argtuple[:2]
                formatter = argtuple[2] if len(argtuple) > 2 else None
                if targ in kwargs:
                    # The template parameter was actually specified
                    provided_value = kwargs.pop(targ)
                    repr_arguments.append((targ, provided_value))
                    if formatter:
                        code = formatter(provided_value)
                    else:
                        # no formatter was specified
                        # assert the value is a string, take it as a template argument
                        assert type(provided_value) == str
                        code = provided_value
                    targs.append(code)
                else:
                    # The parameter was not specified. This is fine as long as
                    # the rest of the parameters are also unspecified.
                    assert not any([
                        targ in kwargs
                        for targ in self._template_arguments[n + 1:]
                    ])
                    break
            template_arguments = '<' + ', '.join(targs) + '>'
        else:
            template_arguments = ''
        if len(kwargs):
            raise Exception("Unknown parameters were provided: " +
                            repr(kwargs))
        cpp_str = '{name}{template_params}( {args} )'.format(
            name=self._cppname,
            template_params=template_arguments,
            args=', '.join(cpp_arguments))
        repr_str = '{name}({args})'.format(
            name=representation,
            args=', '.join(["{}={}".format(n, v) for n, v in repr_arguments]))
        return BoundFunctor(
            code=cpp_str,
            nice_code=repr_str,
            inputs=inputs,
            nice_name=representation,
            docs=brief_docs)

    call_doc_lines = ["Keyword arguments:"]
    for argtuple in Params:
        argname, argdocs, argtype = argtuple[:3]
        if type(argtype) == tuple:
            doc_types = "Any of: " + ", ".join([t.__name__ for t in argtype])
        else:
            doc_types = argtype.__name__
        call_doc_lines.append(' -- '.join([argname, argdocs, doc_types]))
    call_with_args.__doc__ = '\n'.join(call_doc_lines)

    def code_without_args(self):
        """This is needed to handle the case that the entire functor is
        something like "PT" -- i.e. an unbound functor that does not take any
        parameters. If we did not implement this method, this functor would
        have to be written as PT(), which would be inconsistent.
        """
        return self._cppname + '{}'

    def code_with_args(self):
        """We cannot produce C++ directly from an unbound functor with arguments.
        This is a dummy method that exists only to throw an informative error.
        """
        raise Exception(
            "This functor expects arguments, so you must provide them. Try: FUNCTOR -> FUNCTOR(a=b, ...)"
        )

    has_args = (len(Params) > 0 or len(TemplateParams) > 0)
    cdict = {
        '__doc__': brief_docs,
        '__call__': call_with_args if has_args else call_without_args,
        '_arguments': Params,
        '_template_arguments': TemplateParams,
        '_cppname': top_level_namespace + cppname,
        'code': code_with_args if has_args else code_without_args,
    }
    new_type_name = cppname.replace('::', '__')
    t = type(new_type_name, (FunctorBase, ), cdict)()
    return t if has_args else t()
