#!/usr/bin/env python
###############################################################################
# (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import print_function
from __future__ import division
from past.utils import old_div
import pickle
from array import array
from Functors import PT, ISMUON, MINIPCUT, VALUE_OR
from Functors import math as fmath
from Functors.tests.categories import dummy_data_pv_container as pvs_dh
from GaudiKernel.SystemOfUnits import *
from PyConf.Algorithms import Gaudi__Examples__IntDataProducer


def not_empty(f):
    assert len(f.code()) > 0


def check(f):
    not_empty(f)
    print(f.code())


def test_basic_grammar():
    ipcut = MINIPCUT(IPCut=1.0, Vertices=pvs_dh)
    f1 = ISMUON | ipcut
    f2 = ISMUON & ipcut
    f3 = ipcut | ISMUON
    f4 = ipcut & ISMUON
    f5 = ~ISMUON
    f6 = ~ipcut
    check(f2 | ~ISMUON)
    check(~ISMUON | f2)
    not_empty(f1)
    not_empty(f2)
    not_empty(f3)
    not_empty(f4)
    not_empty(f5)
    not_empty(f6)


def test_array_input():
    check(VALUE_OR(array('b', [1, 2, 3])))  # vector< signed char >
    check(VALUE_OR(array('B', [1, 2, 3])))  # vector< unsigned char >
    check(VALUE_OR(array('u', ['a', 'b'])))  # vector< wchart_t >
    check(VALUE_OR(array('h', [1, 2, 3])))  # vector< signed short >
    check(VALUE_OR(array('H', [1, 2, 3])))  # vector< unsigned short >
    check(VALUE_OR(array('i', [1, 2, 3])))  # vector< signed int >
    check(VALUE_OR(array('I', [1, 2, 3])))  # vector< unsigned int >
    check(VALUE_OR(array('l', [1, 2, 3])))  # vector< signed long >
    check(VALUE_OR(array('L', [1, 2, 3])))  # vector< unsigned long >
    check(VALUE_OR(array('q', [1, 2, 3])))  # vector< signed long long >
    check(VALUE_OR(array('Q', [1, 2, 3])))  # vector< unsigned long long >
    check(VALUE_OR(array('f', [1, 2, 3])))  # vector< float >
    check(VALUE_OR(array('d', [1, 2, 3])))  # vector< double >


def test_arithmetric():
    check(30 * GeV < PT)
    check(PT > 30 * GeV)
    check(PT >= 30 * GeV)
    check(PT <= 30 * GeV)
    check(PT < 30 * GeV)
    check(PT == 30 * GeV)
    check(PT != 30 * GeV)
    check(PT + ISMUON > 15 * GeV)


def test_functions():
    check(fmath.log(old_div(PT, GeV)) > 1.0)


def test_disabled_logical_operators():
    check((PT > 10 * GeV) & ISMUON)
    try:
        (PT > 10 * GeV) and ISMUON
    except Exception:
        pass
    else:
        raise Exception("Use of logical `and` did not raise an exception.")
    try:
        (PT > 10 * GeV) or ISMUON
    except Exception:
        pass
    else:
        raise Exception("Use of logical `or` did not raise an exception.")
    try:
        not ISMUON
    except Exception:
        pass
    else:
        raise Exception("Use of logical `not` did not raise an exception.")


def test_serialisation():
    """A BoundFunctor must be seriasable by the pickle module."""
    functor = MINIPCUT(IPCut=1.0, Vertices=pvs_dh)
    # Use the same protocol as in `gaudirun.py -o options.pkl`
    loaded = pickle.loads(pickle.dumps(functor, protocol=-1))
    assert functor.code() == loaded.code()
    assert functor.code_repr() == loaded.code_repr()
    assert functor.name() == loaded.name()
    assert repr(functor) == repr(loaded)
    assert str(functor) == str(loaded)

    # It is expected that the data dependencies are lost during serialisation;
    # see BoundFunctor.__getstate__
    assert len(functor.data_dependencies()) > len(loaded.data_dependencies())
    assert len(loaded.data_dependencies()) == 0
