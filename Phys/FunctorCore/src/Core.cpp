/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/Core.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include <string_view>

/** @file  Core.cpp
 *  @brief Definitions of core functions.
 */

namespace Functors {
  void TopLevelInfo::Warning( std::string_view message, std::size_t count ) const {
    /** @todo Also support efficient MsgCounter use for functors */
    // Note we have to capture 'this' so that algorithm() works.
    m_msg_counters->with_lock( [this, message, count]( MsgMap& counters ) {
      auto iter = counters.find( message );
      if ( iter == counters.end() ) {
        auto [new_iter, success] = counters.emplace(
            std::piecewise_construct, std::forward_as_tuple( message ),
            std::forward_as_tuple( const_cast<Gaudi::Algorithm*>( algorithm() ), std::string{message}, count ) );
        if ( success ) {
          iter = new_iter;
        } else {
          throw GaudiException{"m_msg_counters manipulation failed.", "Functors::TopLevelInfo::Warning",
                               StatusCode::FAILURE};
        }
      }
      ++iter->second;
    } );
  }

  std::string TopLevelInfo::generate_property_name() {
    auto* alg = algorithm();
    if ( !alg ) {
      throw GaudiException{"generate_property_name called with a nullptr algorithm",
                           "Functors::TopLevelInfo::generate_property_name", StatusCode::FAILURE};
    }
    std::string name;
    for ( auto i = 0;; ++i ) {
      name = "DummyFunctorProperty" + std::to_string( i );
      if ( !alg->hasProperty( name ) ) { break; }
    }
    return name;
  }
} // namespace Functors
