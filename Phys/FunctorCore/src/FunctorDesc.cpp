/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Functors/FunctorDesc.h"

namespace std {
  /**
   * @brief operator<< specialization for a Functor Description (FuntorDesc)
   *
   * Output should match the python repr result, e.g for the PT Functor:
   * "('::Functors::Track::TransverseMomentum{}', 'PT')"
   *
   * @param o stream to output into
   * @param f FunctorDesc to stream into o
   * @return ostream& filled with the string representation of f
   */
  std::ostream& operator<<( std::ostream& o, ThOr::FunctorDesc const& f ) {
    o << "\"(" << std::quoted( f.code, '\'' ) << ", " << std::quoted( f.repr, '\'' ) << ")\"";
    return o;
  }
} // namespace std
