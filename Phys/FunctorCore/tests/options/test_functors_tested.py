###############################################################################
# (c) Copyright 2022-2024 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
'''
Test if functors defined in module Functors are tested in TestFunctors.cpp.
In case this test fails, please add a few lines of code in TestFunctors.cpp which use the new functor.
'''
import Functors
import os
import re


def error(msg):
    print("ERROR", msg)


# just add everything which is not tested for now.
exceptions = [
    'Functors::Adapters::ConvertToPOD', 'Functors::Adapters::Maximum',
    'Functors::Adapters::Minimum', 'Functors::Adapters::SubCombination',
    'Functors::Combination::Charge',
    'Functors::Combination::CosAngleBetweenDecayProducts',
    'Functors::Combination::DistanceOfClosestApproach',
    'Functors::Combination::DistanceOfClosestApproachChi2',
    'Functors::Combination::MaxDistanceOfClosestApproach',
    'Functors::Combination::MaxDistanceOfClosestApproachChi2',
    'Functors::Combination::MaxDistanceOfClosestApproachChi2Cut',
    'Functors::Combination::MaxDistanceOfClosestApproachCut',
    'Functors::Combination::MaxSDistanceOfClosestApproach',
    'Functors::Combination::MaxSDistanceOfClosestApproachChi2',
    'Functors::Combination::MaxSDistanceOfClosestApproachChi2Cut',
    'Functors::Combination::MaxSDistanceOfClosestApproachCut',
    'Functors::Combination::SDistanceOfClosestApproach',
    'Functors::Combination::SDistanceOfClosestApproachChi2',
    'Functors::Composite::ComputeDecayLengthSignificance',
    'Functors::Composite::FlightDistanceChi2ToVertex',
    'Functors::Composite::MassWithHypotheses',
    'Functors::Composite::MotherTrajectoryDistanceOfClosestApproachChi2',
    'Functors::Examples::GreaterThan', 'Functors::Examples::ThorBeatsLoki',
    'Functors::MVA', 'Functors::Track::Covariance', 'Functors::Track::PIDe',
    'Functors::Track::PIDk', 'Functors::Track::PIDmu', 'Functors::Track::PIDp',
    'Functors::Track::PIDpi', 'Functors::Track::QoverP',
    'Functors::Track::Flag', 'Functors::Track::nPRVelo3DExpect',
    'Functors::Common::UnitVector', 'Functors::Common::Call',
    'Functors::Common::Dot', 'Functors::Common::NormedDot',
    'Functors::Common::ImpactParameterChi2ToVertex',
    'Functors::Decay::FindDecay', 'Functors::Decay::FindMCDecay',
    'Functors::Column_t', 'Functors::Identity_t',
    'Functors::Particle::ParticlePropertyUser',
    'Functors::PID::RichThresholdDe', 'Functors::PID::RichThresholdKa',
    'Functors::PID::RichThresholdMu', 'Functors::PID::RichThresholdPi',
    'Functors::PID::RichThresholdPr', 'Functors::Detector::DeLHCb',
    'Functors::Detector::FillNumber', 'Functors::Detector::LHCEnergy',
    'Functors::Detector::LHCbClockPhase',
    'Functors::Detector::SMOGInjectionMode',
    'Functors::Detector::SMOGInjectedGas',
    'Functors::Detector::SMOGStableInjection'
]

functor_cpp_names = []
for name, val in Functors.__dict__.items():
    if isinstance(val, Functors.grammar.ComposedBoundFunctor):
        # Do we need to check composed functors?
        # right now this isn't supported
        continue
    if isinstance(val, Functors.grammar.BoundFunctor):
        code = val._code
        # find start of arguments passed to functor
        argument = re.search(r"[\({]", code)
        argument_index = argument.start()
        # the first two characters are the global scope ::
        functor_cpp_names.append(code[2:argument_index])
    elif isinstance(val, Functors.grammar.FunctorBase):
        functor_cpp_names.append(val._cppname[2:])
functor_cpp_names = sorted(functor_cpp_names)

test_file_name = os.path.expandvars(
    '$FUNCTORCOREROOT/tests/src/TestFunctors.cpp')
tested_functors = []
with open(test_file_name) as test_file:
    file_content = test_file.read()
    if ("using namespace Functors" in file_content):
        print(
            "Warning: Using namespaces might break the heuristics of this test. Try not to use them."
        )
    for functor in functor_cpp_names:
        if functor in file_content:
            tested_functors.append(functor)

untested_functors = list(set(functor_cpp_names).difference(tested_functors))
n_untested_functors = len(untested_functors)

print()
print("Before ignoring exceptions")
print(
    f"{n_untested_functors} of {len(functor_cpp_names)} factors are not tested!"
)
print(
    "The following functors are now tested and can be removed from the exceptions: ",
    set(exceptions).difference(untested_functors))
untested_functors = list(set(untested_functors).difference(exceptions))
n_untested_functors = len(untested_functors)
print("After ignoring exceptions")
print(
    f"{n_untested_functors} of {len(functor_cpp_names)} factors are not tested!"
)

if n_untested_functors > 0:
    print(untested_functors)
    error(
        "Above functor(s) not compiled in TestFunctors.cpp. Please add a test."
    )
    exit(1)
