###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import os
import hashlib
from PyConf import Algorithms
from PyConf.application import configure_input, configure
import GaudiKernel.Configurable
from Gaudi.Configuration import DEBUG
from Functors import POD
from Functors.tests.categories import functors_for_class
from Configurables import FunctorFactory
from PyConf.Algorithms import (
    UniqueIDGeneratorAlg,
    ProduceSOATracks,
)
from GaudiConf.LbExec import Options
from PyConf.control_flow import CompositeNode

# Split the testing of functors into N tests, see `should_include` below
SPLIT_I, SPLIT_N = map(int, os.getenv('TEST_FUNCTORS_SPLIT', '1/1').split('/'))

produce = ProduceSOATracks(
    InputUniqueIDGenerator=UniqueIDGeneratorAlg().Output)

# Workaround for gaudi/Gaudi#117 that monkey patches gaudirun.py
GaudiKernel.Configurable.expandvars = lambda x: x

FunctorFactory().OutputLevel = DEBUG


def make_algorithm(type_suffix, eval_or_init, functors, producer=None):
    algo_type_str = 'TestThOrFunctor' + eval_or_init + '__' + type_suffix
    algo_type = getattr(Algorithms, algo_type_str)

    functors = {f.code_repr(): f for f in functors}
    kwargs = {}
    if producer is not None:
        kwargs["Input"] = producer
    kwargs["OutputLevel"] = DEBUG

    return algo_type(
        Functors=functors,
        PODFunctors={k: POD(v)
                     for k, v in functors.items()},
        **kwargs)


# which inputs we can provide at runtime in this test
inputs = {
    'Container': produce.Output,
    'Children': [produce.Output],
}


def should_include(functor):
    """Should the functor be included in this job."""
    x = repr(functor).encode()
    return hashlib.blake2s(x).digest()[0] % SPLIT_N == (SPLIT_I - 1)


def setup_test(type_suffix,
               class_name,
               producer=None,
               exclusions=[],
               cannot_execute=[]):
    # get the functors corresponding to this class
    functors, functors_init_only = functors_for_class(
        class_name,
        inputs,
        exclusions=exclusions,
        cannot_execute=cannot_execute)

    functors = [f for f in functors if should_include(f)]
    functors_init_only = [f for f in functors_init_only if should_include(f)]

    algs = []
    if producer is None:
        # in principle `functors` are executable, but we don't have any input
        # to feed them -- shuffle these functors into `functors_init_only`
        functors_init_only += functors
    elif len(functors):
        # `functors` are executable and we have a producer to feed them with
        algs.append(
            make_algorithm(
                type_suffix, 'Evaluation', functors, producer=producer))
    if len(functors_init_only):
        algs.append(
            make_algorithm(type_suffix, 'Initialisation', functors_init_only))
    return algs


test_algs = []
# somewhat-special case of void functors
test_algs += setup_test('void', 'Event')
# LHCb::Composites
# @todo setup a producer of Composites from 'produce' and add execution tests
test_algs += setup_test(
    'Composites',
    'Composite',
    # @note this exclusion doesn't work for composed functors!!
    exclusions=[
        'CHARGE',
    ])
# LHCb::Pr::Fitted::Forward::Tracks
PrFittedForwardTracks_derived_execute_exclusions = [
    # This is because the fake LHCb::Pr::Fitted::Forward::Tracks that we use
    # for the execute tests do not have valid long track ancestors.
    'NHITS',
]
test_algs += setup_test(
    'PrFittedForwardTracks',
    'Track',
    producer=inputs["Container"],
    cannot_execute=PrFittedForwardTracks_derived_execute_exclusions)
# LHCb::Pr::Fitted::Forward::TracksWithPVs
test_algs += setup_test(
    'PrFittedForwardTracksWithPVs',
    'Track',
    cannot_execute=PrFittedForwardTracks_derived_execute_exclusions)
# LHCb::Pr::Fitted::Forward::TracksWithMuonID
test_algs += setup_test(
    'PrFittedForwardTracksWithMuonID',
    'TrackWithMuonID',
    cannot_execute=PrFittedForwardTracks_derived_execute_exclusions)

options = Options(
    simulation=True,
    data_type="Upgrade",
    event_store="EvtStoreSvc",
    evt_max=10,
    dddb_tag='upgrade/master',
    conddb_tag='upgrade/master',
)

configure_input(options)
top_node = CompositeNode("Top", test_algs)
configure(options, top_node)
