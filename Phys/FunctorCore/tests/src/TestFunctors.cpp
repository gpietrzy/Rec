/***************************************************************************** \
* (c) Copyright 2019-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestSelection
#include "Event/GenerateSOATracks.h"
#include "Event/HltDecReports.h"
#include "Event/MCHeader.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/ODIN.h"
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Event/PrVeloTracks.h"
#include "Event/PrimaryVertices.h"
#include "Event/Proxy.h"
#include "Event/RecSummary.h"
#include "Event/RelationTable.h"
#include "Event/TrackEnums.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "Functors/Adapters.h"
#include "Functors/Composite.h"
#include "Functors/Filter.h"
#include "Functors/JIT_includes.h"
#include "Functors/MVA.h"
#include "Functors/NeutralLike.h"
#include "Functors/Particle.h"
#include "Functors/Simulation.h"
#include "Functors/TES.h"
#include "Functors/TrackLike.h"
#include "Kernel/ParticleID.h"
#include "Kernel/Traits.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include "PrKernel/PrSelection.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
#include "SelKernel/ParticleCombination.h"
#include "SelTools/MatrixNet.h"

#include <boost/test/unit_test.hpp>

#include "Math/VectorUtil.h"

#include <cmath>
#include <random>
#include <type_traits>
#include <vector>

static const LHCb::UniqueIDGenerator unique_id_gen;

// Define some special functors that have execution counters so that we can
// test logical operator short-circuiting. Note that when constructing
// composite expressions the functors are copied, so having a mutable member
// variable and accessor does not work, as it is difficult to find the actual
// PT functor deep inside the composite functor object.
struct PT_Count : public Functors::Function {
  std::size_t& m_counter;
  PT_Count( std::size_t& counter ) : m_counter( counter ) {}
  template <typename TL>
  auto operator()( TL const& track ) const {
    m_counter++;
    return track.pt();
  }
};

struct ETA_Count : public Functors::Function {
  std::size_t& m_counter;
  ETA_Count( std::size_t& counter ) : m_counter( counter ) {}
  template <typename TL>
  auto operator()( TL const& track ) const {
    m_counter++;
    return track.pseudoRapidity();
  }
};

// Check that functors can be instantiated
BOOST_AUTO_TEST_CASE( test_creating_functors ) {
  [[maybe_unused]] Functors::AcceptAll ALL;
  [[maybe_unused]] auto                ip_gt_10 = Functors::Common::ImpactParameter();
}

struct DummyState {
  ROOT::Math::XYZVector m_position, m_slopes;
  DummyState( ROOT::Math::XYZVector position, ROOT::Math::XYZVector slopes )
      : m_position( std::move( position ) ), m_slopes( std::move( slopes ) ) {}
  ROOT::Math::XYZVector const& position() const { return m_position; }
  ROOT::Math::XYZVector const& slopes() const { return m_slopes; }

  friend auto slopes( const DummyState& s ) {
    return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{s.m_slopes.X(), s.m_slopes.Y(), s.m_slopes.Z()};
  }
  friend auto referencePoint( const DummyState& s ) {
    return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{s.m_position.X(), s.m_position.Y(), s.m_position.Z()};
  }
};

// dummy track type
struct DummyTrack {
  float                          m_pt{0.f}, m_eta{0.f};
  bool                           m_inacceptance{false};
  bool                           m_ismuon{false};
  float                          m_ebrem{0.f};
  std::vector<DummyState*>       m_states;
  float                          pt() const { return m_pt; }
  bool                           loop_mask() const { return true; }
  bool                           InAcceptance() const { return m_inacceptance; }
  bool                           IsMuon() const { return m_ismuon; }
  bool                           IsMuonTight() const { return false; }
  float                          Chi2Corr() const { return 1.f; }
  float                          LLMu() const { return 1.f; }
  float                          LLBg() const { return -3.f; }
  float                          CatBoost() const { return 0.5f; }
  float                          pseudoRapidity() const { return m_eta; }
  float                          chi2() const { return 1.f; }
  int                            nDoF() const { return 1; }
  float                          chi2PerDoF() const { return chi2() / nDoF(); }
  float                          ghostProbability() const { return 0.5f; }
  int                            history() const { return 0; }
  LHCb::Event::Enum::Track::Type type() const { return LHCb::Event::Enum::Track::Type::Long; }
  int                            nVPHits() const { return 3; }
  int                            nUTHits() const { return 4; }
  int                            nFTHits() const { return 5; }
  int                            nHits() const { return 12; }
  auto                           ClusterID() const { return LHCb::Detector::Calo::CellID{1}; }
  float                          BremEnergy() const { return m_ebrem; }
  float                          BremBendingCorrection() const { return 1.f; }
  auto                           BremHypoID() const { return LHCb::Detector::Calo::CellID{}; }
  float                          BremHypoMatch() const { return 0.f; }
  bool                           InBrem() const { return true; }
  bool                           HasBrem() const { return true; }
  bool checkFlag( LHCb::Event::Enum::Track::Flag flag ) const { return static_cast<int>( flag ) == 0; }
  const std::vector<DummyState*>& states() const { return m_states; }
  void                            addToStates( DummyState* state ) { m_states.push_back( state ); };

  template <Functors::detail::Pid t>
  float probNN() const {
    switch ( t ) {
    case Functors::detail::Pid::electron:
      return 0.3f;
    case Functors::detail::Pid::muon:
      return 0.3f;
    case Functors::detail::Pid::pion:
      return 0.3f;
    case Functors::detail::Pid::kaon:
      return 0.3f;
    case Functors::detail::Pid::proton:
      return 0.3f;
    case Functors::detail::Pid::deuteron:
      return 0.3f;
    case Functors::detail::Pid::ghost:
      return 0.3f;
    }
    __builtin_unreachable(); // suppress gcc warning -Wreturn-type
  }
  LHCb::State const*     stateAt( LHCb::State::Location ) const { return nullptr; }
  ROOT::Math::XYZVectorF closestToBeamStatePos() const { return {0.0, 0.0, 0.0}; }
  auto                   closestToBeamStateDir() const { return ROOT::Math::XYZVectorF{1, 0, std::sinh( m_eta )}; }
  auto                   threeMomentum() const { return m_pt * ROOT::Math::XYZVectorF{1, 0, std::sinh( m_eta )}; }
  LHCb::State            closestToBeamState() const { return {}; }
  friend auto            slopes( const DummyTrack& t ) {
    auto sl = t.closestToBeamStateDir();
    return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{sl.X(), sl.Y(), sl.Z()};
  }
  friend auto threeMomentum( const DummyTrack& t ) {
    auto mom = t.threeMomentum();
    return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{mom.X(), mom.Y(), mom.Z()};
  }
  friend auto referencePoint( const DummyTrack& t ) {
    auto pos = t.closestToBeamStatePos();
    return LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>{pos.X(), pos.Y(), pos.Z()};
  }
  static constexpr auto canBeExtrapolatedDownstream = LHCb::Event::CanBeExtrapolatedDownstream::yes;
  friend auto           trackState( const DummyTrack& t ) { return t.closestToBeamState(); }

  DummyTrack( float pt, float eta, bool inacceptance, bool ismuon, float ebrem = 0.f )
      : m_pt( pt ), m_eta( eta ), m_inacceptance( inacceptance ), m_ismuon( ismuon ), m_ebrem( ebrem ) {}
};
struct DummyReconstructedObject {
  DummyReconstructedObject( const int charge, const DummyTrack* track ) : m_charge( charge ), m_track( track ) {}
  const DummyTrack* track() const { return m_track; }
  bool              charge() const { return m_charge; }

private:
  int               m_charge;
  const DummyTrack* m_track;
};

using Tracks       = std::vector<DummyTrack>;
using TrackFunctor = Functors::Functor<Tracks( Tracks const& )>;
Tracks make_tracks() {
  Tracks tracks;
  tracks.emplace_back( 0.f, 3.f, true, true );
  tracks.emplace_back( 20.f, 1.f, false, false );
  tracks.emplace_back( 40.f, 5.f, false, false );
  return tracks;
}

using Functors::Filter;
auto const PT            = chain( Functors::Common::Rho_Coordinate{}, Functors::Track::ThreeMomentum{} );
auto const PZ            = chain( Functors::Common::Z_Coordinate{}, Functors::Track::ThreeMomentum{} );
auto const ETA           = chain( Functors::Common::Eta_Coordinate{}, Functors::Track::Slopes{} );
auto const ENDVERTEX_POS = chain( Functors::Common::Position{}, Functors::Common::EndVertex{} );
auto const END_VX        = chain( Functors::Common::X_Coordinate{}, ENDVERTEX_POS );
auto const END_VY        = chain( Functors::Common::Y_Coordinate{}, ENDVERTEX_POS );
auto const END_VZ        = chain( Functors::Common::Z_Coordinate{}, ENDVERTEX_POS );

// check we can define and access RICH DLL values
BOOST_AUTO_TEST_CASE( test_rich_dll ) {
  double true_val = 0.3;

  // get functors
  auto const RICH_DLL_E  = Functors::PID::RichDLLe{};
  auto const RICH_DLL_K  = Functors::PID::RichDLLk{};
  auto const RICH_DLL_PI = Functors::PID::RichDLLpi{};
  auto const RICH_DLL_P  = Functors::PID::RichDLLp{};
  auto const RICH_DLL_MU = Functors::PID::RichDLLmu{};
  auto const RICH_DLL_D  = Functors::PID::RichDLLd{};
  auto const RICH_DLL_BT = Functors::PID::RichDLLbt{};

  // define particle and protoparticle
  auto part  = LHCb::Particle( LHCb::ParticleID{11} );
  auto proto = LHCb::ProtoParticle();

  // add DLL info
  auto pid = LHCb::RichPID();
  pid.setParticleDeltaLL( Rich::ParticleIDType::Electron, true_val );
  pid.setParticleDeltaLL( Rich::ParticleIDType::Muon, true_val );
  pid.setParticleDeltaLL( Rich::ParticleIDType::Pion, true_val );
  pid.setParticleDeltaLL( Rich::ParticleIDType::Kaon, true_val );
  pid.setParticleDeltaLL( Rich::ParticleIDType::Proton, true_val );
  pid.setParticleDeltaLL( Rich::ParticleIDType::Deuteron, true_val );
  pid.setParticleDeltaLL( Rich::ParticleIDType::BelowThreshold, true_val );

  // add info
  proto.setRichPID( &pid );
  part.setProto( &proto );

  // do checks
  BOOST_CHECK( RICH_DLL_E( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
  BOOST_CHECK( RICH_DLL_K( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
  BOOST_CHECK( RICH_DLL_PI( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
  BOOST_CHECK( RICH_DLL_P( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
  BOOST_CHECK( RICH_DLL_MU( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
  BOOST_CHECK( RICH_DLL_D( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
  BOOST_CHECK( RICH_DLL_BT( part ).value_or( true_val + 1 ) - true_val < 1e-6 );
}

// check we can apply an IsMuon cut to some tracks
BOOST_AUTO_TEST_CASE( test_ptcut ) {
  TrackFunctor ISMUON{Filter{Functors::PID::IsMuon{}}};
  auto         tracks          = make_tracks();
  auto         filtered_tracks = ISMUON( tracks );
  BOOST_CHECK_EQUAL( tracks.size(), 3 );
  BOOST_CHECK_EQUAL( filtered_tracks.size(), 1 );
}
// check we can apply an InMuon cut to some tracks
BOOST_AUTO_TEST_CASE( test_inmuon ) {
  TrackFunctor INMUON{Filter{Functors::PID::InAcceptance{}}};
  auto         tracks          = make_tracks();
  auto         filtered_tracks = INMUON( tracks );
  BOOST_CHECK_EQUAL( tracks.size(), 3 );
  BOOST_CHECK_EQUAL( filtered_tracks.size(), 1 );
}
using SelOfDummy = Pr::Selection<DummyTrack>;

BOOST_AUTO_TEST_CASE( test_ptcut_inmuon ) {
  Functors::Functor<SelOfDummy( SelOfDummy const& )> INMUON{Filter{Functors::PID::InAcceptance{}}};
  auto                                               tracks = make_tracks();
  Pr::Selection                                      track_sel{tracks};
  auto                                               filtered_track_sel = INMUON( track_sel );
  BOOST_CHECK_EQUAL( track_sel.size(), 3 );
  BOOST_CHECK_EQUAL( filtered_track_sel.size(), 1 );
}

BOOST_AUTO_TEST_CASE( test_ptcut_selection ) {
  Functors::Functor<SelOfDummy( SelOfDummy const& )> ISMUON{Filter{Functors::PID::IsMuon{}}};
  auto                                               tracks = make_tracks();
  Pr::Selection                                      track_sel{tracks};
  auto                                               filtered_track_sel = ISMUON( track_sel );
  BOOST_CHECK_EQUAL( track_sel.size(), 3 );
  BOOST_CHECK_EQUAL( filtered_track_sel.size(), 1 );
}

// check that composition gives something sensible
BOOST_AUTO_TEST_CASE( test_functor_composition ) {
  auto         tracks = make_tracks();
  TrackFunctor high_pT_any_eta{Filter{( PT > 10.f ) & ( ETA > 0 )}};
  BOOST_CHECK_EQUAL( high_pT_any_eta( tracks ).size(), 2 );
  TrackFunctor high_pT_or_eta{Filter{( PT > 10.f ) | ( ETA > 2 )}};
  BOOST_CHECK_EQUAL( high_pT_or_eta( tracks ).size(), 3 );
  TrackFunctor low_pT{Filter{~( PT > 10.f )}};
  BOOST_CHECK_EQUAL( low_pT( tracks ).size(), 1 );
  TrackFunctor low_eta_or_high_pT{Filter{~( ETA > 2 ) | ( PT > 30.f )}};
  BOOST_CHECK_EQUAL( low_eta_or_high_pT( tracks ).size(), 2 );
}

// check that predicates constructed from functors work
BOOST_AUTO_TEST_CASE( test_predicate_from_function ) {
  auto         tracks       = make_tracks();
  TrackFunctor pt_gt_10     = Filter{PT > 10};
  TrackFunctor pt_lt_10     = Filter{PT < 10};
  TrackFunctor pt_gt_10_alt = Filter{10 < PT};
  TrackFunctor pt_lt_10_alt = Filter{10 > PT};
  BOOST_CHECK_EQUAL( pt_gt_10( tracks ).size(), 2 );
  BOOST_CHECK_EQUAL( pt_lt_10( tracks ).size(), 1 );
  BOOST_CHECK_EQUAL( pt_gt_10( tracks ).size(), pt_gt_10_alt( tracks ).size() );
  BOOST_CHECK_EQUAL( pt_lt_10( tracks ).size(), pt_lt_10_alt( tracks ).size() );
}

auto one_track_functor() {
  // This is the full Run2 Hlt1TrackMVA expression, but we can't actually test using it because it has data
  // dependencies. ProbNN cuts have then been added by https://gitlab.cern.ch/lhcb/Rec/-/merge_requests/2471
  Functors::Track::GhostProbability GHOSTPROB;
  Functors::Track::Chi2PerDoF       CHI2PERDOF;

  auto MinPT     = 1.f;
  auto MaxPT     = 25.f;
  auto MinIPChi2 = 7.4f;
  auto PV_loc    = "Rec/Vertex/Primary";

  // generated via python. One would never actually rigth the below in c++
  auto f_min_ipchi2_cut = Functors::bind(
      ::Functors::Functional::MapAllOf( operator>( ::Functors::Common::ImpactParameterChi2{}, MinIPChi2 ) ),
      ::Functors::Common::TES<LHCb::Event::PV::PrimaryVertexContainer>( std::vector{std::string{PV_loc}} ),
      ::Functors::Common::ForwardArgs{} );

  auto f_min_ipchi2 =
      Functors::chain( ::Functors::Functional::Min{},
                       Functors::bind( ::Functors::Functional::Map( ::Functors::Common::ImpactParameterChi2{} ),
                                       ::Functors::Common::TES<LHCb::Event::PV::PrimaryVertexContainer>(
                                           std::vector{std::string{PV_loc}} ),
                                       ::Functors::Common::ForwardArgs{} ) );

  TrackFunctor one_track = Filter{
      ( CHI2PERDOF < 2.f ) & ( GHOSTPROB < 0.3f ) & ( Functors::Track::PROBNN_PI_t{} > 0.1f ) &
      ( ( ( PT > MaxPT ) & f_min_ipchi2_cut ) |
        ( Functors::math::in_range( MinPT, PT, MaxPT ) &
          ( Functors::math::log( f_min_ipchi2 ) > ( 1.0 / Functors::math::pow( PT - 1.0, 2 ) +
                                                    ( 1.0 / MaxPT ) * ( MaxPT - PT ) + std::log( MinIPChi2 ) ) ) ) )};
  // If we actually call one_track_functor() this will crash, but we can at least make sure the template can be
  // instantiated
  BOOST_CHECK_EQUAL( one_track( make_tracks() ).size(), 0 );
}

template <typename T, typename Fun>
void testOnParticles( std::vector<int> const& codes, Fun fun ) {
  // make filter
  using PartsFunctor = Functors::Functor<T( T const& )>;
  PartsFunctor cut   = Filter{fun};
  // make particles
  T parts;
  for ( auto i = codes.begin(); i != codes.end(); i++ ) {
    if constexpr ( std::is_pointer_v<typename T::value_type> ) {
      parts.push_back( new LHCb::Particle( LHCb::ParticleID( *i ), int( i - codes.begin() ) ) ); // creator with PID and
                                                                                                 // key
    } else {
      parts.push_back( LHCb::Particle( LHCb::ParticleID( *i ), int( i - codes.begin() ) ) );
    }
  }
  // test
  BOOST_CHECK( ( cut( parts ) ).empty() );
}
template <typename Fun>
void testPointerAndObject( std::vector<int> const& codes, Fun fun ) {
  testOnParticles<std::vector<LHCb::Particle const*>>( codes, fun );
  testOnParticles<std::vector<LHCb::Particle>>( codes, fun );
}

BOOST_AUTO_TEST_CASE( test_ai_functors ) {
  // test probNN
  testPointerAndObject( std::vector<int>{13, 211, 321},
                        ( Functors::Track::PROBNN_MU_t{} > 0.1f ) & ( Functors::Track::PROBNN_E_t{} > 0.1f ) &
                            ( Functors::Track::PROBNN_P_t{} > 0.1f ) & ( Functors::Track::PROBNN_K_t{} > 0.1f ) &
                            ( Functors::Track::PROBNN_PI_t{} > 0.1f ) & ( Functors::Track::PROBNN_D_t{} > 0.1f ) &
                            ( Functors::Track::PROBNN_GHOST_t{} < 0.8f ) );
  // test neutral functors
  testPointerAndObject(
      std::vector<int>{22, 111},
      ( Functors::Neutral::IsPhoton_t{} > 0.5f ) & ( Functors::Neutral::IsNotH_t{} > 0.1f ) &
          ( Functors::Neutral::ShowerShape_t{} < 0.f ) & ( Functors::Neutral::NeutralE19_t{} > 0.3f ) &
          ( Functors::chain( Functors::PID::CaloCellID::All{}, Functors::Neutral::NeutralID_t{} ) > 1.0f ) &
          ( Functors::Neutral::ClusterMass_t{} > 0.1f ) & ( Functors::Neutral::NeutralEcal_t{} > 0.1f ) &
          ( Functors::Neutral::NeutralHcal2Ecal_t{} > 0.1f ) & ( Functors::Neutral::NeutralE49_t{} > 0.1f ) &
          ( Functors::Neutral::Saturation_t{} > 0.1f ) );

  // test calo and rich track functors
  testPointerAndObject(
      std::vector<int>{11, 22, 211},
      ( Functors::PID::HasBrem{} ) & ( Functors::Track::HasBremAdded{} ) & ( Functors::PID::InEcal{} ) &
          ( Functors::chain( Functors::PID::CaloCellID::All{}, Functors::PID::ClusterID{} ) > 0 ) &
          ( Functors::PID::InHcal{} ) & ( Functors::PID::InBrem{} ) & ( Functors::PID::BremEnergy{} > 0.f ) &
          ( Functors::PID::BremBendCorr{} > 0.f ) & ( Functors::PID::BremPIDe{} > 0.f ) &
          ( Functors::PID::EcalPIDe{} > 0.f ) & ( Functors::PID::EcalPIDmu{} > 0.f ) &
          ( Functors::PID::HcalPIDe{} > 0.f ) & ( Functors::PID::HcalPIDmu{} > 0.f ) &
          ( Functors::PID::ElectronShowerEoP{} > 0.f ) & ( Functors::PID::ElectronShowerDLL{} > 0.f ) &
          ( Functors::PID::ClusterMatch{} > 0.f ) & ( Functors::PID::ElectronMatch{} > 0.f ) &
          ( Functors::PID::BremHypoMatch{} > 0.f ) & ( Functors::PID::ElectronEnergy{} > 0.f ) &
          ( Functors::PID::BremHypoEnergy{} > 0.f ) & ( Functors::PID::BremHypoDeltaX{} > 0.f ) &
          ( Functors::chain( Functors::PID::CaloCellID::All{}, Functors::PID::BremHypoID{} ) > 0 ) &
          ( Functors::PID::BremTrackBasedEnergy{} > 0.f ) &
          ( Functors::chain( Functors::PID::CaloCellID::All{}, Functors::PID::ElectronID{} ) > 0 ) &
          ( Functors::chain( Functors::PID::CaloCellID::Column{}, Functors::PID::ClusterID{} ) > 0.f ) &
          ( Functors::chain( Functors::PID::CaloCellID::Row{}, Functors::PID::ClusterID{} ) > 0.f ) &
          ( Functors::chain( Functors::PID::CaloCellID::Area{}, Functors::PID::ClusterID{} ) > 0.f ) &
          ( Functors::PID::HcalEoP{} > 0.f ) & ( Functors::PID::RichDLLe{} > 0.f ) &
          ( Functors::PID::RichDLLmu{} > 0.f ) & ( Functors::PID::RichDLLp{} > 0.f ) &
          ( Functors::PID::RichDLLk{} > 0.f ) & ( Functors::PID::RichDLLpi{} > 0.f ) &
          ( Functors::PID::RichDLLd{} > 0.f ) & ( Functors::PID::RichDLLbt{} > 0.f ) &
          ( Functors::PID::RichScaledDLLe{} > 0.f ) & ( Functors::PID::RichScaledDLLmu{} > 0.f ) &
          ( Functors::PID::RichThresholdEl{} ) & ( Functors::PID::Rich1GasUsed{} ) &
          ( Functors::PID::Rich2GasUsed{} ) );
}

BOOST_AUTO_TEST_CASE( test_1trackmva_functor ) {
  Functors::Track::GhostProbability GHOSTPROB;
  Functors::Track::Chi2PerDoF       CHI2PERDOF;
  auto                              MinPT          = 1.f;
  auto                              MaxPT          = 25.f;
  auto                              MinIPChi2      = 7.4f;
  TrackFunctor                      pt_squared_cut = Filter{Functors::math::pow( PT, 2 ) > 2.f};
  // This has had ETA put in place of MINIPCHI2 so it should actually be able to run...
  TrackFunctor one_track_simple =
      Filter{( CHI2PERDOF < 2.f ) & ( GHOSTPROB < 0.3f ) &
             ( ( ( PT > MaxPT ) & ( ETA > MinIPChi2 ) ) |
               ( Functors::math::in_range( MinPT, PT, MaxPT ) &
                 ( Functors::math::log( ETA ) > ( 1.0 / Functors::math::pow( PT - 1.0, 2 ) +
                                                  ( 1.0 / MaxPT ) * ( MaxPT - PT ) + std::log( MinIPChi2 ) ) ) ) )};
  auto tracks = make_tracks();
  auto test_1 = pt_squared_cut( tracks );
  auto test_2 = one_track_simple( tracks );
}

BOOST_AUTO_TEST_CASE( test_general_track_functor ) {
  Functors::Track::Type                                                      TYPE;
  Functors::Track::HasT                                                      HAST;
  Functors::Track::HasUT                                                     HASUT;
  Functors::Track::HasVelo                                                   HASVELO;
  Functors::Track::History                                                   HISTORY;
  Functors::Track::nFTHits                                                   NFTHITS;
  Functors::Track::nUTHits                                                   NUTHITS;
  Functors::Track::nVPHits                                                   NVPHITS;
  Functors::Track::nHits                                                     NHITS;
  Functors::Track::nDoF                                                      NDOF;
  Functors::Track::Chi2                                                      CHI2;
  Functors::Track::MC_Reconstructed                                          MC_RECONSTRUCTED;
  Functors::Track::States                                                    STATES;
  Functors::Particle::GetTrack                                               GETTRACK;
  Functors::Track::ReferencePoint                                            REFERENCEPOINT;
  Functors::Track::IsTrackType<LHCb::Event::Enum::Track::Type::Ttrack>       ISTTRACK;
  Functors::Track::IsTrackType<LHCb::Event::Enum::Track::Type::Downstream>   ISDOWNSTREAM;
  Functors::Track::IsTrackType<LHCb::Event::Enum::Track::Type::Upstream>     ISUPSTREAM;
  Functors::Track::IsTrackType<LHCb::Event::Enum::Track::Type::Long>         ISLONG;
  Functors::Track::IsTrackType<LHCb::Event::Enum::Track::Type::Velo>         ISVELO;
  Functors::Track::IsTrackType<LHCb::Event::Enum::Track::Type::VeloBackward> ISVELOBACKWARD;
  Functors::Track::HasTrackFlag<LHCb::Event::Enum::Track::Flag::Invalid>     ISINVALID;
  Functors::Track::HasTrackFlag<LHCb::Event::Enum::Track::Flag::Selected>    ISSELECTED;
  Functors::Track::HasTrackFlag<LHCb::Event::Enum::Track::Flag::Clone>       ISCLONE;

  Functors::Functional::CastTo<int> CAST_TO_INT;
  BOOST_CHECK_EQUAL( CAST_TO_INT( true ), 1 );
  BOOST_CHECK_EQUAL( CAST_TO_INT( false ), 0 );

  DummyTrack track{10.f, 10.f, true, true};
  BOOST_CHECK_EQUAL( TYPE( track ), LHCb::Event::Enum::Track::Type::Long );
  BOOST_CHECK_EQUAL( HAST( TYPE( track ) ), true );
  BOOST_CHECK_EQUAL( HASUT( TYPE( track ) ), true );
  BOOST_CHECK_EQUAL( HASVELO( TYPE( track ) ), true );
  BOOST_CHECK_EQUAL( ISTTRACK( track ), false );
  BOOST_CHECK_EQUAL( ISDOWNSTREAM( track ), false );
  BOOST_CHECK_EQUAL( ISUPSTREAM( track ), false );
  BOOST_CHECK_EQUAL( ISLONG( track ), true );
  BOOST_CHECK_EQUAL( ISVELO( track ), false );
  BOOST_CHECK_EQUAL( ISVELOBACKWARD( track ), false );
  BOOST_CHECK_EQUAL( ISINVALID( track ), false );
  BOOST_CHECK_EQUAL( ISSELECTED( track ), false );
  BOOST_CHECK_EQUAL( ISCLONE( track ), false );
  BOOST_CHECK_EQUAL( HISTORY( track ), 0 );
  BOOST_CHECK_EQUAL( NVPHITS( track ), 3 );
  BOOST_CHECK_EQUAL( NUTHITS( track ), 4 );
  BOOST_CHECK_EQUAL( NFTHITS( track ), 5 );
  BOOST_CHECK_EQUAL( NHITS( track ), 12 );
  BOOST_CHECK_EQUAL( NDOF( track ), 1 );
  BOOST_CHECK_EQUAL( CHI2( track ), 1.f );

  DummyReconstructedObject rec{1, &track};
  BOOST_CHECK_EQUAL( MC_RECONSTRUCTED( rec ), 1 );
  BOOST_CHECK_EQUAL( TYPE( GETTRACK( rec ).value() ), LHCb::Event::Enum::Track::Type::Long );

  DummyState state_0( {0, 0, 0}, {1, 1, 1} );
  track.addToStates( &state_0 );
  BOOST_CHECK_EQUAL( REFERENCEPOINT( STATES( track ).front() ).x().cast(), 0 );

  Functors::Track::StateAt       STATE_AT{LHCb::State::Location::FirstMeasurement};
  Functors::Track::StateAt       STATE_AT_CTB{"ClosestToBeam"};
  Functors::Track::TrackState    FIRST_STATE{};
  Functors::Common::Position     POS{};
  Functors::Common::X_Coordinate X{};
  auto                           v1track = LHCb::Event::v1::Track{};
  v1track.addToStates( LHCb::State{{1, 2, 3, 4, 5}, 6, LHCb::State::Location::FirstMeasurement} );

  BOOST_CHECK_EQUAL( X( POS( ( STATE_AT( v1track ) ) ) ), 1 );

  auto const  tracks1 = LHCb::Event::v3::generate_tracks( 1 /* nTracks */, unique_id_gen, 1 /* seed */ );
  const auto& proxy   = tracks1.scalar()[0];
  BOOST_CHECK( abs( X( POS( STATE_AT_CTB( proxy ) ) ) ).cast() < 10.f );
  BOOST_CHECK( abs( X( POS( FIRST_STATE( proxy ) ) ) ).cast() < 10.f );
}

BOOST_AUTO_TEST_CASE( test_brem_functors ) {
  auto const BREM           = Functors::Track::Bremsstrahlung{};
  auto const PZ_WITH_BREM   = chain( PZ, BREM );
  auto const MASS_WITH_BREM = chain( Functors::Composite::Mass{}, BREM );

  auto prepared_call = []( auto& functor, auto const& input ) {
    return functor.prepare( EventContext{}, Functors::TopLevelInfo{} )( true, input );
  };

  DummyTrack track{10.f, 0.f, true, true, 2.f};
  auto       mom1 = threeMomentum( BREM( track ) );
  auto       mom2 = threeMomentum( track );
  BOOST_CHECK_EQUAL( mom1.mag().cast(), 1.2 * mom2.mag().cast() );

  auto etrack1 = LHCb::Track();
  auto eproto1 = LHCb::ProtoParticle();
  auto ebrem1  = LHCb::Event::Calo::v1::BremInfo();
  ebrem1.setInBrem( true );
  ebrem1.setHasBrem( true );
  ebrem1.setBremEnergy( 2. * Gaudi::Units::GeV );
  eproto1.setTrack( &etrack1 );
  eproto1.setBremInfo( &ebrem1 );
  Gaudi::LorentzVector emom1     = {0., 0., 10. * Gaudi::Units::GeV, 10. * Gaudi::Units::GeV};
  auto                 electron1 = LHCb::Particle( LHCb::ParticleID{11} );
  electron1.setMomentum( emom1 );
  electron1.setProto( &eproto1 );
  BOOST_CHECK_EQUAL( threeMomentum( BREM( electron1 ) ).z().cast(), 12. * Gaudi::Units::GeV );

  auto etrack2 = LHCb::Track();
  auto eproto2 = LHCb::ProtoParticle();
  auto ebrem2  = LHCb::Event::Calo::v1::BremInfo();
  ebrem2.setInBrem( true );
  ebrem2.setHasBrem( true );
  ebrem2.setBremEnergy( 3. * Gaudi::Units::GeV );
  eproto2.setTrack( &etrack2 );
  eproto2.setBremInfo( &ebrem2 );
  Gaudi::LorentzVector emom2     = {0., 0., -9. * Gaudi::Units::GeV, 9. * Gaudi::Units::GeV};
  auto                 electron2 = LHCb::Particle( LHCb::ParticleID{-11} );
  electron2.setMomentum( emom2 );
  electron2.setProto( &eproto2 );

  auto dielectron = LHCb::Particle( LHCb::ParticleID{443} );
  dielectron.addToDaughters( &electron1 );
  dielectron.addToDaughters( &electron2 );
  dielectron.setMomentum( electron1.momentum() + electron2.momentum() );
  auto const dielectron_vertex = LHCb::Vertex( Gaudi::XYZPoint( 0., 0., 0. ) );
  dielectron.setEndVertex( &dielectron_vertex );
  BOOST_CHECK_EQUAL( endVertexPos( BREM( dielectron ) ).x().cast(), 0. );
  BOOST_CHECK_EQUAL( prepared_call( PZ_WITH_BREM, dielectron ).cast(), 0. );
  BOOST_CHECK_EQUAL( prepared_call( MASS_WITH_BREM, dielectron ).cast(), 24. * Gaudi::Units::GeV );
}

BOOST_AUTO_TEST_CASE( test_muonPID_functors ) {
  Functors::PID::IsMuonTight  ISMUONTIGHT;
  Functors::PID::MuonChi2Corr MUONCHI2CORR;
  Functors::PID::MuonLLMu     MUONLLMU;
  Functors::PID::MuonLLBg     MUONLLBG;
  Functors::PID::MuonCatBoost MUONCATBOOST;

  DummyTrack track{10.f, 10.f, true, true};
  BOOST_CHECK_EQUAL( ISMUONTIGHT( track ), false );
  BOOST_CHECK_EQUAL( MUONCHI2CORR( track ), 1.f );
  BOOST_CHECK_EQUAL( MUONLLMU( track ), 1.f );
  BOOST_CHECK_EQUAL( MUONLLBG( track ), -3.f );
  BOOST_CHECK_EQUAL( MUONCATBOOST( track ), 0.5f );
}

BOOST_AUTO_TEST_CASE( test_reconstructible_functor ) {
  // Build MCProperty
  const auto        electron = LHCb::ParticleID{11};
  LHCb::MCProperty  mc_property{};
  LHCb::MCParticles mc_particles{};
  mc_particles.insert( new LHCb::MCParticle{}, 1 );
  mc_particles.insert( new LHCb::MCParticle{}, 2 );
  mc_particles( 1 )->setParticleID( electron );
  mc_particles( 2 )->setParticleID( electron );

  int property_1 = MCTrackInfo::flagMasks::maskHasT | MCTrackInfo::flagMasks::maskHasUT |
                   MCTrackInfo::flagMasks::maskAccT | MCTrackInfo::flagMasks::maskAccUT;
  int property_2 = property_1 | MCTrackInfo::flagMasks::maskHasVelo | MCTrackInfo::flagMasks::maskAccVelo;

  mc_property.setProperty( mc_particles( 1 ), property_1 );
  mc_property.setProperty( mc_particles( 2 ), property_2 );

  // Build functor
  Functors::Simulation::MC::Property              MC_PROPERTY;
  Functors::Simulation::CheckMask                 CHECKMASK{MCTrackInfo::flagMasks::maskHasVelo};
  Functors::Simulation::MC::ChargeReconstructible CHARGE_RECONSTRUCTIBLE;
  Functors::Simulation::Category                  CATEGORY;

  // Test
  BOOST_CHECK_EQUAL( MC_PROPERTY( mc_property, mc_particles( 1 ) ), property_1 );
  BOOST_CHECK_EQUAL( MC_PROPERTY( mc_property, mc_particles( 2 ) ), property_2 );
  BOOST_CHECK_EQUAL( CHECKMASK( MC_PROPERTY( mc_property, mc_particles( 1 ) ).value_or( 0 ) ), false );
  BOOST_CHECK_EQUAL( CHECKMASK( MC_PROPERTY( mc_property, mc_particles( 2 ) ).value_or( 0 ) ), true );
  BOOST_CHECK_EQUAL( CHARGE_RECONSTRUCTIBLE( MC_PROPERTY( mc_property, mc_particles( 1 ) ).value_or( 0 ) ),
                     3 ); // Downstream = 3
  BOOST_CHECK_EQUAL( CHARGE_RECONSTRUCTIBLE( MC_PROPERTY( mc_property, mc_particles( 2 ) ).value_or( 0 ) ),
                     2 ); // Long = 2
  BOOST_CHECK_EQUAL( CATEGORY( 123 ), 123 );
}

BOOST_AUTO_TEST_CASE( test_short_circuiting ) {
  std::size_t  pT_counter{0}, eta_counter{0};
  PT_Count     counting_PT( pT_counter );
  ETA_Count    counting_ETA( eta_counter );
  TrackFunctor high_pT_and_eta = Filter{( counting_PT > 10.f ) & ( counting_ETA > 3.f )};
  auto         cut_tracks      = high_pT_and_eta( make_tracks() );
  BOOST_CHECK_EQUAL( cut_tracks.size(), 1 );
  BOOST_CHECK_EQUAL( pT_counter, 3 );  // should be called for every track
  BOOST_CHECK_EQUAL( eta_counter, 2 ); // should only be called for high pT tracks

  pT_counter = eta_counter    = 0; // reset counters for clarity
  TrackFunctor high_pT_or_eta = Filter{( counting_PT > 10.f ) | ( counting_ETA > 2.f )};
  auto         cut_tracks2    = high_pT_or_eta( make_tracks() );
  BOOST_CHECK_EQUAL( cut_tracks2.size(), 3 );
  BOOST_CHECK_EQUAL( pT_counter, 3 );  // should be called for every track
  BOOST_CHECK_EQUAL( eta_counter, 1 ); // should only be called for low pT tracks
}

BOOST_AUTO_TEST_CASE( test_scalar_functors ) {
  auto f_min_ip_cut =
      Functors::bind( ::Functors::Functional::MapAllOf( operator>( ::Functors::Common::ImpactParameter{}, -.1 ) ),
                      Functors::chain( ::Functors::Functional::Map( Functors::chain( ::Functors::Common::ToLinAlg{},
                                                                                     ::Functors::Common::Position{} ) ),
                                       ::Functors::Common::TES<LHCb::Event::PV::PrimaryVertexContainer>(
                                           std::vector{std::string{"path/to/pvs"}} ) ),
                      ::Functors::Common::ForwardArgs{} );

  BOOST_CHECK_THROW( TrackFunctor{Filter{f_min_ip_cut}}( make_tracks() ), GaudiException );

  auto f_min_ip_cut_recvtx = Functors::bind(
      ::Functors::Functional::MapAllOf( operator>( ::Functors::Common::ImpactParameter{}, -.1 ) ),
      Functors::chain( ::Functors::Functional::Map(
                           Functors::chain( ::Functors::Common::ToLinAlg{}, ::Functors::Common::Position{} ) ),
                       ::Functors::Common::TES<LHCb::RecVertices>( std::vector{std::string{"path/to/pvs"}} ) ),
      ::Functors::Common::ForwardArgs{} );
  BOOST_CHECK_THROW( TrackFunctor{Filter{f_min_ip_cut_recvtx}}( make_tracks() ), GaudiException );
}

BOOST_AUTO_TEST_CASE( test_vector_functors ) {

  Functors::Functional::Reverse REVERSE_RANGE;
  Functors::Functional::Front   FRONT;
  std::vector<int>              vec_int{1, 2, 3, 4, 5};
  BOOST_CHECK_EQUAL( FRONT( REVERSE_RANGE( vec_int ) ), 5 );

  using VT = LHCb::Pr::Velo::Tracks;
  auto f_min_ip_cut =
      Functors::bind( ::Functors::Functional::MapAllOf( operator>( ::Functors::Common::ImpactParameter{}, 0.1 ) ),
                      Functors::chain( ::Functors::Functional::Map( Functors::chain( ::Functors::Common::ToLinAlg{},
                                                                                     ::Functors::Common::Position{} ) ),
                                       ::Functors::Common::TES<LHCb::Event::PV::PrimaryVertexContainer>(
                                           std::vector{std::string{"path/to/pvs"}} ) ),
                      ::Functors::Common::ForwardArgs{} );

  BOOST_CHECK_THROW( Functors::Functor<VT( VT const& )>{Filter{f_min_ip_cut}}( VT{} ), GaudiException );
}

BOOST_AUTO_TEST_CASE( test_combination_cut_and_function ) {
  using Combination         = Sel::ParticleCombination<DummyTrack, DummyTrack>;
  using CombinationCut      = Functors::Functor<bool( Combination const& )>;
  using CombinationFunction = Functors::Functor<float( Combination const& )>;
  DummyTrack     t1{5.f, 0.f, false, false}, t2{10.f, 0.f, false, false};
  Combination    combination{t1, t2}; // the scalar sum of pTs is 15
  auto const     SUMPT = Functors::Adapters::Accumulate{PT};
  CombinationCut comb_cut{SUMPT > 20.f}, comb_cut_2{SUMPT > 10.f};
  BOOST_CHECK( !comb_cut( combination ) );
  BOOST_CHECK( comb_cut_2( combination ) );

  CombinationFunction sumpt_fn{SUMPT};
  BOOST_CHECK( abs( sumpt_fn( combination ) - ( t1.pt() + t2.pt() ) ) < std::numeric_limits<float>::epsilon() );
}

using TrackFunction  = Functors::Functor<float( DummyTrack const& )>;
using TrackPredicate = Functors::Functor<bool( DummyTrack const& )>;
BOOST_AUTO_TEST_CASE( test_single_object_functions ) {
  DummyTrack     track{5.f, 1.f, false, false}; // pT, eta,  InMuon, IsMuon
  TrackPredicate inacceptance{Functors::PID::InAcceptance{}}, ismuon{Functors::PID::IsMuon{}};
  TrackFunction  pT{PT}, eta{ETA};
  BOOST_CHECK_EQUAL( pT( track ), track.pt() );
  BOOST_CHECK_CLOSE( eta( track ), track.pseudoRapidity(), 0.0001 );
  BOOST_CHECK_EQUAL( inacceptance( track ), track.InAcceptance() );
  BOOST_CHECK_EQUAL( ismuon( track ), track.IsMuon() );
}

struct InfoStruct {
  std::ostream& stream() { return std::cout; }
};

BOOST_AUTO_TEST_CASE( test_mva_functor ) {
  using namespace Functors;
  auto         mva = MVA<Sel::MatrixNet>( {{"MatrixnetFile", std::string{"paramfile://data/Hlt1TwoTrackMVA.mx"}}},
                                  MVAInput( "chi2", Track::Chi2PerDoF{} ), MVAInput( "fdchi2", ETA ),
                                  MVAInput( "sumpt", Adapters::Accumulate{PT} ), MVAInput( "nlt16", ETA ) );
  TopLevelInfo top_level;
  mva.bind( top_level );
}

struct TwoProngs;
struct ChildIndexArray : LHCb::Event::indices2D_field<LHCb::Event::v3::Tracks const, std::index_sequence<24>, 2> {};
struct TwoProngs : LHCb::Event::SOACollection<TwoProngs, ChildIndexArray> {

  template <typename Tag>
  decltype( auto ) containers() const {
    if constexpr ( std::is_same_v<Tag, ChildIndexArray> ) { return childContainers; }
  }

  TwoProngs( LHCb::Event::v3::Tracks const* t1, LHCb::Event::v3::Tracks const* t2 ) : childContainers{t1, t2} {}

  static constexpr std::size_t num_children = 2;

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  struct TwoProngProxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
    using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;
    using simd_t = SIMDWrapper::type_map_t<simd>;
    auto track( size_t i ) const {
      assert( i < 2 );
      if constexpr ( behaviour != LHCb::Pr::ProxyBehaviour::ScatterGather2D ) {
        return this->container()
            ->template simd<simd>()
            .gather2D( this->indices(), this->loop_mask() )
            .template get<ChildIndexArray>( i );
      } else {
        return this->template get<ChildIndexArray>( i );
      }
    }

    TwoProngProxy& assign( std::array<int, 2> i, std::array<int, 2> j ) {
      this->template field<ChildIndexArray>( 0 ).set( i[0], i[1] );
      this->template field<ChildIndexArray>( 1 ).set( j[0], j[1] );
      return *this;
    }
    auto decayProducts() const { return Sel::ParticleCombination{track( 0 ), track( 1 )}; }
  };

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
  using proxy_type = TwoProngProxy<simd, behaviour, ContainerType>;

private:
  std::array<LHCb::Event::v3::Tracks const*, 2> childContainers;
};

BOOST_AUTO_TEST_CASE( test_soa_references ) {
  using StateLocation = LHCb::Event::v3::Tracks::StateLocation;
  auto const tracks1  = LHCb::Event::v3::generate_tracks( 3 /* nTracks */, unique_id_gen, 1 /* seed */ );
  auto const tracks2  = LHCb::Event::v3::generate_tracks( 3 /* nTracks */, unique_id_gen, 2 /* seed */ );

  // three vertices, two tracks per vertex, and a pair of container_index, item_in_container_index per track
  auto combinations =
      std::array{std::array{std::array{0, 0}, std::array{1, 0}}, std::array{std::array{0, 0}, std::array{1, 1}},
                 std::array{std::array{0, 2}, std::array{1, 2}}};

  auto vertices = TwoProngs{&tracks1, &tracks2};
  for ( auto const& combination : combinations ) {
    auto vtx = vertices.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    vtx.assign( combination[0], combination[1] );
  }

  // Functor we want to apply to the combination -- scalar sum of pT
  Functors::Adapters::Accumulate pf{PT};

  // Prepare it (in this case we know it provides .prepare() so we don't need
  // to use the detail::prepare() helper)
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         prepared = pf.prepare( evtCtx, top_level );

  int        i               = 0;
  auto const iterable_tracks = std::array{tracks1.scalar(), tracks2.scalar()};
  for ( auto vertex : vertices.scalar() ) { // will do three iterations...
    auto cf_result = prepared( true /* mask */, vertex );

    // Get the tracks by hand, so we can check the functor works properly
    auto const& t1 = iterable_tracks[combinations[i][0][0]][combinations[i][0][1]];
    auto const& t2 = iterable_tracks[combinations[i][1][0]][combinations[i][1][1]];
    ++i;

    // Check that the result is correct... allow for tiny numerical tolerance: 1e-7 relative
    BOOST_CHECK( all( abs( cf_result - ( t1.pt( StateLocation::ClosestToBeam ) +
                                         t2.pt( StateLocation::ClosestToBeam ) ) ) < 1.e-6 * abs( cf_result ) ) );
  }
}

struct Prepare_Helper : public Functors::Function {
  std::size_t& m_executions;
  std::size_t& m_preparations;
  Prepare_Helper( std::size_t& executions, std::size_t& preparations )
      : m_executions{executions}, m_preparations{preparations} {}

  auto prepare() const {
    m_preparations++;
    return [this]() { this->m_executions++; };
  }
};

BOOST_AUTO_TEST_CASE( test_preparation ) {
  std::size_t               execs{0}, preps{0};
  Functors::Functor<void()> func{Prepare_Helper{execs, preps}};
  BOOST_CHECK( execs == 0 );
  BOOST_CHECK( preps == 0 );
  auto prepped = func.prepare();
  BOOST_CHECK( preps == 1 );
  BOOST_CHECK( execs == 0 );
  prepped();
  BOOST_CHECK( preps == 1 );
  BOOST_CHECK( execs == 1 );
  prepped();
  BOOST_CHECK( preps == 1 );
  BOOST_CHECK( execs == 2 );
  func();
  BOOST_CHECK( preps == 2 );
  BOOST_CHECK( execs == 3 );
}

struct PrepareWithTopLevel : public Functors::Function {
  auto prepare( Functors::TopLevelInfo const& top_level ) const {
    return [ptr = top_level.algorithm()]() { return ptr; };
  }
};

BOOST_AUTO_TEST_CASE( test_toplevelinfo_prepare ) {
  auto                                         parent = reinterpret_cast<Gaudi::Algorithm*>( 0xdeadbeef );
  Functors::Functor<Gaudi::Algorithm const*()> func{PrepareWithTopLevel{}};
  func.bind( parent );
  BOOST_CHECK( func() == parent );
}

// Define some basic particle-like types
struct Charged {
  int  nHits() const { return 5; }
  auto threeMomentum() const { return LHCb::LinAlg::Vec{1.f, 2.f, 30.f}; }
  auto pt() const { return threeMomentum().rho(); }
};

struct Composite {
  auto threeMomentum() const { return LHCb::LinAlg::Vec{1.f, 4.f, 30.f}; }
  auto pt() const { return threeMomentum().rho(); }
};

// And a generic version
using Particle = std::variant<Charged, Composite>;

BOOST_AUTO_TEST_CASE( test_variants ) {
  // Make some different kinds of particles
  Particle p_charged   = Charged{};
  Particle p_composite = Composite{};

  // Use std::any so we can check what the type is without conversions
  Functors::Functor<std::any( Particle const& )> func{PT};

  // Exercise calling functors as visitors to variants
  auto charged_res   = func( p_charged );
  auto composite_res = func( p_composite );

  // The return type should be the common type to the different variant
  // members' pt() accessor return types, i.e. float
  BOOST_CHECK( func.rtype() == typeid( float ) );
  BOOST_CHECK( charged_res.type() == typeid( float ) );
  BOOST_CHECK( composite_res.type() == typeid( float ) );

  // Check the hardcoded values too
  BOOST_CHECK( abs( std::any_cast<float>( charged_res ) - Charged{}.pt() ) < std::numeric_limits<float>::epsilon() );
  BOOST_CHECK( abs( std::any_cast<float>( composite_res ) - Composite{}.pt() ) <
               std::numeric_limits<float>::epsilon() );
}

namespace detail {
  /** This test and these adapters are examples of how things could be done.
   *  If this functionality is useful, the adapters could be published in, for
   *  example, Adapters.h
   */
  template <typename F, typename... AllowedTypes>
  struct Adapter : public std::conditional_t<Functors::detail::is_functor_predicate_v<F>, Functors::Predicate,
                                             Functors::Function> {
    Adapter( F f ) : m_f{std::move( f )} {}

    /** Generic to the contained functor
     */
    void bind( Functors::TopLevelInfo& top_level ) { Functors::detail::bind( m_f, top_level ); }

    /** Prepare the contained functor and bake it into a new lambda.
     */
    auto prepare( EventContext const& evtCtx, Functors::TopLevelInfo const& top_level ) const {
      // Figure out what the common return type is for invoking the prepared
      // functor with the different permitted types (AllowedTypes...)
      using ret_t = std::common_type_t<std::invoke_result_t<Functors::detail::prepared<F>, AllowedTypes>...>;
      return [this, f = Functors::detail::prepare( m_f, evtCtx, top_level )]( auto const& part ) -> ret_t {
        // If a functor is called with a variant type then the functor is used
        // as a visitor on that type. If a particular functor is only valid for
        // a subset of the types then -- without an adapter -- this would lead
        // to compilation errors. This helper will be invoked for *all* members
        // of the variant, but it only forwards to the contained functors for a
        // subset of those members.
        using arg_t = std::decay_t<decltype( part )>;
        if constexpr ( ( std::is_same_v<arg_t, AllowedTypes> || ... ) ) {
          return std::invoke( f, part );
        } else {
          auto const our_name = Functors::detail::get_name( *this );
          throw GaudiException{our_name + " blocked call with type " + System::typeinfoName( typeid( arg_t ) ),
                               our_name, StatusCode::FAILURE};
        }
      };
    }

  private:
    F m_f;
  };

  template <typename F_allowed, typename F_default, typename... AllowedTypes>
  struct DefaultAdapter : public std::conditional_t<Functors::detail::is_functor_predicate_v<F_allowed>,
                                                    Functors::Predicate, Functors::Function> {
    static_assert( Functors::detail::is_functor_predicate_v<F_allowed> ==
                   Functors::detail::is_functor_predicate_v<F_default> );
    DefaultAdapter( F_allowed f_allowed, F_default f_default )
        : m_f_allowed{std::move( f_allowed )}, m_f_default{std::move( f_default )} {}

    /** Generic to the contained functors
     */
    void bind( Functors::TopLevelInfo& top_level ) {
      Functors::detail::bind( m_f_allowed, top_level );
      Functors::detail::bind( m_f_default, top_level );
    }

    /** Prepare the contained functors and bake them into a new lambda.
     */
    auto prepare( EventContext const& evtCtx, Functors::TopLevelInfo const& top_level ) const {
      return [f_allowed = Functors::detail::prepare( m_f_allowed, evtCtx, top_level ),
              f_default = Functors::detail::prepare( m_f_default, evtCtx, top_level )]( auto const& part ) {
        if constexpr ( ( std::is_same_v<std::decay_t<decltype( part )>, AllowedTypes> || ... ) ) {
          return std::invoke( f_allowed, part );
        } else {
          return std::invoke( f_default, part );
        }
      };
    }

  private:
    F_allowed m_f_allowed;
    F_default m_f_default;
  };
} // namespace detail

template <typename... AllowedTypes, typename F>
auto Adapter( F&& f ) {
  return detail::Adapter<F, AllowedTypes...>( std::forward<F>( f ) );
}

template <typename... AllowedTypes, typename F_allowed, typename F_default>
auto DefaultAdapter( F_allowed&& f_allowed, F_default&& f_default ) {
  return detail::DefaultAdapter<F_allowed, F_default, AllowedTypes...>( std::forward<F_allowed>( f_allowed ),
                                                                        std::forward<F_default>( f_default ) );
}

BOOST_AUTO_TEST_CASE( test_variant_adapters ) {
  // Prepare inputs
  Particle p_charged   = Charged{};
  Particle p_composite = Composite{};

  // This:
  //  Functors::Functor<int( Particle const& )> func{Functors::Track::nHits{}};
  // wouldn't compile because one member of the variant (Composite) doesn't
  // have the nHits() accessor demanded by the functor.

  // Use an adapter to specify which members of the variant are OK
  Functors::Functor<int( Particle const& )> func{Adapter<Charged>( Functors::Track::nHits{} )};

  // Use the adapter on a charged Particle, this should work:
  BOOST_CHECK_EQUAL( func( p_charged ), Charged{}.nHits() );

  // Using the Charged adapter on a Composite Particle should produce a runtime error
  try {
    func( p_composite );
    BOOST_CHECK( false ); // should never get here
  } catch ( GaudiException& e ) {
    BOOST_CHECK( e.message() == "detail::Adapter<Functors::Track::nHits,Charged> blocked call with type Composite" );
  }

  // Use an adapter that has a fallback instead of throwing a runtime error
  auto                                      default_nhits = 0;
  Functors::Functor<int( Particle const& )> func_default{
      DefaultAdapter<Charged>( Functors::Track::nHits{}, Functors::detail::promote( default_nhits ) )};

  BOOST_CHECK_EQUAL( func_default( p_charged ), Charged{}.nHits() );
  BOOST_CHECK_EQUAL( func_default( p_composite ), default_nhits );

  // Try with a cut instead
  Functors::Functor<bool( Particle const& )> cut{
      DefaultAdapter<Charged>( Functors::Track::nHits{} > 3, Functors::AcceptNone{} )};
  BOOST_CHECK( cut( p_charged ) );
  BOOST_CHECK( !cut( p_composite ) );

  // And check that the adapters don't choke on non-variant input, even though
  // in this case the adapters are pointless overhead that should be dropped
  // from the configuration
  BOOST_CHECK_EQUAL( func( Charged{} ), Charged{}.nHits() );
  BOOST_CHECK_THROW( func( Composite{} ), GaudiException );
  BOOST_CHECK_EQUAL( func_default( Charged{} ), Charged{}.nHits() );
  BOOST_CHECK_EQUAL( func_default( Composite{} ), default_nhits );
  BOOST_CHECK( cut( Charged{} ) );
  BOOST_CHECK( !cut( Composite{} ) );
}

BOOST_AUTO_TEST_CASE( test_bitwise_and_or ) {
  using VoidPredicate  = Functors::Functor<bool()>;
  using BitwiseFunctor = Functors::Functor<int()>;

  // 'Reference' value which we'll check for set bits
  auto           ref = Functors::detail::promote( 0b1010 );
  VoidPredicate  f_pass{Functors::math::test_bit( ref, 0b0010 )};
  VoidPredicate  f_fail{Functors::math::test_bit( ref, 0b0100 )};
  BitwiseFunctor f_and{Functors::bitwise_and( ref, 0b0101 )};
  BitwiseFunctor f_or{Functors::bitwise_or( ref, 0b0101 )};

  BOOST_CHECK( f_pass() == true );
  BOOST_CHECK( f_fail() == false );
  BOOST_CHECK( f_and() == 0b0000 );
  BOOST_CHECK( f_or() == 0b1111 );
}

namespace {
  template <typename T>
  constexpr auto approx_equal( T const& a, T const& b, float tolerance = std::numeric_limits<float>::epsilon() ) {
    using std::abs;
    using std::max;
    auto const diff = abs( a - b );
    return ( diff < tolerance ) || ( diff < max( abs( a ), abs( b ) ) );
  }
} // namespace

BOOST_AUTO_TEST_CASE( test_v2_particle_functors ) {
  using StateLocation        = LHCb::Event::v3::Tracks::StateLocation;
  constexpr auto ncomposites = 18;
  constexpr auto ntracks     = ncomposites + 2;
  // Produce the children
  auto const input1 = LHCb::Event::v3::generate_tracks( ntracks, unique_id_gen, 0 );
  auto const input2 = LHCb::Event::v3::generate_tracks( ntracks, unique_id_gen, 1 );
  // Iterable versions for convenience
  auto const iterin1 = input1.simd();
  auto const iterin2 = input2.simd();
  // Get the plain integers for convenience
  auto const input1_family_id = int{input1.zipIdentifier()};
  auto const input2_family_id = int{input2.zipIdentifier()};
  // Generate some composites
  using int_v   = SIMDWrapper::scalar::int_v;
  using float_v = SIMDWrapper::scalar::float_v;
  LHCb::Event::Composites threebody{unique_id_gen};

  for ( auto i = 0; i < ncomposites; ++i ) {
    std::array<int_v, 3> child_indices{i, i + 1, 0},
        child_zip_ids{input1_family_id, input2_family_id, input1_family_id};
    std::vector<LHCb::UniqueIDGenerator::ID<int_v>> child_unique_ids{
        unique_id_gen.generate<int_v>(), unique_id_gen.generate<int_v>(), unique_id_gen.generate<int_v>()};
    threebody.emplace_back<SIMDWrapper::InstructionSet::Scalar, float_v, int_v>(
        {} /* pos */, {i * 1.f, i * 2.f, 0.f, 0.f} /* p4 */, 42 /* pid */, 3.f /* chi2 */, 2 /*ndof */,
        {} /* pos_cov */, {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f} /* p4_cov */,
        {0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f, 0.f} /* mom_pos_cov */, child_indices, child_zip_ids,
        child_unique_ids );
  }
  BOOST_CHECK_EQUAL( threebody.size(), ncomposites );

  // Functor we want to apply to the combination -- scalar sum of pT
  auto const SUMPT = Functors::Adapters::Accumulate{PT};

  // Set up the functor that should recover the combination of these tracks
  // from 'parent'...but this includes some magic to get the child containers
  // from the given TES locations, so we can't easily run it here. For now
  // we just check that creating it works.
  auto const pf = Functors::Adapters::CombinationFromComposite( SUMPT );

  // Prepare it (in this case we know it provides .prepare() so we don't need
  // to use the detail::prepare() helper)
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         prepared = pf.prepare( evtCtx, top_level );

  for ( auto composite : threebody.simd() ) {
    // Actually call the converter, making sure to add the data dependencies that
    // the wrapper would have injected.
    auto const cf_result = prepared( composite.loop_mask(), composite, input1, input2 );

    // Get the tracks by hand, so we can check the functor works properly
    auto const& t1 = iterin1[composite.offset()];
    auto const& t2 = iterin2[composite.offset() + 1];
    auto const& t3 = iterin1.scalar_fill( 0 ); // broadcasting

    // Check that the result is correct...
    auto const mask = approx_equal( cf_result, decltype( cf_result )( t1.pt( StateLocation::ClosestToBeam ) +
                                                                      t2.pt( StateLocation::ClosestToBeam ) +
                                                                      t3.pt( StateLocation::ClosestToBeam ) ) );
    BOOST_CHECK( none( composite.loop_mask() && !mask ) );
  }
}

// Create some particles. @todo Need to be adapted to v2
LHCb::Particle::Container make_parts() {
  LHCb::Particle::Container parts;
  LHCb::Particle*           p0 = new LHCb::Particle( LHCb::ParticleID( 431 ), 0 ); // creator with PID and key
  LHCb::Particle*           p1 = new LHCb::Particle( LHCb::ParticleID( 431 ), 1 ); // creator with PID and key
  LHCb::Particle*           p2 = new LHCb::Particle( LHCb::ParticleID( 431 ), 2 ); // creator with PID and key
  p0->setMomentum( Gaudi::LorentzVector( 100., 100., 1000., 5000. ) );             // 141 MeV pt
  p1->setMomentum( Gaudi::LorentzVector( 0., 0., 1000., 5000. ) );                 // 0 MeV pt
  p2->setMomentum( Gaudi::LorentzVector( 1000., 0., 1000., 5000. ) );              // 1000 MeV pt
  parts.insert( p0, 0 );                                                           // creator with PID and key
  parts.insert( p1, 1 );
  parts.insert( p2, 2 );
  return parts;
}

using RelationTable = LHCb::Relation1D<LHCb::Particle, LHCb::Particle>;

RelationTable make_relations( LHCb::Particle::Container const& parts ) {
  RelationTable table;
  table.relate( parts( 0 ), parts( 1 ) ).ignore(); // make sure I relate all of them. To what doesn't matter here.
  table.relate( parts( 1 ), parts( 2 ) ).ignore();
  table.relate( parts( 2 ), parts( 0 ) ).ignore();
  table.i_sort();
  return table;
}

using IsolationTable = LHCb::RelationWeighted2D<LHCb::Particle, LHCb::Particle, double>;

// I cannot use this as I have no access to Gaudi and thus cannot put these Relations on the TES
IsolationTable make_relations2D( LHCb::Particle::Container const& parts ) {
  IsolationTable table;
  // Dummy relations
  table.relate( parts( 0 ), parts( 1 ), 0.5 ).ignore();
  table.relate( parts( 0 ), parts( 2 ), 0.7 ).ignore();
  table.relate( parts( 1 ), parts( 0 ), 0.1 ).ignore();
  table.relate( parts( 1 ), parts( 2 ), 1. ).ignore();
  table.relate( parts( 2 ), parts( 0 ), 1.2 ).ignore();
  table.relate( parts( 2 ), parts( 1 ), 0.3 ).ignore();
  table.i_sort();
  return table;
}

BOOST_AUTO_TEST_CASE( test_relation_table ) {
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  // MapRelInputToFuncOutput functor: for Particle P get value of functor F for particle P' that is related to P.
  // Typical use case: Functor to apply other functor to refitted particle

  auto                        parts = make_parts();            // Create three dummay particles
  RelationTable               table = make_relations( parts ); // Relate them in a circular way
  RelationTable               empty_table;
  LHCb::Particle::ConstVector just0;
  just0.push_back( parts( 0 ) ); // Only part0
  LHCb::Particle::ConstVector just1;
  just1.push_back( parts( 1 ) ); // Only part1

  // Functor applying cut on another particle
  // @todo implement the generic case once the specific one works
  auto const empty_range = Functors::Common::Relations{}( empty_table, parts( 0 ) );
  auto const range0      = Functors::Common::Relations{}( table, parts( 0 ) );
  auto const range0_size = Functors::detail::SizeOf{}( range0.value() );

  BOOST_CHECK( !empty_range.has_value() );
  BOOST_CHECK( range0.value().size() == 1 );
  BOOST_CHECK( range0_size == 1 );

  auto const tmp =
      Functors::chain( PT, Functors::Common::To{}, Functors::Functional::Front{}, Functors::Common::Relations{} );
  auto const tmp_p       = tmp.prepare( evtCtx, top_level );
  auto       check_empty = tmp_p( true, empty_table, parts( 1 ) );
  auto       p0          = tmp_p( true, table, parts( 2 ) );
  auto       p1          = tmp_p( true, table, parts( 0 ) );
  auto       p2          = tmp_p( true, table, parts( 1 ) );
  BOOST_CHECK( !check_empty.has_value() );
  BOOST_CHECK( p0.has_value() );
  BOOST_CHECK( p1.has_value() );
  BOOST_CHECK( p2.has_value() );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( p0.value() ) > 100 && LHCb::Utils::as_arithmetic( p0.value() ) < 200 );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( p1.value() ) < 100 );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( p2.value() ) > 200 );
}

BOOST_AUTO_TEST_CASE( test_relation_table_array ) {
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         parts = make_parts(); // Create three dummay particles
  RelationTable                table = make_relations( parts );

  auto const range = Functors::Common::Relations{}( table, parts( 0 ) );
  BOOST_CHECK( range.has_value() );
  auto const pt_to     = Functors::chain( PT, Functors::Common::To{} );
  auto const pt_to_map = Functors::Functional::Map( pt_to );
  auto const prepped   = pt_to_map.prepare( evtCtx, top_level );
  auto const result    = prepped( true, range.value() );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( result.size() ) == 1 );
  BOOST_CHECK_SMALL( LHCb::Utils::as_arithmetic( result[0] ), std::numeric_limits<float>::min() );
}

BOOST_AUTO_TEST_CASE( test_relation_table_isolation ) {

  // Create vector of fake particles
  auto parts = make_parts();

  float fake_w0_min     = 0.25;
  float fake_w1_min     = 1.15;
  float fake_w0_max     = 0.35;
  float fake_w1_max     = 1.25;
  float fake_sum_pt_min = 1100.f;
  float fake_sum_pt_max = 1800.f;
  float fake_max_pt_min = 980.f;
  float fake_max_pt_max = 1020.f;
  float fake_min_pt_min = 130.f;
  float fake_min_pt_max = 150.f;
  float fake_asym_pt    = -1.;

  auto ref_part = std::make_unique<LHCb::Particle>( LHCb::ParticleID( 431 ), 0 );
  ref_part->setMomentum( Gaudi::LorentzVector( 0., 0., 1000., 5000. ) );

  LHCb::Vertex* vertex = new LHCb::Vertex();

  vertex->setPosition( Gaudi::XYZPoint{0.0, 0.0, 5.0} );

  ref_part->setEndVertex( vertex );

  // SUMRANGE and ConeVariables tests
  EventContext const evtCtx;

  Functors::TopLevelInfo top_level;

  IsolationTable table = make_relations2D( parts );
  IsolationTable empty_table;

  auto const front_weight =
      Functors::chain( Functors::Common::Weight{}, Functors::Functional::Front{}, Functors::Common::Relations{} );
  auto const front_weight_prepped = front_weight.prepare( evtCtx, top_level );
  auto const back_weight =
      Functors::chain( Functors::Common::Weight{}, Functors::Functional::Back{}, Functors::Common::Relations{} );
  auto const back_weight_prepped = back_weight.prepare( evtCtx, top_level );
  auto       w0                  = front_weight_prepped( true, table, parts( 2 ) );
  auto       w1                  = back_weight_prepped( true, table, parts( 2 ) );
  BOOST_CHECK( w0.has_value() );
  BOOST_CHECK( w1.has_value() );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( w0.value() ) > fake_w0_min &&
               LHCb::Utils::as_arithmetic( w0.value() ) < fake_w0_max );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( w1.value() ) > fake_w1_min &&
               LHCb::Utils::as_arithmetic( w1.value() ) < fake_w1_max );

  auto const pt_to_min_weight_entry = Functors::chain(
      PT, Functors::Common::To{}, Functors::Functional::EntryWithMinRelatedValueOf( Functors::Common::Weight{} ),
      Functors::Common::Relations{} );
  auto const pt_to_min_weight_entry_prepped = pt_to_min_weight_entry.prepare( evtCtx, top_level );
  auto const pt_to_max_weight_entry         = Functors::chain(
      PT, Functors::Common::To{}, Functors::Functional::EntryWithMaxRelatedValueOf( Functors::Common::Weight{} ),
      Functors::Common::Relations{} );
  auto const pt_to_max_weight_entry_prepped = pt_to_max_weight_entry.prepare( evtCtx, top_level );
  auto       empty_test                     = pt_to_min_weight_entry_prepped( true, empty_table, parts( 0 ) );
  auto       empty_test2                    = pt_to_max_weight_entry_prepped( true, empty_table, parts( 1 ) );
  auto       pt_wmin                        = pt_to_min_weight_entry_prepped( true, table, parts( 2 ) );
  auto       pt_wmax                        = pt_to_max_weight_entry_prepped( true, table, parts( 2 ) );
  BOOST_CHECK( !empty_test.has_value() );
  BOOST_CHECK( !empty_test2.has_value() );
  BOOST_CHECK( pt_wmin.has_value() );
  BOOST_CHECK( pt_wmax.has_value() );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( pt_wmin.value() ) < 100 );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( pt_wmax.value() ) > 100 );

  auto const value_or_pt_to_min_weight_entry = Functors::chain(
      Functors::Functional::ValueOr{0}, PT, Functors::Common::To{},
      Functors::Functional::EntryWithMinRelatedValueOf( Functors::Common::Weight{} ), Functors::Common::Relations{} );

  auto const value_or_pt_to_min_weight_entry_prepped = value_or_pt_to_min_weight_entry.prepare( evtCtx, top_level );
  auto       value_or_empty_test = value_or_pt_to_min_weight_entry_prepped( true, empty_table, parts( 0 ) );

  BOOST_CHECK( LHCb::Utils::as_arithmetic( value_or_empty_test ) == 0 );

  auto const relation = Functors::Common::Relations{}( table, parts( 1 ) );
  BOOST_CHECK( relation.has_value() );
  auto       pt_to     = chain( PT, Functors::Common::To{} );
  auto const pt_to_map = Functors::Functional::Map( pt_to );
  auto const prepped   = pt_to_map.prepare( evtCtx, top_level );
  auto const mapcone   = prepped( true, relation.value() );

  auto sumcone = Functors::Functional::Sum{}( mapcone );
  auto mincone = Functors::Functional::Min{}( mapcone );
  auto maxcone = Functors::Functional::Max{}( mapcone );
  auto nincone = Functors::detail::SizeOf{}( relation.value() );

  BOOST_CHECK( LHCb::Utils::as_arithmetic( sumcone ) > fake_sum_pt_min &&
               LHCb::Utils::as_arithmetic( sumcone ) < fake_sum_pt_max );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( maxcone ) > fake_max_pt_min &&
               LHCb::Utils::as_arithmetic( maxcone ) < fake_max_pt_max );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( mincone ) > fake_min_pt_min &&
               LHCb::Utils::as_arithmetic( mincone ) < fake_min_pt_max );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( nincone ), 2 );

  auto sum        = chain( Functors::Functional::Sum{}, Functors::Functional::Map( pt_to ) );
  auto p1_pt      = chain( PT, Functors::Common::ForwardArgs<0>{} );
  auto p1_cone_pt = chain( sum, Functors::Common::ForwardArgs<1>{} );
  auto tmp        = ( p1_pt - p1_cone_pt ) / ( p1_pt + p1_cone_pt );
  auto asym_p     = tmp.prepare( evtCtx, top_level );
  auto asym       = asym_p( true, parts( 1 ), relation.value() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( asym ), fake_asym_pt );
}

bool random_bool() {
  static std::default_random_engine engine{42};
  return std::uniform_int_distribution<int>{0, 1}( engine );
}

namespace Functors {
  template <typename Operator>
  struct Op_Count : public Predicate {
    float        m_threshold;
    std::size_t &m_counter, &m_scalar_counter;
    Op_Count( std::size_t& counter, std::size_t& scalar_counter, float threshold )
        : m_threshold{threshold}, m_counter{counter}, m_scalar_counter{scalar_counter} {}
    template <typename Data>
    auto operator()( bool mask, Data const& x ) const {
      m_scalar_counter += mask;
      if ( mask ) {
        return Operator{}( x, m_threshold );
      } else {
        // Return random results in slots that are invalid according to 'mask'
        return random_bool();
      }
    }

    template <typename mask_v, typename float_v>
    auto operator()( mask_v const& mask, float_v const& x ) const {
      m_counter += popcount( mask );
      auto const                         result = Operator{}( x, m_threshold );
      std::array<float, float_v::size()> data_to_generate_mask{};
      for ( auto i = 0ul; i < float_v::size(); ++i ) {
        data_to_generate_mask[i] = testbit( mask, i ) ? testbit( result, i ) : random_bool();
      }
      // 'result' where 'mask' was true, random results where 'mask' was false
      return float_v{data_to_generate_mask.data()} > float_v{0.5f};
    }
    constexpr static bool requires_explicit_mask = true;
  };
} // namespace Functors

BOOST_AUTO_TEST_CASE( test_mask_logic ) {
  // Test the short-circuiting behaviour of functors that use explicit masks by
  // generating a large number of combinations of input data and checking that
  // scalar (assumed correct...) and vector backends return the same results
  // and invoke the "calculations" with the same total number of positive mask
  // bits
  using simd_t = SIMDWrapper::sse::types; // vector width 4 is enough...
  std::size_t x1_counter{}, x1_counter_scalar{}, x2_counter{}, x2_counter_scalar{}, x4_counter{}, x4_counter_scalar{},
      x7_counter{}, x7_counter_scalar{};
  Functors::Op_Count<std::less<>>    X1{x1_counter, x1_counter_scalar, 0.5f};
  Functors::Op_Count<std::less<>>    X2{x2_counter, x2_counter_scalar, 2.f};
  auto const                         X3 = X1 | X2;
  Functors::Op_Count<std::greater<>> X4{x4_counter, x4_counter_scalar, 2.5f};
  auto const                         X5 = X1 | X2 | X4;
  auto const                         X6 = X1 | ( ~X2 & X4 );
  Functors::Op_Count<std::greater<>> X7{x7_counter, x7_counter_scalar, 0.5f};
  auto const                         X8 = X2 & ( X7 | X1 );
  EventContext const                 evtCtx;
  Functors::TopLevelInfo const       top_level;
  auto const                         X3_prepared  = X3.prepare( evtCtx, top_level );
  auto const                         X5_prepared  = X5.prepare( evtCtx, top_level );
  auto const                         X6_prepared  = X6.prepare( evtCtx, top_level );
  auto const                         X8_prepared  = X8.prepare( evtCtx, top_level );
  auto const                         check_counts = [&]() {
    BOOST_CHECK_EQUAL( x1_counter, x1_counter_scalar );
    BOOST_CHECK_EQUAL( x2_counter, x2_counter_scalar );
    BOOST_CHECK_EQUAL( x4_counter, x4_counter_scalar );
    BOOST_CHECK_EQUAL( x7_counter, x7_counter_scalar );
  };

  // Generate all possible masks for simd_t
  std::array<float, simd_t::size> data_to_generate_mask{}, data_to_generate_float{};
  for ( auto i = 0; i < std::pow( 2, simd_t::size ); ++i ) {
    for ( auto j = 0ul; j < simd_t::size; ++j ) { data_to_generate_mask[simd_t::size - 1 - j] = ( i & ( 0x1 << j ) ); }
    auto const mask = simd_t::float_v{data_to_generate_mask.data()} > 0.5f;

    // Generate some float_v vectors we can use as input, based on the
    // thresholds hardcoded in the test functors (X{1,2,...} above) then
    // [0, 1, 2, 3] are the only relevant "float" values -- generate all
    // possible vectors of simd_t::size (4) floats with the elements taking
    // these values
    for ( auto j = 0; j < std::pow( 2ul, 2 * simd_t::size ); ++j ) {
      for ( auto l = 0ul; l < simd_t::size; ++l ) { data_to_generate_float[l] = ( j >> 2 * l ) & 0x3; }
      auto const data = simd_t::float_v{data_to_generate_float};
      // Do some checks using 'mask' and 'data', vector-wise and scalar
      auto const do_tests = [&]( auto const& obj ) {
        auto const vector_res = obj( mask, data );
        for ( auto l = 0ul; l < simd_t::size; ++l ) {
          // Only compare parts of the result that were 'valid' in 'mask'
          // If the relevant part of 'mask' was zero, then the functor was free
          // to return any value there.
          if ( testbit( mask, l ) ) {
            auto const scalar_res = obj( true, data_to_generate_float[l] );
            BOOST_CHECK_EQUAL( testbit( vector_res, l ), scalar_res );
          }
        }
      };
      do_tests( X1 );
      do_tests( X2 );
      check_counts();
      do_tests( X3_prepared );
      check_counts();
      do_tests( X4 );
      check_counts();
      do_tests( X5_prepared );
      check_counts();
      do_tests( X6_prepared );
      check_counts();
      do_tests( X7 );
      check_counts();
      do_tests( X8_prepared );
      check_counts();
    }
  }
  check_counts();
}

BOOST_AUTO_TEST_CASE( test_cov_matrix_functors ) {

  // Define the input covariance matrix
  using ROOT::Math::SVector;
  SVector<double, 10>  data_momCovMatrix{1., 2., 3., 4., 5., 6., 7., 8., 9., 10.};
  SVector<double, 6>   data_posCovMatrix{6., 5., 4., 3., 2., 1.};
  double               data_posMomCovMatrix[12] = {11., 12., 13., 14., 15., 16., 17., 18., 19., 20., 21., 22.};
  Gaudi::SymMatrix4x4  v1_momCovMatrix{data_momCovMatrix};
  Gaudi::SymMatrix3x3  v1_posCovMatrix{data_posCovMatrix};
  Gaudi::Matrix4x3     v1_posMomCovMatrix{data_posMomCovMatrix, 12};
  Gaudi::LorentzVector v1_p4{1., 2., 3., 4.};

  // Create v1 particle
  LHCb::Particle v1_particle{LHCb::ParticleID( 511 ), 0};
  v1_particle.setMomCovMatrix( v1_momCovMatrix );
  v1_particle.setPosCovMatrix( v1_posCovMatrix );
  v1_particle.setPosMomCovMatrix( v1_posMomCovMatrix );
  v1_particle.setMomentum( v1_p4 );

  // Create v2 particles
  using SIMDWrapper::scalar::float_v;
  using SIMDWrapper::scalar::int_v;

  auto v2_momCovMatrix    = LHCb::LinAlg::convert<float_v>( v1_momCovMatrix );
  auto v2_posCovMatrix    = LHCb::LinAlg::convert<float_v>( v1_posCovMatrix );
  auto v2_posMomCovMatrix = LHCb::LinAlg::convert<float_v>( v1_posMomCovMatrix );

  auto                    zip_id = Zipping::generateZipIdentifier();
  LHCb::Event::Composites v2_particles{unique_id_gen, zip_id};
  v2_particles.emplace_back<SIMDWrapper::InstructionSet::Scalar, float_v, int_v, 0>(
      {0., 0., 0.}, {v1_p4.Px(), v1_p4.Py(), v1_p4.Pz(), v1_p4.E()}, 0, 0.f, 1, v2_posCovMatrix, v2_momCovMatrix,
      v2_posMomCovMatrix, {}, {}, {} );

  // Define functors
  EventContext const                       evtCtx;
  Functors::TopLevelInfo const             top_level;
  Functors::Particle::momCovMatrix         MOM_COV_MATRIX;
  Functors::Particle::posCovMatrix         POS_COV_MATRIX;
  Functors::Particle::threeMomCovMatrix    THREE_MOM_COV_MATRIX;
  Functors::Particle::momPosCovMatrix      MOM_POS_COV_MATRIX;
  Functors::Particle::threeMomPosCovMatrix THREE_MOM_POS_COV_MATRIX;
  Functors::Track::ThreeMomentum           THREEMOMENTUM;
  auto const                               P = chain( Functors::Common::Magnitude{}, THREEMOMENTUM );
  auto PERR2                                 = Functors::math::similarity( THREEMOMENTUM / P, THREE_MOM_COV_MATRIX );

  // Expected result
  auto mom_cov_matrix = LHCb::LinAlg::convert<float_v>( v1_momCovMatrix );
  auto pos_cov_matrix = LHCb::LinAlg::convert<float_v>( v1_posCovMatrix );
  auto three_mom_cov_matrix =
      LHCb::LinAlg::convert<float_v>( v1_momCovMatrix.template Sub<Gaudi::SymMatrix3x3>( 0, 0 ) );
  auto mom_pos_cov_matrix = LHCb::LinAlg::convert<float_v>( v1_posMomCovMatrix );
  auto three_mom_pos_cov_matrix =
      LHCb::LinAlg::convert<float_v>( v1_posMomCovMatrix.template Sub<Gaudi::Matrix3x3>( 0, 0 ) );
  LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3> three_mom{v1_p4.px(), v1_p4.py(), v1_p4.pz()};
  auto expected_similarity = similarity( three_mom / three_mom.mag(), three_mom_cov_matrix );

  // Checks
  // - v1 particle
  BOOST_CHECK_EQUAL( POS_COV_MATRIX( v1_particle )( 0, 0 ).cast(), pos_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( MOM_COV_MATRIX( v1_particle )( 0, 0 ).cast(), mom_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( THREE_MOM_COV_MATRIX( v1_particle )( 0, 0 ).cast(), three_mom_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( MOM_POS_COV_MATRIX( v1_particle )( 0, 0 ).cast(), mom_pos_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( THREE_MOM_POS_COV_MATRIX( v1_particle )( 0, 0 ).cast(), three_mom_pos_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( PERR2.prepare( evtCtx, top_level )( true, v1_particle ).cast(), expected_similarity.cast() );

  // - v2 particle
  auto v2_particle = v2_particles.scalar()[0];
  BOOST_CHECK_EQUAL( POS_COV_MATRIX( v2_particle )( 0, 0 ).cast(), pos_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( MOM_COV_MATRIX( v2_particle )( 0, 0 ).cast(), mom_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( THREE_MOM_COV_MATRIX( v2_particle )( 0, 0 ).cast(), three_mom_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( MOM_POS_COV_MATRIX( v2_particle )( 0, 0 ).cast(), mom_pos_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( THREE_MOM_POS_COV_MATRIX( v2_particle )( 0, 0 ).cast(), three_mom_pos_cov_matrix( 0, 0 ).cast() );
  BOOST_CHECK_EQUAL( PERR2.prepare( evtCtx, top_level )( true, v2_particle ).cast(), expected_similarity.cast() );
}

BOOST_AUTO_TEST_CASE( test_vertex_diff_functors ) {
  // difference in z position of endvertex of parent and composite child
  const double B0_vertex_z{5.0};
  const double delta_z{5.0};

  // B0 particle
  auto B0_part = LHCb::Particle{LHCb::ParticleID( 511 ), 0};
  // set endvertex of B0
  SmartRef<LHCb::Vertex> B0_endvertex = new LHCb::Vertex();
  B0_endvertex->setPosition( Gaudi::XYZPoint{0.0, 0.0, B0_vertex_z} );
  B0_part.setEndVertex( B0_endvertex );

  // Ds- particle with vertex and a basic K+
  auto Ds_part = LHCb::Particle( LHCb::ParticleID( -432 ), 1 );
  // set endvertex of Ds
  SmartRef<LHCb::Vertex> Ds_endvertex = new LHCb::Vertex();
  Ds_endvertex->setPosition( Gaudi::XYZPoint{0.0, 0.0, B0_vertex_z + delta_z} );
  Ds_part.setEndVertex( Ds_endvertex );
  auto K_part = LHCb::Particle( LHCb::ParticleID( 321 ), 2 );

  // set Ds+ and K- as daughters of B0
  SmartRefVector<LHCb::Particle> daugs;
  daugs.push_back( &Ds_part );
  daugs.push_back( &K_part );
  B0_part.setDaughters( daugs );

  // define EventContext and TopLevelInfo
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;

  const auto Ds_VZ             = Functors::Adapters::Child{1, END_VZ};
  const auto Ds_VZ_prepared    = Ds_VZ.prepare( evtCtx, top_level );
  auto const DELTA_VZ          = Ds_VZ - END_VZ;
  const auto DELTA_VZ_prepared = DELTA_VZ.prepare( evtCtx, top_level );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( END_VZ.prepare( evtCtx, top_level )( true, B0_part ) ), B0_vertex_z );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( Ds_VZ_prepared( true, B0_part ) ), B0_vertex_z + delta_z );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( DELTA_VZ_prepared( true, B0_part ) ), delta_z );
}

BOOST_AUTO_TEST_CASE( test_decaytime_functor ) {
  auto part = LHCb::Particle{LHCb::ParticleID( 511 ), 0};

  // set endvertex of B0
  auto decayvertex = LHCb::Vertex{};
  decayvertex.setPosition( Gaudi::XYZPoint{0.0, 0.0, 5.0} );
  part.setEndVertex( &decayvertex );

  const double pz = 1e5;
  const double m  = 5e3;
  part.setMomentum( Gaudi::LorentzVector{0, 0, pz, sqrt( pz * pz + m * m )} );
  auto momcov    = part.momCovMatrix();
  momcov( 0, 0 ) = momcov( 1, 1 ) = momcov( 2, 2 ) = momcov( 2, 2 ) = 1;
  part.setMomCovMatrix( momcov );

  auto poscov    = decayvertex.covMatrix();
  poscov( 0, 0 ) = poscov( 1, 1 ) = poscov( 2, 2 ) = 1;
  decayvertex.setCovMatrix( poscov );
  part.setPosCovMatrix( poscov );

  std::vector<LHCb::Vertex> vertices{};
  auto&                     pv = vertices.emplace_back();
  pv.setCovMatrix( poscov );

  Functors::Composite::Lifetime l{};
  /*auto const lifetime =*/l( pv, part );
}

BOOST_AUTO_TEST_CASE( test_mc_prompt ) {
  // Prepare functors
  using MC_FIRST_LONGLIVED_ANCESTOR = Functors::Simulation::MC::FirstLongLivedAncestor;
  using HAS_VALUE                   = Functors::Functional::HasValue;
  using VALUE_OR                    = Functors::Functional::ValueOr<int>;
  using PARTICLE_ID                 = Functors::Simulation::Particle_Id;
  using OBJECT_KEY                  = Functors::TES::ObjectKey;

  // Sice the F.MC_FIRST_LONGLIVED_ANCESTOR needs ParticlePropertySvc from Gaudi Application, it can not be truely
  // tested, here we just check that it at least compiles.
  const auto MC_ISPROMPT      = chain( HAS_VALUE{}, MC_FIRST_LONGLIVED_ANCESTOR{} );
  const auto MC_LONGLIVED_ID  = chain( VALUE_OR{0}, PARTICLE_ID{}, MC_FIRST_LONGLIVED_ANCESTOR{} );
  const auto MC_LONGLIVED_KEY = chain( VALUE_OR{-1}, OBJECT_KEY{}, MC_FIRST_LONGLIVED_ANCESTOR{} );

  // Test the F.HAS_VALUE
  Functors::Optional<int> valid_optional{0}, invalid_optional{std::nullopt};
  BOOST_CHECK_EQUAL( HAS_VALUE{}( valid_optional ), true );
  BOOST_CHECK_EQUAL( HAS_VALUE{}( invalid_optional ), false );
}

BOOST_AUTO_TEST_CASE( test_mcorr_functors ) {
  auto B0_parr     = LHCb::Particle{LHCb::ParticleID( 511 ), 0};
  auto decayvertex = LHCb::Vertex{};
  decayvertex.setPosition( Gaudi::XYZPoint{0.0, 0.0, 5.0} );
  B0_parr.setEndVertex( &decayvertex );

  auto B0_perp = LHCb::Particle{LHCb::ParticleID( 511 ), 0};
  B0_perp.setEndVertex( &decayvertex );

  const double pz = 1.e5;
  const double m  = 5.e3;
  const double py = 2.5e3;
  B0_parr.setMomentum( Gaudi::LorentzVector{0, 0, pz, sqrt( pz * pz + m * m )} );
  B0_perp.setMomentum( Gaudi::LorentzVector{0, py, 0, sqrt( py * py )} );
  auto momcov    = B0_parr.momCovMatrix();
  momcov( 0, 0 ) = momcov( 1, 1 ) = momcov( 2, 2 ) = momcov( 2, 2 ) = 0.;
  B0_parr.setMomCovMatrix( momcov );
  B0_perp.setMomCovMatrix( momcov );

  auto poscov    = decayvertex.covMatrix();
  poscov( 0, 0 ) = poscov( 1, 1 ) = poscov( 2, 2 ) = 1.;
  decayvertex.setCovMatrix( poscov );
  B0_parr.setPosCovMatrix( poscov );
  B0_perp.setPosCovMatrix( poscov );

  std::vector<LHCb::Vertex> vertices{};
  auto                      pv = LHCb::Vertex{};
  pv.setPosition( Gaudi::XYZPoint{0.0, 0.0, 0.0} );
  pv.setCovMatrix( poscov );

  Functors::Composite::CorrectedMass      mc;
  auto const                              mcorr_parr = mc( pv, B0_parr );
  auto const                              mcorr_perp = mc( pv, B0_perp );
  Functors::Composite::CorrectedMassError mce;
  auto const                              mcorrerr_perp = mce( pv, B0_perp );

  const double bmass     = 5.e3;
  const double truemcorr = 0.;

  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( mcorr_parr ), bmass );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( mcorr_perp ), bmass );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( mcorrerr_perp ), truemcorr );
}

BOOST_AUTO_TEST_CASE( test_mc_header ) {

  LHCb::MCHeader mc_header;

  LHCb::MCVertices mc_vertices;
  mc_vertices.insert( new LHCb::MCVertex{} );
  mc_vertices.insert( new LHCb::MCVertex{} );
  mc_vertices.insert( new LHCb::MCVertex{} );

  mc_header.setEvtNumber( 123 );
  mc_header.setRunNumber( 111 );
  mc_header.setEvtTime( 321 );
  mc_header.addToPrimaryVertices( mc_vertices( 0 ) );
  mc_header.addToPrimaryVertices( mc_vertices( 1 ) );
  mc_header.addToPrimaryVertices( mc_vertices( 2 ) );

  const auto MCHEADER_ALLPVS    = Functors::Simulation::MCHeader::AllPVs{};
  const auto MCHEADER_EVTTIME   = Functors::Simulation::MCHeader::EvtTime{};
  const auto MCHEADER_EVTNUMBER = Functors::Simulation::MCHeader::EvtNumber{};
  const auto MCHEADER_RUNNUMBER = Functors::Simulation::MCHeader::RunNumber{};

  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( MCHEADER_ALLPVS( mc_header ).size() ), 3 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( MCHEADER_EVTTIME( mc_header ) ), 321 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( MCHEADER_EVTNUMBER( mc_header ) ), 123 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( MCHEADER_RUNNUMBER( mc_header ) ), 111 );
}

BOOST_AUTO_TEST_CASE( test_mc_functors ) {

  // define container for mc particles and mc vertices
  auto mc_particles = LHCb::MCParticles{};
  auto mc_vertices  = LHCb::MCVertices{};

  // define properties for an mcp
  const auto pid               = LHCb::ParticleID{211};
  const auto electron          = LHCb::ParticleID{11};
  const auto muon              = LHCb::ParticleID{12};
  const auto mother_key        = 1;
  const auto gd_mother_key     = 2;
  const auto child_key         = 42;
  const auto childs_child_key  = 420;
  const auto child_momentum    = Gaudi::LorentzVector{3.0, 6.0, 9.0, 12.0};
  const auto mother_momentum   = Gaudi::LorentzVector{1.0, 2.0, 4.0, 16.0};
  const auto gm_momentum       = Gaudi::LorentzVector{0.5, 1.0, 2.0, 20.0};
  const auto origin_vertex_pos = Gaudi::XYZPoint{1., 2., 3.};
  const auto pp_pos            = Gaudi::XYZPoint{0., 0., 0.};
  const auto lifetime_mother   = ( mother_momentum.M() * ( origin_vertex_pos - pp_pos ).Dot( mother_momentum.Vect() ) /
                                 mother_momentum.Vect().mag2() ) /
                               Gaudi::Units::c_light;
  const auto vertex_time = 0.;

  // insert an mcp and an mc vertex in containers
  mc_particles.insert( new LHCb::MCParticle{}, mother_key );
  mc_particles.insert( new LHCb::MCParticle{}, gd_mother_key );
  mc_particles.insert( new LHCb::MCParticle{}, child_key );
  mc_particles.insert( new LHCb::MCParticle{}, childs_child_key );
  mc_vertices.insert( new LHCb::MCVertex{} );
  mc_vertices.insert( new LHCb::MCVertex{} );

  // assign mother to vertex
  mc_vertices( 0 )
      ->setMother( mc_particles( mother_key ) )
      .addToProducts( mc_particles( child_key ) )
      .setPosition( origin_vertex_pos )
      .setType( LHCb::MCVertex::DecayVertex );

  mc_vertices( 1 )
      ->setMother( mc_particles( gd_mother_key ) )
      .setPosition( pp_pos )
      .setTime( vertex_time )
      .setType( LHCb::MCVertex::ppCollision );

  // set child's origin vertex and ID
  auto& mcp = *mc_particles( child_key );
  mcp.setOriginVertex( mc_vertices( 0 ) ).setParticleID( pid ).setMomentum( child_momentum );

  // set mother's origin vertex and ID
  auto& momcp = *mc_particles( mother_key );
  momcp.setOriginVertex( mc_vertices( 1 ) )
      .addToEndVertices( mc_vertices( 0 ) )
      .setParticleID( muon )
      .setMomentum( mother_momentum );

  auto& gdmomcp = *mc_particles( gd_mother_key );
  gdmomcp.setParticleID( electron ).setMomentum( gm_momentum );

  // get the functors to retrieve various information
  const EventContext           evtCtx;
  const Functors::TopLevelInfo top_level;
  const auto                   PARTICLE_ID   = Functors::Simulation::Particle_Id{};
  const auto                   OBJECT_KEY    = Functors::TES::ObjectKey{};
  const auto                   MOTHER        = Functors::Simulation::MC::Mother{};
  const auto                   LIFETIME      = Functors::Simulation::MC::LifeTime{};
  const auto                   P4            = Functors::Track::FourMomentum{};
  const auto                   GM_P4         = chain( Functors::Track::FourMomentum{}, MOTHER, MOTHER );
  const auto                   GM_MRF        = bind( Functors::LHCbMath::boost_to{}, GM_P4, chain( P4, MOTHER ) );
  const auto                   CHILD_MRF     = bind( Functors::LHCbMath::boost_to{}, P4, chain( P4, MOTHER ) );
  const auto                   CHILD_MRF_REV = bind( Functors::LHCbMath::boost_from{}, CHILD_MRF, chain( P4, MOTHER ) );
  const auto                   HELICITY_ANGLE = bind( Functors::LHCbMath::normed_dot_3dim{}, GM_MRF, CHILD_MRF );
  const auto                   VTX_TIME       = Functors::Simulation::MCVertex::Time{};
  const auto                   VTX_TYPE       = Functors::Simulation::MCVertex::Type{};
  const auto                   VTX_ISPRIMARY  = Functors::Simulation::MCVertex::IsPrimary{};
  const auto                   VTX_ISDECAY    = Functors::Simulation::MCVertex::IsDecay{};
  const auto                   VTX_PRODUCTS   = Functors::Simulation::MCVertex::Products{};
  auto const                   PX = chain( Functors::Common::X_Coordinate{}, Functors::Track::ThreeMomentum{} );
  auto const                   PY = chain( Functors::Common::Y_Coordinate{}, Functors::Track::ThreeMomentum{} );
  auto const                   PZ = chain( Functors::Common::Z_Coordinate{}, Functors::Track::ThreeMomentum{} );
  auto const                   E  = chain( Functors::Common::E_Coordinate{}, Functors::Track::FourMomentum{} );
  auto const                   P  = chain( Functors::Common::Magnitude{}, Functors::Track::ThreeMomentum{} );
  const auto                   TRUEORIGINVERTEX_X =
      chain( Functors::Common::X_Coordinate{}, Functors::Common::Position{}, Functors::Simulation::MC::OriginVertex{} );
  const auto TRUEORIGINVERTEX_Y =
      chain( Functors::Common::Y_Coordinate{}, Functors::Common::Position{}, Functors::Simulation::MC::OriginVertex{} );
  const auto TRUEORIGINVERTEX_Z =
      chain( Functors::Common::Z_Coordinate{}, Functors::Common::Position{}, Functors::Simulation::MC::OriginVertex{} );
  const auto TRUEPV_X        = chain( Functors::Common::X_Coordinate{}, Functors::Common::Position{},
                               Functors::Simulation::MC::PrimaryVertex{} );
  const auto TRUEPV_Y        = chain( Functors::Common::Y_Coordinate{}, Functors::Common::Position{},
                               Functors::Simulation::MC::PrimaryVertex{} );
  const auto TRUEPV_Z        = chain( Functors::Common::Z_Coordinate{}, Functors::Common::Position{},
                               Functors::Simulation::MC::PrimaryVertex{} );
  const auto TRUEENDVERTEX_X = END_VX.prepare( evtCtx, top_level );
  const auto TRUEENDVERTEX_Y = END_VY.prepare( evtCtx, top_level );
  const auto TRUEENDVERTEX_Z = END_VZ.prepare( evtCtx, top_level );

  const auto MC_MOTHER_KEY    = chain( OBJECT_KEY, MOTHER );
  const auto MC_MOTHER_ID     = chain( PARTICLE_ID, MOTHER );
  const auto MC_GD_MOTHER_KEY = chain( OBJECT_KEY, MOTHER, MOTHER );
  const auto MC_GD_MOTHER_ID  = chain( PARTICLE_ID, MOTHER, MOTHER );
  const auto MC_MOTHER_P      = chain( P, MOTHER );
  // these should return invalid because there is no third generation mother defined here
  const auto MC_GD_GD_MOTHER_KEY = chain( OBJECT_KEY, MOTHER, MOTHER, MOTHER );
  const auto MC_GD_GD_MOTHER_ID  = chain( PARTICLE_ID, MOTHER, MOTHER, MOTHER );
  const auto MC_GD_GD_MOTHER_P   = chain( P, MOTHER, MOTHER, MOTHER );

  const auto ref_helicity_angle = []( const ROOT::Math::PxPyPzEVector& B, const ROOT::Math::PxPyPzEVector& X,
                                      const ROOT::Math::PxPyPzEVector& h1 ) {
    auto boost_beta  = -X.Vect() / X.t();
    auto grandparent = ROOT::Math::VectorUtil::boost( B, boost_beta );
    auto daughter    = ROOT::Math::VectorUtil::boost( h1, boost_beta );
    return ROOT::Math::VectorUtil::CosTheta( daughter.Vect(), grandparent.Vect() );
  }( gm_momentum, mother_momentum, child_momentum );

  // conduct checks
  BOOST_CHECK_EQUAL( PARTICLE_ID( mcp ), mcp.particleID().pid() );
  BOOST_CHECK_EQUAL( OBJECT_KEY( mcp ), mcp.key() );

  auto prepared_call = []( auto& functor, auto const& input ) {
    return functor.prepare( EventContext{}, Functors::TopLevelInfo{} )( true, input );
  };

  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( LIFETIME( momcp ).value_or( -1. ) ), lifetime_mother );

  float arr_boosted_child_momentum_ref[4] = {0};
  float arr_boosted_child_momentum[4]     = {-1};
  ROOT::Math::VectorUtil::boost( child_momentum, mother_momentum.BoostToCM() )
      .GetCoordinates( arr_boosted_child_momentum_ref );
  LHCb::LinAlg::convert( prepared_call( CHILD_MRF, mcp )
                             .value_or( LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>( -1.f, -1.f, -1.f, -1.f ) ) )
      .GetCoordinates( arr_boosted_child_momentum );
  LHCb::Utils::unwind<0, 4>( [&]( const unsigned i ) {
    BOOST_CHECK_CLOSE( arr_boosted_child_momentum_ref[i], arr_boosted_child_momentum[i], 1e-4 );
  } );

  float arr_child_momentum_ref[4] = {0};
  float arr_child_momentum[4]     = {-1};
  child_momentum.GetCoordinates( arr_child_momentum_ref );
  LHCb::LinAlg::convert( prepared_call( CHILD_MRF_REV, mcp )
                             .value_or( LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 4>( -1.f, -1.f, -1.f, -1.f ) ) )
      .GetCoordinates( arr_child_momentum );
  LHCb::Utils::unwind<0, 4>(
      [&]( const unsigned i ) { BOOST_CHECK_CLOSE( arr_child_momentum_ref[i], arr_child_momentum[i], 1e-4 ); } );

  BOOST_CHECK_CLOSE( LHCb::Utils::as_arithmetic( prepared_call( HELICITY_ANGLE, mcp ).value() ), ref_helicity_angle,
                     1e-4 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VTX_TIME( mc_vertices( 1 ) ).value() ), vertex_time );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VTX_TYPE( mc_vertices( 0 ) ).value() ), LHCb::MCVertex::DecayVertex );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VTX_TYPE( mc_vertices( 1 ) ).value() ), LHCb::MCVertex::ppCollision );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VTX_ISPRIMARY( mc_vertices( 1 ) ).value() ), 1 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VTX_ISDECAY( mc_vertices( 1 ) ).value() ), 0 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VTX_PRODUCTS( mc_vertices( 0 ) ).value().size() ), 1 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( PX, mcp ) ), child_momentum.Px() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( PY, mcp ) ), child_momentum.Py() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( PZ, mcp ) ), child_momentum.Pz() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( E, mcp ) ), child_momentum.energy() );
  BOOST_CHECK_CLOSE( LHCb::Utils::as_arithmetic( prepared_call( P, mcp ) ), child_momentum.P(), 0.0001 );
  BOOST_CHECK_CLOSE( LHCb::Utils::as_arithmetic( prepared_call( PT, mcp ) ), child_momentum.Pt(), 0.0001 );

  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( TRUEORIGINVERTEX_X, mcp ) ), origin_vertex_pos.X() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( TRUEORIGINVERTEX_Y, mcp ) ), origin_vertex_pos.Y() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( TRUEORIGINVERTEX_Z, mcp ) ), origin_vertex_pos.Z() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( TRUEENDVERTEX_X( true, momcp ) ), origin_vertex_pos.X() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( TRUEENDVERTEX_Y( true, momcp ) ), origin_vertex_pos.Y() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( TRUEENDVERTEX_Z( true, momcp ) ), origin_vertex_pos.Z() );

  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( TRUEPV_X, mcp ) ), pp_pos.X() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( TRUEPV_Y, mcp ) ), pp_pos.Y() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( TRUEPV_Z, mcp ) ), pp_pos.Z() );

  BOOST_CHECK_EQUAL( Functors::Track::ReferencePoint{}( momcp ).x().cast(), 0 );
  BOOST_CHECK_EQUAL( Functors::Track::ReferencePoint{}( momcp ).y().cast(), 0 );
  BOOST_CHECK_EQUAL( Functors::Track::ReferencePoint{}( momcp ).z().cast(), 0 );

  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_MOTHER_KEY, mcp ).value_or( -1 ) ), momcp.key() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_MOTHER_ID, mcp ).value_or( 0 ) ), muon.pid() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_GD_MOTHER_ID, mcp ).value_or( 0 ) ),
                     electron.pid() );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_GD_MOTHER_KEY, mcp ).value_or( -1 ) ),
                     gdmomcp.key() );
  BOOST_CHECK_CLOSE( LHCb::Utils::as_arithmetic( prepared_call( MC_MOTHER_P, mcp ).value_or( 0. ) ),
                     mother_momentum.P(), 0.0001 );
  // these check for invalid values, in case of no specific value implemented std::nullopt is returned
  // which is checked by returning 42 in that case
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_GD_GD_MOTHER_KEY, mcp ).value_or( 123 ) ), 123 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_GD_GD_MOTHER_ID, mcp ).value_or( 42 ) ), 42 );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( prepared_call( MC_GD_GD_MOTHER_P, mcp ).value_or( 42 ) ), 42 );
}

BOOST_AUTO_TEST_CASE( test_simd_type_compare ) {
  // see https://gitlab.cern.ch/lhcb/Rec/-/issues/290 This is a "as simple as
  // possible" test to showcase the behaviour of comparisons of simd types

  using simd_t = SIMDWrapper::sse::types; // vector width 4
  std::array<float, 4> tmp{0, 1, 2, 3};
  auto                 x1 = simd_t::float_v{tmp};
  tmp                     = {3, 2, 1, 0};
  auto x2                 = simd_t::float_v{tmp};

  // mask is sse{0011}
  auto mask_x = x1 > x2;

  // let's be paranoid and test our mask
  BOOST_CHECK_EQUAL( testbit( mask_x, 0 ), false );
  BOOST_CHECK_EQUAL( testbit( mask_x, 1 ), false );
  BOOST_CHECK_EQUAL( testbit( mask_x, 2 ), true );
  BOOST_CHECK_EQUAL( testbit( mask_x, 3 ), true );

  // functors which simply return the value they are given
  auto f1 = Functors::NumericValue{x1};
  auto f2 = Functors::NumericValue{x2};

  // we can skip prepare because the functor doesn't need it
  auto y1 = f1();
  auto y2 = f2();
  // again some gymnastics because boost doesn't like SIMDWrapper
  BOOST_CHECK( all( abs( x1 - y1 ) < std::numeric_limits<float>::epsilon() ) );
  BOOST_CHECK( all( abs( x2 - y2 ) < std::numeric_limits<float>::epsilon() ) );

  auto composed = f1 > f2;
  // composed functors need a prepare even though we know the underlying
  // functors don't. But a dummy EventContext and top_level are enough
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         comp_prepared = composed.prepare( evtCtx, top_level );
  // composed functors also need a mask to know which elements to evaluate.
  auto in_mask = simd_t::mask_true();
  auto mask_f  = comp_prepared( in_mask );

  // we should get a sse4::mask_v type  that has size == 4
  BOOST_CHECK_EQUAL( mask_f.size(), 4 );
  // mask_v doesn't have a operator== and we don't want to risk any type
  // promotion so we manually check similar to above that the mask is 0011
  BOOST_CHECK_EQUAL( testbit( mask_f, 0 ), false );
  BOOST_CHECK_EQUAL( testbit( mask_f, 1 ), false );
  BOOST_CHECK_EQUAL( testbit( mask_f, 2 ), true );
  BOOST_CHECK_EQUAL( testbit( mask_f, 3 ), true );
}

BOOST_AUTO_TEST_CASE( test_tes_func ) {
  // make few properties of odin
  using BXTypes                               = LHCb::ODIN::BXTypes;
  std::uint32_t                fake_runNum    = 10;
  std::uint64_t                fake_evtNum    = 50;
  std::uint16_t                fake_evtType   = 0x0002; // Physics
  std::uint16_t                fake_bunchId   = 1;
  BXTypes                      fake_bunchType = BXTypes::BeamCrossing; // BeamCrossing
  std::uint32_t                fake_odinTck   = 5;
  std::uint64_t                fake_gpsTime   = 1000;
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  // make odin
  LHCb::ODIN odin{};
  odin.setRunNumber( fake_runNum );
  odin.setEventNumber( fake_evtNum );
  odin.setEventType( fake_evtType );
  odin.setBunchId( fake_bunchId );
  odin.setBunchCrossingType( fake_bunchType );
  odin.setTriggerConfigurationKey( fake_odinTck );
  odin.setGpsTime( fake_gpsTime );

  // make few properties of selection lines
  unsigned int line1_dec = 0;
  unsigned int line2_dec = 1;
  unsigned int hlt_tck   = 22;
  // create selection (Hlt1,Hlt2,Spruce) decreports
  LHCb::HltDecReports            sel_dec;
  LHCb::HltDecReports::Container map;
  map.insert( std::make_pair( "Line1Decision", LHCb::HltDecReport{line1_dec} ) );
  map.insert( std::make_pair( "Line2Decision", LHCb::HltDecReport{line2_dec} ) );
  sel_dec.setDecReports( map );
  sel_dec.setConfiguredTCK( hlt_tck );

  // test operator() of Functors::details::OdinInfo
  Functors::TES::RunNumber         op_RUNNUMBER;
  Functors::TES::EventNumber       op_EVENTNUMBER;
  Functors::TES::EventType         op_EVENTTYPE;
  Functors::TES::BunchCrossingID   op_BUNCHROSSING_ID;
  Functors::TES::BunchCrossingType op_BUNCHROSSING_TYPE;
  Functors::TES::OdinTCK           op_ODINTCK;
  Functors::TES::GpsTime           op_GPSTIME;
  // conduct checks
  BOOST_CHECK_EQUAL( op_RUNNUMBER( odin ), fake_runNum );
  BOOST_CHECK_EQUAL( op_EVENTNUMBER( odin ), fake_evtNum );
  BOOST_CHECK_EQUAL( op_EVENTTYPE( odin ), fake_evtType );
  BOOST_CHECK_EQUAL( op_BUNCHROSSING_ID( odin ), fake_bunchId );
  BOOST_CHECK_EQUAL( op_BUNCHROSSING_TYPE( odin ), fake_bunchType );
  BOOST_CHECK_EQUAL( op_ODINTCK( odin ), fake_odinTck );
  BOOST_CHECK_EQUAL( op_GPSTIME( odin ), fake_gpsTime );

  // test decreport functions taking dummy location
  std::vector<std::string> tup = {"Line1Decision", "Line2Decision", "Line3Decision"};
  auto                     op_DECISION_1 =
      Functors::chain( Functors::Functional::ValueOr{false}, Functors::TES::SelectionDecision{tup[0]} );
  auto op_DECISION_2 =
      Functors::chain( Functors::Functional::ValueOr{false}, Functors::TES::SelectionDecision{tup[1]} );
  auto op_DECISION_3 =
      Functors::chain( Functors::Functional::ValueOr{false}, Functors::TES::SelectionDecision{tup[2]} );
  auto op_DECISION_4 =
      Functors::chain( Functors::Functional::ValueOr{false},
                       ( Functors::TES::SelectionDecision{tup[0]} ) & ( Functors::TES::SelectionDecision{tup[2]} ) );
  auto op_DECISION_5 =
      Functors::chain( Functors::Functional::ValueOr{false},
                       ( Functors::TES::SelectionDecision{tup[0]} ) | ( Functors::TES::SelectionDecision{tup[2]} ) );
  auto op_DECISION_6 =
      Functors::chain( Functors::Functional::ValueOr{false},
                       ( Functors::TES::SelectionDecision{tup[0]} ) | ( Functors::TES::SelectionDecision{tup[1]} ) );
  Functors::TES::SelectionTCK op_TCK{};
  // checks
  BOOST_CHECK_EQUAL( op_TCK( sel_dec ), hlt_tck );
  BOOST_CHECK_EQUAL( op_DECISION_1.prepare( evtCtx, top_level )( true, sel_dec ), static_cast<bool>( line1_dec ) );
  BOOST_CHECK_EQUAL( op_DECISION_2.prepare( evtCtx, top_level )( true, sel_dec ), static_cast<bool>( line2_dec ) );
  BOOST_CHECK_EQUAL( op_DECISION_3.prepare( evtCtx, top_level )( true, sel_dec ), false );
  BOOST_CHECK_EQUAL( op_DECISION_4.prepare( evtCtx, top_level )( true, sel_dec ), false );
  BOOST_CHECK_EQUAL( op_DECISION_5.prepare( evtCtx, top_level )( true, sel_dec ), false );
  BOOST_CHECK_EQUAL( op_DECISION_6.prepare( evtCtx, top_level )( true, sel_dec ),
                     static_cast<bool>( line1_dec ) || static_cast<bool>( line2_dec ) );

  // test operator() of Functors::TES::SelectionDecision
  auto const op_DECISION_FILTER12 =
      Functors::TES::DecReportsFilter( std::vector<std::string>{"Line1Decision", "Line2Decision"} );
  auto const op_DECISION_FILTER1 = Functors::TES::DecReportsFilter( std::vector<std::string>{"Line1Decision"} );
  auto const op_DECISION_FILTER2 = Functors::TES::DecReportsFilter( std::vector<std::string>{"Line2Decision"} );
  BOOST_CHECK_EQUAL( op_DECISION_FILTER12( sel_dec ), true );
  BOOST_CHECK_EQUAL( op_DECISION_FILTER1( sel_dec ), false );
  BOOST_CHECK_EQUAL( op_DECISION_FILTER2( sel_dec ), true );

  // test operator() of Functors::TES::SelectionDecision
  auto const op_RE_DECISION_FILTER12 = Functors::TES::DecReportsRegExFilter( "^Line.Decision$" );
  auto const op_RE_DECISION_FILTER1  = Functors::TES::DecReportsRegExFilter( ".*1Decision$" );
  auto const op_RE_DECISION_FILTER2  = Functors::TES::DecReportsRegExFilter( ".*2Decision$" );
  BOOST_CHECK_EQUAL( op_RE_DECISION_FILTER12( sel_dec ), true );
  BOOST_CHECK_EQUAL( op_RE_DECISION_FILTER1( sel_dec ), false );
  BOOST_CHECK_EQUAL( op_RE_DECISION_FILTER2( sel_dec ), true );

  // test operator() of Functors::TES::SizeOf
  Functors::detail::SizeOf op_size;
  BOOST_CHECK_EQUAL( op_size( tup ), tup.size() );
}

BOOST_AUTO_TEST_CASE( test_functional_functors ) {
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;

  auto pvs = LHCb::Event::PV::PrimaryVertices{};

  pvs.emplace_back( Gaudi::XYZPoint( 1, 2, 3 ) );
  pvs.emplace_back( Gaudi::XYZPoint( 3, 2, 1 ) );

  auto x_pos = chain( Functors::Common::X_Coordinate{}, Functors::Common::Position{} );

  auto mapped_x_pos = Functors::Functional::Map( x_pos );

  auto prepped = mapped_x_pos.prepare( evtCtx, top_level );

  auto x = prepped( true, pvs );

  BOOST_CHECK_EQUAL( Functors::Functional::Front{}( x ), 1 );

  BOOST_CHECK_EQUAL( Functors::Functional::Back{}( x ), 3 );

  // Let's do a check of a mapped IP functor
  auto f_ip = Functors::Common::ImpactParameter{};

  auto mapped_ip   = Functors::Functional::Map( f_ip );
  auto p_mapped_ip = mapped_ip.prepare( evtCtx, top_level );

  // this represents the trajectory
  DummyState state( {0, 0, 0}, {1, 1, 1} );

  // position of pvs
  std::vector<LHCb::LinAlg::Vec<SIMDWrapper::scalar::float_v, 3>> positions{};
  positions.emplace_back( 1.f, 1.f, 0.f ); // ip = 0.816497
  positions.emplace_back( 1.f, 1.f, 1.f ); // ip = 0

  auto tmp = p_mapped_ip( true, positions, state );

  auto first_ip = tmp[0].cast();

  BOOST_CHECK_EQUAL( tmp.size(), 2 );
  //
  BOOST_CHECK( ( first_ip > 0.81649 && first_ip < 0.81650 ) );
  BOOST_CHECK_EQUAL( tmp[1].cast(), 0 );

  auto min = Functors::Functional::Min{};
  auto max = Functors::Functional::Max{};

  std::vector<float> vec{1.1, 2.2, 3.3, 1.5, 0.9, 1.0};

  float output_min = min( vec );
  float output_max = max( vec );

  float desired_min = 0.9;
  float desired_max = 3.3;

  BOOST_CHECK_EQUAL( output_min, desired_min );
  BOOST_CHECK_EQUAL( output_max, desired_max );
}

BOOST_AUTO_TEST_CASE( test_ft_functors ) {

  // define three different B particles
  LHCb::Particle particleA( LHCb::ParticleID( 511 ), 0 );  // B0
  LHCb::Particle particleB( LHCb::ParticleID( -511 ), 0 ); // B0bar
  LHCb::Particle particleC( LHCb::ParticleID( 521 ), 0 );  // B+

  Gaudi::LorentzVector momentumA( 1000, 1000, 9000, 10530 );
  Gaudi::XYZPoint      referencePointA( 0.75, 0.07, 25 );
  particleA.setMomentum( momentumA );
  particleA.setReferencePoint( referencePointA );

  Gaudi::LorentzVector momentumB( 2000, 2000, 15000, 16151.5 );
  Gaudi::XYZPoint      referencePointB( 0.82, 0.15, -4 );
  particleB.setMomentum( momentumB );
  particleB.setReferencePoint( referencePointB );

  // define tagger type for test (str and int)
  std::vector<std::string> inputtype_str   = {"Run2_SSPion", "Run2_OSKaon",   "Run2_OSMuon",        "Run2_OSElectron",
                                            "Run2_SSKaon", "Run2_SSProton", "Run2_OSVertexCharge"};
  int                      inputtype_int[] = {2, 5, 6, 7, 3, 4, 8};
  auto                     inputtype_type =
      std::array{LHCb::Tagger::TaggerType::Run2_SSPion,        LHCb::Tagger::TaggerType::Run2_OSKaon,
                 LHCb::Tagger::TaggerType::Run2_OSMuon,        LHCb::Tagger::TaggerType::Run2_OSElectron,
                 LHCb::Tagger::TaggerType::Run2_SSKaon,        LHCb::Tagger::TaggerType::Run2_SSProton,
                 LHCb::Tagger::TaggerType::Run2_OSVertexCharge};

  for ( unsigned int i = 0; i < inputtype_str.size(); i++ ) {
    // define two Flavour Tag, one each for the A & B, none for C
    LHCb::Tagger TaggerA;
    TaggerA.setType( inputtype_type[i] );
    TaggerA.setDecision( LHCb::FlavourTag::TagResult::b );
    TaggerA.setOmega( 0.2 );

    LHCb::FlavourTag* flavourTagA = new LHCb::FlavourTag();
    flavourTagA->setTaggedB( &particleA );
    flavourTagA->addTagger( TaggerA );

    auto TaggerB =
        LHCb::Tagger{}.setType( inputtype_type[i] ).setDecision( LHCb::FlavourTag::TagResult::bbar ).setOmega( 0.15 );

    LHCb::FlavourTag* flavourTagB = new LHCb::FlavourTag;
    flavourTagB->setTaggedB( &particleB );
    flavourTagB->addTagger( TaggerB );

    LHCb::FlavourTags allTagger;
    LHCb::FlavourTags TagAOnly;
    LHCb::FlavourTags TagBOnly;

    allTagger.insert( flavourTagA );
    allTagger.insert( flavourTagB );
    TagAOnly.insert( flavourTagA );
    TagBOnly.insert( flavourTagB );

    const auto TaggingMistag_str   = Functors::Composite::TaggingMistag{inputtype_str[i]};
    const auto TaggingDecision_str = Functors::Composite::TaggingDecision{inputtype_str[i]};
    const auto TaggingMistag_int   = Functors::Composite::TaggingMistag{inputtype_int[i]};
    const auto TaggingDecision_int = Functors::Composite::TaggingDecision{inputtype_int[i]};

    const auto defaultOmega    = LHCb::FlavourTag{}.omega();
    const auto defaultDecision = LHCb::FlavourTag{}.decision();

    // check int input and str input return the expected misTag rate
    BOOST_CHECK_EQUAL( TaggingMistag_str( allTagger, particleA ), TaggingMistag_int( allTagger, particleA ) );

    // check A and B return their expected misTag rate
    BOOST_CHECK_EQUAL( TaggingMistag_str( allTagger, particleA ), TaggerA.omega() );
    BOOST_CHECK_EQUAL( TaggingMistag_str( allTagger, particleB ), TaggerB.omega() );

    // check A and B returns default misTag rate when their taggers are not in the KeyedContainer
    BOOST_CHECK_EQUAL( TaggingMistag_str( TagBOnly, particleA ), defaultOmega );
    BOOST_CHECK_EQUAL( TaggingMistag_str( TagAOnly, particleB ), defaultOmega );

    // check int input and str input return the expected misTag rate
    BOOST_CHECK_EQUAL( TaggingDecision_str( allTagger, particleA ), TaggingDecision_int( allTagger, particleA ) );

    // check A and B return their expected decision
    BOOST_CHECK_EQUAL( TaggingDecision_str( allTagger, particleA ), TaggerA.decision() );
    BOOST_CHECK_EQUAL( TaggingDecision_str( allTagger, particleB ), TaggerB.decision() );

    // check A and B returns default decision when their taggers are not in the KeyedContainer
    BOOST_CHECK_EQUAL( TaggingDecision_str( TagBOnly, particleA ), defaultDecision );
    BOOST_CHECK_EQUAL( TaggingDecision_str( TagAOnly, particleB ), defaultDecision );

    delete flavourTagA;
    delete flavourTagB;
  }
}

BOOST_AUTO_TEST_CASE( check_binary_functors ) {
  float fake_delta_eta     = 2.f;
  float fake_delta_phi     = 2.f;
  float fake_delta_r2      = 4.f;
  float fake_delta_r       = 2.f;
  float fake_comb_mass_min = 9000.f;
  float fake_comb_mass_max = 10000.f;
  // float fake_vtx_fit_chi2       = 15.f;
  float fake_ref_part_pv_ipchi2 = 0.f;

  EventContext const     evtCtx;
  Functors::TopLevelInfo top_level;

  auto pvs = LHCb::Event::PV::PrimaryVertices{};
  pvs.emplace_back( Gaudi::XYZPoint( 1, 1, 1 ) );
  auto parts  = make_parts();
  auto tracks = make_tracks();

  auto eta = chain( Functors::Common::Eta_Coordinate{}, Functors::Track::Slopes{} );
  auto phi = chain( Functors::Common::Phi_Coordinate{}, Functors::Track::Slopes{} );

  auto eta0 = chain( eta, Functors::Common::ForwardArgs<0>{} );
  auto eta1 = chain( eta, Functors::Common::ForwardArgs<1>{} );

  auto phi0 = chain( phi, Functors::Common::ForwardArgs<0>{} );
  auto phi1 = chain( phi, Functors::Common::ForwardArgs<1>{} );

  auto diff_eta = eta0 - eta1;
  auto diff_phi = phi0 - phi1;
  auto dphi_adj = chain( Functors::Common::AdjustAngle{}, diff_phi );

  auto deta_p = diff_eta.prepare( evtCtx, top_level );
  auto dphi_p = dphi_adj.prepare( evtCtx, top_level );

  auto deta = deta_p( true, tracks[0], tracks[1] );
  auto dphi = dphi_p( true, tracks[0], tracks[1] );

  auto delta_r2 = diff_eta * diff_eta + dphi_adj * dphi_adj;
  auto dr2_p    = delta_r2.prepare( evtCtx, top_level );
  auto dr2      = dr2_p( true, tracks[0], tracks[1] );
  auto delta_r  = chain( Functors::Common::Sqrt{}, delta_r2 );
  auto dr_p     = delta_r.prepare( evtCtx, top_level );
  auto dr       = dr_p( true, tracks[0], tracks[1] );

  auto mom0 = chain( Functors::Track::FourMomentum{}, Functors::Common::ForwardArgs<0>{} );
  auto mom1 = chain( Functors::Track::FourMomentum{}, Functors::Common::ForwardArgs<1>{} );

  auto comb_mass = chain( Functors::Composite::Mass{}, ( mom0 + mom1 ) );
  auto cmass_p   = comb_mass.prepare( evtCtx, top_level );
  auto cmass     = cmass_p( true, parts( 0 ), parts( 1 ) );

  auto part0 = *parts( 0 );

  auto vertex = LHCb::Vertex{};
  vertex.setPosition( Gaudi::XYZPoint{1.0, 1.0, 1.0} );
  part0.setEndVertex( &vertex );

  auto part = *parts( 1 );

  // set endvertex of B0
  auto decayvertex = LHCb::Vertex{};
  decayvertex.setPosition( Gaudi::XYZPoint{0.0, 0.0, 5.0} );
  part.setEndVertex( &decayvertex );
  part.setPV( &pvs.front() );

  const double pz = 1e5;
  const double m  = 1870.;
  part.setMomentum( Gaudi::LorentzVector{0, 0, pz, sqrt( pz * pz + m * m )} );
  auto momcov    = part.momCovMatrix();
  momcov( 0, 0 ) = momcov( 1, 1 ) = momcov( 2, 2 ) = momcov( 2, 2 ) = 1;
  part.setMomCovMatrix( momcov );

  auto poscov    = decayvertex.covMatrix();
  poscov( 0, 0 ) = poscov( 1, 1 ) = poscov( 2, 2 ) = 1;
  decayvertex.setCovMatrix( poscov );
  part.setPosCovMatrix( poscov );

  const auto& best_pv_p0   = Functors::Common::BestPV{}( pvs, part0 );
  auto        refpv_ipchi2 = Functors::Common::ImpactParameterChi2{}( best_pv_p0, part );
  auto        ownpv_ipchi2 = Functors::Common::ImpactParameterChi2ToOwnPV{}( part );

  BOOST_CHECK( LHCb::Utils::as_arithmetic( deta ) < fake_delta_eta );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( dphi ) < fake_delta_phi );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( dr2 ) < fake_delta_r2 );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( dr ) < fake_delta_r );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( cmass ) > fake_comb_mass_min &&
               LHCb::Utils::as_arithmetic( cmass ) < fake_comb_mass_max );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( refpv_ipchi2 ) > fake_ref_part_pv_ipchi2 );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( ownpv_ipchi2 ) > fake_ref_part_pv_ipchi2 );
  BOOST_CHECK( Functors::Common::OwnPV{}( part )->position() == pvs.front().position() );
}

BOOST_AUTO_TEST_CASE( test_flatten_functor ) {
  // First, make a couple of particles
  auto B_plus   = LHCb::Particle{LHCb::ParticleID( 521 ), 0};
  auto B0       = LHCb::Particle{LHCb::ParticleID( 511 ), 0};
  auto pi_plus  = LHCb::Particle( LHCb::ParticleID( 211 ), 0 );
  auto pi_minus = LHCb::Particle( LHCb::ParticleID( -211 ), 0 );
  auto K0       = LHCb::Particle( LHCb::ParticleID( 311 ), 0 );
  auto K_plus   = LHCb::Particle( LHCb::ParticleID( 321 ), 0 );
  auto K_minus  = LHCb::Particle( LHCb::ParticleID( -321 ), 0 );
  auto Bs0      = LHCb::Particle( LHCb::ParticleID( 531 ), 0 );
  auto Ds_plus  = LHCb::Particle( LHCb::ParticleID( 431 ), 0 );
  auto D_minus  = LHCb::Particle( LHCb::ParticleID( -411 ), 0 );

  // First decay: Bs0 -> ( B+ -> K0 pi+ ) pi- (DUMMY)
  // set pi- and K+ as daughters of B0
  SmartRefVector<LHCb::Particle> Bplus_children;
  Bplus_children.push_back( &pi_plus );
  Bplus_children.push_back( &K0 );
  B_plus.setDaughters( Bplus_children );

  // set B0 and pi+ as daughters of Bs0
  SmartRefVector<LHCb::Particle> Bs0_children;
  Bs0_children.push_back( &B_plus );
  Bs0_children.push_back( &pi_minus );
  Bs0.setDaughters( Bs0_children );

  // To be extra pediantic, let's test the functors also considering
  // a decay with two intermediate states

  // Second decay: B0 -> ( Ds+ -> K+ K- pi+ ) ( D- -> K+ pi- pi- )

  // set KKpi as children of Ds
  SmartRefVector<LHCb::Particle> Ds_children;
  Ds_children.push_back( &K_plus );
  Ds_children.push_back( &K_minus );
  Ds_children.push_back( &pi_plus );
  Ds_plus.setDaughters( Ds_children );

  // set Kpipi as children of D
  SmartRefVector<LHCb::Particle> D_children;
  D_children.push_back( &K_plus );
  D_children.push_back( &pi_minus );
  D_children.push_back( &pi_minus );
  D_minus.setDaughters( D_children );

  SmartRefVector<LHCb::Particle> B0_children;
  B0_children.push_back( &Ds_plus );
  B0_children.push_back( &D_minus );
  B0.setDaughters( B0_children );

  // We now have the decay Bs0 -> (B0 -> pi- K+) pi+ if the flattening functor
  // actually does what we want and flattens all basic particles from the decay
  // the resulting vector should have a size of 3 (pi- K+ pi+)

  // Initiate the Flattening Functor and apply it to the Bs0 particle
  const auto GET_BASICS        = Functors::Adapters::BasicsFromCompositeV1{};
  const auto GET_DESCENDANT    = Functors::Adapters::DescendantsFromCompositeV1{};
  const auto GET_CHILDREN      = Functors::Adapters::GenerationFromCompositeV1( 1 );
  const auto GET_GRANDCHILDREN = Functors::Adapters::GenerationFromCompositeV1( 2 );

  auto bs_basics        = GET_BASICS( Bs0 );
  auto bs_descendants   = GET_DESCENDANT( Bs0 );
  auto bs_children      = GET_CHILDREN( Bs0 );
  auto bs_grandchildren = GET_GRANDCHILDREN( Bs0 );

  auto b0_basics        = GET_BASICS( B0 );
  auto b0_descendants   = GET_DESCENDANT( B0 );
  auto b0_children      = GET_CHILDREN( B0 );
  auto b0_grandchildren = GET_GRANDCHILDREN( B0 );

  B0.setMomentum( Gaudi::LorentzVector( 100., 100., 1000., 5000. ) );     // 141 MeV pt
  pi_minus.setMomentum( Gaudi::LorentzVector( 0., 0., 1000., 5000. ) );   // 0 MeV pt
  pi_plus.setMomentum( Gaudi::LorentzVector( 1000., 0., 1000., 5000. ) ); // 1000 MeV pt
  K_plus.setMomentum( Gaudi::LorentzVector( 200., 200., 0., 5000. ) );    // 283 MeV pt
  K_minus.setMomentum( Gaudi::LorentzVector( 400., 400., 0., 5000. ) );   // 566 MeV pt
  D_minus.setMomentum( Gaudi::LorentzVector( 100., 0., 0., 5000. ) );     // 100 MeV pt
  Ds_plus.setMomentum( Gaudi::LorentzVector( 300., 0., 0., 5000. ) );     // 300 MeV pt

  // Are there actually 3 basic particles in the vector
  BOOST_CHECK_EQUAL( bs_basics.size(), 3 );
  // Are there actually 4 descendants in the vector
  BOOST_CHECK_EQUAL( bs_descendants.size(), 4 );
  // Are there actually 2 children in the vector
  BOOST_CHECK_EQUAL( bs_children.size(), 2 );
  // Are there actually 2 grandchildren in the vector
  BOOST_CHECK_EQUAL( bs_grandchildren.size(), 2 );

  // Are there actually 6 basic particles in the vector
  BOOST_CHECK_EQUAL( b0_basics.size(), 6 );
  // Are there actually 8 descendants in the vector
  BOOST_CHECK_EQUAL( b0_descendants.size(), 8 );
  // Are there actually 2 children in the vector
  BOOST_CHECK_EQUAL( b0_children.size(), 2 );
  // Are there actually 6 grandchildren in the vector
  BOOST_CHECK_EQUAL( b0_grandchildren.size(), 6 );

  // The logic is as such: Look at the Bs0, is it basic? No -> Look at the first
  // daughter, which is the B0. Is it basic? No. Look at the first daughter of the B0,
  // which is the pi-. Is it basic? Yes, then put it into the vector.
  BOOST_CHECK_EQUAL( bs_basics[0]->particleID().pid(), 211 );        // is the first entry a pi+
  BOOST_CHECK_EQUAL( bs_basics[1]->particleID().pid(), 311 );        // is the second entry a K0
  BOOST_CHECK_EQUAL( bs_basics[2]->particleID().pid(), -211 );       // is the third entry a pi-
  BOOST_CHECK_EQUAL( bs_descendants[0]->particleID().pid(), 521 );   // is the first entry a B+
  BOOST_CHECK_EQUAL( bs_descendants[1]->particleID().pid(), 211 );   // is the second entry a pi+
  BOOST_CHECK_EQUAL( bs_descendants[2]->particleID().pid(), 311 );   // is the third entry a K0
  BOOST_CHECK_EQUAL( bs_descendants[3]->particleID().pid(), -211 );  // is the fourth entry a pi-
  BOOST_CHECK_EQUAL( bs_children[0]->particleID().pid(), 521 );      // is the first entry a B+
  BOOST_CHECK_EQUAL( bs_children[1]->particleID().pid(), -211 );     // is the second entry a pi-
  BOOST_CHECK_EQUAL( bs_grandchildren[0]->particleID().pid(), 211 ); // is the first entry a pi+
  BOOST_CHECK_EQUAL( bs_grandchildren[1]->particleID().pid(), 311 ); // is the second entry a K0

  BOOST_CHECK_EQUAL( b0_basics[0]->particleID().pid(), 321 );       // is the first entry a K+
  BOOST_CHECK_EQUAL( b0_basics[1]->particleID().pid(), -321 );      // is the second entry a K-
  BOOST_CHECK_EQUAL( b0_basics[2]->particleID().pid(), 211 );       // is the third entry a pi+
  BOOST_CHECK_EQUAL( b0_basics[3]->particleID().pid(), 321 );       // is the first entry a K+
  BOOST_CHECK_EQUAL( b0_basics[4]->particleID().pid(), -211 );      // is the second entry a pi-
  BOOST_CHECK_EQUAL( b0_basics[5]->particleID().pid(), -211 );      // is the third entry a pi-
  BOOST_CHECK_EQUAL( b0_descendants[0]->particleID().pid(), 431 );  // is the first entry a Ds
  BOOST_CHECK_EQUAL( b0_descendants[1]->particleID().pid(), 321 );  // is the second entry a K+
  BOOST_CHECK_EQUAL( b0_descendants[2]->particleID().pid(), -321 ); // is the third entry a K-
  BOOST_CHECK_EQUAL( b0_descendants[3]->particleID().pid(), 211 );  // is the fourth entry a pi+
  BOOST_CHECK_EQUAL( b0_descendants[4]->particleID().pid(), -411 ); // is the first entry a D-
  BOOST_CHECK_EQUAL( b0_descendants[5]->particleID().pid(), 321 );  // is the second entry a K+
  BOOST_CHECK_EQUAL( b0_descendants[6]->particleID().pid(), -211 ); // is the third entry a pi-
  BOOST_CHECK_EQUAL( b0_descendants[7]->particleID().pid(), -211 ); // is the fourth entry a pi-
  BOOST_CHECK_EQUAL( b0_children[0]->particleID().pid(), 431 );     // is the first entry a Ds+
  BOOST_CHECK_EQUAL( b0_children[1]->particleID().pid(), -411 );    // is the second entry a D-
}

BOOST_AUTO_TEST_CASE( check_addressof_functor ) {
  int  a    = 10;
  auto func = Functors::Common::AddressOf{};
  auto ptr  = func( a );
  BOOST_CHECK( ptr == &a );
  BOOST_CHECK( *ptr == a );
}

struct DbgVal final {
// set this to 1 to enable printing
#if 0
  DbgVal( int value ) : m_value( value ) { std::cout << "Constructor " << m_value << std::endl; }
  ~DbgVal() { std::cout << "Destructor " << m_value << std::endl; m_value=-1;}
  DbgVal( DbgVal&& other ) noexcept : m_value( other.m_value ) { std::cout << "move const " << m_value << std::endl; }
  DbgVal( DbgVal const& other ) : m_value( other.m_value ) { std::cout << "copy const " << m_value << std::endl; }

  DbgVal& operator=( DbgVal const& other ) {
    m_value = other.m_value;
    std::cout << "copy assign " << m_value << std::endl;
    return *this;
  }

  DbgVal& operator=( DbgVal&& other ) noexcept {
    m_value = other.m_value;
    std::cout << "move assign " << m_value << std::endl;
    return *this;
  }
#else
  DbgVal( int value ) : m_value( value ) {}
#endif

  int m_value;

  friend std::ostream& operator<<( std::ostream& os, DbgVal const& m ) {
    os << "DbgVal{" << m.m_value << "}";
    return os;
  }
  friend auto operator+( DbgVal const& m, int const& i ) { return DbgVal( i + m.m_value ); }
  friend auto operator+( int const& i, DbgVal const& m ) { return DbgVal( i + m.m_value ); }
  friend auto operator+( DbgVal const& m, DbgVal const& n ) { return DbgVal( n.m_value + m.m_value ); }
};

BOOST_AUTO_TEST_CASE( check_lifetime_and_forwarding_logic ) {
  // mainly to check that temporaries don't go out of scope inside bind_helper before their use.
  EventContext const     evtCtx;
  Functors::TopLevelInfo top_level;

  auto add_3 = Functors::bind( Functors::Examples::AddInputs{}, Functors::Examples::PlusN{5},
                               Functors::Examples::PlusN{3}, Functors::Examples::PlusN{1} );

  auto p_add_3 = add_3.prepare( evtCtx, top_level );

  auto res = p_add_3( true, 1 );
  BOOST_CHECK_EQUAL( res, 12 );

  auto add_fwd = Functors::bind( Functors::Examples::AddInputs{},
                                 Functors::chain( Functors::Common::ForwardArgs<>{}, Functors::Examples::PlusN{5},
                                                  Functors::Common::ForwardArgs<>{} ),
                                 Functors::Examples::PlusN{3} );

  auto p_add_fwd = add_fwd.prepare( evtCtx, top_level );

  auto res2 = p_add_fwd( true, 1 );
  BOOST_CHECK_EQUAL( res2, 10 );

  // the below is helpfull for debuggin lifetime and perfect forwarding issues (enable printing in DbgVal)
  auto res3 = p_add_fwd( true, DbgVal( 10 ) );
  BOOST_CHECK_EQUAL( res3.m_value, 28 );

  auto val  = DbgVal( 5 );
  auto res4 = p_add_fwd( true, val );
  BOOST_CHECK_EQUAL( res4.m_value, 18 );
}

BOOST_AUTO_TEST_CASE( check_optional_returns ) {
  EventContext const     evtCtx;
  Functors::TopLevelInfo top_level;

  // OptReturn returns a number if called with true else an empty Functors::Optional

  auto opt_times_two = Functors::chain( Functors::Examples::TimesTwo{}, Functors::Examples::OptReturn{10} );

  auto p_opt_times_two = opt_times_two.prepare( evtCtx, top_level );

  // res1 should be 2 * Functors::Optional<int>{10};
  auto res1 = p_opt_times_two( true, true );
  // res1 should be 2 * Functors::Optional<int>{};
  auto res2 = p_opt_times_two( true, false );

  BOOST_CHECK_EQUAL( res1, 20 );
  BOOST_CHECK( !res2.has_value() );

  // Test that binding of functors with optional return also works.
  auto add_3_opt   = Functors::bind( Functors::Examples::AddInputs{}, Functors::Examples::OptReturn{5},
                                   Functors::Examples::OptReturn{3}, Functors::Examples::OptReturn{1} );
  auto p_add_3_opt = add_3_opt.prepare( evtCtx, top_level );

  auto res3 = p_add_3_opt( true, true );
  auto res4 = p_add_3_opt( true, false );

  BOOST_CHECK_EQUAL( res3, 9 );
  BOOST_CHECK( !res4.has_value() );

  // bigger test that has 3 functors that discconected from each other can return an empty optional
  auto opt0       = Functors::chain( Functors::Examples::OptReturn{5}, Functors::Common::ForwardArgs<0>{} );
  auto opt1       = Functors::chain( Functors::Examples::OptReturn{3}, Functors::Common::ForwardArgs<1>{} );
  auto opt2       = Functors::chain( Functors::Examples::OptReturn{1}, Functors::Common::ForwardArgs<2>{} );
  auto add_opts   = Functors::bind( Functors::Examples::AddInputs{}, opt0, opt1, opt2 );
  auto p_add_opts = add_opts.prepare( evtCtx, top_level );

  auto res5  = p_add_opts( true, true, true, true );
  auto res6  = p_add_opts( true, false, true, true );
  auto res7  = p_add_opts( true, true, false, true );
  auto res8  = p_add_opts( true, true, true, false );
  auto res9  = p_add_opts( true, false, false, true );
  auto res10 = p_add_opts( true, true, false, false );
  auto res11 = p_add_opts( true, false, true, false );
  auto res12 = p_add_opts( true, false, false, false );

  BOOST_CHECK_EQUAL( res5, 9 );

  BOOST_CHECK( !res6.has_value() );
  BOOST_CHECK( !res7.has_value() );
  BOOST_CHECK( !res8.has_value() );
  BOOST_CHECK( !res9.has_value() );
  BOOST_CHECK( !res10.has_value() );
  BOOST_CHECK( !res11.has_value() );
  BOOST_CHECK( !res12.has_value() );

  // check the VALUE_OR functor
  auto val_or   = Functors::chain( Functors::Functional::ValueOr{123}, Functors::Examples::OptReturn{10} );
  auto p_val_or = val_or.prepare( evtCtx, top_level );
  auto res13    = p_val_or( true, true );
  auto res14    = p_val_or( true, false );

  BOOST_CHECK_EQUAL( res13, 10 );
  BOOST_CHECK_EQUAL( res14, 123 );

  auto long_val_or = Functors::chain( Functors::Functional::ValueOr{123}, Functors::Examples::TimesTwo{},
                                      Functors::Examples::TimesTwo{}, Functors::Examples::OptReturn{10} );

  auto p_long_val_or = long_val_or.prepare( evtCtx, top_level );

  auto res15 = p_long_val_or( true, true );
  auto res16 = p_long_val_or( true, false );

  BOOST_CHECK_EQUAL( res15, 40 );
  BOOST_CHECK_EQUAL( res16, 123 );

  auto val_or_v   = Functors::chain( Functors::Functional::ValueOr{123},
                                   Functors::Examples::OptReturn<SIMDWrapper::scalar::int_v>( 10 ) );
  auto p_val_or_v = val_or_v.prepare( evtCtx, top_level );
  auto res17      = p_val_or_v( true, true );
  auto res18      = p_val_or_v( true, false );

  BOOST_CHECK_EQUAL( res17.cast(), 10 );
  BOOST_CHECK_EQUAL( res18.cast(), 123 );
  BOOST_CHECK( (std::is_same_v<decltype( res17 ), SIMDWrapper::scalar::int_v>));

  //
  // Check operators
  //
  auto test_operators_0 = Functors::chain( Functors::Functional::ValueOr{123},
                                           Functors::Examples::OptReturn{10} + Functors::Examples::OptReturn{11} );
  auto test_operators_1 = Functors::chain( Functors::Functional::ValueOr{123},
                                           Functors::Examples::OptReturn{10} - Functors::Examples::OptReturn{11} );
  auto test_operators_2 = Functors::chain( Functors::Functional::ValueOr{123},
                                           Functors::Examples::OptReturn{10} * Functors::Examples::OptReturn{11} );
  auto test_operators_3 = Functors::chain( Functors::Functional::ValueOr{123},
                                           Functors::Examples::OptReturn{10} / Functors::Examples::OptReturn{11} );

  auto p_test_operators_0 = test_operators_0.prepare( evtCtx, top_level );
  auto p_test_operators_1 = test_operators_1.prepare( evtCtx, top_level );
  auto p_test_operators_2 = test_operators_2.prepare( evtCtx, top_level );
  auto p_test_operators_3 = test_operators_3.prepare( evtCtx, top_level );

  BOOST_CHECK_EQUAL( p_test_operators_0( true, true ), 10 + 11 );
  BOOST_CHECK_EQUAL( p_test_operators_1( true, true ), 10 - 11 );
  BOOST_CHECK_EQUAL( p_test_operators_2( true, true ), 10 * 11 );
  BOOST_CHECK_EQUAL( p_test_operators_3( true, true ), 10 / 11 );
  BOOST_CHECK_EQUAL( p_test_operators_0( true, false ), 123 );
  BOOST_CHECK_EQUAL( p_test_operators_1( true, false ), 123 );
  BOOST_CHECK_EQUAL( p_test_operators_2( true, false ), 123 );
  BOOST_CHECK_EQUAL( p_test_operators_3( true, false ), 123 );

  LHCb::LinAlg::Vec3<float> vec_a{1.f, 2.f, 3.f}, vec_b{4.f, 5.f, 6.f};
  auto                      test_operators_4 =
      Functors::chain( Functors::Functional::ValueOr{123.f}, Functors::Common::Magnitude{},
                       Functors::Examples::OptReturn{vec_a} + Functors::Examples::OptReturn{vec_b} );
  auto test_operators_5 =
      Functors::chain( Functors::Functional::ValueOr{123.f}, Functors::Common::Magnitude{},
                       Functors::Examples::OptReturn{vec_a} - Functors::Examples::OptReturn{vec_b} );
  auto p_test_operators_4 = test_operators_4.prepare( evtCtx, top_level );
  auto p_test_operators_5 = test_operators_5.prepare( evtCtx, top_level );

  BOOST_CHECK_EQUAL( p_test_operators_4( true, true ), ( vec_a + vec_b ).mag() );
  BOOST_CHECK_EQUAL( p_test_operators_5( true, true ), ( vec_a - vec_b ).mag() );
  BOOST_CHECK_EQUAL( p_test_operators_4( true, false ), 123.f );
  BOOST_CHECK_EQUAL( p_test_operators_5( true, false ), 123.f );

  //
  // Check value or in LHCb::LinAlg::Vec
  //

  // values
  LHCb::LinAlg::Vec3<SIMDWrapper::scalar::float_v> value_vec3_float_v{1.f, 2.f, 3.f};
  LHCb::LinAlg::Vec3<SIMDWrapper::scalar::int_v>   value_vec3_int_v{1, 2, 3};
  LHCb::LinAlg::Vec3<SIMDWrapper::scalar::float_v> default_vec3_float_v{6.f, 6.f, 6.f};
  LHCb::LinAlg::Vec3<SIMDWrapper::scalar::int_v>   default_vec3_int_v{999, 999, 999};

  // Input container
  Functors::Optional<LHCb::LinAlg::Vec3<SIMDWrapper::scalar::float_v>> opt_vec3_float_v;
  Functors::Optional<LHCb::LinAlg::Vec3<SIMDWrapper::scalar::int_v>>   opt_vec3_int_v;

  // Test functors
  Functors::Functional::ValueOr VALUE_OR_VEC3_FLOAT{std::make_tuple( 6.f, 6.f, 6.f )};
  Functors::Functional::ValueOr VALUE_OR_VEC3_INT{std::make_tuple( 999, 999, 999 )};

  // Check before emplace
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VALUE_OR_VEC3_FLOAT( opt_vec3_float_v ).mag2() ),
                     LHCb::Utils::as_arithmetic( default_vec3_float_v.mag2() ) );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VALUE_OR_VEC3_INT( opt_vec3_int_v ).mag2() ),
                     LHCb::Utils::as_arithmetic( default_vec3_int_v.mag2() ) );

  // Emplace
  opt_vec3_float_v = value_vec3_float_v;
  opt_vec3_int_v   = value_vec3_int_v;

  // Check after emplace
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VALUE_OR_VEC3_FLOAT( opt_vec3_float_v ).mag2() ),
                     LHCb::Utils::as_arithmetic( value_vec3_float_v.mag2() ) );
  BOOST_CHECK_EQUAL( LHCb::Utils::as_arithmetic( VALUE_OR_VEC3_INT( opt_vec3_int_v ).mag2() ),
                     LHCb::Utils::as_arithmetic( value_vec3_int_v.mag2() ) );
}

BOOST_AUTO_TEST_CASE( test_recsummary_functors ) {
  LHCb::RecSummary rec_summary;
  int              npvs{3}, ntracks{60}, nftclusters{70};
  rec_summary.addInfo( LHCb::RecSummary::nPVs, npvs );
  rec_summary.addInfo( LHCb::RecSummary::nTracks, ntracks );
  rec_summary.addInfo( LHCb::RecSummary::nFTClusters, nftclusters );
  auto func_npvs        = Functors::TES::RecSummaryInfo{LHCb::RecSummary::nPVs}( rec_summary );
  auto func_ntracks     = Functors::TES::RecSummaryInfo{LHCb::RecSummary::nTracks}( rec_summary );
  auto func_nftclusters = Functors::TES::RecSummaryInfo{LHCb::RecSummary::nFTClusters}( rec_summary );
  BOOST_CHECK_EQUAL( npvs, func_npvs.value() );
  BOOST_CHECK_EQUAL( ntracks, func_ntracks.value() );
  BOOST_CHECK_EQUAL( nftclusters, func_nftclusters.value() );
}

BOOST_AUTO_TEST_CASE( test_arithmetic_functor ) {
  EventContext const     evtCtx;
  Functors::TopLevelInfo top_level;

  auto opt_abs = Functors::chain( Functors::Common::Abs{}, Functors::Examples::OptReturn{-42} );

  auto p_opt_abs = opt_abs.prepare( evtCtx, top_level );

  auto res = p_opt_abs( true, true );

  BOOST_CHECK_EQUAL( res, 42 );
}

BOOST_AUTO_TEST_CASE( test_get_proto_functor ) {
  // create protoparticle
  LHCb::ProtoParticle proto;

  Functors::Particle::PPHasMuonInfo PPHASMUON;
  Functors::Particle::PPHasRich     PPHASRICH;

  BOOST_CHECK_EQUAL( PPHASMUON( proto ), false );
  BOOST_CHECK_EQUAL( PPHASRICH( proto ), false );
}

BOOST_AUTO_TEST_CASE( test_get_track_functor ) {
  // v1
  // create a state
  LHCb::State state;
  // Positions (x,y,z = 1) of state, slopes (tx = dx/dz,ty = dy/dz) of state and charge/momentum (q/p) of state
  float qpBeg = 1. / ( 10. * Gaudi::Units::GeV );
  state.setState( 0., 0., 1., 3., 2., qpBeg ); // x,y,z,tx,ty,q/p
  Gaudi::XYZVector true_threemom = state.momentum();
  // create a track
  LHCb::Track track;
  track.addToStates( state );
  // create protoparticle
  LHCb::ProtoParticle proto;
  proto.setTrack( &track );
  // create a particle
  LHCb::Particle charged_basic;
  charged_basic.setProto( &proto );
  // get track from functor for v1
  auto TRACK              = Functors::Particle::GetTrack{};
  auto THREEMOMENTUM      = Functors::Track::ThreeMomentum{};
  auto trck_func          = TRACK( charged_basic ).value();
  auto trck_func_threemom = THREEMOMENTUM( trck_func );
  // Check that the result is correct... allow for tiny numerical tolerance: 1e-6 relative
  BOOST_CHECK( abs( trck_func_threemom.X().cast() - true_threemom.X() ) <
               1.e-6 * abs( trck_func_threemom.X().cast() ) );
  BOOST_CHECK( abs( trck_func_threemom.Y().cast() - true_threemom.Y() ) <
               1.e-6 * abs( trck_func_threemom.Y().cast() ) );
  BOOST_CHECK( abs( trck_func_threemom.Z().cast() - true_threemom.Z() ) <
               1.e-6 * abs( trck_func_threemom.Z().cast() ) );

  // v2
  // For some reason could move the producing of charged basic to a seperate function
  // Could be related to: "A Zip only keep pointers to existing containers and does not own any memory
  // thus it should always live on the stack where it can be optimized away by the compiler."
  unsigned int n_elements = 3;
  using namespace LHCb::Event;
  using StateLocation   = LHCb::Event::v3::Tracks::StateLocation;
  using simd_t          = SIMDWrapper::type_map_t<SIMDWrapper::InstructionSet::Scalar>;
  using StatusMasks     = LHCb::Event::v2::Muon::StatusMasks;
  auto           zn     = Zipping::generateZipIdentifier();
  auto           tracks = v3::generate_tracks( n_elements, unique_id_gen, 1, zn );
  v2::Muon::PIDs muon_pids{zn};
  muon_pids.reserve( n_elements );
  LHCb::Event::ChargedBasics charged_basic_v2{&tracks, &muon_pids};
  charged_basic_v2.reserve( n_elements );
  for ( unsigned int i = 0; i < n_elements; ++i ) {
    auto part = charged_basic_v2.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    part.field<ChargedBasicsTag::RichPIDCode>().set( 0 );

    auto mu_pid = muon_pids.emplace_back<SIMDWrapper::InstructionSet::Scalar>();
    part.field<ChargedBasicsTag::MuonPID>().set( mu_pid.offset() );
    LHCb::Event::flags_v<simd_t, StatusMasks> flags;
    flags.set<StatusMasks::IsMuon>( false );
    mu_pid.field<v2::Muon::Tag::Status>().set( flags );
    mu_pid.field<v2::Muon::Tag::Chi2Corr>().set( std::numeric_limits<float>::lowest() );

    part.field<ChargedBasicsTag::Track>().set( i );

    part.field<ChargedBasicsTag::Mass>().set( 0 );
    part.field<ChargedBasicsTag::ParticleID>().set( 0 );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::p ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::e ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::pi ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::K ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::mu ).set( std::numeric_limits<float>::lowest() );
    part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::d ).set( std::numeric_limits<float>::lowest() );
  }

  auto const& truev3_threemom      = tracks.scalar()[0].threeMomentum( StateLocation::ClosestToBeam );
  auto const& trckv3_func          = TRACK( charged_basic_v2 ); // get track from functors
  auto const& trckv3_func_threemom = trckv3_func.scalar()[0].threeMomentum( StateLocation::ClosestToBeam );
  ////Following complains about "error: ‘const struct LHCb::Event::v3::Tracks’ has no member named ‘threeMomentum’"
  // auto trckv3_func_threemom   = THREEMOMENTUM( trckv3_func );
  BOOST_CHECK( abs( trckv3_func_threemom.X().cast() - truev3_threemom.X().cast() ) <
               1.e-6 * abs( trckv3_func_threemom.X().cast() ) );
  BOOST_CHECK( abs( trckv3_func_threemom.Y().cast() - truev3_threemom.Y().cast() ) <
               1.e-6 * abs( trckv3_func_threemom.Y().cast() ) );
  BOOST_CHECK( abs( trckv3_func_threemom.Z().cast() - truev3_threemom.Z().cast() ) <
               1.e-6 * abs( trckv3_func_threemom.Z().cast() ) );

  // the following tests accessing weights (ExtraTags) of RelationTable1D/2D using
  // the Weight and Get functors.
  auto WEIGHT = Functors::Common::Weight{};
  auto GET_0  = Functors::Common::Get<0>{};
  auto MIN    = Functors::Functional::MinElementNotZero{};
  struct Val1 : LHCb::Event::int_field {};
  struct Val2 : LHCb::Event::float_field {};
  const auto reltracks   = v3::generate_tracks( 2 * SIMDWrapper::best::types::size, unique_id_gen, 1, zn );
  const auto othertracks = v3::generate_tracks( 2 * SIMDWrapper::best::types::size + 1, unique_id_gen, 1, zn );
  LHCb::Event::RelationTable1D<decltype( tracks ), Val1, Val2>                    reltable{&reltracks};
  LHCb::Event::RelationTable2D<decltype( tracks ), decltype( othertracks ), Val2> reltable2{&reltracks, &othertracks};
  reltable.reserve( tracks.size() );
  for ( const auto& track : reltracks.scalar() ) {
    reltable.add( track, 4 + track.offset(), 2.f );
    reltable2.add( track, othertracks.scalar()[track.offset()], 3.f );
  }
  reltable2.add( reltracks.scalar()[0], othertracks.scalar()[reltracks.size()], 3.f );

  auto view = reltable.buildView();
  auto zip  = LHCb::Event::make_zip( *reltable.from(), view );
  for ( const auto& t : zip ) {
    auto       value     = std::invoke( GET_0, std::invoke( WEIGHT, t ) );
    const auto test_mask = ( value == ( 4 + t.indices() ) ) && t.loop_mask();
    BOOST_CHECK_EQUAL( popcount( test_mask ), popcount( t.loop_mask() ) );
  }

  auto view2 = reltable2.buildFromView();
  auto zip2  = LHCb::Event::make_zip( *reltable2.from(), view2 );
  for ( const auto& t : zip2 ) {
    auto       value     = std::invoke( MIN, std::invoke( GET_0, std::invoke( WEIGHT, t ) ) );
    const auto test_mask = ( abs( value - 3.f ) <= std::numeric_limits<decltype( value )>::min() ) && t.loop_mask();
    BOOST_CHECK_EQUAL( popcount( test_mask ), popcount( t.loop_mask() ) );
  }
}

BOOST_AUTO_TEST_CASE( test_Value_ValueOrDict ) {
  EventContext const     evtCtx;
  Functors::TopLevelInfo top_level;

  std::map<std::string, int> map         = {{"a", 2}, {"b", 3}};
  std::map<std::string, int> map_default = {{"a", -1}, {"b", -1}};

  // test Value
  auto val    = Functors::chain( Functors::Functional::Value{}, Functors::Examples::OptReturn{map} );
  auto p_val  = val.prepare( evtCtx, top_level );
  auto funval = p_val( true, true );
  BOOST_CHECK_THROW( p_val( true, false ), GaudiException );
  BOOST_CHECK_EQUAL( funval["a"], 2 );
  BOOST_CHECK_EQUAL( funval["b"], 3 );

  // test ValueOr
  auto val_or = Functors::chain( Functors::Functional::ValueOr{map_default}, Functors::Examples::OptReturn{map} );
  // works specifying the type explicitly to ValueOr functor
  // auto val_or_mine   = Functors::Functional::ValueOr<std::map<std::string, int>>{{{ std::string{"a"}, int{-1} }, {
  //  std::string{"b"}, int{-1} }}};
  // works specifying the type explicitly in the parameter
  // auto val_or_mine   = Functors::Functional::ValueOr{std::map<std::string,int>{{ std::string{"a"}, int{-1} }, {
  //  std::string{"b"}, int{-1} }}};
  // doesn't work with type deduction
  // auto val_or_mine   = Functors::Functional::ValueOr{{{ std::string{"a"}, int{-1} }, { std::string{"b"}, int{-1} }}};
  auto p_val_or      = val_or.prepare( evtCtx, top_level );
  auto funval_or     = p_val_or( true, true );
  auto def_funval_or = p_val_or( true, false );
  BOOST_CHECK_EQUAL( funval_or["a"], 2 );
  BOOST_CHECK_EQUAL( funval_or["b"], 3 );
  BOOST_CHECK_EQUAL( def_funval_or["a"], -1 );
  BOOST_CHECK_EQUAL( def_funval_or["b"], -1 );
}
BOOST_AUTO_TEST_CASE( test_tree_functors ) {

  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;

  // First, make a couple of particles
  auto B0       = LHCb::Particle{LHCb::ParticleID( 511 ), 0};
  auto pi_plus  = LHCb::Particle( LHCb::ParticleID( 211 ), 0 );
  auto pi_minus = LHCb::Particle( LHCb::ParticleID( -211 ), 0 );
  auto K_plus   = LHCb::Particle( LHCb::ParticleID( 321 ), 0 );
  auto K_minus  = LHCb::Particle( LHCb::ParticleID( -321 ), 0 );
  auto Ds_plus  = LHCb::Particle( LHCb::ParticleID( 431 ), 0 );
  auto D_minus  = LHCb::Particle( LHCb::ParticleID( -411 ), 0 );

  // Decay: B0 -> ( Ds+ -> K+ K- pi+ ) ( D- -> K+ pi- pi- )

  // set KKpi as children of Ds
  SmartRefVector<LHCb::Particle> Ds_children;
  Ds_children.push_back( &K_plus );
  Ds_children.push_back( &K_minus );
  Ds_children.push_back( &pi_plus );
  Ds_plus.setDaughters( Ds_children );

  // set Kpipi as children of D
  SmartRefVector<LHCb::Particle> D_children;
  D_children.push_back( &K_plus );
  D_children.push_back( &pi_minus );
  D_children.push_back( &pi_minus );
  D_minus.setDaughters( D_children );

  SmartRefVector<LHCb::Particle> B0_children;
  B0_children.push_back( &Ds_plus );
  B0_children.push_back( &D_minus );
  B0.setDaughters( B0_children );

  // create a state
  LHCb::State state1, state2;
  // Positions (x,y,z = 1) of state, slopes (tx = dx/dz,ty = dy/dz) of state and charge/momentum (q/p) of state
  float qpBeg = 1. / ( 10. * Gaudi::Units::GeV );
  state1.setState( 0., 0., 1., 3., 2., qpBeg ); // x,y,z,tx,ty,q/p
  state2.setState( 1., 0., 0., 2., 3., qpBeg ); // x,y,z,tx,ty,q/p
  // create a track
  LHCb::Track t1, t2;
  t1.addToStates( state1 );
  t2.addToStates( state2 );
  // create protoparticle
  LHCb::ProtoParticle pp1, pp2;
  pp1.setTrack( &t1 );
  pp2.setTrack( &t2 );
  LHCb::Particle p1, p2, p3;
  p1.setProto( &pp1 );
  p2.setProto( &pp2 );

  SmartRefVector<LHCb::Particle> p3_children;
  p3_children.push_back( &p1 );
  p3_children.push_back( &p2 );
  p3.setDaughters( p3_children );

  B0.setMomentum( Gaudi::LorentzVector( 100., 100., 1000., 5000. ) );     // 141 MeV pt
  pi_minus.setMomentum( Gaudi::LorentzVector( 0., 0., 1000., 5000. ) );   // 0 MeV pt
  pi_plus.setMomentum( Gaudi::LorentzVector( 1000., 0., 1000., 5000. ) ); // 1000 MeV pt
  K_plus.setMomentum( Gaudi::LorentzVector( 200., 200., 0., 5000. ) );    // 283 MeV pt
  K_minus.setMomentum( Gaudi::LorentzVector( 400., 400., 0., 5000. ) );   // 566 MeV pt
  D_minus.setMomentum( Gaudi::LorentzVector( 100., 0., 0., 5000. ) );     // 100 MeV pt
  Ds_plus.setMomentum( Gaudi::LorentzVector( 300., 0., 0., 5000. ) );     // 300 MeV pt

  auto const& MINTREE =
      Functors::chain( ::Functors::Functional::Min{},
                       ::Functors::Functional::Map( /* The functor to map over a range. */ Functors::chain(
                           ::Functors::Common::Rho_Coordinate{}, ::Functors::Track::ThreeMomentum{} ) ),
                       ::Functors::Filter( /* Predicate to filter the container with. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       ::Functors::Adapters::DescendantsFromCompositeV1{} );
  auto const& MAXTREE =
      Functors::chain( ::Functors::Functional::Max{},
                       ::Functors::Functional::Map( /* The functor to map over a range. */ Functors::chain(
                           ::Functors::Common::Rho_Coordinate{}, ::Functors::Track::ThreeMomentum{} ) ),
                       ::Functors::Filter( /* Predicate to filter the container with. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       ::Functors::Adapters::DescendantsFromCompositeV1{} );
  auto const& NINTREE =
      Functors::chain( ::Functors::detail::SizeOf{},
                       ::Functors::Filter( /* Predicate to filter the container with. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       ::Functors::Adapters::DescendantsFromCompositeV1{} );
  auto const& INTREE =
      Functors::chain( ::Functors::Functional::MapAnyOf( /* The predicate functor to map over a range. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       ::Functors::Adapters::DescendantsFromCompositeV1{} );

  auto const& FIND_IN_TREE = Functors::bind(
      ::Functors::Functional::MapAnyOf( /* The predicate functor to map over a range. */ operator==(
          Functors::chain( ::Functors::Particle::GetTrack{}, ::Functors::Particle::GetProtoParticle{},
                           ::Functors::Common::ForwardArgs<0>() ),
          Functors::chain( ::Functors::Particle::GetTrack{}, ::Functors::Particle::GetProtoParticle{},
                           ::Functors::Common::ForwardArgs<1>() ) ) ),
      Functors::chain( ::Functors::Adapters::BasicsFromCompositeV1{}, ::Functors::Common::ForwardArgs<0>() ),
      ::Functors::Common::ForwardArgs<1>() );

  auto const& SHARE_TRACKS = operator==(
      Functors::chain( ::Functors::Particle::GetTrack{}, ::Functors::Particle::GetProtoParticle{},
                       ::Functors::Common::ForwardArgs<0>() ),
      Functors::chain( ::Functors::Particle::GetTrack{}, ::Functors::Particle::GetProtoParticle{},
                       ::Functors::Common::ForwardArgs<1>() ) );

  auto mintree_prepped      = MINTREE.prepare( evtCtx, top_level );
  auto maxtree_prepped      = MAXTREE.prepare( evtCtx, top_level );
  auto nintree_prepped      = NINTREE.prepare( evtCtx, top_level );
  auto intree_prepped       = INTREE.prepare( evtCtx, top_level );
  auto find_in_tree_prepped = FIND_IN_TREE.prepare( evtCtx, top_level );
  auto share_tracks_prepped = SHARE_TRACKS.prepare( evtCtx, top_level );

  BOOST_CHECK( LHCb::Utils::as_arithmetic( mintree_prepped( true, B0 ) ) < 500 );
  BOOST_CHECK( LHCb::Utils::as_arithmetic( maxtree_prepped( true, B0 ) ) > 500 );
  BOOST_CHECK_EQUAL( nintree_prepped( true, B0 ), 3 );
  BOOST_CHECK_EQUAL( intree_prepped( true, B0 ), true );
  BOOST_CHECK_EQUAL( find_in_tree_prepped( true, p3, p1 ), true );
  BOOST_CHECK_EQUAL( share_tracks_prepped( true, p1, p1 ), true );

  auto const& CHILD  = ::Functors::Adapters::GenerationFromCompositeV1( std::integral_constant<int, 1>{} );
  auto const& GCHILD = ::Functors::Adapters::GenerationFromCompositeV1( std::integral_constant<int, 2>{} );

  auto children      = CHILD( B0 );
  auto grandchildren = GCHILD( B0 );

  BOOST_CHECK_EQUAL( children[0]->particleID().pid(), 431 );       // is the first entry a Ds+
  BOOST_CHECK_EQUAL( children[1]->particleID().pid(), -411 );      // is the second entry a D-
  BOOST_CHECK_EQUAL( grandchildren[0]->particleID().pid(), 321 );  // is the first entry a K+
  BOOST_CHECK_EQUAL( grandchildren[1]->particleID().pid(), -321 ); // is the second entry a K-
  BOOST_CHECK_EQUAL( grandchildren[2]->particleID().pid(), 211 );  // is the third entry a pi+
  BOOST_CHECK_EQUAL( grandchildren[3]->particleID().pid(), 321 );  // is the first entry a K+
  BOOST_CHECK_EQUAL( grandchildren[4]->particleID().pid(), -211 ); // is the second entry a pi-
  BOOST_CHECK_EQUAL( grandchildren[5]->particleID().pid(), -211 ); // is the third entry a pi-

  auto const& CHILD_NINGENERATION =
      Functors::chain( ::Functors::detail::SizeOf{},
                       ::Functors::Filter( /* Predicate to filter the container with. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       CHILD );

  auto const& CHILD_INGENERATION =
      Functors::chain( ::Functors::Functional::MapAnyOf( /* The predicate functor to map over a range. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       CHILD );
  auto const& GCHILD_NINGENERATION =
      Functors::chain( ::Functors::detail::SizeOf{},
                       ::Functors::Filter( /* Predicate to filter the container with. */ operator>(
                           Functors::chain( ::Functors::Common::X_Coordinate{}, ::Functors::Track::ThreeMomentum{} ),
                           std::integral_constant<int, 200>{} ) ),
                       GCHILD );

  auto const& GCHILD_INGENERATION = Functors::chain(
      ::Functors::Functional::MapAnyOf( /* The predicate functor to map over a range. */ ::Functors::AcceptAll{} ),
      GCHILD );

  auto child_ningeneration_prepped  = CHILD_NINGENERATION.prepare( evtCtx, top_level );
  auto child_ingeneration_prepped   = CHILD_INGENERATION.prepare( evtCtx, top_level );
  auto gchild_ningeneration_prepped = GCHILD_NINGENERATION.prepare( evtCtx, top_level );
  auto gchild_ingeneration_prepped  = GCHILD_INGENERATION.prepare( evtCtx, top_level );

  BOOST_CHECK_EQUAL( child_ningeneration_prepped( true, B0 ), 1 );
  BOOST_CHECK_EQUAL( child_ingeneration_prepped( true, B0 ), true );
  BOOST_CHECK_EQUAL( gchild_ningeneration_prepped( true, B0 ), 2 );
  BOOST_CHECK_EQUAL( gchild_ingeneration_prepped( true, B0 ), true );

  Functors::Particle::IsBasicParticle ISBASICPARTICLE;
  BOOST_CHECK_EQUAL( ISBASICPARTICLE( B0 ), false );
  BOOST_CHECK_EQUAL( ISBASICPARTICLE( children[0] ), false );
  BOOST_CHECK_EQUAL( ISBASICPARTICLE( grandchildren[0] ), true );
}

BOOST_AUTO_TEST_CASE( test_tistos_functors ) {
  using map_t                 = std::map<std::string, LHCb::detail::TisTosResult_t>;
  using optional_map_t        = Functors::Optional<map_t>;
  using RelationTable_P2Map_t = LHCb::Relation1D<LHCb::Particle, map_t>;
  using VALUE_FROM_DICT       = Functors::Functional::ValueFromDict<map_t::key_type>;
  using IS_TIS                = Functors::Particle::IsTis;
  using IS_TOS                = Functors::Particle::IsTos;
  using TO                    = Functors::Common::To;
  using FRONT                 = Functors::Functional::Front;
  using RELATIONS             = Functors::Common::Relations;
  using CAST_TO_INT           = Functors::Functional::CastTo<int>;
  using VALUE_OR_INT          = Functors::Functional::ValueOr<int>;
  using VALUE_OR_BOOL         = Functors::Functional::ValueOr<bool>;
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;

  // Create three dummay particles
  auto parts = make_parts();
  // Create "optional" tistos map object
  map_t                        tisTosMap;
  LHCb::detail::TisTosResult_t Line1res;
  Line1res.setTOS( true );
  LHCb::detail::TisTosResult_t Line2res;
  Line2res.setTIS( true );
  tisTosMap["Line1"] = Line1res;
  tisTosMap["Line2"] = Line2res;
  optional_map_t tisTosMapOpt{tisTosMap};
  // Relate the tistos map to first particle and not other two particles
  RelationTable_P2Map_t table;
  table.relate( parts( 0 ), tisTosMap ).ignore();
  // Create composed functors for various scenarious
  auto const MAP_TO_RELATED = Functors::chain( TO{}, FRONT{}, RELATIONS{} );
  // for integer outputs (tupling)
  auto const tmp_l1_istos_int = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TOS{}, VALUE_FROM_DICT{"Line1"},
                                                 MAP_TO_RELATED ); // all correct
  auto const tmp_l1_istis_int = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TIS{}, VALUE_FROM_DICT{"Line1"},
                                                 MAP_TO_RELATED ); // all correct
  auto const tmp_l2_istos_int = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TOS{}, VALUE_FROM_DICT{"Line2"},
                                                 MAP_TO_RELATED ); // all correct
  auto const tmp_l2_istis_int = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TIS{}, VALUE_FROM_DICT{"Line2"},
                                                 MAP_TO_RELATED ); // all correct
  auto const tmp_wrong1_int   = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TOS{}, VALUE_FROM_DICT{"Line3"},
                                               MAP_TO_RELATED ); // wrong line name
  auto const tmp_wrong2_int   = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TOS{}, VALUE_FROM_DICT{"Line2"},
                                               MAP_TO_RELATED ); // no trigger info for particle two
  auto const tmp_wrong3_int   = Functors::chain( VALUE_OR_INT{-1}, CAST_TO_INT{}, IS_TOS{}, VALUE_FROM_DICT{"Line3"},
                                               MAP_TO_RELATED ); // wrong line name and no trigger info
  auto       val_l1_istos_int = tmp_l1_istos_int.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto       val_l1_istis_int = tmp_l1_istis_int.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto       val_l2_istos_int = tmp_l2_istos_int.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto       val_l2_istis_int = tmp_l2_istis_int.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto       val_wrong1_int = tmp_wrong1_int.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // wrong line name
  auto       val_wrong2_int =
      tmp_wrong2_int.prepare( evtCtx, top_level )( true, table, parts( 1 ) ); // no trigger info for particle two
  auto val_wrong3_int = tmp_wrong3_int.prepare( evtCtx, top_level )(
      true, table, parts( 1 ) ); // wrong line name and no trigger info particle two
  BOOST_CHECK_EQUAL( val_l1_istos_int, static_cast<int>( Line1res.TOS() ) );
  BOOST_CHECK_EQUAL( val_l1_istis_int, static_cast<int>( Line1res.TIS() ) );
  BOOST_CHECK_EQUAL( val_l2_istos_int, static_cast<int>( Line2res.TOS() ) );
  BOOST_CHECK_EQUAL( val_l2_istis_int, static_cast<int>( Line2res.TIS() ) );
  BOOST_CHECK_EQUAL( val_wrong1_int, -1 );
  BOOST_CHECK_EQUAL( val_wrong2_int, -1 );
  BOOST_CHECK_EQUAL( val_wrong3_int, -1 );
  // same as above but for booleans useful for trigger cuts
  auto const tmp_l1_istos_bool =
      Functors::chain( VALUE_OR_BOOL{false}, IS_TOS{}, VALUE_FROM_DICT{"Line1"}, MAP_TO_RELATED ); // all correct
  auto const tmp_l1_istis_bool =
      Functors::chain( VALUE_OR_BOOL{false}, IS_TIS{}, VALUE_FROM_DICT{"Line1"}, MAP_TO_RELATED ); // all correct
  auto const tmp_l2_istos_bool =
      Functors::chain( VALUE_OR_BOOL{false}, IS_TOS{}, VALUE_FROM_DICT{"Line2"}, MAP_TO_RELATED ); // all correct
  auto const tmp_l2_istis_bool =
      Functors::chain( VALUE_OR_BOOL{false}, IS_TIS{}, VALUE_FROM_DICT{"Line2"}, MAP_TO_RELATED ); // all correct
  auto const tmp_wrong1_bool =
      Functors::chain( VALUE_OR_BOOL{false}, IS_TOS{}, VALUE_FROM_DICT{"Line3"}, MAP_TO_RELATED ); // wrong line name
  auto const tmp_wrong2_bool = Functors::chain( VALUE_OR_BOOL{false}, IS_TOS{}, VALUE_FROM_DICT{"Line2"},
                                                MAP_TO_RELATED ); // no trigger info for particle two
  auto const tmp_wrong3_bool = Functors::chain( VALUE_OR_BOOL{false}, IS_TOS{}, VALUE_FROM_DICT{"Line3"},
                                                MAP_TO_RELATED ); // wrong line name and no trigger info
  auto val_l1_istos_bool     = tmp_l1_istos_bool.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto val_l1_istis_bool     = tmp_l1_istis_bool.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto val_l2_istos_bool     = tmp_l2_istos_bool.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto val_l2_istis_bool     = tmp_l2_istis_bool.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // all correct
  auto val_wrong1_bool = tmp_wrong1_bool.prepare( evtCtx, top_level )( true, table, parts( 0 ) ); // wrong line name
  auto val_wrong2_bool =
      tmp_wrong2_bool.prepare( evtCtx, top_level )( true, table, parts( 1 ) ); // no trigger info for particle two
  auto val_wrong3_bool = tmp_wrong3_bool.prepare( evtCtx, top_level )(
      true, table, parts( 1 ) ); // wrong line name and no trigger info particle two
  BOOST_CHECK_EQUAL( val_l1_istos_bool, Line1res.TOS() );
  BOOST_CHECK_EQUAL( val_l1_istis_bool, Line1res.TIS() );
  BOOST_CHECK_EQUAL( val_l2_istos_bool, Line2res.TOS() );
  BOOST_CHECK_EQUAL( val_l2_istis_bool, Line2res.TIS() );
  BOOST_CHECK_EQUAL( val_wrong1_bool, false );
  BOOST_CHECK_EQUAL( val_wrong2_bool, false );
  BOOST_CHECK_EQUAL( val_wrong3_bool, false );
}

BOOST_AUTO_TEST_CASE( test_lhcb_math_functors ) {

  // ValueWithError
  Gaudi::Math::ValueWithError math_value{999., 222.};

  Functors::LHCbMath::ValueWithError::Value MATH_VALUE;
  Functors::LHCbMath::ValueWithError::Error MATH_ERROR;

  BOOST_CHECK_EQUAL( MATH_VALUE( math_value ), math_value.value() );
  BOOST_CHECK_EQUAL( MATH_ERROR( math_value ), math_value.error() );

  // scalarMomentum and invariantMass
  const int           nDim = 4;
  Gaudi::SymMatrix4x4 covMatrix;
  double              cov[nDim][nDim] = {{0.01, 0., 0., 0.}, {0, 0.1, 0., 0.}, {0., 0., 0.1, 0.}, {0., 0., 0., 0.1}};
  for ( int i = 0; i < nDim; i++ ) {
    for ( int j = 0; j < nDim; j++ ) { covMatrix( i, j ) = cov[i][j]; }
  }
  using Gaudi::Units::GeV;
  Gaudi::Vector4                      mom( 0.2 * GeV, 1 * GeV, 0.5 * GeV, 5 * GeV );
  Gaudi::Math::LorentzVectorWithError P4{mom, covMatrix};

  Functors::LHCbMath::scalarMomentum SCALAR_MOMENTUM;
  Functors::LHCbMath::invariantMass  INVARIANT_MASS;

  BOOST_CHECK_EQUAL( SCALAR_MOMENTUM( P4 ), P4.scalarMomentum() );
  BOOST_CHECK_EQUAL( INVARIANT_MASS( P4 ), P4.invariantMass() );

  // Particle Params
  using Gaudi::Units::cm;
  Gaudi::Math::Point3DWithError pos;
  Gaudi::Math::ValueWithError   decay_length{0.1 * cm, 1.e-8 * cm};
  Gaudi::Vector3                len_pos = {0.01, 0.01, 0.01};
  Gaudi::Vector4                len_mom = {0.01, 0.01, 0.01, 0.01};
  Gaudi::Matrix4x3              pos_mom;
  pos_mom( 0, 0 ) = 0.01;
  pos_mom( 1, 1 ) = 0.01;
  pos_mom( 2, 2 ) = 0.01;
  Gaudi::Math::ParticleParams params{pos, P4, decay_length, pos_mom, len_pos, len_mom};

  Functors::LHCbMath::ParticleParams::ctau           CTAU;
  Functors::LHCbMath::ParticleParams::flightDistance FLIGHT_DISTANCE;
  Functors::LHCbMath::ParticleParams::lenPosCov      LEN_POS_COV;
  Functors::LHCbMath::ParticleParams::lenMomCov      LEN_MOM_COV;

  BOOST_CHECK_EQUAL( CTAU( params ), params.ctau() );
  BOOST_CHECK_EQUAL( FLIGHT_DISTANCE( params ), params.flightDistance() );
  BOOST_CHECK_EQUAL( LEN_POS_COV( params ), params.lenPosCov() );
  BOOST_CHECK_EQUAL( LEN_MOM_COV( params ), params.lenMomCov() );
}

BOOST_AUTO_TEST_CASE( test_require_close ) {
  // make some dummy particles
  auto B0       = LHCb::Particle{LHCb::ParticleID( 511 ), 0};
  auto pi_plus  = LHCb::Particle( LHCb::ParticleID( 211 ), 0 );
  auto pi_minus = LHCb::Particle( LHCb::ParticleID( -211 ), 0 );
  pi_plus.setMomentum( Gaudi::LorentzVector( 0., 0., 1000., 5000. ) );
  pi_minus.setMomentum( Gaudi::LorentzVector( 0., 0., 1000., 5000. ) );
  // set B0 daughters
  SmartRefVector<LHCb::Particle> B0_children;
  B0_children.push_back( &pi_plus );
  B0_children.push_back( &pi_minus );
  B0.setDaughters( B0_children );

  // define the functors and prepare them
  auto const                   PT = chain( Functors::Common::Rho_Coordinate{}, Functors::Track::ThreeMomentum{} );
  const auto                   CHILD1_PT      = Functors::Adapters::Child{1, PT};
  const auto                   CHILD2_PT      = Functors::Adapters::Child{2, PT};
  auto const                   _REQUIRE_CLOSE = Functors::Common::RequireClose( 1e-34f, 1e-07f );
  auto const                   REQUIRE_CLOSE  = Functors::bind( _REQUIRE_CLOSE, CHILD1_PT, CHILD2_PT );
  EventContext const           evtCtx;
  Functors::TopLevelInfo const top_level;
  auto                         REQUIRE_CLOSE_prepared = REQUIRE_CLOSE.prepare( evtCtx, top_level );

  // check that it is equal
  BOOST_CHECK_EQUAL( REQUIRE_CLOSE_prepared( true, B0 ), true );
}

BOOST_AUTO_TEST_CASE( test_mc_originflag ) {
  Functors::Simulation::MC::OriginFlag of;
  const LHCb::MCParticle               mc1{};
  const LHCb::MCParticle               mc2{};
  BOOST_CHECK_EQUAL( LHCb::FlavourTagging::originTagCodes::Prompt_From_SignalB_PV, of( &mc1, &mc2 ) );
}

BOOST_AUTO_TEST_CASE( test_muon_nhits ) {
  // Fills each station's first region with as many hits as the station index.
  constexpr unsigned int                                     nStations( 5 );
  boost::container::static_vector<CommonMuonHits, nStations> hits( nStations );
  std::array<CommonMuonStation, nStations>                   stations;
  for ( unsigned int station = 0; station < nStations; ++station ) {
    for ( unsigned int iHit = 0; iHit < station; iHit++ ) {
      hits[station].emplace_back( LHCb::Detector::Muon::TileID{0}, 0., 0., 0., 0., 0., 0., false, 0, 0 );
    }
    // create a MuonHitContainer to be returned and stored in the TES
    // For the upgrade nStations == 4, and M1 has been removed, so use
    // 1-5, for Run II use 0-5
    stations[station].setHits( std::move( hits[station] ) );
  }
  LHCb::Pr::Hits<LHCb::Pr::HitType::Muon> muon_hits{std::move( stations )};
  for ( int station = 0; station < 4; station++ )
    for ( int region = 0; region < 4; region++ ) {
      Functors::TES::NHitsInMuon nHits( station, region );
      if ( region == 0 )
        BOOST_CHECK_EQUAL( station, nHits( muon_hits ) );
      else
        BOOST_CHECK_EQUAL( 0, nHits( muon_hits ) );
    }
}
