/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKINTERFACES_IPARTICLETRAJPROVIDER_H
#define TRACKINTERFACES_IPARTICLETRAJPROVIDER_H 1

// Include files
// -------------
#include "GaudiKernel/IAlgTool.h"

// From TrackEvent
#include "TrajParticle.h"

static const InterfaceID IID_IParticleTrajProvider( "IParticleTrajProvider", 1, 0 );

class IParticleTrajProvider : virtual public IAlgTool {
public:
  /// Retrieve interface ID
  static const InterfaceID& interfaceID() { return IID_IParticleTrajProvider; }

  /// returns a TrajParticle object for use in vertexing
  virtual std::unique_ptr<LHCb::TrajParticle> trajectory( const LHCb::Particle& particle ) const = 0;
  virtual std::unique_ptr<LHCb::TrajParticle> trajectory( const LHCb::Track& track ) const       = 0;
};

#endif
