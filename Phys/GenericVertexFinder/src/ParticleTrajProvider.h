/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "TrajParticle.h"

#include "GaudiKernel/IAlgTool.h"

#include "DetDesc/IGeometryInfo.h"
struct ITrackStateProvider;

namespace ParticleTrajProvider {
  // some global functions to create TrajParticles
  std::unique_ptr<LHCb::TrajParticle> trajectory( ITrackStateProvider const& stateprovider,
                                                  LHCb::Particle const& particle, IGeometryInfo const& geometry );
  std::unique_ptr<LHCb::TrajParticle> trajectory( ITrackStateProvider const& stateprovider, LHCb::Track const& particle,
                                                  IGeometryInfo const& geometry );
} // namespace ParticleTrajProvider
