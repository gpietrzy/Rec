###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/JetAccessories
-------------------
#]=======================================================================]

gaudi_add_header_only_library(JetAccessoriesLib
    LINK
        Gaudi::GaudiKernel
        LHCb::CaloDetLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        Rec::DaVinciInterfacesLib
)

gaudi_add_module(JetAccessories
    SOURCES
        src/FastJetBuilder.cpp
        src/JetTag.cpp
        src/ParticleFlowFilter.cpp
        src/ParticleFlowMaker.cpp
        src/ParticleFlowMakerMC.cpp
    LINK
        AIDA::aida
        Boost::headers
        FastJet::FastJet
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        JetAccessoriesLib
        LHCb::CaloDetLib
        LHCb::CaloFutureUtils
        LHCb::DetDescLib
        LHCb::DigiEvent
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
        Rec::DaVinciTypesLib
        Rec::LoKiPhysLib
        Rec::TrackInterfacesLib
        ROOT::GenVector
        ROOT::Hist
        ROOT::MathCore
)

gaudi_add_dictionary(JetAccessoriesDict
    HEADERFILES dict/JetAccessoriesDict.h
    SELECTION dict/JetAccessories.xml
    LINK JetAccessoriesLib
)
