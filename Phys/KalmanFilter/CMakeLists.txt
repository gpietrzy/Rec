###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/KalmanFilter
-----------------
#]=======================================================================]

gaudi_add_library(KalmanFilter
    SOURCES
        src/FastVertex.cpp
        src/IParticleClassifier.cpp
        src/VertexFit.cpp
        src/VertexFitWithTracks.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::DetDescLib
            LHCb::LHCbMathLib
            LHCb::PhysEvent
            LHCb::TrackEvent
            Rec::DaVinciTypesLib
        PRIVATE
            LHCb::CaloFutureUtils
            Rec::DaVinciInterfacesLib
)

gaudi_add_dictionary(KalmanFilterDict
    HEADERFILES dict/KalmanFilterDict.h
    SELECTION dict/KalmanFilter.xml
    LINK KalmanFilter
)
