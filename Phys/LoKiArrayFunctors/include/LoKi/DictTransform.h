/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/IParticleDictTool.h" // Interface used by this tool

#include "LoKi/PhysTypes.h"

#include "GaudiAlg/GaudiTool.h"

#include <map>
#include <string>

namespace LoKi {
  namespace Hybrid {

    /** @class DictTransform
     *  Generic implementation of a Dict->Dict Transformation
     *  The input dictionairy is aquired through an IParticleDictTool
     *  which has to be added to the tool
     *  the transformed dictionary is returned through IParticleDictTool
     *  The transformation is templated
     *  The template parameter Transform needs to fulfill the following policy
     *  TransForm::Init(const std::map<std::string,std::string>& options, ostream&)
     *  TransForm::operator()(const IParticleDictTool::DICT& inDict, IParticleDictTool::Dict& outDict) const
     *
     *  @author Sebastian Neubert
     *  @date   2013-07-10
     */
    template <class Transform>
    class DictTransform : public GaudiTool, virtual public IParticleDictTool {

    public:
      /// Standard constructor
      DictTransform( const std::string& type, const std::string& name, const IInterface* parent )
          : GaudiTool( type, name, parent ) {
        declareInterface<IParticleDictTool>( this );
        declareProperty( "Source", m_sourcename, "Type/Name for Dictionary Tool" );
        declareProperty( "Options", m_options, "dictionary of options passed to the Transform" );
      }

      StatusCode initialize() override {
        const StatusCode sc = GaudiTool::initialize();
        if ( sc.isFailure() ) { return sc; } // RETURN
        // acquire the DictTool
        m_source = tool<IParticleDictTool>( m_sourcename, this );
        if ( !m_source ) { return Error( "Unable to find the source DictTool " + m_sourcename ); }
        // initialise the transform
        if ( !m_transform.Init( m_options, debug().stream(), msgLevel( MSG::DEBUG ) ) ) {
          return Error( "Initialization of Transform failed." );
        }
        debug() << "Successfully initialized DictTransform" << endmsg;
        return sc;
      }

      StatusCode fill( const LHCb::Particle* p, IParticleDictTool::DICT& dict,
                       IGeometryInfo const& geometry ) const override {
        // request the dictionary s from the source DictTool
        IParticleDictTool::DICT sourceDict;
        m_source->fill( p, sourceDict, geometry ).ignore();
        // apply the transformation to the dictionary
        // this is delegated to the Transform
        m_transform( sourceDict, dict );
        return StatusCode::SUCCESS;
      }

    protected:
      IParticleDictTool* m_source = nullptr;
      std::string        m_sourcename;

      /// Templated worker class to encapsulate the transformation algorithm
      Transform                          m_transform;
      std::map<std::string, std::string> m_options;
    };
  } // end namespace Hybrid
} // end namespace LoKi
