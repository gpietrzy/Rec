/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/Particle.h"
#include "Event/Vertex.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Kernel.h"
#include "KalmanFilter/ParticleTypes.h"
#include "Kernel/IParticleClassifier.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/NodesPIDs.h"
#include "LoKi/IDecay.h"
#include "LoKi/Trees.h"
#include <algorithm>
#include <set>
// ============================================================================
namespace LoKi {
  // ==========================================================================
  /** @class ParticleClassificator ParticleClassificator.h
   *
   *  The useful base class for classification of particles
   *  @see LoKi::KalmanFilter::ParticleType
   *
   *  This file is a part of
   *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
   *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
   *
   *  The package has been designed with the kind help from
   *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
   *  contributions and advices from G.Raven, J.van Tilburg,
   *  A.Golutvin, P.Koppenburg have been used in the design.
   *
   *  @author Vanya Belyaev Ivan.Belyaev@cern.ch
   *  @date   2010-11-11
   */
  class ParticleClassificator : public extends<GaudiTool, IParticleClassifier> {
  public:
    // ========================================================================
    /// initialize the tool
    StatusCode initialize() override; // initialize the tool
    /// finalize   the tool
    StatusCode finalize() override; // finalize   the tool
    // ========================================================================
  public: // see IParticleClassifier
    // ========================================================================
    /** get the particle type
     *  @see LoKi::KalmanFilter::ParticleType
     */
    LoKi::KalmanFilter::ParticleType particleType( const LHCb::Particle* p ) const override {
      return particleType_( p );
    }
    // ==========================================================================
    /** check the particle type
     *  @see LoKi::KalmanFilter::ParticleType
     */
    bool isParticleType( const LHCb::Particle* p, const LoKi::KalmanFilter::ParticleType t ) const override {
      return p && isParticleType_( *p, t );
    }
    // ========================================================================
    /** good for vertex ?
     *  @attention This definiton is <b>different</b> from the
     *  definition by LoKi::KalmanFilter::okForVertex
     *  @see LoKi::KalmanFilter::okForVertex
     */
    bool goodForVertex( const LHCb::Particle::Range& parts ) const override { return goodForVertex_( parts ); }
    // ========================================================================
  protected:
    // ========================================================================
    /** get the particle type
     *  @see LoKi::KalmanFilter::ParticleType
     */
    LoKi::KalmanFilter::ParticleType particleType_( const LHCb::Particle& p ) const;
    // ========================================================================
    /** get the particle type
     *  @see LoKi::KalmanFilter::ParticleType
     */
    LoKi::KalmanFilter::ParticleType particleType_( const LHCb::Particle* p ) const {
      return p ? particleType_( *p ) : LoKi::KalmanFilter::UnspecifiedParticle;
    }
    // ========================================================================
    /** check the particle type
     *  @see LoKi::KalmanFilter::ParticleType
     */
    bool isParticleType_( const LHCb::Particle& p, const LoKi::KalmanFilter::ParticleType t ) const {
      return particleType_( p ) == t;
    }
    // ========================================================================
    /** good for vertex ?
     *  @attention This definiton is <b>different</b> from the
     *  definition by LoKi::KalmanFilter::okForVertex
     *  @see LoKi::KalmanFilter::okForVertex
     */
    bool goodForVertex_( const LHCb::Particle::Range& parts ) const {
      /// two or more long-lived particles are required for vertex
      return 2 <= nForVertex( parts.begin(), parts.end() );
    }
    // ========================================================================
    /** check if particle is rho+like one:
     *  Exactly 1 long lived particle in the decay tree
     */
    bool rhoPlusLike_( const LHCb::Particle* particle ) const;
    /** get long-lived particle from rho+-like particle
     */
    const LHCb::Particle* longFromRhoPlusLike_( const LHCb::Particle* particle ) const;
    // ========================================================================
  protected:
    // ========================================================================
    /// get the correct algorithm context
    bool getMyAlg() const;
    // ========================================================================
  public:
    // ========================================================================
    /** standard constructor
     *  @param type   the actual type of the tool
     *  @param name   the instance name
     *  @param parent the parent
     */
    ParticleClassificator( const std::string& type,    //         the actual tool type (?)
                           const std::string& name,    //            the instance name
                           const IInterface*  parent ); //                   the parent
    // ========================================================================
  private:
    // ========================================================================
    /// good gor vertex ?
    template <class PARTICLE>
    std::size_t nForVertex( PARTICLE first, PARTICLE last ) const;
    // ========================================================================
  private:
    // ========================================================================
    /// particle property service
    mutable const LHCb::IParticlePropertySvc* m_ppSvc;
    /// Long-lived particles
    mutable Decays::Nodes::LongLived_ m_longLived;
    /// Short-lived particles
    mutable Decays::Nodes::ShortLived_ m_shortLived;
    /// Gamma-like particles
    mutable Decays::Trees::Stable_<const LHCb::Particle*> m_gammaLike;
    /// GammaC-like particles (gamma-> e+ e-)
    mutable Decays::IDecay::Tree m_gammaCLike;
    /// Di-Gamma-like particles ( pi0 -> gamma gamma , eta -> gamma gamma )
    mutable Decays::IDecay::Tree m_digammaLike;
    /// Merged-pi0-like particles
    mutable Decays::Trees::Stable_<const LHCb::Particle*> m_mergedPi0Like;
    /// get like particle
    Decays::Node m_jetsLike;
    // ========================================================================
    /// decay descriptor for gammaC-like particles:
    std::string m_dd_gammaC; //   decay descriptor for gammaC-like particles
    /// decay descriptor for di-gamma-like particles:
    std::string m_dd_digamma; // decay descriptor for di-gamma-like particles
    /// descriptor for jets-like particle
    std::string m_dd_jets; // descriptor for jets-like particle
    // ========================================================================
  private:
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_jets_warning{
        this, "The special treatment of Jets-like objects is disabled"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_digamma_warning{
        this, "The special treatment of DiGamma is disabled"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_gammac_warning{
        this, "The special treatment of Gamma_c is disabled"};
    // ========================================================================
    /// Unclassified particles
    mutable std::set<LHCb::ParticleID> m_unclassified;
    mutable std::set<LHCb::ParticleID> m_gamma_like;
    mutable std::set<LHCb::ParticleID> m_gammaC_like;
    mutable std::set<LHCb::ParticleID> m_digamma_like;
    mutable std::set<LHCb::ParticleID> m_mergedPi0_like;
    mutable std::set<LHCb::ParticleID> m_jets_like;
    mutable std::set<LHCb::ParticleID> m_rhoplus_like;
    // ========================================================================
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
// good for vertex ?
// ============================================================================
template <class PARTICLE>
std::size_t LoKi::ParticleClassificator::nForVertex( PARTICLE first, PARTICLE last ) const {
  //
  std::size_t nTr = 0;
  //
  for ( ; first != last; ++first ) {
    const LHCb::Particle* p = *first;
    if ( !p ) { continue; }
    //
    switch ( particleType_( p ) ) {
      // 1. for long-lived particles
    case LoKi::KalmanFilter::LongLivedParticle:
      ++nTr;
      break;
      // 2. "rho+-ilke" particles also have exactly one long lived particle
    case LoKi::KalmanFilter::RhoPlusLikeParticle:
      ++nTr;
      break;
      // number for long-lived particles for short lived resonance
    case LoKi::KalmanFilter::ShortLivedParticle: {
      const auto& daughters = p->daughters();
      nTr += nForVertex( daughters.begin(), daughters.end() );
      break;
    }
    default:;
    }
    //
  }
  return nTr;
}
// ============================================================================
// get long-type particle from rho+-like paricle
// ============================================================================
inline const LHCb::Particle* LoKi::ParticleClassificator::longFromRhoPlusLike_( const LHCb::Particle* particle ) const {
  if ( !particle ) { return nullptr; } // RETURN
  // get daughters
  const auto& daughters = particle->daughters();
  if ( 2 > daughters.size() ) { return nullptr; } // REUTRN
  //
  // require exactly one long-lived daughter particle
  for ( auto& dau : daughters ) {
    // the type of daughter particle
    switch ( particleType_( dau ) ) {
    case LoKi::KalmanFilter::LongLivedParticle:
      return dau; // RETURN
      // case LoKi::KalmanFilter::RhoPlusLikeParticle :               // NB!
      // return longFromRhoPlus_ ( dau ) ;                          // RETURN
    case LoKi::KalmanFilter::ShortLivedParticle:
      return nullptr; // RETURN
    case LoKi::KalmanFilter::JetLikeParticle:
      return nullptr; // RETURN
    default:;
    }
  }
  //
  return nullptr; // RETURN
}
// ===========================================================================
// rho+-like category ?
// ============================================================================
inline bool LoKi::ParticleClassificator::rhoPlusLike_( const LHCb::Particle* particle ) const {
  if ( !particle || particle->isBasicParticle() ) { return false; } // RETURN
  // get daughters
  const auto& daughters = particle->daughters();
  if ( 2 > daughters.size() ) { return false; } // REUTRN
  //
  // require exactly one long-lived daughter particle
  unsigned short nLong = 0;
  for ( auto const& idau : daughters ) {
    // the type of daughter particle
    switch ( particleType_( idau ) ) {
    case LoKi::KalmanFilter::LongLivedParticle:
      ++nLong;
      break;
      // case LoKi::KalmanFilter::RhoPlusLikeParticle : ++nLong ; break ;  // NB!!
    case LoKi::KalmanFilter::ShortLivedParticle:
      return false; // RETURN
    case LoKi::KalmanFilter::JetLikeParticle:
      return false; // RETURN
    default:;
    }
    if ( 1 < nLong ) { return false; }
  }
  //
  return 1 == nLong;
}
// ===========================================================================

// ============================================================================
// The END
// ============================================================================
