/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIPHYSDICT_H
#  define LOKI_LOKIPHYSDICT_H 1
// ============================================================================
// build/Phys/LoKiPhys/LoKiPhysDict.cpp:26369:38: warning: string literal of
// length 95823 exceeds maximum length 65536
// that C++ compilers are required to support [-Woverlength-strings]
//    static const char* fwdDeclCode = R"DICTFWDDCLS(
// See issue Phys#4 for further discussion
#  if defined( __clang__ )
#    pragma clang diagnostic ignored "-Woverlength-strings"
#  endif
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/LoKiPhys_dct.h"
// ============================================================================
/** @file
 *  The dictionaries for the package Phys/LoKiPhys
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2007-12-01
 */
// ============================================================================
//                                                                     The END
// ============================================================================
#endif // LOKI_LOKIPHYSDICT_H
// ============================================================================
