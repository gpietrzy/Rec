/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_AUXDTFBASE_H
#  define LOKI_AUXDTFBASE_H 1
// ============================================================================
// Include files
// ============================================================================
// Include files
// ============================================================================
// DaVinciInterfaces
// ============================================================================
#  include "Kernel/IDecayTreeFit.h"
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/AuxDesktopBase.h"
#  include "LoKi/Interface.h"
// ============================================================================
/** @file LoKi/AuxDTFBase.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2010-06-03
 */
namespace LoKi {
  // =========================================================================
  /** @class AuxDTFBase LoKi/AuxDTFBase.h
   *
   *  Helper base class to deal with DecayTreeFitter
   *  @see IDecayTreeFit
   *  @see  DecayTreeFit::Fitter
   *
   *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
   *  @date   2010-06-03
   */
  class GAUDI_API AuxDTFBase : public virtual LoKi::AuxDesktopBase {
  public:
    // ========================================================================
    /// the actual type to define the non-standard masses
    typedef std::map<std::string, double> MASSES;
    /// the actual type to define the non-standard masses
    typedef std::map<LHCb::ParticleID, double> MASSES2;
    // ========================================================================
  public:
    // ========================================================================
    /// constructor from the algorithm
    AuxDTFBase( const IDVAlgorithm* algorithm, const std::string& fitter = "" );
    /// constructor from the algorithm
    AuxDTFBase( const IDVAlgorithm* algorithm, const std::vector<std::string>& constraints,
                const MASSES& masses = MASSES(), const std::string& fitter = "" );
    /// constructor from the algorithm
    AuxDTFBase( const IDVAlgorithm* algorithm, const std::vector<LHCb::ParticleID>& constraints,
                const MASSES& masses = MASSES(), const std::string& fitter = "" );
    /// constructor from the algorithm
    AuxDTFBase( const IDVAlgorithm* algorithm, const std::vector<std::string>& constraints, const MASSES2& masses,
                const std::string& fitter = "" );
    /// constructor from the algorithm
    AuxDTFBase( const IDVAlgorithm* algorithm, const std::vector<LHCb::ParticleID>& constraints, const MASSES2& masses,
                const std::string& fitter = "" );
    /// copy constructor
    AuxDTFBase( const AuxDTFBase& right );
    /// virtual destructor
    virtual ~AuxDTFBase();
    // ========================================================================
  private:
    // ========================================================================
    /// the default constructor is disabled
    AuxDTFBase(); // the default constructor is disabled
    // ========================================================================
  public:
    // ========================================================================
    /// get the fitter
    IDecayTreeFit* fitter() const; // get the fitter
    /// load the fitter
    void loadFitter( const std::string& name ) const;
    /// get the fitter name
    const std::string& fitterName() const { return m_fitterName; }
    /// get constraints
    std::vector<std::string> constraints() const;
    /// get non-standard masses
    MASSES masses() const;
    // ========================================================================
  protected:
    // ========================================================================
    /// set vector of constraints
    unsigned int setConstraint( const std::vector<std::string>& pids );
    /// set constraint
    unsigned int setConstraint( const std::string& pids );
    /// set vector of constraints
    unsigned int setConstraint( const std::vector<LHCb::ParticleID>& pids );
    /// set constraints
    unsigned int setConstraint( const LHCb::ParticleID& pids );
    /// set vector of constraints
    unsigned int setConstraint( const MASSES& pids );
    /// set vector of constraints
    unsigned int setConstraint( const MASSES2& pids );
    // ========================================================================
  public:
    // ========================================================================
    // apply mass-constraints
    void applyConstraints() const;
    /// print constraints
    std::ostream& printConstraints( std::ostream& s ) const;
    // ========================================================================
  private:
    // ========================================================================
    /// the fitter name
    std::string m_fitterName{}; // the fitter name
    /// the fitter itself
    mutable LoKi::Interface<IDecayTreeFit> m_fitter{nullptr}; // the fiter itself
    /// the list of mass constraints
    std::vector<LHCb::ParticleID> m_constraints{}; // mass-constarints
    /// the list of mass constraints
    MASSES2 m_masses{}; // mass-constraints
    // ========================================================================
  };
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_AUXDTFBASE_H
// ============================================================================
