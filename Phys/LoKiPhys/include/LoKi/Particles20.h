/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/Particles1.h"
#include "LoKi/Particles16.h"
#include "LoKi/Particles19.h"
#include "LoKi/Particles4.h"
#include "LoKi/PhysSources.h"
#include "LoKi/PhysTypes.h"
#include "LoKi/Vertices5.h"

/** @file
 *  Collection of "context-dependent" functors, needed for the
 *  new framework "CombineParticles", developed by Juan PALACIOS,
 *   Patrick KOPPENBURG and Gerhard RAVEN.
 *
 *  Essentially all these functord depends on "event-data" and
 *  get the nesessary "context-dependent" data from Algorithm Context Service
 *
 *  The basic ingredients here:
 *   - LoKi Service
 *   - Algorithm Context Service
 *   - PhysDesktop
 *   - LoKi::getPhysDesktop
 *   - Gaudi::Utils::getDValgorithm
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2008-01-16
 */
namespace LoKi {

  namespace Particles {

    /** @class IsBestPVValid
     *  Trivial predicate which evaluates to true
     *  for particles with a valid best primary vertex,
     *  useful to test if PV refit is successful
     *
     *  It relies on the method bestVertex of DVCommonBase
     *
     *  @see LHCb::Particle
     *  @see LoKi::Cuts::BPVVALID
     *
     *  @author Sascha Stahl sascha.stahl@cern.ch
     *  @date 2016-05-29
     */
    struct GAUDI_API IsBestPVValid : LoKi::BasicFunctors<const LHCb::Particle*>::Predicate,
                                     virtual LoKi::AuxDesktopBase {
      /// Default Constructor
      IsBestPVValid( const IDVAlgorithm* algorithm );
      /// clone method (mandatory!)
      IsBestPVValid* clone() const override;
      /// the only one essential method
      result_type operator()( argument p ) const override;
      /// the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class CosineDirectionAngleWithTheBestPV
     *  The special version of LoKi::Particles::CosineDirectionAngle functor,
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVDIRA
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::CosineDirectionAngle
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    struct GAUDI_API CosineDirectionAngleWithTheBestPV : LoKi::Particles::CosineDirectionAngle,
                                                         virtual LoKi::AuxDesktopBase {
      /// the default constructor, creates the object in invalid state
      CosineDirectionAngleWithTheBestPV( const IDVAlgorithm* algorithm );
      /// MANDATORY: the clone method ("virtual constructor")
      CosineDirectionAngleWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class TzWithTheBestPV
     *  The special version of LoKi::Particles::Tz functor,
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVTZ
     *  @see LoKi::Cuts::TZ
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2016-06-20
     */
    struct GAUDI_API TzWithTheBestPV : LoKi::Particles::Tz, virtual LoKi::AuxDesktopBase {
      /** the default constructor,
       *  @param mass to be used, if negatiev - use the particle mass
       */
      TzWithTheBestPV( const IDVAlgorithm* algorithm, const double mass = -1000 );
      /// MANDATORY: the clone method ("virtual constructor")
      TzWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class ImpParWithTheBestPV
     *  The special version of LoKi::Particles::ImpPar functor
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVIP
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::ImpPar
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    class GAUDI_API ImpParWithTheBestPV : public LoKi::Particles::ImpPar, public virtual LoKi::AuxDesktopBase {
    public:
      /** the "default" constructor,
       *  gets the IDistanceCalculator tool from DVAlgorithm by nickname or
       *  by full type/name
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      ImpParWithTheBestPV( const IDVAlgorithm* algorithm, const std::string& geo = "" );
      /// MANDATORY: the clone method ("virtual constructor")
      ImpParWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      const std::string& geo() const { return m_geo; }

    private:
      /// the nick name or type name of the IDistanceCalculator
      std::string m_geo;
    };

    /** @class ImpParChi2WithTheBestPV
     *  The special version of LoKi::Particles::ImpParChi2  functor
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVIPCHI2
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::ImpParChi2
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    struct GAUDI_API ImpParChi2WithTheBestPV : ImpParWithTheBestPV {

      /** the "default" constructor,
       *  gets the IDistanceCalculator tool from DVAlgorithm by nickname or
       *  by full type/name
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      ImpParChi2WithTheBestPV( const IDVAlgorithm* algorithm, const std::string& geo = "" );
      /// MANDATORY: the clone method ("virtual constructor")
      ImpParChi2WithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class MinImpParWithSource
     *  The special version of LoKi::Particles::MinImpPar functor
     *  which gets all the primary vertices form the "source"
     *
     *  @see LoKi::Cuts::MIPSOURCE
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::MinImpPar
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    class GAUDI_API MinImpParWithSource : public LoKi::Particles::MinImpPar {
    protected:
      // the source of vertices
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;

    public:
      // the source of vertices
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source Source;

      /** constructor from the source and nickname or full type/name of
       *  IDistanceCalculator tool
       *  @see DVAlgorithm::distanceCalculator
       *  @param source the source
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParWithSource( const IDVAlgorithm* algorithm, const _Source& source, const std::string& geo = "" );
      /// MANDATORY: clone method ("virtual constructor")
      MinImpParWithSource* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// get the source
      const LoKi::BasicFunctors<const LHCb::VertexBase*>::Source& source() const { return m_source; }
      /// cast to the source
      operator const LoKi::BasicFunctors<const LHCb::VertexBase*>::Source&() const { return m_source; }

      // get the nickname of full type/name of IDistanceCalculator tool
      const std::string& geo() const { return m_geo; }

    private:
      // no default constructor
      MinImpParWithSource(); ///< no default constructor

      /// the source
      LoKi::Assignable<_Source>::Type m_source; // the source
      /// the nickname or type/name of IDistanceCalculator tool
      std::string m_geo; // IDistanceCalculator tool
    };

    /** @class MinImpParDV
     *  The special version of LoKi::Particles::MinImpParWithSource functor
     *  which gets all the primary vertices from the Desktop
     *
     *  @see LoKi::Cuts::MIPDV
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::MinImpPar
     *  @see LoKi::Particles::MinImpParWithSource
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    struct GAUDI_API MinImpParDV : LoKi::Particles::MinImpParWithSource, virtual LoKi::AuxDesktopBase {

      /** the "default" constructor,
       *  gets the IDistanceCalculator tool from DVAlgorithm by nickname or
       *  by full type/name
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParDV( const IDVAlgorithm* algorithm, const std::string& geo = "" );
      /** the constructor form the vertex selection functot and
       *  the name/nickname of IDistanceCalculator tool from DVAlgorithm
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParDV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cuts, const std::string& geo = "" );
      /** the constructor form the vertex selection functot and
       *  the name/nickname of IDistanceCalculator tool from DVAlgorithm
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParDV( const IDVAlgorithm* algorithm, const std::string& geo, const LoKi::PhysTypes::VCuts& cuts );
      /// MANDATORY: clone method ("virtual constructor")
      MinImpParDV* clone() const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class MinImpParChi2WithSource
     *  The special version of LoKi::Particles::MinImpParChi2 functor
     *  which gets all the primary vertoices form "source"
     *
     *  @see LoKi::Cuts::MIPCHI2SOURCE
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::MinImpParChi2
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    class GAUDI_API MinImpParChi2WithSource : public LoKi::Particles::MinImpParWithSource {
    public:
      /** constructor from the source and nickname or full type/name of
       *  IDistanceCalculator tool
       *  @see DVAlgorithm::distanceCalculator
       *  @param source the source
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParChi2WithSource( const IDVAlgorithm* algo, const _Source& source, const std::string& geo = "" );
      /// MANDATORY: clone method ("virtual constructor")
      MinImpParChi2WithSource* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

    private:
      /// no default constructor
      MinImpParChi2WithSource(); // no default constructor
    };

    /** @class MinImpParChi2DV
     *  The special version of LoKi::Particles::MinImpParChi2WithSource functor
     *  which gets all the primary vertices from the Desktop
     *
     *  @see LoKi::Cuts::MIPCHI2DV
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::MinImpParChi2
     *  @see LoKi::Particles::MinImpParChi2WithSource
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-16
     */
    struct GAUDI_API MinImpParChi2DV : LoKi::Particles::MinImpParChi2WithSource, virtual LoKi::AuxDesktopBase {

      /** the "default" constructor,
       *  gets the IDistanceCalculator tool from DVAlgorithm by nickname or
       *  by full type/name
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParChi2DV( const IDVAlgorithm* algorithm, const std::string& geo = "" );
      /** the constructor,
       *  gets the IDistanceCalculator tool from DVAlgorithm by nickname or
       *  by full type/name
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParChi2DV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cuts, const std::string& geo = "" );
      /** the  constructor,
       *  gets the IDistanceCalculator tool from DVAlgorithm by nickname or
       *  by full type/name
       *  @see DVAlgorithm::distanceCalculator
       *  @param geo the nickname (or type/name)  of IDistanceCalculator tool
       */
      MinImpParChi2DV( const IDVAlgorithm* algorithm, const std::string& geo, const LoKi::PhysTypes::VCuts& cuts );
      /// MANDATORY: clone method ("virtual constructor")
      MinImpParChi2DV* clone() const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class VertexDistanceDV
     *  The special version of LoKi::Particles::VertexDistance functor
     *  which gets "the best primary vertex" from the Desktop
     *
     *  @see LoKi::Cuts::BPVVD
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::VertexDistance
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API VertexDistanceDV : LoKi::Particles::VertexDistance, virtual LoKi::AuxDesktopBase {

      /// the default constructor
      VertexDistanceDV( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      VertexDistanceDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class VertexSignedDistanceDV
     *  The special version of LoKi::Particles::VertexSignedDistance functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *
     *  @see LoKi::Cuts::BPVVDSIGN
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::VertexSignedDistance
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API VertexSignedDistanceDV : LoKi::Particles::VertexSignedDistance, virtual LoKi::AuxDesktopBase {

      /// the default constructor
      VertexSignedDistanceDV( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      VertexSignedDistanceDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class VertexDotDistanceDV
     *  The special version of LoKi::Particles::VertexDotDistance functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *
     *  @see LoKi::Cuts::BPVVDDOT
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::VertexDotDistance
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API VertexDotDistanceDV : LoKi::Particles::VertexDotDistance, virtual LoKi::AuxDesktopBase {
    public:
      /// the default constructor
      VertexDotDistanceDV( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      VertexDotDistanceDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class VertexChi2DistanceDV
     *  The special version of LoKi::Particles::VertexChi2Distance functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *
     *  @see LoKi::Cuts::BPVVDCHI2
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::VertexChi2Distance
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API VertexChi2DistanceDV : LoKi::Particles::VertexChi2Distance, virtual LoKi::AuxDesktopBase {

      /// the default constructor
      VertexChi2DistanceDV( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      VertexChi2DistanceDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class LifeTimeDV
     *  The special version of LoKi::Particles::LifeTime functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *  and ILifetimeFitter from GaudiAlgorithm
     *
     *  @see LoKi::Cuts::BPVLTIME
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::LifeTime
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    class GAUDI_API LifeTimeDV : public LoKi::Particles::LifeTime {
    public:
      /// the "default" constructor
      LifeTimeDV( const IDVAlgorithm* algorithm, const double chi2cut = -1 );
      /// the constructor form tool type/name
      LifeTimeDV( const IDVAlgorithm* algorithm, const std::string& fit, const double chi2cut = -1 );
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      const std::string& fitter() const { return m_fit; }

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

    private:
      /// the tool type/name
      std::string m_fit; // the tool type/name
    };

    /** @class LifeTimeChi2DV
     *  The special version of LoKi::Particles::LifeTimeChi2 functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *  and ILifetimeFitter from GaudiAlgorithm
     *
     *  @see LoKi::Cuts::BPVLTCHI2
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::LifeTimeChi2
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeChi2DV : LoKi::Particles::LifeTimeDV {

      /// the "default" constructor
      LifeTimeChi2DV( const IDVAlgorithm* algorithm, const double chi2cut = -1 );
      /// the constructor form tool type/name
      LifeTimeChi2DV( const IDVAlgorithm* algorithm, const std::string& fit, const double chi2cut = -1 );
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeChi2DV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class LifeTimeSignedChi2DV
     *  The special version of LoKi::Particles::LifeTimeChi2 functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *  and ILifetimeFitter from GaudiAlgorithm
     *
     *  @see LoKi::Cuts::BPVLTSIGNCHI2
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::LifeTimeSignedChi2
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeSignedChi2DV : LoKi::Particles::LifeTimeChi2DV {

      /// the "default" constructor
      LifeTimeSignedChi2DV( const IDVAlgorithm* algorithm, const double chi2cut = -1 );
      /// the constructor form tool type/name
      LifeTimeSignedChi2DV( const IDVAlgorithm* algorithm, const std::string& fit, const double chi2cut = -1 );
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeSignedChi2DV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class LifeTimeFitChi2DV
     *  The special version of LoKi::Particles::LifeTimeFitChi2 functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *  and ILifetimeFitter from DVAlgorithm
     *
     *  @see LoKi::Cuts::BPVLTFITCHI2
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::LifeTimeSignedChi2
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeFitChi2DV : LoKi::Particles::LifeTimeDV {

      /// the "default" constructor
      LifeTimeFitChi2DV( const IDVAlgorithm* algorithm );
      /// the constructor form tool type/name
      LifeTimeFitChi2DV( const IDVAlgorithm* algorithm, const std::string& fit );
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeFitChi2DV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class LifeTimeErrorDV
     *  The special version of LoKi::Particles::LifeTimeFitChi2 functor
     *  which gets "the best primary vertex" from IPhysDesktop
     *  and ILifetimeFitter from DVAlgorithm
     *
     *  @see LoKi::Cuts::BPVLTERR
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API LifeTimeErrorDV : LoKi::Particles::LifeTimeDV {

      /// the "default" constructor
      LifeTimeErrorDV( const IDVAlgorithm* algorithm );
      /// the constructor form tool type/name
      LifeTimeErrorDV( const IDVAlgorithm* algorithm, const std::string& fit );
      /// MANDATORY: clone method ("virtual constructor")
      LifeTimeErrorDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class VertexZDistanceWithTheBestPV
     *  The functor which evaluates Delta(Z) for the end-vertex of the
     *  particle and "the best primary vertex" from the Desktop
     *
     *  @see LoKi::Cuts::BPVVDZ
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *
     *  The concept and the name come
     *     from Sean BRISBANE s.brisbane1@physics.ox.ac.uk
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API VertexZDistanceWithTheBestPV : LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                                    virtual LoKi::AuxDesktopBase {

      /// the default constructor
      VertexZDistanceWithTheBestPV( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      VertexZDistanceWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class VertexRhoDistanceWithTheBestPV
     *  The functor which evaluates Delta(Z) for the end-vertex of the
     *  particle and "the best primary vertex" from the Desktop
     *
     *  @see LoKi::Cuts::BPVVDR
     *  @see LoKi::Cuts::BPVVDRHO
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *
     *  The concept and the name come
     *     from Sean BRISBANE s.brisbane1@physics.ox.ac.uk
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2008-01-17
     */
    struct GAUDI_API VertexRhoDistanceWithTheBestPV : LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                                      virtual LoKi::AuxDesktopBase {

      /// the default constructor
      VertexRhoDistanceWithTheBestPV( const IDVAlgorithm* algorithm );
      /// MANDATORY: clone method ("virtual constructor")
      VertexRhoDistanceWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }
    };

    /** @class MinVertexDistanceWithSource
     *  The simple functor which evaluates the minimal distance
     *  between the vertex and vertices from the "source"
     *  @see LoKi::Vertices::MinVertexDistanceWithSource
     *  @see LoKi::Vertices::MinVertexDistance
     *  @see LoKi::Cuts::MINVDSOURCE
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexDistanceWithSource : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {

      /// the source of vertices
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;

    public:
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source Source;

      /// constructor from the source
      MinVertexDistanceWithSource( const _Source& source );
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexDistanceWithSource* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

    private:
      /// the default constructor is disabled
      MinVertexDistanceWithSource(); // the default constructor is disabled

      /// get the underlying functor
      mutable LoKi::Vertices::MinVertexDistanceWithSource m_fun; // the evaluator
    };

    /** @class MinVertexDistanceDV
     *  The special functor
     *  which gets all the primary vertices from the Desktop
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @see LoKi::Cuts::MINVDDV
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexDistanceDV : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      /// the default constructor
      MinVertexDistanceDV( const IDVAlgorithm* algorithm );
      /// the constructor from the vertex filter
      MinVertexDistanceDV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cut );
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexDistanceDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need the context algorithm
      static bool context_dvalgo() { return true; }

    private:
      /// the actual functor
      LoKi::Vertices::MinVertexDistanceDV m_fun; // the actual functor
    };

    /** @class MinVertexChi2DistanceWithSource
     *  The simple functor which evaluates the minimal distance
     *  between the vertex and vertices from the "source"
     *  @see LoKi::Vertices::MinVertexChi2DistanceWithSource
     *  @see LoKi::Vertices::MinVertexChi2Distance
     *  @see LoKi::Cuts::MINVDCHI2SOURCE
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexChi2DistanceWithSource : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {

      // the source of vertices
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source _Source;

    public:
      typedef LoKi::BasicFunctors<const LHCb::VertexBase*>::Source Source;

      /// constructor from the source
      MinVertexChi2DistanceWithSource( const _Source& source );
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexChi2DistanceWithSource* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

    private:
      /// the default constructor is disabled
      MinVertexChi2DistanceWithSource(); // the default constructor is disabled

      /// get the underlying functor
      mutable LoKi::Vertices::MinVertexChi2DistanceWithSource m_fun; // the evaluator
    };

    /** @class MinVertexChi2DistanceDV
     *  The special functor
     *  which gets all the primary vertices from the Desktop
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @see LoKi::Cuts::MINVDCHI2DV
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-03-28
     */
    class GAUDI_API MinVertexChi2DistanceDV : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      /// the default constructor
      MinVertexChi2DistanceDV( const IDVAlgorithm* algorithm );
      /// the constructor from the vertex filter
      MinVertexChi2DistanceDV( const IDVAlgorithm* algorithm, const LoKi::PhysTypes::VCuts& cut );
      /// MANDATORY: clone method ("virtual constructor")
      MinVertexChi2DistanceDV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument v ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

    private:
      /// the actual functor

      LoKi::Vertices::MinVertexChi2DistanceDV m_fun; // the actual functor
    };

    /** @class TrgPointingScoreWithBestPV
     *  The special version of LoKi::Particles::TrgPointingScore functor,
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVTRGPOINTING
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::TrgPointingScore
     *
     *  @attention There are no direct needs to use this "Context"
     *             functor inside the native LoKi-based C++ code,
     *             there are more efficient, transparent,
     *             clear and safe analogues...
     *
     *  @author Patrick SPRADLIN P.Spradlin1@physics.ox.ac.uk
     *  @date 2009-03-10
     */
    struct GAUDI_API TrgPointingScoreWithBestPV : LoKi::Particles::TrgPointingScore, virtual LoKi::AuxDesktopBase {

      /// constructor
      TrgPointingScoreWithBestPV( const IDVAlgorithm* algorithm );
      /// MANDATORY: the clone method ("virtual constructor")
      TrgPointingScoreWithBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override { return s << "BPVTRGPOINTING"; }
    };

    /** @class PseudoRapidityWithTheBestPV
     *  A special version of the LoKi::Particles::PseudoRapidityFromVertex functor,
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVETA
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::PseudoRapidityFromVertex
     *
     *  @author Albert Puig (albert.puig@epfl.ch)
     *  @date 2015-03-03
     */
    struct GAUDI_API PseudoRapidityWithTheBestPV : LoKi::Particles::PseudoRapidityFromVertex,
                                                   virtual LoKi::AuxDesktopBase {

      /// the default constructor, creates the object in invalid state
      PseudoRapidityWithTheBestPV( const IDVAlgorithm* algorithm );
      /// MANDATORY: the clone method ("virtual constructor")
      PseudoRapidityWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

    /** @class PhiWithTheBestPV
     *  A special version of the LoKi::Particles::PhiFromVertex functor,
     *  which gets the related primary vertex from IPhysDesktop tool
     *
     *  @see LoKi::Cuts::BPVPHI
     *  @see IPhysDesktop
     *  @see LoKi::getPhysDesktop
     *  @see LoKi::Particles::PseudoRapidityFromVertex
     *
     *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
     *  @date 2015-04-26
     */
    struct GAUDI_API PhiWithTheBestPV : PseudoRapidityWithTheBestPV {

      /// the default constructor, creates the object in invalid state
      PhiWithTheBestPV( const IDVAlgorithm* algorithm );
      /// MANDATORY: the clone method ("virtual constructor")
      PhiWithTheBestPV* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: the specific printout
      std::ostream& fillStream( std::ostream& s ) const override;
    };

  } // namespace Particles

} // namespace LoKi
