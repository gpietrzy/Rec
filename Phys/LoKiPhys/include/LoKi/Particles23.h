/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LoKi/AuxDesktopBase.h"
#include "LoKi/BasicFunctors.h"
#include "LoKi/Interface.h"
#include "LoKi/Objects.h"
#include "LoKi/WrongMass.h"

#include "Event/Particle.h"
#include "Kernel/IParticleTransporter.h"

/**
 *  Collection of functors to evaluate "wrong" mass
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2008-10-13
 */
namespace LoKi {

  namespace Particles {

    /**
     *  Simple class to evaluate the mass of the mother particle
     *  using "wrong" mass hypotheses for daughter particles
     *  @see LoKi::Cuts::WM
     *  @see LoKi::Cuts::WMASS
     *  @see LoKi::Cuts::AWMASS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-10-13
     */
    class GAUDI_API WrongMass : public LoKi::BasicFunctors<const LHCb::Particle*>::Function,
                                virtual public LoKi::AuxDesktopBase {
    protected:
      enum { InvalidParticleID = 310, InvalidParticleName = 311 };

    public:
      /// constructor from masses
      WrongMass( IDVAlgorithm const* algorithm, const double m1, const double m2,
                 const IParticleTransporter* t = nullptr, const double dz = 5 * Gaudi::Units::mm );

      WrongMass( IDVAlgorithm const* algorithm, const double m1, const double m2, const double m3,
                 const IParticleTransporter* t = nullptr, const double dz = 5 * Gaudi::Units::mm );

      WrongMass( IDVAlgorithm const* algorithm, const double m1, const double m2, const double m3, const double m4,
                 const IParticleTransporter* t = nullptr, const double dz = 5 * Gaudi::Units::mm );

      WrongMass( IDVAlgorithm const* algorithm, const std::vector<double>& masses,
                 const IParticleTransporter* t = nullptr, const double dz = 5 * Gaudi::Units::mm );

      /// constructor from pids
      WrongMass( IDVAlgorithm const* algorithm, const LHCb::ParticleID& p1, const LHCb::ParticleID& p2,
                 const double dz = 5 * Gaudi::Units::mm, const IParticleTransporter* t = nullptr );

      WrongMass( IDVAlgorithm const* algorithm, const LHCb::ParticleID& p1, const LHCb::ParticleID& p2,
                 const LHCb::ParticleID& p3, const double dz = 5 * Gaudi::Units::mm,
                 const IParticleTransporter* t = nullptr );

      WrongMass( IDVAlgorithm const* algorithm, const LHCb::ParticleID& p1, const LHCb::ParticleID& p2,
                 const LHCb::ParticleID& p3, const LHCb::ParticleID& p4, const double dz = 5 * Gaudi::Units::mm,
                 const IParticleTransporter* t = nullptr );

      WrongMass( IDVAlgorithm const* algorithm, const std::vector<LHCb::ParticleID>& pids,
                 const double dz = 5 * Gaudi::Units::mm, const IParticleTransporter* t = nullptr );

      /// constructor from names
      WrongMass( IDVAlgorithm const* algorithm, const std::string& m1, const std::string& m2,
                 const double dz = 5 * Gaudi::Units::mm, const IParticleTransporter* t = nullptr );

      WrongMass( IDVAlgorithm const* algorithm, const std::string& m1, const std::string& m2, const std::string& m3,
                 const double dz = 5 * Gaudi::Units::mm, const IParticleTransporter* t = nullptr );

      WrongMass( IDVAlgorithm const* algorithm, const std::string& m1, const std::string& m2, const std::string& m3,
                 const std::string& m4, const double dz = 5 * Gaudi::Units::mm,
                 const IParticleTransporter* t = nullptr );

      WrongMass( IDVAlgorithm const* algorithm, const std::vector<std::string>& names,
                 const double dz = 5 * Gaudi::Units::mm, const IParticleTransporter* t = nullptr );

      /// MANDATORY : virtual destructor
      virtual ~WrongMass();

      /// MANDATORY : clone method ("virtual constructor")
      WrongMass* clone() const override { return new WrongMass( *this ); }
      /// MANDATORY : the only one essential method
      result_type operator()( argument a ) const override { return wmass( a ); }
      /// OPTIONAL  : the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      /// notify that we need he context algorithm
      static bool context_dvalgo() { return true; }

    protected:
      /// decode the masses
      StatusCode decode() const;

    public:
      /// evaluate the wrong mass
      double wmass( argument p ) const;
      /// evaluate the wrong mass
      double wmass( const LHCb::Particle::ConstVector& p ) const { return wmass( p.begin(), p.end() ); }
      /// evaluate the wrong mass
      double wmass( const LHCb::Particle::Range& p ) const { return wmass( p.begin(), p.end() ); }
      /// evaluate the wrong mass
      double wmass( const SmartRefVector<LHCb::Particle>& p ) const { return wmass( p.begin(), p.end() ); }

      /// set new tolerance
      void setTolerance( const double value ) { m_dz = value; }
      /// set the transporter
      void setTransporter( const IParticleTransporter* tr ) const { m_transporter = tr; }
      /// set the transporter
      void setTransporter( const LoKi::Interface<IParticleTransporter>& tr ) const { m_transporter = tr; }

      /// get the tolerance
      double tolerance() const { return m_dz; }
      // get the tarnsporter
      const LoKi::Interface<IParticleTransporter>& transporter() const { return m_transporter; }

      template <class DAUGHTER>
      double wmass( DAUGHTER first, DAUGHTER last ) const {
        StatusCode sc = check( first, last );
        if ( sc.isFailure() ) {
          Error( "Invalid configuration, return InvalidMass", sc ).ignore();
          return LoKi::Constants::InvalidMass;
        }
        // evaluate the 'wrong mass'
        const LoKi::LorentzVector wm =
            LoKi::Kinematics::wrongMass( first, last, m_masses.begin(), LoKi::Objects::_VALID_ );
        return wm.M();
      }

      /// check the configuration
      StatusCode check() const {
        if ( !m_masses.empty() ) { return StatusCode::SUCCESS; }
        return decode();
      }
      /// check the argument
      StatusCode check( argument p ) const {
        if ( !p ) { return Error( "LHCb::Particle* points to NULL" ); }
        StatusCode sc = check();
        if ( sc.isFailure() ) { return Error( "Invalid status", sc ); }
        return StatusCode::SUCCESS;
      }
      /// check the daughters
      template <class DAUGHTER>
      StatusCode check( DAUGHTER first, DAUGHTER last ) const {
        StatusCode sc = check();
        if ( sc.isFailure() ) { return Error( "Invalid status", sc ); }
        if ( last - first != (long)m_masses.size() ) { return Error( " #masses != #daughters" ); }
        return StatusCode::SUCCESS;
      }

      const std::vector<double>&           masses() const { return m_masses; }
      const std::vector<std::string>&      names() const { return m_names; }
      const std::vector<LHCb::ParticleID>& pids() const { return m_pids; }

    private:
      /// the default constructor is disabled
      WrongMass(); // the default constructor is disabled

      /// the actual list of masses
      mutable std::vector<double> m_masses; // the actual list of masses
      /// the list of PIDs
      std::vector<LHCb::ParticleID> m_pids; // the list of PIDs
      /// the list of names
      std::vector<std::string> m_names; // the list of names
      /// the particle transporter
      mutable LoKi::Interface<IParticleTransporter> m_transporter;
      /// the delta-z tolerance
      double m_dz; // the delta-z tolerance
    };

    /** @class DeltaWrongMass
     *  Simple class to evaluate the the
     *  delta mass of the mother particle
     *  using "wrong" mass hypotheses for daughter particles
     *  with respect to soem "reference" mass
     *  @see LoKi::Cuts::DWMASS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-10-13
     */
    class GAUDI_API DeltaWrongMass : public WrongMass {
    public:
      /// constructor from masses
      DeltaWrongMass( IDVAlgorithm const* algorithm, const double m0, const WrongMass& wm );

      /// constructor from pids
      DeltaWrongMass( IDVAlgorithm const* algorithm, const LHCb::ParticleID& m0, const WrongMass& wm );

      /// constructor form names
      DeltaWrongMass( IDVAlgorithm const* algorithm, const std::string& m0, const WrongMass& wm );

      /// MANDATORY : clone method ("virtual constructor")
      DeltaWrongMass* clone() const override { return new DeltaWrongMass( *this ); }
      /// MANDATORY : the only one essential method
      result_type operator()( argument a ) const override { return dwmass( a ); }
      /// OPTIONAL  : the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;

      /// evaluate delta wrong mass
      double dwmass( argument p ) const;
      /// evaluate the wrong mass
      double dwmass( const LHCb::Particle::ConstVector& p ) const { return dwmass( p.begin(), p.end() ); }
      /// evaluate the wrong mass
      double dwmass( const SmartRefVector<LHCb::Particle>& p ) const { return dwmass( p.begin(), p.end() ); }

      template <class DAUGHTER>
      double dwmass( DAUGHTER first, DAUGHTER last ) const {
        if ( m0() < 0 ) { return m0(); }
        const double dm = wmass( first, last );
        if ( dm < 0 ) { return m0(); }
        return dm - m0();
      }

      double                  m0() const { return m_m0; }
      const std::string&      name0() const { return m_name0; }
      const LHCb::ParticleID& pid0() const { return m_pid0; }

    private:
      /// the default constructor is disabled
      DeltaWrongMass(); // the default constructor is disabled

      /// the reference mass
      double m_m0; //        the reference mass
      /// the reference mass
      std::string m_name0; //      the nominal particle
      /// the reference mass
      LHCb::ParticleID m_pid0; //           the nominal pid
    };

    /** @class AbdDeltaWrongMass
     *  Simple class to evaluate the the
     *  absolute value of delta mass of the mother particle
     *  using "wrong" mass hypotheses for daughter particles
     *  with respect to soem "reference" mass
     *  @see LoKi::Cuts::ADWMASS
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2008-10-13
     */
    class GAUDI_API AbsDeltaWrongMass : public DeltaWrongMass {
    public:
      /// constructor from masses
      AbsDeltaWrongMass( IDVAlgorithm const* algorithm, const double m0, const WrongMass& wm );

      /// constructor from pids
      AbsDeltaWrongMass( IDVAlgorithm const* algorithm, const LHCb::ParticleID& p1, const WrongMass& wm );

      /// constructor form names
      AbsDeltaWrongMass( IDVAlgorithm const* algorithm, const std::string& m0, const WrongMass& wm );

      /// constructor from delta mass
      AbsDeltaWrongMass( IDVAlgorithm const* algorithm, const DeltaWrongMass& wm );

      /// MANDATORY : clone method ("virtual constructor")
      AbsDeltaWrongMass* clone() const override { return new AbsDeltaWrongMass( *this ); }
      /// MANDATORY : the only one essential method
      result_type operator()( argument a ) const override { return adwmass( a ); }
      /// OPTIONAL  : the nice printout
      std::ostream& fillStream( std::ostream& s ) const override;

    public:
      /// evaluate delta wrong mass
      double adwmass( argument p ) const;
      /// evaluate the wrong mass
      double adwmass( const LHCb::Particle::ConstVector& p ) const { return adwmass( p.begin(), p.end() ); }
      /// evaluate the wrong mass
      double adwmass( const SmartRefVector<LHCb::Particle>& p ) const { return adwmass( p.begin(), p.end() ); }

      template <class DAUGHTER>
      double adwmass( DAUGHTER first, DAUGHTER last ) const {
        return m0() < 0 ? m0() : ::fabs( dwmass( first, last ) );
      }

    private:
      /// the default constructor is disabled
      AbsDeltaWrongMass(); // the default constructor is disabled
    };

  } // namespace Particles

} //                                                      end of namespace LoKi
