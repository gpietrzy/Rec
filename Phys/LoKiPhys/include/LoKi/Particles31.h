/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PARTICLES31_H
#  define LOKI_PARTICLES31_H 1
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/ATypes.h"
#  include "LoKi/PhysTypes.h"
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Particles {
    // ========================================================================
    /** @class PCutA
     *  Simple adapter (particle predicate) whcih delegates
     *  "array-predicate" to its daughter particles:
     *
     *  @code
     *
     *        array_functor ( p-> daughters() )
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::PCUTA
     *
     *  @see LoKi::Types::ACut
     *  @see LoKi::Types::ACuts
     *  @see LoKi::ATypes::ACut
     *  @see LoKi::ATypes::ACuts
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2009-11-16
     */
    class GAUDI_API PCutA : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// constructor from "array"-predicate
      PCutA( const LoKi::ATypes::ACuts& cut );
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      PCutA* clone() const override;
      /// MANDATORY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // the actual predicate
      LoKi::ATypes::ACut m_cut; // the actual predicate
      // ======================================================================
    };
    // ========================================================================
    /** @class PFunA
     *  Simple adapter (particle function) which delegates
     *  "array-predicate" to its daughter particles:
     *
     *  @code
     *
     *        array_functor ( p-> daughters() )
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::PFUNA
     *
     *  @see LoKi::Types::AFun
     *  @see LoKi::Types::AFunc
     *  @see LoKi::ATypes::AFun
     *  @see LoKi::ATypes::AFunc
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2009-11-16
     */
    class GAUDI_API PFunA : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor from "array"-predicate
      PFunA( const LoKi::ATypes::AFunc& fun );
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      PFunA* clone() const override;
      /// MANDATIRY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // the actual functors
      LoKi::ATypes::AFun m_fun; // the actual functor
      // ======================================================================
    };
    // ========================================================================
    /** @class PCutV
     *  Simple adapter (particle predicate) whcih delegates
     *  "array-predicate" to its daughter particles:
     *
     *  @code
     *
     *        array_functor ( p-> daughters() )
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::PCUTV
     *
     *  @see LoKi::Types::CutVals
     *  @see LoKi::Types::CutVal
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2009-11-16
     */
    class GAUDI_API PCutV : public LoKi::BasicFunctors<const LHCb::Particle*>::Predicate {
    public:
      // ======================================================================
      /// constructor from "array"-predicate
      PCutV( const LoKi::Types::CutVals& cut );
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      PCutV* clone() const override;
      /// MANDATIRY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // the actual predicate
      LoKi::Types::CutVal m_cut; // the actual predicate
      // ======================================================================
    };
    // ========================================================================
    /** @class PFunV
     *  Simple adapter (particle function) which delegates
     *  "array-predicate" to its daughter particles:
     *
     *  @code
     *
     *        array_functor ( p-> daughters() )
     *
     *  @endcode
     *
     *  @see LoKi::Cuts::PFUNV
     *
     *  @see LoKi::Types::FunVal
     *  @see LoKi::Types::FunVals
     *  @author Vanya BELYAEV  Ivan.BElyaev@nikhef.nl
     *  @date 2009-11-16
     */
    class GAUDI_API PFunV : public LoKi::BasicFunctors<const LHCb::Particle*>::Function {
    public:
      // ======================================================================
      /// constructor from "array"-predicate
      PFunV( const LoKi::Types::FunVals& fun );
      // ======================================================================
      /// MANDATORY: clone method ("virtual constructor")
      PFunV* clone() const override;
      /// MANDATIRY: the only one essential method
      result_type operator()( argument p ) const override;
      /// OPTIONAL: nice printout
      std::ostream& fillStream( std::ostream& s ) const override;
      // ======================================================================
    private:
      // ======================================================================
      // the actual functors
      LoKi::Types::FunVal m_fun; // the actual functor
      // ======================================================================
    };
    // ========================================================================
  } // namespace Particles
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
namespace LoKi {
  // ==========================================================================
  namespace Cuts {
    // ========================================================================
    /** @typedef PCUTA
     *  Adapter for array-functor, which acts according to the rule:
     *  @code
     *  cut ( p ) :  array_cut ( p->daughters )
     *  @endcode
     *  @see LoKi::Particles::PCutA
     *  @see LoKi::ATypes::ACut
     *  @see LoKi::ATypes::ACuts
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-11-16
     */
    typedef LoKi::Particles::PCutA PCUTA;
    // ========================================================================
    /** @typedef PCUTV
     *  Adapter for array-functor, which acts according to the rule:
     *  @code
     *  cut ( p ) :  vector_cut ( p->daughters )
     *  @endcode
     *  @see LoKi::Particles::PCutA
     *  @see LoKi::Types::CutVals
     *  @see LoKi::Types::CutVal
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-11-16
     */
    typedef LoKi::Particles::PCutV PCUTV;
    // ========================================================================
    /** @typedef PFUNA
     *  Adapter for array-functor, which acts according to the rule:
     *  @code
     *  fun ( p ) :  array_fun ( p->daughters )
     *  @endcode
     *  @see LoKi::Particles::PFunA
     *  @see LoKi::ATypes::AFun
     *  @see LoKi::ATypes::AFunc
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-11-16
     */
    typedef LoKi::Particles::PFunA PFUNA;
    // ========================================================================
    /** @typedef PFUNV
     *  Adapter for array-functor, which acts according to the rule:
     *  @code
     *  fun ( p ) :  vector_fun ( p->daughters )
     *  @endcode
     *  @see LoKi::Particles::PFunV
     *  @see LoKi::Types::FunVals
     *  @see LoKi::Types::FunVal
     *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
     *  @date 2009-11-16
     */
    typedef LoKi::Particles::PFunV PFUNV;
    // ========================================================================
  } // namespace Cuts
  // ==========================================================================
} //                                                      end of namespace LoKi
// ============================================================================
//                                                                      The END
// ============================================================================
#endif // LOKI_PARTICLES31_H
// ============================================================================
