/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_PHYSMONIDICTS_H
#  define LOKI_PHYSMONIDICTS_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/PhysTypes.h"
// ============================================================================
// forward declarations
// ============================================================================
namespace AIDA {
  class IHistogram1D;
}
class StatEntity;
// ============================================================================
namespace LoKi {
  namespace Dicts {} // namespace Dicts
} // end of namespace LoKi
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_PHYSMONIDICTS_H
// ============================================================================
