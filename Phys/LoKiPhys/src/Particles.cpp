/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
#include <functional>
// ============================================================================
// Event
// ============================================================================
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
// ============================================================================
// DaVinciKernel
// ============================================================================
#include "Kernel/IParticleFilter.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/ParticleContextCuts.h"
#include "LoKi/Particles.h"
// ============================================================================
// Boost
// ============================================================================
#include "boost/format.hpp"
// ============================================================================
/** @file
 *
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-10
 */
// ============================================================================
// get unique string for LHCb::Particle
// ============================================================================
std::string LoKi::Particles::hex_id( const LHCb::Particle* particle ) {
  if ( 0 == particle ) { return "NULL"; }
  boost::format fmt( "%p" );
  const void*   p = particle;
  fmt % p;
  return fmt.str();
}
// ============================================================================
namespace {
  // hashing object
  const std::hash<const void*> s_hash{};
} // namespace
// ============================================================================
// unique hash
// ============================================================================
std::size_t LoKi::Particles::hash( const LHCb::Particle* particle ) { return 0 == particle ? 0 : s_hash( particle ); }

// ============================================================================
// The END
// ============================================================================
