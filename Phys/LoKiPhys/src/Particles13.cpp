/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <algorithm>
// ============================================================================
// Event
// ============================================================================
#include "Event/Track.h"
// ============================================================================
// LoKiCore
// ============================================================================
#include "LoKi/Constants.h"
#include "LoKi/Print.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles13.h"
#include "LoKi/PhysExtract.h"
// ============================================================================
// GSL
// ============================================================================
#include "gsl/gsl_cdf.h"
// ============================================================================
/** @file
 *
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-22
 */
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackChi2::result_type LoKi::Particles::TrackChi2::
                                        operator()( LoKi::Particles::TrackChi2::argument p ) const {
  //
  if ( !p ) {
    Error( "Argument is invalid! return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }
  //
  return track->chi2(); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackChi2::fillStream( std::ostream& s ) const { return s << "TRCHI2"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackChi2PerDoF::result_type LoKi::Particles::TrackChi2PerDoF::
                                              operator()( LoKi::Particles::TrackChi2PerDoF::argument p ) const {
  //
  if ( !p ) {
    Error( "Argument is invalid! return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return 'InvalidChi2'" ).ignore();
    return LoKi::Constants::InvalidChi2; // RETURN
  }
  //
  return track->chi2PerDoF(); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackChi2PerDoF::fillStream( std::ostream& s ) const { return s << "TRCHI2DOF"; }
// ============================================================================
LoKi::Particles::TrackHasState::TrackHasState( const LHCb::State::Location location )
    : LoKi::AuxFunBase( std::tie( location ) ), m_location( location ) {}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackHasState::result_type LoKi::Particles::TrackHasState::
                                            operator()( LoKi::Particles::TrackHasState::argument p ) const {
  //
  if ( 0 == p ) {
    Error( "Argument is invalid! return false" ).ignore();
    return false; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return false" ).ignore();
    return false; // RETURN
  }
  //
  return track->hasStateAt( m_location ); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackHasState::fillStream( std::ostream& s ) const {
  return s << "HASSTATE['" << m_location << "']";
}
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackType::result_type LoKi::Particles::TrackType::
                                        operator()( LoKi::Particles::TrackType::argument p ) const {
  //
  const int errVal = -1000;
  //
  if ( 0 == p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  return static_cast<std::underlying_type_t<LHCb::Event::Enum::Track::Type>>( track->type() ); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackType::fillStream( std::ostream& s ) const { return s << "TRTYPE"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackHistory::result_type LoKi::Particles::TrackHistory::
                                           operator()( LoKi::Particles::TrackHistory::argument p ) const {
  //
  const int errVal = -1000;
  //
  if ( 0 == p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  return static_cast<std::underlying_type_t<LHCb::Event::Enum::Track::History>>( track->history() ); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackHistory::fillStream( std::ostream& s ) const { return s << "TRHISTORY"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackHistoryFit::result_type LoKi::Particles::TrackHistoryFit::
                                              operator()( LoKi::Particles::TrackHistoryFit::argument p ) const {
  //
  const int errVal = -1000;
  //
  if ( 0 == p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  return static_cast<std::underlying_type_t<LHCb::Event::Enum::Track::FitHistory>>( track->fitHistory() ); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackHistoryFit::fillStream( std::ostream& s ) const { return s << "TRHISTFIT"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackStatus::result_type LoKi::Particles::TrackStatus::
                                          operator()( LoKi::Particles::TrackStatus::argument p ) const {
  //
  const int errVal = -1000;
  //
  if ( 0 == p ) {
    Error( "Argument is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return " + Gaudi::Utils::toString( errVal ) ).ignore();
    return errVal; // RETURN
  }
  //
  return static_cast<std::underlying_type_t<LHCb::Event::Enum::Track::FitStatus>>( track->fitStatus() ); // RETURN
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackStatus::fillStream( std::ostream& s ) const { return s << "TRSTATUS"; }
// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackChi2Prob::result_type LoKi::Particles::TrackChi2Prob::
                                            operator()( LoKi::Particles::TrackChi2Prob::argument p ) const {
  if ( 0 == p ) {
    Error( "Argument is invalid! return 'InvalidConfLevel'" ).ignore();
    return LoKi::Constants::InvalidConfLevel; // RETURN
  }
  // get the track:
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return 'InvalidConfLevel'" ).ignore();
    return LoKi::Constants::InvalidConfLevel; // RETURN
  }
  //
  return gsl_cdf_chisq_Q( track->chi2(), track->nDoF() );
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackChi2Prob::fillStream( std::ostream& s ) const { return s << "TRPCHI2"; }

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::GhostProbability::result_type LoKi::Particles::GhostProbability::
                                               operator()( LoKi::Particles::GhostProbability::argument p ) const {
  if ( 0 == p ) {
    Error( "Argument is invalid! return 'InvalidConfLevel'" ).ignore();
    return LoKi::Constants::InvalidConfLevel; // RETURN
  }
  // get the track:
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( 0 == track ) {
    Error( "Track    is invalid! return 'InvalidConfLevel'" ).ignore();
    return LoKi::Constants::InvalidConfLevel; // RETURN
  }
  //
  return track->ghostProbability();
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::GhostProbability::fillStream( std::ostream& s ) const { return s << "TRGHOSTPROB"; }
// ============================================================================

// ============================================================================
// MANDATORY: the only one essential method
// ============================================================================
LoKi::Particles::TrackLikelihood::result_type LoKi::Particles::TrackLikelihood::
                                              operator()( LoKi::Particles::TrackLikelihood::argument p ) const {
  if ( !p ) {
    Error( "Argument is invalid! return 'InvalidConfLevel'" ).ignore();
    return LoKi::Constants::InvalidConfLevel; // RETURN
  }
  // get the track:
  const LHCb::Track* track = Extract::Particle2Track( p );
  //
  if ( !track ) {
    Error( "Track    is invalid! return 'InvalidConfLevel'" ).ignore();
    return LoKi::Constants::InvalidConfLevel; // RETURN
  }
  //
  return track->likelihood();
}
// ============================================================================
// OPTIONAL:  the specific printout
// ============================================================================
std::ostream& LoKi::Particles::TrackLikelihood::fillStream( std::ostream& s ) const { return s << "TRLIKELIHOOD"; }
// ============================================================================

// ============================================================================
// The END
// ============================================================================
