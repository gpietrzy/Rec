/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LoKi/Particles3.h"
#include "LoKi/Constants.h"
#include "LoKi/Kinematics.h"
#include "LoKi/PhysHelpers.h"

#include "GaudiKernel/SystemOfUnits.h"

#include <algorithm>
#include <vector>

/** @file
 *
 *  Implementation file for functions from namespace  LoKi::Particles
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV ibelyaev@physics.syr.edu
 *  @date 2006-02-10
 */

//  constructor from the particle and the tool
LoKi::Particles::ClosestApproach::ClosestApproach( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                                                   const LoKi::Vertices::ImpactParamTool& tool, const bool allow )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::Vertices::ImpactParamTool( tool )
    , m_particle( particle )
    , m_allow( allow ) {}

//  constructor from the particle and the tool
LoKi::Particles::ClosestApproach::ClosestApproach( IDVAlgorithm const*                    algorithm,
                                                   const LoKi::Vertices::ImpactParamTool& tool,
                                                   const LHCb::Particle* particle, const bool allow )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::Vertices::ImpactParamTool( tool )
    , m_particle( particle )
    , m_allow( allow ) {}

LoKi::Particles::ClosestApproach::result_type
LoKi::Particles::ClosestApproach::distance_( LHCb::Particle const* p1, LHCb::Particle const* p2,
                                             IGeometryInfo const& geometry ) const {
  if ( 0 != p1 && p1 == p2 ) {
    Warning( "doca: the same particle, return 0" ).ignore();
    return 0;
  }
  if ( 0 == tool() ) {
    Error( "Tool      is invalid, return 'InvalidDistance' " ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  double     dist = LoKi::Constants::InvalidDistance;
  StatusCode sc   = tool()->distance( p1, p2, dist, geometry, m_allow );
  if ( sc.isFailure() ) {
    Warning( "Error from IDistanceCalculator, return 'InvalidDistance' ", sc, 3 ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  return dist;
}

std::ostream& LoKi::Particles::ClosestApproach::fillStream( std::ostream& stream ) const { return stream << "CLAPP"; }

LoKi::Particles::ClosestApproach::result_type
LoKi::Particles::ClosestApproach::chi2_( LHCb::Particle const* p1, LHCb::Particle const* p2,
                                         IGeometryInfo const& geometry ) const {
  if ( 0 != p1 && p1 == p2 ) {
    Warning( "chi2: the same particle, return 0" ).ignore();
    return 0;
  }
  if ( 0 == tool() ) {
    Error( " Tool      is invalid, return 'InvalidChi2' " ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  double     dist = LoKi::Constants::InvalidDistance;
  double     chi2 = LoKi::Constants::InvalidChi2;
  StatusCode sc   = tool()->distance( p1, p2, dist, chi2, geometry, m_allow );
  if ( sc.isFailure() ) {
    Warning( "IDistanceCalculator::distance Failed, return 'InvalidChi2' ", sc, 3 ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  return chi2;
}

//  constructor from the particle and the tool
LoKi::Particles::ClosestApproachChi2::ClosestApproachChi2( IDVAlgorithm const*                    algorithm,
                                                           const LHCb::Particle*                  particle,
                                                           const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::ClosestApproach( algorithm, particle, tool ) {}

//  constructor from the particle and the tool
LoKi::Particles::ClosestApproachChi2::ClosestApproachChi2( IDVAlgorithm const*                    algorithm,
                                                           const LoKi::Vertices::ImpactParamTool& tool,
                                                           const LHCb::Particle*                  particle )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::ClosestApproach( algorithm, tool, particle ) {}

std::ostream& LoKi::Particles::ClosestApproachChi2::fillStream( std::ostream& stream ) const {
  return stream << "CLAPPCHI2";
}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproach::MinClosestApproach( IDVAlgorithm const*                    algorithm,
                                                         const LHCb::Particle::ConstVector&     particles,
                                                         const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::UniqueKeeper<LHCb::Particle>( particles.begin(), particles.end() )
    , m_fun( algorithm, LoKi::Helpers::_First( particles ), tool ) {}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproach::MinClosestApproach( IDVAlgorithm const*                    algorithm,
                                                         const LoKi::PhysTypes::Range&          particles,
                                                         const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::UniqueKeeper<LHCb::Particle>( particles.begin(), particles.end() )
    , m_fun( algorithm, LoKi::Helpers::_First( particles ), tool ) {}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproach::MinClosestApproach( IDVAlgorithm const*                    algorithm,
                                                         const LoKi::Vertices::ImpactParamTool& tool,
                                                         const LHCb::Particle::ConstVector&     particles )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::UniqueKeeper<LHCb::Particle>( particles.begin(), particles.end() )
    , m_fun( algorithm, LoKi::Helpers::_First( particles ), tool ) {}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproach::MinClosestApproach( IDVAlgorithm const*                    algorithm,
                                                         const LoKi::Vertices::ImpactParamTool& tool,
                                                         const LoKi::PhysTypes::Range&          particles )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::Particle*>::Function()
    , LoKi::UniqueKeeper<LHCb::Particle>( particles.begin(), particles.end() )
    , m_fun( algorithm, LoKi::Helpers::_First( particles ), tool ) {}

template <class FUNCTOR>
struct PMFA2 {
  typedef typename FUNCTOR::argument    argument;
  typedef typename FUNCTOR::result_type result_type;
  typedef result_type ( FUNCTOR::*PMF )( argument, const IGeometryInfo& ) const;

  /// constructor
  PMFA2( const FUNCTOR* fun, PMF _pmf ) : m_fun( fun ), m_pmf( _pmf ) {}
  /// the only one method
  result_type operator()( argument a, const IGeometryInfo& g ) const { return ( m_fun->*m_pmf )( a, g ); }

  template <class PARTICLE>
  void setParticle( PARTICLE particle ) const {
    m_fun->setParticle( particle );
  }

  /// the functor
  const FUNCTOR* m_fun;
  /// member function
  PMF m_pmf;
};

LoKi::Particles::MinClosestApproach::result_type
LoKi::Particles::MinClosestApproach::distance( LoKi::Particles::MinClosestApproach::argument p,
                                               IGeometryInfo const&                          geometry ) const {
  if ( 0 == p ) {
    Error( "Argument  is invalid, return 'InvalidDistance' " ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  result_type result = LoKi::Constants::InvalidDistance;
  if ( end() == LoKi::Helpers::_Min_particle(
                    begin(), end(),
                    PMFA2<LoKi::Particles::ClosestApproach>( &m_fun, &LoKi::Particles::ClosestApproach::distance ),
                    result, p, geometry ) ) {
    Error( "Invalid evaluation, return 'InvalidDistance' " ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  return result;
}

LoKi::Particles::MinClosestApproach::result_type
LoKi::Particles::MinClosestApproach::chi2( LoKi::Particles::MinClosestApproach::argument p,
                                           IGeometryInfo const&                          geometry ) const {
  if ( 0 == p ) {
    Error( "Argument  is invalid, return 'InvalidChi2' " ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  result_type result = LoKi::Constants::InvalidDistance;
  if ( end() == LoKi::Helpers::_Min_particle(
                    begin(), end(),
                    PMFA2<LoKi::Particles::ClosestApproach>( &m_fun, &LoKi::Particles::ClosestApproach::chi2 ), result,
                    p, geometry ) ) {
    Error( "Invalid evaluation, return 'InvalidChi2' " ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  return result;
}

std::ostream& LoKi::Particles::MinClosestApproach::fillStream( std::ostream& stream ) const {
  return stream << "MINCLAPP";
}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproachChi2::MinClosestApproachChi2( IDVAlgorithm const*                    algorithm,
                                                                 const LHCb::Particle::ConstVector&     particles,
                                                                 const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinClosestApproach( algorithm, particles, tool ) {}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproachChi2::MinClosestApproachChi2( IDVAlgorithm const*                    algorithm,
                                                                 const LoKi::PhysTypes::Range&          particles,
                                                                 const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinClosestApproach( algorithm, particles, tool ) {}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproachChi2::MinClosestApproachChi2( IDVAlgorithm const*                    algorithm,
                                                                 const LoKi::Vertices::ImpactParamTool& tool,
                                                                 const LHCb::Particle::ConstVector&     particles )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinClosestApproach( algorithm, particles, tool ) {}

//  constructor from the particle(s) and the tool
LoKi::Particles::MinClosestApproachChi2::MinClosestApproachChi2( IDVAlgorithm const*                    algorithm,
                                                                 const LoKi::Vertices::ImpactParamTool& tool,
                                                                 const LoKi::PhysTypes::Range&          particles )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::MinClosestApproach( algorithm, particles, tool ) {}

std::ostream& LoKi::Particles::MinClosestApproachChi2::fillStream( std::ostream& stream ) const {
  return stream << "MINCLAPPCHI2";
}
