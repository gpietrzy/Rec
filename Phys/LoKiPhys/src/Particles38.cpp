/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LoKi/Particles38.h"

/** @file
 *
 * Implementation file for functions from "M_corr" family by Mike Williams
 *
 *  This file is a part of
 *  <a href="http://cern.ch/lhcb-comp/Analysis/LoKi/index.html">LoKi project:</a>
 *  ``C++ ToolKit for Smart and Friendly Physics Analysis''
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya Belyaev Ivan.Belyaev@nikhef.nl
 *  @date   2010-10-23
 *
 * 2017-01-17
 * Guido Andreassi, Violaine Bellée, Pavol Štefko
 * Added BremMCorrected functor to compute the HOP mass. See description of the HOP mass here:
 * <a href="https://cds.cern.ch/record/2102345?ln=en">LHCb-INT-2015-037</a>
 */

/// anonymous namespace to hide local varibales
namespace {
  /// the invalid 3-momentum
  const LoKi::ThreeVector s_VECTOR = LoKi::ThreeVector( 0, 0, -1 * Gaudi::Units::TeV );
  /// the invalid 3Dpoint
  const LoKi::Point3D s_POINT = LoKi::Point3D( 0, 0, -1 * Gaudi::Units::km );
} // namespace

/*  constructor from the primary vertex
 *  @param x the x-position of primary vertex
 *  @param y the x-position of primary vertex
 *  @param z the x-position of primary vertex
 */
LoKi::Particles::PtFlight::PtFlight( IDVAlgorithm const* algorithm, const double x, const double y, const double z )
    : AuxFunBase{std::tie( algorithm, x, y, z )}
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::TransverseMomentumRel( s_VECTOR )
    , LoKi::Vertices::VertexHolder( LoKi::Point3D( x, y, z ) ) {}

/*  constructor from the primary vertex
 *  @param pv  the primary vertex
 */
LoKi::Particles::PtFlight::PtFlight( IDVAlgorithm const* algorithm, const LHCb::VertexBase* pv )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::TransverseMomentumRel( s_VECTOR )
    , LoKi::Vertices::VertexHolder( pv ) {}

/*  constructor from the primary vertex
 *  @param point  the position of primary vertex
 */
LoKi::Particles::PtFlight::PtFlight( IDVAlgorithm const* algorithm, const LoKi::Point3D& point )
    : AuxFunBase{std::tie( algorithm, point )}
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::TransverseMomentumRel( s_VECTOR )
    , LoKi::Vertices::VertexHolder( point ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::PtFlight* LoKi::Particles::PtFlight::clone() const { return new LoKi::Particles::PtFlight( *this ); }

// MANDATORY: the only one essential method
LoKi::Particles::PtFlight::result_type LoKi::Particles::PtFlight::
                                       operator()( LoKi::Particles::PtFlight::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore();
    return LoKi::Constants::InvalidMomentum;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore();
    return LoKi::Constants::InvalidMomentum;
  }
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  return ptFlight( p->momentum(), vx->position(), position() );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Particles::PtFlight::fillStream( std::ostream& s ) const {
  return s << "PTFLIGHT(" << position().X() << "," << position().Y() << "," << position().Z() << ")";
}

/*  constructor from the primary vertex
 *  @param x the x-position of primary vertex
 *  @param y the x-position of primary vertex
 *  @param z the x-position of primary vertex
 */
LoKi::Particles::MCorrected::MCorrected( IDVAlgorithm const* algorithm, const double x, const double y, const double z )
    : AuxFunBase{std::tie( algorithm, x, y, z )}
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PtFlight( algorithm, x, y, z ) {}

/*  constructor from the primary vertex
 *  @param pv  the primary vertex
 */
LoKi::Particles::MCorrected::MCorrected( IDVAlgorithm const* algorithm, const LHCb::VertexBase* pv )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::PtFlight( algorithm, pv ) {}

/*  constructor from the primary vertex
 *  @param point  the position of primary vertex
 */
LoKi::Particles::MCorrected::MCorrected( IDVAlgorithm const* algorithm, const LoKi::Point3D& point )
    : AuxFunBase{std::tie( algorithm, point )}
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PtFlight( algorithm, point ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MCorrected* LoKi::Particles::MCorrected::clone() const {
  return new LoKi::Particles::MCorrected( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MCorrected::result_type LoKi::Particles::MCorrected::
                                         operator()( LoKi::Particles::MCorrected::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  return mCorrFlight( p->momentum(), vx->position(), position() );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MCorrected::fillStream( std::ostream& s ) const {
  return s << "CORRM(" << position().X() << "," << position().Y() << "," << position().Z() << ")";
}

// constructor
LoKi::Particles::PtFlightWithBestVertex::PtFlightWithBestVertex( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PtFlight( algorithm, s_POINT ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::PtFlightWithBestVertex* LoKi::Particles::PtFlightWithBestVertex::clone() const {
  return new LoKi::Particles::PtFlightWithBestVertex( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::PtFlightWithBestVertex::result_type LoKi::Particles::PtFlightWithBestVertex::
                                                     operator()( LoKi::Particles::PtFlightWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Momentum'" ).ignore();
    return LoKi::Constants::InvalidMomentum;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Momentum'" ).ignore();
    return LoKi::Constants::InvalidMomentum;
  }
  const LHCb::VertexBase* pv = bestVertex( p, desktop()->geometry() );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Momentum'" ).ignore();
    return LoKi::Constants::InvalidMomentum;
  }
  setVertex( pv );
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  return ptFlight( p->momentum(), vx->position(), position() );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Particles::PtFlightWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVPTFLIGHT";
}

// constructor
LoKi::Particles::MCorrectedWithBestVertex::MCorrectedWithBestVertex( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PtFlightWithBestVertex( algorithm ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::MCorrectedWithBestVertex* LoKi::Particles::MCorrectedWithBestVertex::clone() const {
  return new LoKi::Particles::MCorrectedWithBestVertex( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::MCorrectedWithBestVertex::result_type LoKi::Particles::MCorrectedWithBestVertex::
                                                       operator()( LoKi::Particles::MCorrectedWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  const LHCb::VertexBase* pv = bestVertex( p, desktop()->geometry() );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  setVertex( pv );
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  return mCorrFlight( p->momentum(), vx->position(), position() );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Particles::MCorrectedWithBestVertex::fillStream( std::ostream& s ) const { return s << "BPVCORRM"; }

// 00oo..oo00
// Implementation of the Bremshtralung corrected B mass
/*  constructor from the primary vertex
 *  @param x the x-position of primary vertex
 *  @param y the x-position of primary vertex
 *  @param z the x-position of primary vertex
 */
LoKi::Particles::BremMCorrected::BremMCorrected( IDVAlgorithm const* algorithm, const double x, const double y,
                                                 const double z )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::PtFlight( algorithm, x, y, z ) {}

/*  constructor from the primary vertex
 *  @param pv  the primary vertex
 */
LoKi::Particles::BremMCorrected::BremMCorrected( IDVAlgorithm const* algorithm, const LHCb::VertexBase* pv )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::PtFlight( algorithm, pv ) {}

/*  constructor from the primary vertex
 *  @param point  the position of primary vertex
 */
LoKi::Particles::BremMCorrected::BremMCorrected( IDVAlgorithm const* algorithm, const LoKi::Point3D& point )
    : LoKi::AuxDesktopBase( algorithm ), LoKi::Particles::PtFlight( algorithm, point ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::BremMCorrected* LoKi::Particles::BremMCorrected::clone() const {
  return new LoKi::Particles::BremMCorrected( *this );
}

// MANDATORY: the only one essential method
double                                       LoKi::Particles::BremMCorrected::m_mass = -1 * Gaudi::Units::GeV;
LHCb::ParticleID                             LoKi::Particles::BremMCorrected::m_pid  = LHCb::ParticleID();
LoKi::Particles::BremMCorrected::result_type LoKi::Particles::BremMCorrected::
                                             operator()( LoKi::Particles::BremMCorrected::argument p ) const {
  if ( 0 > m_mass || 0 == m_pid.pid() ) {
    m_mass = LoKi::Particles::massFromName( "e-" );
    m_pid  = LoKi::Particles::pidFromName( "e-" );
  }
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  LHCb::Particle::ConstVector m_all_electrons;
  LHCb::Particle::ConstVector m_rest_electrons;
  LHCb::Particle::ConstVector m_electron_mothers;
  LHCb::Particle::ConstVector m_others;
  classify( p, m_all_electrons, m_rest_electrons, m_electron_mothers, m_others );
  LorentzVector P_h_tot, P_e_tot;
  LorentzVector P_e_corr_tot, P_e_corr_temp;
  for ( const auto child : ( m_others ) ) { P_h_tot += ( child->momentum() ); }
  for ( const auto child : ( m_electron_mothers ) ) { P_e_tot += ( child->momentum() ); }
  for ( const auto child : ( m_rest_electrons ) ) { P_e_tot += ( child->momentum() ); }
  double pt_h = ptFlight( P_h_tot, p->endVertex()->position(), position() );
  double pt_e = ptFlight( P_e_tot, p->endVertex()->position(), position() );
  double alpha = pt_h / pt_e;
  for ( const auto child : m_all_electrons ) {
    double E_e = sqrt( pow( ( alpha * child->momentum().X() ), 2 ) + pow( ( alpha * child->momentum().Y() ), 2 ) +
                       pow( ( alpha * child->momentum().Z() ), 2 ) + ( m_mass * m_mass ) );
    P_e_corr_temp.SetXYZT( alpha * child->momentum().X(), alpha * child->momentum().Y(), alpha * child->momentum().Z(),
                           E_e );
    P_e_corr_tot += P_e_corr_temp;
  }
  LorentzVector P         = P_h_tot + P_e_corr_tot;
  double        corr_mass = P.M();
  return corr_mass;
}

// method which classifies all particles in the decay chain into following containers:
// m_all_electrons    - all electrons
// m_electron_mothers - all particles that decay exclusively into electrons and/or positrons
//                      (i.e. no hadronic/muonic daughters)
// m_rest_electrons   - electrons whose mother particle is not in 'm_electron_mothers'
//                      (i.e. comes from a decaty that has also hadronic component)
// m_others           - all non-electron particles
//                     (i.e. either particles that don't have an electron/positron among
//                     its daughters or 'stable' particles coming from semi-electronic decays)
bool LoKi::Particles::BremMCorrected::classify( const LHCb::Particle*        parent,
                                                LHCb::Particle::ConstVector& m_all_electrons,
                                                LHCb::Particle::ConstVector& m_rest_electrons,
                                                LHCb::Particle::ConstVector& m_electron_mothers,
                                                LHCb::Particle::ConstVector& m_others ) const {
  if ( !parent->isBasicParticle() ) {
    // if the particle has only electronic daughters push the particle into
    //  'm_electron_mothers'; also put all the daughters of the particle into 'm_all_electrons'
    if ( has_only_electrons( parent ) ) {
      m_electron_mothers.emplace_back( parent );
      for ( const auto& child : parent->daughtersVector() ) { m_all_electrons.emplace_back( child ); }
    }
    // if the particle has an electron among the daughters
    //  apply the 'classify' method recursively on its children;
    //  if it has no electronic component push it to 'm_others'
    else {
      if ( has_electron( parent ) ) {
        for ( const auto& child : parent->daughtersVector() ) {
          classify( child, m_all_electrons, m_rest_electrons, m_electron_mothers, m_others );
        }
      } else
        m_others.emplace_back( parent );
    }
  }
  // if the particle is a basic particle put it into
  // both 'm_all_electrons' and 'm_rest_electrons' containers
  // if it is an electron/positron and into 'm_others' if it is not an electron/positron
  else {
    if ( parent->particleID().abspid() == m_pid.abspid() ) {
      m_all_electrons.emplace_back( parent );
      m_rest_electrons.emplace_back( parent );
    } else
      m_others.emplace_back( parent );
  }
  return true;
}

// method that checks whether a particle has only electronic daughters
bool LoKi::Particles::BremMCorrected::has_only_electrons( const LHCb::Particle* parent ) const {
  for ( const auto& child : parent->daughtersVector() ) {
    if ( child->particleID().abspid() != m_pid.abspid() ) return false;
  }
  return true;
}

// method that checks whether a particle has at least one electron in its decay chain
bool LoKi::Particles::BremMCorrected::has_electron( const LHCb::Particle* parent ) const {
  for ( const auto& child : parent->daughtersVector() ) {
    if ( !child->isBasicParticle() ) {
      if ( has_electron( child ) ) return true;
    } else if ( child->particleID().abspid() == m_pid.abspid() )
      return true;
  }
  return false;
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Particles::BremMCorrected::fillStream( std::ostream& s ) const {
  return s << "HOPM(" << position().X() << "," << position().Y() << "," << position().Z() << ")";
}

// constructor
LoKi::Particles::BremMCorrectedWithBestVertex::BremMCorrectedWithBestVertex( const IDVAlgorithm* algorithm )
    : LoKi::AuxFunBase( std::tie( algorithm ) )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::Particles::PtFlightWithBestVertex( algorithm ) {}

// MANDATORY: clone method ("virtual constructor")
LoKi::Particles::BremMCorrectedWithBestVertex* LoKi::Particles::BremMCorrectedWithBestVertex::clone() const {
  return new LoKi::Particles::BremMCorrectedWithBestVertex( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::BremMCorrectedWithBestVertex::result_type LoKi::Particles::BremMCorrectedWithBestVertex::
                                                           operator()( LoKi::Particles::BremMCorrectedWithBestVertex::argument p ) const {
  if ( 0 == p ) {
    Error( "Invalid argument, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  // get the decay vertex:
  const LHCb::VertexBase* vx = p->endVertex();
  if ( 0 == vx ) {
    Error( "EndVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  const LHCb::VertexBase* pv = bestVertex( p, desktop()->geometry() );
  if ( 0 == pv ) {
    Error( "BestVertex is invalid, return 'Invalid Mass'" ).ignore();
    return LoKi::Constants::InvalidMass;
  }
  setVertex( pv );
  Assert( LoKi::Vertices::VertexHolder::valid(), "Vertex-Information is not valid" );
  BremMCorrected bremcorr( desktop(), pv );
  return bremcorr( p );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Particles::BremMCorrectedWithBestVertex::fillStream( std::ostream& s ) const {
  return s << "BPVHOPM()";
}
