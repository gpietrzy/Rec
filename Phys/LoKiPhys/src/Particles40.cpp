/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// LoKiCode
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles40.h"
#include "LoKi/PhysTypes.h"
// ============================================================================
/** @file
 *  Implementation file for functions form file LoKi/Particles40.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-01-17
 *
 */
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::SmartInfo::SmartInfo( const int index, const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun,
                                       const bool update )
    : LoKi::AuxFunBase( std::tie( index, fun, update ) )
    , LoKi::ExtraInfo::GetSmartInfo<const LHCb::Particle*>( index, fun, update ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::SmartInfo::SmartInfo( const LoKi::BasicFunctors<const LHCb::Particle*>::Function& fun, const int index,
                                       const bool update )
    : LoKi::AuxFunBase( std::tie( fun, index, update ) )
    , LoKi::ExtraInfo::GetSmartInfo<const LHCb::Particle*>( index, fun, update ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Particles::SmartInfo* LoKi::Particles::SmartInfo::clone() const {
  return new LoKi::Particles::SmartInfo( *this );
}
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Particles::SmartInfo::fillStream( std::ostream& s ) const {
  s << "SINFO(" << func() << "," << index() << ",";
  if ( update() ) {
    s << "True";
  } else {
    s << "False";
  }
  return s << ")";
}
// ============================================================================
// constructor
// ============================================================================
LoKi::Vertices::SmartInfo::SmartInfo( const int                                                     index,
                                      const LoKi::BasicFunctors<const LHCb::VertexBase*>::Function& fun,
                                      const bool                                                    update )
    : LoKi::AuxFunBase( std::tie( index, fun, update ) )
    , LoKi::ExtraInfo::GetSmartInfo<const LHCb::VertexBase*>( index, fun, update ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Vertices::SmartInfo::SmartInfo( const LoKi::BasicFunctors<const LHCb::VertexBase*>::Function& fun,
                                      const int index, const bool update )
    : LoKi::AuxFunBase( std::tie( fun, index, update ) )
    , LoKi::ExtraInfo::GetSmartInfo<const LHCb::VertexBase*>( index, fun, update ) {}
// ============================================================================
// constructor
// ============================================================================
LoKi::Vertices::SmartInfo* LoKi::Vertices::SmartInfo::clone() const { return new LoKi::Vertices::SmartInfo( *this ); }
// ============================================================================
// the specific printout
// ============================================================================
std::ostream& LoKi::Vertices::SmartInfo::fillStream( std::ostream& s ) const {
  s << "VSINFO(" << func() << "," << index() << ",";
  if ( update() ) {
    s << "True";
  } else {
    s << "False";
  }
  return s << ")";
}
// ============================================================================
// The END
// ============================================================================
