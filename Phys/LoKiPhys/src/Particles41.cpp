/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
// ============================================================================
// STD & STL
// ============================================================================
#include <functional>
// ============================================================================
// LoKiCode
// ============================================================================
#include "LoKi/Algs.h"
#include "LoKi/Constants.h"
// ============================================================================
// LoKiPhys
// ============================================================================
#include "LoKi/Particles41.h"
#include "LoKi/PhysExtract.h"
#include "LoKi/PhysKinematics.h"
// ============================================================================
/** @file
 *  Implementation file for functions form file LoKi/Particles39.h
 *
 *  This file is a part of LoKi project -
 *    "C++ ToolKit  for Smart and Friendly Physics Analysis"
 *
 *  The package has been designed with the kind help from
 *  Galina PAKHLOVA and Sergey BARSUK.  Many bright ideas,
 *  contributions and advices from G.Raven, J.van Tilburg,
 *  A.Golutvin, P.Koppenburg have been used in the design.
 *
 *  @author Vanya BELYAEV Ivan.Belyaev@itep.ru
 *  @date 2012-03-31
 *
 */

//@TODO: tmeplate on m_distance implementation instead

// ============================================================================
// constructor
// ============================================================================
/*  constructor
 *  @param minmax  min/max flag
 *  @param cut     the selection of daughters
 *  @param fun     the function
 */
// ============================================================================
LoKi::Particles::MinMaxPair::MinMaxPair( const bool                                                   minmax,
                                         const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut,
                                         const LoKi::Particles::MinMaxPair::dist_func                 fun )
    : AuxFunBase{std::tie( minmax, cut, fun )}, m_minimum( minmax ), m_cut( cut ), m_distance( fun ) {
  Assert( 0 != m_distance, "Invalid distance-function is specified!" );
}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinMaxPair* LoKi::Particles::MinMaxPair::clone() const {
  return new LoKi::Particles::MinMaxPair( *this );
}
// ============================================================================
// evaluate the distance between two particles
// ============================================================================
double LoKi::Particles::MinMaxPair::distance( const LHCb::Particle* p1, const LHCb::Particle* p2 ) const {
  return ( *m_distance )( p1, p2 );
}
// ============================================================================
// the actual computation
// ============================================================================
double LoKi::Particles::MinMaxPair::distance( const LHCb::Particle* p ) const {
  /// get all proper daughters
  LHCb::Particle::ConstVector good;
  good.reserve( 8 );
  //
  LoKi::Extract::particles( p, std::back_inserter( good ), m_cut.func() );
  //
  double result = m_minimum ? LoKi::Constants::PositiveInfinity : LoKi::Constants::NegativeInfinity;
  //
  if ( 2 > good.size() ) { return result; } // RETURN
  //
  // double loop to find the appropriate pairs
  //
  for ( auto ip1 = good.begin(); good.end() != ip1; ++ip1 ) {
    result = m_minimum
                 ? std::accumulate(
                       std::next( ip1 ), good.end(), result,
                       [&]( double r, const LHCb::Particle* p2 ) { return std::min( r, distance( *ip1, p2 ) ); } )
                 : std::accumulate( std::next( ip1 ), good.end(), result, [&]( double r, const LHCb::Particle* p2 ) {
                     return std::max( r, distance( *ip1, p2 ) );
                   } );
  }
  //
  return result;
}
// ============================================================================
// MANDATORY: the only essential method
// ============================================================================
LoKi::Particles::MinMaxPair::result_type LoKi::Particles::MinMaxPair::
                                         operator()( LoKi::Particles::MinMaxPair::argument p ) const {
  if ( !p ) {
    Error( "LHCb::Particle* points to NULL" ).ignore();
    return m_minimum ? LoKi::Constants::PositiveInfinity : LoKi::Constants::NegativeInfinity;
  }
  //
  return distance( p );
}
// ============================================================================

// ============================================================================
// constructor from daughter selection
// ============================================================================
LoKi::Particles::MinKullback::MinKullback( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( cut ) ), LoKi::Particles::MinMaxPair( true, cut, &LoKi::PhysKinematics::kullback ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinKullback* LoKi::Particles::MinKullback::clone() const {
  return new LoKi::Particles::MinKullback( *this );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinKullback::fillStream( std::ostream& s ) const {
  return s << " MINKULLBACK( " << cut() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from daughter selection
// ============================================================================
LoKi::Particles::MinAng::MinAng( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( cut ) )
    , LoKi::Particles::MinMaxPair( true, cut, &LoKi::PhysKinematics::deltaAlpha ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinAng* LoKi::Particles::MinAng::clone() const { return new LoKi::Particles::MinAng( *this ); }
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinAng::fillStream( std::ostream& s ) const { return s << " MINANG( " << cut() << ") "; }
// ============================================================================

// ============================================================================
// constructor from daughter selection
// ============================================================================
LoKi::Particles::MinDeltaM2::MinDeltaM2( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( cut ) ), LoKi::Particles::MinMaxPair( true, cut, &LoKi::PhysKinematics::deltaM2 ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MinDeltaM2* LoKi::Particles::MinDeltaM2::clone() const {
  return new LoKi::Particles::MinDeltaM2( *this );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MinDeltaM2::fillStream( std::ostream& s ) const {
  return s << " MINDELTAM2( " << cut() << ") ";
}
// ============================================================================

// ============================================================================
// constructor from daughter selection
// ============================================================================
LoKi::Particles::MaxOverlap::MaxOverlap( const LoKi::BasicFunctors<const LHCb::Particle*>::Predicate& cut )
    : LoKi::AuxFunBase( std::tie( cut ) ), LoKi::Particles::MinMaxPair( false, cut, &LoKi::PhysKinematics::overlap ) {}
// ============================================================================
// MANDATORY: clone method ("virtual constructor")
// ============================================================================
LoKi::Particles::MaxOverlap* LoKi::Particles::MaxOverlap::clone() const {
  return new LoKi::Particles::MaxOverlap( *this );
}
// ============================================================================
// OPTIONAL: nice printout
// ============================================================================
std::ostream& LoKi::Particles::MaxOverlap::fillStream( std::ostream& s ) const {
  return s << " MAXOVERLAP( " << cut() << ") ";
}
// ============================================================================

// ============================================================================
// The END
// ============================================================================
