/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"

#include "Kernel/RelatedInfoNamed.h"

#include "boost/algorithm/string/replace.hpp"

// local
#include "LoKi/ILoKiSvc.h"
#include "LoKi/Particles48.h"

LoKi::Particles::AutoRelatedInfo::AutoRelatedInfo( const std::string& to, const short index, const double bad,
                                                   const std::string& from )
    : LoKi::AuxFunBase( std::tie( from, to, index, bad ) )
    , m_from( from )
    , m_to( to )
    , m_index( index )
    , m_bad( bad ) {}

LoKi::Particles::AutoRelatedInfo::AutoRelatedInfo( const std::string& to, const std::string& variable, const double bad,
                                                   const std::string& from )
    : LoKi::AuxFunBase( std::tie( from, to, variable, bad ) ), m_from( from ), m_to( to ), m_bad( bad ) {
  const auto index = RelatedInfoNamed::indexByName( variable );
  if ( index == RelatedInfoNamed::UNKNOWN ) { Warning( "RelatedInfo variable " + variable + " unknown" ).ignore(); }
  m_index = index;
}

LoKi::Particles::AutoRelatedInfo* LoKi::Particles::AutoRelatedInfo::clone() const {
  return new LoKi::Particles::AutoRelatedInfo( *this );
}

// MANDATORY: the only one essential method
LoKi::Particles::AutoRelatedInfo::result_type LoKi::Particles::AutoRelatedInfo::
                                              operator()( LoKi::Particles::AutoRelatedInfo::argument p ) const {
  if ( !p ) {
    Error( "Invalid particle, return ..." ).ignore();
    return -2000;
  }

  // Get location in TES for p
  auto objectLocation = []( const auto* obj ) {
    const auto parent = ( obj ? obj->parent() : nullptr );
    const auto reg    = ( parent ? parent->registry() : nullptr );
    return ( reg ? reg->identifier() : "" );
  };
  auto tesLoc = objectLocation( p );
  if ( tesLoc.empty() ) {
    Error( "Particle object not in TES" ).ignore();
    return -2000;
  }

  // Form TES location for table based on replacement strings
  boost::replace_all( tesLoc, m_from, m_to );

  // reload table ?
  if ( tesLoc != m_location || !sameEvent() ) {
    SmartIF<IDataProviderSvc> ds( lokiSvc().getObject() );
    SmartDataPtr<IMAP>        data( ds, tesLoc );
    if ( !data ) {
      Warning( "No table at location " + tesLoc ).ignore();
      m_location = "";
      m_table    = nullptr;
      return -2000;
    }
    m_table    = data;
    m_location = tesLoc;
    setEvent();
  }
  if ( !m_table ) { return -2000; }

  const auto& r = m_table->relations( p );
  if ( r.empty() ) {
    Warning( "No entry for particle" ).ignore();
    return m_bad;
  }
  if ( 1 != r.size() ) {
    Warning( ">1 entry for particle" ).ignore();
    return m_bad;
  }
  const auto& m = r[0].to();

  return m.info( m_index, m_bad );
}

// OPTIONAL: the specific printout
std::ostream& LoKi::Particles::AutoRelatedInfo::fillStream( std::ostream& s ) const {
  return s << "RELATEDINFO('" << m_to << "'," << m_index << "," << m_bad << ",'" << m_from << "')";
}
