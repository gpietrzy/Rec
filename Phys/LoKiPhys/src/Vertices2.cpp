/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LoKi/Vertices2.h"
#include "LoKi/Constants.h"
#include "LoKi/Particles4.h"

//  constructor from the particle and tool
LoKi::Vertices::ImpPar::ImpPar( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                                const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, (const LHCb::VertexBase*)0, tool )
    , m_particle( particle ) {}

//  constructor from the particle and tool
LoKi::Vertices::ImpPar::ImpPar( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                                const LoKi::Vertices::ImpParBase& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, tool )
    , m_particle( particle ) {}

//  constructor from the particle and tool
LoKi::Vertices::ImpPar::ImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                const LHCb::Particle* particle )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, (const LHCb::VertexBase*)0, tool )
    , m_particle( particle ) {}

//  constructor from the particle and tool
LoKi::Vertices::ImpPar::ImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpParBase& tool,
                                const LHCb::Particle* particle )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, tool )
    , m_particle( particle ) {}

// copy constructor
LoKi::Vertices::ImpPar::ImpPar( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpPar& right )
    : LoKi::AuxFunBase( right )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function( right )
    , m_evaluator( algorithm, right.m_evaluator )
    , m_particle( right.m_particle ) {}

//  mandatory clone ("virtual constructor")
LoKi::Vertices::ImpPar* LoKi::Vertices::ImpPar::clone() const { return new LoKi::Vertices::ImpPar( *this ); }

//  destructor
LoKi::Vertices::ImpPar::~ImpPar() {}

//  the only one essential method
LoKi::Vertices::ImpPar::result_type LoKi::Vertices::ImpPar::operator()( LoKi::Vertices::ImpPar::argument v ) const {
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* argument is invalid! return InvalidDistance" ).ignore();
    return LoKi::Constants::InvalidDistance;
  }
  m_evaluator.setVertex( v );
  if ( 0 == m_particle ) { Warning( "LHCb::Particle* parameter is invalid!" ).ignore(); }
  // use the actual evaluator
  return m_evaluator( m_particle );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Vertices::ImpPar::fillStream( std::ostream& s ) const { return s << "VIP"; }

//  constructor from the particle and tool
LoKi::Vertices::ImpParChi2::ImpParChi2( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                                        const LoKi::Vertices::ImpactParamTool& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, (const LHCb::VertexBase*)0, tool )
    , m_particle( particle ) {}

//  constructor from the particle and tool
LoKi::Vertices::ImpParChi2::ImpParChi2( IDVAlgorithm const* algorithm, const LHCb::Particle* particle,
                                        const LoKi::Vertices::ImpParBase& tool )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, tool )
    , m_particle( particle ) {}

//  constructor from the particle and tool
LoKi::Vertices::ImpParChi2::ImpParChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpactParamTool& tool,
                                        const LHCb::Particle* particle )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, (const LHCb::VertexBase*)0, tool )
    , m_particle( particle ) {}

//  constructor from the particle and tool
LoKi::Vertices::ImpParChi2::ImpParChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpParBase& tool,
                                        const LHCb::Particle* particle )
    : LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function()
    , m_evaluator( algorithm, tool )
    , m_particle( particle ) {}

// copy constructor
LoKi::Vertices::ImpParChi2::ImpParChi2( IDVAlgorithm const* algorithm, const LoKi::Vertices::ImpParChi2& right )
    : LoKi::AuxFunBase( right )
    , LoKi::AuxDesktopBase( algorithm )
    , LoKi::BasicFunctors<const LHCb::VertexBase*>::Function( right )
    , m_evaluator( algorithm, right.m_evaluator )
    , m_particle( right.m_particle ) {}

//  mandatory clone ("virtual constructor")
LoKi::Vertices::ImpParChi2* LoKi::Vertices::ImpParChi2::clone() const {
  return new LoKi::Vertices::ImpParChi2( *this );
}

//  destructor
LoKi::Vertices::ImpParChi2::~ImpParChi2() {}

//  the only one essential method
LoKi::Vertices::ImpParChi2::result_type LoKi::Vertices::ImpParChi2::
                                        operator()( LoKi::Vertices::ImpParChi2::argument v ) const {
  if ( 0 == v ) {
    Error( "LHCb::VertexBase* argument is invalid! return InvalidChi2" ).ignore();
    return LoKi::Constants::InvalidChi2;
  }
  m_evaluator.setVertex( v );
  if ( 0 == m_particle ) { Warning( "LHCb::Particle* parameter is invalid!" ).ignore(); }
  // use the actual evaluator
  return m_evaluator( m_particle );
}

//  OPTIONAL: the specific printout
std::ostream& LoKi::Vertices::ImpParChi2::fillStream( std::ostream& s ) const { return s << "VIPCHI2"; }
