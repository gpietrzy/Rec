/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#ifndef LOKI_LOKIPHYSMCDICT_H
#  define LOKI_LOKIPHYSMCDICT_H 1
// ============================================================================
// Include files
// ============================================================================
// LoKi
// ============================================================================
#  include "LoKi/LoKiPhysMC.h"
// ============================================================================
#  include "LoKi/MCMatchDicts.h"
// ============================================================================
#  ifdef __INTEL_COMPILER
#    pragma warning( disable : 177 ) //  variable ... was declared but never referenced
#    pragma warning( disable : 191 ) // type qualifier is meaningless on cast type
#  endif
// ============================================================================
// The END
// ============================================================================
#endif // LOKI_LOKIPHYSMCDICT_H
// ============================================================================
