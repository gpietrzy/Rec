###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/LoKiUtils
--------------
#]=======================================================================]

gaudi_add_library(LoKiUtils
    SOURCES
        src/GetTools.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::DetDescLib
            LHCb::LoKiCoreLib
            LHCb::RecEvent
            LHCb::TrackEvent
        PRIVATE
            Gaudi::GaudiAlgLib
            LHCb::PhysInterfacesLib
            Rec::DaVinciInterfacesLib
            Rec::TrackInterfacesLib
)

gaudi_add_dictionary(LoKiUtilsDict
    HEADERFILES dict/LoKiUtilsDict.h
    SELECTION dict/LoKiUtils.xml
    LINK LoKiUtils
)

gaudi_install(PYTHON)
