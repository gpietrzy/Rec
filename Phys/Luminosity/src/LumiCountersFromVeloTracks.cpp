/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

#include "Event/HltLumiSummary.h"
#include "Event/PrVeloTracks.h"

/** @class LumiCountersFromVeloTracks
 *
 * @brief Create an HltLumiSummary object filled with counters based on a LHCb::Pr::Velo::Tracks.
 *
 * Takes a single LHCb::Pr::Velo::Tracks as input and outputs an HltLumiSummary object,
 * which maps counter names to values.
 *
 * @see LumiCounterMerger for merging multiple summary objects into one.
 */
class LumiCountersFromVeloTracks : public LHCb::Algorithm::Transformer<LHCb::HltLumiSummary(
                                       const LHCb::Pr::Velo::Tracks&, const LHCb::Pr::Velo::Tracks& )> {
public:
  using base_class = LHCb::Algorithm::Transformer<LHCb::HltLumiSummary( const LHCb::Pr::Velo::Tracks&,
                                                                        const LHCb::Pr::Velo::Tracks& )>;

  LumiCountersFromVeloTracks( const std::string& name, ISvcLocator* pSvc )
      : base_class( name, pSvc, {KeyValue{"ForwardTracks", ""}, KeyValue{"BackwardTracks", ""}},
                    KeyValue{"OutputSummary", ""} ) {}

  static auto velo_DOCAz( float x, float y, float tx, float ty ) {
    return std::abs( ty * x - tx * y ) / std::sqrt( tx * tx + ty * ty );
  }

  // This fast pseudorapidity calculation comes from https://root.cern/doc/v608/eta_8h_source.html#l00050
  static auto eta_from_rho_z( float rho, float z ) {
    if ( rho > 0.f ) {
      // value to control Taylor expansion of sqrt
      // constant value from std::pow(std::numeric_limits<float>::epsilon(), static_cast<float>(-.25));
      constexpr float big_z_scaled = 53.817371f;
      float           z_scaled     = z / rho;
      if ( std::fabs( z_scaled ) < big_z_scaled ) {
        return std::log( z_scaled + std::sqrt( z_scaled * z_scaled + 1.f ) );
      } else {
        // apply correction using first order Taylor expansion of sqrt
        return z > 0.f ? std::log( 2.f * z_scaled + 0.5f / z_scaled ) : -std::log( -2.f * z_scaled );
      }
    }
    // case vector has rho = 0
    // constant value for the largest pseudorapidity possible with non-zero rho and double precision
    // std::log ( std::numeric_limits<long double>::max()/256.0l ) -
    //            std::log ( std::numeric_limits<long double>::denorm_min()*256.0l )
    //            + 16.0 * std::log(2.0);
    constexpr float eta_max = 22756.f;
    if ( LHCb::essentiallyZero( z ) ) return 0.f;
    if ( z < 0.f ) return z - eta_max;
    return z + eta_max;
  }

  LHCb::HltLumiSummary operator()( LHCb::Pr::Velo::Tracks const& forwards,
                                   LHCb::Pr::Velo::Tracks const& backwards ) const override {

    // init temporary counter name
    std::string counterName = "";
    // create a map of counter names to hits vector
    std::map<std::string, int> counterMap;
    // assign as keys the counter names and init the values to 0
    for ( auto i = 0u; i < m_counterNames.size(); ++i ) { counterMap[m_counterBaseName + m_counterNames[i]] = 0; }

    counterMap[m_counterBaseName + "Tracks"]         = forwards.size() + backwards.size();
    counterMap[m_counterBaseName + "FiducialTracks"] = 0;

    // loop over forward tracks
    for ( auto const& velotrack : forwards.scalar() ) {
      auto pos    = velotrack.closestToBeamStatePos();
      auto dir    = velotrack.closestToBeamStateDir();
      auto dirRho = std::sqrt( dir.x().cast() * dir.x().cast() + dir.y().cast() * dir.y().cast() );
      auto docaZ  = velo_DOCAz( pos.x().cast(), pos.y().cast(), dir.x().cast(), dir.y().cast() );
      auto eta    = eta_from_rho_z( dirRho, 1.f );

      if ( pos.z() > -300. * Gaudi::Units::mm && pos.z() < 300. * Gaudi::Units::mm && docaZ < 3.f * Gaudi::Units::mm ) {
        ++counterMap[m_counterBaseName + "FiducialTracks"];
      }

      auto etaBin{0};
      for ( auto etaEdge : m_etaBinEdges ) {
        if ( eta < etaEdge ) break;
        ++etaBin;
      }
      ++counterMap[m_counterBaseName + "TracksEtaBin" + std::to_string( etaBin )];
    }

    // loop over backward tracks
    for ( auto const& velotrack : backwards.scalar() ) {
      auto pos    = velotrack.closestToBeamStatePos();
      auto dir    = velotrack.closestToBeamStateDir();
      auto dirRho = std::sqrt( dir.x().cast() * dir.x().cast() + dir.y().cast() * dir.y().cast() );
      auto docaZ  = velo_DOCAz( pos.x().cast(), pos.y().cast(), dir.x().cast(), dir.y().cast() );
      auto eta    = eta_from_rho_z( dirRho, -1.f );

      if ( pos.z() > -300. * Gaudi::Units::mm && pos.z() < 300. * Gaudi::Units::mm && docaZ < 3.f * Gaudi::Units::mm ) {
        ++counterMap[m_counterBaseName + "FiducialTracks"];
      }

      auto etaBin{0};
      for ( auto etaEdge : m_etaBinEdges ) {
        if ( eta < etaEdge ) break;
        ++etaBin;
      }
      ++counterMap[m_counterBaseName + "TracksEtaBin" + std::to_string( etaBin )];
    }

    LHCb::HltLumiSummary summary{};

    for ( auto i = 0u; i < m_counterNames.size(); ++i ) {
      summary.addInfo( m_counterBaseName + m_counterNames[i], counterMap[m_counterBaseName + m_counterNames[i]] );
    }

    return summary;
  }

private:
  Gaudi::Property<std::string> m_counterBaseName{this, "CounterBaseName", "", "Prefix for luminosity counter names"};
  Gaudi::Property<std::vector<std::string>>                         m_counterNames{this,
                                                           "CounterNames",
                                                           {
                                                               "Tracks",
                                                               "FiducialTracks",
                                                               "TracksEtaBin0",
                                                               "TracksEtaBin1",
                                                               "TracksEtaBin2",
                                                               "TracksEtaBin3",
                                                               "TracksEtaBin4",
                                                               "TracksEtaBin5",
                                                               "TracksEtaBin6",
                                                               "TracksEtaBin7",
                                                           },
                                                           "Names of the counters"};
  Gaudi::Property<std::vector<unsigned>>                            m_counterMax{this,
                                                      "CounterMax",
                                                      {
                                                          1913,
                                                          1913,
                                                          68,
                                                          294,
                                                          302,
                                                          287,
                                                          471,
                                                          427,
                                                          328,
                                                          81,
                                                      },
                                                      "Required maximum values of the counters"};
  Gaudi::Property<std::map<std::string, std::pair<double, double>>> m_counterShiftAndScale{
      this,
      "CounterShiftAndScale",
      {},
      "Optional shift and scale factors used to process counters when encoding to a LumiSummary bank"};
  Gaudi::Property<std::vector<double>> m_etaBinEdges{
      this, "etaBinEdges", {-4., -3., -2., 2., 3., 4., 5.}, "Bin edges for track counters in ranges of eta"};
};

DECLARE_COMPONENT( LumiCountersFromVeloTracks )
