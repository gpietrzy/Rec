/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef DICT_PARTICLE2MCTRUTHDICT_H
#  define DICT_PARTICLE2MCTRUTHDICT_H 1

// Include files

/** @file dict/Particle2MCTruthDict.h
 *
 *
 *  @author Juan PALACIOS
 *  @date   2009-03-10
 */
#  include "P2MCP/IP2MCP.h"
#  include "P2MCP/P2MCPTypes.h"
#endif // DICT_PARTICLE2MCTRUTHDICT_H

namespace {
  std::vector<LHCb::MCParticle::ConstVector> __stdvector_mcpVC;
}
