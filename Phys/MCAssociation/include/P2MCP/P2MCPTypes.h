/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef P2MCP_P2MCPTYPES_H
#define P2MCP_P2MCPTYPES_H 1

// Include files
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Kernel/MCAssociation.h"
#include "Relations/Relation.h"
#include "Relations/Relation1D.h"
#include <vector>
/** @namespace P2MCPTypes P2MCP/P2MCPTypes.h
 *
 *
 *  Namespace containing types corresponding to the Particle -> MCParticle
 *  uni-directional weighted relationship.
 *
 *  @author Juan PALACIOS
 *  @date   2009-03-10
 */
namespace P2MCP {

  namespace Types {

    typedef LHCb::MCParticle::ConstVector FlatTree;
    typedef std::vector<FlatTree>         FlatTrees;

    typedef LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle> Table;

    typedef Relations::Relation<LHCb::Particle, LHCb::MCParticle> LightTable;
    typedef Table::Range                                          Range;
    typedef Table::IBase::Entry                                   Relation;
    typedef Table::IBase::Entries                                 Relations;
    typedef Table::To                                             To;
    typedef Table::From                                           From;

    typedef std::vector<MCAssociation> ToVector;
  } // namespace Types

} // namespace P2MCP
#endif // P2MCP_P2MCPTYPES_H
