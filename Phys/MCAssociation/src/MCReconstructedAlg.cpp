/*****************************************************************************\
* (c) Copyright 2021-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Core/FloatComparison.h"
#include "Event/CaloHypo.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"
#include "LHCbAlgs/Transformer.h"
#include "MCInterfaces/IMCReconstructed.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"

/// ProtoParticle -> MCParticle relation table
using Relation_PP2MC = LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double>;
/// MCParticle -> Reconstructed Object (Proto Particle) relation table
using Relation_MC2REC = LHCb::RelationWeighted1D<LHCb::MCParticle, LHCb::ProtoParticle, double>;
/// Algorithm output
using alg_out = Relation_MC2REC;
/// Algorithm base
using Transformer = LHCb::Algorithm::Transformer<alg_out( const LHCb::MCParticle::Range&, const Relation_PP2MC&,
                                                          const Relation_PP2MC& )>;

// ============================================================================
/** @class MCReconstructedAlg
 *  The Algorithm for associating MCParticle with reconstructed objects,
 *  it uses two relation tables, ChargedPP2MCP and NeutralPP2MCP, as input.
 *
 *  @param Input Location of MCParticles
 *  @param ChargedPP2MCP Charged ProtoParticle -> MCParticle relation table, created by Moore
 *  @param NeutralPP2MCP Neutral ProtoParticle -> MCParticle relation table, created by Moore
 *  @returns MC2Reconstructed: Relation table that associate MCParticle -> reconstructed object
 *
 */

class MCReconstructedAlg final : public Transformer {
public:
  MCReconstructedAlg( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc, {KeyValue{"Input", ""}, KeyValue{"ChargedPP2MCP", ""}, KeyValue{"NeutralPP2MCP", ""}},
                     KeyValue{"MC2Reconstructed", ""} ){};

  alg_out operator()( const LHCb::MCParticle::Range& mc_particles, const Relation_PP2MC& m_cpp2mcp,
                      const Relation_PP2MC& m_npp2mcp ) const override {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Start running MCReconstructedAlg Algorithm." << endmsg;

    /// Define outputs
    Relation_MC2REC mc2rec_out;

    /// Get relations
    auto relation_cpp2mcp = m_cpp2mcp.relations();
    auto relation_npp2mcp = m_npp2mcp.relations();

    for ( const auto& mc_particle : mc_particles ) {

      bool charged{mc_particle->particleID().threeCharge() != 0};

      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endmsg;
        verbose() << "Matching the MCP: Charged = " << charged << ", Key = " << mc_particle->key()
                  << ", PID = " << mc_particle->particleID().pid() << endmsg;
      }

      // Chose a relation table
      const auto& relation = charged ? relation_cpp2mcp : relation_npp2mcp;

      ///
      ///     Note:
      ///       Relation table inversion has problem with inverse_tag, so here we manually match the result,
      ///       we should implement the sort and the binary search algorithm if we got any throughput issue.
      ///       it can be done by std::sort and std::binary_search. BTW, the default weighted relation table
      ///       doesn't do binary search, only unweighted relation table do binary search.
      ///
      const LHCb::ProtoParticle* best_pp     = nullptr;
      double                     best_weight = 0;
      for ( const auto& entry : relation ) {
        /// Alias the relation components
        const auto& pp     = entry.from();
        const auto& mcp    = entry.to();
        const auto& weight = entry.weight();

        /// Maching condition
        if ( mcp != mc_particle || !pp || LHCb::essentiallyZero( weight ) ) continue;

        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Matched: pp = " << pp << ", weight = " << weight << endmsg;

        /// Case 1: store the best track
        if ( m_use_best_mcmatch ) {
          if ( weight > best_weight ) {
            best_pp     = pp;
            best_weight = weight;
          };
        }
        /// Case 2: store all tracks
        else {
          mc2rec_out.relate( mc_particle, pp, weight ).ignore();
        };
      };

      /// Once the best match is found
      if ( m_use_best_mcmatch ) {
        if ( !LHCb::essentiallyZero( best_weight ) && best_pp )
          mc2rec_out.relate( mc_particle, best_pp, best_weight ).ignore();
      };

      /// Update counter
      if ( !mc2rec_out.relations( mc_particle ).empty() )
        ++m_matched;
      else
        ++m_unmatched;

      /// Debug
      if ( msgLevel( MSG::VERBOSE ) ) {
        auto range_pp = mc2rec_out.relations( mc_particle );
        if ( !range_pp.empty() )
          verbose() << "Best PP: ptr = " << range_pp.back().to() << ", weight = " << range_pp.back().weight() << endmsg;
        else
          verbose() << "NO RECONSTRUCTED IS MATCHED!" << endmsg;

        verbose() << "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<" << endmsg;
      }
    };

    /// update event counter
    if ( !mc_particles.empty() ) ++m_event;

    return mc2rec_out;
  };

private:
  Gaudi::Property<bool> m_use_best_mcmatch{this, "use_best_mcmatch", true, "Only return the best matched track."};

  // counters (event and ghost)
  mutable Gaudi::Accumulators::Counter<> m_event{this, "#Events"};
  mutable Gaudi::Accumulators::Counter<> m_matched{this, "#Matched"};
  mutable Gaudi::Accumulators::Counter<> m_unmatched{this, "#Unmatched"};
};

DECLARE_COMPONENT( MCReconstructedAlg )
