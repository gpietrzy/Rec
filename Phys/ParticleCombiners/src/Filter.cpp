/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "PrFilters/PrFilter.h"

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Filter<LHCb::Event::ChargedBasics>, "ChargedBasicsFilter" )

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Filter<LHCb::Particle::Range>, "ParticleRangeFilter" )

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Filter<LHCb::MCParticle::Range>, "MCParticleRangeFilter" )
