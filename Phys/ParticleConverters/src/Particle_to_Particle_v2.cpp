/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Transformer.h"

#include "Event/Particle.h"
#include "Event/Particle_v2.h"
#include "Event/ProtoParticle.h"
#include "Event/SOATrackConversion.h"
#include "Event/Track_v1.h"
#include "Event/Track_v2.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "Event/ZipUtils.h"
#include "Functors/with_functors.h"
#include "Kernel/IParticle2State.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

#include <cmath>

namespace MuonTag          = LHCb::Event::v2::Muon::Tag;
namespace ChargedBasicsTag = LHCb::Event::ChargedBasicsTag;
using MuonStatusMasks      = LHCb::Event::v2::Muon::StatusMasks;
using MuonFlags            = LHCb::Event::flags_v<SIMDWrapper::scalar::types, MuonStatusMasks>;

using output_t = std::tuple<LHCb::Event::ChargedBasics, std::unique_ptr<LHCb::Event::v2::Muon::PIDs>,
                            std::unique_ptr<LHCb::Event::v3::Tracks>>;

namespace {
  struct TrackPredicate {
    using Signature                    = bool( const LHCb::Track& );
    static constexpr auto PropertyName = "TrackPredicate";
  };
  struct ProtoParticlePredicate {
    using Signature                    = bool( const LHCb::ProtoParticle& );
    static constexpr auto PropertyName = "ProtoParticlePredicate";
  };
} // namespace

class Proto2ChargedBasic
    : public with_functors<LHCb::Algorithm::MultiTransformer<output_t( LHCb::ProtoParticle::Range const&,
                                                                       LHCb::UniqueIDGenerator const& )>,
                           TrackPredicate, ProtoParticlePredicate> {

public:
  Proto2ChargedBasic( const std::string& name, ISvcLocator* pSvcLocator )
      : with_functors( name, pSvcLocator,
                       {KeyValue{"InputProtoParticles", LHCb::ProtoParticleLocation::Charged},
                        KeyValue{"InputUniqueIDGenerator", LHCb::UniqueIDGeneratorLocation::Default}},
                       {
                           KeyValue{"Particles", ""},
                           KeyValue{"MuonPIDs", ""},
                           KeyValue{"Tracks", ""},
                       } ) {}

  StatusCode initialize() override {
    return with_functors::initialize().andThen( [&] {
      const std::map<std::string, std::string> id_to_descriptor = {
          {"pion", "pi+"}, {"kaon", "K+"}, {"muon", "mu+"}, {"electron", "e+"}, {"proton", "p+"}};
      const auto result = id_to_descriptor.find( m_particleid.value() );
      if ( result == id_to_descriptor.end() ) {
        throw GaudiException( "Unknown ParticleID value: " + m_particleid.value(),
                              "FunctionalDiElectronMaker::initialize", StatusCode::FAILURE );
      }
      auto descriptor = ( *result ).second;
      m_particle_prop = m_particlePropertySvc->find( descriptor );
      if ( !m_particle_prop ) {
        throw GaudiException( "Could not find ParticleProperty for " + m_particleid.value(),
                              "FunctionalDiElectronMaker::initialize", StatusCode::FAILURE );
      }
      m_antiparticle_prop = m_particle_prop->antiParticle();
      return StatusCode::SUCCESS;
    } );
  }

  output_t operator()( LHCb::ProtoParticle::Range const& protos,
                       LHCb::UniqueIDGenerator const&    unique_id_gen ) const override {
    auto zn        = Zipping::generateZipIdentifier();
    auto muon_pids = std::make_unique<LHCb::Event::v2::Muon::PIDs>( zn );
    auto tracks =
        std::make_unique<LHCb::Event::v3::Tracks>( LHCb::Event::v3::TrackType::Long, unique_id_gen, zn, nullptr );
    LHCb::Event::ChargedBasics chargedbasics{tracks.get(), muon_pids.get()};
    int                        nparticles     = 0;
    int                        nantiparticles = 0;

    auto const& proto_pred = getFunctor<ProtoParticlePredicate>();
    auto const& track_pred = getFunctor<TrackPredicate>();
    for ( const auto* proto : protos ) {
      const auto* track = proto->track();
      // Sanity checks
      if ( track == nullptr ) {
        ++m_part_with_no_track;
        continue;
      }
      if ( track->states().empty() ) {
        ++m_part_with_empty_state;
        continue;
      }

      const auto selected_track = track_pred( *track );
      m_npassed_track_filter += selected_track;
      if ( !selected_track ) { continue; }

      const auto selected_proto = proto_pred( *proto );
      m_npassed_proto_filter += selected_proto;
      if ( !selected_proto ) { continue; }

      // Get the (anti)particle property corresponding to the charge we have
      const LHCb::ParticleProperty* prop = nullptr;
      if ( proto->charge() == (int)std::round( m_particle_prop->charge() ) ) {
        prop = m_particle_prop;
      } else if ( proto->charge() == (int)std::round( m_antiparticle_prop->charge() ) ) {
        prop = m_antiparticle_prop;
      }

      { // fill particles

        auto part = chargedbasics.emplace_back<SIMDWrapper::InstructionSet::Scalar>();

        part.field<ChargedBasicsTag::Mass>().set( prop->mass() );
        part.field<ChargedBasicsTag::ParticleID>().set( prop->particleID().pid() );

        // protos
        auto mu_pid = muon_pids->emplace_back<SIMDWrapper::InstructionSet::Scalar>();
        part.field<ChargedBasicsTag::MuonPID>().set( mu_pid.offset() );

        if ( auto* muonpid = proto->muonPID() ) {
          MuonFlags flags;
          flags.set<MuonStatusMasks::IsMuon>( muonpid->IsMuon() );
          // -- What about the other fields?
          mu_pid.field<MuonTag::Status>().set( flags );
          mu_pid.field<MuonTag::Chi2Corr>().set( muonpid->chi2Corr() );
        } else {
          mu_pid.field<MuonTag::Status>().set( MuonFlags( 0 ) ); // TODO make a correct invalid entry
          mu_pid.field<MuonTag::Chi2Corr>().set( std::numeric_limits<float>::lowest() );
        }
        if ( auto* richpid = proto->richPID() ) {
          part.field<ChargedBasicsTag::RichPIDCode>().set( richpid->pidResultCode() );
        } else {
          part.field<ChargedBasicsTag::RichPIDCode>().set( std::numeric_limits<int>::lowest() );
        }

        { // tracks
          auto const* track    = proto->track();
          auto        outTrack = tracks->emplace_back<SIMDWrapper::InstructionSet::Scalar>();
          LHCb::Event::conversion::convert_track( LHCb::Event::v3::TrackType::Long, outTrack, track, unique_id_gen );
          part.field<ChargedBasicsTag::Track>().set( outTrack.offset() );
        }

        // combdlls
        auto const pid = proto->globalChargedPID();
        part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::p )
            .set( pid ? pid->CombDLLp() : std::numeric_limits<float>::lowest() );
        part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::e )
            .set( pid ? pid->CombDLLe() : std::numeric_limits<float>::lowest() );
        part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::pi )
            .set( pid ? pid->CombDLLpi() : std::numeric_limits<float>::lowest() );
        part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::K )
            .set( pid ? pid->CombDLLk() : std::numeric_limits<float>::lowest() );
        part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::mu )
            .set( pid ? pid->CombDLLmu() : std::numeric_limits<float>::lowest() );
        part.field<ChargedBasicsTag::CombDLL>( ChargedBasicsTag::Hypo::d )
            .set( pid ? pid->CombDLLd() : std::numeric_limits<float>::lowest() );
      }
      if ( prop == m_particle_prop ) {
        ++nparticles;
      } else if ( prop == m_antiparticle_prop ) {
        ++nantiparticles;
      }
    }

    m_nparticles += nparticles;
    m_nantiparticles += nantiparticles;

    return std::make_tuple( std::move( chargedbasics ), std::move( muon_pids ), std::move( tracks ) );
  }

private:
  ServiceHandle<LHCb::IParticlePropertySvc> m_particlePropertySvc{this, "ParticleProperty",
                                                                  "LHCb::ParticlePropertySvc"};

  /// Tool for filling the particle's kinematics from a given state
  ToolHandle<IParticle2State> m_particle_from_state_tool = {this, "Particle2StateTool", "Particle2State"};

  /// Particle property information for matter particles to be created
  const LHCb::ParticleProperty* m_particle_prop = nullptr;
  /// Particle property information for antimatter particles to be created
  const LHCb::ParticleProperty* m_antiparticle_prop = nullptr;

  Gaudi::Property<std::string> m_particleid{this, "ParticleID", "UNDEFINED",
                                            "Particle species hypothesis to apply to each created object."};

  /// Number of created LHCb::Particle objects with particle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nparticles{this, "Nb created particles"};
  /// Number of created LHCb::Particle objects with antiparticle IDs
  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_nantiparticles{this, "Nb created anti-particles"};
  /// Number of protoparticles passing the protoparticle filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_proto_filter{this, "# passed ProtoParticle filter"};
  /// Number of tracks passing the track filter
  mutable Gaudi::Accumulators::BinomialCounter<> m_npassed_track_filter{this, "# passed Track filter"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_part_with_no_track{
      this, "Charged ProtoParticle with no Track found. Ignoring"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_part_with_empty_state{
      this, "Track has empty states. This is likely to be a bug"};
};

DECLARE_COMPONENT( Proto2ChargedBasic )
