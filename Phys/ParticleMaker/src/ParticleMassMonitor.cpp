/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/Particle.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"

/** @class ParticleMassMonitor
 *
 *  Trivial consumer to produce mass and momentum histograms out of LHCb::Particles,
 *  using the mean, sigma and bins specified by the user for the mass plots and a
 *  given range for the momentum plots. For the mass plots the range of the histogram
 *  is set to 5*sigma around the mean on each side.
 *
 *  Upon request, it also generates tuples with information about the particles.
 *
 */

// ParticleMassMonitor class definition
class ParticleMassMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::Particle::Range& ),
                                                             LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>> {

public:
  ParticleMassMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator, {KeyValue{"Particles", ""}} ) {}

  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      for ( unsigned int i = 0; i < m_particles.size(); i++ ) {

        // Mass histogram
        if ( i == 0 ) {
          m_histograms_mass.emplace( std::piecewise_construct, std::forward_as_tuple( 0 ),
                                     std::forward_as_tuple( this, m_bins, m_pdiff_bins, m_particles[i], m_mass, m_sigma,
                                                            m_minpt, m_maxpt, m_minp, m_maxp,
                                                            m_pdiff_range / Gaudi::Units::GeV ) );
        }

        // Momentum histograms
        m_histograms_momentum.emplace(
            std::piecewise_construct, std::forward_as_tuple( i ),
            std::forward_as_tuple( this, m_bins, m_particles[i], m_minpt, m_maxpt, m_minp, m_maxp ) );
      }
    } );
  }

  void operator()( LHCb::Particle::Range const& ) const override;

private:
  // Histogram settings. Default values are the standard ones for D0 -> KPi candidates
  Gaudi::Property<unsigned int> m_bins{this, "Bins", 100};
  Gaudi::Property<unsigned int> m_pdiff_bins{this, "PdiffBins", 20};
  Gaudi::Property<float>        m_mass{this, "MassMean", 1864.84 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_sigma{this, "MassSigma", 18 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_minpt{this, "MinPt", 800 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_maxpt{this, "MaxPt", 5000 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_minp{this, "MinP", 2000 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_maxp{this, "MaxP", 150000 * Gaudi::Units::MeV};
  Gaudi::Property<float>        m_pdiff_range{this, "PdiffDelta", 40000 * Gaudi::Units::MeV};

  // Vector with the name of the particles in the same order as in the particle combiner
  Gaudi::Property<std::vector<std::string>> m_particles{this, "ParticleType", {"D0", "Pi", "K"}};

  // Flag for tuple generation
  Gaudi::Property<bool> m_generateTuples{this, "GenerateTuples", false};
  // counters
  mutable Gaudi::Accumulators::Counter<> m_n1sigma{this, "Candidates in 1 sigma"};

  // Definition of histograms
  struct HistoMomentum {
    mutable Gaudi::Accumulators::Histogram<1> CandidatePt;
    mutable Gaudi::Accumulators::Histogram<1> CandidateMom;

    HistoMomentum( const ParticleMassMonitor* owner, unsigned int const& bins, std::string const& name,
                   float const& min_pt, float const& max_pt, float const& min_p, float const& max_p )
        : CandidatePt{owner, name + "_Pt", name + "_Pt", {bins, min_pt, max_pt, "pt [MeV/c]"}}
        , CandidateMom{owner, name + "_P", name + "_P", {bins, min_p, max_p, "p [MeV/c]"}} {}
  };

  struct HistoMass {
    mutable Gaudi::Accumulators::Histogram<1>        CandidateMass;
    mutable Gaudi::Accumulators::Histogram<2>        MassvsPt;
    mutable Gaudi::Accumulators::Histogram<2>        MassvsP;
    mutable Gaudi::Accumulators::ProfileHistogram<1> MassvsPdiff;

    HistoMass( const ParticleMassMonitor* owner, unsigned int const& bins, unsigned int const& bins_pdiff,
               std::string const& name, float const& mass, float const& sigma, float const& min_pt, float const& max_pt,
               float const& min_p, float const& max_p, float const& range_pdiff )
        : CandidateMass{owner,
                        name + "_mass",
                        name + " mass",
                        {bins, mass - 5 * sigma, mass + 5 * sigma, "m [MeV/c^{2}]"}}
        , MassvsPt{owner,
                   name + "_MassvsPt",
                   name + " mass vs pt",
                   {{bins, min_pt, max_pt, "pt [MeV/c]"}, {bins, mass - 5 * sigma, mass + 5 * sigma, "m [MeV/c^{2}]"}}}
        , MassvsP{owner,
                  name + "_MassvsP",
                  name + " mass vs p",
                  {{bins, min_p, max_p, "p [MeV/c]"}, {bins, mass - 5 * sigma, mass + 5 * sigma, "m [MeV/c^{2}]"}}}
        , MassvsPdiff{owner,
                      name + "_MassvsPdiff",
                      fmt::format( "m({}) - {:.2f} MeV/c^{{2}} vs daughter p difference", name, mass ),
                      {bins_pdiff, -range_pdiff, range_pdiff, "Daughter p_{+} - p_{-} [GeV/c]"}} {}
  };

  // Histogram objects are stored inside of maps
  std::map<int, HistoMomentum> m_histograms_momentum;
  std::map<int, HistoMass>     m_histograms_mass;

  // Function to calculate the momentum difference of the daughter particles
  double calculate_momentum_difference( const LHCb::Particle& particle1, const LHCb::Particle& particle2 ) const;
};

DECLARE_COMPONENT( ParticleMassMonitor )

// Algorithm
void ParticleMassMonitor::operator()( LHCb::Particle::Range const& particles ) const {
  auto b_1sig = m_n1sigma.buffer();
  for ( const auto& p : particles ) {
    const double m         = p->momentum().M();
    const auto   daughters = p->daughtersVector();
    const double pt        = p->pt();
    const double mom       = p->p();
    const double mom_difference =
        daughters.size() > 2 ? 0. : calculate_momentum_difference( *daughters[0], *daughters[1] );

    // Mass plots
    auto& mass_histo = m_histograms_mass.at( 0 );

    // count candidates in 1 sigma
    if ( std::abs( m_mass - m ) / m_sigma < 1 ) b_1sig++;

    ++mass_histo.CandidateMass[m];
    ++mass_histo.MassvsPt[{pt, m}];
    ++mass_histo.MassvsP[{mom, m}];
    if ( m_particles.size() == 3 ) {
      mass_histo.MassvsPdiff[mom_difference / Gaudi::Units::GeV] += m - m_mass; // Momentum in GeV for this plot
    }

    // Momentum plots
    for ( unsigned int i = 0; i < m_particles.size(); i++ ) {
      auto& momentum_histo = m_histograms_momentum.at( i );
      if ( i == 0 ) {
        ++momentum_histo.CandidatePt[pt];
        ++momentum_histo.CandidateMom[mom];
      } else {
        ++momentum_histo.CandidatePt[daughters[i - 1]->pt()];
        ++momentum_histo.CandidateMom[daughters[i - 1]->p()];
      }
    }

    // Code for tuple generation
    if ( m_generateTuples ) {
      const auto id            = p->particleID().pid();
      Tuple      particleTuple = nTuple( "Particletuples", "" );
      particleTuple->column( m_particles[0] + "_M", m ).ignore();
      particleTuple->column( m_particles[0] + "_Pt", pt ).ignore();
      particleTuple->column( m_particles[0] + "_ID", id ).ignore();
      particleTuple->column( m_particles[0] + "_P", mom ).ignore();

      for ( unsigned int i = 1; i < m_particles.size(); i++ ) {
        particleTuple->column( m_particles[i] + "_Pt", daughters[i - 1]->pt() ).ignore();
        particleTuple->column( m_particles[i] + "_ID", daughters[i - 1]->particleID().pid() ).ignore();
        particleTuple->column( m_particles[i] + "_P", daughters[i - 1]->p() ).ignore();
        particleTuple->column( m_particles[i] + "_PxoverPz", daughters[i - 1]->slopes().X() ).ignore();
        particleTuple->column( m_particles[i] + "_PyoverPz", daughters[i - 1]->slopes().Y() ).ignore();
      }

      if ( m_particles.size() == 3 ) { particleTuple->column( "Daughter_p_plus_p_minus", mom_difference ).ignore(); }
      particleTuple->write().ignore();
    }
  }
}

double ParticleMassMonitor::calculate_momentum_difference( const LHCb::Particle& particle1,
                                                           const LHCb::Particle& particle2 ) const {
  const int    charge_p1 = particle1.charge();
  const int    charge_p2 = particle2.charge();
  const double p1        = particle1.p();
  const double p2        = particle2.p();
  const double p_diff    = ( charge_p1 > 0 && charge_p2 < 0 ) ? p1 - p2 : p2 - p1;
  return p_diff;
}