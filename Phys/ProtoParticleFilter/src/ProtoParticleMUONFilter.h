/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file ProtoParticleMUONFilter.h
 *
 * Header file for algorithm ProtoParticleMUONFilter
 *
 * CVS Log :-
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-05-03
 */
//-----------------------------------------------------------------------------

#ifndef PROTOPARTICLEFILTER_ProtoParticleMUONFilter_H
#define PROTOPARTICLEFILTER_ProtoParticleMUONFilter_H 1

// base class
#include "ProtoParticleCALOFilter.h"

// Boost
#include "boost/lexical_cast.hpp"

//-----------------------------------------------------------------------------
/** @class ProtoParticleMUONFilter ProtoParticleMUONFilter.h
 *
 *  Filter tool to extend the simple DLL filter ProtoParticleDLLFilter
 *  with MUON system specific cuts and detector requirements.
 *
 *  @author Chris Jones   Christoper.Rob.Jones@cern.ch
 *  @date   2006-05-03
 */
//-----------------------------------------------------------------------------

class ProtoParticleMUONFilter : public ProtoParticleCALOFilter {

public: // Core Gaudi methods
  /// Standard constructor
  ProtoParticleMUONFilter( const std::string& type, const std::string& name, const IInterface* parent );

  virtual ~ProtoParticleMUONFilter(); ///< Destructor

protected:
  // Create a cut object from decoded cut options
  const ProtoParticleSelection::Cut* createCut( const std::string& tag, const std::string& delim,
                                                const std::string& value ) const override;
};

#endif // PROTOPARTICLEFILTER_ProtoParticleMUONFilter_H
