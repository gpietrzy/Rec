/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/**
Algorithms for handling 1D relation tables:

- to extract particle range from relation table (FROM = particles, TO= double).
Can optionally filter the tables using any functor with respect to the value
in TO column of the table. For single or multiple relation tables.
- to construct multiple relation tables from Additional Info
*/
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/TableView.h"
#include "Functors/with_functors.h"
#include "GaudiAlg/FunctionalDetails.h"
#include "GaudiAlg/MergingTransformer.h"
#include "GaudiAlg/SplittingTransformer.h"
#include "GaudiAlg/Transformer.h"
#include "Relations/Relation1D.h"

#include <map>
#include <stdexcept>
#include <string>

#include <type_traits>
#include <vector>

namespace LHCb::Phys::RelationTables {
  using BaseClass_t       = Gaudi::Functional::Traits::BaseClass_t<Gaudi::Algorithm>;
  using Relations         = LHCb::Relation1D<LHCb::Particle, double>;
  using VectorOfRelations = Gaudi::Functional::vector_of_const_<Relations>;

  /**
  Construct multiple relation tables from Additional Info
  */
  struct MakeRelTables final
      : Gaudi::Functional::SplittingTransformer<std::vector<Relations>( const LHCb::Particle::Range& )> {

    Gaudi::Property<std::vector<LHCb::ProtoParticle::additionalInfo>> m_vai{
        this, "AdditionalInfo", {LHCb::ProtoParticle::additionalInfo::IsPhoton}, "List of all Ai."};

    MakeRelTables( const std::string& name, ISvcLocator* svcLoc )
        : SplittingTransformer( name, svcLoc, KeyValue( "InputParticles", {} ), KeyValues( "OutputLocation", {""} ) ) {}

    std::vector<Relations> operator()( const LHCb::Particle::Range& input ) const override {
      assert( m_vai.size() == outputLocationSize() );
      std::vector<Relations> vai;
      vai.reserve( m_vai.size() );
      for ( unsigned int i = 0; i < m_vai.size(); i++ ) { vai.emplace_back( Relations{input.size()} ); }
      for ( const auto* particle : input ) {
        LHCb::ProtoParticle const* pp = particle->proto();
        for ( auto&& [i, m_ai] : range::enumerate( m_vai.value() ) ) {
          auto ai = pp ? pp->info( m_ai, Functors::invalid_value ) : Functors::invalid_value;
          vai[i].relate( particle, ai ).ignore();
        }
      }
      return vai;
    }
  };

  DECLARE_COMPONENT( LHCb::Phys::RelationTables::MakeRelTables )

  /**
  Predicate from templated type to bool, to be used with_functors
  */
  template <typename Arg>
  struct RelCuts {
    constexpr static auto PropertyName = "RelCuts";
    using InputType                    = Arg;
    using Signature                    = bool( Arg );
  };

  using Row = TableView<Relations, std::string>::Row;

  /**
  Filter multiple relation tables of type (particle : value) and return particle range
  */
  struct FilterRelTables final
      : with_functors<
            Gaudi::Functional::MergingTransformer<LHCb::Particle::Selection( VectorOfRelations const& ), BaseClass_t>,
            RelCuts<Row>> {

    FilterRelTables( const std::string& name, ISvcLocator* svcLoc )
        : with_functors<
              Gaudi::Functional::MergingTransformer<LHCb::Particle::Selection( const VectorOfRelations& ), BaseClass_t>,
              RelCuts<Row>>{name, svcLoc, KeyValues( "InputTables", {} ), KeyValue( "OutputLocation", "" )} {}

    LHCb::Particle::Selection operator()( const VectorOfRelations& input ) const override {
      auto const&               predicate = this->template getFunctor<RelCuts<Row>>();
      LHCb::Particle::Selection selection;
      std::vector<std::string>  locations;
      locations.reserve( inputLocationSize() );
      for ( unsigned i = 0; i < inputLocationSize(); ++i ) { locations.emplace_back( inputLocation( i ) ); }
      for ( Row row : TableView{input, locations} ) {
        if ( predicate( row ) ) { selection.push_back( row.from() ); }
      }
      return selection;
    }
  };

  DECLARE_COMPONENT( LHCb::Phys::RelationTables::FilterRelTables )

  /**
  Predicate from templated type to bool, to be used with_functors
  */
  struct Pred {
    constexpr static auto PropertyName = "Predicate";
    using InputType                    = double;
    using Signature                    = bool( double );
  };

  /**
  Filter relation table of type (particle : value) and return particle range
  */
  struct FilterRelTable final
      : with_functors<Gaudi::Functional::Transformer<
                          LHCb::Particle::Selection( const LHCb::Relation1D<LHCb::Particle, double>& ), BaseClass_t>,
                      Pred> {

    FilterRelTable( const std::string& name, ISvcLocator* svcLoc )
        : with_functors<Gaudi::Functional::Transformer<
                            LHCb::Particle::Selection( const LHCb::Relation1D<LHCb::Particle, double>& ), BaseClass_t>,
                        Pred>::with_functors( name, svcLoc, KeyValue( "InputTable", "" ),
                                              KeyValue( "OutputLocation", "" ) ) {}

    LHCb::Particle::Selection operator()( const LHCb::Relation1D<LHCb::Particle, double>& input ) const override {
      auto const&               predicate = this->template getFunctor<Pred>();
      LHCb::Particle::Selection selection;
      for ( const auto& rel : input.relations() ) {
        if ( predicate( rel.to() ) ) { selection.push_back( rel.from() ); }
      }
      return selection;
    }
  };

  DECLARE_COMPONENT( LHCb::Phys::RelationTables::FilterRelTable )

  /**
  Construct relation table from Additional Info
  */
  struct AiToRelTable final
      : Gaudi::Functional::Transformer<LHCb::Relation1D<LHCb::Particle, double>( const LHCb::Particle::Range& ),
                                       BaseClass_t> {

    Gaudi::Property<LHCb::ProtoParticle::additionalInfo> m_ai{this, "AdditionalInfo",
                                                              LHCb::ProtoParticle::additionalInfo::IsPhoton,
                                                              "Assign which additionalInfo we're retrieving."};

    AiToRelTable( const std::string& name, ISvcLocator* svcLoc )
        : Transformer( name, svcLoc, KeyValue( "InputParticles", "" ), KeyValue( "OutputLocation", "" ) ) {}

    LHCb::Relation1D<LHCb::Particle, double> operator()( const LHCb::Particle::Range& input ) const override {
      LHCb::Relation1D<LHCb::Particle, double> table;
      for ( const auto* particle : input ) {
        LHCb::ProtoParticle const* pp = particle->proto();
        auto                       ai = pp ? pp->info( m_ai, Functors::invalid_value ) : Functors::invalid_value;
        table.relate( particle, ai ).ignore();
      }
      return table;
    }
  };

  DECLARE_COMPONENT( LHCb::Phys::RelationTables::AiToRelTable )

} // namespace LHCb::Phys::RelationTables
