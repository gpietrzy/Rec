/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle_v2.h"
#include "Event/UniqueIDGenerator.h"
#include "Functors/with_functors.h"
#include "Kernel/AllocatorUtils.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"
#include "LHCbMath/MatVec.h"
#include "SelKernel/ParticleCombination.h"
#include "SelKernel/Utilities.h"

#include "LHCbAlgs/Transformer.h"

#include <tuple>

namespace SelAlgorithms::CombineTracksSIMD {
  namespace detail {
    template <SIMDWrapper::InstructionSet Backend>
    using int_b_v = typename SIMDWrapper::type_map_t<Backend>::int_v;
    template <SIMDWrapper::InstructionSet Backend>
    using mask_b_v = typename SIMDWrapper::type_map_t<Backend>::mask_v;
    template <SIMDWrapper::InstructionSet Backend>
    using float_b_v = typename SIMDWrapper::type_map_t<Backend>::float_v;

    template <std::size_t N, typename T>
    using VecN = LHCb::LinAlg::Vec<T, N>;

    template <std::size_t N, typename T>
    using SymNxN = LHCb::LinAlg::MatSym<T, N>;

    template <std::size_t N, std::size_t M, typename T>
    using Matrix = LHCb::LinAlg::Mat<T, N, M>;

    template <std::size_t N, typename T>
    using MatrixNxN = LHCb::LinAlg::Mat<T, N>;

    // Shorthand for below.
    template <typename T, std::size_t N>
    using FilterTransform = LHCb::Algorithm::MultiTransformerFilter<std::tuple<LHCb::Event::Composites>(
        EventContext const&, LHCb::UniqueIDGenerator const&, T const& )>;

    // Get the name of the Mth combination cut of an N-particle combiner
    // Very ugly, but not worth the effort to do something beautiful and constexpr
    template <std::size_t N, std::size_t M>
    std::string combinationCutName() {
      static_assert( N >= 2 );
      static_assert( M + 1 < N );
      std::string name{"Combination"};
      if ( M + 2 < N ) {
        // M = 0 => "12"
        // M = 1 => "123"
        // etc.
        name += "12";
        for ( auto i = 1ul; i <= M; ++i ) { name += std::to_string( i + 2 ); }
      }
      name += "Cut";
      return name;
    }

    // Tag types for functors/dumping
    template <typename T, std::size_t N, SIMDWrapper::InstructionSet Backend = SIMDWrapper::Scalar>
    struct Comb {
      using zip_t          = typename LHCb::Event::simd_zip_t<Backend, T>;
      using load_proxy_t   = typename zip_t::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::Contiguous>;
      using bcast_proxy_t  = typename zip_t::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScalarFill>;
      using gather_proxy_t = typename zip_t::template zip_proxy_type<LHCb::Pr::ProxyBehaviour::ScatterGather>;
      template <std::size_t M>
      using Combination = std::conditional_t<
          M == 2, Sel::ParticleCombination<load_proxy_t, bcast_proxy_t>,
          boost::mp11::mp_append<boost::mp11::mp_repeat_c<Sel::ParticleCombination<gather_proxy_t>, M - 1>,
                                 Sel::ParticleCombination<bcast_proxy_t>>>;
      // Combination12Cut (M = 0), Combination123Cut (M = 1), ..., CombinationCut (M = N-1)
      template <std::size_t M>
      struct Cut {
        using Signature                       = mask_b_v<Backend>( Functors::mask_arg_t, mask_b_v<Backend> const&,
                                             Combination<M + 2> const& );
        inline static auto const PropertyName = combinationCutName<N, M>();
      };
    };

    template <typename T, std::size_t N, SIMDWrapper::InstructionSet Backend = SIMDWrapper::Scalar>
    struct VertexCut {
      using particle_t =
          typename LHCb::Event::Composites::template proxy_type<LHCb::Event::resolve_instruction_set_v<Backend>,
                                                                LHCb::Pr::ProxyBehaviour::Contiguous,
                                                                LHCb::Event::Composites const>;
      using Signature = mask_b_v<Backend>( Functors::mask_arg_t, mask_b_v<Backend> const&, particle_t const& );
      constexpr static auto PropertyName = "VertexCut";
    };

    template <typename T, std::size_t N, SIMDWrapper::InstructionSet SIMDBackend>
    struct base_helper {
      // For the scalar backend we have all cuts once, with a vector backend we
      // have them all twice -- once as vector cuts and once as scalar fallbacks
      // This makes an mp_list of integral_constant<InstructionSet, X> types with
      // either one or two members.
      using backend_ts =
          std::conditional_t<SIMDBackend == SIMDWrapper::Scalar,
                             boost::mp11::mp_list_c<SIMDWrapper::InstructionSet, SIMDBackend>,
                             boost::mp11::mp_list_c<SIMDWrapper::InstructionSet, SIMDBackend, SIMDWrapper::Scalar>>;

      // Bake T and N into aliases that we can pass to mp_product.
      template <typename M, typename Backend>
      using CombCut = typename Comb<T, N, Backend::value>::template Cut<M::value>;
      template <typename Backend>
      using VtxCut = VertexCut<T, N, Backend::value>;
      // Make with_functors<FilterTransform<T, N>,
      //                    [cartesian expansion of CombCut<A, B>...],
      //                    VtxCut<B>...>
      // Where A is an integer from 0 to N-2 and B is the set of SIMD backends
      using type = boost::mp11::mp_append<
          with_functors<FilterTransform<T, N>>,
          boost::mp11::mp_product<CombCut, boost::mp11::mp_from_sequence<std::make_index_sequence<N - 1>>, backend_ts>,
          boost::mp11::mp_product<VtxCut, backend_ts>>;
    };

    template <typename T, std::size_t N, SIMDWrapper::InstructionSet SIMDBackend>
    using base_t = typename base_helper<T, N, SIMDBackend>::type;

    template <typename F>
    struct MiniState {
      F            x{}, y{}, tx{}, ty{}, z{}, qOverP{};
      SymNxN<5, F> cov{};

      template <typename T, std::enable_if_t<Sel::Utils::canBeExtrapolatedDownstream_v<T>, int> = 0>
      MiniState( T const& other )
          : x{other.x()}, y{other.y()}, tx{other.tx()}, ty{other.ty()}, z{other.z()}, qOverP{other.qOverP()} {
        auto const& other_cov = other.covariance();
        LHCb::Utils::unwind<0, 5>( [this, &other_cov]( auto i ) {
          LHCb::Utils::unwind<i, 5>( [this, i, &other_cov]( auto j ) { cov( i, j ) = other_cov( i, j ); } );
        } );
      }

      void linear_transport_to( F const& new_z ) {
        const auto dz  = new_z - z;
        const auto dz2 = dz * dz;
        x += dz * tx;
        y += dz * ty;
        cov( 0, 0 ) += dz2 * cov( 2, 2 ) + 2 * dz * cov( 2, 0 );
        cov( 1, 0 ) += dz2 * cov( 3, 2 ) + dz * ( cov( 3, 0 ) + cov( 2, 1 ) );
        cov( 2, 0 ) += dz * cov( 2, 2 );
        cov( 3, 0 ) += dz * cov( 3, 2 );
        cov( 4, 0 ) += dz * cov( 4, 2 );
        cov( 1, 1 ) += dz2 * cov( 3, 3 ) + 2 * dz * cov( 3, 1 );
        cov( 2, 1 ) += dz * cov( 3, 2 );
        cov( 3, 1 ) += dz * cov( 3, 3 );
        cov( 4, 1 ) += dz * cov( 4, 3 );
        z = new_z;
      }

      MatrixNxN<2, F> covXT() const { return cov.template sub<MatrixNxN<2, F>, 2, 0>(); }
    };

    template <typename T, std::size_t N, typename F, std::size_t... Is>
    auto transform_helper( std::array<T, N> const& arr, F&& fun, std::index_sequence<Is...> ) {
      return std::array<decltype( fun( std::declval<T>() ) ), N>{fun( std::get<Is>( arr ) )...};
    }

    template <typename T, std::size_t N, typename F>
    auto transform( std::array<T, N> const& arr, F&& fun ) {
      return transform_helper( std::forward<decltype( arr )>( arr ), std::forward<F>( fun ),
                               std::make_index_sequence<N>{} );
    }

    template <SIMDWrapper::InstructionSet Backend, std::size_t N, typename buffered_binomial_t>
    inline mask_b_v<Backend>
    do_vertex_fit( std::array<MiniState<float_b_v<Backend>>, N>&                     current_states,
                   std::array<float_b_v<Backend>, N> const&                          current_masses,
                   std::array<int_b_v<Backend>, N> const&                            current_child_indices,
                   std::array<int_b_v<Backend>, N> const&                            current_zip_families,
                   std::vector<LHCb::UniqueIDGenerator::ID<int_b_v<Backend>>> const& current_unique_ids,
                   mask_b_v<Backend> const& mask, LHCb::Event::Composites& tmp_storage,
                   buffered_binomial_t& npassed_vertex_fit ) {
      using simd_t   = typename SIMDWrapper::type_map_t<Backend>;
      using int_v    = typename simd_t::int_v;
      using mask_v   = typename simd_t::mask_v;
      using float_v  = typename simd_t::float_v;
      auto const& s0 = std::get<0>( current_states );
      auto const& s1 = std::get<1>( current_states );
      // STEP 1: Define an initial position with the first two states, stolen from TrackVertexUtils::poca
      // define d^2 = ( x0 + mu0*tx0 - x1 - mu1*tx1)^2 +  ( y0 + mu0*ty0 - x1 - mu1*ty1)^2 + (z0 + mu0 - z1 - mu1)^2
      // compute (half) 2nd derivative to mu0 and mu1 in point mu0=mu1=0
      auto const second00 = s0.tx * s0.tx + s0.ty * s0.ty + 1.f;
      auto const second11 = s1.tx * s1.tx + s1.ty * s1.ty + 1.f;
      auto const second01 = -s0.tx * s1.tx - s0.ty * s1.ty - 1.f;
      // compute inverse matrix, but stop if determinant not positive
      auto const det = second00 * second11 - second01 * second01;
      // OL: NN wrote 'abs( det )' but the comment above suggests it should be 'det'
      auto const prelim_fit_mask = mask && ( abs( det ) > 0.f );

      auto const secondinv00 = second11 / det;
      auto const secondinv11 = second00 / det;
      auto const secondinv01 = -second01 / det;

      // compute (half) first derivative
      auto first0 = s0.tx * ( s0.x - s1.x ) + s0.ty * ( s0.y - s1.y ) + ( s0.z - s1.z );
      auto first1 = -s1.tx * ( s0.x - s1.x ) - s1.ty * ( s0.y - s1.y ) - ( s0.z - s1.z );

      // compute mu0 and mu1 with delta-mu = - secondderivative^-1 * firstderivative
      auto const mu0 = -( secondinv00 * first0 + secondinv01 * first1 );
      auto const mu1 = -( secondinv11 * first1 + secondinv01 * first0 );

      // the initial point, this one will be updated in the fit loop
      VecN<3, float_v> vertex_position{0.5f * ( s0.x + mu0 * s0.tx + s1.x + mu1 * s1.tx ),
                                       0.5f * ( s0.y + mu0 * s0.ty + s1.y + mu1 * s1.ty ),
                                       0.5f * ( s0.z + mu0 + s1.z + mu1 )};

      // std::cout << "new vertex_position: " << vertex_position << std::endl;
      // TODO to match the particlevertexfitter, we have to choose another state if we are inside the beam pipe!

      // prepare some data that will change within the vertex loop
      auto fitted_slopes = detail::transform( current_states, []( auto const& state ) {
        return VecN<2, float_v>{state.tx, state.ty};
      } );

      // check if need this actually here
      auto weight_matrices = detail::transform( current_states, []( auto const& ) { return SymNxN<2, float_v>{}; } );

      auto projection_matrices =
          detail::transform( current_states, []( auto const& ) { return Matrix<2, 3, float_v>{}; } );

      SymNxN<3, float_v> poscov{};
      float_v            chi2{};
      int_v              ndof{-3 + 2 * N};
      mask_v             converged = simd_t::mask_false();
      mask_v             success   = simd_t::mask_true();

      constexpr auto max_iter       = 10;
      constexpr auto max_delta_chi2 = 0.01f;
      for ( auto iter = 0; iter < max_iter && !all( converged ); ++iter ) {
        chi2                = 0.;
        auto halfD2ChisqDX2 = LHCb::LinAlg::initialize_with_zeros<SymNxN<3, float_v>>();
        auto halfDChisqDX   = LHCb::LinAlg::initialize_with_zeros<VecN<3, float_v>>();
        LHCb::Utils::unwind<0, N>( [&]( auto state_idx ) {
          // STEP 3.1: project
          auto& state             = current_states[state_idx];
          auto& weight_matrix     = weight_matrices[state_idx];
          auto& fitted_slope      = fitted_slopes[state_idx];
          auto& projection_matrix = projection_matrices[state_idx];
          state.linear_transport_to( vertex_position( 2 ) );

          { // weight_matrix = inverted position covariance
            // how about invChol?
            auto det = state.cov( 0, 0 ) * state.cov( 1, 1 ) - state.cov( 0, 1 ) * state.cov( 0, 1 );
            // if (det == T(0.)) { return false; }
            weight_matrix( 0, 0 ) = state.cov( 1, 1 ) / det;
            weight_matrix( 1, 1 ) = state.cov( 0, 0 ) / det;
            weight_matrix( 0, 1 ) = -1.f * state.cov( 0, 1 ) / det;
          }

          VecN<2, float_v> residual{vertex_position( 0 ) - state.x, vertex_position( 1 ) - state.y};

          projection_matrix( 0, 0 ) = projection_matrix( 1, 1 ) = 1.f;
          projection_matrix( 0, 1 ) = projection_matrix( 1, 0 ) = 0.f;
          projection_matrix( 0, 2 )                             = -fitted_slope( 0 );
          projection_matrix( 1, 2 )                             = -fitted_slope( 1 );

          auto _intermediate = projection_matrix.transpose() * weight_matrix.cast_to_mat();
          // updating relevant fit results
          halfD2ChisqDX2 = halfD2ChisqDX2 + ( _intermediate * projection_matrix ).cast_to_sym();
          halfDChisqDX   = halfDChisqDX + _intermediate * residual;
          chi2           = chi2 + LHCb::LinAlg::similarity( residual, weight_matrix );
        } );

        // Step 3.2: calculate change in position and the covariance
        std::tie( success, poscov ) = halfD2ChisqDX2.invChol();

        auto delta_pos  = poscov * halfDChisqDX * float_v{-1.f};
        vertex_position = vertex_position + delta_pos;

        // add to chi2
        auto delta_chi2 = delta_pos.dot( halfDChisqDX );
        chi2            = chi2 + delta_chi2;
        converged       = ( -1.f * delta_chi2 ) < max_delta_chi2;

        if ( !all( converged ) ) { // FIXME apply mask to all operations? its for free anyway
          // update the slopes of the daughters
          LHCb::Utils::unwind<0, N>( [&]( auto state_idx ) {
            auto&            fitted_slope  = fitted_slopes[state_idx];
            auto const&      state         = current_states[state_idx];
            auto const&      weight_matrix = weight_matrices[state_idx];
            auto const       dz            = vertex_position( 2 ) - state.z;
            VecN<2, float_v> res{vertex_position( 0 ) - ( state.x + state.tx * dz ),
                                 vertex_position( 1 ) - ( state.y + state.ty * dz )};
            fitted_slope =
                VecN<2, float_v>{state.tx, state.ty} + state.covXT().transpose() * weight_matrix.cast_to_mat() * res;
          } );
        }
      }
      // Fit iteration completed
      auto const        fit_mask  = prelim_fit_mask && converged && success;
      std::size_t const fit_count = popcount( fit_mask );

      // Record the fit success statistics.
      npassed_vertex_fit += {fit_count, std::size_t( popcount( prelim_fit_mask ) )};

      // Now we've recorded statistics we could shortcircuit if we wanted.
      // if ( !fit_count ) { return; }

      // calculate the 4 momentum
      // TODO does that even make sense to do? why not just save tx,ty,qOverP
      auto p4          = LHCb::LinAlg::initialize_with_zeros<VecN<4, float_v>>();
      auto p4cov       = LHCb::LinAlg::initialize_with_zeros<SymNxN<4, float_v>>();
      auto gain_matrix = LHCb::LinAlg::initialize_with_zeros<Matrix<4, 3, float_v>>();
      LHCb::Utils::unwind<0, N>( [&]( auto state_idx ) { // depends on children
        auto const& state         = current_states[state_idx];
        auto&       fitted_slope  = fitted_slopes[state_idx];
        auto const& weight_matrix = weight_matrices[state_idx];
        // update tx, ty, qOverP
        auto             dz = vertex_position( 2 ) - state.z;
        VecN<2, float_v> res{vertex_position( 0 ) - ( state.x + fitted_slope( 0 ) * dz ),
                             vertex_position( 1 ) - ( state.y + fitted_slope( 1 ) * dz )};
        auto const       Vba    = state.cov.template sub<Matrix<3, 2, float_v>, 2, 0>();
        auto             K      = Vba * weight_matrix.cast_to_mat();
        auto             tx     = state.tx + ( K * res )( 0 );
        auto             ty     = state.ty + ( K * res )( 1 );
        auto             qOverP = state.qOverP + ( K * res )( 2 );

        // update 4-momentum
        auto const mass = current_masses[state_idx];
        auto       p    = 1.f / abs( qOverP );
        auto       n2   = 1.f + tx * tx + ty * ty;
        auto       n    = sqrt( n2 );
        auto       pz   = p / n;
        auto       px   = tx * pz;
        auto       py   = ty * pz;
        auto       E    = sqrt( p * p + mass * mass );
        p4              = p4 + VecN<4, float_v>{px, py, pz, E}; // TODO check whether this gives right results
                                                                // it was E px py pz before

        Matrix<4, 3, float_v> dp4dmom{}; // jacobi matrix of del(p4)/del(tx,ty,qOverP)

        auto n3         = n * n * n;
        dp4dmom( 0, 0 ) = p * ( 1.f + ty * ty ) / n3; // dpx/dtx
        dp4dmom( 0, 1 ) = p * tx * -ty / n3;          // dpx/dty
        dp4dmom( 0, 2 ) = -px / qOverP;               // dpx/dqop

        dp4dmom( 1, 0 ) = p * ty * -tx / n3;        // dpy/dtx
        dp4dmom( 1, 1 ) = p * ( 1 + tx * tx ) / n3; // dpy/dty
        dp4dmom( 1, 2 ) = -py / qOverP;             // dpy/dqop

        dp4dmom( 2, 0 ) = pz * -tx / n2; // dpz/dtx
        dp4dmom( 2, 1 ) = pz * -ty / n2; // dpz/dtx
        dp4dmom( 2, 2 ) = -pz / qOverP;  // dpz/dqop

        dp4dmom( 3, 0 ) = 0.0;                 // dE/dtx
        dp4dmom( 3, 1 ) = 0.0;                 // dE/dty
        dp4dmom( 3, 2 ) = p / E * -p / qOverP; // dE/dqop

        auto FK = dp4dmom * K;

        p4cov = p4cov + LHCb::LinAlg::similarity( dp4dmom, state.cov.template sub<SymNxN<3, float_v>, 2, 2>() ) -
                LHCb::LinAlg::similarity( FK, state.cov.template sub<SymNxN<2, float_v>, 0, 0>() );

        gain_matrix = gain_matrix + FK * projection_matrices[state_idx];
        // TODO electrons
      } );
      p4cov                = p4cov + LHCb::LinAlg::similarity( gain_matrix, poscov );
      auto const momposcov = gain_matrix * poscov.cast_to_mat();

      // Actually fill the output storage
      tmp_storage.emplace_back<Backend>( vertex_position, p4, int_v{0} /* parent PID */, chi2, ndof, poscov, p4cov,
                                         momposcov, current_child_indices, current_zip_families, current_unique_ids );
      // Return a mask of the validity of the first chunk of tmp_storage
      return fit_mask;
    }

    template <typename T, std::size_t... Is>
    constexpr T binom_impl( T n, std::integer_sequence<T, Is...> ) {
      return ( ( n - Is ) * ... ) / ( ( Is + 1 ) * ... );
    }

    // n choose k for small compile-time-constant k
    template <std::size_t k, typename T>
    constexpr T binom( T n ) {
      return binom_impl( n, std::make_integer_sequence<T, k>{} );
    }
  } // namespace detail

  /** @class CombineTracksSIMD CombineTracksSIMD.cpp
   *
   *  @tparam T TODO
   */
  template <typename T, std::size_t N, SIMDWrapper::InstructionSet SIMDBackend = SIMDWrapper::Best>
  struct CombineTracksSIMD final : public detail::base_t<T, N, SIMDBackend> {
    using base_t   = typename detail::base_t<T, N, SIMDBackend>;
    using KeyValue = typename base_t::KeyValue;
    // Bake T and N into some shorthand aliases
    template <std::size_t M, SIMDWrapper::InstructionSet Backend>
    using Combination = typename detail::Comb<T, N, Backend>::template Combination<M>;
    template <std::size_t M, SIMDWrapper::InstructionSet Backend>
    using CombCut = typename detail::Comb<T, N, Backend>::template Cut<M>;

    CombineTracksSIMD( const std::string& name, ISvcLocator* pSvcLocator )
        : base_t( name, pSvcLocator,
                  {KeyValue{"InputUniqueIDGenerator", LHCb::UniqueIDGeneratorLocation::Default},
                   KeyValue{"InputTracks", ""}},
                  {KeyValue{"Output", ""}} ) {}

    StatusCode initialize() override {
      StatusCode sc = base_t::initialize();
      if ( sc.isFailure() ) { return sc; }
      m_assumed_mass.useUpdateHandler();
      return sc;
    }

    std::tuple<bool, LHCb::Event::Composites> operator()( EventContext const&            evtCtx,
                                                          LHCb::UniqueIDGenerator const& unique_id_gen,
                                                          T const&                       raw_in ) const override {
      // Create a parameter pack for manipulating the N-1 combination cuts
      return dispatch( evtCtx, unique_id_gen, raw_in, std::make_index_sequence<N - 1>{} );
    }

  private:
    // Runtime dispatch to either the vectorised or scalar implementation
    template <std::size_t... NCombinationCuts>
    std::tuple<bool, LHCb::Event::Composites> dispatch( EventContext const&            evtCtx,
                                                        LHCb::UniqueIDGenerator const& unique_id_gen, T const& raw_in,
                                                        std::index_sequence<NCombinationCuts...> ) const {
      // Try to get the vectorised versions of the combination and vertex cuts
      auto const  vector_comb_cuts  = this->template getFunctors<CombCut<NCombinationCuts, SIMDBackend>...>();
      auto const& vector_vertex_cut = this->template getFunctor<detail::VertexCut<T, N, SIMDBackend>>();
      // See if we got **all** the vectorised versions
      if ( ( std::get<NCombinationCuts>( vector_comb_cuts ) && ... ) && vector_vertex_cut ) {
        // Yes, do all the hard work using the vectorised versions
        return do_the_work<SIMDBackend>( evtCtx, unique_id_gen, raw_in, vector_comb_cuts, vector_vertex_cut,
                                         std::index_sequence<NCombinationCuts...>{} );
      } else {
        // Presumably there was a cache miss, dispatch to the implementation with JIT-able cuts
        return do_the_work<SIMDWrapper::Scalar>(
            evtCtx, unique_id_gen, raw_in,
            this->template getFunctors<CombCut<NCombinationCuts, SIMDWrapper::Scalar>...>(),
            this->template getFunctor<detail::VertexCut<T, N>>(), std::index_sequence<NCombinationCuts...>{} );
      }
    }

    // This is the meat of the implementation, inside here the SIMD backend is
    // known. An integer pack satisfying sizeof...(NCombinationCuts) == N-1
    // is passed in for convenience
    template <SIMDWrapper::InstructionSet Backend, std::size_t... NCombinationCuts>
    std::tuple<bool, LHCb::Event::Composites>
    do_the_work( EventContext const& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen, T const& raw_in,
                 std::tuple<Functors::Functor<typename CombCut<NCombinationCuts, Backend>::Signature> const&...>
                                                                                                unprepared_combination_cuts,
                 Functors::Functor<typename detail::VertexCut<T, N, Backend>::Signature> const& unprepared_vertex_cut,
                 std::index_sequence<NCombinationCuts...> ) const {
      using simd_t  = typename SIMDWrapper::type_map_t<Backend>;
      using int_v   = typename simd_t::int_v;
      using mask_v  = typename simd_t::mask_v;
      using float_v = typename simd_t::float_v;
      // Get an iterable version of the input if need be -- this should be a noop if the input is already a zip
      auto const in = LHCb::Event::make_zip<Backend>( raw_in );

      // Prepare the output storage
      auto const memResource = LHCb::getMemResource( evtCtx );
      std::tuple ret{false, LHCb::Event::Composites{unique_id_gen, Zipping::generateZipIdentifier(), memResource}};
      // Can't use a structured binding here because we need to capture these
      // names in lambdas below (and clang doesn't let you do that...)
      auto& filter_pass = std::get<0>( ret );
      auto& storage     = std::get<1>( ret );
      // Estimate the required output capacity based on what we've seen so far
      {
        // Number of events processed so far
        auto const num_events = std::max( 10ul, m_output_container_reallocated.nFalseEntries() );
        // Number of accepted composite particles so far
        auto const num_parts = std::max( 100ul, m_npassed_vertex_cut.nTrueEntries() );
        // Average number of accepted composites in an event, and a fudge...
        constexpr auto fudge_factor = 4;
        storage.reserve( fudge_factor * num_parts / num_events );
        // TODO I think it would be better to just use some 99th (or whatever)
        //      percentile value of the final container size, rather than
        //      taking some multiple of the number of input tracks or maximum
        //      number of combinations. Also note that this is going to need
        //      some exclusions in the tests, as using a rolling estimate will
        //      introduce instabilities in e.g. counters that record how many
        //      times the container had to reallocate.
      }
      // Store the initial capacity so we can record whether or not we had to
      // reallocate during processing. Note that SOACollection does **not**
      // guarantee that this is the same as the value we reserved, it will in
      // fact be a bit more
      auto const initial_capacity = storage.capacity();

      // Temporary storage used as a buffer before the mother cut
      // TODO make the scheduler provide a second memory resource that is reset after every algorithm and use that here
      LHCb::Event::Composites tmp_storage{unique_id_gen, storage.zipIdentifier(), memResource};
      tmp_storage.reserve( simd_t::size );
      auto tmp_storage_view = tmp_storage.template simd<Backend>();

      // Do some potentially-expensive preparation before we enter the loop
      auto       vertex_cut = unprepared_vertex_cut.prepare( evtCtx );
      std::tuple combination_cuts{std::get<NCombinationCuts>( unprepared_combination_cuts ).prepare( evtCtx )...};

      // Buffer our counters to reduce the number of atomic operations
      auto       vertex_cut_load    = m_vertex_cut_load.buffer();
      auto       npassed_vertex_cut = m_npassed_vertex_cut.buffer();
      auto       npassed_vertex_fit = m_npassed_vertex_fit.buffer();
      std::tuple buffer_shuffles{m_buffer_shuffles[NCombinationCuts].buffer()...};
      std::tuple combination_cut_loads{m_combination_cut_loads[NCombinationCuts].buffer()...};
      std::tuple npassed_combination_cuts{m_npassed_combination_cuts[NCombinationCuts].buffer()...};

      constexpr float inv_simd_size = 1.f / simd_t::size;
      constexpr auto  buffer_depth  = 2 * simd_t::size;
      // These never change -- just the mass and input zip ID copied N times
      auto const current_masses       = LHCb::make_object_array<float_v, N>( m_assumed_mass_value );
      auto const current_zip_families = LHCb::make_object_array<int_v, N>( int{in.zipIdentifier()} );
      // Buffers that we track combinations in. +2 because the 0th combination
      // cut is applied to a 2-track combination and so on.
      std::array<std::size_t, N - 1>                                                 combination_indices_sizes{};
      std::tuple<std::array<std::array<int, buffer_depth>, NCombinationCuts + 2>...> combination_indices{};
      // Lambda that handles N-body combinations once we've got that far
      auto do_vertex_fit = [&]( mask_v const& mask, auto const& indices, auto current_states,
                                auto current_unique_ids ) {
        // In principle these are compile time checks (and gcc lets you use
        // static_assert...but clang doesn't). Given that we have debug builds
        // it doesn't seem worth the effort to portably promote these to
        // static_assert.
        assert( indices.size() == N );
        assert( current_states.size() == N );
        assert( current_unique_ids.size() == N );
        // Run a vertex fit and store the result in tmp_storage
        auto const particle_mask = SelAlgorithms::CombineTracksSIMD::detail::do_vertex_fit<Backend>(
            current_states, current_masses, indices, current_zip_families, current_unique_ids, mask, tmp_storage,
            npassed_vertex_fit );
        std::size_t const particle_count = popcount( particle_mask );
        // Check that some fits succeeded. Because we consolidate before the
        // vertex fit, and the fit should rarely fail, the mask should normally
        // be quite well-populated. The exception might be the last call in each
        // event, where we are tidying up the leftovers.
        if ( !particle_count ) { return; }
        // Last fit_count elements of tmp_storage are valid particles that we can apply the vertex cut to
        auto const& particle_chunk = *tmp_storage_view.begin();
        // Record how well we're SIMDifying the vertex cut
        vertex_cut_load += inv_simd_size * particle_count;
        // Apply the vertex cut to those particles
        auto const vertex_cut_mask = particle_mask && vertex_cut( Functors::mask_arg, particle_mask, particle_chunk );
        npassed_vertex_cut += {std::size_t( popcount( vertex_cut_mask ) ), particle_count};
        // Copy the particles that matched into the actual storage
        storage.template copy_back<simd_t>( tmp_storage, 0, vertex_cut_mask );
        // Clear the temporary storage again
        tmp_storage.clear();
      };
      [[maybe_unused]] int seen_2body_combs{0};
      int const            input_size = in.size();
      for ( auto i = 0; i < input_size; i += simd_t::size ) {
        auto const& t1      = in[i];
        auto const  t1_mask = t1.loop_mask();
        auto const  t1_inds = t1.indices();
        for ( auto j = i + 1; j < input_size; ++j ) {
          auto const& t2 = in.scalar_fill( j );
          assert( all( t2.indices() == int_v{j} ) );
          auto const mask = t1_mask && ( j > t1_inds );
          auto const comb_mask =
              mask && std::get<0>( combination_cuts )( Functors::mask_arg, mask, Combination<2, Backend>{t1, t2} );
          std::size_t const mask_count = popcount( mask );
          std::size_t const comb_count = popcount( comb_mask );
          // Record statistics
          seen_2body_combs += mask_count;
          std::get<0>( combination_cut_loads ) += inv_simd_size * mask_count;
          std::get<0>( npassed_combination_cuts ) += {comb_count, mask_count};
          // Short-circuit if we can
          if ( !comb_count ) { continue; }
          // Buffer the indices of the first and second elements of the combination
          auto& combination12_indices      = std::get<0>( combination_indices );
          auto& combination12_indices_size = std::get<0>( combination_indices_sizes );
          t1_inds.compressstore( comb_mask, &combination12_indices[0][combination12_indices_size] );
          int_v{j}.compressstore( comb_mask, &combination12_indices[1][combination12_indices_size] );
          combination12_indices_size += comb_count;
          // If we don't have a full SIMD unit of 'comb12's, hold off
          if ( combination12_indices_size < simd_t::size ) { continue; }
          // Promote a load of 2-body combinations into 3+-body ones, or if
          // N = 2 then just do the vertex fit
          add_more_tracks<Backend>( in, do_vertex_fit, combination_cuts, buffer_shuffles, combination_cut_loads,
                                    npassed_combination_cuts, combination_indices, combination_indices_sizes,
                                    std::make_index_sequence<2>{} );
        }
      }
      // In optimised builds then seen_2body_combs should be optimised out...
      assert( seen_2body_combs == input_size * ( input_size - 1 ) / 2 );
      // Done with the loop that generates 2-body combinations.
      // Process any leftovers in 'combination_indices'.
      // This expression expands to (N=4 example)
      // - "promote_2_body_to_3_body()"
      // - "promote_3_body_to_4_body()"
      // - "vertex_fit_4_body_combinations()"
      ( add_more_tracks<Backend>( in, do_vertex_fit, combination_cuts, buffer_shuffles, combination_cut_loads,
                                  npassed_combination_cuts, combination_indices, combination_indices_sizes,
                                  std::make_index_sequence<NCombinationCuts + 2>{} ),
        ... );

      // Record if we had to reallocate the output container
      m_output_container_reallocated += bool{storage.capacity() > initial_capacity};

      filter_pass = !storage.empty();
      return ret;
    }

    /** Process a SIMD vector width worth of NBodies-body combinations defined
     *  by indices stored in 'combination_indices'. If NBodies < N this means
     *  creating (NBodies+1)-body combinations. If NBodies == N this means
     *  running the vertex fit and applying the vertex cut.
     *
     *  Only the first two template parameters need to be template parameters,
     *  everything else **could** be written out explicitly...
     */
    template <SIMDWrapper::InstructionSet Backend, std::size_t... Particle, typename CombinationCuts,
              typename IterableInput, typename VertexFit, typename CombinationCutCounters,
              typename CombinationCutLoadCounters, typename CombinationIndices, typename BufferShuffleCounters>
    void add_more_tracks( IterableInput const& in, VertexFit const& do_vertex_fit,
                          CombinationCuts const& combination_cuts, BufferShuffleCounters& buffer_shuffles,
                          CombinationCutLoadCounters& combination_cut_loads,
                          CombinationCutCounters& npassed_combination_cuts, CombinationIndices& combination_indices,
                          std::array<std::size_t, N - 1>& combination_indices_sizes,
                          std::index_sequence<Particle...> ) const {
      // Sanity checks...
      constexpr auto NBodies = sizeof...( Particle );
      static_assert( NBodies >= 2 );
      static_assert( NBodies <= N );
      static_assert( std::tuple_size_v<CombinationCuts> == N - 1 );
      static_assert( std::tuple_size_v<CombinationIndices> == N - 1 );
      static_assert( std::tuple_size_v<BufferShuffleCounters> == N - 1 );
      static_assert( std::tuple_size_v<CombinationCutCounters> == N - 1 );
      static_assert( std::tuple_size_v<CombinationCutLoadCounters> == N - 1 );
      // Useful aliases
      using simd_t  = typename SIMDWrapper::type_map_t<Backend>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;
      // Work out the index into the various containers and collections of
      // combination cuts/counters. The offset of 2 comes because the first
      // invocation of this function is on 2-body combinations.
      constexpr auto CombCutIndex = NBodies - 2;
      // Helper constant
      constexpr float inv_simd_size = 1.f / simd_t::size;
      // e.g. if CombCutIndex = 0 it means std::get<0>( combination_indices )
      // contains combination_indices_sizes[0] 2-body combinations and this
      // function should promote these to be 3-body combinations buffered in
      // std::get<1>( combination_indices ) and so on (if N > 2), or run the
      // vertex fit (if N = 2).
      auto& current_size    = std::get<CombCutIndex>( combination_indices_sizes );
      auto& current_indices = std::get<CombCutIndex>( combination_indices );
      // For convenience, so we can just call this in a fold expression and
      // tidy up leftovers at the end of the 2-body combination generation loop
      if ( !current_size ) { return; }
      // When tidying up after the 2-body generation loop this != mask_true()
      auto const comb_mask = simd_t::loop_mask( 0, current_size );
      // Get the indices of members of the combination we're operating on
      std::array indices{int_v{current_indices[Particle].data()}...};
      // Make sure we don't load anything invalid -- replace invalid indices
      // with the minimum valid index. current_size is nonzero, so there is
      // always at least one valid index here.
      ( ( std::get<Particle>( indices ) =
              select( comb_mask, std::get<Particle>( indices ), std::get<Particle>( indices ).hmin( comb_mask ) ) ),
        ... );
      // Remove the indices that we just loaded from 'current_indices'
      if ( current_size > simd_t::size ) {
        // Scalar loop shuffling O(1) index we didn't consume to the front of
        // current_indices
        for ( auto i = simd_t::size; i < current_size; ++i ) {
          ( ( current_indices[Particle][i - simd_t::size] = current_indices[Particle][i] ), ... );
        }
        current_size -= simd_t::size;
        if constexpr ( Backend != SIMDWrapper::Scalar ) { std::get<CombCutIndex>( buffer_shuffles ) += current_size; }
      } else {
        current_size = 0;
      }
      // Branch off depending on whether we're adding a track or doing a vertex fit
      if constexpr ( NBodies == N ) {
        // Do a vertex fit...
        static_assert( indices.size() == N );
        do_vertex_fit(
            comb_mask, indices,
            std::array<detail::MiniState<float_v>, N>{
                in.gather( indices[Particle], comb_mask ).closestToBeamState()...},
            std::vector<LHCb::UniqueIDGenerator::ID<int_v>>{in.gather( indices[Particle], comb_mask ).unique_id()...} );
      } else {
        // Our task is to make (NBodies+1)-body combinations from NBodies-body
        // combinations.
        //
        // We only generate combination where the Nth index > (N-1)th index
        // etc. (single input container), so work out where in the container we
        // have to start looping.
        auto const min_last_index = indices.back().hmin( comb_mask );
        // Get the type of an (NBodies+1)-body combination
        using new_comb_t = Combination<NBodies + 1, Backend>;
        // Loop over the new additions to the combinations
        int const input_size = in.size();
        for ( auto i = min_last_index + 1; i < input_size; ++i ) {
          // Make sure the indices satisfy Nth > ... > 2nd > 1st
          // If we're making an X+1-body combination from an X-body one,
          // indices.back() are the indices of the last member of the X-body
          // one
          auto const  mask     = comb_mask && ( i > indices.back() );
          auto const& comb_cut = std::get<CombCutIndex + 1>( combination_cuts );
          auto const  cut_mask =
              mask && comb_cut( Functors::mask_arg, mask,
                                new_comb_t{in.gather( indices[Particle], mask )..., in.scalar_fill( i )} );
          std::size_t const mask_count = popcount( mask );
          std::size_t const cut_count  = popcount( cut_mask );
          std::get<CombCutIndex + 1>( combination_cut_loads ) += inv_simd_size * mask_count;
          std::get<CombCutIndex + 1>( npassed_combination_cuts ) += {cut_count, mask_count};
          if ( !cut_count ) { continue; }
          // Store indices of the new X+1-body combination
          auto& next_size    = std::get<CombCutIndex + 1>( combination_indices_sizes );
          auto& next_indices = std::get<CombCutIndex + 1>( combination_indices );
          // Store the indices of the surviving X-body combination members
          ( indices[Particle].compressstore( cut_mask, &next_indices[Particle][next_size] ), ... );
          // Store the index of the final element that we just added.
          int_v{i}.compressstore( cut_mask, &next_indices.back()[next_size] );
          next_size += cut_count;
          // If our collection of (NBodies+1)-body combinations has at least a
          // whole SIMD vector worth of elements, process a chunk now.
          if ( next_size < simd_t::size ) { continue; }
          add_more_tracks<Backend>( in, do_vertex_fit, combination_cuts, buffer_shuffles, combination_cut_loads,
                                    npassed_combination_cuts, combination_indices, combination_indices_sizes,
                                    std::make_index_sequence<NBodies + 1>{} );
        }
      }
    }

    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeCombinationCutCounters( CombineTracksSIMD* ptr,
                                                                      std::index_sequence<Ms...> ) {
      return {C{ptr, "# passed " + SelAlgorithms::CombineTracksSIMD::detail::combinationCutName<N, Ms>()}...};
    }
    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeCombinationCutLoadCounters( CombineTracksSIMD* ptr,
                                                                          std::index_sequence<Ms...> ) {
      return {C{ptr, SelAlgorithms::CombineTracksSIMD::detail::combinationCutName<N, Ms>() + " SIMD utilisation"}...};
    }
    template <typename C, std::size_t... Ms>
    static std::array<C, sizeof...( Ms )> makeBufferShuffleCounters( CombineTracksSIMD* ptr,
                                                                     std::index_sequence<Ms...> ) {
      return {C{ptr, "# shuffled in " + std::to_string( Ms + 2 ) + "-particle buffer"}...};
    }
    /// Number of combinations passing the combination cut
    using cut_counter_t = Gaudi::Accumulators::BinomialCounter<>;
    mutable std::array<cut_counter_t, N - 1> m_npassed_combination_cuts{
        makeCombinationCutCounters<cut_counter_t>( this, std::make_index_sequence<N - 1>{} )};
    /// SIMD utilisation of the combination cuts
    using load_counter_t = Gaudi::Accumulators::AveragingCounter<>;
    mutable std::array<load_counter_t, N - 1> m_combination_cut_loads{
        makeCombinationCutLoadCounters<load_counter_t>( this, std::make_index_sequence<N - 1>{} )};
    /// Number of indices shuffled
    mutable std::array<load_counter_t, N - 1> m_buffer_shuffles{
        makeBufferShuffleCounters<load_counter_t>( this, std::make_index_sequence<N - 1>{} )};
    /// Number of successfully fitted combinations
    mutable cut_counter_t m_npassed_vertex_fit{this, "# passed vertex fit"};
    /// Number of fitted vertices passing vertex cut
    mutable cut_counter_t m_npassed_vertex_cut{this, "# passed VertexCut"};
    /// How often we had to reallocate
    mutable cut_counter_t m_output_container_reallocated{this, "# output reallocations"};
    /// SIMD utilisation of the vertex cuts
    mutable load_counter_t m_vertex_cut_load{this, "VertexCut SIMD utilisation"};
    /// Service used to convert particle names into floating point mass values
    ServiceHandle<LHCb::IParticlePropertySvc> m_pp_svc{this, "ParticlePropertyService", "LHCb::ParticlePropertySvc"};
    /// Mass hypothesis applied to children when calculating parent 4-momenta
    float                        m_assumed_mass_value{};
    Gaudi::Property<std::string> m_assumed_mass{
        this, "AssumedMass", "K+",
        [this]( auto& ) {
          auto const exception = [this]( auto msg ) -> GaudiException {
            // Print the error as well as throwing; hopefully this will make
            // the message more obvious in the logfile.
            this->fatal() << msg << endmsg;
            return {std::move( msg ), "SelAlgorithms::CombineTracksSIMD", StatusCode::FAILURE};
          };
          if ( !m_pp_svc ) { throw exception( "Couldn't get ServiceHandle<LHCb::IParticlePropertySvc>" ); }
          if ( !m_pp_svc->size() ) {
            throw exception( "Particle property service didn't know about any particles, perhaps you didn't set the "
                             "database tags?" );
          }
          if ( auto const* pp = m_pp_svc->find( m_assumed_mass ) ) {
            // Successfully looked up a string name (e.g. K+)
            m_assumed_mass_value = pp->mass();
          } else {
            // Interpreting m_assumed_mass as a particle name failed, try
            // interpreting it as a floating point value
            try {
              m_assumed_mass_value = std::stof( m_assumed_mass );
            } catch ( std::exception const& e ) {
              throw exception( "Couldn't convert AssumedMass value '" + m_assumed_mass.value() +
                               "' to float: " + e.what() );
            }
          }
          if ( this->msgLevel( MSG::DEBUG ) ) {
            this->debug() << "Converted " << m_assumed_mass.value() << " to " << m_assumed_mass_value << endmsg;
          }
          if ( m_assumed_mass_value < 0.f ) {
            throw exception( "Got invalid mass value: " + std::to_string( m_assumed_mass_value ) );
          }
        },
        "Mass hypothesis assumed for the tracks when calculating the parent 4-momentum. This will be interpreted as "
        "a particle name, and if that lookup fails it will be interpreted as a floating point value. If that also "
        "fails, an exception will be thrown."};
  };
} // namespace SelAlgorithms::CombineTracksSIMD
