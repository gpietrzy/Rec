/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/Particle.h"
#include "LHCbAlgs/Transformer.h"
#include "LoKi/PhysExtract.h"

using counter_t       = Gaudi::Accumulators::SummingCounter<unsigned int>;
using out_particles_t = LHCb::Particle::Selection;
using in_particles_t  = const LHCb::Particle::Range&;

namespace LHCb {
  /**
   * Trivial algorithm to extract all particles from a container,
   * this includes basics, but also the composites.
   *
   * @author Laurent Dufour
   */
  class FlattenDecayTree final : public Algorithm::Transformer<out_particles_t( in_particles_t )> {
  public:
    FlattenDecayTree( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, {"InputParticles", {}}, {KeyValue{"OutputParticles", ""}} ) {}

    out_particles_t operator()( in_particles_t particles ) const override {
      LHCb::Particle::Selection out_particles;
      m_ninput_particles += particles.size();

      for ( const auto& rawParticle : particles ) {
        LoKi::Extract::particles( rawParticle, std::back_inserter( out_particles ) );
      }

      m_noutput_particles += out_particles.size();

      return out_particles;
    }

  private:
    mutable counter_t m_ninput_particles{this, "# input particles"};
    mutable counter_t m_noutput_particles{this, "# output particles"};
  };

  DECLARE_COMPONENT_WITH_ID( FlattenDecayTree, "FlattenDecayTree" )
} // namespace LHCb
