/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/GhostProbability.h"
#include "Event/MuonPIDs_v2.h"
#include "Event/Particle_v2.h"
#include "Event/Track_v3.h"
#include "LHCbAlgs/LHCbAlgsHelpers.h"
#include "LHCbAlgs/Transformer.h"
#include "SelKernel/VertexRelation.h"

namespace {
  struct deduce_return_type_t {};

  template <typename OutputType, typename... Inputs>
  struct Helper {
    static constexpr bool deduce_return_type_v = std::is_same_v<OutputType, deduce_return_type_t>;
    using Output                  = std::conditional_t<deduce_return_type_v, LHCb::Event::zip_t<Inputs...>, OutputType>;
    using Base                    = typename LHCb::Algorithm::Transformer<Output( Inputs const&... )>;
    static const std::size_t size = sizeof...( Inputs );
    static auto              InputNames() {
      return LHCb::Algorithm::IndexingNamesHelper<Base>( "Input", std::make_index_sequence<size>{} );
    }
  };

} // namespace

/** @class MakeZip MakeZip.cpp
 *  @brief Puts an iterable zip of its input containers onto the TES
 *
 *  This algorithm is a very thin wrapper around LHCb::Event::make_zip( in... ),
 *  given a set of N inputs (property name Input{1..N}) it stores the return
 *  value of LHCb::Event::make_zip( in... ) on the TES (property name Output).
 *
 *  @tparam  OutputType Output type to force. If this is set to deduce_return_type_t then the type will be deduced from
 *                      the input types.
 *  @tparam  Inputs...  Types of the input containers taken from the TES
 *  @returns            Iterable, non-owning view type referring to the given
 *                      input containers.
 */
template <typename OutputType, typename... Inputs>
struct MakeZip final : public Helper<OutputType, Inputs...>::Base {
  using Help     = Helper<OutputType, Inputs...>;
  using KeyValue = typename Help::Base::KeyValue;
  MakeZip( const std::string& name, ISvcLocator* pSvcLocator )
      : Help::Base{name, pSvcLocator, Help::InputNames(), KeyValue{"Output", ""}} {}
  typename Help::Output operator()( Inputs const&... in ) const override { return LHCb::Event::make_zip( in... ); }
};

// using MakeZip__ChargedBasics_as_Particles = MakeZip<LHCb::Event::Particles, LHCb::Event::ChargedBasics>;
// DECLARE_COMPONENT_WITH_ID( MakeZip__ChargedBasics_as_Particles, "MakeZip__ChargedBasics_as_Particles" )

using MakeZip__PrFittedForwardTracks__BestVertexRelations =
    MakeZip<deduce_return_type_t, LHCb::Event::v3::Tracks, BestVertexRelations>;
DECLARE_COMPONENT_WITH_ID( MakeZip__PrFittedForwardTracks__BestVertexRelations,
                           "MakeZip__PrFittedForwardTracks__BestVertexRelations" )

using MakeZip__BestTracks__MuonPIDs__v2 =
    MakeZip<deduce_return_type_t, LHCb::Event::v3::Tracks, LHCb::Event::v2::Muon::PIDs>;
DECLARE_COMPONENT_WITH_ID( MakeZip__BestTracks__MuonPIDs__v2, "MakeZip__BestTracks__MuonPIDs__v2" )
