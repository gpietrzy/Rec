/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/Environment.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/ServiceHandle.h"
#include "Kernel/STLExtensions.h"
#include "SelTools/MVAUtils.h"
#include <Gaudi/Algorithm.h>
#include <any>
#include <cstring>
#include <map>
#include <string>
#include <vector>

namespace Sel {
  namespace detail {
// it works - don't touch it!
#define array( type, name, fml )                                                                                       \
  unsigned int name##_count = *(unsigned int const*)fml;                                                               \
  fml += sizeof( unsigned int );                                                                                       \
  type const* name = (type const*)fml;                                                                                 \
  fml              = (char const*)( name + name##_count )

#define array_check( type, name, fml, limit, info )                                                                    \
  if ( fml + sizeof( unsigned int ) > limit ) {                                                                        \
    info << "formula truncated" << std::endl;                                                                          \
    return false;                                                                                                      \
  }                                                                                                                    \
  unsigned int name##_count = *(unsigned int const*)fml;                                                               \
  fml += sizeof( unsigned int );                                                                                       \
  if ( fml + name##_count * sizeof( type ) > limit ) {                                                                 \
    info << "formula truncated" << std::endl;                                                                          \
    return false;                                                                                                      \
  }                                                                                                                    \
  type const* name = (type const*)fml;                                                                                 \
  fml              = (char const*)( name + name##_count )

    inline unsigned int yabs_mx_check( char const* fml, size_t len, std::ostream& info ) {
      char const* limit = fml + len;
      array_check( unsigned int, bf_counts, fml, limit, info );
      array_check( float, bfs, fml, limit, info );

      unsigned int bfs_check = 0;
      while ( bf_counts_count-- ) bfs_check += *( bf_counts++ );
      if ( bfs_check != bfs_count ) {
        info << "formula incorrect #1 " << std::endl;
        return 0;
      }

      array_check( unsigned int, classes, fml, limit, info );

      if ( classes_count != 0 ) {
        info << "multiclassification not supported" << std::endl;
        return false;
      }

      array_check( unsigned int, nf_counts, fml, limit, info );
      array_check( unsigned int, ids, fml, limit, info );
      array_check( int, tbl, fml, limit, info );

      unsigned int ids_check = 0;
      unsigned int tbl_check = 0;

      unsigned int i;
      for ( i = 0; i < nf_counts_count; ++i ) {
        unsigned int nf_count = *( nf_counts++ );
        ids_check += nf_count * ( i + 1 );
        tbl_check += nf_count * ( 1U << ( i + 1 ) );
      }

      if ( ids_check != ids_count ) {
        info << "formula incorrect #2" << std::endl;
        return false;
      }

      if ( tbl_check != tbl_count ) {
        info << "formula incorrect #3" << std::endl;
        return false;
      }

      while ( ids_count-- )
        if ( *( ids++ ) >= bfs_count ) {
          info << "formula incorrect #4" << std::endl;
          return false;
        }

      if ( limit < fml + 2 * sizeof( double ) ) {
        info << "formula truncated" << std::endl;
        return false;
      }

      if ( limit > fml + 2 * sizeof( double ) ) {
        info << "junk at the end of formula" << std::endl;
        return false;
      }

      return true;
    }

#undef array_check

    template <typename TYPE>
    TYPE to( const char* c ) {
      TYPE t{};
      std::memcpy( &t, c, sizeof( TYPE ) );
      return t;
    }

    inline double yabs_mx_apply( char const* fml, float const* fp ) {
      array( unsigned int, bf_counts, fml );
      array( float, bfs, fml );

      unsigned int* vars = (unsigned int*)alloca( bfs_count * sizeof( unsigned int ) );
      unsigned int* vp   = vars;

      while ( bf_counts_count-- ) {
        unsigned int bf_count = *( bf_counts++ );
        float        f        = *( fp++ );

        while ( bf_count-- ) *( vp++ ) = f > *( bfs++ ) ? 1 : 0;
      }

      fml += sizeof( unsigned int ); /* classes_count == 0 */

      array( unsigned int, nf_counts, fml );
      array( unsigned int, ids, fml );
      array( int, tbl, fml );

      int64_t res = 0;

      unsigned int i;
      for ( i = 0; i < nf_counts_count; ++i ) {
        unsigned int nf_count = *( nf_counts++ );
        while ( nf_count-- ) {
          unsigned int idx = 0;

          unsigned int ii;
          for ( ii = 0; ii <= i; ++ii ) idx += ( vars[*( ids++ )] << ii );

          res += tbl[idx];
          tbl += ( 1U << ( i + 1 ) );
        }
      }

      const double bias = to<double>( fml );
      fml += sizeof( double );
      const double delta_mult = to<double>( fml );

      const double mn_out = bias + res / delta_mult;
      // OL surely 1.f / ( 1.f + exp( -mn_out ) ), or with expf, would be fine?
      return exp( mn_out ) / ( 1. + exp( mn_out ) );
    }
#undef array
  } // namespace detail

  /** @class MatrixNet
   *
   *  Heavily based on the Run 2 MatrixnetTransform.
   */
  struct MatrixNet {
    /** Specifies what type the input variables will be passed in as. */
    using input_type = float;

    MatrixNet( MVA_config_dict config ) : m_config( std::move( config ) ) {}

    GaudiException exception( std::string s ) const {
      return GaudiException{std::move( s ), "Sel::MatrixNet", StatusCode::FAILURE};
    }

    void bind( Gaudi::Algorithm* alg ) {
      ServiceHandle<IFileAccess> filesvc{"ParamFileSvc", "MatrixNet"};
      filesvc.retrieve().ignore();
      // Get the path to the weights file
      auto matrixnet_file = std::any_cast<std::string>( m_config.at( "MatrixnetFile" ) );
      // Read the weights file
      auto temp = filesvc->read( matrixnet_file );
      if ( !temp ) { throw exception( "Couldn't open file: " + matrixnet_file ); }
      const char* data           = temp->c_str();
      const int   factors_number = detail::to<int>( data );
      data += sizeof( const int );
      for ( int i = 0; i < factors_number; ++i ) {
        const int factor_length = detail::to<int>( data );
        data += sizeof( const int );
        m_variables.emplace_back( data, data + factor_length );
        data += factor_length;
      }
      const int formula_length = detail::to<int>( data );
      data += sizeof( const int );
      m_formula = std::string( data, data + formula_length );

      if ( !detail::yabs_mx_check( m_formula.c_str(), m_formula.length(), alg ? alg->info().stream() : std::cout ) ) {
        throw exception( "MatrixNet formula check failed on file: " + matrixnet_file );
      }
    }

    /** Return the (ordered) list of expected inputs. */
    std::vector<std::string> const& variables() const { return m_variables; }

    /** Evaluate the classifier. The caller is responsible for giving us the
     *  right values in the right order.
     */
    auto operator()( LHCb::span<input_type const> values ) const {
      if ( m_formula.empty() ) { throw exception( "Tried to evaluate MatrixNet classifier with empty formula!" ); }
      return detail::yabs_mx_apply( m_formula.c_str(), values.data() );
    }

  private:
    MVA_config_dict          m_config;
    std::string              m_formula;
    std::vector<std::string> m_variables; // Input variables needed by the classifier
  };
} // namespace Sel
