/*****************************************************************************\
* (c) Copyright 2019-20 CERN for the benefit of the LHCb Collaboration        *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "GaudiKernel/GaudiException.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/SynchronizedValue.h"
#include "SelTools/MVAUtils.h"
#include <Gaudi/Algorithm.h>
#include <GaudiKernel/IFileAccess.h>

#include "TMVA/MethodBase.h"
#include "TMVA/Reader.h"
#include "TXMLDocument.h"
#include "TXMLEngine.h"
#include "TXMLNode.h"

#include <string>
#include <vector>

// Fix conflicting naming of Sel::TMVA and TMVA namespace
namespace TMVAlib = TMVA;
namespace Sel {
  namespace TMVA_details {
    template <typename To, typename From>
    std::unique_ptr<To> dynamic_unique_cast( std::unique_ptr<From> p ) {
      if ( To* cast = dynamic_cast<To*>( p.get() ); cast ) {
        std::unique_ptr<To> result( cast );
        p.release();
        return result;
      }
      return std::unique_ptr<To>( nullptr );
    }
  } // namespace TMVA_details
  /** @class Sel::TMVA
   * @brief Implementation of a TMVA algorithm to be used trough the MVA ThOr functor. Heavily based on the Run 2
   * TMVATransform.
   * @author Alejandro Alfonso Albero
   * @details This implementation of TMVA is thread-safe and can be called from the MVA ThOr functor. The configuration
   * is passed down to the class through the config keyword as a dictionary, which must specify:
   *  - Name: Name of the TMVA method that must be used. It has to match (one of) the methods provided in the xml files.
   *        The method name is specified by the user when training and is available in the XML file.
   *  - XMLFile: Path to the XML file containing the TMVA method.
   *  - TMVAOptions (optional): Options to be passed to the TMVA::Reader. Deafult is "Silent".
   * The following shows an example on how to use TMVA with a XML file containing a "BDT" method (which coincidentally
   * enough is a BDT), and keeping events with a response above 0.
   * @code{.py}
   * F.MVA(
            MVAType='TMVA',
            Config={
                'XMLFile': 'paramfile://data/Hlt2_Radiative_HHgamma.xml',
                'Name': 'BDT',
            },
            Inputs={
                "ipchi2": log(F.BPVIPCHI2(pvs)),
                'ipchi2_min': log(F.BPVIPCHI2(pvs)),
                'gamma_pt': F.PT,
                'm_corrected': F.MASS,
                'vm_corrected': F.MASS,
                'fdchi2': log(F.BPVFDCHI2(pvs)),
                'vtx_chi2': log(F.CHI2),
                'doca': F.BPVFDCHI2(pvs)
            }) > 0.)
   * @endcode
   *
   */
  class TMVA {
  public:
    using input_type = double;

  private:
    MVA_config_dict                           m_config;
    std::vector<std::string>                  m_variables; // Input variables needed by the classifier
    std::vector<std::string>                  m_spectator; // Spectator variables
    std::unique_ptr<int>                      m_spectator_var;
    std::string                               m_name; // Name of the method to book
    std::string                               m_weightfile;
    std::string                               m_reader_opts{"Silent"};
    bool                                      m_verbose{false};
    Gaudi::Algorithm const*                   m_parent = nullptr;
    std::optional<ServiceHandle<IFileAccess>> m_filesvc;

    // TMVA bits
    std::unique_ptr<TMVAlib::Reader>                                          m_reader;
    mutable LHCb::cxx::SynchronizedValue<std::unique_ptr<::TMVA::MethodBase>> m_mva{nullptr};

    auto evaluateMVA( std::vector<float> const& vals ) const {
      // given that the MVA must know how many inputs it expects, and `vals` also knows how many
      // items it contains, why must one add an explicit `GetNVariables()` to `Event`? And why
      // must one pass `event` instead of just vals?
      // and why does `GetMvaValue` assign the address of `event` to fTmpEvent, call the internal
      // GetMvaValue, and then reset fTmpEvent instead of just passing the pointer explicitly?
      // As a result of this use of fTmpEvent, this method is not thread-safe, and thus needs
      // a lock...
      return m_mva.with_lock( [event = ::TMVA::Event{vals, m_reader->DataInfo().GetNVariables()}](
                                  std::unique_ptr<::TMVA::MethodBase>& mb ) { return mb->GetMvaValue( &event, 0 ); } );
    }

  public:
    TMVA( MVA_config_dict config ) : m_config( std::move( config ) ) {}
    auto operator()( LHCb::span<input_type const> values ) const;

    /** Return the list of expected inputs. */
    std::vector<std::string> const& variables() const { return m_variables; }

    void bind( Gaudi::Algorithm* alg );
    // Gaudi exception handler
    GaudiException exception( std::string s ) const {
      return GaudiException{std::move( s ), "Sel::TMVA", StatusCode::FAILURE};
    }

  private:
    // Helper functions
    void readConfig();
    void readWeightsFile();
  };

  /** Evaluate the classifier. The caller is responsible for giving us the
   *  right values in the right order.
   */
  inline auto TMVA::operator()( LHCb::span<input_type const> values ) const {
    // Check is performed by mama MVA functor
    // We need to convert input into a vector
    std::vector<float> v_values( values.begin(), values.end() );

    // Verbose output
    assert( m_parent != nullptr );
    if ( m_parent->msgLevel( MSG::VERBOSE ) ) {
      m_parent->verbose() << m_name << " inputs"
                          << "\n------------------\n";
      for ( unsigned int i = 0; i < v_values.size(); i++ ) {
        m_parent->verbose() << m_variables[i] << ": " << v_values[i] << "\n";
      }
      m_parent->verbose() << endmsg;
      m_parent->verbose() << m_name << " output: " << evaluateMVA( v_values );
      m_parent->verbose() << "\n\n" << endmsg;
    }

    // Evaluate MVA
    return evaluateMVA( v_values );
  }

  inline void TMVA::bind( Gaudi::Algorithm* alg ) {
    m_parent = alg;
    m_filesvc.emplace( "ParamFileSvc", alg->name() );
    m_filesvc->retrieve().ignore();
    readConfig();      // Retrieve the configuration
    readWeightsFile(); // Read the XMLFile with the weights and Load the TMVA Reader with the variables and spectators
    if ( alg->msgLevel( MSG::VERBOSE ) ) {
      m_verbose = true;
      alg->verbose() << "Variables found in " << m_weightfile << ": ";
      alg->verbose() << m_variables;
      alg->verbose() << endmsg;
    }
  }

  // Retrieve the configuration
  inline void TMVA::readConfig() {
    // Name and XMLFile MUST be provided
    if ( m_config.find( "Name" ) == m_config.end() ) throw exception( "'Name' not provided" );
    if ( m_config.find( "XMLFile" ) == m_config.end() ) throw exception( "'XMLFile' not provided" );
    m_name        = std::any_cast<std::string>( m_config.at( "Name" ) );
    m_weightfile  = std::any_cast<std::string>( m_config.at( "XMLFile" ) );
    m_reader_opts = ( m_config.find( "TMVAOptions" ) == m_config.end() )
                        ? m_reader_opts
                        : std::any_cast<std::string>( m_config.at( "TMVAOptions" ) );
  }

  inline void TMVA::readWeightsFile() {
    // Check that the weightfile exists
    auto buffer = m_filesvc.value()->read( m_weightfile );
    if ( !buffer ) throw exception( "Couldn't open file: " + m_weightfile + " using ParamFileSvc" );
    TXMLEngine xmlparser;
    auto       doc            = xmlparser.ParseString( buffer->c_str() );
    auto       root           = xmlparser.DocGetRootElement( doc );
    TString    fullMethodName = xmlparser.GetAttr( root, "Method" );
    TString    methodType     = fullMethodName( 0, fullMethodName.Index( "::" ) );
    auto       child          = xmlparser.GetChild( root );
    // Initialization of variables
    XMLNodePointer_t gchild;   // grandchild (grandnode)
    std::string      nodename; // Node name of current child (node)
    std::string      exp;      // expression assigned to current gchild
    unsigned         nvar;
    // Loop over all children (nodes)
    while ( child ) {
      nodename = xmlparser.GetNodeName( child );
      if ( nodename == "Variables" ) { // Get training variables
        nvar   = atoi( xmlparser.GetAttr( child, "NVar" ) );
        gchild = xmlparser.GetChild( child );
        for ( unsigned i = 0; i < nvar; ++i ) {
          exp = xmlparser.GetAttr( gchild, "Expression" );
          m_variables.push_back( exp );
          gchild = xmlparser.GetNext( gchild );
        }
      } else if ( nodename == "Spectators" ) { // Get spectator variables
        nvar   = atoi( xmlparser.GetAttr( child, "NSpec" ) );
        gchild = xmlparser.GetChild( child );
        for ( unsigned i = 0; i < nvar; ++i ) {
          exp = xmlparser.GetAttr( gchild, "Expression" );
          m_spectator.push_back( exp );
          gchild = xmlparser.GetNext( gchild );
        }
      } // end if statement(s)
      child = xmlparser.GetNext( child );
    } // end loop over child nodes
    // cleanup
    xmlparser.FreeDoc( doc );

    // Initialize new TMVA::Reader with training variables and reader options
    m_reader = std::make_unique<TMVAlib::Reader>( m_variables, m_reader_opts.c_str() );
    // Tell the reader about Spectator variables, but we can just set their values to 1
    // ... and since they all have the same value, we can just create one instance and
    //     re-use it again and again
    //   - TMVA will never know.
    // (and why does `AddSpectator` want a pointer to a non-const spectator???)
    m_spectator_var = std::make_unique<int>( 1 );
    for ( const auto& svar : m_spectator ) { m_reader->AddSpectator( svar.c_str(), m_spectator_var.get() ); }
    // why do we have to tell `BookMVA` the method type, which is explicitly encoded in the XML content
    // which is _also_ passed? The only added functionality is the ability to pass something inconsistent,
    // and the requirement to parse the XML twice in order to be consistent...
    auto mva = std::unique_ptr<::TMVA::IMethod>{
        m_reader->BookMVA( ::TMVA::Types::Instance().GetMethodType( methodType ), buffer->c_str() )};
    if ( !mva ) throw exception( m_name + " method couldn't be booked" );
    auto mb = TMVA_details::dynamic_unique_cast<::TMVA::MethodBase>( std::move( mva ) );
    if ( !mb ) throw exception( m_name + " method is not a MethodBase" );
    m_mva.with_lock( []( std::unique_ptr<::TMVA::MethodBase>& ptr, auto src ) { ptr = std::move( src ); },
                     std::move( mb ) );
  }

} // namespace Sel
