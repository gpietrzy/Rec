/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "Event/HltDecReports.h"
#include "Event/HltSelReports.h"

// local
#include "Hlt1TriggerTisTos.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Hlt1TriggerTisTos
//
// 2010-06-23 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( Hlt1TriggerTisTos )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Hlt1TriggerTisTos::Hlt1TriggerTisTos( const std::string& type, const std::string& name, const IInterface* parent )
    : TriggerTisTos( type, name, parent ) {
  declareInterface<ITriggerTisTos>( this );
  setProperty( "HltDecReportsLocation", "Hlt1/DecReports" ).ignore();
  setProperty( "HltSelReportsLocation", "Hlt1/SelReports" ).ignore();
}

//=============================================================================
// Destructor
//=============================================================================
Hlt1TriggerTisTos::~Hlt1TriggerTisTos() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode Hlt1TriggerTisTos::initialize() {
  const StatusCode sc = TriggerTisTos::initialize();
  if ( sc.isFailure() ) return sc;

  return sc;
}
