/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

#include "Event/HltDecReports.h"
#include "Event/HltSelReports.h"

// local
#include "Hlt2TriggerTisTos.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Hlt2TriggerTisTos
//
// 2010-06-23 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

// Declaration of the Tool Factory
DECLARE_COMPONENT( Hlt2TriggerTisTos )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
Hlt2TriggerTisTos::Hlt2TriggerTisTos( const std::string& type, const std::string& name, const IInterface* parent )
    : TriggerTisTos( type, name, parent ) {
  declareInterface<ITriggerTisTos>( this );
  setProperty( "HltDecReportsLocation", "Hlt2/DecReports" ).ignore();
  setProperty( "HltSelReportsLocation", "Hlt2/SelReports" ).ignore();
}

//=============================================================================
// Destructor
//=============================================================================
Hlt2TriggerTisTos::~Hlt2TriggerTisTos() {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode Hlt2TriggerTisTos::initialize() {
  const StatusCode sc = TriggerTisTos::initialize();
  if ( sc.isFailure() ) return sc;

  return sc;
}
