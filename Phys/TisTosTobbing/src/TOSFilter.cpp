/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <mutex>
#include <regex>

#include "LHCbAlgs/Transformer.h"

#include "Event/HltObjectSummary.h"
#include "Event/HltSelReports.h"
#include "Event/RecVertex.h"
#include "Event/Track_v1.h"
#include "Kernel/IParticleTisTos.h"

#include <string_view>

namespace {
  /** @brief Helper class that allows us to define an std::regex object directly as a Gaudi::Property.
   */
  class RegEx {
    std::string m_str;
    std::regex  m_regex;

  public:
    RegEx( std::string_view s ) : m_str{s}, m_regex{m_str} {}

    template <typename Arg>
    decltype( auto ) match( Arg&& arg ) const {
      return std::regex_match( std::forward<Arg>( arg ), m_regex );
    }

    friend StatusCode parse( RegEx& r, std::string_view s ) {
      try {
        if ( ( s.front() == '"' || s.front() == '\'' ) && ( s.front() == s.back() ) ) {
          s.remove_prefix( 1 );
          s.remove_suffix( 1 );
        }
        r = RegEx{s};
        return StatusCode::SUCCESS;
      } catch ( const std::regex_error& ) { return StatusCode::FAILURE; }
    }
    friend std::string   toString( const RegEx& r ) { return r.m_str; }
    friend std::ostream& toStream( const RegEx& r, std::ostream& os ) {
      return os << std::quoted( toString( r ), '\'' );
    }
  };

  /// Return LHCbIDs from a track-like object
  template <typename TrackLike>
  decltype( auto ) lhcb_ids( const TrackLike& obj ) {
    return obj.lhcbIDs();
  }

  /// Return the union of all LHCbIDs referenced by tracks in a vertex
  std::vector<LHCb::LHCbID> lhcb_ids( const LHCb::RecVertex& obj ) {
    const auto& tracks = obj.tracks();
    // Add IDs to a set for de-duplication
    std::set<LHCb::LHCbID> ids_set;
    for ( const auto& track : tracks ) {
      const auto& track_ids = track->lhcbIDs();
      ids_set.insert( track_ids.begin(), track_ids.end() );
    }
    // Convert to a vector to match the return type
    std::vector<LHCb::LHCbID> ids{ids_set.begin(), ids_set.end()};
    return ids;
  }
} // namespace

/** @brief Filter objects by their TOS decision against some set of HLT decisions.
 *
 * An object container is filtered object-by-object based on whether each
 * object is TOS with respect to a set of trigger decisions. The set is based
 * on a regular expression which matches the decisions found in the SelReports
 * container.
 *
 * @tparam Container Object container to filter the contents of.
 *
 * Example:
 *
 * Filter all objects from a TES location that are TOS against trigger objects
 * created by lines with the pattern `Hlt1.*MVA.*`.
 *
 * @code
 * from Configurables import TOSFilter__v1__Track
 * TOSFilter__v1__Track(
 *     InputContainer="/Event/Tracks",
 *     SelReports="/Event/Hlt1/SelReports",
 *     DecisionPattern="Hlt1.*MVA.*",
 * )
 * @endcode
 */
template <typename Container>
class TOSFilter : public LHCb::Algorithm::MultiTransformerFilter<std::tuple<Container>( const Container&,
                                                                                        const LHCb::HltSelReports& )> {
public:
  using base_class =
      LHCb::Algorithm::MultiTransformerFilter<std::tuple<Container>( const Container&, const LHCb::HltSelReports& )>;
  using KeyValue = typename base_class::KeyValue;

  TOSFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : base_class( name, pSvcLocator, {KeyValue{"InputContainer", ""}, KeyValue{"SelReports", ""}},
                    {KeyValue{"OutputContainer", ""}} ) {}

  std::tuple<bool, Container> operator()( const Container&           input,
                                          const LHCb::HltSelReports& sel_reports ) const override {
    Container output;

    // Collect the reports we'll match against
    std::vector<const LHCb::HltObjectSummary*> summaries_to_check;
    for ( const auto& name_report : sel_reports ) {
      if ( m_decisionPattern.value().match( name_report.first ) ) {
        const auto& report = name_report.second;
        summaries_to_check.push_back( &report );
      }
    }

    // Check each object to see if it's TOS against at least one report
    std::scoped_lock guard{m_lock};
    for ( const auto& obj : input ) {
      auto&& ids = lhcb_ids( obj );
      m_tistos->setSignal( ids );
      auto passed = std::any_of( summaries_to_check.begin(), summaries_to_check.end(),
                                 [&]( const auto& summary ) { return m_tistos->tos( *summary ); } );
      if ( passed ) { output.push_back( obj ); }
      m_eff_object += passed;
    }

    auto filter_passed = !output.empty();
    m_eff_event += filter_passed;

    return {filter_passed, std::move( output )};
  };

private:
  Gaudi::Property<RegEx> m_decisionPattern{this, "DecisionPattern", "",
                                           "Regular expression of decision names to require TOS against."};

  /// Tool to compute TIS/TOS decisions
  /// Must be mutable as all the methods of this tool are non-const
  mutable ToolHandle<IParticleTisTos> m_tistos{this, "ParticleTisTosTool", "ParticleTisTos"};

  /// Lock acquired during `operator()` to avoid race conditions when
  /// interacting with `m_tistos`
  mutable std::mutex m_lock;

  mutable Gaudi::Accumulators::BinomialCounter<> m_eff_object{this, "Object selection efficiency"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_eff_event{this, "Event selection efficiency"};
};

DECLARE_COMPONENT_WITH_ID( TOSFilter<std::vector<LHCb::Event::v1::Track>>, "TOSFilter__v1__Track" )
DECLARE_COMPONENT_WITH_ID( TOSFilter<std::vector<LHCb::RecVertex>>, "TOSFilter__v1__RecVertex" )
