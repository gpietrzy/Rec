###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/TrackRefitting
------------------
#]=======================================================================]

gaudi_add_module(TrackRefitting
    SOURCES
        src/SelectTracksForParticles.cpp
    LINK
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbAlgsLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        Rec::TrackInterfacesLib
        Rec::LoKiPhysLib
)
