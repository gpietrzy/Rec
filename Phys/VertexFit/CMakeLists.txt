###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Phys/VertexFit
--------------
#]=======================================================================]

gaudi_add_header_only_library(VertexFitLib
    LINK
        Gaudi::GaudiKernel
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::TrackEvent
        Rec::SelKernelLib
        Rec::SelToolsLib
)

gaudi_add_module(VertexFit
    SOURCES
        src/AdaptivePVReFitter.cpp
        src/DirectionFitter.cpp
        src/MomentumCombiner.cpp
        src/OfflineVertexFitter.cpp
        src/PVReFitter.cpp
        src/PVTrackRemover.cpp
        src/ParticleAdder.cpp
        src/ParticleVertexFitter.cpp
        src/PropertimeFitter.cpp
    LINK
        Boost::container
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        LHCb::LHCbKernel
        LHCb::LHCbMathLib
        LHCb::PartPropLib
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::TrackEvent
        Rec::DaVinciInterfacesLib
        Rec::DaVinciKernelLib
        Rec::DaVinciTypesLib
        Rec::LoKiPhysLib
        Rec::TrackInterfacesLib
        Rec::TrackKernel
        ROOT::MathCore
        VertexFitLib
)

gaudi_add_tests(QMTest)
