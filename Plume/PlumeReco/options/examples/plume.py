###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
import glob
from Gaudi.Configuration import VERBOSE
from GaudiConf.LbExec import Options

from PyConf.application import (
    default_raw_banks,
    configure_input,
    configure,
    make_odin,
    CompositeNode,
)
from PyConf.Algorithms import (
    PlumeRawToDigits,
    PlumeDigitMonitor,
    PlumeTuple,
)

options = Options(
    input_type="RAW",
    simulation=False,
    data_type="2023",
    input_files=
    'mdf:root://eoslhcb.cern.ch//eos/lhcb/cern-swtest/lhcb/swtest/2024_raw_hlt1_290683/Run_0000290683_20240417-065023-114_UCEB02_0001.mdf',
    dddb_tag='dddb-20210617',
    conddb_tag='sim-20210617-vc-md100',
    evt_max=200,
    histo_file=f'./plume_histos.root',
    ntuple_file=f'./plume_ntuple.root',
    monitoring_file=f"plume_output.json",
)
configure_input(options)  # must call this before calling default_raw_event
odin = make_odin()

read_all_channels = True

digits = PlumeRawToDigits(
    ReadAllChannels=read_all_channels,
    RawBankLocation=default_raw_banks("Plume")).Output

monitor = PlumeDigitMonitor(Input=digits, ODIN=odin)

plume_tuple = PlumeTuple(
    Input=digits, ODIN=odin, ReadAllChannels=read_all_channels)

top_node = CompositeNode("Top", [plume_tuple, monitor])
configure(options, top_node)

from Configurables import LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV
reserveIOV("reserveIOV").PreloadGeometry = False
