/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PlumeAdc.h"
#include "Event/RawEvent.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/Accumulators/Histogram.h"
#include "Kernel/PlumeChannelID.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbMath/LHCbMath.h"
#include "boost/container/small_vector.hpp"

#include <bitset>
#include <fmt/core.h>
#include <map>
#include <vector>

using LHCb::Detector::Plume::ChannelID;

/** @class PlumeRawToDigits
 *
 * Decode Plume raw data to PlumeAdcs
 *
 * @author Vladyslav Orlov
 * @author Rosen Matev
 *
 */
class PlumeRawToDigits : public LHCb::Algorithm::Transformer<LHCb::PlumeAdcs( const LHCb::RawBank::View& )> {

public:
  /// Standard constructor
  PlumeRawToDigits( const std::string&, ISvcLocator* );
  LHCb::PlumeAdcs operator()( const LHCb::RawBank::View& ) const override;

private:
  Gaudi::Property<bool> m_read_all_channels{this, "ReadAllChannels", false,
                                            "Read all channels, including those that are nominally not used"};
  Gaudi::Property<int>  m_pedestalOffset{this, "PedestalOffset", 256, "Offset to subtract from raw ADC counts."};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noLumiAndMonitoringBank{
      this, "No lumi and monitoring bank found for Plume"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noTimingBank{this, "No timing bank found for Plume"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_tooManyBanks{this, "Too many banks for Plume"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noFoundBanks{this, "Banks not found"};
  mutable Gaudi::Accumulators::WeightedHistogram<1>     m_sumADCPerChannel{
      this, "SumADCPerChannel", "Sum of ADCs per channel", {4 * 32, 0, 4 * 32}};
};

DECLARE_COMPONENT( PlumeRawToDigits )

//=============================================================================
// Standard creator, initializes variables
//=============================================================================
PlumeRawToDigits::PlumeRawToDigits( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, KeyValue{"RawBankLocation", ""}, {KeyValue{"Output", ""}} ) {}

//=========================================================================
//  Main execution
//=========================================================================

LHCb::PlumeAdcs PlumeRawToDigits::operator()( const LHCb::RawBank::View& banks ) const {
  LHCb::PlumeAdcs adcs;

  if ( banks.size() > 4 ) { ++m_tooManyBanks; } // two banks for lumi + monitoring, two banks for timing

  bool foundLumiAndMonitoringBank = false;
  bool foundTimingBank            = false;
  bool foundBanks                 = false;

  for ( const auto& bank : banks ) {
    if ( bank->type() != LHCb::RawBank::BankType::Plume ) {
      foundBanks = false;
      continue;
    }
    if ( bank->version() != 4 ) {
      throw GaudiException( "Plume raw bank version not supported, decoding is assuming version 4",
                            "LHCb::PlumeAdcs PlumeRawToDigits", StatusCode::FAILURE );
    }

    foundBanks         = true;
    const int sourceID = bank->sourceID();

    using bitset = std::bitset<32 * 32>; // 32 channels x 32 bits
    bitset bits;
    for ( const auto& [i, data] : LHCb::range::enumerate( bank->range<std::byte>().subspan( 0, 128 ) ) ) {
      auto num = static_cast<uint8_t>( data );
      bits |= bitset{num} << ( i * 8 );

      if ( msgLevel( MSG::VERBOSE ) ) { verbose() << fmt::format( "i={:02} num={:02x}", i, num ) << endmsg; }
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Reading PLUME bank ID " << sourceID << endmsg;

    const int n_channels = 32;
    if ( sourceID == LHCb::Plume::lumiAndMonitoringSourceID_1 ||
         sourceID == LHCb::Plume::lumiAndMonitoringSourceID_2 ) {
      foundLumiAndMonitoringBank = true;

      unsigned int shift = ( sourceID == LHCb::Plume::lumiAndMonitoringSourceID_1 ) ? 0 : 32;
      for ( int i = 0; i < n_channels; ++i ) {
        auto channel       = (unsigned int)( bitset{0xFFFFFFFF} & ( bits >> ( 32 * i ) ) ).to_ulong();
        auto overThreshold = (unsigned int)( ( 0x80000000 & channel ) >> 31 ); // first bit is ovt bit
        auto pedsubAdc     = (unsigned int)( ( 0x7ffff000 & channel ) >> 12 ); // then 19 bits ped sub adc
        auto rawAdc        = (unsigned int)( 0xFFF & channel );                // then 12 bits not ped sub from FEB
        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << i << " , CHANNEL: " << channel << " , OVT:  " << overThreshold
                    << " , PED SUB ADC: " << pedsubAdc / 128. << " , RAW ADC: " << rawAdc << endmsg;
        }

        if ( i < LHCb::Plume::nLumiPMTsPerBank ) {
          auto pmt_id = LHCb::Plume::lumiFebToLogicalChannel.at( shift + i );
          auto adc    = std::make_unique<LHCb::PlumeAdc>( ChannelID( ChannelID::ChannelType::LUMI, pmt_id ),
                                                       std::round( pedsubAdc / 128. ) - m_pedestalOffset,
                                                       std::round( rawAdc - pedsubAdc / 128. ), overThreshold );
          if ( msgLevel( MSG::VERBOSE ) ) {
            verbose() << "Lumi channel, Bank: " << sourceID << " / " << shift << " / " << i << " ADC: " << *adc
                      << endmsg;
          }

          adcs.insert( adc.release() );
          m_sumADCPerChannel[shift + i] += std::round( pedsubAdc / 128. );
        } else if ( i >= LHCb::Plume::nLumiPMTsPerBank &&
                    i < LHCb::Plume::nLumiPMTsPerBank + LHCb::Plume::nEmptyPMTsPerBank ) { // skip two empty channels at
                                                                                           // the end of 3rd line of raw
                                                                                           // bank
          if ( msgLevel( MSG::VERBOSE ) ) {
            verbose() << "Empty channel, Bank: " << sourceID << " / " << shift << " / " << i
                      << " , ADC: " << pedsubAdc / 128. << endmsg;
          }

          m_sumADCPerChannel[shift + i] += std::round( pedsubAdc / 128. );
        } else {
          unsigned int j = i - LHCb::Plume::nLumiPMTsPerBank -
                           LHCb::Plume::nEmptyPMTsPerBank; // index needed for PIN and MON channels. Starts from 0, 0-7
                                                           // PIN diodes, j = 0...7
          if ( sourceID == LHCb::Plume::lumiAndMonitoringSourceID_2 ) {
            j += LHCb::Plume::nPINChannels; // Starts from 8, 8-11 Monitoring PMTs, j = 8...11
            if ( j >= LHCb::Plume::nPINChannels + LHCb::Plume::nMonitoringPMTs )
              continue; // j = 12...15 are not mapped, skip them
          }

          const auto it = LHCb::Plume::monitoringFebToLogicalChannel.find( j );
          if ( it == LHCb::Plume::monitoringFebToLogicalChannel.end() ) continue;

          auto adc = std::make_unique<LHCb::PlumeAdc>(
              ChannelID( it->second.first, it->second.second ), std::round( pedsubAdc / 128. ) - m_pedestalOffset,
              std::round( rawAdc - pedsubAdc / 128. ),
              false ); // ovt bit not meaningful for PIN and Monitoring PMTs, so set to false
          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << "Monitoring channel, Bank: " << sourceID << " / " << shift << " / " << i << " ADC: " << *adc
                      << endmsg;
          adcs.insert( adc.release() );
          m_sumADCPerChannel[shift + i] += std::round( pedsubAdc / 128. );
        }
      }
    } else if ( sourceID == LHCb::Plume::timingSourceID_1 || sourceID == LHCb::Plume::timingSourceID_2 ) {
      foundTimingBank = true;

      unsigned int shift = ( sourceID == LHCb::Plume::timingSourceID_1 ) ? 0 : 32;
      for ( int i = 0; i < n_channels; ++i ) {
        auto channel   = (unsigned int)( bitset{0xFFFFFFFF} & ( bits >> ( 32 * i ) ) ).to_ulong();
        auto pedsubAdc = (unsigned int)( ( 0x7ffff000 & channel ) >> 12 ); // no ovt bit, so 19 bits ped sub adc
        auto rawAdc    = (unsigned int)( 0xFFF & channel );                // then 12 bits not ped sub from FEB

        const auto it =
            LHCb::Plume::timingFebToLogicalChannel[( sourceID == LHCb::Plume::timingSourceID_1 ? 0 : 1 )].find( i );
        if ( it == LHCb::Plume::timingFebToLogicalChannel[( sourceID == LHCb::Plume::timingSourceID_1 ? 0 : 1 )].end() )
          continue;

        auto const& [channelID, channelSubID] = it->second;

        auto adc = std::make_unique<LHCb::PlumeAdc>( ChannelID( ChannelID::ChannelType::TIME, channelID, channelSubID ),
                                                     std::round( pedsubAdc / 128. ) - m_pedestalOffset,
                                                     std::round( rawAdc - pedsubAdc / 128. ),
                                                     false ); // ovt bit not meaningful for Timing PMTs, so set to false

        if ( msgLevel( MSG::VERBOSE ) ) {
          verbose() << "Timing channel, Bank: " << sourceID << " / " << i << " ADC: " << *adc << endmsg;
        }

        adcs.insert( adc.release() );
        m_sumADCPerChannel[n_channels * 2 + shift + i] += std::round( pedsubAdc / 128. );
      }
    } else {
      warning() << fmt::format( "Unknown source ID {}", sourceID ) << endmsg;
    }
  }

  if ( !foundLumiAndMonitoringBank ) ++m_noLumiAndMonitoringBank;
  if ( !foundTimingBank ) ++m_noTimingBank;
  if ( !foundBanks ) ++m_noFoundBanks;

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Found ADCs:\n";
    for ( const auto& adc : adcs ) { debug() << *adc << '\n'; }
    debug() << endmsg;
  }

  return adcs;
}
