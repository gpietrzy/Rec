/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/VP/VPChannelID.h"
#include "VPDet/DeVP.h"

#include "Event/VPLightCluster.h"
#include "Event/VPMicroCluster.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   *  Transform the "VPMicroClusters" to a set of VPLiteClusters.
   *
   *  Depends on the geometry by the filling of the cluster's position information
   *
   *  @author Laurent Dufour
   */
  struct VPMicroClustersToVPLightClustersConverter
      : Algorithm::Transformer<VPLightClusters( VPMicroClusters const&, DeVP const& ),
                               Algorithm::Traits::usesConditions<DeVP>> {

    VPMicroClustersToVPLightClustersConverter( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {KeyValue{"InputClusters", LHCb::VPClusterLocation::Micro},
                        KeyValue{"DeVPLocation", LHCb::Det::VP::det_path}},
                       {KeyValue{"OutputClusters", ""}} ) {}
    LHCb::VPLightClusters operator()( VPMicroClusters const&, DeVP const& ) const override;

    mutable Gaudi::Accumulators::AveragingCounter<unsigned long> m_convertedClusters{this, "# Converted Clusters"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::VPMicroClustersToVPLightClustersConverter,
                           "VPMicroClustersToVPLightClustersConverter" )

LHCb::VPLightClusters LHCb::VPMicroClustersToVPLightClustersConverter::operator()( LHCb::VPMicroClusters const& dst,
                                                                                   DeVP const& devp ) const {
  LHCb::VPLightClusters clusters;

  for ( const auto& vp_micro_cluster : dst.range() ) {
    const auto& vpID = vp_micro_cluster.channelID();

    const float    fx      = vp_micro_cluster.xfraction() / 255.;
    const float    fy      = vp_micro_cluster.yfraction() / 255.;
    const uint32_t cx      = vpID.scol();
    const float    local_x = devp.local_x( cx ) + fx * devp.x_pitch( cx );
    const float    local_y = ( 0.5f + fy + to_unsigned( vpID.row() ) ) * devp.pixel_size();

    const auto& ltg = devp.ltg( vpID.sensor() );

    const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
    const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
    const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

    clusters.emplace_back( vp_micro_cluster.xfraction(), vp_micro_cluster.yfraction(), gx, gy, gz, vpID );
  }

  m_convertedClusters += clusters.size();

  return clusters;
}
