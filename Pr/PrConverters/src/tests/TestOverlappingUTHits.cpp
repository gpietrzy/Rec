/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/UTLiteCluster.h"
#include "LHCbAlgs/Consumer.h"
#include "PrKernel/UTHit.h"
#include "PrKernel/UTHitHandler.h"

namespace LHCb {
  /**
   *  Tests to see whether all of the clusters in the UTHits container
   *  (i.e. the UT Hit Hanlder)
   *  are indeed present in the other UTHits container.
   *
   *  @author Laurent Dufour
   */
  struct TestOverlappingUTHits : Algorithm::Consumer<void( UT::HitHandler const&, UT::HitHandler const& )> {

    TestOverlappingUTHits( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {KeyValue{"UTHits_A", ""}, KeyValue{"UTHits_B", ""}} ) {}
    void operator()( UT::HitHandler const&, UT::HitHandler const& ) const override;

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_missed_in_B{this, "Missing clusters in [B]"};
    mutable Gaudi::Accumulators::Counter<>                m_found_in_B{this, "Found clusters in [B]"};

    bool is_same_uthit( UT::Hit const& uth_a, UT::Hit const& uth_b ) const {
      if ( uth_a.lhcbID().lhcbID() != uth_b.lhcbID().lhcbID() ) // the most obvious one
        return false;

      // let's check things that should match, on the lowest level
      if ( uth_a.size() != uth_b.size() || uth_a.clusterCharge() != uth_b.clusterCharge() ) {
        if ( this->msgLevel( MSG::VERBOSE ) ) {
          this->verbose() << "Mismatch: [size] (A) " << uth_a.size();
          this->verbose() << " (B) " << uth_b.size() << endmsg;

          this->verbose() << "Mismatch: [clusterCharge] (A) " << uth_a.clusterCharge();
          this->verbose() << " (B) " << uth_b.clusterCharge() << endmsg;
        }

        return false;
      }

      if ( fabs( uth_a.fracStrip() - uth_b.fracStrip() ) > .55 / 255 ) // tolerance due to limited precision
      {
        if ( this->msgLevel( MSG::VERBOSE ) ) {
          this->verbose() << "Mismatch: [fracStrip] (A) " << uth_a.fracStrip();
          this->verbose() << " (B) " << uth_b.fracStrip() << endmsg;
        }

        return false;
      }

      if ( uth_a.strip() != uth_b.strip() ) {
        if ( this->msgLevel( MSG::VERBOSE ) ) {
          this->verbose() << "Mismatch: [strip] (A) " << uth_a.strip();
          this->verbose() << " (B) " << uth_b.strip() << endmsg;
        }

        return false;
      }

      // now derived quantities
      // there are these (seemingly arbitrary) tolerances on the differences
      // due to floating point precision
      if ( ( uth_a.pseudoSize() != uth_b.pseudoSize() ) || ( fabs( uth_a.tanT() - uth_b.tanT() ) > 0.001 ) ||
           ( uth_a.planeCode() != uth_b.planeCode() ) || ( fabs( uth_a.weight() - uth_b.weight() ) > 0.0001 ) ||
           ( fabs( uth_a.error() - uth_b.error() ) > 0.001 ) ||
           ( fabs( uth_a.xAtYEq0() - uth_b.xAtYEq0() ) > 0.003 ) || // can depend on fracstrip; looser tolerance
           ( fabs( uth_a.xT() - uth_b.xT() ) > 0.001 ) || ( fabs( uth_a.yBegin() - uth_b.yBegin() ) > 0.001 ) ||
           ( fabs( uth_a.yEnd() - uth_b.yEnd() ) > 0.001 ) || ( fabs( uth_a.yMid() - uth_b.yMid() ) > 0.001 ) ||
           ( fabs( uth_a.zAtYEq0() - uth_b.zAtYEq0() ) > 0.001 ) ) {
        if ( this->msgLevel( MSG::VERBOSE ) ) {
          this->verbose() << "Mismatch in derived quantities: " << endmsg;
          this->verbose() << "pseudosize: " << uth_a.pseudoSize() << " vs " << uth_b.pseudoSize() << endmsg;
          this->verbose() << "planeCode: " << uth_a.planeCode() << " vs " << uth_b.planeCode() << endmsg;
          this->verbose() << "tanT: " << uth_a.tanT() << " vs " << uth_b.tanT() << endmsg;
          this->verbose() << "weight: " << uth_a.weight() << " vs " << uth_b.weight() << endmsg;
          this->verbose() << "error: " << uth_a.error() << " vs " << uth_b.error() << endmsg;
          this->verbose() << "xAtYEq0: " << uth_a.xAtYEq0() << " vs " << uth_b.xAtYEq0() << endmsg;
          this->verbose() << "(difference: " << fabs( uth_a.xAtYEq0() - uth_b.xAtYEq0() ) << endmsg; // 0.000366211
          this->verbose() << "xT: " << uth_a.xT() << " vs " << uth_b.xT() << endmsg;
          this->verbose() << "yBegin: " << uth_a.yBegin() << " vs " << uth_b.yBegin() << endmsg;
          this->verbose() << "yEnd: " << uth_a.yEnd() << " vs " << uth_b.yEnd() << endmsg;
          this->verbose() << "yMid: " << uth_a.yMid() << " vs " << uth_b.yMid() << endmsg;
          this->verbose() << "zAtYEq0: " << uth_a.zAtYEq0() << " vs " << uth_b.zAtYEq0() << endmsg;
        }

        return false;
      }

      return true;
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::TestOverlappingUTHits, "TestOverlappingUTHits" )

void LHCb::TestOverlappingUTHits::operator()( UT::HitHandler const& utlcs_a, UT::HitHandler const& utlcs_b ) const {

  /// Tests for the presence of the UTHit from A in the Container B
  // This is rather slow - ideally the UTHit container moves to
  // a IndexedHitContainer
  for ( const auto& ut_hit : utlcs_a.hits() ) {
    auto it = std::find_if( utlcs_b.hits().begin(), utlcs_b.hits().end(),
                            [&]( const UT::Hit& utlc_b ) { return is_same_uthit( ut_hit, utlc_b ); } );
    if ( it == utlcs_b.hits().end() ) {
      ++m_missed_in_B;
    } else {
      ++m_found_in_B;
    }
  }
}
