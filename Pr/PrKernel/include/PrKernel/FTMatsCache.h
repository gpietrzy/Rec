/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "FTDAQ/FTInfo.h"
#include "FTDet/DeFTDetector.h"

#include "Core/FloatComparison.h"

#include <array>

namespace LHCb::Detector::FT::Cache {
  struct MatsCache {
    static constexpr std::string_view Location = "AlgorithmSpecific-FTMatsCache";
    /**
     * partial SoA cache for mats, reserve enough (here 4096 which is more than enough)
     * space for all mats ( all mats should be less than 2 * 8 mats * 12 modules * 12 layers)
     */
    std::array<float, maxNumberMats>                  dxdy{};
    std::array<float, maxNumberMats>                  dzdy{};
    std::array<float, maxNumberMats>                  globaldy{};
    std::array<ROOT::Math::XYZPointF, maxNumberMats>  mirrorPoint{};
    std::array<ROOT::Math::XYZVectorF, maxNumberMats> ddx{};
    std::array<std::vector<double>, maxNumberMats>    matContractionParameterVector{};
    float                                             uBegin{};
    float                                             halfChannelPitch{};
    float                                             dieGap{};
    float                                             sipmPitch{};

    MatsCache(){}; // Needed for DD4HEP
    MatsCache( const DeFT& ftDet ) {
      const auto first_mat = ftDet.firstMat();
      // This parameters are constant accross all mats:
#ifdef USE_DD4HEP
      this->dieGap           = first_mat.dieGap();
      this->sipmPitch        = first_mat.sipmPitch();
      this->uBegin           = first_mat.uBegin();
      this->halfChannelPitch = first_mat.halfChannelPitch();
#else
      this->dieGap           = first_mat->dieGap();
      this->sipmPitch        = first_mat->sipmPitch();
      this->uBegin           = first_mat->uBegin();
      this->halfChannelPitch = first_mat->halfChannelPitch();
#endif
      auto func = [this]( const DeFTMat& mat ) {
        assert( LHCb::essentiallyEqual( this->dieGap, mat.dieGap() ) && "Unexpected difference in dieGap" );
        assert( LHCb::essentiallyEqual( this->sipmPitch, mat.sipmPitch() ) && "Unexpected difference in sipmPitch" );
        assert( LHCb::essentiallyEqual( this->uBegin, mat.uBegin() ) && "Unexpected difference in uBegin" );
        assert( LHCb::essentiallyEqual( this->halfChannelPitch, mat.halfChannelPitch() ) &&
                "Unexpected difference in halfChannelPitch" );
        const auto index = mat.elementID().globalMatID();
        // FIXME
        this->mirrorPoint[index]                   = mat.mirrorPoint();
        this->ddx[index]                           = mat.ddx();
        this->dxdy[index]                          = mat.dxdy();
        this->dzdy[index]                          = mat.dzdy();
        this->globaldy[index]                      = mat.globaldy();
        this->matContractionParameterVector[index] = mat.getmatContractionParameterVector();
      };
      ftDet.applyToAllMats( func );
    };
  };
} // namespace LHCb::Detector::FT::Cache
