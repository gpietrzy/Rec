/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "PrKernel/PrChecker.h"

#include "Core/FloatComparison.h"
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCProperty.h"
#include "Event/MCTrackInfo.h"
#include "Event/MCVertex.h"
#include "Event/Track.h"
#include "LoKi/IMCHybridFactory.h"
#include "LoKi/MCParticles.h"
#include "LoKi/Primitives.h"
#include "MCInterfaces/IMCReconstructible.h"
#include "MuonDet/DeMuonDetector.h"
#include "TrackInterfaces/ITrackExtrapolator.h"

#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiAlg/GaudiHistoTool.h"
#include "GaudiAlg/IHistoTool.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Consumer.h"

#include "boost/algorithm/string/replace.hpp"

#include "PrTrackCounter.h"
#include "PrUTCounter.h"

/**
 *  Check the quality of the pattern recognition, by comparing to MC information
 *  Produces efficiency, ghost rate and clone rate numbers.
 *  Parameters:
 *   - [deprecated] Eta25Cut: Only consider particles with 2 < eta < 5? (default: false)
 *   - CheckNegEtaPlot: Check eta plotting range, plot negative values only if no eta25 cut was applied (default: false)
 *   - TriggerNumbers: Give numbers for p > 3GeV, pT > 500 MeV? (default: false)
 *     if selected long_fromB_P>3GeV_Pt>0.5GeV cut is added to each track container
 *   - VetoElectrons: Take electrons into account in numbers? (default: true)
 *   - WriteTexOutput: Writes the statistics table to a TeX file (default: false)
 *     which is dumped to the location specified in TexOutputFolder
 *   - MyCuts: selection cuts to be applied (default: empty)
 *   - WriteHistos: whether to plot histograms via IHistoTool. Values are -1 (default, no histograms), 1 (histograms), 2
 * (mote histograms : expectedHits, docaz, PVz, EtaP, EtaPhi, efficiency maps @z=9000mm XYZ9000 and @z=2485mm XYZ2485)
 *
 * This class is templated and is available in python with PrCounter and PrUTCounter as template arguments
 * The respective names to import are PrChecker and PrUTHitChecker
 *
 * Typical usage :
 *
 * @code
 *   from Configurables import PrChecker
 *   mychecker = PrChecker("PrCheckerVelo",
 *                          Title="Velo",
 *                          Tracks = "Rec/Track/Velo",
 *                          TriggerNumbers=True,
 *                          MyCuts = { "01_velo" : "isVelo",
 *                                     "02_long" : "isLong",
 *                                     "03_long>5GeV" : "isLong & over5" } ))
 *
 *   from Configurables import LoKi__Hybrid__MCTool
 *   myFactory = LoKi__Hybrid__MCTool("MCHybridFactory")
 *   myFactory.Modules = [ "LoKiMC.decorators" ]
 *   mychecker.addTool( myFactory )
 *  @endcode
 *
 *  As a default selection cuts of old PrChecker are used. The following cuts are predefined:
 *  - is(Not)Long, is(Not)Velo, is(Not)Down, is(Not)Up, is(Not)UT, is(Not)Seed,
 *  - fromB, fromD, BOrDMother, fromKsFromB, strange
 *  - is(Not)Electron, eta25, over5, trigger
 *
 *  and LoKi syntax (LoKi::MCParticles) can be used for kinematical cuts, e.g. (MCPT> 2300), here the '()' are
 * essential.
 *
 *  NB: If you care about the implementation: The cut-strings are converted into two types of functors:
 *  - LoKi-type functors (hence all LoKi::MCParticles cuts work)
 *  - and custom-defined ones, mostly for type of reconstructibility and daughter-criteria (like 'isNotLong')
 *  where in the end all functors are evaluated on each MCParticle for each track container to define the
 * reconstructibility. If a MCParticle is actually reconstructed is checked. A large part of the code just deals with
 * the conversion of the strings into functors.
 *
 */

namespace LHCb::Pr::Checker {

  namespace {
    using Gaudi::Functional::details::zip::range;
    class isTrack {
    public:
      isTrack( RecAs kind ) : m_kind{kind} {};
      /// Functor that checks if the MCParticle fulfills certain criteria, e.g. reco'ble as long track, B daughter, ...
      bool operator()( const MCParticle* mcp, const MCTrackInfo& mcInfo, span<const LHCbID> lhcbIds ) const {
        if ( auto type = reconstructibleType( mcp, m_kind, mcInfo ); type.has_value() ) return type.value();
        if ( auto particle = particleType( mcp, m_kind ); particle.has_value() ) return particle.value();
        switch ( m_kind ) {
        case RecAs::hasVeloOverlap: {
          std::vector<LHCb::LHCbID> vpids{};
          for ( auto const& lhcbid : lhcbIds ) {
            if ( lhcbid.isVP() ) {
              auto vp_sensor = lhcbid.vpID().sensor();
              auto vp_module = lhcbid.vpID().module();
              for ( auto const& id2 : vpids ) {
                // if 2 ids are in the same module but different sensors, there is an overlap
                if ( vp_module == id2.vpID().module() && vp_sensor != id2.vpID().sensor() ) return true;
              }
              vpids.push_back( lhcbid );
            }
          }
          return false;
        }
        case RecAs::hasVeloCrossingSide: {
          std::vector<LHCb::LHCbID> vpids{};
          for ( auto const& lhcbid : lhcbIds ) {
            if ( lhcbid.isVP() ) {
              auto vp_station = lhcbid.vpID().station();
              auto vp_module  = lhcbid.vpID().module();
              for ( auto const& id2 : vpids ) {
                // if 2 ids are in the same station but different modules, the particle is crossing side
                if ( vp_station == id2.vpID().station() && vp_module != id2.vpID().module() ) return true;
              }
              vpids.push_back( lhcbid );
            }
          }
          return false;
        }
        case RecAs::muonHitsInAllStations: {
          // Check that lhcbIds contains at least one muon hit in each muon station
          constexpr auto          n_stations = 4;
          std::bitset<n_stations> seen;
          for ( auto const& lhcbid : lhcbIds ) {
            if ( lhcbid.isMuon() ) { seen[lhcbid.muonID().station()] = true; }
          }
          return seen.all();
        }
        case RecAs::muonHitsInAtLeastTwoStations: {
          // Check that lhcbIds contains at least one muon hit in at least two muon stations
          constexpr auto          n_stations = 4;
          std::bitset<n_stations> seen;
          for ( auto const& lhcbid : lhcbIds ) {
            if ( lhcbid.isMuon() ) { seen[lhcbid.muonID().station()] = true; }
          }
          return ( seen.count() >= 2 );
        }
        default:;
        }
        if ( auto origin = originType( mcp, m_kind ); origin.has_value() ) return origin.value();
        return false;
      }

    private:
      RecAs m_kind;
    };

    /**
     *  Class that adds selection cuts defined in isTrack to cuts
     */
    class RecAsCuts {
      std::vector<isTrack> m_cuts;

    public:
      void addCut( RecAs cat ) { m_cuts.emplace_back( cat ); }

      /// Functor that evaluates all 'isTrack' cuts
      bool operator()( const MCParticle* mcp, const MCTrackInfo& mcInfo,
                       std::vector<LHCb::LHCbID> const& lhcbIds ) const {
        return std::all_of( m_cuts.begin(), m_cuts.end(),
                            [&]( auto cut ) { return std::invoke( cut, mcp, mcInfo, lhcbIds ); } );
      }
    };
  } // namespace
  template <typename InternalCounter>
  class PrCheckerAlgorithm
      : public LHCb::Algorithm::Consumer<
            void( LHCb::Track::Range const&, LHCb::MCParticles const&, LHCb::MCVertices const&, LHCb::MCProperty const&,
                  LHCb::LinksByKey const&, LHCb::LinksByKey const&, DetectorElement const& ),
            LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DetectorElement>> {
  public:
    PrCheckerAlgorithm( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"Tracks", ""}, KeyValue{"MCParticleInput", LHCb::MCParticleLocation::Default},
                     KeyValue{"MCVerticesInput", LHCb::MCVertexLocation::Default},
                     KeyValue{"MCPropertyInput", LHCb::MCPropertyLocation::TrackInfo}, KeyValue{"Links", ""},
                     KeyValue{"LinkTableLocation", "Link/Pr/LHCbID"},
                     KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}} ) {}

    /// Algorithm initialization
    StatusCode initialize() override;
    /// Algorithm execution
    void operator()( LHCb::Track::Range const&, LHCb::MCParticles const&, LHCb::MCVertices const&,
                     LHCb::MCProperty const&, LHCb::LinksByKey const&, LHCb::LinksByKey const&,
                     DetectorElement const& ) const override;
    /// Algorithm finalization
    StatusCode finalize() override;

  private:
    Gaudi::Property<std::string>        m_title{this, "Title", ""};
    Gaudi::Property<unsigned int>       m_firstNVeloHits{this, "FirstNVeloHits", 3};
    Gaudi::Property<unsigned int>       m_hitTypesToCheck{this, "HitTypesToCheck", 0};
    Gaudi::Property<LHCb::Track::Types> m_trackType{this, "TrackType", LHCb::Track::Types::Unknown};

    // The counters are not at all thread safe for the moment
    // So we protect their use by a mutex. Not optimal but MC checking
    // does not need to be absolutely fast
    mutable InternalCounter m_counter;
    mutable std::mutex      m_counterMutex;

    // -- histograming options
    Gaudi::Property<int>  m_writeHistos{this, "WriteHistos", -1};
    Gaudi::Property<bool> m_triggerNumbers{this, "TriggerNumbers", false};
    Gaudi::Property<bool> m_vetoElectrons{this, "VetoElectrons", true};
    Gaudi::Property<bool> m_xyPlots{this, "XYPlots", false};

    Gaudi::Property<bool>        m_writetexfile{this, "WriteTexOutput", false};
    Gaudi::Property<std::string> m_texfilename{this, "TexOutputName", "efficiencies"};
    Gaudi::Property<std::string> m_texfolder{this, "TexOutputFolder", ""};
    // maps for each track container with {cut name,selection cut}
    Gaudi::Property<std::map<std::string, std::string>> m_cutMap{this, "MyCuts", {}};

    ToolHandle<IHistoTool>             m_histoTool{this, "HistoTool", "HistoTool/PrCheckerHistos"};
    ToolHandle<LoKi::IMCHybridFactory> m_factory{this, "LoKiFactory", "LoKi::Hybrid::MCTool/MCHybridFactory:PUBLIC"};
    ToolHandle<ITrackExtrapolator>     m_extrapolator{this, "TrackMasterExtrapolator", "TrackMasterExtrapolator"};

    std::vector<LoKi::Types::MCCut> m_MCCuts;
    std::vector<RecAsCuts>          m_MCRecAsCuts;
  };

  template <typename InternalCounter>
  StatusCode PrCheckerAlgorithm<InternalCounter>::initialize() {
    StatusCode sc = Consumer::initialize();
    if ( sc.isFailure() ) { return sc; }

    static const std::string histoDir = "Track/";
    if ( "" == histoTopDir() ) setHistoTopDir( histoDir );
    m_histoTool.retrieve().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ ); // needs to be done for next line to
                                                                                    // work
    GaudiHistoTool* ghtool = dynamic_cast<GaudiHistoTool*>( m_histoTool.get() );

    // -- catch the possible failure of the dynamic cast
    if ( !ghtool ) {
      error() << "Dynamic cast of Gaudi Histogramming Tool failed!" << endmsg;
      return StatusCode::FAILURE;
    }

    ghtool->setHistoDir( histoDir + name() );

    m_counter.setTitle( m_title.value() );
    m_counter.setFirstNVeloHits( m_firstNVeloHits );
    m_counter.setWriteHistos( m_writeHistos );
    m_counter.setHitTypesToCheck( m_hitTypesToCheck );
    m_counter.setTrackType( m_trackType );
    m_counter.setXYPlots( m_xyPlots );
    m_counter.setTriggerNumbers( m_triggerNumbers );
    if ( m_writetexfile.value() ) m_counter.setTeXName( m_texfolder, m_texfilename );

    // remove all RecAs type cuts from string and fill them into separate object
    // after this only LoKi functors are left in the string. The string is then
    // converted into a LoKi MCCut by the factory
    for ( auto [name, cutString] : m_cutMap ) {
      // last arg is true is "eta25" not found which triggers negative eta values to be plotted
      m_counter.addSelection( name, true, cutString.find( "eta25" ) == std::string::npos );
      auto& thisRecAsSelection = m_MCRecAsCuts.emplace_back();
      // flag to circumvent veto of electrons for selections that only look at elctrons
      bool ExplicitlyKeepElectrons = cutString.find( "isElectron" ) != std::string::npos;
      // -- extract aliases from cutString and replace with 'MCTRUE'
      for ( auto alias : members_of<RecAs>() ) {
        if ( alias == RecAs::Unknown ) continue; // skip unknown
        std::size_t found = cutString.find( toString( alias ) );

        if ( found != std::string::npos ) {   // if found then
          thisRecAsSelection.addCut( alias ); // other components are already filled //add this
                                              // category of cuts to addOtherCuts()
          cutString.replace( found, toString( alias ).length(),
                             "MCTRUE" ); // replace found at position found, with length of string
                                         // to replace, replace it with string "" (Loki Cut)
        }
      }

      // -- Veto electrons or not
      if ( m_vetoElectrons && !ExplicitlyKeepElectrons ) {
        thisRecAsSelection.addCut( RecAs::isNotElectron );
        boost::replace_first( cutString, toString( RecAs::isNotElectron ), "MCTRUE" );
      }

      // -- LoKi cuts: define aliases for better use
      const std::string etaString( "eta25" );
      std::size_t       found = cutString.find( etaString );
      if ( found != std::string::npos ) {
        cutString.replace( found, etaString.length(), "(MCETA > 2.0) & (MCETA < 5.0)" );
      }

      boost::replace_first( cutString, "over5", "(MCP > 5000)" );
      boost::replace_first( cutString, "trigger", "(MCP>3000) & (MCPT>500)" );

      // ---------------------------------------------------------------------------------

      LoKi::Types::MCCut tmp = LoKi::BasicFunctors<const LHCb::MCParticle*>::BooleanConstant( false ); //
      if ( m_factory->get( cutString, tmp ).isFailure() ) {
        throw GaudiException( "Failed to get LoKi MCCuts.", this->name(), StatusCode::FAILURE );
      }
      m_MCCuts.push_back( tmp );
    }

    return StatusCode::SUCCESS;
  }

  template <typename InternalCounter>
  void PrCheckerAlgorithm<InternalCounter>::
       operator()( LHCb::Track::Range const& tracks, LHCb::MCParticles const& mcParts, LHCb::MCVertices const& mcVert,
              LHCb::MCProperty const& flags, LHCb::LinksByKey const& tr2McLink, LHCb::LinksByKey const& mc2IdLink,
              DetectorElement const& geometry ) const {
    // in debug mode, check consistency of the inputs
    assert( tr2McLink.sourceClassID() == LHCb::Track::classID() &&
            "Incompatible link table in PrCheckerAlgorithm. Source should be Track" );
    assert( tr2McLink.targetClassID() == LHCb::MCParticle::classID() &&
            "Incompatible link table in PrCheckerAlgorithm. Target should be McParticle" );

    MCTrackInfo  trackInfo = {flags};
    unsigned int nPrim     = std::count_if( mcVert.begin(), mcVert.end(), [&]( const auto& vertex ) {
      if ( !vertex->isPrimary() ) return false;
      int nbVisible = std::count_if( mcParts.begin(), mcParts.end(), [&]( const auto& part ) {
        return part->primaryVertex() == vertex && trackInfo.hasVelo( part );
      } );
      return nbVisible > 4;
    } );

    m_counter.initEvent( m_histoTool.get(), m_extrapolator.get(), nPrim, tracks, tr2McLink, geometry );

    //== Build a table (vector of map) of Track -> weigth per MCParticle, indexed by MCParticle key.
    std::vector<std::map<const LHCb::Track*, double>> tracksForParticle;
    tr2McLink.applyToAllLinks( [&tracksForParticle, &tracks]( int trackKey, unsigned int mcPartKey, float weight ) {
      auto track = std::find_if( tracks.begin(), tracks.end(), [&trackKey]( auto t ) { return t->key() == trackKey; } );
      if ( track != tracks.end() ) {
        if ( tracksForParticle.size() <= mcPartKey ) { tracksForParticle.resize( mcPartKey + 1 ); }
        tracksForParticle[mcPartKey][*track] += weight;
      }
    } );

    //== Build a table (vector of vectors) of LHCbID per MCParticle, indexed by MCParticle key.
    std::vector<std::vector<LHCb::LHCbID>> idsForParticle;
    mc2IdLink.applyToAllLinks( [&idsForParticle]( unsigned int id, unsigned int mcPartKey, float ) {
      if ( idsForParticle.size() <= mcPartKey ) { idsForParticle.resize( mcPartKey + 1 ); }
      idsForParticle[mcPartKey].emplace_back( id );
    } );

    std::vector<LHCb::LHCbID>            noids;
    std::map<const LHCb::Track*, double> noTracksList;
    for ( const auto part : mcParts ) {
      if ( 0 == trackInfo.fullInfo( part ) ) continue;
      // get the LHCbIDs that go with the MCParticle
      auto& ids = idsForParticle.size() > (unsigned int)part->key() ? idsForParticle[part->key()] : noids;
      // cuts
      std::vector<bool> flags;
      for ( const auto& [lokiCut, recAsCut] : range( m_MCCuts, m_MCRecAsCuts ) ) {
        flags.push_back( lokiCut( part ) && recAsCut( part, trackInfo, ids ) );
      }
      try {
        m_counter.countAndPlot( m_histoTool.get(), m_extrapolator.get(), part, flags, ids, nPrim,
                                tracksForParticle.size() > (unsigned int)part->key() ? tracksForParticle[part->key()]
                                                                                     : noTracksList,
                                geometry );
      } catch ( const std::string& msg ) {
        Warning( msg ).ignore();
        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "... Flag size " << flags.size() << " >  " << m_counter.title().size() << " declared selections"
                  << endmsg;
        }
      }
    }
  }

  template <typename InternalCounter>
  StatusCode PrCheckerAlgorithm<InternalCounter>::finalize() {
    info() << "Results" << endmsg;
    m_counter.printStatistics( info(), inputLocation<0>() );
    return Consumer::finalize(); // must be called after all other actions
  }

  // Easier with typedefs to avoid strange syntax in Macros
  typedef PrCheckerAlgorithm<PrTrackCounter> PrTrackChecker;
  typedef PrCheckerAlgorithm<PrUTCounter>    PrUTHitChecker;

  DECLARE_COMPONENT_WITH_ID( PrTrackChecker, "PrTrackChecker" )
  DECLARE_COMPONENT_WITH_ID( PrUTHitChecker, "PrUTHitChecker" )

} // namespace LHCb::Pr::Checker
