/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <array>
#include <tuple>
#include <vector>

#include "Detector/VP/VPChannelID.h"
#include "Event/PrHits.h"
#include "Event/VPLightCluster.h"
#include "GaudiKernel/Point3DTypes.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"
#include "PrKernel/VeloPixelInfo.h"

#include "VPDet/DeVP.h"

namespace LHCb::Pr::Velo {
  class PrVPHitsToVPLightClusters
      : public LHCb::Algorithm::MultiTransformer<
            std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>( const VP::Hits&,
                                                                                                        DeVP const& ),
            Algorithm::Traits::usesConditions<DeVP>> {
  public:
    PrVPHitsToVPLightClusters( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer(
              name, pSvcLocator,
              {KeyValue{"HitsLocation", "Raw/VP/Hits"}, KeyValue{"DeVPLocation", LHCb::Det::VP::det_path}},
              {KeyValue{"ClusterLocation", VPClusterLocation::Light},
               KeyValue{"ClusterOffsets", "Raw/VP/LightClustersOffsets"}} ) {}

    std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
    operator()( const VP::Hits& hits, DeVP const& devp ) const override {
      auto result = std::tuple<std::vector<VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};

      auto& [pool, offsets] = result;
      pool.reserve( hits.size() );

      for ( const auto hit : hits.scalar() ) {
        Detector::VPChannelID cid{static_cast<unsigned>( hit.template get<VP::VPHitsTag::ChannelId>().cast() )};
        float                 gx = hit.template get<VP::VPHitsTag::pos>().x().cast();
        float                 gy = hit.template get<VP::VPHitsTag::pos>().y().cast();
        float                 gz = hit.template get<VP::VPHitsTag::pos>().z().cast();

        // do the work of calculating the fractionx, fraction y
        const DeVPSensor& sensor         = devp.sensor( cid );
        const auto        local_position = sensor.globalToLocal( Gaudi::XYZPoint( gx, gy, gz ) );

        const uint32_t      cx        = cid.scol();
        const unsigned char xfraction = 255 * ( local_position.X() - devp.local_x( cx ) ) / devp.x_pitch( cx );

        const uint32_t cy        = to_unsigned( cid.row() );
        const char     yfraction = ( local_position.Y() / devp.pixel_size() - cy - 0.5f ) * 255;

        pool.emplace_back( xfraction, yfraction, gx, gy, gz, cid );
        ++offsets[cid.module()];
      }

      std::partial_sum( offsets.rbegin(), offsets.rend(), offsets.rbegin() );

      // sorting in phi for even modules
      auto cmp_phi_for_odd_modules = []( const VPLightCluster& a, const VPLightCluster& b ) {
        return ( a.y() < 0.f && b.y() > 0.f ) ||
               // same y side even and odd modules, check y1/x1 < y2/x2
               ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
      };

      // sorting in phi for odd modules
      auto cmp_phi_for_even_modules = []( const VPLightCluster& a, const VPLightCluster& b ) {
        return ( a.y() > 0.f && b.y() < 0.f ) ||
               // same y side even and odd modules, check y1/x1 < y2/x2
               ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
      };

      auto sort_module = [pool = std::ref( pool ), offsets = std::ref( offsets )]( auto id, auto cmp ) {
        std::sort( pool.get().begin() + offsets.get()[id + 1], pool.get().begin() + offsets.get()[id], cmp );
      };

      for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
        if ( moduleID % 2 == 1 ) {
          sort_module( moduleID, cmp_phi_for_odd_modules );
        } else {
          sort_module( moduleID, cmp_phi_for_even_modules );
        }
      }

      m_nbClustersCounter += pool.size();
      return result;
    }

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbClustersCounter{this, "Nb of Produced Clusters"};
  };
} // namespace LHCb::Pr::Velo

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::PrVPHitsToVPLightClusters, "PrVPHitsToVPLightClusters" )
