/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <vector>

// Gaudi
#include "LHCbAlgs/Transformer.h"

// LHCb
#include "Event/StateParameters.h"
#include "Event/Track.h"
#include "LHCbDet/InteractionRegion.h"

#include "Event/PrHits.h"
#include "Event/PrLongTracks.h"
#include "Event/PrVeloTracks.h"
#include "Event/Track_v3.h"

#include "VeloKalmanHelpers.h"

/**
 * Velo only Kalman fit
 *
 * @author Arthur Hennequin (CERN, LIP6)
 */
namespace LHCb::Pr::Velo {
  using VP::Hits;

  class Kalman : public Algorithm::Transformer<Event::v3::Tracks( const EventContext&, UniqueIDGenerator const&,
                                                                  const Hits&, const Tracks&, const Long::Tracks&,
                                                                  const Conditions::InteractionRegion& ),
                                               DetDesc::usesConditions<Conditions::InteractionRegion>> {
    using TracksVP  = Tracks;
    using TracksFT  = Long::Tracks;
    using TracksFit = Event::v3::Tracks;
    using simd      = SIMDWrapper::best::types;
    using I         = simd::int_v;
    using F         = simd::float_v;

  public:
    Kalman( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {KeyValue{"UniqueIDGenerator", UniqueIDGeneratorLocation::Default},
                        KeyValue{"HitsLocation", "Raw/VP/Hits"}, KeyValue{"TracksVPLocation", "Rec/Track/VP"},
                        KeyValue{"TracksFTLocation", "Rec/Track/FT"},
                        KeyValue{"InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion"}},
                       KeyValue{"OutputTracksLocation", "Rec/Track/Fit"} ) {}

    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&]() {
        // This is only needed to have a fallback in case the IR condition does not exist. In that case, the information
        // is taken from DeVP and thus the Velo motion system which is not exactly the same.
        Conditions::InteractionRegion::addConditionDerivation( this, inputLocation<Conditions::InteractionRegion>() );
      } );
    }

    TracksFit operator()( const EventContext& evtCtx, LHCb::UniqueIDGenerator const& unique_id_gen, const Hits& hits,
                          const TracksVP& tracksVP, const TracksFT& tracksFT,
                          const LHCb::Conditions::InteractionRegion& region ) const override {
      // Forward tracks and its fit are zipable as there is a one to one correspondence.
      TracksFit out{Event::Enum::Track::Type::Long,
                    Event::Enum::Track::FitHistory::VeloKalman,
                    false,
                    unique_id_gen,
                    tracksFT.zipIdentifier(),
                    LHCb::getMemResource( evtCtx )};
      out.reserve( tracksFT.size() );
      m_nbTracksCounter += tracksFT.size();

      for ( auto const& track : tracksFT.simd() ) {
        auto       loop_mask = track.loop_mask();
        auto const idxVP     = track.trackVP();
        auto const qop       = track.qOverP();

        auto [stateInfo, chi2, nDof] = fitBackwardWithMomentum(
            loop_mask, tracksVP, idxVP, qop, hits, Event::Enum::State::Location::ClosestToBeam, region.avgPosition );

        auto outTrack = out.emplace_back( tracksFT.size() );

        outTrack.field<LHCb::Event::v3::Tag::trackVP>().set( track.trackVP() );
        outTrack.field<LHCb::Event::v3::Tag::trackUT>().set( track.trackUT() );
        outTrack.field<LHCb::Event::v3::Tag::trackSeed>().set( track.indices() );
        outTrack.field<LHCb::Event::v3::Tag::Chi2>().set( chi2 );
        outTrack.field<LHCb::Event::v3::Tag::nDoF>().set( nDof );
        outTrack.field<LHCb::Event::v3::Tag::history>().set( tracksFT.history() );
        outTrack.field<LHCb::Event::v3::Tag::UniqueID>().set(
            decltype( outTrack.field<LHCb::Event::v3::Tag::UniqueID>().get() ){unique_id_gen.generate<int>().value()} );

        // Copy LHCbIds
        auto nVPHits   = track.nVPHits();
        auto VPHitsOut = outTrack.field<LHCb::Event::v3::Tag::VPHits>();
        VPHitsOut.resize( nVPHits );
        for ( int i = 0; i < nVPHits.hmax( loop_mask ); i++ ) {
          VPHitsOut[i].template field<LHCb::Event::v3::Tag::LHCbID>().set( track.vp_lhcbID( i ) );
        }

        auto nUTHits   = track.nUTHits();
        auto UTHitsOut = outTrack.field<LHCb::Event::v3::Tag::UTHits>();
        UTHitsOut.resize( nUTHits );
        for ( int i = 0; i < nUTHits.hmax( loop_mask ); i++ ) {
          UTHitsOut[i].template field<LHCb::Event::v3::Tag::LHCbID>().set( track.ut_lhcbID( i ) );
        }

        auto nFTHits   = track.nFTHits();
        auto FTHitsOut = outTrack.field<LHCb::Event::v3::Tag::FTHits>();
        FTHitsOut.resize( nFTHits );
        for ( int i = 0; i < nFTHits.hmax( loop_mask ); i++ ) {
          FTHitsOut[i].template field<LHCb::Event::v3::Tag::LHCbID>().set( track.ft_lhcbID( i ) );
        }

        // Closest to beam
        outTrack.field<LHCb::Event::v3::Tag::States>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .setPosition( stateInfo.pos().x(), stateInfo.pos().y(), stateInfo.pos().z() );
        outTrack.field<LHCb::Event::v3::Tag::States>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .setDirection( stateInfo.dir().x(), stateInfo.dir().y() );
        outTrack.field<LHCb::Event::v3::Tag::States>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .setQOverP( qop );
        outTrack
            .field<LHCb::Event::v3::Tag::StateCovs>()[outTrack.state_index( TracksFit::StateLocation::ClosestToBeam )]
            .set( stateInfo.covX().x(), 0, stateInfo.covX().y(), 0, 0, stateInfo.covY().x(), 0, stateInfo.covY().y(), 0,
                  stateInfo.covX().z(), 0, 0, stateInfo.covY().z(), 0, 0.1 * qop * qop );
      }

      return out;
    };

  private:
    mutable Gaudi::Accumulators::SummingCounter<> m_nbTracksCounter{this, "Nb of Produced Tracks"};
  };
} // namespace LHCb::Pr::Velo

DECLARE_COMPONENT_WITH_ID( LHCb::Pr::Velo::Kalman, "VeloKalman" )
