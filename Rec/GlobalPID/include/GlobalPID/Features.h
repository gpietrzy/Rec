/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ProtoParticle.h"
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/VectorizedML/Feature.h"

namespace ProbNN::Features {
  // Track features
  struct TrackChi2PerDoF : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->track() ? proto->track()->chi2PerDoF() : 0.f;
    }

  private:
    static constexpr auto m_name = "TrackChi2PerDoF";
  };

  struct TrackGhostProb : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->track() ? proto->track()->ghostProbability() : 1.f;
    }

  private:
    static constexpr auto m_name = "TrackGhostProb";
  };

  // Calo features
  struct InEcal : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->InEcal() : 0.f;
    }

  private:
    static constexpr auto m_name = "InEcal";
  };

  struct ElectronShowerEoP : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->ElectronShowerEoP() : 0.f;
    }

  private:
    static constexpr auto m_name = "ElectronShowerEoP";
  };

  struct ElectronShowerDLL : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->ElectronShowerDLL() : 0.f;
    }

  private:
    static constexpr auto m_name = "ElectronShowerDLL";
  };

  struct ElectronHypoEoP : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid && proto->track() && proto->track()->p() > 0. ? pid->ElectronEnergy() / proto->track()->p() : 0.f;
    }

  private:
    static constexpr auto m_name = "ElectronHypoEoP";
  };

  struct LogElectronHypoMatch : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      // shape out of bounds values a bit to make it more gentle for NNs
      // not having a match is not electron-like, so use max_chi2 as not available value
      float constexpr max_chi2 = 10000.f;
      auto  pid                = proto->caloChargedPID();
      float chi2               = pid ? pid->ElectronMatch() : -1.f;
      return LHCb::Math::fast_log( ( chi2 > 0. && chi2 < max_chi2 ) ? chi2 : max_chi2 );
    }

  private:
    static constexpr auto m_name = "LogElectronHypoMatch";
  };

  struct EcalPIDe : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->EcalPIDe() : 0.f;
    }

  private:
    static constexpr auto m_name = "EcalPIDe";
  };

  struct EcalPIDmu : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->EcalPIDmu() : 0.f;
    }

  private:
    static constexpr auto m_name = "EcalPIDmu";
  };

  struct InHcal : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->InHcal() : 0.f;
    }

  private:
    static constexpr auto m_name = "InHcal";
  };

  struct HcalEoP : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->HcalEoP() : 0.f;
    }

  private:
    static constexpr auto m_name = "HcalEoP";
  };

  struct HcalPIDe : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->HcalPIDe() : 0.f;
    }

  private:
    static constexpr auto m_name = "HcalPIDe";
  };

  struct HcalPIDmu : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->caloChargedPID();
      return pid ? pid->HcalPIDmu() : 0.f;
    }

  private:
    static constexpr auto m_name = "HcalPIDmu";
  };

  // Brem features
  struct InBrem : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->bremInfo();
      return pid ? pid->InBrem() : 0.f;
    }

  private:
    static constexpr auto m_name = "InBrem";
  };

  struct BremPIDe : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->bremInfo();
      return pid ? pid->BremPIDe() : 0.f;
    }

  private:
    static constexpr auto m_name = "BremPIDe";
  };

  // Rich features
  struct RichDLLe : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->richPID() ? proto->richPID()->scaledDLLForCombDLL( Rich::ParticleIDType::Electron ) : 0.f;
    }

  private:
    static constexpr auto m_name = "RichDLLe";
  };

  struct RichDLLmu : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->richPID() ? proto->richPID()->scaledDLLForCombDLL( Rich::ParticleIDType::Muon ) : 0.f;
    }

  private:
    static constexpr auto m_name = "RichDLLmu";
  };

  struct RichDLLk : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->richPID() ? proto->richPID()->scaledDLLForCombDLL( Rich::ParticleIDType::Kaon ) : 0.f;
    }

  private:
    static constexpr auto m_name = "RichDLLk";
  };

  struct RichDLLp : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->richPID() ? proto->richPID()->scaledDLLForCombDLL( Rich::ParticleIDType::Proton ) : 0.f;
    }

  private:
    static constexpr auto m_name = "RichDLLp";
  };

  struct RichDLLbt : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->richPID() ? proto->richPID()->scaledDLLForCombDLL( Rich::ParticleIDType::BelowThreshold ) : 0.f;
    }

  private:
    static constexpr auto m_name = "RichDLLbt";
  };

  struct usedRich1Gas : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->richPID();
      return pid ? pid->usedRich1Gas() : 0;
    }

  private:
    static constexpr auto m_name = "usedRich1Gas";
  };

  struct usedRich2Gas : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->richPID();
      return pid ? pid->usedRich2Gas() : 0;
    }

  private:
    static constexpr auto m_name = "usedRich2Gas";
  };

  // Muon features
  struct InMuon : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->muonPID() ? proto->muonPID()->InAcceptance() : 0.f;
    }

  private:
    static constexpr auto m_name = "InMuon";
  };

  struct IsMuon : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->muonPID() ? proto->muonPID()->IsMuon() : 0.f;
    }

  private:
    static constexpr auto m_name = "IsMuon";
  };

  struct IsMuonTight : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      return proto->muonPID() ? proto->muonPID()->IsMuonTight() : 0.f;
    }

  private:
    static constexpr auto m_name = "IsMuonTight";
  };

  struct LogMuonChi2 : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      // shape out of bounds values a bit to make it more gentle for NNs
      // high chi2 is not muon like, just like no having IsMuon,
      // so out of bounds is set to max chi2, use in combination with `InMuon`
      float constexpr max_chi2 = 500.f;
      auto  pid                = proto->muonPID();
      float chi2               = pid ? pid->chi2Corr() : -1.f;
      return LHCb::Math::fast_log( ( chi2 > 0. && chi2 < max_chi2 ) ? chi2 : max_chi2 );
    }

  private:
    static constexpr auto m_name = "LogMuonChi2";
  };

  struct MuonLLMu : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->muonPID();
      return pid ? pid->MuonLLMu() : 0.f;
    }

  private:
    static constexpr auto m_name = "MuonLLMu";
  };

  struct MuonLLBg : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->muonPID();
      return pid ? pid->MuonLLBg() : 0.f;
    }

  private:
    static constexpr auto m_name = "MuonLLBg";
  };

  struct MuonCatBoost : LHCb::VectorizedML::Feature {
    const char* name() const { return m_name; }
    float       operator()( LHCb::ProtoParticle const* proto ) const {
      auto pid = proto->muonPID();
      // looks like a DLL value, hence these out of bounds values, using not -1000, more gentle for NNs
      return pid ? ( pid->IsMuon() ? proto->muonPID()->muonMVA2() : -4.f ) : 0.f;
    }

  private:
    static constexpr auto m_name = "MuonCatBoost";
  };

} // namespace ProbNN::Features
