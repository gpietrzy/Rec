/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IFileAccess.h"
#include "GlobalPID/ProbNNs.h"
#include "LHCbAlgs/Consumer.h"

namespace {
  template <typename KeyedObject>
  LHCb::MCParticle const* mcTruth( KeyedObject const* obj, LHCb::MCParticles const& mcparts,
                                   LHCb::LinksByKey const& links ) {
    LHCb::MCParticle const* mcp{nullptr};
    if ( obj ) {
      links.applyToLinks( obj->key(), [&mcp, &mcparts]( unsigned int, unsigned int mcpkey, float ) {
        if ( !mcp ) mcp = static_cast<LHCb::MCParticle const*>( mcparts.containedObject( mcpkey ) );
      } );
    }
    return mcp;
  }
} // namespace

template <typename ModelEvaluator, typename InputObjects>
class ProbNNTrainingTupleAlg
    : public LHCb::Algorithm::Consumer<void( InputObjects const&, LHCb::MCParticles const&, LHCb::LinksByKey const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>> {
public:
  using base_type =
      LHCb::Algorithm::Consumer<void( InputObjects const&, LHCb::MCParticles const&, LHCb::LinksByKey const& ),
                                LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>>;
  using KeyValue = typename base_type::KeyValue;

  ProbNNTrainingTupleAlg( std::string const& name, ISvcLocator* pSvc )
      : base_type{name, pSvc, {KeyValue{"InputObjects", ""}, KeyValue{"MCParticles", ""}, KeyValue{"LinksToMC", ""}}} {}

  StatusCode initialize() override {
    auto sc = base_type::initialize();
    if ( sc.isFailure() ) return sc;
    if ( evaluate_model ) {
      // for loading weights file
      auto buffer = m_filesvc->read( m_weightsfilename.value() );
      sc &= m_model.load( buffer );
    }
    return sc;
  }

  void operator()( InputObjects const& objs, LHCb::MCParticles const& mcparts,
                   LHCb::LinksByKey const& links ) const override {
    auto       tuple = this->nTuple( ( m_modelname.value() == "" ? m_modelname.value() + "" : "/" ) + "TrainingTuple" );
    StatusCode sc    = StatusCode::SUCCESS;
    for ( auto const obj : objs ) {
      // write features first
      LHCb::Utils::unwind<0, ModelEvaluator::ModelType::InputVec::size>( [&]( auto k ) {
        auto const feature = m_model.features()->template get<k>();
        sc &= tuple->column( feature.name(), feature( obj ) );
      } );
      // further selection options after (MC truth, ...)
      auto                    mcp     = mcTruth( obj, mcparts, links );
      LHCb::MCParticle const* mother  = mcp ? mcp->mother() : nullptr;
      LHCb::MCParticle const* gmother = mother ? mother->mother() : nullptr;
      sc &= tuple->column( "MC_TRUEID", mcp ? mcp->particleID().pid() : 0 );
      sc &= tuple->column( "MC_MOTHER_ID", mother ? mother->particleID().pid() : 0 );
      sc &= tuple->column( "MC_GMOTHER_ID", gmother ? gmother->particleID().pid() : 0 );
      sc &= tuple->column( "MC_TRUE_P", mcp ? mcp->momentum().P() : 0.f );
      sc &= tuple->column( "MC_TRUE_PT", mcp ? mcp->momentum().Pt() : 0.f );
      sc &= tuple->column( "MC_TRUE_eta", mcp ? mcp->momentum().Eta() : 0.f );
      sc &= tuple->column( "MC_TRUE_phi", mcp ? mcp->momentum().Phi() : -10.f );
      sc &= tuple->column( "MC_Signal", mcp ? mcp->fromSignal() : 0 );
      // if available evaluate (old) model
      if ( evaluate_model ) {
        auto invec = typename ModelEvaluator::ModelType::InputVec();
        LHCb::Utils::unwind<0, ModelEvaluator::ModelType::InputVec::size>( [&]( auto k ) {
          auto const feature = m_model.features()->template get<k>();
          invec( k )         = typename ModelEvaluator::ModelType::FType( feature( obj ) );
        } );
        auto output  = m_model.model()->evaluate( invec );
        auto out_arr = SIMDWrapper::to_array( output( 0 ) );
        sc &= tuple->column( "model_output", out_arr[0] );
      }
      // write out row
      sc.andThen( [&] { return tuple->write(); } ).ignore();
    }
  };

private:
  // properties
  Gaudi::Property<std::string> m_modelname{this, "ModelName", ""};
  Gaudi::Property<std::string> m_weightsfilename{this, "WeightsFileName", "",
                                                 [&]( auto& ) { evaluate_model = m_weightsfilename.value() != ""; }};
  bool                         evaluate_model;

  // services
  ServiceHandle<IFileAccess> m_filesvc{this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents"};

  // data members
  ModelEvaluator m_model;
};

using ProbNNElectronTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Long::Electron::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNElectronTrainingTupleAlg, "ProbNNElectronTrainingTupleAlg" )

using ProbNNMuonTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Long::Muon::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNMuonTrainingTupleAlg, "ProbNNMuonTrainingTupleAlg" )

using ProbNNPionTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Long::Pion::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNPionTrainingTupleAlg, "ProbNNPionTrainingTupleAlg" )

using ProbNNKaonTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Long::Kaon::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNKaonTrainingTupleAlg, "ProbNNKaonTrainingTupleAlg" )

using ProbNNProtonTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Long::Proton::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNProtonTrainingTupleAlg, "ProbNNProtonTrainingTupleAlg" )

using ProbNNGhostTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Long::Ghost::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNGhostTrainingTupleAlg, "ProbNNGhostTrainingTupleAlg" )

// for reduced upstream features (reduced w.r.t long/downstream)
using ProbNNElectronUpstreamTrainingTupleAlg =
    ProbNNTrainingTupleAlg<ProbNN::Upstream::Electron::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNElectronUpstreamTrainingTupleAlg, "ProbNNElectronUpstreamTrainingTupleAlg" )

using ProbNNMuonUpstreamTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Upstream::Muon::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNMuonUpstreamTrainingTupleAlg, "ProbNNMuonUpstreamTrainingTupleAlg" )

using ProbNNPionUpstreamTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Upstream::Pion::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNPionUpstreamTrainingTupleAlg, "ProbNNPionUpstreamTrainingTupleAlg" )

using ProbNNKaonUpstreamTrainingTupleAlg = ProbNNTrainingTupleAlg<ProbNN::Upstream::Kaon::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNKaonUpstreamTrainingTupleAlg, "ProbNNKaonUpstreamTrainingTupleAlg" )

using ProbNNProtonUpstreamTrainingTupleAlg =
    ProbNNTrainingTupleAlg<ProbNN::Upstream::Proton::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNProtonUpstreamTrainingTupleAlg, "ProbNNProtonUpstreamTrainingTupleAlg" )

using ProbNNGhostUpstreamTrainingTupleAlg =
    ProbNNTrainingTupleAlg<ProbNN::Upstream::Ghost::Model, LHCb::ProtoParticles>;
DECLARE_COMPONENT_WITH_ID( ProbNNGhostUpstreamTrainingTupleAlg, "ProbNNGhostUpstreamTrainingTupleAlg" )
