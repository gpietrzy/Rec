/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <iostream>
#include <map>
#include <memory>
#include <sstream>
#include <string>

#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>

#include "TCanvas.h"
#include "TCut.h"
#include "TFile.h"
#include "TH1F.h"
#include "TPad.h"
#include "TTree.h"

void MakeRichPlots() {

  const std::string imageType = "pdf";

  const std::string dir = "/usera/jonesc/LHCb/output/U2/InclB/WithSpill";

  const std::string fName = "RichFuture-ProtoTuple.root";

#include "utils.C"

  const std::string scopeN = "Scope3";
  for ( const std::string SiPMTW : {"PhoTW-0.150", "PhoTW-0.075"} ) {
    for ( const auto scenario : {"3.0-Value", "2.0-MidRange", "2.1-MidRange", "1.0-Finest"} ) {
      for ( const std::string lumi : {"1.5e34", "1.2e34", "1.0e34"} ) {
        // for ( const auto scenario : {"1.0-Finest"} ) {
        // for ( const std::string lumi : {"1.2e34"} ) {
        findAll( scopeN + "-" + SiPMTW + "-" + lumi, dir + "/" + scopeN + "/" + scenario, {lumi, SiPMTW},
                 scopeN + "-" + scenario + "-" + lumi + "-" + SiPMTW );
      }
    }
  }

  for ( auto& [dname, tags] : dataSets ) {
    for ( auto& [nametag, label] : tags ) {

      // load the file and TTree
      auto f = TFile::Open( ( dir + "/" + nametag + "/" + fName ).c_str() );
      if ( !f ) continue;
      auto tree = (TTree*)gDirectory->Get( "ChargedProtoTuple/protoPtuple" );
      if ( !tree ) continue;

      TCut detOK = "RichUsedAero || RichUsedR1Gas || RichUsedR2Gas";

      std::string trackSelS = "TrackType==3 && TrackP>3000 && TrackP<120000 && TrackChi2PerDof<5";
      // trackSelS += " && TrackGhostProb<0.9"; // add ghost prob cut
      // trackSelS += " && TrackLikelihood>-40"; // Likelihood
      // trackSelS += " && (TrackCloneDist>5000 || TrackCloneDist<0)"; // clone distance
      TCut trackSel = trackSelS.c_str();

      TCut realPr = "abs(MCParticleType) == 2212";
      TCut realKa = "abs(MCParticleType) == 321";
      TCut realPi = "abs(MCParticleType) == 211";

      TCut prAboveThres = "RichAbovePrThres";
      TCut kaAboveThres = "RichAboveKaThres";
      TCut piAboveThres = "RichAbovePiThres";

      boost::replace_all( label, "/", "-" );

      auto c = std::make_unique<TCanvas>( label.c_str(), label.c_str(), 1400, 1000 );

      gStyle->SetOptStat( 0 );
      // gStyle->SetStatY(0.9);
      // gStyle->SetStatX(0.9);
      // gStyle->SetStatW(0.1);
      // gStyle->SetStatH(0.02);

      const std::vector<double> cuts = {-10, -8, -6, -4, -2, 0, 2, 4, 6, 8, 10};
      // const std::vector<double> cuts = {0};

      // Open a PDF file
      const std::string pdfFile = label + "-RichPID." + imageType;
      c->SaveAs( ( pdfFile + "[" ).c_str() );

      // Kaon ID plots
      for ( const auto& cut : cuts ) {
        std::ostringstream cC;
        cC << cut;

        tree->Draw( ( "(RichDLLk>" + cC.str() + "?100:0):TrackP>>kIDEff" ).c_str(),
                    detOK && realKa && trackSel && piAboveThres, "prof" );
        tree->Draw( ( "(RichDLLk>" + cC.str() + "?100:0):TrackP>>piMisIDEff" ).c_str(),
                    detOK && realPi && trackSel && piAboveThres, "prof" );
        auto kIDEff     = (TH1F*)gDirectory->Get( "kIDEff" );
        auto piMisIDEff = (TH1F*)gDirectory->Get( "piMisIDEff" );

        if ( kIDEff && piMisIDEff ) {
          // kIDEff->SetTitle( ( "DLLk>" + cC.str() + " && " + trackSelS ).c_str() );
          kIDEff->SetTitle( "Kaon ID Versus Pion MisID" );
          kIDEff->GetXaxis()->SetTitle( "Momentum / MeV/c" );
          kIDEff->GetYaxis()->SetTitle( "Efficiency / %" );
          kIDEff->Draw();
          kIDEff->SetMarkerColor( kRed );
          kIDEff->SetLineColor( kRed );

          piMisIDEff->Draw( "SAME" );
          piMisIDEff->SetMarkerColor( kBlue );
          piMisIDEff->SetLineColor( kBlue );

          auto leg = std::make_unique<TLegend>( 0.15, 0.45, 0.35, 0.55 );
          leg->SetHeader( ( "RichDLLk > " + cC.str() ).c_str() );
          leg->SetTextSize( 0.015 );
          leg->SetBorderSize( 0 );
          leg->SetEntrySeparation( 0.02 );
          leg->AddEntry( kIDEff, "Kaon ID Eff" );
          leg->AddEntry( piMisIDEff, "Pion MisID Eff" );
          leg->Draw();

          c->SaveAs( pdfFile.c_str() );
        } else {
          std::cerr << "Error making kaon plots" << std::endl;
        }
      }

      // Proton ID plots
      for ( const auto& cut : cuts ) {
        std::ostringstream cC;
        cC << cut;

        tree->Draw( ( "(RichDLLp>" + cC.str() + "?100:0):TrackP>>prIDEff" ).c_str(),
                    detOK && realPr && trackSel && piAboveThres, "prof" );
        tree->Draw( ( "(RichDLLp>" + cC.str() + "?100:0):TrackP>>kMisIDEff" ).c_str(),
                    detOK && realPi && trackSel && piAboveThres, "prof" );
        auto prIDEff    = (TH1F*)gDirectory->Get( "prIDEff" );
        auto piMisIDEff = (TH1F*)gDirectory->Get( "kMisIDEff" );

        if ( prIDEff && piMisIDEff ) {
          // prIDEff->SetTitle( ( "DLLp>" + cC.str() + " && " + trackSelS ).c_str() );
          prIDEff->SetTitle( "Proton ID Versus Pion MisID" );
          prIDEff->GetXaxis()->SetTitle( "Momentum / MeV/c" );
          prIDEff->GetYaxis()->SetTitle( "Efficiency / %" );
          prIDEff->Draw();
          prIDEff->SetMarkerColor( kRed );
          prIDEff->SetLineColor( kRed );

          piMisIDEff->Draw( "SAME" );
          piMisIDEff->SetMarkerColor( kBlue );
          piMisIDEff->SetLineColor( kBlue );

          auto leg = std::make_unique<TLegend>( 0.15, 0.45, 0.35, 0.55 );
          leg->SetHeader( ( "RichDLLp > " + cC.str() ).c_str() );
          leg->SetTextSize( 0.015 );
          leg->SetBorderSize( 0 );
          leg->SetEntrySeparation( 0.02 );
          leg->AddEntry( prIDEff, "Proton ID Eff" );
          leg->AddEntry( piMisIDEff, "Pion MisID Eff" );
          leg->Draw();

          c->SaveAs( pdfFile.c_str() );
        } else {
          std::cerr << "Error making proton plots" << std::endl;
        }
      }

      // close the PDF file
      c->SaveAs( ( pdfFile + "]" ).c_str() );

      // clean up
      f->Close();
    }
  }
}
