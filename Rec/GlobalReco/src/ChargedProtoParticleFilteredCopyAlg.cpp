/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MuonPID.h"
#include "Event/ProtoParticle.h"
#include "Event/RichPID.h"
#include "Event/Track.h"
#include "Functors/Function.h"
#include "Functors/with_functors.h"
#include "LHCbAlgs/Transformer.h"

namespace {
  using OutData =
      std::tuple<LHCb::ProtoParticles, LHCb::Tracks, LHCb::RichPIDs, LHCb::MuonPIDs, LHCb::Tracks,
                 LHCb::Event::Calo::v1::CaloChargedPIDs, LHCb::Event::Calo::v1::BremInfos, LHCb::GlobalChargedPIDs>;

  struct ProtoParticlePredicate {
    using Signature                    = bool( const LHCb::ProtoParticle& );
    static constexpr auto PropertyName = "ProtoParticlePredicate";
  };

  struct TrackPredicate {
    using Signature                    = bool( const LHCb::Track& );
    static constexpr auto PropertyName = "TrackPredicate";
  };

} // namespace

namespace LHCb {

  class ChargedProtoParticleFilteredCopyAlg
      : public with_functors<LHCb::Algorithm::MultiTransformer<OutData( ProtoParticle::Range const& )>,
                             ProtoParticlePredicate, TrackPredicate> {
  public:
    using KeyValue = ChargedProtoParticleFilteredCopyAlg::KeyValue;

    ChargedProtoParticleFilteredCopyAlg( const std::string& name, ISvcLocator* pSvc )
        : with_functors( name, pSvc, {KeyValue( "InputProtos", "" )},
                         {KeyValue( "OutputProtos", "" ), KeyValue( "OutputTracks", "" ),
                          KeyValue( "OutputRichPIDs", "" ), KeyValue( "OutputMuonPIDs", "" ),
                          KeyValue( "OutputMuonTracks", "" ), KeyValue( "OutputCaloChargedPIDs", "" ),
                          KeyValue( "OutputBremInfos", "" ), KeyValue( "OutputGlobalChargedPIDs", "" )} ) {}

    OutData operator()( ProtoParticle::Range const& ) const override;

  private:
    Gaudi::Property<unsigned short int> m_rpidversion{this, "RichPIDVersion", 2u, "Version of the RichPID container"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_proto_with_no_track{
        this, "Charged ProtoParticle with no track found. Ignoring"};
  };

} // namespace LHCb

OutData LHCb::ChargedProtoParticleFilteredCopyAlg::operator()( ProtoParticle::Range const& input_protos ) const {
  OutData result;
  auto& [out_protos, out_tracks, out_rpids, out_mpids, out_mtracks, out_cpids, out_binfos, out_gpids] = result;

  // set upper limit on container size/capacity
  auto max_size            = input_protos.size();
  auto constexpr n_results = std::tuple_size<OutData>();
  LHCb::Utils::unwind<0, n_results>( [&]( auto i ) { std::get<i>( result ).reserve( max_size ); } );

  // set data object versions where necessary
  out_rpids.setVersion( (unsigned char)m_rpidversion );

  auto const& proto_pred = getFunctor<ProtoParticlePredicate>();
  auto const& track_pred = getFunctor<TrackPredicate>();

  for ( const auto* input_proto : input_protos ) {
    // sanity check, only for charged protos
    const auto* input_track = input_proto->track();
    if ( !input_track ) {
      ++m_proto_with_no_track;
      continue;
    }
    // filter proto
    if ( !track_pred( *input_track ) || !proto_pred( *input_proto ) ) continue;

    // now copy contents
    auto* out_proto = new LHCb::ProtoParticle( *input_proto );

    // reset track info
    auto const track_key = input_track->key();
    out_tracks.insert( new LHCb::Track( *input_track ), track_key );
    auto const out_track = out_tracks.object( track_key );
    out_proto->setTrack( out_track );

    // copy RICH info if available
    auto const* input_rpid = input_proto->richPID();
    if ( input_rpid ) {
      auto* out_rpid = new LHCb::RichPID( *input_rpid );
      out_rpid->setTrack( out_track );
      auto const rpid_key = input_rpid->key();
      out_rpids.insert( out_rpid, rpid_key );
      out_proto->setRichPID( out_rpids.object( rpid_key ) );
    }

    // copy Muon info if available
    auto const* input_mpid = input_proto->muonPID();
    if ( input_mpid ) {
      auto* out_mpid = new LHCb::MuonPID( *input_mpid );
      out_mpid->setIDTrack( out_track );
      auto const* input_mtrack = input_mpid->muonTrack();
      if ( input_mtrack ) {
        auto const mtrack_key = input_mtrack->key();
        out_mtracks.insert( new LHCb::Track( *input_mtrack ), mtrack_key );
        out_mpid->setMuonTrack( out_mtracks.object( mtrack_key ) );
      }
      auto const mpid_key = input_mpid->key();
      out_mpids.insert( out_mpid, mpid_key );
      out_proto->setMuonPID( out_mpids.object( mpid_key ) );
    }

    // copy brem, calochargedpid and global charged pid objects if available
    auto const* input_binfo = input_proto->bremInfo();
    if ( input_binfo ) {
      auto out_binfo = std::make_unique<LHCb::Event::Calo::v1::BremInfo>( *input_binfo );
      out_binfo->setIDTrack( out_track );
      out_proto->setBremInfo( out_binfo.get() );
      out_binfos.insert( out_binfo.release() );
    }

    auto const* input_cpid = input_proto->caloChargedPID();
    if ( input_cpid ) {
      auto out_cpid = std::make_unique<LHCb::Event::Calo::v1::CaloChargedPID>( *input_cpid );
      out_cpid->setIDTrack( out_track );
      out_proto->setCaloChargedPID( out_cpid.get() );
      out_cpids.insert( out_cpid.release() );
    }

    auto const* input_gpid = input_proto->globalChargedPID();
    if ( input_gpid ) {
      auto out_gpid = std::make_unique<LHCb::GlobalChargedPID>( *input_gpid );
      out_gpid->setIDTrack( out_track );
      out_proto->setGlobalChargedPID( out_gpid.get() );
      out_gpids.insert( out_gpid.release() );
    }

    // NOTE: calo info to be added externally, as electron/photon containers are unpacked separately

    // move to containers
    out_protos.insert( out_proto, input_proto->key() );
  }

  return result;
}

DECLARE_COMPONENT_WITH_ID( LHCb::ChargedProtoParticleFilteredCopyAlg, "ChargedProtoParticleFilteredCopyAlg" )
