/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @class ChargedProtoParticleTupleAlg ChargedProtoParticleTupleAlg.h
 *
 * Implementation file for algorithm ChargedProtoParticleTupleAlg
 *  Simple algorithm to produce an ntuple containing the charged ProtoParticle
 *  PID information. Useful for tuning purposes.
 *
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 2006-11-15
 */

#include "Event/MCParticle.h"
#include "Event/MuonPID.h"
#include "Event/ProtoParticle.h"
#include "Event/RichPID.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "MCInterfaces/IRichMCTruthTool.h"

class ChargedProtoParticleTupleAlg final
    : public LHCb::Algorithm::Consumer<void( LHCb::ProtoParticles const& ),
                                       Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  /// Standard constructor
  ChargedProtoParticleTupleAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {"ProtoParticleLocation", LHCb::ProtoParticleLocation::Charged}} {
    // Turn off Tuple printing during finalize
    setProperty( "NTuplePrint", false ).ignore();
  }

  void operator()( const LHCb::ProtoParticles& ) const override; ///< Algorithm execution

private:
  /// Use RICH tool to get MCParticle associations for Tracks (To avoid Linker details)
  ToolHandle<Rich::MC::IMCTruthTool> m_truth{this, "MCTruthTool", "Rich::MC::MCTruthTool/MCTruth"};
};

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( ChargedProtoParticleTupleAlg )

//=============================================================================
// Main execution
//=============================================================================
void ChargedProtoParticleTupleAlg::operator()( const LHCb::ProtoParticles& protos ) const {

  // Loop over the protos
  for ( const auto* proto : protos ) {

    // Check this is a charged track ProtoParticle
    const auto* track = proto->track();
    if ( !track ) continue;

    // make a tuple
    Tuple tuple = nTuple( "protoPtuple", "ProtoParticle PID Information" );

    // Status Code for filling the ntuple
    StatusCode sc = StatusCode::SUCCESS;

    // reco variables

    // some track info
    if ( sc ) sc = tuple->column( "TrackP", track->p() );
    if ( sc ) sc = tuple->column( "TrackPt", track->pt() );
    if ( sc ) sc = tuple->column( "TrackChi2PerDof", track->chi2PerDoF() );
    if ( sc ) sc = tuple->column( "TrackNumDof", track->nDoF() );
    if ( sc ) sc = tuple->column( "TrackType", static_cast<int>( track->type() ) );
    if ( sc ) sc = tuple->column( "TrackHistory", static_cast<int>( track->history() ) );
    if ( sc ) sc = tuple->column( "TrackGhostProb", track->ghostProbability() );
    if ( sc ) sc = tuple->column( "TrackLikelihood", track->likelihood() );
    if ( sc ) sc = tuple->column( "TrackCloneDist", track->info( LHCb::Track::AdditionalInfo::CloneDist, 9e10 ) );

    // rich
    const auto rPID = proto->richPID();
    if ( sc ) sc = tuple->column( "RichDLLe", ( rPID ? rPID->particleDeltaLL( Rich::Electron ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLmu", ( rPID ? rPID->particleDeltaLL( Rich::Muon ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLpi", ( rPID ? rPID->particleDeltaLL( Rich::Pion ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLk", ( rPID ? rPID->particleDeltaLL( Rich::Kaon ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLp", ( rPID ? rPID->particleDeltaLL( Rich::Proton ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLd", ( rPID ? rPID->particleDeltaLL( Rich::Deuteron ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichDLLbt", ( rPID ? rPID->particleDeltaLL( Rich::BelowThreshold ) : 0 ) );
    if ( sc ) sc = tuple->column( "RichUsedAero", ( rPID ? rPID->usedAerogel() : false ) );
    if ( sc ) sc = tuple->column( "RichUsedR1Gas", ( rPID ? rPID->usedRich1Gas() : false ) );
    if ( sc ) sc = tuple->column( "RichUsedR2Gas", ( rPID ? rPID->usedRich2Gas() : false ) );
    if ( sc ) sc = tuple->column( "RichAboveElThres", ( rPID ? rPID->electronHypoAboveThres() : false ) );
    if ( sc ) sc = tuple->column( "RichAboveMuThres", ( rPID ? rPID->muonHypoAboveThres() : false ) );
    if ( sc ) sc = tuple->column( "RichAbovePiThres", ( rPID ? rPID->pionHypoAboveThres() : false ) );
    if ( sc ) sc = tuple->column( "RichAboveKaThres", ( rPID ? rPID->kaonHypoAboveThres() : false ) );
    if ( sc ) sc = tuple->column( "RichAbovePrThres", ( rPID ? rPID->protonHypoAboveThres() : false ) );
    if ( sc ) sc = tuple->column( "RichAboveDeThres", ( rPID ? rPID->deuteronHypoAboveThres() : false ) );
    if ( sc ) sc = tuple->column( "RichBestPID", ( rPID ? static_cast<int>( rPID->bestParticleID() ) : -1 ) );

    // muon
    const auto mPID = proto->muonPID();
    if ( sc ) sc = tuple->column( "MuonBkgLL", ( mPID ? mPID->MuonLLBg() : 0 ) );
    if ( sc ) sc = tuple->column( "MuonMuLL", ( mPID ? mPID->MuonLLMu() : 0 ) );
    if ( sc ) sc = tuple->column( "MuonNShared", ( mPID ? mPID->nShared() : 0 ) );
    if ( sc ) sc = tuple->column( "MuonIsLooseMuon", ( mPID ? mPID->IsMuonLoose() : false ) );
    if ( sc ) sc = tuple->column( "MuonIsMuon", ( mPID ? mPID->IsMuon() : false ) );
    if ( sc ) sc = tuple->column( "MuonInAcc", ( mPID ? mPID->InAcceptance() : false ) );

    // calo
    if ( sc ) sc = tuple->column( "InAccSpd", proto->info( LHCb::ProtoParticle::additionalInfo::InAccSpd, false ) );
    if ( sc ) sc = tuple->column( "InAccPrs", proto->info( LHCb::ProtoParticle::additionalInfo::InAccPrs, false ) );
    if ( sc ) sc = tuple->column( "InAccEcal", proto->info( LHCb::ProtoParticle::additionalInfo::InAccEcal, false ) );
    if ( sc ) sc = tuple->column( "InAccHcal", proto->info( LHCb::ProtoParticle::additionalInfo::InAccHcal, false ) );
    if ( sc ) sc = tuple->column( "InAccBrem", proto->info( LHCb::ProtoParticle::additionalInfo::InAccBrem, false ) );
    if ( sc ) sc = tuple->column( "CaloTrMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrMatch, 0 ) );
    if ( sc )
      sc = tuple->column( "CaloElectronMatch",
                          proto->info( LHCb::ProtoParticle::additionalInfo::CaloElectronMatch, 0 ) );
    if ( sc )
      sc = tuple->column( "CaloBremMatch", proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, 0 ) );
    if ( sc )
      sc = tuple->column( "CaloChargedSpd", proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedSpd, 0 ) );
    if ( sc )
      sc = tuple->column( "CaloChargedPrs", proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedPrs, 0 ) );
    if ( sc )
      sc = tuple->column( "CaloChargedEcal", proto->info( LHCb::ProtoParticle::additionalInfo::CaloChargedEcal, 0 ) );
    if ( sc ) sc = tuple->column( "CaloEcalChi2", proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalChi2, 0 ) );
    if ( sc ) sc = tuple->column( "CaloClusChi2", proto->info( LHCb::ProtoParticle::additionalInfo::CaloClusChi2, 0 ) );
    if ( sc ) sc = tuple->column( "CaloBremChi2", proto->info( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, 0 ) );
    if ( sc ) sc = tuple->column( "CaloEcalE", proto->info( LHCb::ProtoParticle::additionalInfo::CaloEcalE, 0 ) );
    if ( sc ) sc = tuple->column( "CaloHcalE", proto->info( LHCb::ProtoParticle::additionalInfo::CaloHcalE, 0 ) );
    if ( sc )
      sc = tuple->column( "CaloTrajectoryL", proto->info( LHCb::ProtoParticle::additionalInfo::CaloTrajectoryL, 0 ) );
    if ( sc ) sc = tuple->column( "EcalPIDe", proto->info( LHCb::ProtoParticle::additionalInfo::EcalPIDe, 0 ) );
    if ( sc ) sc = tuple->column( "HcalPIDe", proto->info( LHCb::ProtoParticle::additionalInfo::HcalPIDe, 0 ) );
    if ( sc ) sc = tuple->column( "PrsPIDe", proto->info( LHCb::ProtoParticle::additionalInfo::PrsPIDe, 0 ) );
    if ( sc ) sc = tuple->column( "BremPIDe", proto->info( LHCb::ProtoParticle::additionalInfo::BremPIDe, 0 ) );
    if ( sc ) sc = tuple->column( "EcalPIDmu", proto->info( LHCb::ProtoParticle::additionalInfo::EcalPIDmu, 0 ) );
    if ( sc ) sc = tuple->column( "HcalPIDmu", proto->info( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, 0 ) );

    // combined DLLs
    if ( sc ) sc = tuple->column( "CombDLLe", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLe, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLmu", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLmu, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLpi", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLpi, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLk", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLk, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLp", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLp, 0 ) );
    if ( sc ) sc = tuple->column( "CombDLLd", proto->info( LHCb::ProtoParticle::additionalInfo::CombDLLd, 0 ) );

    // ANN PID Probabilities
    if ( sc ) sc = tuple->column( "ProbNNe", proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNe, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNmu", proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNmu, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNpi", proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNpi, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNk", proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNk, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNp", proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNp, -1 ) );
    if ( sc ) sc = tuple->column( "ProbNNghost", proto->info( LHCb::ProtoParticle::additionalInfo::ProbNNghost, -1 ) );

    // VeloCharge
    if ( sc ) sc = tuple->column( "VeloCharge", proto->info( LHCb::ProtoParticle::additionalInfo::VeloCharge, 0 ) );

    // MCParticle information
    const auto* mcPart = m_truth->mcParticle( track );
    if ( sc ) sc = tuple->column( "MCParticleType", mcPart ? mcPart->particleID().pid() : 0 );
    if ( sc ) sc = tuple->column( "MCParticleP", mcPart ? mcPart->p() : -99999 );
    if ( sc ) sc = tuple->column( "MCParticlePt", mcPart ? mcPart->pt() : -99999 );
    if ( sc ) sc = tuple->column( "MCVirtualMass", mcPart ? mcPart->virtualMass() : -99999 );

    // write the tuple for this protoP
    sc.andThen( [&] { return tuple->write(); } ).orThrow( "Failed to fill ntuple", "ChargedProtoParticleTupleAlg" );

  } // loop over protos
}

//=============================================================================
