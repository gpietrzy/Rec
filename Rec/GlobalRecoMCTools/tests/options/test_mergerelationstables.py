###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration .         #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test the MergeRelationsTablesPP2MCP algorithm.

Asserts that the list of input tables is merged into a single table.

Runs over Brunel output and creates a number of dummy
ProtoParticle-to-MCParticle relations, merges these using the algorithm, and
then checks that the merged table contains all the information present in the
individual tables.
"""
from __future__ import print_function
import random

from Configurables import (
    ApplicationMgr,
    LHCbApp,
    MergeRelationsTablesPP2MCP,
    UnpackMCParticle,
    UnpackProtoParticle,
)
import GaudiPython as GP
from GaudiPython.GaudiAlgs import GaudiAlgo, SUCCESS
from PRConfig.TestFileDB import test_file_db
from DDDB.CheckDD4Hep import UseDD4Hep

# Relations table type we will create
RelationWeighted1D = GP.gbl.LHCb.RelationWeighted1D(
    GP.gbl.LHCb.ProtoParticle, GP.gbl.LHCb.MCParticle, GP.gbl.double)


class MakeRelationsTables(GaudiAlgo):
    def __init__(self,
                 name,
                 protoparticles,
                 mc_particles,
                 output_tables,
                 nentries_per_table=5):
        """Instantiate a dummy relations table maker algorithm.

        Accepts ProtoParticle and MCParticle containers, and then creates a
        number of RelationsWeighted1D output tables which associate the
        former to the latter.

        Args:
            protoparticles (str): Location of a KeyedContainer of ProtoParticle objects.
            mc_particles (str): Location of a KeyedContainer of MCParticle objects.
            output_tables (list of str): Locations to put output relations tables.
            nentries_per_table (int): Number of relations to create per output table.
        """
        super(MakeRelationsTables, self).__init__(name)
        self.protoparticles = str(protoparticles)
        self.mc_particles = str(mc_particles)
        self.output_tables = output_tables
        self.nentries_per_table = nentries_per_table

    def execute(self):
        # Access the underlying vector so we can pick elements by index rather
        # than key, which we don't care about
        protos = self.get(self.protoparticles).containedObjects()
        mcparticles = self.get(self.mc_particles).containedObjects()

        ntables = len(self.output_tables)
        nentries = self.nentries_per_table * ntables
        # Check we have enough objects to create all relations
        assert protos.size() >= nentries and mcparticles.size() >= nentries

        # Create one table per nentries_per_table-length chunk of
        # ProtoParticles and MCParticles
        for offset, output_loc in enumerate(self.output_tables):
            table = RelationWeighted1D()
            for idx in range(offset * self.nentries_per_table,
                             (offset + 1) * self.nentries_per_table):
                pp = protos.at(idx)
                mcp = mcparticles.at(idx)
                status = table.relate(pp, mcp, random.uniform(0, 1))
                if status.isFailure():
                    self.error("Failed making a relation")
                    break
            self.put(table, output_loc)
        return SUCCESS


app = LHCbApp(EvtMax=50)
test_file_db["upgrade_minbias_hlt1_filtered"].run(configurable=app)
if UseDD4Hep:
    from Configurables import (
        LHCb__Det__LbDD4hep__IOVProducer as IOVProducer,
        LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc,
        LHCb__Tests__FakeRunNumberProducer as FET,
    )
    DD4hepSvc(DetectorList=[])
    cond_algs = [
        FET('FakeRunNumber', ODIN='DummyODIN', Start=0, Step=0),
        IOVProducer("ReserveIOVDD4hep", ODIN='DummyODIN'),
    ]
    # this is needed bacause of the way TopAlg is passed to
    # GaudiPython.AppMgr
    cond_algs = [alg.getFullJobOptName() for alg in cond_algs]
else:
    from Configurables import CondDB
    CondDB(Upgrade=True)
    cond_algs = []

unpack_mcp = UnpackMCParticle()
unpack_charged_pp = UnpackProtoParticle()

relations_locations = ["TableA", "TableB", "TableC"]
merger = MergeRelationsTablesPP2MCP(
    InputRelationsTables=relations_locations,
    Output="TableMerged",
)

mgr_config = ApplicationMgr(TopAlg=cond_algs + [unpack_mcp, unpack_charged_pp])

appMgr = GP.AppMgr()

# Have to create our algorithm after the AppMgr, then add it to the sequence by
# hand
maker = MakeRelationsTables(
    name="MakeRelationsTables",
    protoparticles=unpack_charged_pp.getProp("OutputName"),
    mc_particles=unpack_mcp.getProp("OutputName"),
    output_tables=relations_locations,
)
appMgr.setAlgorithms(mgr_config.TopAlg + [maker, merger])

TES = appMgr.evtsvc()
appMgr.run(1)

# Check that the merged table contains all relations from the input tables
i = 0
while TES["/Event"] and i < app.EvtMax:
    merged = TES[str(merger.getProp("Output"))]
    merged_rels = set()
    for rel in merged.relations():
        merged_rels.add((
            getattr(rel, "from")().index(),
            rel.to().index(),
            rel.weight(),
        ))

    table_rels = set()
    for loc in relations_locations:
        table = TES[loc]
        for rel in table.relations():
            table_rels.add((
                getattr(rel, "from")().index(),
                rel.to().index(),
                rel.weight(),
            ))

    assert merged_rels == table_rels
    i += 1
    appMgr.run(1)

print("SUCCESS")
