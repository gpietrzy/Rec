/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/RecSummary.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiAlg/ISequencerTimerTool.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/Memory.h"
#include "LHCbAlgs/Consumer.h"

#include <algorithm>
#include <atomic>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : TimingTuple
//
// 2010-08-18 : Patrick Koppenburg
//-----------------------------------------------------------------------------

/** @class TimingTuple TimingTuple.h
 *
 *  Fill a Tuple with timing, memory and some event variables
 *
 *  @author Patrick Koppenburg
 *  @date   2010-08-18
 */
class TimingTuple final : public LHCb::Algorithm::Consumer<void( const LHCb::ODIN&, const LHCb::RecSummary& ),
                                                           Gaudi::Functional::Traits::BaseClass_t<GaudiTupleAlg>> {

public:
  /// Standard constructor
  TimingTuple( const std::string& name, ISvcLocator* pSvcLocator );

  StatusCode initialize() override;                                                   ///< Algorithm initialization
  void       operator()( const LHCb::ODIN&, const LHCb::RecSummary& ) const override; ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_algos{
      this, "TimeAlgos", {}, "List of algorithm names to get time counters from."};
  mutable PublicToolHandle<ISequencerTimerTool> m_timerTool{this, "TimerTool", "SequencerTimerTool"};
  int                                           m_timer      = 0;
  mutable std::atomic<unsigned long long>       m_evtCounter = {0};

  template <class TYPE>
  void fillTuple( Tuple& tuple, const std::string& var, const TYPE number ) const {
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Filling " << var << " with " << number << endmsg;
    tuple->column( var, number ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
};
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( TimingTuple )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TimingTuple::TimingTuple( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                 KeyValue{"RecSummaryLocation", LHCb::RecSummaryLocation::Default}} ) {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode TimingTuple::initialize() {
  return GaudiTupleAlg::initialize().andThen( [&] { return m_timerTool.retrieve(); } ).andThen( [&] {
    m_timer = m_timerTool->addTimer( name() );
    m_timerTool->start( m_timer ); // start it now
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void TimingTuple::operator()( const LHCb::ODIN& odin, const LHCb::RecSummary& summary ) const {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;
  ++m_evtCounter;

  const double t = m_timerTool->stop( m_timer ); // stop
  if ( msgLevel( MSG::DEBUG ) ) debug() << "Time is " << t << endmsg;
  m_timerTool->start( m_timer ); // start again

  // Fill tuple
  Tuple tuple = nTuple( "TimingTuple", "TimingTuple" );

  fillTuple( tuple, "EventInSequence", m_evtCounter.load() );
  fillTuple( tuple, "RunNumber", odin.runNumber() );
  fillTuple( tuple, "EvtNumber", (int)odin.eventNumber() );
  fillTuple( tuple, "Memory", (double)System::virtualMemory() );
  fillTuple( tuple, "Time", t );
  for ( const auto& algName : m_algos ) {
    const auto index = m_timerTool->indexByName( algName );
    if ( index < 0 ) {
      throw GaudiException( algName + " not registered with TimerTool.", this->name(), StatusCode::FAILURE );
    }
    fillTuple( tuple, algName, m_timerTool->lastTime( index ) );
  }

  fillTuple( tuple, "nPVs", summary.info( LHCb::RecSummary::DataTypes::nPVs, -999 ) );
  fillTuple( tuple, "VPClusters", summary.info( LHCb::RecSummary::DataTypes::nVPClusters, -999 ) );
  fillTuple( tuple, "UTClusters", summary.info( LHCb::RecSummary::DataTypes::nUTClusters, -999 ) );
  fillTuple( tuple, "FTClusters", summary.info( LHCb::RecSummary::DataTypes::nFTClusters, -999 ) );
  fillTuple( tuple, "spdMult", summary.info( LHCb::RecSummary::DataTypes::nSPDhits, -999 ) );

  fillTuple( tuple, "MuonCoordsS0", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS0, -999 ) );
  fillTuple( tuple, "MuonCoordsS1", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS1, -999 ) );
  fillTuple( tuple, "MuonCoordsS2", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS2, -999 ) );
  fillTuple( tuple, "MuonCoordsS3", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS3, -999 ) );
  fillTuple( tuple, "MuonCoordsS4", summary.info( LHCb::RecSummary::DataTypes::nMuonCoordsS4, -999 ) );
  fillTuple( tuple, "MuonTracks", summary.info( LHCb::RecSummary::DataTypes::nMuonTracks, -999 ) );

  fillTuple( tuple, "BestTracks", summary.info( LHCb::RecSummary::DataTypes::nTracks, -999 ) );
  fillTuple( tuple, "BackwardTracks", summary.info( LHCb::RecSummary::DataTypes::nBackTracks, -999 ) );
  fillTuple( tuple, "VeloTracks", summary.info( LHCb::RecSummary::DataTypes::nVeloTracks, -999 ) );
  fillTuple( tuple, "LongTracks", summary.info( LHCb::RecSummary::DataTypes::nLongTracks, -999 ) );
  fillTuple( tuple, "DownstreamTracks", summary.info( LHCb::RecSummary::DataTypes::nDownstreamTracks, -999 ) );
  fillTuple( tuple, "UpstreamTracks", summary.info( LHCb::RecSummary::DataTypes::nUpstreamTracks, -999 ) );

  fillTuple( tuple, "Rich1Hits", summary.info( LHCb::RecSummary::DataTypes::nRich1Hits, -999 ) );
  fillTuple( tuple, "Rich2Hits", summary.info( LHCb::RecSummary::DataTypes::nRich2Hits, -999 ) );

  tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}
