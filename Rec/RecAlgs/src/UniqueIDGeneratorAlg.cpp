/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "LHCbAlgs/Producer.h"

#include "Event/UniqueIDGenerator.h"

/** @class UniqueIDGeneratorAlg UniqueIDGeneratorAlg.h
 *
 * Algorithm to create the UniqueIDGenerator object. Beware that the user is
 * responsible for maintaining the generator instance unique and constant
 * within an execution unit and across different executions. Visit
 * @ref LHCb::UniqueIDGenerator for more details on the current limitations
 * related to this.
 *
 * @author Miguel Ramos Pernas
 */
class UniqueIDGeneratorAlg final : public LHCb::Algorithm::Producer<LHCb::UniqueIDGenerator()> {
public:
  UniqueIDGeneratorAlg( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::Producer<LHCb::UniqueIDGenerator()>(
            name, pSvcLocator, KeyValue{"Output", LHCb::UniqueIDGeneratorLocation::Default} ) {}

  LHCb::UniqueIDGenerator operator()() const override { return LHCb::UniqueIDGenerator(); }
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UniqueIDGeneratorAlg )
