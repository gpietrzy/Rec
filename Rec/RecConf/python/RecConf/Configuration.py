###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
## @package RecConf
#  High level configuration tools for REC
#  @author Rob Lambert
#  @date   21/05/2010

__author__ = "Rob Lambert"

from LHCbKernel.Configuration import *
from GlobalReco.Configuration import *
from Configurables import CaloFutureProcessor

from Configurables import (ProcessPhase, GlobalRecoChecks,
                           MuonTrackMonitorConf)


## @class RecSysConf
#  Configurable for LHCb reconstruction
#  @author Chris Jones  (Christopher.Rob.Jones@cern.ch)
#  @date   15/08/2008
class RecSysConf(LHCbConfigurableUser):

    ## Possible used Configurables
    __used_configurables__ = [GlobalRecoConf, CaloFutureProcessor]

    ## Default tracking sequence for Run 3
    DefaultTracking = ["TrFast", "TrBest"]

    ## List of known special data processing options
    KnownSpecialData = [
        "cosmics",
        "veloOpen",
        "fieldOff",
        "beamGas",
        "microBiasTrigger",
        "pA",
    ]
    ## List of DataTypes (years) for Run 3
    Run3DataTypes = ["Upgrade"]

    ## Steering options
    __slots__ = {
        "RecoSequence":
        None  # The Sub-detector sequencing. Default is all known
        ,
        "SpecialData": [
        ]  # Various special data processing options. See KnownSpecialData for all options
        ,
        "Detectors": ['VP', 'UT', 'FT', 'Rich', 'Tr', 'Calo', 'Muon'],
        "Histograms":
        "OfflineFull"  # Type of histograms
        ,
        "Context":
        "Offline"  # The context within which to run the reco sequences
        ,
        "OutputType":
        ""  # DST type, not used for now
        ,
        "DataType":
        ""  # Type of data, propagated from application
        ,
        "InputType":
        ""  # Input file type, propagated from application
        ,
        "OutputLevel":
        INFO  # The printout level to use
        ,
        "Simulation":
        False  # Simulated data
        ,
        "OnlineMode":
        False  # Online mode
        ,
        "SkipTracking":
        False  # Skip the tracking sequence
    }

    ## Initialisation. Needed to set RICH option before applyConf method starts.
    def __init__(self, name=Configurable.DefaultName, *args, **kwargs):
        LHCbConfigurableUser.__init__(
            self, name=Configurable.DefaultName, *args, **kwargs)

    def expertHistos(self):
        return self.getProp("Histograms") == "Expert"

    ## Apply the configuration
    def applyConf(self):

        # Check the special data options
        for option in self.getProp("SpecialData"):
            if option not in self.KnownSpecialData:
                raise RuntimeError("Unknown SpecialData option '%s'" % option)

        if self.getProp("SkipTracking"):
            raise RuntimeError(
                "SkipTracking not (yet) supported for Upgrade configurations")

        # Phases
        if not self.isPropertySet("RecoSequence"):
            recoDets = ["Decoding"] + self.DefaultTracking + [
                "RICH", "CALOFUTURE", "MUON", "PROTO", "SUMMARY"
            ]
            self.setProp("RecoSequence", recoDets)

        from Configurables import ProcessPhase
        recoSeq = self.getProp("RecoSequence")
        ProcessPhase("Reco").DetectorList += recoSeq

        # Primary Vertex and V0 finding. Here for reference, not actually called for Run 3, to be adapted
        if "Vertex" in recoSeq and self.getProp(
                "DataType") not in self.Run3DataTypes:
            from Configurables import PatPVOffline, TrackV0Finder
            pvAlg = PatPVOffline()
            if ("veloOpen" in self.getProp("SpecialData")
                    or "microBiasTrigger" in self.getProp("SpecialData")):
                from PatPV import PVConf
                PVConf.LoosePV().configureAlg()

            from Configurables import LHCb__Converters__Track__v1__fromLHCbTrackVector as FromLHCbTrackVector
            trackConverter = FromLHCbTrackVector("VeloTrackConverter")
            trackConverter.InputTracksName = "Rec/Track/Best"
            trackConverter.OutputTracksName = "Rec/Track/Vector/Best"
            pvAlg.InputTracks = "Rec/Track/Vector/Best"
            pvAlg.OutputVertices = "Rec/Vertex/Vector/Primary"
            from Configurables import LHCb__Converters__RecVertex__v1__fromVectorLHCbRecVertex as FromVectorLHCbRecVertex
            vertexConverter = FromVectorLHCbRecVertex("VertexConverter")
            vertexConverter.InputVerticesName = "Rec/Vertex/Vector/Primary"
            vertexConverter.InputTracksName = "Rec/Track/Best"
            vertexConverter.OutputVerticesName = "Rec/Vertex/Primary"

            GaudiSequencer("RecoVertexSeq").Members += [
                trackConverter, pvAlg, vertexConverter
            ]

            trackV0Finder = TrackV0Finder()
            GaudiSequencer("RecoVertexSeq").Members += [trackV0Finder]

        specialDataList = self.getProp("SpecialData")
        specialDataList.append("upgrade")
        self.setProp("SpecialData", specialDataList)

        # Tracking (Should make it more fine grained ??)
        DoTracking = False
        for seq in self.DefaultTracking:
            if seq in recoSeq: DoTracking = True

        # RICH
        if "RICH" in recoSeq:

            # For now explicitly run the RICH future decoder here. Longer term should be elsewhere..
            from Configurables import Rich__Future__RawBankDecoder as RichDecoder
            from Configurables import LHCb__UnpackRawEvent
            richUnpack = LHCb__UnpackRawEvent(
                'UnpackRawEvent',
                BankTypes=['Rich'],
                RawEventLocation='DAQ/RawEvent:Rich/RawEvent',
                RawBankLocations=['/Event/DAQ/RawBanks/Rich'])
            richDecode = RichDecoder("RichFutureDecode")

            # The main sequence
            seq = GaudiSequencer("RecoRICHSeq")

            # Filter the best track container by track type
            from Configurables import Rich__Future__Rec__TrackFilter as TrackFilter
            tkFilt = TrackFilter("RichTrackTypeFilter")
            tkFilt.InTracksLocation = "Rec/Track/Best"

            # Input tracks
            tkLocs = {
                "Long": tkFilt.OutLongTracksLocation,
                "Down": tkFilt.OutDownTracksLocation,
                "Up": tkFilt.OutUpTracksLocation
            }

            # Output PID
            pidLocs = {
                "Long": "Rec/Rich/LongPIDs",
                "Down": "Rec/Rich/DownPIDs",
                "Up": "Rec/Rich/UpPIDs"
            }

            # Merged output
            finalPIDLoc = "Rec/Rich/PIDs"

            # Allow the RICH to create states on the fly when processing DSTs
            # where the tracks as read in might have their states stripped.
            createStates = -1 != self.getProp("InputType").find("DST")

            # The reconstruction
            from RichFutureRecSys.ConfiguredRichReco import RichRecoSequence
            RichRec = RichRecoSequence(
                dataType=self.getProp("DataType"),
                onlineBrunelMode=self.getProp("OnlineMode"),
                createMissingStates=createStates,
                inputTrackLocations=tkLocs,
                outputPIDLocations=pidLocs,
                mergedOutputPIDLocation=finalPIDLoc)

            # Add to the sequence
            seq.Members = [richUnpack, richDecode, tkFilt, RichRec]

        # CALO
        if "CALOFUTURE" in recoSeq:
            import GaudiKernel.ProcessJobOptions
            seq = GaudiSequencer('RecoCALOFUTURESeq')
            caloConf = CaloFutureProcessor(
                Context=self.getProp('Context'),
                OutputLevel=self.getProp('OutputLevel'),
                UseTracks=True,
                EnableOnDemand=False,
                DataType=self.getProp('DataType'))
            GaudiKernel.ProcessJobOptions.PrintOn(force=True)
            seq.Members = [caloConf.caloFutureSequence()]
            GaudiKernel.ProcessJobOptions.PrintOff()

        # MUON
        if "MUON" in recoSeq:
            from Configurables import RawBankReadoutStatusConverter, RawBankReadoutStatusFilter, MuonIDHlt2Alg, fromV2MuonPIDV1MuonPID, PrepareMuonHits, UniqueIDGeneratorAlg, MergeMuonPIDsV1, MuonPIDToMuonTracks, MuonPIDV2ToMuonTracks
            from Configurables import LHCb__Converters__Track__SOA__fromV1Track as TrackSOAFromV1
            longTrackAlg = TrackSOAFromV1(
                "longTrackAlg",
                InputTracks="/Event/Rec/Track/Best",
                OutputTracks="/Event/Rec/Track/LongBest",
                Relations="/Event/Rec/Track/LongBestFromV1SOARelations",
                RestrictToType="Long")
            longMuonPIDAlg = MuonIDHlt2Alg(
                "longMuonPIDAlg",
                InputTracks="/Event/Rec/Track/LongBest",
                InputMuonHits="/Event/Muon/MuonHits",
                OutputMuonPID="/Event/Muon/LongMuonPIDs")
            longMuonTrackAlg = MuonPIDV2ToMuonTracks(
                "longMuonPIDv2ToMuonTracksAlg",
                InputMuonPIDs="/Event/Muon/LongMuonPIDs",
                InputTracks="/Event/Rec/Track/Best",
                OutputMuonTracks="/Event/Rec/Muon/LongMuonTracks",
                RestrictToType="Long")
            longMuonPIDv1Alg = fromV2MuonPIDV1MuonPID(
                "longMuonPIDv1Alg",
                InputMuonPIDs="/Event/Muon/LongMuonPIDs",
                InputTracks="/Event/Rec/Track/Best",
                InputMuonTracks="/Event/Rec/Muon/LongMuonTracks",
                OutputMuonPIDs="/Event/Muon/LongMuonPIDsv1",
                RestrictToType="Long")

            downTrackAlg = TrackSOAFromV1(
                "downTrackAlg",
                InputTracks="/Event/Rec/Track/Best",
                OutputTracks="/Event/Rec/Track/DownBest",
                Relations="/Event/Rec/Track/DownBestFromV1SOARelations",
                RestrictToType="Downstream")
            downMuonPIDAlg = MuonIDHlt2Alg(
                "downMuonPIDAlg",
                InputTracks="/Event/Rec/Track/DownBest",
                InputMuonHits="/Event/Muon/MuonHits",
                OutputMuonPID="/Event/Muon/DownMuonPIDs")
            downMuonTrackAlg = MuonPIDV2ToMuonTracks(
                "downMuonPIDv2ToMuonTracksAlg",
                InputMuonPIDs="/Event/Muon/DownMuonPIDs",
                InputTracks="/Event/Rec/Track/Best",
                OutputMuonTracks="/Event/Rec/Muon/DownMuonTracks",
                RestrictToType="Downstream")
            downMuonPIDv1Alg = fromV2MuonPIDV1MuonPID(
                "downMuonPIDv1Alg",
                InputMuonPIDs="/Event/Muon/DownMuonPIDs",
                InputTracks="/Event/Rec/Track/Best",
                InputMuonTracks="/Event/Rec/Muon/DownMuonTracks",
                OutputMuonPIDs="/Event/Muon/DownMuonPIDsv1",
                RestrictToType="Downstream")

            muonPIDAlg = MergeMuonPIDsV1(
                InputMuonPIDLocations=[
                    "/Event/Muon/LongMuonPIDsv1", "/Event/Muon/DownMuonPIDsv1"
                ],
                OutputMuonPIDLocation="/Event/Rec/Muon/MuonPID")
            muonTrackAlg = MuonPIDToMuonTracks(
                InputMuonPIDs="/Event/Rec/Muon/MuonPID",
                OutputMuonTracks="/Event/Rec/Track/Muon")

            RawBankReadoutStatusConverter("MuonProcStatus").System = "Muon"
            RawBankReadoutStatusConverter("MuonProcStatus").BankTypes = [
                "Muon"
            ]
            RawBankReadoutStatusFilter("MuonROFilter").BankType = 'Muon'
            RawBankReadoutStatusFilter("MuonROFilter").RejectionMask = 2067
            GaudiSequencer("RecoMUONSeq").Members += [
                "MuonRec", "RawBankReadoutStatusConverter/MuonProcStatus",
                "RawBankReadoutStatusFilter/MuonROFilter",
                PrepareMuonHits(),
                UniqueIDGeneratorAlg(), longTrackAlg, longMuonPIDAlg,
                longMuonTrackAlg, longMuonPIDv1Alg, downTrackAlg,
                downMuonPIDAlg, downMuonTrackAlg, downMuonPIDv1Alg, muonPIDAlg,
                muonTrackAlg
            ]

        # PROTO
        if "PROTO" in recoSeq:
            self.setOtherProps(
                GlobalRecoConf(),
                ["DataType", "SpecialData", "Context", "OutputLevel"])
            GlobalRecoConf().RecoSequencer = GaudiSequencer("RecoPROTOSeq")

        # SUMMARY
        if "SUMMARY" in recoSeq:
            from Configurables import RecSummaryAlg
            summary = RecSummaryAlg("RecSummary")
            # make a new list of uppered detectors
            dets = []
            for det in self.getProp("Detectors"):
                dets.append(det.upper())
            summary.Detectors = dets
            GaudiSequencer("RecoSUMMARYSeq").Members += [summary]


## @class RecMoniConf
#  Configurable for LHCb reconstruction monitoring (without MC truth)
#  @author Marco Cattaneo (Marco.Cattaneo@cern.ch)
#  @date   15/10/2009
class RecMoniConf(LHCbConfigurableUser):

    ## Possible used Configurables
    __used_configurables__ = [MuonTrackMonitorConf, GlobalRecoChecks]

    ## Configurables that must be configured before us
    __queried_configurables__ = [
        RecSysConf,
    ]

    ## Steering options
    __slots__ = {
        "MoniSequence": None,
        "Histograms": "OfflineFull"  # Type of histograms
        ,
        "CheckEnabled": False,
        "OutputLevel": INFO,
        "Context": "Offline",
        "DataType": "",
        "Simulation": False  # Simulated data
        ,
        "Detectors": ['VP', 'UT', 'FT', 'Rich', 'Tr', 'Calo', 'Muon'],
        "OnlineMode": False  # Online mode
        ,
        "SkipTracking": False  # Skip the tracking detectors
    }

    _propertyDocDct = {
        'MoniSequence':
        """ List of subdetectors to monitor, default is all known """,
        'Histograms':
        """ Type of histograms """,
        'CheckEnabled':
        """ Flags whether a check sequence (with MC truth) is also enabled (default False) """,
        'OutputLevel':
        """ The printout level to use (default INFO) """,
        'Context':
        """ The context within which to run (default 'Offline') """,
        'DataType':
        """ Data type, propagated from the application """,
        'Simulation':
        """ Is it simulated data? """,
        'OnlineMode':
        """ Online Brunel mode """,
        'SkipTracking':
        """ Skip the tracking detectors """
    }

    ## Known monitoring sequences, all run by default

    KnownMoniSubdets = ["CALOFUTURE", "RICH", "MUON"]
    KnownMoniGeneral = ["GENERAL", "Tr", "PROTO"]
    KnownExpertMoniSubdets = []

    def expertHistos(self):
        return self.getProp("Histograms") == "Expert"

    ## Apply the configuration
    def applyConf(self):

        # Build list of existing detectors
        dets = []
        for det in self.getProp("Detectors"):
            dets.append(det.upper())
        # general monitoring
        for det in self.KnownMoniGeneral:
            dets.append(det)
        if [det for det in ['ECAL', 'HCAL'] if det in dets]:
            dets.append("CALOFUTURE")
        if [
                det for det in ['RICH1', 'RICH2', 'RICH1PMT', 'RICH2PMT']
                if det in dets
        ]:
            dets.append("RICH")

        # Build default monitoring
        moniDets = self.KnownMoniSubdets + self.KnownMoniGeneral
        if self.expertHistos():
            moniDets = moniDets + self.KnownExpertMoniSubdets

        # Set up monitoring (i.e. not using MC truth)
        moniSeq = []
        if not self.isPropertySet("MoniSequence"):
            for det in moniDets:
                if det in dets:
                    moniSeq.append(det)
            self.MoniSequence = moniSeq
        else:
            for seq in self.getProp("MoniSequence"):
                if seq in ["VP", "UT", "FT"]:
                    log.warning("Monitoring not implemented for %s" % seq)
                else:
                    if seq not in moniDets:
                        log.warning(
                            "Unknown subdet '%s' in MoniSequence" % seq)
                if seq not in dets:
                    log.warning("Detector unknown '%s'" % seq)
                if not self.expertHistos():
                    if seq in self.KnownExpertMoniSubdets:
                        log.warning(
                            "Subdet '%s' in MoniSequence only is Expert histos are enabled"
                            % seq)
            moniSeq = self.getProp("MoniSequence")

        if self.getProp("SkipTracking"):
            for det in ['VP', 'FT', 'UT']:
                if det in moniSeq:
                    moniSeq.remove(det)

        from Configurables import ProcessPhase
        ProcessPhase("Moni").DetectorList += moniSeq

        # Histograms filled both in real and simulated data cases
        if "GENERAL" in moniSeq:

            # Enable ChronoAuditor
            chronoAuditor = "ChronoAuditor"
            if chronoAuditor not in AuditorSvc().Auditors:
                AuditorSvc().Auditors += [chronoAuditor]
            ChronoAuditor().Enable = True
            # Turn off most output
            ChronoStatSvc().ChronoPrintOutTable = False
            ChronoStatSvc().PrintUserTime = False
            ChronoStatSvc().PrintSystemTime = False

            from Configurables import GaudiSequencer, RecProcessingTimeMoni
            seq = GaudiSequencer("MoniGENERALSeq")

            # Overall time
            overallTime = RecProcessingTimeMoni("OverallEventProcTime")
            overallTime.Algorithms = ["PhysicsSeq"]
            seq.Members += [overallTime]

            # Tracking
            trackTime = RecProcessingTimeMoni("TrackEventProcTime")
            trackTime.Algorithms = [
                "RecoDecodingSeq", "RecoTrFastSeq", "RecoTrBestSeq"
            ]
            seq.Members += [trackTime]

            # Vertex
            vertTime = RecProcessingTimeMoni("VertexEventProcTime")
            vertTime.Algorithms = ["RecoVertexSeq"]
            seq.Members += [vertTime]

            # RICH
            richTime = RecProcessingTimeMoni("RichEventProcTime")
            richTime.Algorithms = ["RecoRICHSeq"]
            seq.Members += [richTime]

            # CALO
            caloTime = RecProcessingTimeMoni("CaloFutureEventProcTime")
            caloTime.Algorithms = ["RecoCALOFUTURESeq"]
            seq.Members += [caloTime]

            # MUON
            muonTime = RecProcessingTimeMoni("MuonEventProcTime")
            muonTime.Algorithms = ["RecoMUONSeq"]
            seq.Members += [muonTime]

            # PROTO
            protoTime = RecProcessingTimeMoni("ProtoEventProcTime")
            protoTime.Algorithms = ["RecoPROTOSeq"]
            seq.Members += [protoTime]

            # ProcStat Abort rates
            from Configurables import ProcStatAbortMoni
            seq.Members += [ProcStatAbortMoni()]

        if "MUON" in moniSeq:
            from MuonPIDChecker import ConfigureMuonPIDChecker as mmuon
            mydata = self.getProp("DataType")
            mymonitconf = mmuon.ConfigureMuonPIDChecker(data=mydata)
            mymonitconf.configure(
                UseMC=False, HistosLevel=self.getProp("Histograms"))

            mymtmconf = MuonTrackMonitorConf()
            self.setOtherProps(mymtmconf,
                               ["Histograms", "OutputLevel", "DataType"])
            mymtmconf.setProp("Sequencer", GaudiSequencer("MoniMUONSeq"))

        if "Tr" in moniSeq:
            from TrackMonitors.ConfiguredTrackMonitors import ConfiguredTrackMonitorSequence
            ConfiguredTrackMonitorSequence(Name='MoniTrSeq')

        if "PROTO" in moniSeq:
            from Configurables import (ChargedProtoParticleMoni,
                                       GaudiSequencer)
            seq = GaudiSequencer("MoniPROTOSeq")
            seq.Members += [
                ChargedProtoParticleMoni("ChargedProtoPMoni"),
            ]
            if self.expertHistos() and not self.getProp("CheckEnabled"):
                exSeq = GaudiSequencer("ExpertProtoMoni")
                seq.Members += [exSeq]
                GlobalRecoChecks().Sequencer = exSeq

        if "RICH" in moniSeq:
            from Configurables import GaudiSequencer
            seq = GaudiSequencer("MoniRICHSeq")
            tkLocs = {
                "Long": "Rec/Track/BestRichLong",
                "Down": "Rec/Track/BestRichDown",
                "Up": "Rec/Track/BestRichUp"
            }
            pidLocs = {
                "Long": "Rec/Rich/LongPIDs",
                "Down": "Rec/Rich/DownPIDs",
                "Up": "Rec/Rich/UpPIDs"
            }
            from RichFutureRecMonitors.ConfiguredRecoMonitors import RichRecMonitors
            seq.Members = [
                RichRecMonitors(
                    dataType=self.getProp("DataType"),
                    onlineBrunelMode=self.getProp("OnlineMode"),
                    histograms=self.getProp("Histograms"),
                    inputTrackLocations=tkLocs,
                    outputPIDLocations=pidLocs)
            ]
