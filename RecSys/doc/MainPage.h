/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** \mainpage notitle
 *  \anchor recsysdoxygenmain
 *
 * This is the code reference manual for the %LHCb reconstruction classes.
 * <p><b>See also:</b>
 * \li <a href="../release.notes"><b>Release notes history</b></a>
 * \li <a href=" http://cern.ch/lhcb-release-area/DOC/rec/"><b>Rec project Web pages</b></a>
 * \li \ref lhcbdoxygenmain  "LHCbSys documentation (LHCb core classes)"
 * \li \ref gaudidoxygenmain "Gaudi documentation (Framework packages)"
 * \li \ref externaldocs     "Related external libraries"

 * <hr>
 * \htmlinclude new_release.notes
 * <hr>
 */
