2021-05-27 Rec v32r1
===

This version uses
Lbcom [v32r1](../../../../Lbcom/-/tags/v32r1),
LHCb [v52r1](../../../../LHCb/-/tags/v52r1),
Gaudi [v35r4](../../../../Gaudi/-/tags/v35r4) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Rec [v32r0](/../../tags/v32r0), with the following changes:

### New features ~"new feature"

- ~Tracking ~VP | Add full search mode to Velo Tracking, !2364 (@ahennequ)
- ~Tracking ~VP ~Monitoring | Track VP Overlap monitor, !2398 (@bimitres)
- ~Calo | Bremsstrahlung-track matching with selective search in calo, !2267 (@mveghel)
- ~Composites ~Functors | BPVLTIME ThOr Functor added, !2283 (@vrenaudi)
- ~Composites ~"Event model" | Add a Generic Track SOA Container, !2356 (@agilman)
- ~Filters | Support filtering SharedObjectsContainer views, !2425 (@apearce)
- ~"MC checking" | Add VeloHits, VeloTracks, UTHits and FThits to PrMultiplicityChecker, !2322 (@rlitvino)


### Fixes ~"bug fix" ~workaround

- ~Decoding ~UT | Recovery for badly encoded UT raw bank, !2439 (@graven)
- ~Tracking | Fix init of MeasurementProviderT to avoid segfault if UseBfield=true, !2440 (@chasse)
- ~Tracking | Fix minimize function in Measurement.cpp, !2438 (@chasse)
- ~Tracking | PrKalmanFilter: fix creation of Rich1 states, !2414 (@chasse)
- ~Tracking ~"PV finding" | Fix issues uncovered by Clang 11, !2449 (@rmatev)
- ~Build | Extend the unknown-attributes warning workaround to ROOT 6.24, !2419 (@clemenci)


### Enhancements ~enhancement

- ~Tracking | Add MaxNumTracks property in VertexListRefiner, !2442 (@bimitres)
- ~Tracking | PrKalmanFilter: add alignment derivatives & classical smoother iteration, !2433 (@chasse) [#179]
- ~Tracking | New ghost rejection NN for PrForwardTracking, !2397 (@gunther) [Event/LHCb__Converters__Track__v1__fromV2TrackV1Track#4]
- ~Tracking | PrForwardTracking redesign, !2352 (@gunther) [#157,#162,#38] :star:
- ~Tracking | Remove the partners of VP outliers, !2339 (@valukash)
- ~Tracking ~UT | Adapt Pr algorithms for change in UT sector handling, !2404 (@decianm)
- ~Tracking ~Conditions | Reset caches when updating geometry for alignment (DD4hep), !2396 (@sponce)
- ~Calo | Ecal clustering sanity checks, !2351 (@cmarinbe)
- ~Composites ~Monitoring | Fix bias in pv created in Vertex Monitor, !2391 (@ahennequ)
- ~Monitoring | Clean up Run2 stuff in TrackMonitor, !2390 (@peilian) [challenge#2]
- ~Monitoring | VPTrackSelector hit side with module, !2389 (@bimitres)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Follow LHCb!2963, !2366 (@apearce)
- ~Decoding | Unify UT decoders interface, !2386 (@graven)
- ~Tracking | Move StateZTraj to TrackFitEvent, !2401 (@graven)
- ~Tracking | Simplify Measurement & Projectors, !2400 (@graven)
- ~Tracking | Remove TfKernel / TsaKernel / TsaAlgorithms, !2384 (@graven)
- ~Tracking | Cleanup TrackEventFitter and TrackBestTrackCreator, !2368 (@chasse)
- ~Tracking | Made TrackContainerCopy functionnal, !2359 (@sponce)
- ~Tracking ~UT | Change PrUTHitHandler and algorithms for SOACollection, !2422 (@decianm)
- ~Tracking ~Monitoring | Small modifications in TrackResChecker, !2395 (@ausachov)
- ~Muon | Remove MuonClusterTool and MuonTimeCor, !2373 (@pkoppenb)
- ~PID | Remove `beginEvent` method from IGhostProbability interface, !2387 (@graven)
- ~Functors | Functors/Composite: require less template arguments to be specified, !2372 (@graven)
- ~"MC checking" | Modernize PrimaryVertexChecker, !2388 (@graven)
- ~"MC checking" | Refactoring PrimaryVertexChecker, !1925 (@twojton) :star:
- ~Monitoring | Clean and modernize VPTrackMonitor, !2435 (@bimitres)
- ~Build | Add missing links to libraries, !2410 (@clemenci)
- Update reference files from nightly slot lhcb-head, build ID 2934. To be tested with Rec!2404, !2427 (@ascarabo)
- Add assert to AddHit to validate input, !2399 (@graven) [#193]
- Avoid  unnecessary reinterpret_cast, !2393 (@graven)
- Update references for Rec!2351, !2381 (@fsouzade)
- Undo  SmartRefVector workaround following merge of gaudi/Gaudi!1180, !2363 (@chasse)


### Documentation ~Documentation

- ~Tracking | Add warning to IdealStateCreator about non-ideal states, !2421 (@chasse) [#196]
- Update CONTRIBUTING.md to update supported platforms, !2412 (@cattanem)

### Other

- ~Calo | Tweak SelectiveBremMatchAlg, !2423 (@graven)
- Clarify that CHI2DOF and CHI2 are track and vertex chi2 cuts, !2436 (@pkoppenb)
- A grammar for functordesc, !2420 (@nnolte)
- Revert "Merge branch 'alexgilman_v2tracks' into 'master'", !2409 (@graven)
- SOA Track fix (follow up !2356), !2408 (@agilman)
