2021-10-04 Rec v33r2
===

This version uses
Lbcom [v33r3](../../../../Lbcom/-/tags/v33r3),
LHCb [v53r3](../../../../LHCb/-/tags/v53r3),
Gaudi [v36r1](../../../../Gaudi/-/tags/v36r1) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Rec [v33r1](/../../tags/v33r1), with the following changes:

### New features ~"new feature"

- RefBot going live!, !2577 (@chasse)


### Fixes ~"bug fix" ~workaround

- ~Monitoring | Fix typos in TrackMonitor, !2564 (@sstahl)
- Fix DD4hep build, !2567 (@sponce)


### Enhancements ~enhancement

- ~Configuration | Ensure functor configuration is stable, !2576 (@rmatev)
- ~Functors | New Thor functions for BandQ triggers, !2505 (@mengzhen)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Remove dependencies on Run 1/2 detectors, !2532 (@decianm)
