2022-04-14 Rec v34r2
===

This version uses
Lbcom [v33r8](../../../../Lbcom/-/tags/v33r8),
LHCb [v53r8](../../../../LHCb/-/tags/v53r8),
Gaudi [v36r5](../../../../Gaudi/-/tags/v36r5),
Detector [v1r1](../../../../Detector/-/tags/v1r1) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to Rec [v34r1](/../../tags/v34r1), with the following changes:

### New features ~"new feature"

- ~Tracking | Add PrKalmanFilter component without UT, !2844 (@gunther)
- ~Tracking ~"PV finding" ~"Event model" | Change PV finder output, !2512 (@wouter)
- ~VP ~Monitoring | Template VPTrackMonitor to output histograms, !2791 (@anbeck)
- ~Composites | Add DummyParticleMaker (for Alignment), !2832 (@freiss)


### Fixes ~"bug fix" ~workaround

- ~Tracking | Fix rare sorting issue in output states in PrKalman, !2718 (@ausachov) [#275]
- ~Muon | Adapt muon code for flag field, add MuonDLL, !2785 (@decianm)
- ~Functors | Fix: prevent dangling references when calling prepare() on temporary ComposedFunctor, !2841 (@chasse)
- ~Functors | Enable configuration of file splitting via THOR_JIT_N_SPLITS env var, !2830 (@chasse)


### Enhancements ~enhancement

- ~Tracking | Add passed/total counters to VertexListRefiner, !2814 (@freiss)
- ~Tracking | More Flexible Inputs to PrKalmanFilter, !2809 (@gunther) [#303]
- ~Tracking ~Monitoring | Template the whole TrackFitMatchMonitor, !2802 (@jkubat)
- ~Tracking ~Monitoring | Template TrackMonitor on fit node type, !2797 (@jkubat) [Alignment#16]
- ~Calo | New pile-up corrections, !2413 (@canorman)
- ~Functors | Reduce memory usage of functor compilation by O(10%), !2837 (@chasse)
- Define a global set of qmtest exclusions for Rec, !2824 (@chasse)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Adapt to update in LHCb::HitPattern, !2834 (@wouter)
- ~Monitoring | Change 2D histograms to profile in Trackmonitor, !2839 (@bimitres)
- ~Build | Fix functor cache for release builds (follow up !2699), !2829 (@rmatev)
- MuonIDHltAlg - Add missing include for Gaudi Units, !2845 (@jonrob)
- Remove unused variable (clang warning), !2831 (@rmatev)
- Removed duplication of channelIDs between LHCb and Detector. Kept Detector ones, !2826 (@sponce)
- Refactored IGeometry info and fix to extrapolator test, !2777 (@bcouturi)
