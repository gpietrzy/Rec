2023-12-07 Rec v35r21
===

This version uses
Lbcom [v34r21](../../../../Lbcom/-/tags/v34r21),
LHCb [v54r21](../../../../LHCb/-/tags/v54r21),
Detector [v1r24](../../../../Detector/-/tags/v1r24),
Gaudi [v36r16](../../../../Gaudi/-/tags/v36r16) and
LCG [103](http://lcginfo.cern.ch/release/103/) with ROOT 6.28.00.

This version is released on the `master` branch.
Built relative to Rec [v35r20](/../../tags/v35r20), with the following changes:

### New features ~"new feature"

- ~Tracking | Add options to remove beam hole tracks and use sorted clone removal, !3583 (@lohenry)
- ~Luminosity | Add RICH and Calo lumi counters, re-add lumi algorithms, !3472 (@dcraik)


### Fixes ~"bug fix" ~workaround

- ~Tracking | Abort fitting a track if any state has unphysical momentum, !3652 (@gunther) [#516]
- ~Tracking | Use SciFi quarter-level cache in pattern recognition to allow proper alignment, !3565 (@gunther) [#414]
- ~RICH | QuarticSolverNewton: Protect against a (very) rare case of a sqrt(-ve), !3628 (@jonrob)
- Fix clang warning in ParticleToSubcombinationsAlg - add counter for failed vertex fits, !3656 (@ldufour)
- Fixes for gcc 13, clang 16 and C++20, !3626 (@clemenci) [gaudi/Gaudi#278]
- Fix compilation in super-project context, !3617 (@clemenci)


### Enhancements ~enhancement

- ~Functors ~Core | FunctorFactory: Fix DisableJIT behaviour and better feedback, !3620 (@rmatev)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Functors | Move import of cppyy where it's needed, !3638 (@rmatev)


### Documentation ~Documentation


### Other

- ~Tracking | Ghost probability update for long (no UT) pp and PbPb, !3611 (@mveghel)
- ~"PV finding" | InteractionRegion information in additional algorithms, !3622 (@mgiza) [#508]
- ~Calo | Add LED monitoring, !2823 (@jmarchan)
- ~RICH | Restore scripts to submit condor jobs for RichFutureRecSys examples, !3633 (@jonrob)
- ~RICH | RICH: Add time window parameters to info printout when enabled, !3632 (@jonrob)
- ~RICH | Additional histograms for RICH Cherenkov photon 4D hit times, !3607 (@lmalenta)
- ~RICH | RICH 4D Reco Update, !3625 (@jonrob)
- ~RICH | Add options to run over new RICH U2 data sets at various lumi values, !3621 (@jonrob)
- ~RICH | New options for RICH 4D reconstruction, !3616 (@jonrob)
- Revert "Merge branch 'mgiza-interaction-region-supplementary' into 'master'", !3674 (@rmatev)
- Move DaVinciMCTools here, !3673 (@pkoppenb) [Analysis#52]
- Add algorithm to split composite particle into subcombinations, !3640 (@ldufour)
- Update References for: Rec!3607 based on lhcb-master-mr/9510, !3641 (@lhcbsoft)
- Update References for: Lbcom!702, Rec!3632 based on lhcb-master-mr/9488, !3636 (@lhcbsoft)
- Add a python level decay parser and `[]CC` support to SubsPID tool, !3581 (@jzhuo)
- Update References for: Rec!3620, Moore!2703, Moore!2642 based on lhcb-master-mr/9466, !3629 (@lhcbsoft)
- Update References for: LHCb!4321, Lbcom!701, Rec!3625 based on lhcb-master-mr/9449, !3627 (@lhcbsoft)
