2024-01-26 Rec v36r0
===

This version uses
Lbcom [v35r0](../../../../Lbcom/-/tags/v35r0),
LHCb [v55r0](../../../../LHCb/-/tags/v55r0),
Detector [v1r25](../../../../Detector/-/tags/v1r25),
Gaudi [v37r2](../../../../Gaudi/-/tags/v37r2) and
LCG [104](http://lcginfo.cern.ch/release/104/) with ROOT 6.28.04.

This version is released on the `master` branch.
Built relative to Rec [v35r21](/../../tags/v35r21), with the following changes:

### New features ~"new feature"

- PrDebugTrackingLosses add information about ideal states, !3669 (@gunther)


### Fixes ~"bug fix" ~workaround

- ~Tracking | Use dynamic size for clustering temporary data, !3683 (@ahennequ)
- ~RICH | RichDLLs: Fix out-of-range access to array of counters, !3715 (@jonrob)
- ~Monitoring | Fixed small bug in counters of VeloIDOverlapRelationTable, !3463 (@gpietrzy)
- PrKFTool fix missing track invalidation on failure, !3726 (@gunther)
- Remove useless Optional wrapper from States functor, !3672 (@graven)


### Enhancements ~enhancement

- ~Tracking | Simplify PrCloneKiller, !3646 (@gunther) [Moore#657]
- ~RICH ~Monitoring | RichDLLs: Only fill plot for DLL(X-Y) if one or another is above threshold, !3578 (@jonrob)
- Add new options for lepton Velo2Long tracking efficiency lines, !3549 (@ldufour)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Remove usage of FittedForward track type in favour of FitHistory, !3722 (@gunther)
- ~Tracking | Unify SciFiHits hitzone order by taking it from PrSciFiHits header, !3705 (@gunther)
- ~Tracking | Improve code related to PrParameterisationData, !3608 (@gunther)
- ~RICH | RichGlobalPIDWriteRichPIDs: Adapt to RichPID cleanup, !3579 (@jonrob)
- ~Jets | Remove unnecessary dependencies on LoKi in JetAccessories, !3678 (@pkoppenb)
- ~Filters | Put all event/track filters in PrFilters, !3643 (@ldufour)
- Consolidate RichPID functors, !3721 (@graven)
- Streamline FunctionalFlavourTagging algorithm implementations, !3707 (@graven)
- Drop the annoying 'Future' prefix from a number of places in the calo code, !3699 (@graven)
- Delete IBTaggingTool, !3687 (@cprouve)
- Clean up code referring to (unused) PixelModule.h, !3681 (@ldufour)
- Remove DaVinciAlgorithm and LoKi, !3676 (@pkoppenb) [#358]
- Cleanup state location and track type usage in v3 converters, !3677 (@gunther)
- FunctionalParticleMaker: prefer make_unique over naked new/delete, !3649 (@graven)
- MicroDST: Cleanup remnants of the old cloning code, !3619 (@graven)
- Remove use of deprecated `concat_alternatives`, !3599 (@graven)
- Reorganize the way catboost is used, !3576 (@clemenci) [#501]


### Documentation ~Documentation


### Other

- ~Tracking | Charged heavy flavour velo pattern recognition, !2868 (@mveghel)
- ~Tracking | Dynamically sized flags in PrResidualSciFiHits and count empty tracks, !3702 (@gunther) [#527]
- ~Muon ~PID ~"Event model" | Cleanup of duplicate muon info, !3718 (@mveghel)
- ~Calo | Replace neutral functors additionalInfo with neutral pid, !3397 (@tnanut)
- ~RICH | RichDLLs: Use Gaudi CounterArray Accumulator, !3719 (@jonrob)
- ~RICH | RICH 4D Reco: Add support for emulating pixel sizes during reco., !3668 (@jonrob)
- ~RICH | Performance Updates to RICH Reconstruction, !3642 (@jonrob)
- ~RICH | Retune RICH CK theta reco. biases for DD4HEP/DetDesc individually, !3589 (@jonrob)
- ~RICH ~PID ~"Event model" | RICH functors using RichPID object, !3624 (@mveghel)
- ~Filters | Add MCParticleRangeFilter for use in DaVinci & Moore, !3679 (@ldufour)
- ~Functors | Functors for vertex isolation, !3090 (@tfulghes)
- ~Persistency | Further cleanup of old cloning infrastructure, !3630 (@graven)
- ~Monitoring | Fix PV chi2 plots, !3623 (@tmombach)
- ~Monitoring | PrForwardTracking count empty hits and input tracks, !3645 (@gunther)
- ~Monitoring | Add counter to ParticleMassMonitor, !3684 (@alopezhu)
- ~Luminosity | Fix RICH lumi counters, !3695 (@efranzos)
- ~Tuples | Add an extra check for []CC in the decay descriptor of FunTuple, !3637 (@jzhuo)
- ~"Flavour tagging" | Add a new functor that stores the MC_ORIGINFLAG (i.e. a flavour tag type), !3651 (@scelani)
- Update refs from lhcb-master/2279, !3730 (@rmatev)
- Add missing include for standalone header compilation, !3725 (@clemenci)
- Drop unused includes, !3723 (@clemenci)
- Fixed double deletion in TestFunctors, !3720 (@sponce)
- Cleaned up VeloHeavyFlavourTracking to use float rather than scalar::float_v, !3714 (@sponce)
- Update References for: LHCb!4371, Rec!3668, Moore!2846 based on lhcb-master-mr/10149, !3713 (@lhcbsoft)
- New plot to monitor alignment weak modes added to ParticleMassMonitor, !3671 (@miruizdi)
- Update References for: LHCb!4288, Rec!3589, Rec!3579, Rec!3578 based on lhcb-master-mr/10135, !3712 (@lhcbsoft)
- Update References for: LHCb!4337, Lbcom!703, Rec!3642, Moore!2723 based on lhcb-master-mr/10124, !3706 (@lhcbsoft)
- Replace deep-copying of particles with shallow copying, !3538 (@graven)
- Plume timing hists, !3382 (@vyeroshe)
- Add expected type in call to make_data_with_FetchDataFromFile, !3700 (@graven)
- Migrate `IMuonRawBuffer` and `MuonRawBuffer` from LHCb to here, !3698 (@graven) [LHCb#338]
- Various cleanups and modernizations in Phys/DaVinciMCTools, !3697 (@sponce)
- Add unit test to SimplifiedDecayParser, !3696 (@jzhuo)
- Add tests for empty RelationTable, !3694 (@tfulghes)
- Fixed all float comparisons in Rec, !3602 (@sponce)
- Fixed bad loop in FTClusterMonitor, !3692 (@sponce)
- Fixed warnings for C++20, !3690 (@sponce)
- Reduce the number of distinct ValueOr template instantiations, !3685 (@graven)
- Use RawBank::View instead of RawEvent in VPClus{,Full}, !3351 (@graven)
- Dropped unused RecInit, !3680 (@sponce)
- Change ParticleWithBremMaker's counter and make message more clear, !3670 (@ldufour)
- Prefer the more generic Range over KeyedContainer, !3586 (@graven)
- Fix compatibility with fmtlib 10, !3666 (@clemenci)
- Remove old FlavourTagging directory, !3654 (@cprouve)
