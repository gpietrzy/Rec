2024-03-27 Rec v36r4
===

This version uses
Lbcom [v35r4](../../../../Lbcom/-/tags/v35r4),
LHCb [v55r4](../../../../LHCb/-/tags/v55r4),
Detector [v1r29](../../../../Detector/-/tags/v1r29),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `master` branch.
Built relative to Rec [v36r3](/../../tags/v36r3), with the following changes:

### New features ~"new feature"

- Algorithm for deriving FTMatCalibration values, turn on calibration by default, !3693 (@isanders)


### Fixes ~"bug fix" ~workaround

- Fixed compilation of PrCloneKiller.cpp in d0 mode, !3797 (@sponce)
- InteractionRegion information in additional algorithms, !3675 (@rmatev) [#508]


### Enhancements ~enhancement

- Update of TBLV for smog reconstruction, !3781 (@wouter) [Moore#693]
- Persisiting CaloChargedPID, BremInfo and GlobalChargedPID, !3740 (@tnanut)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Remove obsolete use of `context()`, !3783 (@graven)


### Documentation ~Documentation

- Fix Moore docs after !3812 - dont use Indico anchor, !3837 (@rjhunter)
- Fix broken docstring for REQUIRE_CLOSE functor, !3830 (@jagoodin)
- Add more to docstring on what BPV is, !3812 (@rjhunter)

### Other

- Adding b-tracking utils, !3792 (@mveghel)
- VPHitEfficiencyMonitor remove unsused variable to fix clang warning, !3810 (@gunther)
- Follow addition of PV pointer to Particle and PV container update, !3665 (@sesen)
