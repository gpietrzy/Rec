2024-04-19 Rec v36r7
===

This version uses
Lbcom [v35r7](../../../../Lbcom/-/tags/v35r7),
LHCb [v55r7](../../../../LHCb/-/tags/v55r7),
Gaudi [v38r1](../../../../Gaudi/-/tags/v38r1),
Detector [v1r31](../../../../Detector/-/tags/v1r31) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) with ROOT 6.30.04.

This version is released on the `2024-patches` branch.
Built relative to Rec [v36r6](/../../tags/v36r6), with the following changes:

### New features ~"new feature"

- ~Monitoring | Add basic TAE monitor for Plume, !3782 (@rmatev)


### Fixes ~"bug fix" ~workaround

- ~Tracking ~VP ~Monitoring | Add protection in TrackVPOverlapMonitor, !3865 (@mveghel)
- Make sure Functors::Optional is returned from functors, and not std::optional, !3859 (@graven)


### Enhancements ~enhancement



### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- Reduce printout for HLT2, !3862 (@rmatev) [#417]


### Documentation ~Documentation


### Other

- ~Calo | Fix LED monitoring, !3856 (@jmarchan)
- ~Functors | Add functors to retrieve LHC conditions, !3519 (@tfulghes)
- ~Monitoring | Simplify the monitoring and enable the creation of multidimensional histograms, !3791 (@mramospe)
- Prefer Range, !3836 (@sesen)
- Add functors to get the number of hits in regions of the muon stations, !3756 (@lohenry)
