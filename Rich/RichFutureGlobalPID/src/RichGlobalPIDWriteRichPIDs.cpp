/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/EmptyProducer.h"
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/RichPID.h"
#include "Event/Track.h"

// Rec Event
#include "RichFutureRecEvent/RichRecTrackPIDInfo.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// STL
#include <algorithm>
#include <cassert>
#include <memory>

namespace Rich::Future::Rec::GlobalPID {

  namespace {
    /// The input track container type
    using InTracks = LHCb::Track::Range;
  } // namespace

  /** @class WriteRichPIDs RichGlobalPIDWriteRichPIDs.h
   *
   *  Writes the final RichPID data objects.
   *
   *  @author Chris Jones
   *  @date   2016-10-28
   */

  class WriteRichPIDs final : public LHCb::Algorithm::Transformer<LHCb::RichPIDs( const InTracks&,               //
                                                                                  const Summary::Track::Vector&, //
                                                                                  const TrackPIDHypos&,          //
                                                                                  const TrackDLLs::Vector& ),
                                                                  Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    WriteRichPIDs( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input
                       {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                        KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                        KeyValue{"TrackPIDHyposInputLocation", TrackPIDHyposLocation::Default},
                        KeyValue{"TrackDLLsInputLocation", TrackDLLsLocation::Default}},
                       // output
                       {KeyValue{"RichPIDsLocation", LHCb::RichPIDLocation::Default}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Algorithm execution via transform
    LHCb::RichPIDs operator()( const InTracks&               tracks,  //
                               const Summary::Track::Vector& gtracks, //
                               const TrackPIDHypos&          hypos,   //
                               const TrackDLLs::Vector&      dlls ) const override {

      LHCb::RichPIDs rPIDs;
      rPIDs.reserve( tracks.size() );

      // set the version
      rPIDs.setVersion( (unsigned char)m_pidVersion );

      _ri_verbo << "Creating RichPIDs : Version " << (int)rPIDs.version() << endmsg;

      // Loop over the zipped track data
      for ( auto&& [tk, gtk, bestH, dlls] : Ranges::ConstZip( tracks, gtracks, hypos, dlls ) ) {

        // Is this track active ?
        if ( !gtk.active() ) {
          _ri_verbo << "  [ Key " << tk->key() << " skipped as INACTIVE track ]" << endmsg;
          continue;
        }

        // make a new RichPID
        auto pid = std::make_unique<LHCb::RichPID>();

        // Set the track smart ref
        pid->setTrack( tk );

        // Set threshold info
        for ( const auto hypo : activeParticles() ) {
          pid->setAboveThreshold( hypo, gtk.thresholds()[hypo] );
        } // radiator flags
        pid->setUsedAerogel( gtk.radiatorActive( Rich::Aerogel ) );
        pid->setUsedRich1Gas( gtk.radiatorActive( Rich::Rich1Gas ) );
        pid->setUsedRich2Gas( gtk.radiatorActive( Rich::Rich2Gas ) );

        // Best PID value
        pid->setBestParticleID( bestH );

        // -------------------------------------------------------------------------------
        // Finalise delta LL values
        // -------------------------------------------------------------------------------
        auto& vDLLs = pid->particleLLValues();
        assert( dlls.dataArray().size() == vDLLs.size() );
        std::copy( dlls.dataArray().begin(), dlls.dataArray().end(), vDLLs.begin() );
        // Get the pion DLL and ensure its >0
        auto pionDLL = vDLLs[Rich::Pion];
        if ( pionDLL < 0 ) { pionDLL = 0; }
        // sanity check on best ID
        const bool pidOK = ( vDLLs[bestH] <= 1e-10 );
        if ( !pidOK ) {
          ++m_nonZeroBestDLLWarn;
          _ri_debug << "Key = " << tk->key() << " BestID = " << bestH << " Raw DLLs = " << vDLLs << endmsg;
        }
        // Internally, the Global PID normalises the DLL values to the best hypothesis
        // and also works in "-loglikelihood" space.
        // For final storage, renormalise the DLLS w.r.t. the pion hypothesis and
        // invert the values
        for ( const auto hypo : activeParticles() ) {
          if ( vDLLs[hypo] < 0 ) { vDLLs[hypo] = 0; }
          vDLLs[hypo] = ( LHCb::RichPID::DLL )( pionDLL - vDLLs[hypo] );
        }
        // -------------------------------------------------------------------------------

        // -------------------------------------------------------------------------------
        // Final checks
        // -------------------------------------------------------------------------------
        if ( !pid->isAboveThreshold( lightestActiveHypo() ) ) {
          warning() << "Lowest active mass hypothesis '" << lightestActiveHypo() << "' is below threshold ..."
                    << endmsg;
          warning() << *pid << endmsg;
        }
        // -------------------------------------------------------------------------------

        // print the final PID
        if ( !pidOK && !msgLevel( MSG::VERBOSE ) ) {
          _ri_debug << "  " << *pid << endmsg;
        } else {
          _ri_verbo << "  " << *pid << endmsg;
        }

        // insert and release
        rPIDs.insert( pid.release(), tk->key() );
      }

      _ri_verbo << "Created " << rPIDs.size() << " RichPIDs : Version " << (unsigned int)rPIDs.version() << endmsg;

      // return the PID objects
      return rPIDs;
    }

  private:
    // properties

    /// Data object version for RichPID container
    Gaudi::Property<unsigned short int> m_pidVersion{this, "PIDVersion", 2u, "Version of the RichPID container"};

  private:
    // messaging

    /// Non-zero best deltaLL value
    mutable WarningCounter m_nonZeroBestDLLWarn{this, "Track has non-zero best deltaLL value"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( WriteRichPIDs )

} // namespace Rich::Future::Rec::GlobalPID

DECLARE_COMPONENT_WITH_ID( EmptyProducer<LHCb::RichPIDs>, "RichPIDsEmptyProducer" )
