/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <memory>
#include <numeric>

// from Gaudi
#include "LHCbAlgs/MergingTransformer.h"

// Base class
#include "Event/ObjectContainersSharedMerger.h"
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event
#include "Event/RichPID.h"

namespace Rich::Future::Rec {

  /** @class MergeRichPIDs MergeRichPIDs.h
   *
   *  Merges RichPID objects from multiple containers into one.
   *
   *  @author Chris Jones
   *  @date   2016-11-05
   */

  class MergeRichPIDs final
      : public LHCb::Algorithm::MergingTransformer<LHCb::RichPIDs(
                                                       const Gaudi::Functional::vector_of_const_<LHCb::RichPIDs>& ),
                                                   Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    MergeRichPIDs( const std::string& name, ISvcLocator* pSvcLocator )
        : MergingTransformer( name, pSvcLocator,
                              // inputs
                              {"InputRichPIDLocations", {}},
                              // outputs
                              {"OutputRichPIDLocation", LHCb::RichPIDLocation::Default} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    LHCb::RichPIDs operator()( const Gaudi::Functional::vector_of_const_<LHCb::RichPIDs>& inPIDs ) const override;

  private:
    // properties

    /// Should the original RichPID be preserved in the cloned container ?
    Gaudi::Property<bool> m_keepPIDKey{this, "PreserveOriginalKeys", true};

    /// Data object version for RichPID container
    Gaudi::Property<unsigned short int> m_pidVersion{this, "PIDVersion", 2u, "Version of the RichPID container"};

  private:
    // messaging

    /// PID version mis-match
    mutable WarningCounter m_verWarn{this, "Input/Output RichPID version mis-match"};
  };

  // alternative class with SharedObjectsContainer
  using RichPIDContainersSharedMerger = LHCb::ObjectContainersSharedMerger<LHCb::RichPID, LHCb::RichPIDs>;

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

LHCb::RichPIDs MergeRichPIDs::operator()( const Gaudi::Functional::vector_of_const_<LHCb::RichPIDs>& inPIDs ) const {

  // the merged PID container
  LHCb::RichPIDs outPIDs;

  // reserve total size
  outPIDs.reserve( std::accumulate( inPIDs.begin(), inPIDs.end(), 0u,
                                    []( const auto sum, const auto& pids ) { return sum + pids.size(); } ) );

  // set the version
  outPIDs.setVersion( (unsigned char)m_pidVersion );

  // Loop over inputs and clone into output
  for ( const auto& pids : inPIDs ) {

    // Warn if the input and meged output containers have different versions
    if ( outPIDs.version() != pids.version() ) { ++m_verWarn; }

    // loop over PIDs and clone.
    for ( const auto pid : pids ) {

      // Make a new cloned RichPID object.
      auto newPID = std::make_unique<LHCb::RichPID>( *pid );

      // Should we try and preserve the original PID key ?
      if ( m_keepPIDKey ) {
        outPIDs.insert( newPID.get(), pid->key() );
      } else {
        outPIDs.insert( newPID.get() );
      }

      // if get here, insertion was OK so give up ownership
      newPID.release();
    }
  }

  _ri_verbo << "Created " << outPIDs.size() << " RichPIDs : Version " << (unsigned short)outPIDs.version() << endmsg;

  // return the output container
  return outPIDs;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( MergeRichPIDs )
DECLARE_COMPONENT_WITH_ID( RichPIDContainersSharedMerger, "RichPIDContainersSharedMerger" )

//=============================================================================
