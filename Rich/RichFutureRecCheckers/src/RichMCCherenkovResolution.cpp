/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

// FPE exception protection
#include "Kernel/FPEGuard.h"

// STD
#include <algorithm>
#include <array>
#include <cmath>
#include <mutex>

namespace Rich::Future::Rec::MC::Moni {

  /** @class CherenkovResolution RichCherenkovResolution.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class CherenkovResolution final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const CherenkovAngles::Vector&,                  //
                                               const CherenkovResolutions::Vector&,             //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    CherenkovResolution( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      setProperty( "NBins1DHistos", 100 ).ignore(); // to match number of bins in CKResParameterisation
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,     //
                     const LHCb::Track::Range&                       tracks,        //
                     const SIMDPixelSummaries&                       pixels,        //
                     const Rich::PDPixelCluster::Vector&             clusters,      //
                     const Relations::PhotonToParents::Vector&       photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&           segments,      //
                     const CherenkovAngles::Vector&                  expTkCKThetas, //
                     const CherenkovResolutions::Vector&             ckResolutions, //
                     const SIMDCherenkovPhoton::Vector&              photons,       //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// momentum bin
    inline std::size_t pullBin( const double ptot, const Rich::RadiatorType rad ) const {
      return ( ptot < m_minP[rad] ? 0 :                        //
                   ptot > m_maxP[rad] ? m_nPullBins[rad] - 1 : //
                       ( std::size_t )( ( ptot - m_minP[rad] ) * m_pullPtotInc[rad] ) );
    }

    /// min max for given momentum bin ( in GeV )
    std::pair<double, double> binMinMaxPtot( const std::size_t bin, const Rich::RadiatorType rad ) const {
      return {( m_minP[rad] + ( bin / m_pullPtotInc[rad] ) ) / Gaudi::Units::GeV,
              ( m_minP[rad] + ( ( bin + 1 ) / m_pullPtotInc[rad] ) ) / Gaudi::Units::GeV};
    }

    /// Get the histo ID string for a given bin
    inline std::string binToID( const std::size_t i ) const noexcept {
      return "Pulls/PtotBins/ckPull-Bin" + std::to_string( i );
    }

  private:
    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.0f, 0.0f, 0.0f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.010f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.056f, 0.033f}};

    /// Min for expected CK resolution
    Gaudi::Property<RadiatorArray<float>> m_ckResMin{this, "CKResMin", {0.0f, 0.00075f, 0.00040f}};

    /// Max for expected CK resolution
    Gaudi::Property<RadiatorArray<float>> m_ckResMax{this, "CKResMax", {0.025f, 0.0026f, 0.00102f}};

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this, "MinP", {0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV, 0.5 * Gaudi::Units::GeV}};

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this, "MaxP", {120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV}};

    /// Option to skip electrons
    Gaudi::Property<bool> m_skipElectrons{this, "SkipElectrons", false};

    /// Number of bins for binned pull plots
    Gaudi::Property<RadiatorArray<std::size_t>> m_nPullBins{this, "NPullBins", {200, 200, 200}};

  private:
    // cached data

    /// mutex lock
    mutable std::mutex m_updateLock;

    /// 1/increment for pull ptot binning
    RadiatorArray<double> m_pullPtotInc = {0, 0, 0};

    // cached histogram pointers

    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_expCKang      = {{}};
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_ckres         = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_ckresVcktheta = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_ckresVptot    = {{}};

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVckang     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVPtot      = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVckang = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVPtot  = {{}};

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVckangInner     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVPtotInner      = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVckangInner = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVPtotInner  = {{}};

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVckangOuter     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_diffckVPtotOuter      = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVckangOuter = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsdiffckVPtotOuter  = {{}};

    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_ckPullVckang     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_ckPullVPtot      = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsCkPullVckang = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_fabsCkPullVPtot  = {{}};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode CherenkovResolution::prebookHistograms() {
  using namespace Gaudi::Units;

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : activeRadiators() ) {

    // cache some numbers for later use
    assert( m_maxP[rad] > m_minP[rad] );
    assert( 0 < m_minP[rad] );
    m_pullPtotInc[rad] = m_nPullBins[rad] / ( m_maxP[rad] - m_minP[rad] );

    // Loop over all particle codes
    for ( const auto hypo : activeParticlesNoBT() ) {
      // track plots
      ok &= saveAndCheck( h_expCKang[rad][hypo],                                        //
                          richHisto1D( HID( "expCKang", rad, hypo ),                    //
                                       "Expected CK angle",                             //
                                       m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), //
                                       "Cherenkov Theta / rad" ) );
      ok &= saveAndCheck( h_ckres[rad][hypo],                                       //
                          richHisto1D( HID( "ckres", rad, hypo ),                   //
                                       "Calculated CKres",                          //
                                       m_ckResMin[rad], m_ckResMax[rad], nBins1D(), //
                                       "Delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_ckresVcktheta[rad][hypo],                       //
                          richProfile1D( HID( "ckresVcktheta", rad, hypo ), //
                                         "Calculated CKres V CKangle",      //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], m_nPullBins[rad],
                                         "Cherenkov Theta / rad", "Cherenkov Theta Resolution / rad" ) );
      ok &= saveAndCheck( h_ckresVptot[rad][hypo],                                   //
                          richProfile1D( HID( "ckresVptot", rad, hypo ),             //
                                         "Calculated CKres V ptot",                  //
                                         m_minP[rad], m_maxP[rad], m_nPullBins[rad], //
                                         "Momentum / MeV/c", "Cherenkov Theta Resolution / rad" ) );

      // photon plots
      // Combined
      ok &= saveAndCheck( h_diffckVckang[rad][hypo],                                      //
                          richProfile1D( HID( "diffckVckang", rad, hypo ),                //
                                         "Rec-Exp CKtheta V CKtheta - MC true photons",   //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), //
                                         "Cherenkov Theta / rad", "delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_diffckVPtot[rad][hypo],                                  //
                          richProfile1D( HID( "diffckVPtot", rad, hypo ),            //
                                         "Rec-Exp CKtheta V ptot - MC true photons", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),        //
                                         "Momentum / MeV/c", "delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_fabsdiffckVckang[rad][hypo],
                          richProfile1D( HID( "fabsdiffckVckang", rad, hypo ),                //
                                         "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),     //
                                         "Cherenkov Theta / rad", "abs(delta(Cherenkov Theta)) / rad" ) );
      ok &= saveAndCheck( h_fabsdiffckVPtot[rad][hypo],                                    //
                          richProfile1D( HID( "fabsdiffckVPtot", rad, hypo ),              //
                                         "fabs(Rec-Exp) CKtheta V ptot - MC true photons", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),              //
                                         "Momentum / MeV/c", "abs(delta(Cherenkov Theta)) / rad" ) );
      // Inner Regions
      ok &= saveAndCheck( h_diffckVckangInner[rad][hypo],                                              //
                          richProfile1D( HID( "diffckVckangInner", rad, hypo ),                        //
                                         "Rec-Exp CKtheta V CKtheta - MC true photons - Inner Region", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),              //
                                         "Cherenkov Theta / rad", "delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_diffckVPtotInner[rad][hypo],                                            //
                          richProfile1D( HID( "diffckVPtotInner", rad, hypo ),                      //
                                         "Rec-Exp CKtheta V ptot - MC true photons - Inner Region", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),                       //
                                         "Momentum / MeV/c", "delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_fabsdiffckVckangInner[rad][hypo],
                          richProfile1D( HID( "fabsdiffckVckangInner", rad, hypo ),                          //
                                         "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons - Inner Region", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),                    //
                                         "Cherenkov Theta / rad", "abs(delta(Cherenkov Theta)) / rad" ) );
      ok &= saveAndCheck( h_fabsdiffckVPtotInner[rad][hypo],                                              //
                          richProfile1D( HID( "fabsdiffckVPtotInner", rad, hypo ),                        //
                                         "fabs(Rec-Exp) CKtheta V ptot - MC true photons - Inner Region", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),                             //
                                         "Momentum / MeV/c", "abs(delta(Cherenkov Theta)) / rad" ) );
      // Outer Regions
      ok &= saveAndCheck( h_diffckVckangOuter[rad][hypo],                                              //
                          richProfile1D( HID( "diffckVckangOuter", rad, hypo ),                        //
                                         "Rec-Exp CKtheta V CKtheta - MC true photons - Outer Region", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),              //
                                         "Cherenkov Theta / rad", "delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_diffckVPtotOuter[rad][hypo],                                            //
                          richProfile1D( HID( "diffckVPtotOuter", rad, hypo ),                      //
                                         "Rec-Exp CKtheta V ptot - MC true photons - Outer Region", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),                       //
                                         "Momentum / MeV/c", "delta(Cherenkov Theta) / rad" ) );
      ok &= saveAndCheck( h_fabsdiffckVckangOuter[rad][hypo],
                          richProfile1D( HID( "fabsdiffckVckangOuter", rad, hypo ),                          //
                                         "fabs(Rec-Exp) CKtheta V CKtheta - MC true photons - Outer Region", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),                    //
                                         "Cherenkov Theta / rad", "abs(delta(Cherenkov Theta)) / rad" ) );
      ok &= saveAndCheck( h_fabsdiffckVPtotOuter[rad][hypo],                                              //
                          richProfile1D( HID( "fabsdiffckVPtotOuter", rad, hypo ),                        //
                                         "fabs(Rec-Exp) CKtheta V ptot - MC true photons - Outer Region", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),                             //
                                         "Momentum / MeV/c", "abs(delta(Cherenkov Theta)) / rad" ) );

      // pull plots
      ok &= saveAndCheck( h_ckPullVckang[rad][hypo],                                          //
                          richProfile1D( HID( "ckPullVckang", rad, hypo ),                    //
                                         "(Rec-Exp)/Res CKtheta V CKtheta - MC true photons", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),     //
                                         "Cherenkov Theta / rad", "Cherenkov Resolution Pull" ) );
      ok &= saveAndCheck( h_ckPullVPtot[rad][hypo],                                        //
                          richProfile1D( HID( "ckPullVPtot", rad, hypo ),                  //
                                         "(Rec-Exp)/Res CKtheta V ptot - MC true photons", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),              //
                                         "Momentum / MeV/c", "Cherenkov Resolution Pull" ) );
      ok &= saveAndCheck( h_fabsCkPullVckang[rad][hypo],
                          richProfile1D( HID( "fabsCkPullVckang", rad, hypo ),                      //
                                         "fabs((Rec-Exp)/Res) CKtheta V CKtheta - MC true photons", //
                                         m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(),           //
                                         "Cherenkov Theta / rad", "abs(Cherenkov Resolution Pull)" ) );
      ok &= saveAndCheck( h_fabsCkPullVPtot[rad][hypo],                                          //
                          richProfile1D( HID( "fabsCkPullVPtot", rad, hypo ),                    //
                                         "fabs((Rec-Exp)/Res) CKtheta V ptot - MC true photons", //
                                         m_minP[rad], m_maxP[rad], nBins1D(),                    //
                                         "Momentum / MeV/c", "abs(Cherenkov Resolution Pull)" ) );
      // 1D plots binned in ptot
      for ( std::size_t i = 0; i < m_nPullBins[rad]; ++i ) {
        // min/max Ptot for this bin
        const auto binEdges = binMinMaxPtot( i, rad );
        // hist title string
        std::ostringstream title;
        title << "(Rec-Exp)/Res CKtheta - MC true photons - Bin " << i << " Ptot " << binEdges.first << " to "
              << binEdges.second << " GeV";
        // book the histo
        richHisto1D( HID( binToID( i ), rad, hypo ), title.str(), -5, 5, nBins1D() );
      }

    } // active particles
  }

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void CherenkovResolution::operator()( const Summary::Track::Vector&                   sumTracks,     //
                                      const LHCb::Track::Range&                       tracks,        //
                                      const SIMDPixelSummaries&                       pixels,        //
                                      const Rich::PDPixelCluster::Vector&             clusters,      //
                                      const Relations::PhotonToParents::Vector&       photToSegPix,  //
                                      const LHCb::RichTrackSegment::Vector&           segments,      //
                                      const CherenkovAngles::Vector&                  expTkCKThetas, //
                                      const CherenkovResolutions::Vector&             ckResolutions, //
                                      const SIMDCherenkovPhoton::Vector&              photons,       //
                                      const Rich::Future::MC::Relations::TkToMCPRels& tkrels,        //
                                      const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  Helper mcHelper( tkrels, digitSums );

  // the lock
  std::lock_guard lock( m_updateLock );

  // loop over the track info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {
    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) { continue; }

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // get the resolutions for this segment
      const auto& ckRes = ckResolutions[rels.segmentIndex()];

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // Get the MCParticles for this track
      const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // loop over mass hypos
      for ( const auto hypo : activeParticlesNoBT() ) {
        if ( expCKangles[hypo] > 0 ) {
          fillHisto( h_expCKang[rad][hypo], expCKangles[hypo] );
          fillHisto( h_ckres[rad][hypo], ckRes[hypo] );
          fillHisto( h_ckresVcktheta[rad][hypo], expCKangles[hypo], ckRes[hypo] );
          fillHisto( h_ckresVptot[rad][hypo], pTot, ckRes[hypo] );
        }
      }

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) continue;

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, skip
          if ( Rich::Unknown == pid ) continue;
          // skip electrons which are reconstructed badly..
          if ( m_skipElectrons && Rich::Electron == pid ) continue;
          // finally check if PID is in the active list
          if ( !particleIsActive( pid ) ) { continue; }

          // beta cut for true MC type
          const auto mcbeta = richPartProps()->beta( pTot, pid );
          if ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] ) {

            // true Cherenkov signal ?
            const bool trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

            // expected CK theta ( for true type )
            const auto thetaExp = expCKangles[pid];

            // delta theta
            const auto deltaTheta = thetaRec - thetaExp;

            // fill some plots for true photons only
            if ( trueCKSig ) {

              // Res versus Ptot and CK theta
              fillHisto( h_diffckVckang[rad][pid], thetaExp, deltaTheta, mcPW );
              fillHisto( h_diffckVPtot[rad][pid], pTot, deltaTheta, mcPW );
              fillHisto( h_fabsdiffckVckang[rad][pid], thetaExp, fabs( deltaTheta ), mcPW );
              fillHisto( h_fabsdiffckVPtot[rad][pid], pTot, fabs( deltaTheta ), mcPW );
              if ( simdPix.isInnerRegion()[i] ) {
                fillHisto( h_diffckVckangInner[rad][pid], thetaExp, deltaTheta, mcPW );
                fillHisto( h_diffckVPtotInner[rad][pid], pTot, deltaTheta, mcPW );
                fillHisto( h_fabsdiffckVckangInner[rad][pid], thetaExp, fabs( deltaTheta ), mcPW );
                fillHisto( h_fabsdiffckVPtotInner[rad][pid], pTot, fabs( deltaTheta ), mcPW );
              } else {
                fillHisto( h_diffckVckangOuter[rad][pid], thetaExp, deltaTheta, mcPW );
                fillHisto( h_diffckVPtotOuter[rad][pid], pTot, deltaTheta, mcPW );
                fillHisto( h_fabsdiffckVckangOuter[rad][pid], thetaExp, fabs( deltaTheta ), mcPW );
                fillHisto( h_fabsdiffckVPtotOuter[rad][pid], pTot, fabs( deltaTheta ), mcPW );
              }

              // pulls
              if ( ckRes[pid] > 0 ) {
                const auto pull = deltaTheta / ckRes[pid];
                fillHisto( richHisto1D( HID( binToID( pullBin( pTot, rad ) ), rad, pid ) ), pull, mcPW );
                fillHisto( h_ckPullVckang[rad][pid], thetaExp, pull, mcPW );
                fillHisto( h_ckPullVPtot[rad][pid], pTot, pull, mcPW );
                fillHisto( h_fabsCkPullVckang[rad][pid], thetaExp, fabs( pull ), mcPW );
                fillHisto( h_fabsCkPullVPtot[rad][pid], pTot, fabs( pull ), mcPW );
              }
            }

          } // beta selection

        } // loop over associated MCPs

      } // scalar loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CherenkovResolution )

//-----------------------------------------------------------------------------
