/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichFutureUtils/RichSmartIDs.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Event model
#include "Event/MCRichDigitSummary.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Relations
#include "RichFutureMCUtils/RichMCHitUtils.h"
#include "RichFutureMCUtils/RichMCOpticalPhotonUtils.h"
#include "RichFutureMCUtils/RichRecMCHelper.h"

// Kernel
#include "LHCbMath/FastMaths.h"

// STD
#include <algorithm>
#include <cmath>
#include <cstdint>
#include <mutex>

namespace Rich::Future::Rec::MC::Moni {

  // MC helpers
  using namespace Rich::Future::MC::Relations;

  /** @class OpticalPhotons RichMCOpticalPhotons.h
   *
   *  Resolutions using extended MC Optical Photon objects
   *
   *  @author Chris Jones
   *  @date   2021-08-02
   */

  class OpticalPhotons final : public LHCb::Algorithm::Consumer<
                                   void( const Summary::Track::Vector&,                   //
                                         const LHCb::Track::Range&,                       //
                                         const SIMDPixelSummaries&,                       //
                                         const Rich::PDPixelCluster::Vector&,             //
                                         const Relations::PhotonToParents::Vector&,       //
                                         const LHCb::RichTrackSegment::Vector&,           //
                                         const Relations::SegmentToTrackVector&,          //
                                         const SIMDCherenkovPhoton::Vector&,              //
                                         const Rich::Future::MC::Relations::TkToMCPRels&, //
                                         const PhotonYields::Vector&,                     //
                                         const LHCb::MCRichDigitSummarys&,                //
                                         const LHCb::MCRichHits&,                         //
                                         const LHCb::MCRichOpticalPhotons&,               //
                                         const Rich::Utils::RichSmartIDs& ),
                                   LHCb::DetDesc::usesBaseAndConditions<HistoAlgBase, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    OpticalPhotons( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"SegmentToTrackLocation", Relations::SegmentToTrackLocation::Default},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"PhotonYieldLocation", PhotonYieldsLocation::Detectable},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default},
                     KeyValue{"MCRichHitsLocation", LHCb::MCRichHitLocation::Default},
                     KeyValue{"MCRichOpticalPhotonsLocation", LHCb::MCRichOpticalPhotonLocation::Default},
                     // input conditions data
                     KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
    }

    /// Initialize
    StatusCode initialize() override {
      // base class initialise
      return Consumer::initialize().andThen( [&] {
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
      } );
    }

  protected:
    StatusCode prebookHistograms() override {

      bool ok = true;

      using namespace Gaudi::Units;

      const DetectorArray<double> panelXsizes{{650, 800}};
      const DetectorArray<double> panelYsizes{{680, 750}};
      const DetectorArray<double> richXsizes{{300, 1200}};
      const DetectorArray<double> richYsizes{{300, 1200}};
      const DetectorArray<double> shiftXrange{{10, 100}};
      const DetectorArray<double> shiftYrange{{10, 25}};

      // loop over RICHes
      for ( const auto det : activeDetectors() ) {
        // Global X,Y Shifts
        ok &= saveAndCheck( h_pixXGloShift[det],                                         //
                            richHisto1D( HID( "pixXGloShift", det ),                     //
                                         "Reco-MC Global X hit position",                //
                                         -shiftXrange[det], shiftXrange[det], nBins1D(), //
                                         "Reco-MC Global X hit position / mm" ) );
        ok &= saveAndCheck( h_pixYGloShift[det],                                         //
                            richHisto1D( HID( "pixYGloShift", det ),                     //
                                         "Reco-MC Global Y hit position",                //
                                         -shiftYrange[det], shiftYrange[det], nBins1D(), //
                                         "Reco-MC Global Y hit position / mm" ) );
        ok &= saveAndCheck( h_pixXGloShift2D[det],                                                //
                            richProfile2D( HID( "pixXGloShift2D", det ),                          //
                                           "<Rec-MC Global X> hit position on (x,y) local plane", //
                                           -panelXsizes[det], panelXsizes[det], nBins2D(),        //
                                           -panelYsizes[det], panelYsizes[det], nBins2D(),        //
                                           "Panel Local X / mm", "Panel Local Y / mm" ) );
        ok &= saveAndCheck( h_pixYGloShift2D[det],                                                //
                            richProfile2D( HID( "pixYGloShift2D", det ),                          //
                                           "<Rec-MC Global Y> hit position on (x,y) local plane", //
                                           -panelXsizes[det], panelXsizes[det], nBins2D(),        //
                                           -panelYsizes[det], panelYsizes[det], nBins2D(),        //
                                           "Panel Local X / mm", "Panel Local Y / mm" ) );
        // Local X,Y Shifts
        ok &= saveAndCheck( h_pixXLocShift[det],                                         //
                            richHisto1D( HID( "pixXLocShift", det ),                     //
                                         "Reco-MC Local X hit position",                 //
                                         -shiftXrange[det], shiftXrange[det], nBins1D(), //
                                         "Reco-MC Local X hit position / mm" ) );
        ok &= saveAndCheck( h_pixYLocShift[det],                                         //
                            richHisto1D( HID( "pixYLocShift", det ),                     //
                                         "Reco-MC Local Y hit position",                 //
                                         -shiftYrange[det], shiftYrange[det], nBins1D(), //
                                         "Reco-MC Local Y hit position / mm" ) );
        ok &= saveAndCheck( h_pixXLocShift2D[det],                                               //
                            richProfile2D( HID( "pixXLocShift2D", det ),                         //
                                           "<Rec-MC Local X> hit position on (x,y) local plane", //
                                           -panelXsizes[det], panelXsizes[det], nBins2D(),       //
                                           -panelYsizes[det], panelYsizes[det], nBins2D(),       //
                                           "Panel Local X / mm", "Panel Local Y / mm" ) );
        ok &= saveAndCheck( h_pixYLocShift2D[det],                                               //
                            richProfile2D( HID( "pixYLocShift2D", det ),                         //
                                           "<Rec-MC Local Y> hit position on (x,y) local plane", //
                                           -panelXsizes[det], panelXsizes[det], nBins2D(),       //
                                           -panelYsizes[det], panelYsizes[det], nBins2D(),       //
                                           "Panel Local X / mm", "Panel Local Y / mm" ) );
      }

      // Loop over radiators
      for ( const auto rad : activeRadiators() ) {
        ok &= saveAndCheck( h_tkAngle[rad],                                           //
                            richHisto1D( HID( "tkAngle", rad ),                       //
                                         "Angle between Reco and MC Track Direction", //
                                         0, m_maxTkAng[rad], nBins1D(),               //
                                         "Rec-MC Track Angle / mrad" ) );
        ok &= saveAndCheck( h_tkAngleVP[rad],                                           //
                            richProfile1D( HID( "tkAngleVP", rad ),                     //
                                           "Angle between Reco and MC Track Direction", //
                                           m_minP[rad], m_maxP[rad], nBins1D(),         //
                                           "Track Momentum (MeV/c)", "<Rec-MC Track Angle> / mrad" ) );
        ok &= saveAndCheck( h_tkAngleX[rad],                                                          //
                            richHisto1D( HID( "tkAngleX", rad ),                                      //
                                         "X Projection of angle between Reco and MC Track Direction", //
                                         0, m_maxTkAng[rad], nBins1D(),                               //
                                         "Rec-MC Track Angle-X / mrad" ) );
        ok &= saveAndCheck( h_tkAngleXVP[rad],                                                          //
                            richProfile1D( HID( "tkAngleXVP", rad ),                                    //
                                           "X Projection of angle between Reco and MC Track Direction", //
                                           m_minP[rad], m_maxP[rad], nBins1D(),                         //
                                           "Track Momentum (MeV/c)", "<Rec-MC Track Angle-X> / mrad" ) );
        ok &= saveAndCheck( h_tkAngleY[rad],                                                          //
                            richHisto1D( HID( "tkAngleY", rad ),                                      //
                                         "Y Projection of angle between Reco and MC Track Direction", //
                                         0, m_maxTkAng[rad], nBins1D(),                               //
                                         "Rec-MC Track Angle-Y / mrad" ) );
        ok &= saveAndCheck( h_tkAngleYVP[rad],                                                          //
                            richProfile1D( HID( "tkAngleYVP", rad ),                                    //
                                           "Y Projection of angle between Reco and MC Track Direction", //
                                           m_minP[rad], m_maxP[rad], nBins1D(),                         //
                                           "Track Momentum (MeV/c)", "<Rec-MC Track Angle-Y> / mrad" ) );
        // loop over active mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {
          const auto det = ( Rich::Rich2Gas == rad ? Rich::Rich2 : Rich::Rich1 );
          // Book MC yield histos
          ok &= saveAndCheck( h_mcYield[rad][pid],                                      //
                              richHisto1D( HID( "mcYield", rad, pid ),                  //
                                           "MC Photon Yield (>0)",                      //
                                           0.5, m_maxYield[rad] + 0.5, m_maxYield[rad], //
                                           "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_mcYieldVp[rad][pid],                              //
                              richProfile1D( HID( "mcYieldVp", rad, pid ),        //
                                             "MC Photon Yield (>0) V P (MeV/c)",  //
                                             m_minP[rad], m_maxP[rad], nBins1D(), //
                                             "Track Momentum (MeV/c)", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_mcYieldVx[rad][pid],                                       //
                              richProfile1D( HID( "mcYieldVx", rad, pid ),                 //
                                             "MC Photon Yield (>0) V Panel Local X",       //
                                             -richXsizes[det], richXsizes[det], nBins1D(), //
                                             "Panel Local X / mm", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_mcYieldVy[rad][pid],                                       //
                              richProfile1D( HID( "mcYieldVy", rad, pid ),                 //
                                             "MC Photon Yield (>0) V Panel Local Y",       //
                                             -richYsizes[det], richYsizes[det], nBins1D(), //
                                             "Panel Local Y / mm", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_mcYieldVxy[rad][pid],                                      //
                              richProfile2D( HID( "mcYieldVxy", rad, pid ),                //
                                             "MC Photon Yield (>0) V Panel Local (X,Y)",   //
                                             -richXsizes[det], richXsizes[det], nBins2D(), //
                                             -richYsizes[det], richYsizes[det], nBins2D(), //
                                             "Panel Local X / mm", "Panel Local Y / mm", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_mcYieldDiff[rad][pid],                         //
                              richHisto1D( HID( "mcYieldDiff", rad, pid ),     //
                                           "MC Expected-MC Photon Yield (>0)", //
                                           -50.0, 50.0, nBins1D(),             //
                                           "Exp-MC Photon Yield" ) );
          ok &= saveAndCheck( h_mcYieldDiffVp[rad][pid],                                     //
                              richProfile1D( HID( "mcYieldDiffVp", rad, pid ),               //
                                             "MC Expected-MC Photon Yield (>0) V P (MeV/c)", //
                                             1.0 * GeV, 100.0 * GeV, nBins1D(),              //
                                             "Track Momentum (MeV/c)", "Exp-MC Photon Yield" ) );
          // book reco yield histos
          ok &= saveAndCheck( h_recoYield[rad][pid],                                    //
                              richHisto1D( HID( "recoYield", rad, pid ),                //
                                           "Reco Photon Yield (>0)",                    //
                                           0.5, m_maxYield[rad] + 0.5, m_maxYield[rad], //
                                           "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_recoYieldVp[rad][pid],                             //
                              richProfile1D( HID( "recoYieldVp", rad, pid ),       //
                                             "Reco Photon Yield (>0) V P (MeV/c)", //
                                             m_minP[rad], m_maxP[rad], nBins1D(),  //
                                             "Track Momentum (MeV/c)", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_recoYieldVx[rad][pid],                                     //
                              richProfile1D( HID( "recoYieldVx", rad, pid ),               //
                                             "Reco Photon Yield (>0) V Panel Local X",     //
                                             -richXsizes[det], richXsizes[det], nBins1D(), //
                                             "Panel Local X / mm", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_recoYieldVy[rad][pid],                                     //
                              richProfile1D( HID( "recoYieldVy", rad, pid ),               //
                                             "Reco Photon Yield (>0) V Panel Local Y",     //
                                             -richYsizes[det], richYsizes[det], nBins1D(), //
                                             "Panel Local Y / mm", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_recoYieldVxy[rad][pid],                                    //
                              richProfile2D( HID( "recoYieldVxy", rad, pid ),              //
                                             "Reco Photon Yield (>0) V Panel Local (X,Y)", //
                                             -richXsizes[det], richXsizes[det], nBins2D(), //
                                             -richYsizes[det], richYsizes[det], nBins2D(), //
                                             "Panel Local X / mm", "Panel Local Y / mm", "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_recoYieldDiff[rad][pid],                         //
                              richHisto1D( HID( "recoYieldDiff", rad, pid ),     //
                                           "Reco Expected-MC Photon Yield (>0)", //
                                           -50.0, 50.0, nBins1D(),               //
                                           "Exp-MC Photon Yield" ) );
          ok &= saveAndCheck( h_recoYieldDiffVp[rad][pid],                                     //
                              richProfile1D( HID( "recoYieldDiffVp", rad, pid ),               //
                                             "Reco Expected-MC Photon Yield (>0) V P (MeV/c)", //
                                             1.0 * GeV, 100.0 * GeV, nBins1D(),                //
                                             "Track Momentum (MeV/c)", "Exp-MC Photon Yield" ) );
        }
      }

      return StatusCode{ok};
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,    //
                     const LHCb::Track::Range&                       tracks,       //
                     const SIMDPixelSummaries&                       pixels,       //
                     const Rich::PDPixelCluster::Vector&             clusters,     //
                     const Relations::PhotonToParents::Vector&       photToSegPix, //
                     const LHCb::RichTrackSegment::Vector&           segments,     //
                     const Relations::SegmentToTrackVector&          segToTkRel,   //
                     const SIMDCherenkovPhoton::Vector&              photons,      //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,       //
                     const PhotonYields::Vector&                     yields,       //
                     const LHCb::MCRichDigitSummarys&                digitSums,    //
                     const LHCb::MCRichHits&                         mchits,       //
                     const LHCb::MCRichOpticalPhotons&               mcphotons,    //
                     const Rich::Utils::RichSmartIDs&                smartIDsHelper ) const override {

      // Make a local MC helper object
      const Helper mcHelper( tkrels, digitSums );
      // MC hit helper
      const MCHitUtils hitHelper( mchits );
      // MC OpticalPhoton helper
      const MCOpticalPhotonUtils photHelper( mcphotons );

      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over segments
      for ( const auto&& [seg, tkIndex, yield] : Ranges::ConstZip( segments, segToTkRel, yields ) ) {

        // radiator
        const auto rad = seg.radiator();
        if ( !radiatorIsActive( rad ) ) { continue; }

        // Segment momentum
        const auto pTot = seg.bestMomentumMag();
        if ( pTot < m_minP[rad] || pTot > m_maxP[rad] ) { continue; }

        // Track pointer
        const auto tk = tracks.at( tkIndex );

        // MCP Pointers
        const auto mcPs = mcHelper.mcParticles( *tk );

        // Loop over MCPs
        for ( const auto mcP : mcPs ) {
          // The True MCParticle type
          const auto pid = mcHelper.mcParticleType( mcP );
          if ( mcP && pid != Rich::Unknown ) {
            // beta cut for true MC type
            const auto mcbeta = richPartProps()->beta( pTot, pid );
            const auto betaC  = ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] );
            if ( !betaC ) { continue; }
            if ( yield[pid] > 0 ) {
              // MC Optical Photons
              const auto mcPhots = photHelper.mcOpticalPhotons( mcP );
              // Count MC signal photons in this radiator
              const auto nPhots =                                //
                  std::count_if( mcPhots.begin(), mcPhots.end(), //
                                 [&rad]( const auto mcPhot ) {
                                   const auto mcH = ( mcPhot ? mcPhot->mcRichHit() : nullptr );
                                   return ( mcH && mcH->radiator() == rad && mcH->isSignal() );
                                 } );
              if ( nPhots > 0 ) {
                fillHisto( h_mcYield[rad][pid], nPhots );
                fillHisto( h_mcYieldVp[rad][pid], pTot, nPhots );
                fillHisto( h_mcYieldVx[rad][pid], seg.entryPoint().X(), nPhots );
                fillHisto( h_mcYieldVy[rad][pid], seg.entryPoint().Y(), nPhots );
                fillHisto( h_mcYieldVxy[rad][pid], seg.entryPoint().X(), seg.entryPoint().Y(), nPhots );
                const auto yield_diff = yield[pid] - (double)nPhots;
                fillHisto( h_mcYieldDiff[rad][pid], yield_diff );
                fillHisto( h_mcYieldDiffVp[rad][pid], pTot, yield_diff );
              }
            }
          }
        } // MCPs

      } // segment data

      // loop over the photon info
      for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

        // Get the MCParticles for this track
        const auto mcPs = mcHelper.mcParticles( *tk, false, 0.75 );

        // Need MC info so skip tracks without any
        if ( mcPs.empty() ) { continue; }

        // True MC Signal counts for this track
        RadiatorArray<std::size_t>  nSigPhots{{}};
        RadiatorArray<std::int32_t> segIndex{-1, -1, -1};

        // loop over photons for this track
        for ( const auto photIn : sumTk.photonIndices() ) {

          // photon data
          const auto& phot = photons[photIn];
          const auto& rels = photToSegPix[photIn];

          // Get the SIMD summary pixel
          const auto& simdPix = pixels[rels.pixelIndex()];

          // the segment for this photon
          const auto& seg = segments[rels.segmentIndex()];

          // Rich / Radiator info
          const auto rich = seg.rich();
          const auto rad  = seg.radiator();
          segIndex[rad]   = rels.segmentIndex();
          if ( !radiatorIsActive( rad ) ) { continue; }

          // Segment momentum
          const auto pTot = seg.bestMomentumMag();
          if ( pTot < m_minP[rad] || pTot > m_maxP[rad] ) { continue; }

          // Pixel position
          const auto& pixPtnGlo = simdPix.gloPos();
          const auto& pixPtnLoc = simdPix.locPos();

          // Loop over scalar entries in SIMD photon
          for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
            // Select valid entries
            if ( !phot.validityMask()[i] ) { continue; }

            // scalar cluster
            const auto& clus = clusters[simdPix.scClusIndex()[i]];

            // get the MCHits for this cluster
            const auto mcHits = hitHelper.mcRichHits( clus );
            // ... and then the MC Optical Photons also linked to this track's MCParticle(s)
            const auto mcPhots = photHelper.mcOpticalPhotons( mcHits, mcPs );

            // Weight per MCPhoton
            const auto mcPW = ( !mcPhots.empty() ? 1.0 / (double)mcPhots.size() : 1.0 );

            // Count true signal reco photons for each segment
            if ( !mcPhots.empty() ) { ++nSigPhots[rad]; }

            // Loop over MC photons
            for ( const auto mcPhot : mcPhots ) {

              // get angle between reco track segment and MC Photon parent direction at emission
              double theta{0}, phi{0};
              seg.angleToDirection<LHCb::RichTrackSegment::FULL_PRECISION, LHCb::RichTrackSegment::FULL_PRECISION>(
                  mcPhot->parentMomentum(), theta, phi );
              theta = 1000 * std::abs( theta ); // convert to use mrad for plots for CK theta
              if ( theta <= m_maxTkAng[rad] ) {
                fillHisto( h_tkAngle[rad], theta, mcPW );
                fillHisto( h_tkAngleVP[rad], pTot, theta, mcPW );
                // projections in x and y planes
                double sinPhi{0}, cosPhi{0};
                LHCb::Math::fast_sincos( phi, sinPhi, cosPhi );
                fillHisto( h_tkAngleY[rad], std::abs( theta * sinPhi ), mcPW );
                fillHisto( h_tkAngleYVP[rad], pTot, std::abs( theta * sinPhi ), mcPW );
                fillHisto( h_tkAngleX[rad], std::abs( theta * cosPhi ), mcPW );
                fillHisto( h_tkAngleXVP[rad], pTot, std::abs( theta * cosPhi ), mcPW );
              }

              // Hit position resolution
              const auto&  mcHitPtnGlo = mcPhot->pdIncidencePoint();
              const auto   mcHitPtnLoc = smartIDsHelper.globalToPDPanel( mcHitPtnGlo );
              const double xLoc        = pixPtnLoc.X()[i];
              const double yLoc        = pixPtnLoc.Y()[i];
              const double xDiffGlo    = pixPtnGlo.X()[i] - mcHitPtnGlo.X();
              const double yDiffGlo    = pixPtnGlo.Y()[i] - mcHitPtnGlo.Y();
              const double xDiffLoc    = xLoc - mcHitPtnLoc.X();
              const double yDiffLoc    = yLoc - mcHitPtnLoc.Y();
              fillHisto( h_pixXGloShift[rich], xDiffGlo );
              fillHisto( h_pixYGloShift[rich], yDiffGlo );
              fillHisto( h_pixXGloShift2D[rich], xLoc, yLoc, xDiffGlo );
              fillHisto( h_pixYGloShift2D[rich], xLoc, yLoc, yDiffGlo );
              fillHisto( h_pixXLocShift[rich], xDiffLoc );
              fillHisto( h_pixYLocShift[rich], yDiffLoc );
              fillHisto( h_pixXLocShift2D[rich], xLoc, yLoc, xDiffLoc );
              fillHisto( h_pixYLocShift2D[rich], xLoc, yLoc, yDiffLoc );

            } // MC photons

          } // SIMD scalar loop

        } // photons

        // Reco yield plots
        for ( const auto rad : activeRadiators() ) {
          if ( segIndex[rad] >= 0 ) {
            const auto pid = mcHelper.mcParticleType( mcPs.front() );
            if ( pid != Rich::Unknown ) {
              const auto& yield = yields[segIndex[rad]];
              if ( yield[pid] > 0 ) {
                const auto& seg  = segments[segIndex[rad]];
                const auto  pTot = seg.bestMomentumMag();
                // beta cut for true MC type
                const auto mcbeta = richPartProps()->beta( pTot, pid );
                const auto betaC  = ( mcbeta >= m_minBeta[rad] && mcbeta <= m_maxBeta[rad] );
                if ( !betaC ) { continue; }
                const auto nPhots = nSigPhots[rad];
                if ( nPhots > 0 ) {
                  fillHisto( h_recoYield[rad][pid], nPhots );
                  fillHisto( h_recoYieldVp[rad][pid], pTot, nPhots );
                  fillHisto( h_recoYieldVx[rad][pid], seg.entryPoint().X(), nPhots );
                  fillHisto( h_recoYieldVy[rad][pid], seg.entryPoint().Y(), nPhots );
                  fillHisto( h_recoYieldVxy[rad][pid], seg.entryPoint().X(), seg.entryPoint().Y(), nPhots );
                  const auto yield_diff = yield[pid] - (double)nPhots;
                  fillHisto( h_recoYieldDiff[rad][pid], yield_diff );
                  fillHisto( h_recoYieldDiffVp[rad][pid], pTot, yield_diff );
                }
              }
            }
          }
        }

      } // tracks
    }

  private:
    /// mutex lock
    mutable std::mutex m_updateLock;

  private:
    // histos

    RadiatorArray<AIDA::IHistogram1D*> h_tkAngle    = {{}};
    RadiatorArray<AIDA::IProfile1D*>   h_tkAngleVP  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_tkAngleX   = {{}};
    RadiatorArray<AIDA::IProfile1D*>   h_tkAngleXVP = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_tkAngleY   = {{}};
    RadiatorArray<AIDA::IProfile1D*>   h_tkAngleYVP = {{}};

    DetectorArray<AIDA::IHistogram1D*> h_pixXGloShift   = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixYGloShift   = {{}};
    DetectorArray<AIDA::IProfile2D*>   h_pixXGloShift2D = {{}};
    DetectorArray<AIDA::IProfile2D*>   h_pixYGloShift2D = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixXLocShift   = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_pixYLocShift   = {{}};
    DetectorArray<AIDA::IProfile2D*>   h_pixXLocShift2D = {{}};
    DetectorArray<AIDA::IProfile2D*>   h_pixYLocShift2D = {{}};

    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_mcYield       = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_mcYieldVp     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_mcYieldVx     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_mcYieldVy     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile2D*>>   h_mcYieldVxy    = {{}};
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_mcYieldDiff   = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_mcYieldDiffVp = {{}};

    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_recoYield       = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_recoYieldVp     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_recoYieldVx     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_recoYieldVy     = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile2D*>>   h_recoYieldVxy    = {{}};
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_recoYieldDiff   = {{}};
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>>   h_recoYieldDiffVp = {{}};

  private:
    // JOs

    /// Min momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this, "MinP", {2.0 * Gaudi::Units::GeV, 3.0 * Gaudi::Units::GeV, 16.0 * Gaudi::Units::GeV}};

    /// Max momentum by radiator (MeV/c)
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this, "MaxP", {20.0 * Gaudi::Units::GeV, 80.0 * Gaudi::Units::GeV, 120.0 * Gaudi::Units::GeV}};

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Maximum photon yield
    Gaudi::Property<RadiatorArray<float>> m_maxYield{this, "MaximumYields", {80, 80, 80}};

    /// Max track angle
    Gaudi::Property<RadiatorArray<double>> m_maxTkAng{this, "MaxTkAngle", {5.0, 5.0, 4.0}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( OpticalPhotons )

} // namespace Rich::Future::Rec::MC::Moni
