/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <algorithm>
#include <cassert>
#include <mutex>
#include <utility>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "GaudiUtils/Aida2ROOT.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "Event/MCRichDigitSummary.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Relations
#include "RichFutureMCUtils/RichRecMCHelper.h"

namespace Rich::Future::Rec::MC::Moni {

  /** @class SIMDPhotonTime RichSIMDPhotonTime.h
   *
   *  MC checking of the RICH time information
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonTime final
      : public LHCb::Algorithm::Consumer<void( const Summary::Track::Vector&,                   //
                                               const LHCb::Track::Range&,                       //
                                               const SIMDPixelSummaries&,                       //
                                               const Rich::PDPixelCluster::Vector&,             //
                                               const Relations::PhotonToParents::Vector&,       //
                                               const LHCb::RichTrackSegment::Vector&,           //
                                               const SIMDCherenkovPhoton::Vector&,              //
                                               const Rich::Future::MC::Relations::TkToMCPRels&, //
                                               const LHCb::MCRichDigitSummarys& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonTime( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                     KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                     KeyValue{"RichDigitSummariesLocation", LHCb::MCRichDigitSummaryLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const Summary::Track::Vector&                   sumTracks,    //
                     const LHCb::Track::Range&                       tracks,       //
                     const SIMDPixelSummaries&                       pixels,       //
                     const Rich::PDPixelCluster::Vector&             clusters,     //
                     const Relations::PhotonToParents::Vector&       photToSegPix, //
                     const LHCb::RichTrackSegment::Vector&           segments,     //
                     const SIMDCherenkovPhoton::Vector&              photons,      //
                     const Rich::Future::MC::Relations::TkToMCPRels& tkrels,       //
                     const LHCb::MCRichDigitSummarys&                digitSums ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    // properties

  private:
    // cached data

    /// mutex lock
    mutable std::mutex m_updateLock;

    // histograms
    DetectorArray<AIDA::IHistogram1D*> h_hitTimeAll   = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_hitTimeTrue  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_hitTimeFake  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_timeResAll   = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_timeResTrue  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_timeResFake  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_tkOriginTime = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_tkOriginX    = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_tkOriginY    = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_tkOriginZ    = {{}};
    /////////////////////////////////////////////////////////////////////////////
    AIDA::IHistogram1D* h_tkPVTime       = nullptr;
    AIDA::IHistogram1D* h_tkPVZ          = nullptr;
    AIDA::IHistogram1D* h_tkPVt0         = nullptr;
    AIDA::IHistogram2D* h_PVTime_PVZ_All = nullptr;
    /////////////////////////////////////////////////////////////////////////////
    DetectorArray<AIDA::IHistogram1D*> h_hitTime2All      = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_hitTime2True     = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_hitTime2Fake     = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_hitTime2_t0_All  = {{}};
    DetectorArray<AIDA::IHistogram1D*> h_hitTime2_t0_True = {{}};
    /////////////////////////////////////////////////////////////////////////////
    DetectorArray<AIDA::IHistogram2D*> h_hitTime2_PVt0_momentum_All     = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_hitTime2_PVt0_momentum_True    = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_hitTime2_timeRes_momentum_All  = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_hitTime2_timeRes_momentum_True = {{}};
    /////////////////////////////////////////////////////////////////////////////
    DetectorArray<AIDA::IHistogram2D*> h_PVTime_CkAngle_All            = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_PVTime_CkAngle_True           = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_PVZ_CkAngle_All               = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_PVZ_CkAngle_True              = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_PVTime_Pred_noPV_All          = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_PVTime_Pred_noPV_True         = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_hitTime2_timeRes_CkAngle_All  = {{}};
    DetectorArray<AIDA::IHistogram2D*> h_hitTime2_timeRes_CkAngle_True = {{}};
  };

} // namespace Rich::Future::Rec::MC::Moni

using namespace Rich::Future::Rec::MC::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonTime::prebookHistograms() {

  bool ok = true;

  // time limits
  const DetectorArray<double> minTime{0.0, 40.0}, minTime_t0_corr{12.95, 52.5}, maxTime{25.0, 65.0};
  const DetectorArray<double> maxTime_t0_corr{13.15, 53.5}, timeRes{50, 50};

  ok &= saveAndCheck( h_tkPVTime,                                       //
                      richHisto1D( HID( "tkPVTime" ), "Track PV Times", //
                                   -1.0, 1.0, nBins1D(), "Track PV Time / ns" ) );
  ok &= saveAndCheck( h_tkPVZ,                                       //
                      richHisto1D( HID( "tkPVZ" ), "Track PV Z pos", //
                                   -200.0, 200.0, nBins1D(), "Track PV Z pos / mm" ) );
  ok &= saveAndCheck( h_tkPVt0,                                    //
                      richHisto1D( HID( "tkPVt0" ), "Track PV t0", //
                                   -2.0, 2.0, nBins1D(), "Track PV t0 / ns" ) );
  ok &= saveAndCheck( h_PVTime_PVZ_All,                                    //
                      richHisto2D( HID( "PVTime_PVZ" ), "PV Time vs PV Z", //
                                   -1.0, 1.0, nBins1D(), -200.0, 200.0, nBins1D(), "PV Time / ns", "PV Z / mm" ) );

  // Loop over RICHes
  for ( const auto rich : activeDetectors() ) {
    ok &= saveAndCheck( h_hitTimeAll[rich],                                           //
                        richHisto1D( HID( "pixHitTimeAll", rich ), "Pixel Hit Times", //
                                     minTime[rich], maxTime[rich], nBins1D(), "Hit Time / ns" ) );
    ok &= saveAndCheck( h_hitTimeTrue[rich],                                                         //
                        richHisto1D( HID( "pixHitTimeTrue", rich ), "Pixel Hit Times | True Signal", //
                                     minTime[rich], maxTime[rich], nBins1D(), "Hit Time / ns" ) );
    ok &= saveAndCheck( h_hitTimeFake[rich],                                                         //
                        richHisto1D( HID( "pixHitTimeFake", rich ), "Pixel Hit Times | Fake Signal", //
                                     minTime[rich], maxTime[rich], nBins1D(), "Hit Time / ns" ) );
    ok &= saveAndCheck( h_timeResAll[rich],                                                                  //
                        richHisto1D( HID( "photTimeResAll", rich ), "Photon Hit-Prediction Time Resolution", //
                                     -timeRes[rich], timeRes[rich], nBins1D(), "Hit time - prediction / ps" ) );
    ok &= saveAndCheck( h_timeResTrue[rich],                                                //
                        richHisto1D( HID( "photTimeResTrue", rich ),                        //
                                     "Photon Hit-Prediction Time Resolution | True Signal", //
                                     -timeRes[rich], timeRes[rich], nBins1D(), "Hit time - prediction / ps" ) );
    ok &= saveAndCheck( h_timeResFake[rich],                                                //
                        richHisto1D( HID( "photTimeResFake", rich ),                        //
                                     "Photon Hit-Prediction Time Resolution | Fake Signal", //
                                     -timeRes[rich], timeRes[rich], nBins1D(), "Hit time - prediction / ps" ) );
    ok &= saveAndCheck( h_tkOriginTime[rich],                                           //
                        richHisto1D( HID( "tkOriginTime", rich ), "Track Origin Times", //
                                     -5.0, 15.0, nBins1D(), "Track Origin Time / ns" ) );
    ok &= saveAndCheck( h_tkOriginX[rich],                                       //
                        richHisto1D( HID( "tkOriginX", rich ), "Track Origin X", //
                                     -200.0, 200.0, nBins1D(), "Track Origin X / mm" ) );
    ok &= saveAndCheck( h_tkOriginY[rich],                                       //
                        richHisto1D( HID( "tkOriginY", rich ), "Track Origin Y", //
                                     -200.0, 200.0, nBins1D(), "Track Origin Y / mm" ) );
    ok &= saveAndCheck( h_tkOriginZ[rich],                                       //
                        richHisto1D( HID( "tkOriginZ", rich ), "Track Origin Z", //
                                     -200.0, 200.0, nBins1D(), "Track Origin Z / mm" ) );
    //////////////////////////////////////////////////////////////////////////////////
    ok &= saveAndCheck( h_hitTime2All[rich],                                         //
                        richHisto1D( HID( "photTimeAll", rich ), "Photon Hit Times", //
                                     minTime[rich], maxTime[rich], nBins1D(), "Hit Time / ns" ) );
    ok &= saveAndCheck( h_hitTime2True[rich],                                                          //
                        richHisto1D( HID( "photHitTimeTrue", rich ), "Photon Hit Times | True Signal", //
                                     minTime[rich], maxTime[rich], nBins1D(), "Hit Time / ns" ) );
    ok &= saveAndCheck( h_hitTime2Fake[rich],                                                          //
                        richHisto1D( HID( "photHitTimeFake", rich ), "Photon Hit Times | Fake Signal", //
                                     minTime[rich], maxTime[rich], nBins1D(), "Hit Time / ns" ) );
    ok &= saveAndCheck( h_hitTime2_t0_All[rich],                                                 //
                        richHisto1D( HID( "photHitTime_t0_All", rich ), "Photon Hit Times - t0", //
                                     minTime_t0_corr[rich], maxTime_t0_corr[rich], nBins1D(), "Hit Time - t0 / ns" ) );
    ok &= saveAndCheck( h_hitTime2_t0_True[rich],                                                               //
                        richHisto1D( HID( "photHitTime_t0_True", rich ), "Photon Hit Times - t0 | True Signal", //
                                     minTime_t0_corr[rich], maxTime_t0_corr[rich], nBins1D(), "Hit Time - t0 / ns" ) );
    //////////////////////////////////////////////////////////////////////////////////
    ok &= saveAndCheck( h_hitTime2_PVt0_momentum_All[rich],                        //
                        richHisto2D( HID( "photHitTime_PVt0_momentum_All", rich ), //
                                     "Photon Hit Times - t0 for PV vs momentum",   //
                                     0, 120, nBins1D() * 2, minTime_t0_corr[rich], maxTime_t0_corr[rich], nBins1D() * 5,
                                     "Momentum / GeV", "Hit Time - t0 / ns" ) );
    ok &= saveAndCheck( h_hitTime2_PVt0_momentum_True[rich],                                   //
                        richHisto2D( HID( "photHitTime_PVt0_momentum_True", rich ),            //
                                     "Photon Hit Times - t0 for PV vs momentum | True Signal", //
                                     0, 120, nBins1D() * 2, minTime_t0_corr[rich], maxTime_t0_corr[rich], nBins1D() * 5,
                                     "Momentum / GeV", "Hit Time - t0 / ns" ) );
    ok &= saveAndCheck( h_hitTime2_timeRes_momentum_All[rich], //
                        richHisto2D( HID( "photHitTime_timeRes_momentum_All", rich ),
                                     "Photon Hit Times - prediction vs momentum", //
                                     0, 100, nBins1D(), -500, 500, nBins1D() * 5, "Momentum / GeV",
                                     "Hit Time - prediction / ps" ) );
    ok &= saveAndCheck( h_hitTime2_timeRes_momentum_True[rich], //
                        richHisto2D( HID( "photHitTime_timeRes_momentum_True", rich ),
                                     "Photon Hit Times - prediction vs momentum | True Signal", //
                                     0, 100, nBins1D(), -500, 500, nBins1D() * 5, "Momentum / GeV",
                                     "Hit Time - prediction / ps" ) );
    //////////////////////////////////////////////////////////////////////////////////
    ok &= saveAndCheck( h_PVTime_CkAngle_All[rich],                                        //
                        richHisto2D( HID( "PVTime_CkAngle", rich ), "PV Time vs Ck Angle", //
                                     -1.0, 1.0, nBins1D(), 0.0, 0.1, nBins1D(), "PV Time / ns", "Ck Angle / rad" ) );
    ok &= saveAndCheck( h_PVTime_CkAngle_True[rich],                                                          //
                        richHisto2D( HID( "PVTime_CkAngle_True", rich ), "PV Time vs Ck Angle | True Signal", //
                                     -1.0, 1.0, nBins1D(), 0.0, 0.1, nBins1D(), "PV Time / ns", "Ck Angle / rad" ) );
    ok &= saveAndCheck( h_PVZ_CkAngle_All[rich],                                     //
                        richHisto2D( HID( "PVZ_CkAngle", rich ), "PV Z vs Ck Angle", //
                                     -200.0, 200.0, nBins1D(), 0.0, 0.1, nBins1D(), "PV Z / mm", "Ck Angle / rad" ) );
    ok &= saveAndCheck( h_PVZ_CkAngle_True[rich],                                                       //
                        richHisto2D( HID( "PVZ_CkAngle_True", rich ), "PV Z vs Ck Angle | True Signal", //
                                     -200.0, 200.0, nBins1D(), 0.0, 0.1, nBins1D(), "PV Z / mm", "Ck Angle / rad" ) );
    ok &= saveAndCheck( h_PVTime_Pred_noPV_All[rich],                                         //
                        richHisto2D( HID( "PVTime_Pred_noPV", rich ), "PV Time vs Pred Time", //
                                     -1.0, 1.0, nBins1D(), minTime[rich], maxTime[rich], nBins1D(), "PV Time / ns",
                                     "Pred Time / ns" ) );
    ok &= saveAndCheck( h_PVTime_Pred_noPV_True[rich],                                                           //
                        richHisto2D( HID( "PVTime_Pred_noPV_True", rich ), "PV Time vs Pred Time | True Signal", //
                                     -1.0, 1.0, nBins1D(), minTime[rich], maxTime[rich], nBins1D(), "PV Time / ns",
                                     "Pred Time / ns" ) );
    ok &= saveAndCheck( h_hitTime2_timeRes_CkAngle_All[rich],                    //
                        richHisto2D( HID( "photHitTime_timeRes_CkAngle", rich ), //
                                     "Photon Hit Time - Pred Time vs Ck Angle",  //
                                     0.0, 0.1, nBins1D(), -500, 500, nBins1D(), "Ck Angle / rad",
                                     "Hit Time - Pred Time / ps" ) );
    ok &= saveAndCheck( h_hitTime2_timeRes_CkAngle_True[rich],                                //
                        richHisto2D( HID( "photHitTime_timeRes_CkAngle_True", rich ),         //
                                     "Photon Hit Time - Pred Time vs Ck Angle | True Signal", //
                                     0.0, 0.1, nBins1D(), -500, 500, nBins1D(),               //
                                     "Ck Angle / rad", "Hit Time - Pred Time / ps" ) );
  }

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void SIMDPhotonTime::operator()( const Summary::Track::Vector&                   sumTracks,    //
                                 const LHCb::Track::Range&                       tracks,       //
                                 const SIMDPixelSummaries&                       pixels,       //
                                 const Rich::PDPixelCluster::Vector&             clusters,     //
                                 const Relations::PhotonToParents::Vector&       photToSegPix, //
                                 const LHCb::RichTrackSegment::Vector&           segments,     //
                                 const SIMDCherenkovPhoton::Vector&              photons,      //
                                 const Rich::Future::MC::Relations::TkToMCPRels& tkrels,       //
                                 const LHCb::MCRichDigitSummarys&                digitSums ) const {

  // Make a local MC helper object
  const Helper mcHelper( tkrels, digitSums );

  // the lock
  std::lock_guard lock( m_updateLock );

  // Loop over the hits
  for ( const auto& pix : pixels ) {
    // Which RICH
    const auto rich = pix.rich();
    if ( richIsActive( rich ) ) {
      // Scalar loop
      for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {
        if ( !pix.validMask()[i] ) { continue; }
        fillHisto( h_hitTimeAll[rich], pix.hitTime()[i] );
        const auto isTrue = mcHelper.isCherenkovRadiation( pix.smartID()[i] );
        fillHisto( ( isTrue ? h_hitTimeTrue[rich] : h_hitTimeFake[rich] ), pix.hitTime()[i] );
      }
    }
  }

  // Loop over all segments once
  for ( const auto& seg : segments ) {
    const auto rich = seg.rich();
    if ( !richIsActive( rich ) ) { continue; }
    // Track plots
    fillHisto( h_tkOriginTime[rich], seg.originTime() );
    fillHisto( h_tkOriginX[rich], seg.originVertex().X() );
    fillHisto( h_tkOriginY[rich], seg.originVertex().Y() );
    fillHisto( h_tkOriginZ[rich], seg.originVertex().Z() );
  }

  // loop over the photon info
  for ( const auto&& [sumTk, tk] : Ranges::ConstZip( sumTracks, tracks ) ) {

    // Get the MCParticles for this track
    const auto mcPs = mcHelper.mcParticles( *tk, true, 0.5 );

    // Fill plots per MCP
    for ( const auto mcP : mcPs ) {
      if ( mcP && !mcP->mother() && mcP->primaryVertex() ) {
        const auto PVtime = mcP->primaryVertex()->time();
        const auto PVZpos = mcP->primaryVertex()->position().z();
        const auto PVt0   = PVtime - ( PVZpos / Gaudi::Units::c_light );
        fillHisto( h_tkPVTime, PVtime );
        fillHisto( h_tkPVZ, PVZpos );
        fillHisto( h_tkPVt0, PVt0 );
        fillHisto( h_PVTime_PVZ_All, PVtime, PVZpos );
      }
    }

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {

      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];

      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];

      // RICH, Radiator info
      const auto rich = seg.rich();
      const auto rad  = seg.radiator();
      if ( !richIsActive( rich ) || !radiatorIsActive( rad ) ) { continue; }

      // Weight per MCP
      const auto mcPW = ( !mcPs.empty() ? 1.0 / (double)mcPs.size() : 1.0 );

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( !phot.validityMask()[i] ) { continue; }

        // scalar cluster
        const auto& clus = clusters[simdPix.scClusIndex()[i]];

        // do we have an true MC Cherenkov photon
        const auto trueCKMCPs = mcHelper.trueCherenkovPhoton( *tk, rad, clus );

        // SmartID
        const auto id = phot.smartID()[i];

        // Hit time
        const auto hitTime = id.time();

        // reconstructed theta
        const auto thetaRec = phot.CherenkovTheta()[i];

        // RICH only transit time (from radiator entry plane to photon detectors)
        const auto richTime = phot.radTransitTime()[i];

        // loop over MCPs
        for ( const auto mcP : mcPs ) {

          // get PV infor for this MCP
          const auto mcPV         = ( mcP ? mcP->primaryVertex() : nullptr );
          const auto PVtime       = ( mcPV ? mcPV->time() : -999 );
          const auto PVZpos       = ( mcPV ? mcPV->position().z() : -999 );
          const auto PVt0         = PVtime - ( PVZpos / Gaudi::Units::c_light );
          const auto mcP_momentum = ( mcP ? mcP->momentum().E() / Gaudi::Units::GeV : -999 );

          // The True MCParticle type
          auto pid = mcHelper.mcParticleType( mcP );
          // If MC type not known, assume Pion (as in real data)
          if ( Rich::Unknown == pid ) { pid = Rich::Pion; }

          // Form the full prediction of the detection time
          const auto recoTime = richTime + seg.timeToRadEntry( pid );

          // Time res
          const auto timeRes = ( hitTime - recoTime ) / Gaudi::Units::ps;

          // true Cherenkov signal ?
          const auto trueCKSig = std::find( trueCKMCPs.begin(), trueCKMCPs.end(), mcP ) != trueCKMCPs.end();

          // fill time and time res plots
          fillHisto( h_timeResAll[rich], timeRes, mcPW );
          fillHisto( h_hitTime2All[rich], hitTime, mcPW );
          fillHisto( h_hitTime2_t0_All[rich], hitTime - PVt0, mcPW );
          fillHisto( h_hitTime2_PVt0_momentum_All[rich], mcP_momentum, ( hitTime - PVt0 ), mcPW );
          fillHisto( h_hitTime2_timeRes_momentum_All[rich], mcP_momentum, timeRes, mcPW );
          fillHisto( h_PVTime_CkAngle_All[rich], PVtime, thetaRec, mcPW );
          fillHisto( h_PVZ_CkAngle_All[rich], PVZpos, thetaRec, mcPW );
          fillHisto( h_PVTime_Pred_noPV_All[rich], PVtime, recoTime - PVtime, mcPW );
          fillHisto( h_hitTime2_timeRes_CkAngle_All[rich], thetaRec, timeRes, mcPW );
          if ( trueCKSig ) {
            fillHisto( h_timeResTrue[rich], timeRes, mcPW );
            fillHisto( h_hitTime2True[rich], hitTime, mcPW );
            fillHisto( h_hitTime2_t0_True[rich], hitTime - PVt0, mcPW );
            fillHisto( h_hitTime2_PVt0_momentum_True[rich], mcP_momentum, ( hitTime - PVt0 ), mcPW );
            fillHisto( h_hitTime2_timeRes_momentum_True[rich], mcP_momentum, timeRes, mcPW );
            fillHisto( h_PVTime_CkAngle_True[rich], PVtime, thetaRec, mcPW );
            fillHisto( h_PVZ_CkAngle_True[rich], PVZpos, thetaRec, mcPW );
            fillHisto( h_PVTime_Pred_noPV_True[rich], PVtime, recoTime - PVtime, mcPW );
            fillHisto( h_hitTime2_timeRes_CkAngle_True[rich], thetaRec, timeRes, mcPW );
          } else {
            fillHisto( h_timeResFake[rich], timeRes, mcPW );
            fillHisto( h_hitTime2Fake[rich], hitTime, mcPW );
          }

        } // loop over associated MCPs

      } // SIMD scalar loop
    }   // photon loop
  }     // track loop
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonTime )

//-----------------------------------------------------------------------------
