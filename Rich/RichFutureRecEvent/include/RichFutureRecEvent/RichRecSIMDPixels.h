/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Utils
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichSIMDTypes.h"

// Gaudi
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/Range.h"

// Kernel
#include "Kernel/RichDetectorType.h"
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// Event
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// STL
#include <array>
#include <cstddef>
#include <ostream>
#include <string>
#include <utility>

namespace Rich::Future::Rec {

  /** @class SIMDPixel RichFutureRecEvent/RichRecSIMDPixels.h
   *
   *  Representation of a set of RICH pixels in a SIMD format
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */

  class SIMDPixel final : public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    // types

    /// The scalar floating point precision
    using FP = Rich::SIMD::DefaultScalarFP;
    /// SIMD floating point type
    using SIMDFP = Rich::SIMD::FP<FP>;
    /// SIMD Point
    using Point = Rich::SIMD::Point<FP>;
    /// Type for SmartIDs.
    using SmartIDs = Rich::SIMD::STDArray<LHCb::RichSmartID, SIMDFP>;
    /// Type for index to original scalar cluster
    using ScIndex = Rich::SIMD::Int32;
    /// Selection mask
    using Mask = SIMDFP::mask_type;

  public:
    // constructors

    /// 3D Constructor
    SIMDPixel( const Rich::DetectorType   rich,     //
               const Rich::Side           side,     //
               const SIMDPixel::SmartIDs& smartIDs, //
               const Point&               gPos,     //
               const Point&               lPos,     //
               const SIMDFP&              effArea,  //
               const ScIndex&             scClusIn, //
               const Mask&                mask )
        : m_rich( rich )
        , m_side( side )
        , m_smartID( smartIDs )
        , m_gloPos( gPos )
        , m_locPos( lPos )
        , m_effArea( effArea )
        , m_scClusIn( scClusIn )
        , m_validMask( mask ) {
      // Loop over scalar entries to set Inner / Outer regions via SmartIDs
      GAUDI_LOOP_UNROLL( SIMDFP::Size )
      for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
        if ( m_smartID[i].isValid() ) { m_isInnerRegion[i] = !m_smartID[i].isLargePMT(); }
      }
    }

    /// 4D Constructor
    SIMDPixel( const Rich::DetectorType   rich,     //
               const Rich::Side           side,     //
               const SIMDPixel::SmartIDs& smartIDs, //
               const Point&               gPos,     //
               const Point&               lPos,     //
               const SIMDFP&              effArea,  //
               const ScIndex&             scClusIn, //
               const Mask&                mask,     //
               const SIMDFP&              timeWindow )
        : SIMDPixel( rich, side, smartIDs, gPos, lPos, effArea, scClusIn, mask ) {
      // Set time windows
      m_timeWindow = timeWindow;
      // Loop over scalar entries to copy times from SmartIDs into SIMD storage
      GAUDI_LOOP_UNROLL( SIMDFP::Size )
      for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
        if ( m_smartID[i].isValid() && m_smartID[i].adcTimeIsSet() ) { m_hitTime[i] = m_smartID[i].time(); }
      }
    }

  public:
    // read access

    /// Access the RICH
    [[nodiscard]] inline Rich::DetectorType rich() const noexcept { return m_rich; }

    /// Access the panel
    [[nodiscard]] inline Rich::Side side() const noexcept { return m_side; }

    /// Access the global position
    [[nodiscard]] inline const Point& gloPos() const noexcept { return m_gloPos; }

    /// Access the local position
    [[nodiscard]] inline const Point& locPos() const noexcept { return m_locPos; }

    /// Access the Rich channel IDs
    [[nodiscard]] inline const SmartIDs& smartID() const noexcept { return m_smartID; }

    /// Access the effective cluster area
    [[nodiscard]] inline const SIMDFP& effArea() const noexcept { return m_effArea; }

    /// Access the hit time
    [[nodiscard]] inline const SIMDFP& hitTime() const noexcept { return m_hitTime; }

    /// Access the scalar cluster indices
    [[nodiscard]] inline const ScIndex& scClusIndex() const noexcept { return m_scClusIn; }

    /// Access the validity mask
    [[nodiscard]] inline const Mask& validMask() const noexcept { return m_validMask; }

  public:
    // scalar access

    // global position for given scalar entry
    [[nodiscard]] inline auto gloPos( const std::size_t i ) const noexcept {
      return Gaudi::XYZPoint{gloPos().X()[i], gloPos().Y()[i], gloPos().Z()[i]};
    }

    // local position for given scalar entry
    [[nodiscard]] inline auto locPos( const std::size_t i ) const noexcept {
      return Gaudi::XYZPoint{locPos().X()[i], locPos().Y()[i], locPos().Z()[i]};
    }

  public:
    // 4D reco support.
    // These options are liable to change more in the future as 4D support for U2
    // gets more mature, and as the detector options stabilise.

    /// Read-access time window
    [[nodiscard]] inline const SIMDFP& timeWindow() const noexcept { return m_timeWindow; }

    /// Read access Inner/Outer region mask
    [[nodiscard]] inline const Mask& isInnerRegion() const noexcept { return m_isInnerRegion; }

    /// Determine if a given hit time prediction is inside time window
    [[nodiscard]] inline auto isInTime( const SIMDFP& predT ) const noexcept {
      return ( abs( hitTime() - predT ) < timeWindow() );
    }

    /// Override Inner/Outer region specification
    inline void overrideRegions( const Mask& isInnerRegion ) noexcept { m_isInnerRegion = isInnerRegion; }

  public:
    // write access

    /// Access the global position
    [[nodiscard]] inline Point& gloPos() noexcept { return m_gloPos; }

    /// Access the local position
    [[nodiscard]] inline Point& locPos() noexcept { return m_locPos; }

    /// Access the Rich channel IDs
    [[nodiscard]] inline SmartIDs& smartID() noexcept { return m_smartID; }

    /// Access the effective cluster area
    [[nodiscard]] inline SIMDFP& effArea() noexcept { return m_effArea; }

    /// Access the hit time
    [[nodiscard]] inline SIMDFP& hitTime() noexcept { return m_hitTime; }

    /// Access the scalar cluster indices
    [[nodiscard]] inline ScIndex& scClusIndex() noexcept { return m_scClusIn; }

    /// Access the validity mask
    [[nodiscard]] inline Mask& validMask() noexcept { return m_validMask; }

  public:
    // setters

    /// Set the global position
    inline void setGloPos( const Point& gpos ) noexcept { m_gloPos = gpos; }

    /// Set the local position
    inline void setLocPos( const Point& lpos ) noexcept { m_locPos = lpos; }

    /// Set the Rich channel IDs
    inline void setSmartID( const SmartIDs& ids ) noexcept { m_smartID = ids; }

    /// Set the effective cluster area
    inline void setEffArea( const SIMDFP& area ) noexcept { m_effArea = area; }

    /// Set the scalar cluster indices
    inline void setScClusIndex( const ScIndex& index ) noexcept { m_scClusIn = index; }

    /// Access the validity mask
    inline void setValidMask( const Mask& mask ) noexcept { m_validMask = mask; }

  private:
    // messaging

    template <typename STREAM>
    STREAM& fillStream( STREAM& s ) const {
      auto mend = []() {
        if constexpr ( std::is_same_v<MsgStream, STREAM> ) {
          return endmsg;
        } else {
          return "\n";
        }
      };
      s << "[ " << rich() << " " << Rich::text( rich(), side() ) << mend();
      for ( std::size_t i = 0; i < SIMDFP::Size; ++i ) {
        if ( validMask()[i] ) {
          s << "  " << i << " GloPos      " << gloPos( i ) << mend();
          s << "    LocPos      " << locPos( i ) << mend();
          s << "    EffArea     " << effArea()[i] << mend();
          s << "    ID          " << smartID()[i] << mend();
          s << "    Hit Time    " << hitTime()[i] << mend();
          s << "    Time Window " << timeWindow()[i] << mend();
          s << "    clusIndx    " << scClusIndex()[i] << mend();
        }
      }
      return s << " ]" << mend();
    }

  public:
    // messaging

    /// Implement ostream << method
    friend inline auto& operator<<( std::ostream& s, const SIMDPixel& pix ) { return pix.fillStream( s ); }
    /// Implement MsgStream << method
    friend inline auto& operator<<( MsgStream& s, const SIMDPixel& pix ) { return pix.fillStream( s ); }

  private:
    // data

    /// RICH
    Rich::DetectorType m_rich{Rich::InvalidDetector};

    /// Panel
    Rich::Side m_side{Rich::InvalidSide};

    /// The channel IDs for the photon detection points
    SmartIDs m_smartID;

    /// Hit times
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_hitTime{SIMDFP::Zero()};

    /// Global position
    alignas( LHCb::SIMD::VectorAlignment ) Point m_gloPos;

    /// Local position
    alignas( LHCb::SIMD::VectorAlignment ) Point m_locPos;

    /// Effective cluster area
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_effArea{SIMDFP::Zero()};

    /// Indices to the original scalar clusters
    alignas( LHCb::SIMD::VectorAlignment ) ScIndex m_scClusIn{-ScIndex::One()};

    /// validity mask (default initialised to false)
    alignas( LHCb::SIMD::VectorAlignment ) Mask m_validMask{Mask( false )};

  private:
    /// @todo These might be a temporary inclusion to aid the Upgrade II studies.
    ///       Longer term once the hardware stabilises these likely will be a parameter
    ///       that are instead taken from the detector description.
    ///       For now, as flexibility in the geometry is needed cache here for each pixel.

    /// Inner/Outer region flag
    alignas( LHCb::SIMD::VectorAlignment ) Mask m_isInnerRegion{Mask( true )};

    /// Time window for this pixel
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_timeWindow{SIMDFP( 999999 )};
  };

  /** @class SIMDPixelSummaries RichFutureRecEvent/RichRecSIMDPixels.h
   *
   *  SIMD pixel summaries
   *
   *  @author Chris Jones
   *  @date   2017-10-16
   */

  class SIMDPixelSummaries final : public SIMD::STDVector<SIMDPixel>,
                                   public LHCb::SIMD::AlignedBase<LHCb::SIMD::VectorAlignment> {

  public:
    /// Type for storage of SIMD Pixels
    using Vector = SIMD::STDVector<SIMDPixel>;
    /// Type for range access to SIMD pixels for a given RICH and/or side
    using Range = Gaudi::Range_<Vector, Vector::const_iterator>;

  private:
    /// Internal indices storage for ranges
    using Indices = std::pair<std::size_t, std::size_t>;

  private:
    // accessors

    /// Get number of SIMD hits in given RICH and panel
    [[nodiscard]] inline auto nHitsSIMD( const Rich::DetectorType rich, //
                                         const Rich::Side         side ) const noexcept {
      return m_nHitsSIMD[rich][side];
    }

    /// Get number of SIMD hits in given RICH
    [[nodiscard]] inline auto nHitsSIMD( const Rich::DetectorType rich ) const noexcept {
      return nHitsSIMD( rich, Rich::firstSide ) + nHitsSIMD( rich, Rich::secondSide );
    }

  public:
    // accessors

    /// Access the range for the given RICH and panel
    [[nodiscard]] inline Range range( const Rich::DetectorType rich, //
                                      const Rich::Side         side ) const noexcept {
      return Range( begin() + ( m_panelRanges[rich] )[side].first, //
                    begin() + ( m_panelRanges[rich] )[side].second );
    }

    /// Access the range for the given RICH
    [[nodiscard]] inline Range range( const Rich::DetectorType rich ) const noexcept {
      return Range( begin() + m_richRanges[rich].first, //
                    begin() + m_richRanges[rich].second );
    }

    /// Get number of representated scalar hits in given RICH and panel
    [[nodiscard]] inline auto nHitsScalar( const Rich::DetectorType rich, //
                                           const Rich::Side         side ) const noexcept {
      const auto hits = range( rich, side );
      return std::accumulate( hits.begin(), hits.end(), std::size_t{0},
                              []( const std::size_t sum, const auto& rh ) { return sum + rh.validMask().count(); } );
    }

    /// Get number of representated scalar hits in given RICH
    [[nodiscard]] inline auto nHitsScalar( const Rich::DetectorType rich ) const noexcept {
      return nHitsScalar( rich, Rich::firstSide ) + nHitsScalar( rich, Rich::secondSide );
    }

  public:
    // setters

    /// emplace back a SIMD pixel into the container
    template <typename... ARGS>
    inline auto& emplace_back( ARGS&&... args ) {
      auto& pix = SIMD::STDVector<SIMDPixel>::emplace_back( std::forward<ARGS>( args )... );
      countHit( pix.rich(), pix.side() );
      return pix;
    }

    /// Initialise the ranges once filled
    inline void init() noexcept {
      // get start/stop points for each RICH and panel
      const std::size_t first        = 0;
      const std::size_t r1botstart   = nHitsSIMD( Rich::Rich1, Rich::top );
      const std::size_t r2start      = r1botstart + nHitsSIMD( Rich::Rich1, Rich::bottom );
      const std::size_t r2rightstart = r2start + nHitsSIMD( Rich::Rich2, Rich::left );
      const std::size_t last         = end() - begin();
      // set ranges
      // Note we are storing indices not iterators as these remain valid
      // under container relocations. Conversion to ranges happens on access.
      setRange( Rich::Rich1, first, r2start );
      setRange( Rich::Rich2, r2start, last );
      setRange( Rich::Rich1, Rich::top, first, r1botstart );
      setRange( Rich::Rich1, Rich::bottom, r1botstart, r2start );
      setRange( Rich::Rich2, Rich::left, r2start, r2rightstart );
      setRange( Rich::Rich2, Rich::right, r2rightstart, last );
    }

  private:
    /// Count hits in each RICH and panel
    inline void countHit( const Rich::DetectorType rich, //
                          const Rich::Side         side ) noexcept {
      ++m_nHitsSIMD[rich][side];
    }

    /// Set RICH range
    inline void setRange( const Rich::DetectorType rich,    //
                          const std::size_t        beginit, //
                          const std::size_t        endit ) noexcept {
      m_richRanges[rich] = Indices( beginit, endit );
    }

    /// Set Panel range
    inline void setRange( const Rich::DetectorType rich,    //
                          const Rich::Side         side,    //
                          const std::size_t        beginit, //
                          const std::size_t        endit ) noexcept {
      m_panelRanges[rich][side] = Indices( beginit, endit );
    }

  private:
    // data

    /// Hit counts for each RICH and panel
    DetectorArray<PanelArray<std::size_t>> m_nHitsSIMD = {{}};

    /// RICH ranges
    DetectorArray<Indices> m_richRanges = {{}};

    /// Panel ranges
    DetectorArray<PanelArray<Indices>> m_panelRanges = {{}};
  };

  /// TES locations
  namespace SIMDPixelSummariesLocation {
    /// Default Location in TES for the pixel SIMD summaries
    inline const std::string Default = "Rec/RichFuture/SIMDPixelSummaries/Default";
  } // namespace SIMDPixelSummariesLocation

} // namespace Rich::Future::Rec
