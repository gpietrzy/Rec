/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/Track.h"

// Relations
#include "RichFutureMCUtils/TrackToMCParticle.h"
#include "RichFutureRecEvent/RichRecRelations.h"

// STD
#include <array>
#include <unordered_map>

namespace Rich::Future::Rec::MC {

  namespace {
    using OutData = LHCb::Track::Selection;
  }

  /** @class TrackFilter TrackFilter.h
   *
   *  Filter a container of Tracks based on MC information.
   *  Designed to help mimic the tracks created by TrSegMakerFromMCRichTracks
   *
   *  @author Chris Jones
   *  @date   2020-10-29
   */

  class TrackFilter final
      : public LHCb::Algorithm::Transformer<OutData( const LHCb::Track::Range&,                       //
                                                     const Rich::Future::MC::Relations::TkToMCPRels&, //
                                                     const LHCb::MCRichTracks& ),
                                            Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    TrackFilter( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"InTracksLocation", LHCb::TrackLocation::Default},
                        KeyValue{"TrackToMCParticlesRelations", Rich::Future::MC::Relations::TrackToMCParticles},
                        KeyValue{"MCRichTracksLocation", LHCb::MCRichTrackLocation::Default}},
                       // data output
                       {KeyValue{"OutTracksLocation", LHCb::TrackLocation::Default + "Out"}} ) {}

  public:
    /// Functional operator
    OutData operator()( const LHCb::Track::Range&                       tracks, //
                        const Rich::Future::MC::Relations::TkToMCPRels& tkrels, //
                        const LHCb::MCRichTracks&                       mcRichtracks ) const override {

      // Buld map MCP -> MCRichTrack
      std::unordered_map<const LHCb::MCParticle*, const LHCb::MCRichTrack*> mcPToMCR;
      for ( const auto mcR : mcRichtracks ) {
        const auto mcP = ( mcR ? mcR->mcParticle() : nullptr );
        if ( mcP ) { mcPToMCR[mcP] = mcR; }
      }

      OutData sel_tracks;

      // Make a local MC helper object
      Rich::Future::MC::Relations::TrackToMCParticle mcHelper( tkrels );

      for ( const auto tk : tracks ) {

        // flag for if this track gets selected or not
        bool selected = false;

        // Get MCParticles for this track
        const auto mcPs = mcHelper.mcParticles( *tk );

        // Loop over MCPs
        for ( const auto mcp : mcPs ) {

          // Just in case
          if ( !mcp ) { continue; }

          // get charge
          const auto charge = mcp->particleID().threeCharge() / 3;
          if ( 1 != abs( charge ) ) { continue; }

          // Check MCParticle Pt
          if ( mcp->momentum().Pt() < m_minPt ) { continue; }

          // Check we have a PV
          const auto mcp_orig_pv = mcp->primaryVertex();
          if ( !mcp_orig_pv ) { continue; }

          // Check origin vertex position
          const auto mcp_orig_v = mcp->originVertex();
          if ( !mcp_orig_v ) { continue; }
          if ( m_tkOriginTol[0] < fabs( mcp_orig_v->position().X() ) || //
               m_tkOriginTol[1] < fabs( mcp_orig_v->position().Y() ) || //
               m_tkOriginTol[2] < fabs( mcp_orig_v->position().Z() ) ) {
            continue;
          }

          // Do we have a MCRichTrack
          const auto mcr = mcPToMCR.find( mcp );
          if ( mcr != mcPToMCR.end() ) {
            const auto mcRichTk = mcr->second;

            // Check MC segments
            for ( const auto rad : activeRadiators() ) {

              // Get MC segment for this track and radiator
              const auto mcSeg = mcRichTk->segmentInRad( rad );
              if ( !mcSeg ) { continue; }

              // Apply selection cuts
              if ( mcSeg->pathLength() < m_minPathL[rad] ) { continue; }
              if ( mcSeg->mcRichOpticalPhotons().size() < m_minPhots[rad] ) { continue; }

              // Get entry information
              const auto& entryPoint         = mcSeg->entryPoint();
              const auto& entryStateMomentum = mcSeg->entryMomentum();
              // Get exit information
              const auto& exitPoint         = mcSeg->exitPoint();
              const auto& exitStateMomentum = mcSeg->exitMomentum();
              // Get middle point information
              const auto midPoint         = mcSeg->bestPoint( 0.5 );
              const auto midStateMomentum = mcSeg->bestMomentum( 0.5 );

              // Sanity checks on entry/exit points
              if ( entryPoint.Z() < m_minZ[rad] || //
                   midPoint.Z() < m_minZ[rad] ||   //
                   exitPoint.Z() < m_minZ[rad] ) {
                continue;
              }

              // check min/max momentum on segment
              if ( std::sqrt( entryStateMomentum.mag2() ) < m_minP[rad] || //
                   std::sqrt( midStateMomentum.mag2() ) < m_minP[rad] ||   //
                   std::sqrt( exitStateMomentum.mag2() ) < m_minP[rad] ) {
                continue;
              }
              if ( std::sqrt( entryStateMomentum.mag2() ) > m_maxP[rad] || //
                   std::sqrt( midStateMomentum.mag2() ) > m_maxP[rad] ||   //
                   std::sqrt( exitStateMomentum.mag2() ) > m_maxP[rad] ) {
                continue;
              }

              // if get here track is selected
              selected = true;
              break;
            }
          }

          // If already selected no need to check this track any further
          if ( selected ) { break; }

        } // MCPs loop

        if ( selected ) { sel_tracks.insert( tk ); }

      } // input track loop

      _ri_debug << "Selected " << sel_tracks.size() << " tracks from " << tracks.size() << endmsg;

      return sel_tracks;
    }

  private:
    // properties

    /// Overall Min monentum cut
    Gaudi::Property<RadiatorArray<double>> m_minP{
        this,
        "MinP",
        {0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV, 0.0 * Gaudi::Units::GeV},
        "Minimum momentum (GeV/c)"};

    /// Overall Max monentum cut
    Gaudi::Property<RadiatorArray<double>> m_maxP{
        this,
        "MaxP",
        {9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV, 9e9 * Gaudi::Units::GeV},
        "Maximum momentum (GeV/c)"};

    /// Minimum track transerve momentum
    Gaudi::Property<double> m_minPt{this, "MinPt", 0.0 * Gaudi::Units::GeV, "Minimum transerve momentum (GeV/c)"};

    /// Min path length for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minPathL{
        this, "MinPathLengths", {10 * Gaudi::Units::mm, 500 * Gaudi::Units::mm, 1500 * Gaudi::Units::mm}};

    /// Min number of photons for each radiator
    Gaudi::Property<RadiatorArray<double>> m_minPhots{this, "MinNumPhotons", {0, 0, 0}};

    /// Tolerance on track origin w.r.t. (0,0,0)
    Gaudi::Property<std::array<double, 3>> m_tkOriginTol{this, "TrackOriginTol", {1.0, 1.0, 100.0}};

    /// Minimum z position for states in each radiator (mm)
    Gaudi::Property<RadiatorArray<double>> m_minZ{this, "MinStateZ", {800, 800, 9000}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackFilter )

} // namespace Rich::Future::Rec::MC
