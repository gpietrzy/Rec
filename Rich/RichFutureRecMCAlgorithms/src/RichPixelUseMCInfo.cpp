/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Event Model
#include "Event/MCRichHit.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Utils
#include "RichFutureUtils/RichSmartIDs.h"

// Boost
#include "boost/container/small_vector.hpp"

// STD
#include <cmath>
#include <map>
#include <unordered_map>
#include <utility>
#include <vector>

namespace Rich::Future::Rec::MC {

  /** @class RichPixelUseMCInfo RichPixelUseMCInfo.h
   *
   *  Use MC truth to cheat RICH pixel positions and/or time
   *
   *  @author Chris Jones
   *  @date   2023-09-20
   */

  class RichPixelUseMCInfo final : public LHCb::Algorithm::Transformer<
                                       SIMDPixelSummaries( SIMDPixelSummaries const&, //
                                                           LHCb::MCRichHits const&,   //
                                                           Rich::Utils::RichSmartIDs const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Utils::RichSmartIDs>> {

  public:
    /// Standard constructor
    RichPixelUseMCInfo( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // data inputs
                       {KeyValue{"InRichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                        KeyValue{"MCRichHitsLocation", LHCb::MCRichHitLocation::Default},
                        // input conditions data
                        KeyValue{"RichSmartIDs", Rich::Utils::RichSmartIDs::DefaultConditionKey}},
                       // output data
                       {KeyValue{"OutRichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default + "Out"}} ) {}

    /// Initialize
    StatusCode initialize() override {
      // base class initialise followed by conditions
      return Transformer::initialize().andThen( [&] {
        // random number generator, flat between 0 and 1
        m_randZeroOne = Rndm::Numbers( randSvc(), Rndm::Flat( 0.0, 1.0 ) );
        // create the RICH smartID helper instance
        Rich::Utils::RichSmartIDs::addConditionDerivation( this );
        for ( const auto rich : activeDetectors() ) {
          if ( m_mcPixQuantizeInner[rich] > 0 || m_mcPixQuantizeOuter[rich] > 0 ) {
            info() << "Will quantise " << rich << " inner/outer pixels to " << m_mcPixQuantizeInner[rich] << "/"
                   << m_mcPixQuantizeOuter[rich] << " mm square" << endmsg;
          }
          if ( m_effPixInner[rich] < 1.0 || m_effPixOuter[rich] < 1.0 ) {
            info() << "Will apply " << rich << " inner/outer pixel efficiency factors " << m_effPixInner[rich] << "/"
                   << m_effPixOuter[rich] << endmsg;
          }
        }
        // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
      } );
    }

  private:
    // types

    struct XYData {
      double      x{0}, y{0};
      SIMDPixel*  pix{nullptr};
      std::size_t index{0};
      auto        time() const { return ( pix ? pix->hitTime()[index] : -999.0f ); }
      void        disable() {
        if ( pix ) { pix->validMask()[index] = false; }
      }
      using Vector = boost::container::small_vector<XYData, 10>;
    };

  public:
    /// Functional operator
    SIMDPixelSummaries operator()( SIMDPixelSummaries const&        pixels,      //
                                   LHCb::MCRichHits const&          main_mchits, //
                                   Rich::Utils::RichSmartIDs const& smartIDsHelper ) const override {

      // map from Smart ID to associated MC hits
      using HitData = std::pair<const LHCb::MCRichHit*, double>;
      std::unordered_map<LHCb::RichSmartID, std::vector<HitData>> hitsPerPix;

      // Get the location of the container of an MC hit in the TES
      auto hitLocation = [&]( const auto* obj ) { return ( obj ? objectLocation( obj->parent() ) : "Not Contained" ); };

      // Get hit time from MC hit, including offsets
      auto getTime = [&]( const auto* mchit ) {
        if ( !mchit ) { return 0.0; }
        // Deduce if this is a spillover event and if it is apply offset
        const auto   loc         = hitLocation( mchit );
        const double spillOffset =                                             //
            ( ( loc.find( "PrevPrev" ) != std::string::npos ) ? -50.0 :        //
                  ( loc.find( "Prev" ) != std::string::npos ) ? -25.0 :        //
                      ( loc.find( "NextNext" ) != std::string::npos ) ? 50.0 : //
                          ( loc.find( "Next" ) != std::string::npos ) ? 25.0 : //
                              0.0 );
        const auto hitT = spillOffset + mchit->timeOfFlight();
        // _ri_verbo << "TES Location '" << loc << "' SpillOffset=" << spillOffset << " HitTime=" << hitT << endmsg;
        return hitT;
      };

      auto addMCHits = [&]( auto const& mchits ) {
        for ( const auto mchit : mchits ) {
          auto id = mchit->sensDetID().channelDataOnly();
          if ( id.isValid() ) {
#ifdef USE_DD4HEP
            if ( m_detdescMCinput ) {
              // If built for DD4HEP apply correction to PMT module numbers to account
              // for different numbering scheme between DD4HEP and DetDesc.
              // Option needs to be explicitly activated only when input is known to
              // be DetDesc based MC.
              // ***** To eventually be removed when DetDesc finally dies completely *****
              const Rich::DetectorArray<Rich::PanelArray<LHCb::RichSmartID::DataType>> mod_corr{{{0, 0}, {6, 18}}};
              const auto pdMod      = id.pdMod() + mod_corr[id.rich()][id.panel()];
              const auto pdNumInMod = id.pdNumInMod();
              id.setPD( pdMod, pdNumInMod );
            }
#endif
            // Are we inside the 0-25 ns window ?
            const auto hitT        = getTime( mchit );
            const auto hitTShifted = hitT - m_timeShift[id.rich()];
            // _ri_verbo << "Shifted time = " << hitTShifted << endmsg;
            if ( hitTShifted >= 0.0 && hitTShifted <= 25.0 ) { hitsPerPix[id].emplace_back( mchit, hitT ); }
          }
        } // hit loop
      };

      // add the main event hits
      addMCHits( main_mchits );

      // ... and if requested spillover hits
      if ( m_enableSpillover ) {
        auto addSpilloverHits = [&]( auto const& mchits ) {
          if ( mchits.exist() ) { addMCHits( *mchits.get() ); }
        };
        addSpilloverHits( m_prevHits );
        addSpilloverHits( m_prevPrevHits );
        addSpilloverHits( m_nextHits );
        addSpilloverHits( m_nextNextHits );
        addSpilloverHits( m_lhcBkgHits );
      }

      // Clone the pixels to update them
      SIMDPixelSummaries out_pixs = pixels;

      // local quantized (x,y) hits made for each PD
      std::map<LHCb::RichSmartID, XYData::Vector> madeXY;

      // disabled hits
      DetectorArray<std::size_t> disHits{0, 0};

      // Loop over summary data and update positions
      for ( auto& pix : out_pixs ) {

        // Which rich and side
        const auto rich = pix.rich();
        const auto side = pix.side();

        // flag to indicate if positions have been updated
        bool hasPosUpdate = false;

        // scalar loop
        for ( std::size_t i = 0; i < SIMDPixel::SIMDFP::Size; ++i ) {
          if ( pix.validMask()[i] ) {

            // SmartID for this hit
            const auto id = pix.smartID()[i];
            _ri_verbo << id << endmsg;
            if ( !id.isValid() ) { continue; }

            // pixel region
            const bool isInner = pix.isInnerRegion()[i];

            // Apply pixel efficiency
            const auto pixEff = ( isInner ? m_effPixInner[rich] : m_effPixOuter[rich] );
            if ( pixEff < 1.0 && m_randZeroOne.shoot() > pixEff ) {
              _ri_verbo << "Rejecting " << id << endmsg;
              pix.validMask()[i] = false;
              continue;
            }

            // Do we have any MChits for this ID
            const auto& pix_mchits = hitsPerPix[id.channelDataOnly()];
            if ( !pix_mchits.empty() ) {

              // Find best MCHit to use
              const auto* mch = &pix_mchits.front();
              if ( pix_mchits.size() > 1 ) {

                // If ID has time set use that to select MC hit
                if ( id.adcTimeIsSet() ) {
                  double minTimeDiff = 999999.9;
                  for ( const auto& h : pix_mchits ) {
                    // Apply same ADC truncation as in RichSmartID to MC hit time
                    auto tmpID = id;
                    tmpID.setTime( h.second );
                    const auto tDiff = std::abs( tmpID.adcTime() - id.adcTime() );
                    _ri_verbo << " -> " << id.adcTime() << " " << tmpID.adcTime() << endmsg;
                    if ( tDiff < minTimeDiff ) {
                      _ri_verbo << "  -> SELECTED " << endmsg;
                      minTimeDiff = tDiff;
                      mch         = &h;
                    }
                  } // hit loop
                } else {
                  // Just use first signal hit
                  for ( const auto& h : pix_mchits ) {
                    if ( h.first->isSignal() ) {
                      mch = &h;
                      break;
                    }
                  }
                }
              } // more than one MC hit

              if ( m_useMCPos ) {
                // Update hit position using MC
                auto mc_gpos = mch->first->entry();
                // Shift to local coords
                auto mc_lpos = smartIDsHelper.globalToPDPanel( mc_gpos );
                // Apply Z correction
                const DetectorArray<double> zCorr{-10.8, -10.8};
                mc_lpos.SetZ( mc_lpos.Z() + zCorr[rich] );
                // If requested quantize the pixel position inside the pixel,
                // to emulate smaller pixels
                const auto pixSize = ( isInner ? m_mcPixQuantizeInner[rich] : m_mcPixQuantizeOuter[rich] );
                if ( pixSize > 0.0 ) {
                  auto quantize = [&pixSize]( const auto x ) { return std::round( x / pixSize ) * pixSize; };
                  mc_lpos.SetX( quantize( mc_lpos.X() ) );
                  mc_lpos.SetY( quantize( mc_lpos.Y() ) );
                  // have we already made a hit at this quantized (x,y) for this PD
                  auto&        xys = madeXY[id.pdID()];
                  const XYData new_xyD{mc_lpos.X(), mc_lpos.Y(), &pix, i};
                  auto         old_xyD = std::find_if( xys.begin(), xys.end(), [&new_xyD]( const auto& d ) {
                    const double tol = 1.0e-4;
                    return ( std::fabs( d.x - new_xyD.x ) < tol && std::fabs( d.y - new_xyD.y ) < tol );
                  } );
                  if ( old_xyD != xys.end() ) {
                    // which hit is the earilest ?
                    _ri_verbo << "Found existing hit for (x,y) = " << mc_lpos.X() << "," << mc_lpos.Y() << " time "
                              << pix.hitTime()[i] << " prev time " << old_xyD->time() << endmsg;
                    if ( pix.hitTime()[i] < old_xyD->time() ) {
                      _ri_verbo << " -> early, keep and disable old" << endmsg;
                      // keep this hit, disable the previous one
                      old_xyD->disable();
                      *old_xyD = new_xyD;
                      ++disHits[rich];
                    } else {
                      _ri_verbo << " -> late, disable" << endmsg;
                      // disable this hit, keep the previous one
                      pix.validMask()[i] = false;
                      ++disHits[rich];
                    }
                  } else {
                    // Not seen this (x,y) before so just add to the list
                    xys.push_back( new_xyD );
                  }
                }
                if ( pix.validMask()[i] ) {
                  // Shift back to global
                  mc_gpos = smartIDsHelper.globalPosition( mc_lpos, rich, side );
                  _ri_verbo << " -> Original  LPos " << i << " " << pix.locPos( i ) << endmsg;
                  _ri_verbo << " -> MC update LPos " << i << " " << mc_lpos << endmsg;
                  _ri_verbo << " -> Original  GPos " << i << " " << pix.gloPos( i ) << endmsg;
                  _ri_verbo << " -> MC update GPos " << i << " " << mc_gpos << endmsg;
                  // Update position for this scalar
                  pix.gloPos().X()[i] = mc_gpos.X();
                  pix.gloPos().Y()[i] = mc_gpos.Y();
                  pix.gloPos().Z()[i] = mc_gpos.Z();
                  hasPosUpdate        = true;
                }
              }

              if ( m_useMCTime ) {
                // Set the hit time using MC
                _ri_verbo << " -> original time  " << i << " " << pix.hitTime()[i] << endmsg;
                pix.hitTime()[i] = mch->second;
                _ri_verbo << " -> MC update time " << i << " " << pix.hitTime()[i] << endmsg;
              }

            } else {
              _ri_verbo << " -> No MC signal info" << endmsg;
            }

          } // valid mask
        }   // scalar loop

        // finally update SIMD local positions if needed
        if ( hasPosUpdate ) { pix.locPos() = smartIDsHelper.globalToPDPanel( rich, side, pix.gloPos() ); }

      } // SIMD pixel loop

      // Count disabled hits per event
      for ( const auto rich : {Rich::Rich1, Rich::Rich2} ) { m_disabledHits[rich] += disHits[rich]; }

      return out_pixs;
    }

  private:
    // data

    /// random number between 0 and 1
    Rndm::Numbers m_randZeroOne{};

  private:
    // counters

    /// # Disabled Hits
    mutable DetectorArray<Gaudi::Accumulators::StatCounter<>> m_disabledHits{
        {{this, "# Rich1 Disabled Hits"}, {this, "# Rich2 Disabled Hits"}}};

  private:
    // properties

    /** Temporary workaround for processing DetDesc MC as input.
     *  When active, applies corrections to the data to make compatible
     *  with the dd4hep builds. */
    Gaudi::Property<bool> m_detdescMCinput{this, "IsDetDescMC", true};

    /// Enable use of MC position info
    Gaudi::Property<bool> m_useMCPos{this, "UseMCPosition", true};

    /// Enable use of MC time info
    Gaudi::Property<bool> m_useMCTime{this, "UseMCTime", false};

    /// Include spillover events
    Gaudi::Property<bool> m_enableSpillover{this, "EnableSpillover", true};

    /// Time window shift for each RICH
    Gaudi::Property<DetectorArray<double>> m_timeShift{
        this, "TimeCalib", {0., 40.}, "Global time shift for each RICH, to get both to same calibrated point"};

    /// Inner region spatial quantisation to apply to MC local coordinate hits (<0 means do not apply)
    Gaudi::Property<DetectorArray<double>> m_mcPixQuantizeInner{
        this, "InnerPixelQuantization", {-1.0 * Gaudi::Units::mm, -1.0 * Gaudi::Units::mm}};

    /// Outer region spatial quantisation to apply to MC local coordinate hits (<0 means do not apply)
    Gaudi::Property<DetectorArray<double>> m_mcPixQuantizeOuter{
        this, "OuterPixelQuantization", {-1.0 * Gaudi::Units::mm, -1.0 * Gaudi::Units::mm}};

    /// Inner region pixel efficiency
    Gaudi::Property<DetectorArray<double>> m_effPixInner{this, "InnerPixelEff", {1.0, 1.0}};

    /// Outer region pixel efficiency
    Gaudi::Property<DetectorArray<double>> m_effPixOuter{this, "OuterPixelEff", {1.0, 1.0}};

  private:
    // Handles to access MCRichHits other than Signal, as they are not necessary there
    DataObjectReadHandle<LHCb::MCRichHits> m_prevHits{this, "PrevLocation", "Prev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_prevPrevHits{this, "PrevPrevLocation",
                                                          "PrevPrev/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextHits{this, "NextLocation", "Next/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_nextNextHits{this, "NextNextLocation",
                                                          "NextNext/" + LHCb::MCRichHitLocation::Default};
    DataObjectReadHandle<LHCb::MCRichHits> m_lhcBkgHits{this, "LHCBackgroundLocation",
                                                        "LHCBackground/" + LHCb::MCRichHitLocation::Default};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( RichPixelUseMCInfo )

} // namespace Rich::Future::Rec::MC
