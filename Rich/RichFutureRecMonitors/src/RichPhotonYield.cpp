/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/RichTrackSegment.h"

// STD
#include <mutex>

namespace Rich::Future::Rec::Moni {

  /** @class PhotonYield
   *
   *  Monitors the RICH segment expected photon yields
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class PhotonYield final : public LHCb::Algorithm::Consumer<void( const LHCb::RichTrackSegment::Vector&, //
                                                                   const PhotonYields::Vector& ),         //
                                                             Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    PhotonYield( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"PhotonYieldLocation", PhotonYieldsLocation::Detectable}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector& segments, //
                     const PhotonYields::Vector&           yields ) const override {

      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over segments and yield data
      for ( auto&& [segment, yield] : Ranges::ConstZip( segments, yields ) ) {

        // Radiator info
        const auto rad = segment.radiator();
        if ( !radiatorIsActive( rad ) ) continue;

        // loop over mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {
          // fill histo
          if ( yield[pid] > 0 ) {
            fillHisto( h_yields[rad][pid], yield[pid] );
            fillHisto( h_yieldVp[rad][pid], segment.bestMomentumMag(), yield[pid] );
          }
        }

      } // segment loop
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      using namespace Gaudi::Units;
      bool ok = true;
      // loop over active radiators
      for ( const auto rad : activeRadiators() ) {
        // loop over active mass hypos
        for ( const auto pid : activeParticlesNoBT() ) {
          // book yield histos
          ok &= saveAndCheck( h_yields[rad][pid],                         //
                              richHisto1D( HID( "yield", rad, pid ),      //
                                           "Photon Yield (>0)",           //
                                           0, m_maxYield[rad], nBins1D(), //
                                           "Photon Yield (>0)" ) );
          ok &= saveAndCheck( h_yieldVp[rad][pid],                              //
                              richProfile1D( HID( "yieldVp", rad, pid ),        //
                                             "Photon Yield (>0) V P (MeV/c)",   //
                                             1.0 * GeV, 100.0 * GeV, nBins1D(), //
                                             "Track Momentum (MeV/c)", "Photon Yield (>0)" ) );
        }
      }
      return StatusCode{ok};
    }

  private:
    // properties

    /// Maximum photon yield
    Gaudi::Property<RadiatorArray<float>> m_maxYield{this, "MaximumYields", {80, 80, 80}};

  private:
    // data

    /// mutex lock
    mutable std::mutex m_updateLock;
    /// Yield histograms
    RadiatorArray<ParticleArray<AIDA::IHistogram1D*>> h_yields = {{}};
    /// Yield versus momentum
    RadiatorArray<ParticleArray<AIDA::IProfile1D*>> h_yieldVp = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( PhotonYield )

} // namespace Rich::Future::Rec::Moni
