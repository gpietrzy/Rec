/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"
#include "RichFutureRecEvent/RichSummaryEventData.h"

// Rich Utils
#include "RichUtils/RichPDIdentifier.h"
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Track selector
#include "TrackInterfaces/ITrackSelector.h"

// STD
#include <algorithm>
#include <mutex>
#include <sstream>
#include <vector>

namespace Rich::Future::Rec::Moni {

  /** @class SIMDPhotonCherenkovAngles RichSIMDPhotonCherenkovAngles.h
   *
   *  Monitors the reconstructed cherenkov angles.
   *
   *  @author Chris Jones
   *  @date   2016-12-12
   */

  class SIMDPhotonCherenkovAngles final
      : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&,                 //
                                               const Summary::Track::Vector&,             //
                                               const SIMDPixelSummaries&,                 //
                                               const Relations::PhotonToParents::Vector&, //
                                               const LHCb::RichTrackSegment::Vector&,     //
                                               const CherenkovAngles::Vector&,            //
                                               const SIMDCherenkovPhoton::Vector& ),
                                         Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    SIMDPhotonCherenkovAngles( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"TracksLocation", LHCb::TrackLocation::Default},
                     KeyValue{"SummaryTracksLocation", Summary::TESLocations::Tracks},
                     KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                     KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                     KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                     KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                     KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      setProperty( "NBins1DHistos", 100 ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::Track::Range&                 tracks,        //
                     const Summary::Track::Vector&             sumTracks,     //
                     const SIMDPixelSummaries&                 pixels,        //
                     const Relations::PhotonToParents::Vector& photToSegPix,  //
                     const LHCb::RichTrackSegment::Vector&     segments,      //
                     const CherenkovAngles::Vector&            expTkCKThetas, //
                     const SIMDCherenkovPhoton::Vector&        photons ) const override;

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override;

  private:
    /// Get the per PD resolution histogram ID
    inline std::string pdResPlotID( const LHCb::RichSmartID hpd ) const {
      const Rich::DAQ::PDIdentifier hid( hpd );
      std::ostringstream            id;
      id << "PDs/pd-" << hid.number();
      return id.str();
    }

  private:
    // cached data

    /// mutex lock
    mutable std::mutex m_updateLock;

    // CK theta histograms
    RadiatorArray<AIDA::IHistogram1D*>                          h_thetaRec                = {{}};
    RadiatorArray<AIDA::IHistogram1D*>                          h_phiRec                  = {{}};
    RadiatorArray<AIDA::IHistogram1D*>                          h_ckResAll                = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>>              h_ckResAllPerPanel        = {{}};
    RadiatorArray<AIDA::IHistogram1D*>                          h_ckResAllBetaCut         = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>>              h_ckResAllPerPanelBetaCut = {{}};
    RadiatorArray<AIDA::IHistogram1D*>                          h_ckResAllInner           = {{}};
    RadiatorArray<AIDA::IHistogram1D*>                          h_ckResAllOuter           = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>>              h_ckResAllPerPanelInner   = {{}};
    RadiatorArray<PanelArray<AIDA::IHistogram1D*>>              h_ckResAllPerPanelOuter   = {{}};
    RadiatorArray<PanelArray<std::vector<AIDA::IHistogram1D*>>> h_ckResAllPerCol          = {{}};
    RadiatorArray<PanelArray<std::vector<AIDA::IHistogram1D*>>> h_ckResAllPerColInner     = {{}};
    RadiatorArray<PanelArray<std::vector<AIDA::IHistogram1D*>>> h_ckResAllPerColOuter     = {{}};

  private:
    // properties and tools

    /// minimum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_minBeta{this, "MinBeta", {0.9999f, 0.9999f, 0.9999f}};

    /// maximum beta value for tracks
    Gaudi::Property<RadiatorArray<float>> m_maxBeta{this, "MaxBeta", {999.99f, 999.99f, 999.99f}};

    /// Min theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMin{this, "ChThetaRecHistoLimitMin", {0.150f, 0.010f, 0.010f}};

    /// Max theta limit for histos for each rad
    Gaudi::Property<RadiatorArray<float>> m_ckThetaMax{this, "ChThetaRecHistoLimitMax", {0.325f, 0.056f, 0.033f}};

    /// Histogram ranges for CK resolution plots
    Gaudi::Property<RadiatorArray<float>> m_ckResRange{this, "CKResHistoRange", {0.025f, 0.0026f, 0.002f}};

    /** Track selector.
     *  Longer term should get rid of this and pre-filter the input data instead.
     */
    ToolHandle<const ITrackSelector> m_tkSel{this, "TrackSelector", "TrackSelector"};
  };

} // namespace Rich::Future::Rec::Moni

using namespace Rich::Future::Rec::Moni;

//-----------------------------------------------------------------------------

StatusCode SIMDPhotonCherenkovAngles::prebookHistograms() {

  bool ok = true;

  // Loop over radiators
  for ( const auto rad : activeRadiators() ) {

    // beta cut string
    std::string betaS = "beta";
    if ( m_minBeta[rad] > 0.0 ) { betaS = std::to_string( m_minBeta[rad] ) + "<" + betaS; }
    if ( m_maxBeta[rad] < 1.0 ) { betaS = betaS + "<" + std::to_string( m_maxBeta[rad] ); }

    // RICH detector
    const Rich::DetectorType rich = ( Rich::Rich2Gas == rad ? Rich::Rich2 : Rich::Rich1 );

    // inclusive plots
    ok &= saveAndCheck( h_thetaRec[rad], richHisto1D( HID( "thetaRec", rad ),                          //
                                                      "Reconstructed CKTheta - All photons",           //
                                                      m_ckThetaMin[rad], m_ckThetaMax[rad], nBins1D(), //
                                                      "Cherenkov Theta / rad" ) );
    ok &= saveAndCheck( h_phiRec[rad], richHisto1D( HID( "phiRec", rad ),                   //
                                                    "Reconstructed CKPhi - All photons",    //
                                                    0.0, 2.0 * Gaudi::Units::pi, nBins1D(), //
                                                    "Cherenkov Phi / rad" ) );
    ok &= saveAndCheck( h_ckResAll[rad], richHisto1D( HID( "ckResAll", rad ),                           //
                                                      "Rec-Exp CKTheta - All photons",                  //
                                                      -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                                      "delta(Cherenkov theta) / rad" ) );
    // with beta cut
    ok &= saveAndCheck( h_ckResAllBetaCut[rad],                                        //
                        richHisto1D( HID( "ckResAllBetaCut", rad ),                    //
                                     "Rec-Exp CKTheta - All photons - " + betaS,       //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov theta) / rad" ) );

    // loop over detector sides
    for ( const auto side : Rich::sides() ) {
      ok &= saveAndCheck( h_ckResAllPerPanel[rad][side],                                 //
                          richHisto1D( HID( "ckResAllPerPanel", side, rad ),             //
                                       "Rec-Exp CKTheta - All photons",                  //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
      // beta cut
      ok &= saveAndCheck( h_ckResAllPerPanelBetaCut[rad][side],
                          richHisto1D( HID( "ckResAllPerPanelBetaCut", side, rad ),      //
                                       "Rec-Exp CKTheta - All photons - " + betaS,       //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
      // Per Column resolutions
      const auto nCols = LHCb::RichSmartID::MaPMT::ModuleColumnsPerPanel[rich];
      h_ckResAllPerCol[rad][side].resize( nCols );
      h_ckResAllPerColInner[rad][side].resize( nCols );
      h_ckResAllPerColOuter[rad][side].resize( nCols );
      for ( std::size_t iCol = 0; iCol < nCols; ++iCol ) {
        const auto sCol = std::to_string( iCol );
        ok &= saveAndCheck( h_ckResAllPerCol[rad][side].at( iCol ),
                            richHisto1D( HID( "cols/ckResAllCol" + sCol, side, rad ),      //
                                         "Rec-Exp CKTheta - All photons - Column " + sCol, //
                                         -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                         "delta(Cherenkov theta) / rad" ) );
        // Plots for Inner and outer regions
        ok &= saveAndCheck( h_ckResAllPerColInner[rad][side].at( iCol ),
                            richHisto1D( HID( "cols/Inner/ckResAllCol" + sCol, side, rad ),                    //
                                         "Rec-Exp CKTheta - All photons - Column " + sCol + " - Inner Region", //
                                         -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),                     //
                                         "delta(Cherenkov theta) / rad" ) );
        ok &= saveAndCheck( h_ckResAllPerColOuter[rad][side].at( iCol ),
                            richHisto1D( HID( "cols/Outer/ckResAllCol" + sCol, side, rad ),                    //
                                         "Rec-Exp CKTheta - All photons - Column " + sCol + " - Outer Region", //
                                         -m_ckResRange[rad], m_ckResRange[rad], nBins1D(),                     //
                                         "delta(Cherenkov theta) / rad" ) );
      }
    }

    // Make plots for inner and outer regions
    ok &= saveAndCheck( h_ckResAllInner[rad],                                          //
                        richHisto1D( HID( "ckResAllInner", rad ),                      //
                                     "Rec-Exp CKTheta - All photons - Inner Region",   //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov theta) / rad" ) );
    ok &= saveAndCheck( h_ckResAllOuter[rad],                                          //
                        richHisto1D( HID( "ckResAllOuter", rad ),                      //
                                     "Rec-Exp CKTheta - All photons - Outer Region",   //
                                     -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                     "delta(Cherenkov theta) / rad" ) );
    for ( const auto side : Rich::sides() ) {
      ok &= saveAndCheck( h_ckResAllPerPanelInner[rad][side],                            //
                          richHisto1D( HID( "ckResAllPerPanelInner", side, rad ),        //
                                       "Rec-Exp CKTheta - All photons - Inner Region",   //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
      ok &= saveAndCheck( h_ckResAllPerPanelOuter[rad][side],                            //
                          richHisto1D( HID( "ckResAllPerPanelOuter", side, rad ),        //
                                       "Rec-Exp CKTheta - All photons - Outer Region",   //
                                       -m_ckResRange[rad], m_ckResRange[rad], nBins1D(), //
                                       "delta(Cherenkov theta) / rad" ) );
    }

  } // active rad loop

  return StatusCode{ok};
}

//-----------------------------------------------------------------------------

void SIMDPhotonCherenkovAngles::operator()( const LHCb::Track::Range&                 tracks,        //
                                            const Summary::Track::Vector&             sumTracks,     //
                                            const SIMDPixelSummaries&                 pixels,        //
                                            const Relations::PhotonToParents::Vector& photToSegPix,  //
                                            const LHCb::RichTrackSegment::Vector&     segments,      //
                                            const CherenkovAngles::Vector&            expTkCKThetas, //
                                            const SIMDCherenkovPhoton::Vector&        photons ) const {

  // the lock
  std::lock_guard lock( m_updateLock );

  // loop over the track containers
  for ( const auto&& [tk, sumTk] : Ranges::ConstZip( tracks, sumTracks ) ) {
    // Is this track selected ?
    if ( !m_tkSel.get()->accept( *tk ) ) continue;

    // loop over photons for this track
    for ( const auto photIn : sumTk.photonIndices() ) {
      // photon data
      const auto& phot = photons[photIn];
      const auto& rels = photToSegPix[photIn];
      // the segment for this photon
      const auto& seg = segments[rels.segmentIndex()];
      // Get the SIMD summary pixel
      const auto& simdPix = pixels[rels.pixelIndex()];

      // get the expected CK theta values for this segment
      const auto& expCKangles = expTkCKThetas[rels.segmentIndex()];

      // Radiator info
      const auto rad = seg.radiator();
      if ( !radiatorIsActive( rad ) ) continue;

      // Segment momentum
      const auto pTot = seg.bestMomentumMag();

      // The PID type to assume. Just use Pion here.
      const auto pid = Rich::Pion;

      // beta
      const auto beta = richPartProps()->beta( pTot, pid );

      // selection cuts
      const auto betaC = ( beta >= m_minBeta[rad] && beta <= m_maxBeta[rad] );

      // expected CK theta
      const auto thetaExp = expCKangles[pid];

      // Loop over scalar entries in SIMD photon
      for ( std::size_t i = 0; i < SIMDCherenkovPhoton::SIMDFP::Size; ++i ) {
        // Select valid entries
        if ( phot.validityMask()[i] ) {

          // reconstructed theta
          const auto thetaRec = phot.CherenkovTheta()[i];
          // reconstructed phi
          const auto phiRec = phot.CherenkovPhi()[i];
          // delta theta
          const auto deltaTheta = thetaRec - thetaExp;

          // SmartID
          const auto id = phot.smartID()[i];

          // Detctor side
          const auto side = id.panel();

          // Inner or outer region ?
          const auto isInner = simdPix.isInnerRegion()[i];

          // fill some plots
          fillHisto( h_thetaRec[rad], thetaRec );
          fillHisto( h_phiRec[rad], phiRec );
          fillHisto( h_ckResAll[rad], deltaTheta );
          fillHisto( h_ckResAllPerPanel[rad][side], deltaTheta );
          fillHisto( ( isInner ? h_ckResAllInner[rad] : h_ckResAllOuter[rad] ), deltaTheta );
          fillHisto( ( isInner ? h_ckResAllPerPanelInner[rad][side] : h_ckResAllPerPanelOuter[rad][side] ),
                     deltaTheta );
          if ( betaC ) {
            fillHisto( h_ckResAllBetaCut[rad], deltaTheta );
            fillHisto( h_ckResAllPerPanelBetaCut[rad][side], deltaTheta );
          }
          // FIXME
          // Note very old MC samples have a different number of columns.
          // Handle here by just silently rejecting those out-of-range.
          // Once support for these old samples is dropped this can be
          // changed to a full assert check.
          const auto iCol = id.panelLocalModuleColumn();
          if ( iCol < h_ckResAllPerCol[rad][side].size() ) {
            fillHisto( h_ckResAllPerCol[rad][side].at( iCol ), deltaTheta );
            fillHisto( ( isInner ? h_ckResAllPerColInner[rad][side].at( iCol ) : //
                             h_ckResAllPerColOuter[rad][side].at( iCol ) ),
                       deltaTheta );
          }

        } // valid scalars
      }   // SIMD loop
    }
  }
}

//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonCherenkovAngles )

//-----------------------------------------------------------------------------
