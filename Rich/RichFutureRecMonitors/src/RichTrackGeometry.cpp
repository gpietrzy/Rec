/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STD
#include <mutex>
#include <numeric>

// Gaudi
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/PhysicalConstants.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// base class
#include "RichFutureRecBase/RichRecHistoAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Consumer.h"

// Rich Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"
#include "RichFutureRecEvent/RichRecSpacePoints.h"

// Kernel
#include "LHCbMath/FastMaths.h"

namespace Rich::Future::Rec::Moni {

  /** @class TrackGeometry
   *
   *  Monitors the RICH mass hypothesis rings
   *
   *  @author Chris Jones
   *  @date   2020-03-30
   */

  class TrackGeometry final : public LHCb::Algorithm::Consumer<void( const LHCb::RichTrackSegment::Vector& ), //
                                                               Gaudi::Functional::Traits::BaseClass_t<HistoAlgBase>> {

  public:
    /// Standard constructor
    TrackGeometry( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    // input data
                    {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default}} ) {
      // print some stats on the final plots
      setProperty( "HistoPrint", true ).ignore();
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE ).ignore();
    }

  public:
    /// Functional operator
    void operator()( const LHCb::RichTrackSegment::Vector& segments ) const override {

      // the lock
      std::lock_guard lock( m_updateLock );

      // loop over the segments
      for ( const auto& segment : segments ) {
        // Which radiator
        const auto rad = segment.radiator();
        // fill histos
        fillHisto( h_tkPathL[rad], segment.pathLength() );
        fillHisto( h_tkEntZ[rad], segment.entryPoint().Z() );
        fillHisto( h_tkExtZ[rad], segment.exitPoint().Z() );
        fillHisto( h_tkEntXY[rad], segment.entryPoint().X(), segment.entryPoint().Y() );
        fillHisto( h_tkExtXY[rad], segment.exitPoint().X(), segment.exitPoint().Y() );
      }
    }

  protected:
    /// Pre-Book all histograms
    StatusCode prebookHistograms() override {
      bool                        ok       = true;
      const RadiatorArray<double> minZ     = {0, 900, 9000};
      const RadiatorArray<double> maxZ     = {0, 2500, 12000};
      const RadiatorArray<double> maxX     = {0, 1000, 5000};
      const RadiatorArray<double> maxY     = {0, 1000, 4000};
      const RadiatorArray<double> maxPathL = {0, 2000, 4000};
      // loop over active radiators
      for ( const auto rad : activeRadiators() ) {
        ok &= saveAndCheck( h_tkEntZ[rad],                                //
                            richHisto1D( HID( "tkEntryZ", rad ),          //
                                         "Track Entry point Z",           //
                                         minZ[rad], maxZ[rad], nBins1D(), //
                                         "Track Entry Point Z / mm" ) );
        ok &= saveAndCheck( h_tkExtZ[rad],                                //
                            richHisto1D( HID( "tkExitZ", rad ),           //
                                         "Track Exit point Z",            //
                                         minZ[rad], maxZ[rad], nBins1D(), //
                                         "Track Exit Point Z / mm" ) );
        ok &= saveAndCheck( h_tkPathL[rad],                           //
                            richHisto1D( HID( "tkPathLength", rad ),  //
                                         "Track Path Length",         //
                                         0, maxPathL[rad], nBins1D(), //
                                         "Track Path Length / mm" ) );
        ok &= saveAndCheck( h_tkEntXY[rad],                                //
                            richHisto2D( HID( "tkEntryXY", rad ),          //
                                         "Track Entry point (X,Y)",        //
                                         -maxX[rad], maxX[rad], nBins2D(), //
                                         -maxY[rad], maxY[rad], nBins2D(), //
                                         "Track Entry Point X / mm",       //
                                         "Track Entry Point Y / mm" ) );
        ok &= saveAndCheck( h_tkExtXY[rad],                                //
                            richHisto2D( HID( "tkExitXY", rad ),           //
                                         "Track Exit point (X,Y)",         //
                                         -maxX[rad], maxX[rad], nBins2D(), //
                                         -maxY[rad], maxY[rad], nBins2D(), //
                                         "Track Exit Point X / mm",        //
                                         "Track Exit Point Y / mm" ) );
      }
      return StatusCode{ok};
    }

  private:
    // properties

    /// mutex lock
    mutable std::mutex m_updateLock;
    // histos
    RadiatorArray<AIDA::IHistogram1D*> h_tkEntZ  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_tkExtZ  = {{}};
    RadiatorArray<AIDA::IHistogram1D*> h_tkPathL = {{}};
    RadiatorArray<AIDA::IHistogram2D*> h_tkEntXY = {{}};
    RadiatorArray<AIDA::IHistogram2D*> h_tkExtXY = {{}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( TrackGeometry )

} // namespace Rich::Future::Rec::Moni
