/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi (must be first)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Rec Event
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecCherenkovPhotons.h"
#include "RichFutureRecEvent/RichRecPhotonPredictedPixelSignals.h"
#include "RichFutureRecEvent/RichRecPhotonYields.h"
#include "RichFutureRecEvent/RichRecRelations.h"
#include "RichFutureRecEvent/RichRecSIMDPixels.h"

// Rich Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/ZipRange.h"

// Detector Description
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich2.h"

// STL
#include <array>
#include <limits>

namespace Rich::Future::Rec {

  // Shortcut to output data
  namespace {

    // init value to make sure we never use uninitalised data
    inline constexpr auto NaN = std::numeric_limits<float>::signaling_NaN();

    // output data type
    using OutData = SIMDPhotonSignals::Vector;

    /// Struct for cached information
    struct alignas( LHCb::SIMD::VectorAlignment ) PredSignalDataCache {
      /// cached SIMD scale factor / RoC^2
      DetectorArray<SIMDCherenkovPhoton::SIMDFP> factor = {{NaN, NaN}};
    };

  } // namespace

  /** @class SIMDPhotonPredictedPixelSignal
   *
   *  Computes the expected pixel signals for photon candidates.
   *
   *  See note LHCB/98-040 page 11 equation 18
   *
   *  @author Chris Jones
   *  @date   2016-10-19
   */
  class SIMDPhotonPredictedPixelSignal final
      : public LHCb::Algorithm::Transformer<OutData( const SIMDPixelSummaries&,                 //
                                                     const SIMDCherenkovPhoton::Vector&,        //
                                                     const Relations::PhotonToParents::Vector&, //
                                                     const LHCb::RichTrackSegment::Vector&,     //
                                                     const CherenkovAngles::Vector&,            //
                                                     const CherenkovResolutions::Vector&,       //
                                                     const PhotonYields::Vector&,               //
                                                     const PredSignalDataCache& ),
                                            LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, PredSignalDataCache>> {

  public:
    /// Standard constructor
    SIMDPhotonPredictedPixelSignal( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // input data
                       {KeyValue{"RichSIMDPixelSummariesLocation", SIMDPixelSummariesLocation::Default},
                        KeyValue{"CherenkovPhotonLocation", SIMDCherenkovPhotonLocation::Default},
                        KeyValue{"PhotonToParentsLocation", Relations::PhotonToParentsLocation::Default},
                        KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                        KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Signal},
                        KeyValue{"CherenkovResolutionsLocation", CherenkovResolutionsLocation::Default},
                        KeyValue{"PhotonYieldLocation", PhotonYieldsLocation::Detectable},
                        // input conditions data
                        KeyValue{"DataCache", DeRichLocations::derivedCondition( name + "-DataCache" )}},
                       // output data
                       {KeyValue{"PhotonSignalsLocation", SIMDPhotonSignalsLocation::Default}} ) {
      // debug
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialization of the tool after creation
    StatusCode initialize() override;

  public:
    /// Functional operator
    OutData operator()( const SIMDPixelSummaries&                 pixels,     ///< pixels
                        const SIMDCherenkovPhoton::Vector&        photons,    ///< photons
                        const Relations::PhotonToParents::Vector& photRels,   ///< photon relations
                        const LHCb::RichTrackSegment::Vector&     segments,   ///< segments
                        const CherenkovAngles::Vector&            ckAngles,   ///< CK theta angles
                        const CherenkovResolutions::Vector&       ckRes,      ///< CK theta track resolutions
                        const PhotonYields::Vector&               photYields, ///< photon yields
                        const PredSignalDataCache&                dataCache   ///< Derived data cache
                        ) const override;

  private:
    /// Scalar type
    using FP = SIMDCherenkovPhoton::FP;
    /// SIMD type
    using SIMDFP = SIMDCherenkovPhoton::SIMDFP;

  private:
    /// The exponential function
    inline SIMDFP myexp( const SIMDFP& x ) const noexcept {
      // Fast VDT like function
      return Rich::Maths::fast_exp( x );
      // Approximation
      // return Rich::Maths::Approx::approx_exp(x);
      // Even faster approximation
      // return Rich::Maths::Approx::vapprox_exp(x);
    }

  private:
    /// Cache SIMD minimum argument value for the probability value
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_minArgSIMD = SIMDFP::Zero();

    /// Cache exp(min arg)
    alignas( LHCb::SIMD::VectorAlignment ) SIMDFP m_expMinArg = SIMDFP::Zero();

    /// Cache SIMD minimum cut value for photon probability
    alignas( LHCb::SIMD::VectorAlignment ) RadiatorArray<SIMDFP> m_minPhotonProbSIMD = {{}};

    /// cached min exp(arg) factor
    alignas( LHCb::SIMD::VectorAlignment ) RadiatorArray<SIMDFP> m_minExpArgF = {{}};

  private:
    /// The minimum expected track Cherenkov angle to be considered 'Above Threshold'
    Gaudi::Property<RadiatorArray<float>> m_minExpCKT{
        this,
        "MinExpTrackCKTheta",
        {0.0f, 0.0f, 0.0f}, // was { 1e-6f, 1e-6f, 1e-6f }
        "The minimum expected track Cherenkov angle for each radiator (Aero/R1Gas/R2Gas)"};

    /// The minimum cut value for photon probability
    Gaudi::Property<RadiatorArray<float>> m_minPhotonProb{
        this,
        "MinPhotonProbability",
        {1e-15f, 1e-15f, 1e-15f},
        "The minimum allowed photon probability values for each radiator (Aero/R1Gas/R2Gas)"};

    /// Scale factors
    Gaudi::Property<DetectorArray<float>> m_scaleFactor{this, "ScaleFactor", {4.0f, 4.0f}};

    /// The minimum argument value for the probability value
    Gaudi::Property<float> m_minArg{this, "MinExpArg", -80.0f};

    /// Enable 4D time window
    Gaudi::Property<DetectorArray<bool>> m_enable4D{this, "Enable4D", {false, false}, "Enable 4D reconstruction"};
  };

} // namespace Rich::Future::Rec

using namespace Rich::Future::Rec;

//=============================================================================

StatusCode SIMDPhotonPredictedPixelSignal::initialize() {

  const auto sc = Transformer::initialize();
  if ( !sc ) return sc;

  // The detector objects
  Detector::Rich1::addConditionDerivation( this );
  Detector::Rich2::addConditionDerivation( this );

  // derived data cache
  addConditionDerivation(
      {Detector::Rich1::DefaultConditionKey, Rich::Detector::Rich2::DefaultConditionKey}, // input conditions locations
      inputLocation<PredSignalDataCache>(),                                 // output location for data cache
      [scaleF = m_scaleFactor.value()]( const Rich::Detector::Rich1& rich1, //
                                        const Rich::Detector::Rich2& rich2 ) {
        // Mirror radii of curvature in mm
        const DetectorArray<double> radiusCurv = {rich1.sphMirrorRadius(), rich2.sphMirrorRadius()};
        // cache the factors for each RICH
        auto cache = PredSignalDataCache();
        for ( const auto det : {Rich::Rich1, Rich::Rich2} ) {
          cache.factor[det] = ( scaleF[det] / ( std::pow( radiusCurv[det], 2 ) * std::pow( 2.0 * M_PI, 1.5 ) ) );
        }
        return cache;
      } );

  // loop over RICHes
  for ( const auto rich : activeDetectors() ) {
    if ( m_enable4D[rich] ) { info() << "4D signals enabled for " << rich << endmsg; }
  }

  // loop over radiators
  for ( const auto rad : activeRadiators() ) {
    // cache SIMD versions of min prob values
    m_minPhotonProbSIMD[rad] = SIMDFP( m_minPhotonProb[rad] );
    // Compute an estimate of the minimum argument to the exp function below
    // with which we can be sure the pixel would anyway always fail the min
    // probability cut. The factor of 100 is to take into account the maximum
    // possible factor exp(arg) is scaled by. Max normally never goes above 2
    // so 100 is very safe...
    m_minExpArgF[rad] = SIMDFP( std::max( m_minArg.value(), std::log( m_minPhotonProb[rad] / 100.0f ) ) );
    _ri_verbo << rad << " minExpArgF " << m_minExpArgF << endmsg;
  }

  // cache min exp argument
  m_minArgSIMD = SIMDFP( m_minArg.value() );
  m_expMinArg  = std::exp( m_minArgSIMD );

  // return
  return sc;
}

//=============================================================================

OutData SIMDPhotonPredictedPixelSignal::operator()( const SIMDPixelSummaries&                 pixels,     //
                                                    const SIMDCherenkovPhoton::Vector&        photons,    //
                                                    const Relations::PhotonToParents::Vector& photRels,   //
                                                    const LHCb::RichTrackSegment::Vector&     segments,   //
                                                    const CherenkovAngles::Vector&            ckAngles,   //
                                                    const CherenkovResolutions::Vector&       ckRes,      //
                                                    const PhotonYields::Vector&               photYields, //
                                                    const PredSignalDataCache&                dataCache ) const {

  using namespace LHCb::SIMD;

  // output data
  OutData signals;
  signals.reserve( photons.size() );

  // Min CK theta angle for signal
  const SIMDFP minCKTheta( 1e-10 );

  // Form the zipped track data range
  const auto tkRange = Ranges::ConstZip( ckAngles, ckRes, photYields, segments );

  // Loop over photon data
  for ( const auto&& [phot, rels] : Ranges::ConstZip( photons, photRels ) ) {

    // Save a new entry in the output for this photon
    auto& sigs = signals.emplace_back();

    // Which detector ?
    const auto det = phot.rich();

    // Reconstructed Cherenkov theta angle
    auto thetaReco                      = phot.CherenkovTheta();
    thetaReco( thetaReco < minCKTheta ) = minCKTheta;

    // get the pixel summary via index
    const auto& pix = pixels[rels.pixelIndex()];

    // Compute the ID independent term
    const auto AInd = pix.effArea() * phot.activeSegmentFraction() * dataCache.factor[det] / thetaReco;

    // Access the segment data via index
    const auto& [tkCkAngles, tkCkRes, tkYields, segment] = tkRange[rels.segmentIndex()];

    // radiator
    const auto rad = segment.radiator();

    // Loop over the mass hypos and compute and fill each value
    for ( const auto id : activeParticlesNoBT() ) {

      // get the expected CK theta angle for this hypo
      const auto tkA = tkCkAngles[id];

      // mass types are strictly ordered by increasing mass, so once we
      // are below threshold can abort the mass hypothesis loop.
      if ( tkA <= m_minExpCKT[rad] ) { break; }

      // 1 / Expected Cherenkov theta angle resolution
      const SIMDFP thetaExpResInv( 1.0f / tkCkRes[id] );

      // Theta diff / resolution
      const auto thetaDiffOvRes = ( thetaReco - SIMDFP( tkA ) ) * thetaExpResInv;

      // compute the signal probability for this hypo

      // First the argument to exp() function
      auto arg = SIMDFP( -0.5f ) * thetaDiffOvRes * thetaDiffOvRes;

      // selection mask
      auto mask = phot.validityMask() && ( arg > m_minExpArgF[rad] );

      // Apply 4D time window if enabled
      if ( m_enable4D[det] ) {
        // Get time prediction for this mass hypo
        const auto timePred = SIMDFP( segment.timeToRadEntry( id ) ) + phot.radTransitTime();
        // Apply time window to mask
        mask &= pix.isInTime( timePred );
      }

      // If any are OK continue
      if ( any_of( mask ) ) {

        // Set any arg values below the minimum to the min
        arg( !mask ) = m_minArgSIMD;

        // compute exp(arg)
        const auto expArg = myexp( arg );

        // Expected yield
        const SIMDFP tkY( tkYields[id] );

        // The signal to set
        auto& sig = sigs[id];

        // Compute the signal
        sig = AInd * expArg * tkY * thetaExpResInv;

        // Check min prob value
        mask &= sig > m_minPhotonProbSIMD[rad];
        sig.setZeroInverted( mask );

      } // min arg

    } // hypo loop
  }

  // return the final data
  return signals;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SIMDPhotonPredictedPixelSignal )

//=============================================================================
