###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Configurables import Brunel
from PRConfig import TestFileDB

TestFileDB.test_file_db["2016NB_25ns_L0Filt0x1609"].run(configurable=Brunel())

Brunel().DataType = "2016"
Brunel().Simulation = False
Brunel().WithMC = False
