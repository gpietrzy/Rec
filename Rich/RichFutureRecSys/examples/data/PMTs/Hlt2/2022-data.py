from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
import glob
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/data/RunIII/Hlt2/LHCb/0000248711/",
    # CERN EOS
    "/eos/lhcb/hlt2/LHCb/0000248711/"
]

print("Data Files :-")
data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.mdf"))
    data += ["PFN:" + file for file in files]
    for f in files:
        print(f)

IOHelper('MDF').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp, DDDBConf
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"
LHCbApp().DDDBtag = "upgrade/master"
from DDDB.CheckDD4Hep import UseDD4Hep
if not UseDD4Hep:
    LHCbApp().CondDBtag = "upgrade/master"

from Configurables import CondDB
CondDB().setProp("Upgrade", True)
