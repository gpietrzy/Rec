###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

from Gaudi.Configuration import *
import glob, os
from GaudiConf import IOHelper

# Check what is available
searchPaths = [
    # Cambridge
    "/usera/jonesc/NFS/data/MC/Run3/RealTel40Format/Format10/*/LDST/",
    # CernVM
    "/home/chris/LHCb/Data/MC/Run3/RealTel40Format/Format10/LDST/"
]

data = []
for path in searchPaths:
    files = sorted(glob.glob(path + "*.ldst"))
    data += ["DATAFILE='" + file + "'" for file in files]
print "Found", len(data), "data files"

# Batch options ?
if "CONDOR_FILE_BATCH" in os.environ:
    # Use env vars to define reduced file range for this job
    batch = int(os.environ["CONDOR_FILE_BATCH"])
    nfiles = int(os.environ["CONDOR_FILES_PER_BATCH"])
    firstFile = batch * nfiles
    lastFile = (batch + 1) * nfiles
    if firstFile <= len(data):
        if lastFile > len(data): lastFile = len(data)
        print "Using restricted file range", firstFile, lastFile
        data = data[firstFile:lastFile]
        for f in data:
            print " ", f
    else:
        print "WARNING: File range outside input data list"
        data = []

IOHelper('ROOT').inputFiles(data, clear=True)
FileCatalog().Catalogs = ['xmlcatalog_file:out.xml']

from Configurables import LHCbApp
LHCbApp().Simulation = True
LHCbApp().DataType = "Upgrade"

#LHCbApp().DDDBtag = "upgrade/jonrob/add-realistic-pmt-hardware-maps"
#LHCbApp().CondDBtag = "upgrade/jonrob/add-realistic-pmt-hardware-maps"
LHCbApp().DDDBtag = "upgrade/dddb-20201029"
LHCbApp().CondDBtag = "upgrade/sim-20201029-vc-md100"
