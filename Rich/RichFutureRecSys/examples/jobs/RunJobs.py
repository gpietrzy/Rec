#!/usr/bin/env python3
###############################################################################
# (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################

# Does bad things here so just disable...
# yapf: disable

import subprocess, os, argparse

parser = argparse.ArgumentParser(
    description="Submit condor jobs for RICH reco",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument(
    "-l",
    "--lumis",
    help="luminosity value(s)",
    default="1.5e34,1.2e34,1.0e34,3.0e33,2.0e33,2.0e32",
    type=str)
parser.add_argument("-4d", "--enable4D", help="Enable 4D", action='store_true')
parser.add_argument("--useMCTracks", help="Use tracks built from MC information", action='store_true')
parser.add_argument("--useMCHits", help="Use hits built from MC information", action='store_true')
parser.add_argument("--usePixelMCInfo", help="Enable use of pixel MC info", action='store_true')
parser.add_argument("--usePhotonMCInfo", help="Enable use of photon MC info", action='store_true')
parser.add_argument("--overrideDetRegions", help="Enable override of detector regions", action='store_true')
parser.add_argument(
    "--pixwins",
    help="Pixel time window(s) (only for 4D)",
    default="1.0",
    type=str)
parser.add_argument(
    "--photwinsR1",
    help="RICH1 (Inner,Outer) Photon time window(s) (only for 4D)",
    default="0.300,0.300 : 0.150,0.150 : 0.075,0.075 : 0.050,0.050",
    type=str)
parser.add_argument(
    "--photwinsR2",
    help="RICH2 (Inner,Outer) Photon time window(s) (only for 4D)",
    default="0.300,0.300 : 0.150,0.150 : 0.075,0.075 : 0.050,0.050",
    type=str)
parser.add_argument(
    "--pixbckwR1",
    help="Pixel background weights for RICH1",
    default="",
    type=str)
parser.add_argument(
    "--pixbckwR2",
    help="Pixel background weights for RICH2",
    default="",
    type=str)
parser.add_argument(
    "--pixQR1", help="(Inner,Outer) RICH1 Pixel Quantisation", default="", type=str)
parser.add_argument(
    "--pixQR2", help="(Inner,Outer) RICH2 Pixel Quantisation", default="", type=str)
parser.add_argument(
    "--ckResScaleR1",
    help="RICH1 (Inner,Outer) CK theta resolution scale factor for emulation",
    default="",
    type=str)
parser.add_argument(
    "--ckResScaleR2",
    help="RICH2 (Inner,Outer) CK theta resolution scale factor for emulation",
    default="",
    type=str)
parser.add_argument(
    "--detEffR1",
    help="RICH1 (Inner,Outer) detector efficiency",
    default="",
    type=str)
parser.add_argument(
    "--detEffR2",
    help="RICH2 (Inner,Outer) detector efficiency",
    default="",
    type=str)
parser.add_argument("-N", "--name", help="Job Name", default="Test", type=str)
parser.add_argument(
    "-D",
    "--directory",
    help="Scripts Directory",
    default=
    "/usera/jonesc/LHCb/stack/Feature/Rec/Rich/RichFutureRecSys/examples/jobs",
    type=str)
args = parser.parse_args()
config = vars(args)
#print(config)

name = config["name"]

os.environ["USEMCTRACKS"] = ( "True" if config["useMCTracks"] else "False" )
name += "/" + ( "MCTracks" if config["useMCTracks"] else "RealTracks" )

os.environ["USEMCHITS"] = ( "True" if config["useMCHits"] else "False" )
name += "/" + ( "MCHits" if config["useMCHits"] else "RealHits" )

enable4D = config["enable4D"]
os.environ["ENABLE4D"] = ( "True" if enable4D else "False" )

os.environ["USEPIXELMCINFO"] =  ( "True" if config["usePixelMCInfo"] else "False" )

os.environ["USEPHOTONMCINFO"] =  ( "True" if config["usePhotonMCInfo"] else "False" )

os.environ["OVERRIDEDETREGIONS"] =  ( "True" if config["overrideDetRegions"] else "False" )

os.environ["THOR_JIT_N_JOBS"] = "1"

optsroot = config["directory"]

# List of env vars that have been set
env_vars = []


def nFiles(lumi):
    if lumi < 1.0e33: return "80"
    if lumi < 1.0e34: return "60"
    return "100"


def nFilesPerJob(lumi):
    return ("20" if lumi < 1.0e34 else "20")


def unsetenv():
    global env_vars
    for var in env_vars:
        #print("Unsetting", var)
        del os.environ[var]
    env_vars = []


def setenv(var, val):
    global env_vars
    if var not in env_vars:
        #print("Setting", var, "=", val)
        os.environ[var] = val
        env_vars += [var]
    else:
        print("WARNING: Env var", var, "already set as", os.environ[var])


def nameAdd(opt, val):
    return "/" + opt + "-" + "{:.3f}".format(float(val))


for Lumi in config["lumis"].split(","):

    for r1PixQ in config["pixQR1"].split(":"):
        for r2PixQ in config["pixQR2"].split(":"):

            for r1PixW in config["pixbckwR1"].split(","):
                for r2PixW in config["pixbckwR2"].split(","):

                    for r1ResSc in config["ckResScaleR1"].split(":"):
                        for r2ResSc in config["ckResScaleR2"].split(":"):

                            for r1DetEff in config["detEffR1"].split(":"):
                                for r2DetEff in config["detEffR2"].split(":"):

                                    if enable4D:

                                        for PixWin in config["pixwins"].split(","):
                                            for PhotWinR1 in config["photwinsR1"].split(":"):
                                                for PhotWinR2 in config["photwinsR2"].split(":"):

                                                    setenv("LUMI", Lumi)

                                                    jobName = name + "/4D/lumi-" + Lumi
                                                    if len(r1PixQ) != 0:
                                                        pixQIn, pixQOut = r1PixQ.replace(' ','').split(",")
                                                        setenv("R1INNERPIXQUANT",pixQIn)
                                                        setenv("R1OUTERPIXQUANT",pixQOut)
                                                        jobName += nameAdd("R1IPixQ", pixQIn)
                                                        jobName += nameAdd("R1OPixQ", pixQOut)
                                                    if len(r2PixQ) != 0:
                                                        pixQIn, pixQOut = r2PixQ.replace(' ','').split(",")
                                                        setenv("R2INNERPIXQUANT",pixQIn)
                                                        setenv("R2OUTERPIXQUANT",pixQOut)
                                                        jobName += nameAdd("R2IPixQ", pixQIn)
                                                        jobName += nameAdd("R2OPixQ", pixQOut)
                                                    if len(r1PixW) != 0:
                                                        jobName += nameAdd("R1BckW", r1PixW)
                                                        setenv("PIXBCKWEIGHTRICH1",r1PixW)
                                                    if len(r2PixW) != 0:
                                                        jobName += nameAdd("R2BckW" ,r2PixW)
                                                        setenv("PIXBCKWEIGHTRICH2",r2PixW)
                                                    if len(PixWin) != 0:
                                                        jobName += nameAdd("PixTW", PixWin)
                                                        setenv("PIXWIN",PixWin)
                                                    if len(PhotWinR1) != 0:
                                                        inW, outW = PhotWinR1.replace(' ','').split(",")
                                                        setenv("R1INNERPHOTWIN",inW)
                                                        setenv("R1OUTERPHOTWIN",outW)
                                                        jobName += nameAdd("R1IPhoTW", inW)
                                                        jobName += nameAdd("R1OPhoTW", outW)
                                                    if len(PhotWinR2) != 0:
                                                        inW, outW = PhotWinR2.replace(' ','').split(",")
                                                        setenv("R2INNERPHOTWIN",inW)
                                                        setenv("R2OUTERPHOTWIN",outW)
                                                        jobName += nameAdd("R2IPhoTW", inW)
                                                        jobName += nameAdd("R2OPhoTW", outW)
                                                    if len(r1ResSc) != 0:
                                                        inS, outS = r1ResSc.replace(' ','').split(",")
                                                        setenv("INNERR1RESSCALEF", inS)
                                                        setenv("OUTERR1RESSCALEF", outS)
                                                        jobName += nameAdd("R1ICKResSF", inS)
                                                        jobName += nameAdd("R1OCKResSF", outS)
                                                    if len(r2ResSc) != 0:
                                                        inS, outS = r2ResSc.replace(' ','').split(",")
                                                        setenv("INNERR2RESSCALEF", inS)
                                                        setenv("OUTERR2RESSCALEF", outS)
                                                        jobName += nameAdd("R2ICKResSF", inS)
                                                        jobName += nameAdd("R2OCKResSF", outS)
                                                    if len(r1DetEff) != 0:
                                                        effIn, effOut = r1DetEff.replace(' ','').split(",")
                                                        jobName += nameAdd("R1IEff", effIn)
                                                        jobName += nameAdd("R1OEff", effOut)
                                                        setenv("R1INNERPIXEFF",effIn)
                                                        setenv("R1OUTERPIXEFF",effIn)
                                                    if len(r2DetEff) != 0:
                                                        effIn, effOut = r2DetEff.replace(' ','').split(",")
                                                        jobName += nameAdd("R2IEff", effIn)
                                                        jobName += nameAdd("R2OEff", effOut)
                                                        setenv("R2INNERPIXEFF",effIn)
                                                        setenv("R2OUTERPIXEFF",effIn)

                                                    print("Submitting jobs for", jobName)
                                                    subprocess.run([
                                                        optsroot + "/submit.sh", jobName,
                                                        nFiles(float(Lumi)),
                                                        nFilesPerJob(float(Lumi))
                                                    ])

                                                    unsetenv()

                                    else:

                                        setenv("LUMI", Lumi)

                                        jobName = name + "/3D/lumi-" + Lumi
                                        if len(r1PixQ) != 0:
                                            pixQIn, pixQOut = r1PixQ.replace(' ','').split(",")
                                            setenv("R1INNERPIXQUANT",pixQIn)
                                            setenv("R1OUTERPIXQUANT",pixQOut)
                                            jobName += nameAdd("R1IPixQ", pixQIn)
                                            jobName += nameAdd("R1OPixQ", pixQOut)
                                        if len(r2PixQ) != 0:
                                            pixQIn, pixQOut = r2PixQ.replace(' ','').split(",")
                                            setenv("R2INNERPIXQUANT",pixQIn)
                                            setenv("R2OUTERPIXQUANT",pixQOut)
                                            jobName += nameAdd("R2IPixQ", pixQIn)
                                            jobName += nameAdd("R2OPixQ", pixQOut)
                                        if len(r1PixW) != 0:
                                            jobName += nameAdd("R1BckW", r1PixW)
                                            setenv("PIXBCKWEIGHTRICH1",r1PixW)
                                        if len(r2PixW) != 0:
                                            jobName += nameAdd("R2BckW", r2PixW)
                                            setenv("PIXBCKWEIGHTRICH2",r2PixW)
                                        if len(r1ResSc) != 0:
                                            inS, outS = r1ResSc.replace(' ','').split(",")
                                            setenv("INNERR1RESSCALEF", inS)
                                            setenv("OUTERR1RESSCALEF", outS)
                                            jobName += nameAdd("R1ICKResSF", inS)
                                            jobName += nameAdd("R1OCKResSF", outS)
                                        if len(r2ResSc) != 0:
                                            inS, outS = r2ResSc.replace(' ','').split(",")
                                            setenv("INNERR2RESSCALEF", inS)
                                            setenv("OUTERR2RESSCALEF", outS)
                                            jobName += nameAdd("R2ICKResSF", inS)
                                            jobName += nameAdd("R2OCKResSF", outS)
                                        if len(r1DetEff) != 0:
                                            effIn, effOut = r1DetEff.replace(' ','').split(",")
                                            jobName += nameAdd("R1IEff", effIn)
                                            jobName += nameAdd("R1OEff", effOut)
                                            setenv("R1INNERPIXEFF",effIn)
                                            setenv("R1OUTERPIXEFF",effIn)
                                        if len(r2DetEff) != 0:
                                            effIn, effOut = r2DetEff.replace(' ','').split(",")
                                            jobName += nameAdd("R2IEff", effIn)
                                            jobName += nameAdd("R2OEff", effOut)
                                            setenv("R2INNERPIXEFF",effIn)
                                            setenv("R2OUTERPIXEFF",effIn)

                                        print("Submitting jobs for", jobName)
                                        subprocess.run([
                                            optsroot + "/submit.sh", jobName,
                                            nFiles(float(Lumi)),
                                            nFilesPerJob(float(Lumi))
                                        ])

                                        unsetenv()
