/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <array>
#include <limits>
#include <tuple>

// Gaudi Kernel
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Event Model
#include "RichFutureRecEvent/RichRecPhotonYields.h"

// Utils
#include "RichUtils/FastMaths.h"
#include "RichUtils/RichTrackSegment.h"

// Rich Detector
#include "RichDetectors/Rich1.h"

namespace Rich::Future::Rec {

  namespace {

    /// Output data type
    using OutData = std::tuple<PhotonYields::Vector, PhotonSpectra::Vector>;

    // init value to prevent accidental usage..
    inline constexpr auto NaN = std::numeric_limits<double>::signaling_NaN();

    // Data Cache
    class EmitYieldCache {

    public:
      /// Cache the 'paraW' calculation for each energy bin and radiator
      RadiatorArray<std::array<double, NPhotonSpectraBins>> paraWDiff = {{{NaN}}};
      /// Min photon energies for each radiator
      RadiatorArray<float> minPhotEnergy = {{NaN}};
      /// Max photon energies for each radiator
      RadiatorArray<float> maxPhotEnergy = {{NaN}};

    public:
      /// Constructor from RICH1 and min/max photon energies
      EmitYieldCache( const Detector::Rich1&     rich1,     //
                      const RadiatorArray<float> minPhotEn, //
                      const RadiatorArray<float> maxPhotEn )
          : minPhotEnergy( minPhotEn ), maxPhotEnergy( maxPhotEn ) {

        // Load radiator sellmeir parameters
        const RadiatorArray<double> selF1        = {2.653, rich1.param<double>( "SellC4F10F1Param" ),
                                             rich1.param<double>( "SellCF4F1Param" )};
        const RadiatorArray<double> selF2        = {13.075, rich1.param<double>( "SellC4F10F2Param" ),
                                             rich1.param<double>( "SellCF4F2Param" )};
        const RadiatorArray<double> selE1        = {10.666, rich1.param<double>( "SellC4F10E1Param" ),
                                             rich1.param<double>( "SellCF4E1Param" )};
        const RadiatorArray<double> selE2        = {18.125, rich1.param<double>( "SellC4F10E2Param" ),
                                             rich1.param<double>( "SellCF4E2Param" )};
        const RadiatorArray<double> molW         = {0, rich1.param<double>( "GasMolWeightC4F10Param" ),
                                            rich1.param<double>( "GasMolWeightCF4Param" )};
        const RadiatorArray<double> rho          = {0, rich1.param<double>( "RhoEffectiveSellC4F10Param" ),
                                           rich1.param<double>( "RhoEffectiveSellCF4Param" )};
        const auto                  selLorGasFac = rich1.param<double>( "SellLorGasFacParam" );

        // data caches
        RadiatorArray<double> RXSPscale = {{}};
        RadiatorArray<double> RXSMscale = {{}};
        RadiatorArray<double> REP       = {{}};
        RadiatorArray<double> REM       = {{}};
        RadiatorArray<double> X         = {{}};

        // Initialise the calculations and cache as much as possible for efficiency
        for ( const auto rad : Rich::radiators() ) {
          const bool isAero = ( Rich::Aerogel == rad );
          const auto RC     = ( isAero ? 1.0 : selLorGasFac * rho[rad] / molW[rad] );
          const auto RF     = selF1[rad] + selF2[rad];
          const auto RE02   = ( selF1[rad] * selE2[rad] * selE2[rad] + selF2[rad] * selE1[rad] * selE1[rad] ) / RF;
          const auto RE     = ( selE2[rad] * selE2[rad] + selE1[rad] * selE1[rad] ) / RF;
          const auto RG     = ( selE1[rad] * selE1[rad] * selE2[rad] * selE2[rad] ) / ( RF * RE02 );
          const auto RH     = RE02 / RF;
          const auto RM     = RE + ( 2.0 * RC );
          const auto RS     = RG + ( 2.0 * RC );
          const auto RT     = std::sqrt( 0.25 * RM * RM - RH * RS );
          const auto RXSP   = std::sqrt( ( RM / 2.0 + RT ) / RH );
          const auto RXSM   = std::sqrt( ( RM / 2.0 - RT ) / RH );
          REP[rad]          = std::sqrt( RE02 ) * RXSP;
          REM[rad]          = std::sqrt( RE02 ) * RXSM;
          RXSPscale[rad]    = ( RXSP - 1.0 / RXSP );
          RXSMscale[rad]    = ( RXSM - 1.0 / RXSM );
          X[rad]            = ( 3.0 * RC * std::sqrt( RE02 ) / ( 4.0 * RT ) );
        }

        // Cache the paraW diff calculation for each energy bin

        // lambda function for the paraW calculation
        auto paraW = ( [&RXSPscale, &RXSMscale, &REP, &REM, &X]( const Rich::RadiatorType rad, const double energy ) {
          const auto A = ( RXSPscale[rad] * Rich::Maths::fast_log( ( REP[rad] + energy ) / ( REP[rad] - energy ) ) );
          const auto B = ( RXSMscale[rad] * Rich::Maths::fast_log( ( REM[rad] + energy ) / ( REM[rad] - energy ) ) );
          return X[rad] * ( A - B );
        } );

        // Loop over radiators
        for ( const auto rad : Rich::radiators() ) {

          // temporary photon spectra object
          PhotonSpectra spectra( minPhotEn[rad], maxPhotEn[rad] );

          // loop over the energy bins
          for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin ) {
            // cache the paraW(rad,topEn) - paraW(rad,botEn) factor
            const auto botEn           = spectra.binEnergyLowerEdge( iEnBin );
            const auto topEn           = spectra.binEnergyUpperEdge( iEnBin );
            ( paraWDiff[rad] )[iEnBin] = paraW( rad, topEn ) - paraW( rad, botEn );
          }
        }
      }
    };

  } // namespace

  /** @class EmittedPhotonYields
   *
   *  Computes the emitted photon yield data from Track Segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class EmittedPhotonYields final
      : public LHCb::Algorithm::MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&, //
                                                          const EmitYieldCache& ),
                                                 LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, EmitYieldCache>> {

  public:
    /// Standard constructor
    EmittedPhotonYields( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // data inputs
                            {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                             // conditions input
                             KeyValue{"DataCache", DeRichLocations::derivedCondition( name + "-DataCache" )}},
                            // data outputs
                            {KeyValue{"EmittedPhotonYieldLocation", PhotonYieldsLocation::Emitted},
                             KeyValue{"EmittedPhotonSpectraLocation", PhotonSpectraLocation::Emitted}} ) {
      // setProperty( "OutputLevel", MSG::VERBOSE );
    }

    /// Initialization after creation
    StatusCode initialize() override {
      // Sets up various tools and services
      return MultiTransformer::initialize().andThen( [&] {
        // Retrieve square of particle masses
        for ( const auto pid : Rich::particles() ) { m_particleMassSq[pid] = richPartProps()->massSq( pid ); }
        // derived data cache
        Detector::Rich1::addConditionDerivation( this );
        addConditionDerivation( {Detector::Rich1::DefaultConditionKey},          //
                                inputLocation<EmitYieldCache>(),                 //
                                [minPhotEn = richPartProps()->minPhotonEnergy(), //
                                 maxPhotEn = richPartProps()->maxPhotonEnergy()] //
                                ( const Detector::Rich1& rich1 ) {
                                  return EmitYieldCache{rich1, minPhotEn, maxPhotEn};
                                } );
      } );
    }

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments, //
                        const EmitYieldCache&                 dataCache ) const override;

  private:
    /// particle hypothesis masses squared
    ParticleArray<double> m_particleMassSq = {{NaN}};
  };

} // namespace Rich::Future::Rec

// All code is in general Rich reconstruction namespace
using namespace Rich::Future::Rec;

//=============================================================================

OutData EmittedPhotonYields::operator()( const LHCb::RichTrackSegment::Vector& segments, //
                                         const EmitYieldCache&                 dataCache ) const {

  // make the data to return
  OutData data;
  auto&   yieldV   = std::get<PhotonYields::Vector>( data );
  auto&   spectraV = std::get<PhotonSpectra::Vector>( data );

  // reserve sizes
  yieldV.reserve( segments.size() );
  spectraV.reserve( segments.size() );

  // Loop over segments
  for ( const auto& segment : segments ) {

    // Which radiator
    const auto rad = segment.radiator();

    // Create the emitted photon spectra
    auto& spectra = spectraV.emplace_back( dataCache.minPhotEnergy[rad], //
                                           dataCache.maxPhotEnergy[rad] );

    // create the yield data
    auto& yields = yieldV.emplace_back();

    // Some parameters of the segment
    const auto momentum2 = segment.bestMomentum().Mag2();
    const auto length    = segment.pathLength();

    // energy bin size
    const auto enBinSize = spectra.binSize();

    // Loop over (real) PID types. Below threshold excluded.
    for ( const auto id : activeParticlesNoBT() ) {
      // the signal value
      PhotonYields::Type signal = 0;

      // Compute some segment ID dependent parameters
      const auto Esq         = momentum2 + m_particleMassSq[id];
      const auto invBetaSqA  = 37.0 * ( momentum2 > 0 ? Esq / momentum2 : 0 );
      const auto invGammaSqE = ( Esq > 0 ? m_particleMassSq[id] / Esq : 0 ) * enBinSize;

      //_ri_verbo << std::setprecision(9)
      //          << id << " Esq " << Esq << " invBetaSqA " << invBetaSqA
      //          << " invGammaSqE " << invGammaSqE << endmsg;

      // loop over the energy bins
      for ( unsigned int iEnBin = 0; iEnBin < spectra.energyBins(); ++iEnBin ) {

        // Compute number of photons
        auto nPhot = invBetaSqA * ( ( dataCache.paraWDiff[rad] )[iEnBin] - invGammaSqE );

        // bin signal
        const auto binSignal = length * ( nPhot > 0 ? nPhot : 0.0 );
        //_ri_verbo << std::setprecision(9)
        //          << " bin " << iEnBin << " nPhot " << nPhot
        //          << " binSignal " << binSignal << endmsg;

        // save in the spectra object
        ( spectra.energyDist( id ) )[iEnBin] = binSignal;

        // Add to the total signal
        signal += binSignal;

      } // energy bins

      // save the yield for this hypo
      //_ri_verbo << std::setprecision(9) << "final signal " << signal << endmsg;
      yields.setData( id, signal );
    }
  }

  return data;
}

//=============================================================================

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EmittedPhotonYields )

//=============================================================================
