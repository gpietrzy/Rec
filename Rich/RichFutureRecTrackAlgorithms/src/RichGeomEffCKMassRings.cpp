/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// STL
#include <array>
#include <tuple>

// Gaudi
#include "GaudiKernel/PhysicalConstants.h"
#include "LHCbAlgs/Transformer.h"

// Base class
#include "RichFutureRecBase/RichRecAlgBase.h"

// Kernel
#include "Kernel/RichDetectorType.h"

// Event Model
#include "RichFutureRecEvent/RichRecCherenkovAngles.h"
#include "RichFutureRecEvent/RichRecGeomEfficiencies.h"
#include "RichFutureRecEvent/RichRecMassHypoRings.h"

// Utils
#include "RichUtils/RichTrackSegment.h"
#include "RichUtils/ZipRange.h"

namespace Rich::Future::Rec {

  namespace {
    /// The output data
    using OutData = std::tuple<GeomEffs::Vector, GeomEffsPerPDVector, SegmentPhotonFlags::Vector>;
  } // namespace

  /** @class GeomEffCKMassRings RichRichGeomEffCKMassRings.h
   *
   *  Computes the geometrical efficiencies for a set of track segments.
   *
   *  @author Chris Jones
   *  @date   2016-09-30
   */
  class GeomEffCKMassRings final
      : public LHCb::Algorithm::MultiTransformer<OutData( const LHCb::RichTrackSegment::Vector&, //
                                                          const CherenkovAngles::Vector&,        //
                                                          const MassHypoRingsVector& ),          //
                                                 Gaudi::Functional::Traits::BaseClass_t<AlgBase<>>> {

  public:
    /// Standard constructor
    GeomEffCKMassRings( const std::string& name, ISvcLocator* pSvcLocator )
        : MultiTransformer( name, pSvcLocator,
                            // inputs
                            {KeyValue{"TrackSegmentsLocation", LHCb::RichTrackSegmentLocation::Default},
                             KeyValue{"CherenkovAnglesLocation", CherenkovAnglesLocation::Emitted},
                             KeyValue{"MassHypothesisRingsLocation", MassHypoRingsLocation::Emitted}},
                            // outputs
                            {KeyValue{"GeomEffsLocation", GeomEffsLocation::Default},
                             KeyValue{"GeomEffsPerPDLocation", GeomEffsPerPDLocation::Default},
                             KeyValue{"SegmentPhotonFlagsLocation", SegmentPhotonFlagsLocation::Default}} ) {}

  public:
    /// Algorithm execution via transform
    OutData operator()( const LHCb::RichTrackSegment::Vector& segments, //
                        const CherenkovAngles::Vector&        ckAngles, //
                        const MassHypoRingsVector&            massRings ) const override {

      // Scalar type
      using ScType = GeomEffs::Type;

      // make the data to return
      OutData data;
      auto&   geomEffsV      = std::get<GeomEffs::Vector>( data );
      auto&   geomEffsPerPDV = std::get<GeomEffsPerPDVector>( data );
      auto&   segPhotFlags   = std::get<SegmentPhotonFlags::Vector>( data );

      // reserve sizes
      geomEffsV.reserve( ckAngles.size() );
      geomEffsPerPDV.reserve( ckAngles.size() );
      segPhotFlags.reserve( ckAngles.size() );

      // iterate over input data
      for ( auto&& [segment, ckAngles, massRings] : Ranges::ConstZip( segments, ckAngles, massRings ) ) {

        // make new objects for the geom effs
        auto& geomEffs      = geomEffsV.emplace_back();
        auto& geomEffsPerPD = geomEffsPerPDV.emplace_back();

        // make entry for segment photon flags
        auto& photFlags = segPhotFlags.emplace_back();

        // Which rich
        const auto rich = segment.rich();

        // Loop over (real) PID types (Below Threshold excluded).
        for ( const auto id : activeParticlesNoBT() ) {

          // The efficiency
          ScType eff = 0;

          // above threshold ?
          if ( ckAngles[id] > 0 ) {

            // Is the mass ring an alias to another lighter one ?
            const auto aliasID = massRings.massAlias( id );
            if ( aliasID < id ) {

              // Copy overall eff. from alias ID
              eff = geomEffs[aliasID];

              // set alias for Geom. Per PD data
              geomEffsPerPD.setMassAlias( id, aliasID );

            } else {

              if ( !massRings[id].empty() ) {
                // make new geom. eff. entry

                // count the number of detected points
                std::size_t nDetect = 0;

                // number of points
                const auto nPoints = massRings[id].size();

                // PD increment ( 1 / number points )
                const auto pdInc = 1.0f / static_cast<ScType>( nPoints );

                // data for this ID
                auto& idData = geomEffsPerPD[id];

                // Last PD filled
                LHCb::RichSmartID lastPDID;

                // Loop over the points of the mass ring
                for ( const auto& P : massRings[id] ) {

                  // count points in PD acceptance
                  if ( RayTracedCKRingPoint::InHPDTube == P.acceptance() ) {

                    // count detected photons
                    ++nDetect;

                    // The HPD ID
                    const auto pdID = P.smartID().pdID();

                    // Same PD as last time ?
                    if ( lastPDID == pdID ) {
                      // just increment the back() entry
                      idData.back().eff += pdInc;
                    } else {
                      // No, so add a new entry
                      idData.emplace_back( pdID, pdInc );
                      // update cached last PD ID
                      lastPDID = pdID;
                    }

                    // Segment photon flags
                    photFlags.setInAcc( rich, P.globalPosition() );
                  } // acceptance
                }

                // compute the final eff
                eff = static_cast<ScType>( nDetect ) * pdInc;

              } // ring not empty

            } // make new data object

          } // CK theta > 0

          // save the final eff
          geomEffs.setData( id, eff );

        } // loop over hypos

      } // loop over segment data

      // return the new data
      return data;
    }
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( GeomEffCKMassRings )

} // namespace Rich::Future::Rec

//=============================================================================
