###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Rich/RichRecUtils
-----------------
#]=======================================================================]

gaudi_add_library(RichRecUtils
    SOURCES
        src/RichCKResolutionFitter.cpp
        src/RichPhotonSpectra.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::RichFutureUtils
            LHCb::RichUtils
            ROOT::Hist
        PRIVATE
            ROOT::MathCore
)

gaudi_add_dictionary(RichRecUtilsDict
    HEADERFILES dict/RichRecUtilsDict.h
    SELECTION dict/RichRecUtilsDict.xml
    LINK RichRecUtils
    OPTIONS "-DBOOST_DISABLE_ASSERTS" ${REC_DICT_GEN_DEFAULT_OPTS}
)
