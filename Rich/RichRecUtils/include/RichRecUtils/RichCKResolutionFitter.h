/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// ROOT includes
#include <TF1.h>
#include <TH1.h>

// LHCb Kernel
#include "Kernel/RichRadiatorType.h"

// Format
#include <fmt/core.h>

// STL
#include <array>
#include <cstdint>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace Gaudi {
  class Algorithm;
}

namespace Rich::Rec {

  namespace details {
    template <typename... Args>
    inline auto buildStr( Args&&... args ) {
      std::ostringstream stream;
      try {
        ( stream << ... << args );
      } catch ( const std::exception& excpt ) {
        const auto mess =
            fmt::format( "EXCEPTION building string: current='{}' what='{}'", stream.str(), excpt.what() );
        std::cerr << mess << std::endl;
        throw std::runtime_error( mess );
      }
      return stream.str();
    }
  } // namespace details

  /** @class CKResolutionFitter RichRecUtils/RichCKResolutionFitter.h
   *
   *  Utility class to perform a fit to the Cherenkov resolution plots.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2017-08-02
   */
  class CKResolutionFitter final {

  public:
    /// Fitter Parameters
    class FitParams final {

    private:
      /// Number of Radiators
      static const std::uint8_t NRads = 2;

    public:
      // General fitting parameters

      /// Type for list for fit types for each radiator
      using RichFitTypes_t = std::array<std::vector<std::string>, NRads>;
      /// Type for Radiator double array
      using RichRadDouble_t = std::array<double, NRads>;

      //                              RICH1     RICH2
      /// Starting resolution guesses
      RichRadDouble_t RichStartRes{{0.00105, 0.00060}};
      /// Starting prefit deltas
      RichRadDouble_t RichStartDelta{{0.0020, 0.0009}};
      /// Fit Range Minimums
      RichRadDouble_t RichFitMin{{-0.0068, -0.00395}};
      /// Fit range Maxmimums
      RichRadDouble_t RichFitMax{{0.0068, 0.00395}};
      /// Default Gauss Asymmetry
      RichRadDouble_t RichStartAsym{{0.15, 0.22}};
      /// Asymmetry limits
      RichRadDouble_t RichAsymLimit{{0.40, 0.40}};
      /// Fit Type(s)
      RichFitTypes_t RichFitTypes{{{"AsymNormal:FreeNPol"}, {"AsymNormal:FreeNPol"}}};

    public:
      // Free polynominal background fit options

      /// Polynominal degree
      std::array<unsigned int, NRads> RichNPol{{3, 3}};

      /// Starting poly degree
      std::array<unsigned int, NRads> RichStartPol{{1, 1}};

    private:
      // polynominal function
      [[nodiscard]] inline auto Pol( const int          degree, //
                                     const unsigned int iFirstParam = 0 ) const {
        return [iFP = iFirstParam, deg = degree]( const double* x, const double* p ) {
          auto R = p[iFP + deg];
          for ( auto i = deg - 1; i >= 0; --i ) { R = ( R * x[0] ) + p[iFP + i]; };
          return R;
        };
      }

      // Asymmetric Normal function
      [[nodiscard]] inline auto AsymNormal( const unsigned int iFirstParam = 0 ) const {
        return [iFP = iFirstParam]( const double* x, const double* p ) {
          const auto N = p[iFP + 0];
          const auto m = p[iFP + 1];
          const auto s = fabs( p[iFP + 2] );
          const auto a = p[iFP + 3];
          // Compute the peak position of the function as with asymmetry,
          // the fit parameter (mean) is not exactly the same as the peak position.
          // Factor of 0.675 relates to the 50% percentile for the Normal distribution.
          const auto dx = x[0] - m + ( 0.675 * s * a );
          // create the asymmetric sigma, depending on which side of the centre ptn we are
          const auto as = s * ( 1.0 + ( ( dx > 0 ? 0.5 : -0.5 ) * a ) );
          return ( as > 0 ? N * std::exp( -0.5 * std::pow( dx / as, 2 ) ) : 0 );
        };
      }

    public: // Hyperbolic functions
      //                                     RICH1    RICH2
      /// Side band sizes
      std::array<double, NRads> SideBandSize{{0.002, 0.001}};
      /// Init values for parameter B
      std::array<double, NRads> HyperInitB{{1e8, 1e8}};
      /// Init values for parameter D
      std::array<double, NRads> HyperInitD{{7e-4, 5e-4}};

      /// lambda func for pol background
      [[nodiscard]] inline auto PolFitFunc( const int degree ) const { return Pol( degree ); }

      /// lambda function for asym normal signal
      [[nodiscard]] inline auto AsymNormalFitFunc( const unsigned int iFirstParam = 0 ) const {
        return AsymNormal( iFirstParam );
      }

      /// lambda function for signal + background
      [[nodiscard]] inline auto AsymNormalPolFitFunc( const int degree, const unsigned int iFirstParam = 0 ) const {
        return [normal = AsymNormal( iFirstParam ), //
                pol    = Pol( degree, iFirstParam + 4 )]( const double* x, const double* p ) {
          return pol( x, p ) + normal( x, p );
        };
      }

    public: // Fit Quality options
      /// Maximum abs fitted mean for OK fit
      std::array<double, NRads> RichMaxMean{{0.0040, 0.0020}};
      /// Minimum abs fitted sigma for OK fit
      std::array<double, NRads> RichMinSigma{{0.0003, 0.0001}};
      /// Maximum abs fitted sigma for OK fit
      std::array<double, NRads> RichMaxSigma{{0.0030, 0.0020}};
      /// Maximum shift error for OK fit
      std::array<double, NRads> MaxShiftError{{0.0010, 0.0010}};
      /// Maximum resolution error for OK fit
      std::array<double, NRads> MaxSigmaError{{0.0002, 0.0001}};
    };

    /// Class to store the result of a histogram fit
    class FitResult final {
    public:
      /// Overall fit status
      bool fitOK{false};
      /// Number of attempts before a successful fit
      unsigned int fitAttempts{0};
      /// CK shift
      double ckShift{0};
      /// CK shift error
      double ckShiftErr{9999};
      /// CK resolution
      double ckResolution{0};
      /// CK resolution
      double ckResolutionErr{9999};
      /// The minimum of the used fit range
      double fitMin{0};
      /// The maximum of the used fit range
      double fitMax{0};
      /// The overall fitted function
      std::shared_ptr<TF1> overallFitFunc;
      /// The fitted background function
      std::shared_ptr<TF1> bkgFitFunc;
      /// The fitted signal function
      std::shared_ptr<TF1> signalFitFunc;
    };

  public:
    /// Default Constructor
    CKResolutionFitter( const Gaudi::Algorithm* parent = nullptr ) : m_parent( parent ) {}

    /// Constructor with a given set of parameters
    CKResolutionFitter( FitParams prms, const Gaudi::Algorithm* parent = nullptr )
        : m_params( std::move( prms ) ), m_parent( parent ) {}

  public:
    /// Perform a fit to the given TH1 histogram
    [[nodiscard]] FitResult fit( TH1& hist, const Rich::RadiatorType rad ) const;

    /// Write access to the fit parameter object
    [[nodiscard]] FitParams& params() noexcept { return m_params; }

    /// Read access to the fit parameter object
    [[nodiscard]] const FitParams& params() const noexcept { return m_params; }

  private:
    // generate TF1 for prefit
    std::shared_ptr<TF1> getPrefitFunc( const Rich::RadiatorType rad,  //
                                        const double             xMin, //
                                        const double             xMax, //
                                        const unsigned int       recursiveCount ) const;

    // generate TF1 for full fit
    std::shared_ptr<TF1> getFitFunc( const Rich::RadiatorType rad,        //
                                     const unsigned int       nSigParams, //
                                     const unsigned int       nPol,       //
                                     const double             xMin,       //
                                     const double             xMax,       //
                                     const unsigned int       recursiveCount ) const;

    // Run a given fit type
    [[nodiscard]] FitResult fitImp( TH1&                     hist,    //
                                    const Rich::RadiatorType rad,     //
                                    const std::string&       fitType, //
                                    const unsigned int       recursiveCount = 0 ) const;

    /// Check histogram fit result
    template <typename FUNC>
    [[nodiscard]] inline bool fitIsOK( const FUNC& fFitF, const Rich::RadiatorType rad ) const {
      // which RICH
      const auto rIn = rIndex( rad );
      // first check fit error on shift
      bool fitOK = fFitF->GetParError( 1 ) < params().MaxShiftError[rIn];
      // now test the resolution error
      fitOK &= fFitF->GetParError( 2 ) < params().MaxSigmaError[rIn];
      // Final sanity check on the fit results to detect complete failures
      // such as when there is no Cherenkov signal at all
      const auto ckMean = fFitF->GetParameter( 1 );
      const auto ckRes  = fFitF->GetParameter( 2 );
      fitOK &= fabs( ckMean ) < params().RichMaxMean[rIn];
      fitOK &= fabs( ckRes ) > params().RichMinSigma[rIn];
      fitOK &= fabs( ckRes ) < params().RichMaxSigma[rIn];
      // return the final status
      return fitOK;
    }

    /// check fit status
    template <typename RESULT>
    [[nodiscard]] inline bool fitIsValid( const RESULT& fitR ) const {
      return ( fitR.Get() ? fitR->IsValid() : false );
    }

    /// Get the RICH array index from enum
    [[nodiscard]] inline std::uint8_t rIndex( const Rich::RadiatorType rad ) const noexcept {
      return ( Rich::Rich1Gas == rad ? 0 : 1 );
    }

  private:
    /// Define the messenger entity
    inline auto messenger() const noexcept { return m_parent; }

  private:
    /// Parameters
    FitParams m_params;
    /// Pointer back to parent algorithm (for messaging)
    const Gaudi::Algorithm* m_parent{nullptr};
    /// cache of fit functions
    mutable std::unordered_map<std::string, std::shared_ptr<TF1>> m_fitfuncs;
  };

} // namespace Rich::Rec
