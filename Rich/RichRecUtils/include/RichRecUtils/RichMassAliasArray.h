/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// STL
#include <cassert>
#include <cstdint>

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Kernel
#include "Kernel/RichParticleIDType.h"

namespace Rich::Future::Rec {

  /// Templated class implementing a data array with the concept of mass aliases.
  template <typename TYPE>
  class MassAliasArray final {

  private:
    /// The payload data
    using Payload = TYPE;
    /// Payload array type
    using Array = ParticleArray<TYPE>;
    /// Size type
    using size_type = typename Array::size_type;
    /// Index type
    using Index = std::int8_t;
    /// Indices array
    using Indices = ParticleArray<Index>;

  private:
    /// Internal mapping of which indices to use for which particle type
    Indices m_indices{0, 1, 2, 3, 4, 5, 6};
    /// The data array
    Array m_data{{}};

  public:
    /// Set the alias to use for a given mass hypo
    void setMassAlias( const ParticleIDType from, const ParticleIDType to ) {
      assert( from != Rich::Unknown && (Indices::size_type)from < m_indices.size() );
      assert( to != Rich::Unknown && (Indices::size_type)to < m_indices.size() );
      m_indices[from] = to;
    }
    /// Get the mass alias
    decltype( auto ) massAlias( const ParticleIDType from ) const noexcept { return ParticleIDType{m_indices[from]}; }

  public:
    // access operators
    const Payload& operator[]( const size_type i ) const noexcept { return m_data[m_indices[i]]; }
    Payload&       operator[]( const size_type i ) noexcept { return m_data[m_indices[i]]; }
    const Payload& at( const size_type i ) const {
      assert( i < m_indices.size() );
      return m_data.at( m_indices[i] );
    }
    Payload& at( const size_type i ) {
      assert( i < m_indices.size() );
      return m_data.at( m_indices[i] );
    }

  public:
    // messaging

    /// Implement textual ostream << method for MassAliasArray
    friend inline std::ostream& operator<<( std::ostream& os, const MassAliasArray& array ) {
      os << "{";
      for ( const auto id : Rich::particles() ) {
        if ( id != Rich::particles().front() ) { os << ","; }
        os << " [" << id << "->" << array.massAlias( id ) << "]=" << array[id];
      }
      return os << " }";
    }
  };

} // namespace Rich::Future::Rec
