/*****************************************************************************\
* (c) Copyright 2000-2024 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "RichRecUtils/RichCKResolutionFitter.h"

// ROOT
#include <TFitResult.h>
#include <TMath.h>

// boost
#include "boost/algorithm/string.hpp"

// Gaudi
#include "Gaudi/Algorithm.h"

// Messaging
#include "RichFutureUtils/RichMessaging.h"
#define debug( ... )                                                                                                   \
  if ( messenger() ) { ri_debug( "CKResolutionFitter: ", __VA_ARGS__ ); }
#define verbo( ... )                                                                                                   \
  if ( messenger() ) { ri_verbo( "CKResolutionFitter: ", __VA_ARGS__ ); }
#define warning( ... )                                                                                                 \
  if ( messenger() ) { ri_warning( "CKResolutionFitter: ", __VA_ARGS__ ); }
#define error( ... )                                                                                                   \
  if ( messenger() ) { ri_error( "CKResolutionFitter: ", __VA_ARGS__ ); }
#define info( ... )                                                                                                    \
  if ( messenger() ) { ri_info( "CKResolutionFitter: ", __VA_ARGS__ ); }

#include <utility>

#include <fmt/format.h>
#include <string_view>

// fmt::format specialization for TString
template <>
struct fmt::formatter<TString> : formatter<std::string_view> {
  auto format( const TString& s, format_context& ctx ) const {
    return formatter<std::string_view>::format( std::string_view{s}, ctx );
  }
};

using namespace Rich::Rec;

//=============================================================================

CKResolutionFitter::FitResult CKResolutionFitter::fit( TH1& hist, const Rich::RadiatorType rad ) const {
  // which RICH ?
  const auto rIn = rIndex( rad );
  // run the fits in order, returning the first to succeed.
  for ( const auto& fitType : params().RichFitTypes[rIn] ) {
    const auto fitR = fitImp( hist, rad, fitType );
    if ( fitR.fitOK ) { return fitR; }
  }
  // return default if get here. No fit.
  return FitResult{};
}

//=============================================================================

CKResolutionFitter::FitResult CKResolutionFitter::fitImp( TH1&                     hist,    //
                                                          const Rich::RadiatorType rad,     //
                                                          const std::string&       fitType, //
                                                          const unsigned int       recursiveCount ) const {

  // Full fit result object
  FitResult fitRes;

  // shortcuts to fit result data
  auto& fitOK = fitRes.fitOK;

  // used fit range
  auto& fitMin = fitRes.fitMin;
  auto& fitMax = fitRes.fitMax;

  // Set the fit attempt count
  fitRes.fitAttempts = recursiveCount + 1;

  try {

    // which RICH ?
    const auto rIn = rIndex( rad );

    // Extract the signal and background forms from the fit type
    std::vector<std::string> fitOps;
    boost::split( fitOps, fitType, boost::is_any_of( ":" ) );
    // Must have exactly two splits signal:background
    if ( fitOps.size() != 2 ) {
      error( "Unknown fit type ", fitType, endmsg );
      return fitRes;
    }
    const auto sigType = fitOps[0];
    const auto bkgType = fitOps[1];

    // Firstly do a prefit

    // delta for the pre-fit
    const auto delta = params().RichStartDelta[rIn];

    // get the scale of the histogram
    const auto yPeak = hist.GetBinContent( hist.GetMaximumBin() );

    // x value for peak
    auto xPeak = hist.GetBinCenter( hist.GetMaximumBin() );
    // if peak is too far off from expectation, just use 0.0 as starting point
    if ( fabs( xPeak ) > ( delta / 5.0 ) ) { xPeak = 0.0; }

    // range for the pre-fit
    fitMin = xPeak - delta;
    fitMax = xPeak + delta;

    // Do the pre-fit
    auto preFitF = getPrefitFunc( rad, fitMin, fitMax, recursiveCount );
    // Estimate overall scale from max bin
    preFitF->SetParameter( 0, yPeak );
    // Estimate peak from max bin
    preFitF->SetParameter( 1, xPeak );
    // rough guess at resolution. Does not need to be precise
    preFitF->SetParameter( 2, params().RichStartRes[rIn] );
    // run the pre-fit
    try {
      // if ( time( nullptr ) % 3 == 0 ) { throw std::runtime_error( "Test1" ); }
      const auto preFitR = hist.Fit( preFitF.get(), "MQRS0" );
      // Fit result status
      fitOK = fitIsValid( preFitR );
    } catch ( const std::exception& excpt ) {
      const auto mess = fmt::format( "exception during pre-fit {} '{}'", preFitF->GetName(), excpt.what() );
      throw std::runtime_error( mess );
    }

    // Only proceed if prefit is OK
    if ( fitOK ) {
      // Fit above tends to result in too large resolutions, due to no background contribution
      // So just reset these back to starting guesses which are likely closer to the truth
      preFitF->SetParameter( 2, params().RichStartRes[rIn] );

      debug( rad, " pre-fit OK", endmsg );
      for ( unsigned int i = 0; i < 3; ++i ) {
        debug( " -> Parameter '", preFitF->GetParName( i ), "' = ", preFitF->GetParameter( i ), endmsg );
      }
      debug( "Starting ", rad, " SigType=", sigType, " BkgType=", bkgType, " fit", endmsg );

      // The final fit function.
      auto& bestFitF = fitRes.overallFitFunc;
      // Set at first to the pre fit.
      bestFitF = preFitF;
      // the signal/bkg only function
      auto& sigFunc = fitRes.signalFitFunc;
      auto& bkgFunc = fitRes.bkgFitFunc;

      // Range for the final full fit, with background etc.
      fitMin = params().RichFitMin[rIn];
      fitMax = params().RichFitMax[rIn];

      // Number of signal parameters
      const bool         isAsym     = "AsymNormal" == sigType;
      const unsigned int nSigParams = 4; // all forms have 4 parms (1 sometimes fixed).

      // Fit Type
      if ( "FreeNPol" == bkgType ) {
        // Full free polynominal form for background

        // fit options
        const std::string minuitOpts = "MQRSE0";
        // const std::string minuitOpts = "MRSE0";

        // Polynominal degree for background
        const auto nPolFull = params().RichNPol[rIn];

        // degree to start fits at
        const unsigned int firstNPol = params().RichStartPol[rIn];

        // asym parameter number
        const auto asymParamNum = 3u;

        // full fit parameters
        unsigned int bestNPol = 0;

        // save the last fit function
        auto lastFitF = preFitF;

        // Loop over poly orders
        for ( auto nPol = firstNPol; nPol < nPolFull + 1; ++nPol ) {

          debug( " -> ", rad, " NPol=", nPol, " fit", endmsg );

          // create fit function
          auto fFitF = getFitFunc( rad, nSigParams, nPol, fitMin, fitMax, recursiveCount );

          // set parameters from last fit
          // If first iteration, just 3 Gaussian params otherwise number from last full fit
          const auto nPToSet = ( firstNPol == nPol ? 3 : nSigParams + nPol );
          for ( auto i = 0u; i < nPToSet; ++i ) {
            auto p = lastFitF->GetParameter( i );
            if ( 0 == i && firstNPol == nPol ) {
              // For the first pol fit take half the gaussian height and 'give' it to the
              // first pol parameter (linear term).
              // A (very) rough estimate of the relative signal to background size.
              p *= 0.5;
              const unsigned int polLinearTerm = 4;
              fFitF->SetParameter( polLinearTerm, p );
              debug( "  -> Setting parameter ", polLinearTerm, " '", fFitF->GetParName( polLinearTerm ), "' to ", p,
                     endmsg );
            }
            fFitF->SetParameter( i, p );
            debug( "  -> Setting parameter ", i, " '", fFitF->GetParName( i ), "' to ", p, endmsg );
          }

          // Are we using a fit with assymetric signal shape ?
          if ( !isAsym ) {

            // fix the asymmetry so it cannot float
            fFitF->FixParameter( asymParamNum, isAsym ? params().RichStartAsym[rIn] : 0.0 );
            debug( "  -> Fixed parameter  '", fFitF->GetParName( asymParamNum ), "' to ",
                   fFitF->GetParameter( asymParamNum ), endmsg );

            // the fit with no asymmetry
            try {
              const auto fitR     = hist.Fit( fFitF.get(), minuitOpts.c_str() );
              const auto validFit = fitIsValid( fitR );
              fitOK               = validFit && fitIsOK( fFitF, rad );
            } catch ( const std::exception& excpt ) {
              const auto mess = fmt::format( "exception during no-asym fit {} '{}'", fFitF->GetName(), excpt.what() );
              throw std::runtime_error( std::move( mess ) );
            }

            // save the last fit if OK
            if ( fitOK ) { lastFitF = fFitF; }

          } else {
            // asym fit

            // For first pol fit init the asym parameter
            // Subsequent pol fits take the previous fit value (above).
            if ( firstNPol == nPol ) {
              fFitF->SetParameter( asymParamNum, isAsym ? params().RichStartAsym[rIn] : 0.0 );
              debug( "  -> Init    parameter ", asymParamNum, " '", fFitF->GetParName( asymParamNum ), "' to ",
                     fFitF->GetParameter( asymParamNum ), endmsg );
            }

            // fit lambda func
            auto runFit = [&]() {
              const unsigned int maxTryCount = 20;
              unsigned int       tryCount    = 0;
              fitOK                          = false;
              while ( !fitOK && tryCount <= maxTryCount ) {
                ++tryCount;
                try {
                  verbo( "  -> Starting TH1.Fit ...", endmsg );
                  const auto fitR = hist.Fit( fFitF.get(), minuitOpts.c_str() );
                  verbo( "  -> Testing fit result object ...", endmsg );
                  const auto vFit = fitIsValid( fitR );
                  verbo( "  -> Checking fit values ...", endmsg );
                  fitOK = vFit && fitIsOK( fFitF, rad );
                  if ( fitOK ) { lastFitF = fFitF; }
                  verbo( "   -> fit ", ( fitOK ? "OK" : "NOT OK" ), endmsg );

                } catch ( const std::exception& excpt ) {
                  if ( tryCount >= maxTryCount ) {
                    const auto mess =
                        fmt::format( "exception during asym fit {} '{}'", fFitF->GetName(), excpt.what() );
                    throw std::runtime_error( std::move( mess ) );
                  } else {
                    // just try again ..
                    fitOK = false;
                  }
                }
              }
            };

            // try a fit with the asym unconstrained
            runFit();

            // If not OK try again constrained
            if ( !fitOK && isAsym ) {
              const auto lim = params().RichAsymLimit[rIn];
              debug( "  -> Asym fit failed. Try again with asym parameter constrained to +-", lim, endmsg );
              for ( int i = 0; i < 3; ++i ) { fFitF->SetParameter( i, lastFitF->GetParameter( i ) ); }
              fFitF->SetParameter( 3, isAsym ? params().RichStartAsym[rIn] : 0.0 );
              if ( lim <= 1.0 ) {
                // limit the maximum asymmetry
                fFitF->SetParLimits( 3, -lim, lim );
              }
              runFit();
            }

          } // is asym

          // Save end fit result ?
          if ( fitOK ) {
            bestFitF = fFitF;
            bestNPol = nPol;
          } else {
            if ( nPol == nPolFull ) {
              // use last good fit
              try {
                const auto R = hist.Fit( fFitF.get(), minuitOpts.c_str() ); // CRJ : Why do I redo this fit...
                fitOK        = ( R.Get() ? R->IsValid() : false );
                if ( fitOK ) {
                  debug( "    -> FINAL fit OK", endmsg );
                } else {
                  warning( "    -> FINAL fit FAILED", endmsg );
                }
              } catch ( const std::exception& excpt ) {
                const auto mess =
                    fmt::format( "exception during fallback fit {} '{}'", fFitF->GetName(), excpt.what() );
                throw std::runtime_error( mess );
              }
            }
            if ( nPol > firstNPol ) { break; }
          }

        } // loop over pol fits

        // Setup the background only function
        bkgFunc = std::make_shared<TF1>( "Background", params().PolFitFunc( bestNPol ), fitMin, fitMax, bestNPol + 1 );
        // Setup the signal only function
        sigFunc = std::make_shared<TF1>( "Signal", params().AsymNormalFitFunc( 0 ), fitMin, fitMax, nSigParams );

      } else {
        error( "Unknown background type ", bkgType, endmsg );
      }

      // Set the background function parameters
      for ( int i = nSigParams; i < bestFitF->GetNpar(); ++i ) {
        bkgFunc->SetParameter( i - nSigParams, bestFitF->GetParameter( i ) );
        bkgFunc->SetParError( i - nSigParams, bestFitF->GetParError( i ) );
        bkgFunc->SetParName( i - nSigParams, bestFitF->GetParName( i ) );
      }

      // Set the signal function
      for ( auto i = 0u; i < nSigParams; ++i ) {
        sigFunc->SetParameter( i, bestFitF->GetParameter( i ) );
        sigFunc->SetParError( i, bestFitF->GetParError( i ) );
        sigFunc->SetParName( i, bestFitF->GetParName( i ) );
      }

      // Final sanity check on the fit results
      // limits on fit sigma and mean, to detect complete failures such as when there
      // is no Cherenkov signal at all
      fitOK &= fitIsOK( bestFitF, rad );
      if ( fitOK ) {
        fitRes.ckShift         = bestFitF->GetParameter( 1 );
        fitRes.ckShiftErr      = bestFitF->GetParError( 1 );
        fitRes.ckResolution    = bestFitF->GetParameter( 2 );
        fitRes.ckResolutionErr = bestFitF->GetParError( 2 );
      } else {
        warning( "   -> Final sanity check FAILED !!", endmsg );
      }

    } else {
      warning( rad, " pre-fit FAILED", endmsg );
    }

    // return final fit parameters
    return fitRes;

  } catch ( const std::exception& excpt ) {
    // ROOT can infrequently throw strange exceptions
    // recursively try N times to see if one works...
    if ( recursiveCount < 10 ) {
      return fitImp( hist, rad, fitType, recursiveCount + 1 );
    } else {
      // emit error
      error( excpt.what(), endmsg );
      // just pass on exception
      throw std::runtime_error( excpt.what() );
    }
  }

  // If get here return default result object
  return FitResult{};
}

//=============================================================================

std::shared_ptr<TF1> CKResolutionFitter::getPrefitFunc( const Rich::RadiatorType rad,  //
                                                        const double             xMin, //
                                                        const double             xMax, //
                                                        const unsigned int       recursiveCount ) const {
  // name
  const auto preFitFName = details::buildStr( "Rad", rad, "PreFitF" );
  // is this named func already in the cache ?
  auto pfunc = m_fitfuncs.find( preFitFName );
  // Note we force making a new instance, even if one exists, when this is called with a >0
  // recursive call count, as that means its a refit attempt.
  if ( recursiveCount > 0 || pfunc == m_fitfuncs.end() ) {
    debug( "Creating new TF1 for '", preFitFName, "'", endmsg );
    // make new function
    auto preFitF = std::make_shared<TF1>( preFitFName.c_str(), "gaus", xMin, xMax );
    // parameter names
    preFitF->SetParName( 0, "Gaus Const" );
    preFitF->SetParName( 1, "Gaus Mean" );
    preFitF->SetParName( 2, "Gaus Sigma" );
    // save in map
    m_fitfuncs[preFitFName] = preFitF;
    // return
    return preFitF;
  } else {
    debug( "Reusing existing TF1 for '", preFitFName, "'", endmsg );
    // just use existing function
    auto preFitF = pfunc->second;
    // update fit range, as it could change slightly
    preFitF->SetRange( xMin, xMax );
    // No need to reset parameters here, as anyway all 3 are set to guesstimates
    // above before being used for a fit.
    return preFitF;
  }
}

//=============================================================================

std::shared_ptr<TF1> CKResolutionFitter::getFitFunc( const Rich::RadiatorType rad,        //
                                                     const unsigned int       nSigParams, //
                                                     const unsigned int       nPol,       //
                                                     const double             xMin,       //
                                                     const double             xMax,       //
                                                     const unsigned int       recursiveCount ) const {
  // name
  const auto fFitFName = details::buildStr( "Rad", rad, "fFitF", nPol );
  // is this named func already in the cache ?
  auto pfunc = m_fitfuncs.find( fFitFName );
  // Note we force making a new instance, even if one exists, when this is called with a >0
  // recursive call count, as that means its a refit attempt.
  if ( recursiveCount > 0 || pfunc == m_fitfuncs.end() ) {
    // total number of parameters
    const auto nTotalParams = nSigParams + ( nPol + 1 );
    debug( "  -> Creating new TF1 for '", fFitFName, "' recursiveCount=", recursiveCount, endmsg );
    // build new TF1
    auto fFitF = std::make_shared<TF1>( fFitFName.c_str(), params().AsymNormalPolFitFunc( nPol, 0 ), //
                                        xMin, xMax, nTotalParams );
    // Set the gaussian parameter names
    fFitF->SetParName( 0, "Gaus Const" );
    fFitF->SetParName( 1, "Gaus Mean" );
    fFitF->SetParName( 2, "Gaus Sigma" );
    fFitF->SetParName( 3, "Gaus Asym" );
    // ... and then set the background parameter names
    for ( int i = nSigParams; i < fFitF->GetNpar(); ++i ) {
      const auto pName = details::buildStr( "Bkg Par ", ( i - nSigParams ) );
      fFitF->SetParName( i, pName.c_str() );
    }
    // save in map
    m_fitfuncs[fFitFName] = fFitF;
    // return
    return fFitF;
  } else {
    debug( "  -> Reusing existing TF1 for '", fFitFName, "' recursiveCount=", recursiveCount, endmsg );
    // just use existing function
    auto fFitF = pfunc->second;
    // update fit range, as it could change slightly since TF1 was first made
    fFitF->SetRange( xMin, xMax );
    // Set all parameters back to zero
    for ( int i = 0; i < fFitF->GetNpar(); ++i ) { fFitF->SetParameter( i, 0.0 ); }
    // return
    return fFitF;
  }
}

//=============================================================================
