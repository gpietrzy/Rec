/*****************************************************************************\
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/PrVeloTracks.h"
#include "Event/RecVertex_v2.h"
#include "Event/Track.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/Transformer.h"
#include "LHCbDet/InteractionRegion.h"

#ifdef TIMINGHISTOGRAMMING
#  include "AIDA/IProfile1D.h"
#  include "GaudiKernel/IHistogramSvc.h"
#  include "Timer.h"
#endif

#include "TrackPVFinderUtils.h"

// boost includes
#include <boost/container/static_vector.hpp>

// std includes
#include <array>
#include <limits>
#include <vector>

/**
 * PV finding strategy:
 * step 1: select tracks with velo info and cache some information useful for PV finding
 * step 2: fill a histogram with the z of the poca to the beamline
 * step 3: do a peak search in that histogram ('vertex seeds')
 * step 4: assign tracks to the closest seed ('partitioning')
 * step 5: fit the vertices with an adapative vertex fit
 *
 *  @author Wouter Hulsbergen (Nikhef, 2018)
 **/
namespace {
  using namespace LHCb::TrackPVFinder;

  // c++20's remove_cvref
  template <typename T>
  struct remove_cvref {
    using type = std::remove_cv_t<std::remove_reference_t<T>>;
  };

  template <typename T>
  using remove_cvref_t = typename remove_cvref<T>::type;

  template <typename T>
  auto to_std_array( T&& some_v ) {
    if constexpr ( std::is_same_v<remove_cvref_t<T>, float_v> ) {
      std::array<float, simd::size> tmp;
      some_v.store( tmp.data() );
      return tmp;
    } else if ( std::is_same_v<remove_cvref_t<T>, int_v> ) {
      std::array<int, simd::size> tmp;
      some_v.store( tmp.data() );
      return tmp;
    }
  }

  constexpr float defaultZBinSize = 0.25 * Gaudi::Units::mm;
  constexpr int   defaultNZBins   = int( ( defaultMaxZ - defaultMinZ ) / defaultZBinSize );

} // namespace

using namespace LHCb::Event::PV;

class TrackBeamLineVertexFinderSoA
    : public LHCb::Algorithm::Transformer<PrimaryVertexContainer( const EventContext&, const LHCb::Pr::Velo::Tracks&,
                                                                  const LHCb::Pr::Velo::Tracks&,
                                                                  const LHCb::Conditions::InteractionRegion& ),
                                          LHCb::DetDesc::usesConditions<LHCb::Conditions::InteractionRegion>> {
public:
  /// Standard constructor
  TrackBeamLineVertexFinderSoA( const std::string& name, ISvcLocator* pSvcLocator );
  /// Initialization
  StatusCode initialize() override;
  /// Execution
  PrimaryVertexContainer operator()( const EventContext&, const LHCb::Pr::Velo::Tracks&, const LHCb::Pr::Velo::Tracks&,
                                     const LHCb::Conditions::InteractionRegion& ) const override;

private:
  Gaudi::Property<uint16_t> m_minNumTracksPerVertex{this, "MinNumTracksPerVertex", defaultMinNumTracks,
                                                    "Min number of tracks per PV"};
  Gaudi::Property<float>    m_minTotalTrackWeightPerVertex{this, "MinTotalTrackWeightPerVertex", 0.,
                                                        "Min sum of Tukey weights per PV"};
  Gaudi::Property<float>    m_zmin{this, "MinZ", defaultMinZ, "Min z position of vertex seed"};
  Gaudi::Property<float>    m_zmax{this, "MaxZ", defaultMaxZ, "Max z position of vertex seed"};
  Gaudi::Property<float>    m_zminIR{this, "MinZIR", -300., "Min z position of vertex seed for Interaction Region"};
  Gaudi::Property<float>    m_zmaxIR{this, "MaxZIR", +300., "Max z position of vertex seed for Interaction Region"};
  Gaudi::Property<float>    m_dz{this, "ZBinSize", defaultZBinSize, "Z histogram bin size"};
  Gaudi::Property<float>    m_maxTrackZ0Err{this, "MaxTrackZ0Err", 10.0 * Gaudi::Units::mm,
                                         "Maximum z0-error for adding track to histo"};
  Gaudi::Property<float>    m_maxTrackZ0ErrIR{this, "MaxTrackZ0ErrIR", 1.5 * Gaudi::Units::mm,
                                           "Maximum z0-error for adding track to histo for Interaction Region"};
  Gaudi::Property<float>    m_Z0ErrMax{this, "Z0ErrMax", 1.0 * Gaudi::Units::mm, "Maximum for z0-error binning"};
  Gaudi::Property<float> m_minBinContent{this, "MinBinContent", 0.01, "Minimal integral in bin for cluster boundary"};
  Gaudi::Property<float> m_minDipSize{this, "MinDipSize", 1.0, "Minimal track integral on one side of histogram peak"};
  Gaudi::Property<float> m_minDipDensity{this, "MinDipDensity", 0.0 / Gaudi::Units::mm,
                                         "Minimal depth of a dip to split cluster (inverse resolution)"};
  Gaudi::Property<float> m_minTracksInSeed{this, "MinTrackIntegralInSeed", 2.9};
  Gaudi::Property<float> m_maxVertexRho{this, "BeamSpotRCut", 0.3 * Gaudi::Units::mm,
                                        "Maximum distance of vertex to beam line"};
  Gaudi::Property<uint16_t> m_maxFitIter{this, "MaxFitIter", defaultMaxFitIter,
                                         "Maximum number of iterations for vertex fit"};
  Gaudi::Property<float>    m_maxDeltaChi2{this, "MaxDeltaChi2", defaultMaxDeltaChi2,
                                        "Maximum chi2 contribution of track to vertex fit"};
  Gaudi::Property<float>    m_maxDeltaZConverged{this, "MaxDeltaZConverged", defaultMaxDeltaZConverged,
                                              "Limit on change in z to determine if vertex fit has converged"};
  Gaudi::Property<float>    m_maxDeltaChi2Converged{this, "MaxDeltaChi2Converged", defaultMaxDeltaChi2Converged,
                                                 "Limit on change in chi2 to determine if vertex fit has converged"};
  Gaudi::Property<float> m_minVertexZSeparationChi2{this, "MinVertexZSeparationChi2", defaultMinVertexZSeparationChi2};
  Gaudi::Property<float> m_minVertexZSeparation{this, "MinVertexZSeparation", defaultMinVertexZSeparation};
  Gaudi::Property<float> m_maxTrackBLChi2{this, "MaxTrackBLChi2", 25.,
                                          "Maximum chi2 of track to beam line contributing to seeds"};
  Gaudi::Property<float> m_beamLineOffsetX{this, "BeamLineOffsetX", 0.0f, "X correction applied to beamline position"};
  Gaudi::Property<float> m_beamLineOffsetY{this, "BeamLineOffsetY", 0.0f, "Y correction applied to beamline position"};
  static constexpr int   Nztemplatebins           = 16;
  static constexpr int   Nztemplates              = 32;
  static constexpr unsigned TemplateNormalization = 128;
  using ZHistoType                                = uint32_t;
  ZHistoType m_ztemplates[2 * Nztemplates][Nztemplatebins]; // odd and even, see note
  mutable Gaudi::Accumulators::SummingCounter<unsigned int>  m_nbPVsCounter{this, "Nb PVs"};
  mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_frFailedFits{this, "Fraction of failed PV fits"};
  mutable Gaudi::Accumulators::BinomialCounter<unsigned int> m_frUnconvergedFits{this,
                                                                                 "Fraction of unconverged PV fits"};
#ifdef TIMINGHISTOGRAMMING
  AIDA::IProfile1D* m_timeperstepPr{nullptr};
  AIDA::IProfile1D* m_timevsntrksPr{nullptr};
  AIDA::IProfile1D* m_timevsnvtxPr{nullptr};
#endif
};

DECLARE_COMPONENT( TrackBeamLineVertexFinderSoA )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
TrackBeamLineVertexFinderSoA::TrackBeamLineVertexFinderSoA( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {KeyValue{"TracksBackwardLocation", "Rec/Track/VeloBackward"},
                    KeyValue{"TracksLocation", "Rec/Track/Velo"},
                    {KeyValue{"InteractionRegionCache", "AlgorithmSpecific-" + name + "-InteractionRegion"}}},
                   KeyValue{"OutputVertices", LHCb::Event::v2::RecVertexLocation::Primary} ) {}

//=============================================================================
// ::initialize()
//=============================================================================
StatusCode TrackBeamLineVertexFinderSoA::initialize() {
  auto sc = Transformer::initialize().andThen( [&]() {
    LHCb::Conditions::InteractionRegion::addConditionDerivation( this,
                                                                 inputLocation<LHCb::Conditions::InteractionRegion>() );
  } );

  // Print a message if the number of bins in the histogram is too small for
  // stack allocation (not sure it makes a real difference).
  const int nZBins = ( m_zmax - m_zmin ) / m_dz;
  if ( nZBins < defaultNZBins )
    info() << "With this configuration, the number of bins in the zpoca histogram (" << nZBins
           << ") is smaller than used for static allocation (defaultNZB=" << defaultNZBins
           << ") . Please increase the latter in the C++." << endmsg;

  // Fill the odd templates first
  const double sqrthalf = std::sqrt( 0.5 );
  {
    // of course, since the thing is symmetric we can do this more efficiently, but that's not quite worth it now
    const double zmaxodd = m_dz * ( Nztemplatebins / 2 - 1 + 0.5 );
    const double zminodd = -zmaxodd;
    for ( int itemplate = 0; itemplate < Nztemplates; ++itemplate ) {
      const double sigmaz   = m_Z0ErrMax * double( itemplate + 1 ) / Nztemplates;
      double       integral = 0.5 * std::erf( sqrthalf * zminodd / sigmaz );
      const double offset   = ( 1 + 2 * integral ) / ( Nztemplatebins - 1 );
      for ( int ibin = 0; ibin < Nztemplatebins - 1; ++ibin ) {
        double thisintegral                   = 0.5 * std::erf( sqrthalf * ( zminodd + ( ibin + 1 ) * m_dz ) / sigmaz );
        double bincontent                     = thisintegral - integral + offset;
        m_ztemplates[2 * itemplate + 1][ibin] = std::lround( bincontent * TemplateNormalization );
        integral                              = thisintegral;
      }
      // The last bin is empty because it isn't use for odd templates
      m_ztemplates[2 * itemplate + 1][Nztemplatebins - 1] = 0;
      debug() << "Odd template integral: " << itemplate << " "
              << std::accumulate( m_ztemplates[2 * itemplate + 1], m_ztemplates[2 * ( itemplate ) + 2], 0 ) /
                     float( TemplateNormalization )
              << endmsg;
    }
  }

  // even templates
  {
    // of course, since the thing is symmetric we can do this more efficiently, but that's not quite worth it now
    const double zmaxeven = m_dz * Nztemplatebins / 2;
    const double zmineven = -zmaxeven;
    for ( int itemplate = 0; itemplate < Nztemplates; ++itemplate ) {
      const double sigmaz   = m_Z0ErrMax * double( itemplate + 1 ) / Nztemplates;
      double       integral = 0.5 * std::erf( sqrthalf * zmineven / sigmaz );
      const double offset   = ( 1 + 2 * integral ) / Nztemplatebins;
      for ( int ibin = 0; ibin < Nztemplatebins; ++ibin ) {
        double thisintegral               = 0.5 * std::erf( sqrthalf * ( zmineven + ( ibin + 1 ) * m_dz ) / sigmaz );
        double bincontent                 = thisintegral - integral + offset;
        m_ztemplates[2 * itemplate][ibin] = std::lround( bincontent * TemplateNormalization );
        integral                          = thisintegral;
      }
      debug() << "Even template integral: " << itemplate << " "
              << std::accumulate( m_ztemplates[2 * itemplate], m_ztemplates[2 * itemplate + 1], 0 ) /
                     float( TemplateNormalization )
              << endmsg;
    }
  }

#ifdef TIMINGHISTOGRAMMING
  auto hsvc       = service<IHistogramSvc>( "HistogramDataSvc", true );
  m_timeperstepPr = hsvc->bookProf( name() + "/timeperstep", "time per step", 20, -0.5, 19.5 );
  m_timevsntrksPr = hsvc->bookProf( name() + "/timevsntrks", "time vs number of tracks", 50, -0.5, 249.5 );
  m_timevsnvtxPr  = hsvc->bookProf( name() + "/timevsnvtx", "time vs number of vertices", 12, -0.5, 11.5 );
#endif

  return sc;
}

//=============================================================================
// ::execute()
//=============================================================================

namespace {

  using namespace LHCb::Event;

  template <typename FTYPE>
  inline auto sqr( FTYPE x ) {
    return x * x;
  }
  //
  struct Extremum {
    Extremum( uint16_t _index, uint16_t _value, uint32_t _integral )
        : index{_index}, value{_value}, integral{_integral} {}
    uint16_t index;
    uint16_t value;
    uint32_t integral;
  };
  //
  struct Cluster {
    Cluster( uint16_t _izfirst, uint16_t _izlast, uint16_t _izmax )
        : izfirst{_izfirst}, izlast{_izlast}, izmax{_izmax} {}
    uint16_t izfirst;
    uint16_t izlast;
    uint16_t izmax;
    uint16_t ntracks{0};
  };

  // Function that extracts clusters from a container of extrema. It
  // finds the two largest maxima, then tries to partition on the
  // smallest minimum in between. Selector is a function that takes a
  // maximum, a minimum and another maximum. If the partitioning is
  // successful, the container of extrema is split, and the function
  // called on each container. If it is not successful, the second
  // maximum, and all extrema in between the first and second maximum,
  // are removed, and the function is called on the remaining
  // container. The recursion stops of the container has three
  // entries: that constitutes a cluster. At least one cluster is
  // returned.
  template <typename ExtremaContainer, typename ClusterContainer, typename DipSelector>
  void clusterize( const ExtremaContainer& extrema, ClusterContainer& clusters, DipSelector dipselector ) {
    // all odd extrema are maxima. find the two largest
    auto const N = extrema.size();
    assert( N % 2 == 1 );
    if ( N < 3 ) {
      return;
    } else if ( N == 3 ) {
      clusters.emplace_back( extrema[0].index, extrema[2].index, extrema[1].index );
    } else {
      // find the two largest extrema
      unsigned largest     = 1;
      unsigned nextlargest = 3;
      if ( extrema[largest].value < extrema[nextlargest].value ) std::swap( largest, nextlargest );
      for ( unsigned i = 5; i < N; i += 2 ) {
        if ( extrema[i].value > extrema[largest].value ) {
          nextlargest = largest;
          largest     = i;
        } else if ( extrema[i].value > extrema[nextlargest].value ) {
          nextlargest = i;
        }
      }
      // find the smallest minimum in between
      const int delta    = largest < nextlargest ? 1 : -1;
      int       smallest = largest + delta;
      for ( unsigned i = largest + 3 * delta; i != nextlargest + delta; i += 2 * delta ) {
        if ( extrema[smallest].value > extrema[i].value ) smallest = i;
      }
      // can we partition at this point?
      if ( dipselector( extrema, std::min( largest, nextlargest ), smallest, std::max( largest, nextlargest ) ) ) {
        // create two new extrema containers and recurcively call this function on them.
        // Note that they overlap one element, which is okay.
        ExtremaContainer c1{extrema.begin(), extrema.begin() + smallest + 1};
        ExtremaContainer c2{extrema.begin() + smallest, extrema.end()};
        clusterize( c1, clusters, dipselector );
        clusterize( c2, clusters, dipselector );
      } else {
        // we cannot partition between the two maximu.
        // create a new extrema container in which the second maximum and everything up to the first maximum are
        // removed. then try again.
        ExtremaContainer c1;
        if ( largest < nextlargest ) {
          c1.insert( c1.end(), extrema.begin(), extrema.begin() + largest + 1 );
          c1.insert( c1.end(), extrema.begin() + nextlargest + 1, extrema.end() );
        } else {
          c1.insert( c1.end(), extrema.begin(), extrema.begin() + nextlargest );
          c1.insert( c1.end(), extrema.begin() + largest, extrema.end() );
        }
        clusterize( c1, clusters, dipselector );
      }
    }
  }

  // inline std::ostream& operator<<( std::ostream& os, Cluster const& c ) {
  //   os << "[" << c.izfirst << ", " << c.izlast << ", " << c.izmax << "]";
  //   return os;
  // }

  // we put this in local scope such that we can access it from various standalone routines
#ifdef TIMINGHISTOGRAMMING
  std::array<LHCb::TrackKernel::Timer, 20> timer;
  void                                     resettimers() {
    for ( auto& t : timer ) t.reset();
  }
#endif
} // namespace

namespace LHCb::Event::PV {

  namespace PVTrackTag {
    struct zerr : float_field {};   // error in coordinate at beam line
    struct blchi2 : float_field {}; // beamline chi2
    template <typename T>
    using pvseedingtrack_t = SOACollection<T, PVTrackTag::veloindex, PVTrackTag::pvindex, z, zerr, blchi2>;
  } // namespace PVTrackTag

  struct PVSeedingTracks : PVTrackTag::pvseedingtrack_t<PVSeedingTracks> {
    using base_t = typename PVTrackTag::pvseedingtrack_t<PVSeedingTracks>;
    using base_t::base_t;
  };

} // namespace LHCb::Event::PV

PrimaryVertexContainer TrackBeamLineVertexFinderSoA::
                       operator()( const EventContext& evtCtx, const LHCb::Pr::Velo::Tracks& tracksBackward,
            const LHCb::Pr::Velo::Tracks& tracksForward, const LHCb::Conditions::InteractionRegion& region ) const {
  // Get the beamline. this only accounts for position, not
  // rotation. that's something to improve! I have considered caching
  // this (with a handle for changes in the geometry, but the
  // computation is so fast that it isn't worth it.)
  // const auto beamline = Gaudi::XYZVector{beamline.X, beamline.Y, 0};
  // const Vec3<float_v> BL = Vec3<float_v>( beamline.X, beamline.Y, 0 );

  // Get the memory resource
  auto memResource = LHCb::getMemResource( evtCtx );

  // Step 1: select tracks with velo info, compute the poca to the
  // beamline. cache the covariance matrix at this position. I'd
  // rather us a combination of copy_if and transform, but don't know
  // how to do that efficiently.
#ifdef TIMINGHISTOGRAMMING
  resettimers();
  timer[9].start();
  timer[1].start();
#endif

  // Allow for correction for beamline position derived from conditions
  const auto beamspot   = region.avgPosition;
  const auto beamlineX  = beamspot.x() + m_beamLineOffsetX;
  const auto beamlineY  = beamspot.y() + m_beamLineOffsetY;
  const auto beamlineTx = region.tX();
  const auto beamlineTy = region.tY();

  // actually we only need to store the (zbeam,veloindex) and fill a histogram. the rest we do not really need.
  PVSeedingTracks pvseedtracks{Zipping::generateZipIdentifier(), memResource};
  auto            pvseedtracks_simd = pvseedtracks.simd();
  pvseedtracks.reserve( tracksForward.size() + tracksBackward.size() );
  int icontainer{0};
  for ( const auto& tracks : {&tracksForward, &tracksBackward} ) {
    for ( auto const& track : tracks->simd() ) {
      auto loop_mask = track.loop_mask();
      auto index     = pvseedtracks.size();
      pvseedtracks.resize( index + popcount( loop_mask ) );

      // there must be a more efficient way to copy from one container to the other
      auto pos  = track.StatePos( Enum::State::Location::ClosestToBeam );
      auto dir  = track.StateDir( Enum::State::Location::ClosestToBeam );
      auto covX = track.StateCovX( Enum::State::Location::ClosestToBeam ); // covXX, covXTx, covTxTx (beats me why this
                                                                           // is stored as a Vec3)
      auto covY = track.StateCovY( Enum::State::Location::ClosestToBeam );

      // compute the z coordinate closest to the beamline and store it
      auto const dx    = beamlineX - pos.x();
      auto const dy    = beamlineY - pos.y();
      auto const tx    = dir.x() - beamlineTx;
      auto const ty    = dir.y() - beamlineTy;
      auto const dz    = select( loop_mask, ( tx * dx + ty * dy ) / ( tx * tx + ty * ty ), 0.f );
      auto const zbeam = pos.z() + dz;

      auto const    Vx          = covX.x() + 2 * dz * covX.y() + dz * dz * covX.z();
      auto const    Vy          = covY.x() + 2 * dz * covY.y() + dz * dz * covY.z();
      float_v const Wx          = select( loop_mask, 1 / Vx, 1.f );
      float_v const Wy          = select( loop_mask, 1 / Vy, 1.f );
      float_v const zweight     = tx * Wx * tx + ty * Wy * ty;
      float_v const zerr        = select( loop_mask, 1 / sqrt( zweight ), 1.f ); // Q_rsqrt(zweight) ;
      float_v const blchi2      = dx * Wx * dx + dy * Wy * dy;
      auto          pvseedtrack = pvseedtracks_simd[index];
      pvseedtrack.field<PVTrackTag::z>().set( zbeam );
      pvseedtrack.field<PVTrackTag::zerr>().set( zerr );
      pvseedtrack.field<PVTrackTag::blchi2>().set( blchi2 );
      pvseedtrack.field<PVTrackTag::veloindex>().set( icontainer, track.indices() );
    }
    ++icontainer;
  }

#ifdef TIMINGHISTOGRAMMING
  timer[1].stop();
  timer[2].start();
#endif

  // Step 2: fill a histogram with the z position of the poca. Use the
  // projected vertex error on that position as the width of a
  // gauss. Divide the gauss properly over the bins. This is quite
  // slow: some simplification may help here.

  // we need to define what a bin is: integral between
  //   zmin + ibin*dz and zmin + (ibin+1)*dz
  // we'll have lot's of '0.5' in the code below. at some point we may
  // just want to shift the bins.

  // this can be changed into an std::accumulate
  // NOTE/TODO: As discussed in Rec#122, this could (and perhaps should) be drawn from an algorithm-local pool instead
  //            of the event-local pool used here. Alternatively, Wouter suggests that specific example could just be
  //            std::array, but this doesn't invalidate the algorithm-local idea!
  const int                                                     nZBins = ( m_zmax - m_zmin ) / m_dz;
  boost::container::small_vector<ZHistoType, defaultNZBins + 1> zhisto( nZBins, 0 );

  {
    const float halfwindow = ( Nztemplatebins / 2 + 1 ) * m_dz;
    const float zmin       = m_zmin + halfwindow;
    const float zmax       = m_zmax - halfwindow;
    const float zminIR     = m_zminIR.value();
    const float zmaxIR     = m_zmaxIR.value();

    int nacceptedtracks{0};
    for ( const auto pvtrack : pvseedtracks.simd() ) {
      auto const loop_mask = pvtrack.loop_mask();
      const auto zbeam     = pvtrack.get<PVTrackTag::z>();
      const auto zerr      = pvtrack.get<PVTrackTag::zerr>();
      const auto blchi2    = pvtrack.get<PVTrackTag::blchi2>();

      //// make sure the template fits. make sure first and last bin
      //// remain empty. there will be lot's of funny effects at edge of
      //// histogram but we do not care.

      //// compute the beamline chi2. we make a cut for tracks contributing to seeding
      // (bl_x, bl_y)^T * W * (bl_x, bl_y) for diagonal W
      auto const mask =
          to_std_array( int_v( loop_mask && blchi2 < m_maxTrackBLChi2.value() && zmin < zbeam && zbeam < zmax &&
                               zerr < m_maxTrackZ0Err.value() &&
                               ( zminIR > zbeam || zmaxIR < zbeam || zerr < m_maxTrackZ0ErrIR.value() ) ) );
      auto const zerrrebinfactor = int_v( 1 + zerr / m_Z0ErrMax.value() );
      auto const zerrscale       = float_v{1.0} / zerrrebinfactor;
      // auto const zerrbin = int_v(Nztemplates / m_Z0ErrMax.value() * (zerr/zerrrebinfactor)) ;
      auto const zerrbin = int_v( ( Nztemplates / m_Z0ErrMax.value() ) * ( zerr * zerrscale ) );

      // bin in which z0 is, in floating point
      auto const jbin =
          int_v{4 * ( zbeam - m_zmin.value() ) / m_dz.value()}; // we need factor 4 to determine odd or even
      int_v const dbin        = jbin & 3;                       // & 3 is the same as % 4
      auto const  minbin      = to_std_array( ( jbin >> 2 ) - zerrrebinfactor * ( Nztemplatebins / 2 ) +
                                        select( dbin == 0, int_v( 0 ), int_v( 1 ) ) );
      auto const  oddtemplate = select( ( dbin == 0 ) || ( dbin == 3 ), int_v( 0 ), int_v( 1 ) );

      auto const templatebin         = to_std_array( 2 * zerrbin + oddtemplate );
      auto const zerrscale_arr       = to_std_array( zerrscale );
      auto const zerrrebinfactor_arr = to_std_array( zerrrebinfactor );

      for ( std::size_t simd_idx = 0; simd_idx < simd::size; ++simd_idx ) {
        if ( mask[simd_idx] ) {
          ++nacceptedtracks;
          // minbin is the bin in which the templates start (already corrected for scale)
          const int beginbin = std::max( 0, -minbin[simd_idx] );
          const int endbin = std::min( Nztemplatebins * zerrrebinfactor_arr[simd_idx], nZBins - minbin[simd_idx] - 1 );
          // if there were no error scaling, this run would run from 0 to Nztemplatebins=16. now it runs from 0 to
          // Nztemplatebins*rebinfactor. this has become much more timeconsuming perhaps because the compiler no longer
          // unrolls the loop.
          for ( int j = beginbin; j < endbin; ++j ) {
            const auto weight =
                m_ztemplates[templatebin[simd_idx]][j / zerrrebinfactor_arr[simd_idx]] * zerrscale_arr[simd_idx];
            zhisto[j + minbin[simd_idx]] += std::lround( weight );
          }
        }
      }
    }
    if ( msgLevel( MSG::DEBUG ) )
      debug() << "Histogram integral: "
              << std::accumulate( zhisto.begin(), zhisto.end(), 0 ) / float( TemplateNormalization ) << " "
              << pvseedtracks.size() << " " << nacceptedtracks << endmsg;
  }

#ifdef TIMINGHISTOGRAMMING
  timer[2].stop();
  timer[3].start();
#endif
  // info() << "Start of cluster search" << endmsg ;
  ////
  ////// Step 3: perform a peak search in the histogram. This used to be
  ////// very simple but the logic needed to find 'significant dips' made
  ////// it a bit more complicated. In the end it doesn't matter so much
  ////// because it takes relatively little time.
  ////
  boost::container::small_vector<Cluster, 32> clusters;
  {
    // Step A: make 'ProtoClusters': these are subsequent bins with
    // non-zero content and an integral above the threshold.
    //
    // Step B: for each such ProtoClusters create a list of 'extrema':
    // these are points where the derivative changes sign.  an
    // extremum is a bin-index, plus the integral till that point,
    // plus the content of the bin.
    //
    // Step C: using the extrema, find 'partition points': a partition
    // point is the smallest minimum between the two largest
    // maxima. if the partition point is valid, then use it, otherwise
    // delete the smallest of the two maximum and try with the next
    // pair. If no more partition points are found, accept the
    // cluster.

    // Step A: make 'proto-clusters'
    const unsigned minTracksInSeed = m_minTracksInSeed * TemplateNormalization;
    const unsigned mindipsize      = m_minDipSize * TemplateNormalization;
    const unsigned mindip          = m_minDipDensity * m_dz * TemplateNormalization; // need to invent something
    const unsigned threshold       = std::max( int( m_minBinContent * TemplateNormalization ), 1 );

    using BinIndex = unsigned;
    boost::container::small_vector<BinIndex, 64> clusteredges;
    {
      bool     prevempty = true;
      uint32_t integral  = zhisto[0];
      for ( BinIndex i = 1; i < zhisto.size(); ++i ) {
        integral += zhisto[i];
        bool empty = !( zhisto[i] >= threshold );
        if ( empty != prevempty ) {
          if ( empty ) {
            if ( integral >= minTracksInSeed )
              clusteredges.emplace_back( i );
            else
              clusteredges.pop_back();
            integral = 0;
          } else {
            clusteredges.emplace_back( i - 1 );
          }
          prevempty = empty;
        }
      }
    }

    // Step B: find the extrema. All even extrema are maxima.
    const size_t Nproto = clusteredges.size() / 2;
    for ( uint16_t j = 0; j < Nproto; ++j ) {
      const auto ibegin = clusteredges[j * 2];
      const auto iend   = clusteredges[j * 2 + 1];
      // Find the extrema. All odd extrema are minima. All even extrema are maxima.
      boost::container::small_vector<Extremum, 64> extrema;
      uint32_t                                     integral = zhisto[ibegin];
      extrema.emplace_back( ibegin, 0, integral / 2 );
      for ( BinIndex i = ibegin; i < iend; ++i ) {
        const auto thisvalue    = zhisto[i];
        const auto thisintegral = integral + thisvalue / 2;
        integral += thisvalue;
        // Four cases:
        // A. last extremum was a maximum and we are larger: replace the last extremum
        // B. last extremum was a mimimum and we are smaller: replace the last extremum
        // C. last extremum was a maximum and we are smaller: accept if sufficiently smaller.
        // D. last extremum was a minimum and we are larger: accept if sufficiently larger.
        if ( extrema.size() % 2 == 0 ) {            // case A or C
          if ( thisvalue > extrema.back().value ) { // case A
            extrema.back().index    = i;
            extrema.back().value    = thisvalue;
            extrema.back().integral = thisintegral;
          } else { // case C
            if ( thisvalue < zhisto[i + 1] && thisvalue + threshold < extrema.back().value )
              extrema.emplace_back( i, thisvalue, thisintegral );
          }
        } else {                                     // Case B or D
          if ( thisvalue <= extrema.back().value ) { // case B
            extrema.back().index    = i;
            extrema.back().value    = thisvalue;
            extrema.back().integral = thisintegral;
          } else { // Case D
            if ( thisvalue > zhisto[i + 1] && thisvalue > extrema.back().value + threshold )
              extrema.emplace_back( i, thisvalue, thisintegral );
          }
        }
      }

      // replace the last minimum, or add a minimum
      const auto N = extrema.size();
      if ( N % 2 == 1 ) extrema.pop_back();
      extrema.emplace_back( iend, 0, integral );

      // Step C: call the clusterization
      clusterize( extrema, clusters,
                  [mindip, minTracksInSeed, mindipsize]( const auto& v, int posmax1, int posmin, int posmax2 ) {
                    // this functions returns true if the minimum between these maxima is an acceptable partition point.
                    // check that there are enough tracks on either side of the minimum
                    if ( v[posmin].integral - v.front().integral < minTracksInSeed ) return false;
                    if ( v.back().integral - v[posmin].integral < minTracksInSeed ) return false;
                    // check that the maxima are large enough
                    if ( v[posmax1].value < v[posmin].value + mindip ) return false;
                    if ( v[posmax2].value < v[posmin].value + mindip ) return false;
                    if ( mindipsize > 0 ) {
                      if ( ( v[posmin].integral - v[posmax1].integral ) <
                           v[posmin].value * ( v[posmin].index - v[posmax1].index ) + mindipsize )
                        return false;
                      if ( ( v[posmax2].integral - v[posmin].integral ) <
                           v[posmin].value * ( v[posmax2].index - v[posmin].index ) + mindipsize )
                        return false;
                    }
                    return true;
                  } );
    }
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of clusters: " << clusters.size() << endmsg;
  }

#ifdef TIMINGHISTOGRAMMING
  timer[3].stop();
#endif
  // FIXME: we set up a lot of navigation below which is difficult to
  // fix if a PV is removed in the final selection step. Luckily this
  // happens so rarely that we can choose a lazy solution: if a PV is
  // removed, we just kill the corresponding seed and start over. We
  // can improve this in the future.
  PrimaryVertexContainer output{memResource};
  auto&                  vertices = output.vertices;
  auto&                  pvtracks = output.tracks;
  pvtracks.prvelocontainers[0]    = &tracksForward;
  pvtracks.prvelocontainers[1]    = &tracksBackward;

  if ( !clusters.empty() ) {

    // Step 4: partition the set of tracks by vertex seed: just choose
    // the closest one. we used to do this with std::partition, but that
    // requires that the tracks be reordered etc. instead, we now do
    // something else: each bin in the histogram is assigned to a PV. we
    // then simply recompute for each track in which bin it belongs, and
    // then choose the PV corresponding to that bin.

    // what makes this complicated is that we need to remove clusters that are too small.
#ifdef TIMINGHISTOGRAMMING
    timer[4].start();
#endif
    bool clusteringready = false;
    while ( !clusteringready && clusters.size() > 0 ) {
      boost::container::small_vector<int, defaultNZBins> pvindexhisto( nZBins, 0 );
      const auto                                         N          = clusters.size();
      const int                                          minpvindex = 0;
      const int                                          maxpvindex = N - 1;
      int                                                ibin       = 0;
      for ( int ipv = minpvindex; ipv < maxpvindex; ++ipv ) {
        // this will be the source of a small bias for nearby clusters
        const int zmid = ( clusters[ipv].izlast + clusters[ipv + 1].izfirst ) / 2;
        while ( ibin <= zmid ) pvindexhisto[ibin++] = ipv;
      }
      while ( ibin < nZBins ) pvindexhisto[ibin++] = maxpvindex;
      // assign all the tracks
      for ( const auto& pvtrack : pvseedtracks.simd() ) {
        auto ibin     = int_v{( pvtrack.get<PVTrackTag::z>() - m_zmin.value() ) / m_dz.value()};
        auto truncbin = max( 0, min( ibin, nZBins - 1 ) );
        auto pvindex  = gather( pvindexhisto.data(), truncbin );
        pvseedtracks.store<PVTrackTag::pvindex>( pvtrack.offset(), pvindex );
      }
      // count the number of tracks assigned to each vertex.
      for ( auto& clus : clusters ) clus.ntracks = 0;
      for ( const auto& pvtrack : pvseedtracks.scalar() )
        ++( clusters[pvtrack.get<PVTrackTag::pvindex>().cast()].ntracks );
      // remove clusters that are too small. this should happen rarely,
      // so it doesn't make sense to reuse the histogram that we already
      // had. we will first remove the smallest cluster, then restart
      // the loop.
      auto smallestcluster = std::min_element( clusters.begin(), clusters.end(),
                                               []( const auto& a, const auto& b ) { return a.ntracks < b.ntracks; } );
      clusteringready      = smallestcluster->ntracks >= m_minNumTracksPerVertex;
      if ( !clusteringready ) clusters.erase( smallestcluster );
    }
#ifdef TIMINGHISTOGRAMMING
    timer[4].stop();
#endif
  }

  //// Step 5: Seed the primary vertices and assign tracks
  // Now we need to construct the output
  if ( !clusters.empty() ) {
#ifdef TIMINGHISTOGRAMMING
    timer[5].start();
#endif
    // std::vector<Vertex, LHCb::Allocators::EventLocal<Vertex>> vertices{memResource};
    // First initialize the vertex parameters. I found that this funny
    // weighted 'maximum' is better than most other inexpensive
    // solutions.
    auto zClusterMean = [this, &zhisto]( auto izmax ) -> float {
      const auto* b   = zhisto.data() + izmax;
      int         d1  = *b - *( b - 1 );
      int         d2  = *b - *( b + 1 );
      float       idz = d1 + d2 > 0 ? ( 0.5f * ( d1 - d2 ) ) / ( d1 + d2 ) : 0.0f;
      return m_zmin + m_dz * ( izmax + idz + 0.5f );
    };

    /// Create a container that holds the seed positions. We can probably do this a little smarter, without explicitly
    /// creating the container.
    boost::container::small_vector<Gaudi::XYZPoint, 32> seedpositions{clusters.size()};
    std::transform( clusters.begin(), clusters.end(), seedpositions.begin(), [=]( const auto& clus ) {
      const auto z = zClusterMean( clus.izmax );
      return Gaudi::XYZPoint{beamlineX + beamlineTx * z, beamlineY + beamlineTy * z, z};
    } );

    // Now copy all relations, fill data and fit
    const AdaptiveFitConfig fitconfig{m_maxDeltaChi2, m_maxDeltaZConverged, m_maxDeltaChi2Converged, m_maxFitIter};
    initializeFromSeeds( output, seedpositions, pvseedtracks, fitconfig );

    // Count the number of failed and unvonverged PV fits
    {
      auto nFailed = std::count_if( output.vertices.begin(), output.vertices.end(),
                                    []( const auto& pv ) { return pv.status() == PVFitStatus::Failed; } );
      m_frFailedFits += {size_t( nFailed ), clusters.size()};
      auto nUnconverged = std::count_if( output.vertices.begin(), output.vertices.end(),
                                         []( const auto& pv ) { return pv.status() == PVFitStatus::NotConverged; } );
      m_frUnconvergedFits += {size_t( nUnconverged ), clusters.size()};
    }
#ifdef TIMINGHISTOGRAMMING
    timer[5].stop();
    timer[7].start();
#endif

    // Now perform two additional steps:
    // * merging (this seem especially needed on very busy events, like lead-lead)
    // * remove vertices with too little tracks or too large distance to beamline
    // If this leads to a change in the number of vertices, we will also perform another vertex fit

    // Merge vertices that are compatible with the previous vertex
    mergeCloseVertices( output, m_minVertexZSeparation, m_minVertexZSeparationChi2, fitconfig );

    // Remove any vertices that do not pass the selection. We assign their tracks to the next or previous vertex,
    // depending on which one is closer.
    applyBeamlineSelection( output, m_minNumTracksPerVertex, m_minTotalTrackWeightPerVertex, m_maxVertexRho, beamlineX,
                            beamlineY, beamlineTx, beamlineTy, fitconfig );
#ifdef TIMINGHISTOGRAMMING
    timer[7].stop();
#endif
  }
  // Set the keys of the PVs. Unfortunately, as long as PrimaryVertex
  // derives from KeyedObject, we can only set the key once, so we
  // really need to do this at the end. Perhaps we should get rid of
  // keys altogether.
  output.setIndices();
  // make sure to set up some navigation for the unbiasing
  output.updateVeloIDMap();

  // LHCb::Event::v2::RecVertices recvertexcontainer{memResource};
  m_nbPVsCounter += vertices.size();
  // std::cout << "TrackBeamLineVertexFinderSoA::operator end" << std::endl ;

  // PrimaryVertexContainer output{std::move( primaryvertices ), std::move( pvtracks )};
#ifdef TIMINGHISTOGRAMMING
  // timer[8].stop();
  timer[9].stop();
  for ( int i = 0; i < 20; ++i ) m_timeperstepPr->fill( float( i ), timer[i].total() );
  m_timevsntrksPr->fill( pvseedtracks.size(), timer[9].total() );
  m_timevsnvtxPr->fill( vertices.size(), timer[9].total() );
#endif
  return output;
}
