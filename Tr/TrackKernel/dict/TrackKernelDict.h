/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef TRACKFITEVENT_LCGDICT_H
#define TRACKFITEVENT_LCGDICT_H 1

// Additional classes to be added to automatically generated lcgdict

// begin include files
#include "TrackKernel/StateTraj.h"
#include "TrackKernel/TrackStateVertex.h"
#include "TrackKernel/TrackTraj.h"

// end include files

namespace {
  struct _Instantiations {
    // begin instantiations
    // end instantiations
  };
} // namespace

#endif // TRACKFITEVENT_LCGDICT_H
