/*****************************************************************************\
* (c) Copyright 2000-2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/FunctorDefaults.h"
#include "Event/Particle.h"
#include "Event/PrimaryVertices.h"
#include "Event/RecVertex.h"

namespace LHCb {

  namespace PrimaryVertexUtils {
    template <typename T>
    auto const* to_pointer( T&& t ) { // please note the use of `auto`
      if constexpr ( std::is_pointer_v<std::remove_reference_t<T>> ) {
        return t;
      } else if constexpr ( std::is_lvalue_reference_v<T> ) {
        return &t;
      } else {
        static_assert( !std::is_same_v<T, T>, "to_pointer should not be called with type T" );
        return (void*)nullptr;
      }
    }
  } // namespace PrimaryVertexUtils

  /// Helper function to find the best PV. Returns iterator and value of distance^2
  template <typename VertexContainer, typename FTYPE>
  inline auto bestPVWithD2( const VertexContainer& pvs, const FTYPE x, const FTYPE y, const FTYPE z, const FTYPE tx,
                            const FTYPE ty ) {
    std::pair best{std::end( pvs ), std::numeric_limits<FTYPE>::max()};
    for ( auto it = pvs.begin(); it != pvs.end(); ++it ) {
      const auto* pv = PrimaryVertexUtils::to_pointer( *it );
      // const auto& pv = PrimaryVertexUtils::deref_if_ptr(*it) ;
      auto dz = pv->position().z() - z;
      auto dx = pv->position().x() - ( x + dz * tx );
      auto dy = pv->position().y() - ( y + dz * ty );
      auto d2 = dx * dx + dy * dy;
      best    = best.first != pvs.end() && !( best.second > d2 ) ? best : std::pair{it, d2};
    }
    return best;
  }

  /// Helper function to find the best RecVertex PV
  template <typename VertexContainer, typename FTYPE>
  inline auto bestPV( const VertexContainer& pvs, const FTYPE x, const FTYPE y, const FTYPE z, const FTYPE tx,
                      const FTYPE ty ) {
    return pvs.size() > 0 ? PrimaryVertexUtils::to_pointer( *( bestPVWithD2( pvs, x, y, z, tx, ty ).first ) ) : nullptr;
  }

  /// Return the index of the best pointing PV for this state (which could bederived from a particle).
  template <typename Vertices, typename FTYPE>
  inline auto bestPVIndex( const Vertices& pvs, const FTYPE x, const FTYPE y, const FTYPE z, const FTYPE tx,
                           const FTYPE ty ) {
    return pvs.size() > 0
               ? LHCb::Event::PV::PVIndex( std::distance( pvs.begin(), bestPVWithD2( pvs, x, y, z, tx, ty ).first ) )
               : LHCb::Event::PV::PVIndex( LHCb::Event::PV::PVIndexInvalid );
  }

  /// Returns the best PV index given a position and directionvector
  template <typename Vertices, typename Position, typename Direction>
  inline auto bestPVIndex( const Vertices& pvdata, const Position pos, const Direction dir ) {
    return bestPVIndex( pvdata, pos.x(), pos.y(), pos.z(), dir.x() / dir.z(), dir.y() / dir.z() );
  }

  /// Returns the best PV index given a position and directionvector
  template <typename Vertices>
  inline auto bestPVIndex( const Vertices& pvdata, const LHCb::Particle& particle ) {
    return bestPVIndex( pvdata, particle.referencePoint(), particle.momentum() );
  }

  /// Returns the best RecVertex PV given a position and direction vector
  template <typename VertexContainer, typename Position, typename Direction>
  inline auto bestPV( const VertexContainer& pvs, const Position pos, const Direction dir ) {
    return bestPV( pvs, pos.x(), pos.y(), pos.z(), dir.x() / dir.z(), dir.y() / dir.z() );
  }

  /// Return the best RecVertex PV for a particle
  template <typename VertexContainer>
  auto bestPV( const VertexContainer& pvs, const Particle& particle ) {
    return bestPV( pvs, particle.referencePoint(), particle.momentum() );
  }

  template <typename FTYPE>
  inline std::optional<PrimaryVertex> unbiasedBestPV( const LHCb::Event::PV::PrimaryVertexContainer& pvdata,
                                                      const FTYPE x, const FTYPE y, const FTYPE z, const FTYPE tx,
                                                      const FTYPE ty, LHCb::span<int> vetoedvelotrackindices ) {
    return !pvdata.vertices.empty()
               ? unbiasedVertex( pvdata, bestPVIndex( pvdata, x, y, z, tx, ty ), vetoedvelotrackindices )
               : std::nullopt;
  }

  template <typename Position, typename Direction>
  inline std::optional<PrimaryVertex> unbiasedBestPV( const LHCb::Event::PV::PrimaryVertexContainer& pvdata,
                                                      const Position pos, const Direction dir,
                                                      LHCb::span<int> vetoedvelotrackindices ) {
    return !pvdata.vertices.empty()
               ? unbiasedVertex( pvdata,
                                 bestPVIndex( pvdata, pos.x(), pos.y(), pos.z(), dir.x() / dir.z(), dir.y() / dir.z() ),
                                 vetoedvelotrackindices )
               : std::nullopt;
  }

  /// routine to collect all velo-segment IDs from the decay chain of a particle: needed for unbiasing.
  boost::container::small_vector<uint32_t, 16> collectVeloSegmentIDs( const Particle& p );

  /// Return the unbiased best PV for a particle. Note: it is better to use refitted PVs
  inline std::optional<PrimaryVertex> unbiasedBestPV( const LHCb::Event::PV::PrimaryVertexContainer& PVs,
                                                      const Particle&                                particle ) {
    if ( PVs.empty() ) return {};
    auto ids = collectVeloSegmentIDs( particle );
    return LHCb::Event::PV::unbiasedVertex( PVs, bestPVIndex( PVs, particle ), LHCb::span<const uint32_t>( ids ) );
  }

  /// Return the list of unbiased PVs for a particle. Note: it is better to use 'refitted' PVs
  inline auto unbiasedPVs( const LHCb::Event::PV::PrimaryVertexContainer& PVs, const LHCb::Particle& particle ) {
    auto ids = collectVeloSegmentIDs( particle );
    return LHCb::Event::PV::unbiasedVertices( PVs, LHCb::span<const uint32_t>( ids ) );
  }

  /// The following typedef determines which container is used for the Particle->PV pointer
  using PrimaryVertices = LHCb::RecVertex::Range; // Event::PV::PrimaryVertexContainer ;
                                                  // using PrimaryVertices = LHCb::PrimaryVertex::Range ;

  /// This is a wrapper such that we can access a KeyedContainer or ObjectContainer by index,
  /// and without the need to dereference the pointers: It makes it look more like a vector<T>.
  /// We use this in VertexRelations to be able to use RecVertices in place of PrimaryVertexContainer.
  template <typename Container>
  struct ObjectContainerVectorView {
    using value_type = typename Container::contained_type;
    // only one constructor
    const Container& container;
    ObjectContainerVectorView( const Container& c ) : container{c} {}
    // define the iterator
    struct const_iterator {
      typename Container::const_iterator it;
      const_iterator( typename Container::const_iterator i ) : it{i} {}
      const_iterator& operator++() {
        ++it;
        return *this;
      }
      const value_type& operator*() const { return **it; }
      const value_type& operator->() const { return **it; }
      bool              operator<( const const_iterator& rhs ) const { return it < rhs.it; }
      bool              operator!=( const const_iterator& rhs ) const { return it != rhs.it; }
    };
    // add begin and end
    const_iterator    begin() const { return const_iterator{container.begin()}; }
    const_iterator    end() const { return const_iterator{container.end()}; }
    const value_type& operator[]( size_t index ) const {
      const_iterator it = begin();
      std::advance( it.it, index );
      return *it;
    }
    auto              empty() const { return container.empty(); }
    auto              size() const { return container.size(); }
    const value_type& front() const { return **( container.begin() ); }
  };
} // namespace LHCb
