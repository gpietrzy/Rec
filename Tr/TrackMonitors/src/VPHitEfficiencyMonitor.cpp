/*****************************************************************************\
* (c) Copyright (2022-2023) CERN for the benefit of the LHCb Collaboration    *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/PrHits.h"

#include "VPDet/DeVP.h"
#include "VPDet/DeVPSensor.h"

#include "Event/PrHits.h"
#include "Event/Track.h"

#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>

#include "TrackInterfaces/ITrackExtrapolator.h"
#include "TrackInterfaces/ITrackInterpolator.h"

#include "AIDA/IHistogram2D.h"

namespace VPHitsTag = LHCb::Pr::VP::VPHitsTag;

class VPHitEfficiencyMonitor
    : public LHCb::Algorithm::Consumer<void( const LHCb::Track::Range&, const LHCb::Pr::VP::Hits&,
                                             const DetectorElement&, const DeVP& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DetectorElement, DeVP>> {
private:
  Gaudi::Property<unsigned int> m_sensorUnderStudy{this, "SensorUnderStudy"};

  Gaudi::Property<bool> m_expertMode{this, "ExpertMode", false}; // More detailed histograms
  Gaudi::Property<bool> m_onlineMode{this, "OnlineMode",
                                     false}; // Remove requirement for interpolation on first/last station

  Gaudi::Property<float> m_tolerance{this, "ResidualTolerance", 0.1 * Gaudi::Units::mm}; // tolerance in mm, was 0.2
  Gaudi::Property<float> m_maxTrackError{this, "MaxTrackCov", 0.2 * Gaudi::Units::mm};

  Gaudi::Property<bool> m_fillHot{this, "FillHotEfficiencies", true};
  Gaudi::Property<bool> m_checkSensor{this, "CheckSensorPosition", true};

  ToolHandle<ITrackInterpolator> m_interpolator{this, "Interpolator", "TrackInterpolator"};
  ToolHandle<ITrackExtrapolator> m_linearextrapolator{this, "Extrapolator", "TrackRungeKuttaExtrapolator"};

  // Needed for running in the online monitoring
  AIDA::IHistogram2D* hitPass  = nullptr;
  AIDA::IHistogram2D* hitTotal = nullptr;

  mutable Gaudi::Accumulators::ProfileHistogram<1> m_hitEfficiency{
      this, "hitEfficiencyASIC", "hit efficiency per ASIC", {3, -0.5, 2.5}};
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_hotEfficiency{
      this, "hotEfficiencyASIC", "hot efficiency per ASIC", {3, -0.5, 2.5}};
  // Definitions change when changing expert_mode
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<2>> m_hitEfficiencyPerRowColumn;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<2>> m_hotEfficiencyPerRowColumn;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>>        m_xyResiduals;
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<2>> m_xyResidualsProfile;

  mutable Gaudi::Accumulators::SummingCounter<> m_interPolationError{this, "Could not interpolate"};
  mutable Gaudi::Accumulators::SummingCounter<> m_extrapolationError{this, "Could not extrapolate"};
  mutable Gaudi::Accumulators::SummingCounter<> m_moduleError{this, "Could not find this module"};
  mutable Gaudi::Accumulators::SummingCounter<> m_hitsError{
      this, "Could not find hits downstream or upstream of the sensor being studied"};
  mutable Gaudi::Accumulators::SummingCounter<> m_activeAreaError{this, "NotInActiveArea"};
  mutable Gaudi::Accumulators::SummingCounter<> m_trackCounter{this, "TracksUsed"};

public:
  VPHitEfficiencyMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer( name, pSvcLocator,
                  {KeyValue{"TrackLocation", ""}, KeyValue{"PrVPHitsLocation", ""},
                   KeyValue{"LHCbGeoLocation", LHCb::standard_geometry_top},
                   KeyValue{"VPLocation", DeVPLocation::Default}} ) {}

  StatusCode initialize() override;

  void operator()( const LHCb::Track::Range& tracks, const LHCb::Pr::VP::Hits& hits, const DetectorElement& lhcb,
                   const DeVP& vpDet ) const override {
#ifdef USE_DD4HEP
    const auto&         my_sensor = vpDet.sensor( LHCb::Detector::VPChannelID::SensorID( m_sensorUnderStudy.value() ) );
    float               sizeX     = my_sensor.activeSizeX();
    float               sizeY     = my_sensor.activeSizeY();
    const auto          left_top  = my_sensor.localToGlobal( Gaudi::XYZPoint( -0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_top = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_bottom = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, -0.5 * sizeY, 0.0 ) );
    ROOT::Math::Plane3D my_plane( left_top, right_top, right_bottom );
    auto                z_position = my_sensor.z();

#else
    const auto& my_sensor = vpDet.sensor( LHCb::Detector::VPChannelID::SensorID( m_sensorUnderStudy.value() ) );

    float sizeX =
        my_sensor.numChips() * my_sensor.chipSize() + ( my_sensor.numChips() - 1 ) * my_sensor.interChipDist();
    float sizeY = my_sensor.chipSize();

    const auto          left_top     = my_sensor.localToGlobal( Gaudi::XYZPoint( -0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_top    = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, 0.5 * sizeY, 0.0 ) );
    const auto          right_bottom = my_sensor.localToGlobal( Gaudi::XYZPoint( 0.5 * sizeX, -0.5 * sizeY, 0.0 ) );
    ROOT::Math::Plane3D my_plane( left_top, right_top, right_bottom );
    auto                z_position = my_sensor.z();

#endif

    const auto& all_hits      = hits.scalar();
    int         startPosition = 0;
    int         endPosition   = all_hits.size();

    for ( const auto& track : tracks ) {
      if ( track->pseudoRapidity() < 1.0 || track->pseudoRapidity() > 5.3 )
        continue; // This cut is here to remove tracks that are out of acceptance and/or most likely not originating
                  // from a PV

      LHCb::State state;
      if ( !m_interpolator->interpolate( *track, z_position, state, lhcb ).isSuccess() ) {
        ++m_interPolationError;
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Could not interpolate track." << endmsg;
        continue;
      }

      ++m_trackCounter;

      // only consider if state has reasonably small error. we should
      // still fine-tune this a little bit, make a propor projecion
      // etc. the best is to cut only when you have unbiased the
      // state. cutting too tight could introduce a bias in
      // the efficiency measurement?
      if ( std::sqrt( state.covariance()( 0, 0 ) ) > m_maxTrackError.value() ) {
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Too large cov element" << endmsg;
        continue;
      }

      // to get a slightly more precise answer, intersect with the plane
      if ( !m_linearextrapolator->propagate( state, my_plane, lhcb ).isSuccess() ) {
        ++m_extrapolationError;
        if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Could not extrapolate track" << endmsg;
        continue;
      }

      const ROOT::Math::XYZPoint localIntersection =
          my_sensor.globalToLocal( Gaudi::XYZPoint( state.x(), state.y(), state.z() ) );
      if ( !my_sensor.isInActiveArea( localIntersection ) ) {
        ++m_activeAreaError;
        continue;
      }

      // make sure the sensor is between the first and last hits of the tracks
      if ( m_checkSensor.value() ) {
        const auto lhcbids        = track->lhcbIDs();
        bool       find_lower_id  = false;
        bool       find_higher_id = false;
        for ( const auto& lhcbid : lhcbids ) {
          if ( lhcbid.isVP() ) {
            const auto  vpid     = LHCb::Detector::VPChannelID( lhcbid.vpID() );
            const auto& vpsensor = vpDet.sensor( vpid.sensor() );
            if ( vpsensor.z() < z_position ) find_lower_id = true;
            if ( vpsensor.z() > z_position ) find_higher_id = true;
            if ( find_lower_id && find_higher_id ) break;
          }
        }

        // Remove requirement for first final station to have a step before/after for online monitoring
        if ( m_onlineMode.value() && m_sensorUnderStudy <= 7 ) find_lower_id = true;
        if ( m_onlineMode.value() && m_sensorUnderStudy >= 200 ) find_higher_id = true;
        if ( !( find_lower_id && find_higher_id ) ) {
          ++m_hitsError;
          continue;
        }
      }

      float foundHit        = 0;
      float foundHitOnTrack = 0;
      float bestResidual    = -1;

      for ( int i_hit = startPosition; i_hit < endPosition; i_hit++ ) {
        const auto&                 vphit         = all_hits[i_hit];
        const int                   unwrapped_num = vphit.get<VPHitsTag::ChannelId>().cast();
        LHCb::Detector::VPChannelID vpChannel( unwrapped_num );

        if ( unsigned( vpChannel.module() ) > unsigned( my_sensor.module() ) ) startPosition = i_hit;
        if ( unsigned( vpChannel.module() ) < unsigned( my_sensor.module() ) ) endPosition = i_hit;
        if ( to_unsigned( vpChannel.sensor() ) != m_sensorUnderStudy ) continue;

        // get vp hit position
        const auto            vec = vphit.get<VPHitsTag::pos>();
        const Gaudi::XYZPoint VPhit( vec.x().cast(), vec.y().cast(), vec.z().cast() );

        float dz    = state.z() - vec.z().cast();
        float new_x = state.x() - dz * state.tx();
        float new_y = state.y() - dz * state.ty();

        const auto residual_vector = my_sensor.globalToLocal( VPhit ) -
                                     my_sensor.globalToLocal( Gaudi::XYZPoint( new_x, new_y, vec.z().cast() ) );

        const float residual = residual_vector.R();
        if ( fabs( residual ) < m_tolerance ) {
          foundHit = 1;

          if ( bestResidual < 0 || residual < bestResidual ) { bestResidual = residual; }
        }

        ++( *m_xyResiduals )[{state.x(), state.y()}];
        ( *m_xyResidualsProfile )[{state.x(), state.y()}] += residual;

        // find hit on track
        if ( m_fillHot.value() ) {
          auto it = std::find( track->lhcbIDs().begin(), track->lhcbIDs().end(), LHCb::LHCbID( vpChannel ) );
          if ( it != track->lhcbIDs().end() ) foundHitOnTrack = 1;
        }
      }

      LHCb::Detector::VPChannelID expChannel;
      bool                        findchannel = my_sensor.pointToChannel( localIntersection, true, expChannel );
      if ( !findchannel ) warning() << "not in the sensitive region ... no channel found!" << endmsg;

      m_hitEfficiency[{double( expChannel.chip() )}] += foundHit;
      ( *m_hitEfficiencyPerRowColumn )[{unsigned( expChannel.chip() ) * 256 + unsigned( expChannel.col() ),
                                        unsigned( expChannel.row() )}] += foundHit;

      // hit-on-track efficiency PerXY, sensor, chip, cov+row
      m_hotEfficiency[double( expChannel.chip() )] += foundHitOnTrack;
      ( *m_hotEfficiencyPerRowColumn )[{unsigned( expChannel.chip() ) * 256 + unsigned( expChannel.col() ),
                                        unsigned( expChannel.row() )}] += foundHitOnTrack;

      if ( m_onlineMode.value() ) {
        if ( foundHit == 1 ) {
          hitPass->fill( unsigned( expChannel.chip() ) * 256 + unsigned( expChannel.col() ),
                         unsigned( expChannel.row() ) );
        }
        hitTotal->fill( unsigned( expChannel.chip() ) * 256 + unsigned( expChannel.col() ),
                        unsigned( expChannel.row() ) );
      }
    }
  }
};
DECLARE_COMPONENT( VPHitEfficiencyMonitor )

StatusCode VPHitEfficiencyMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    using Axis1D = Gaudi::Accumulators::Axis<double>;
    // Book histograms

    hitPass  = this->book2D( "hitPass", "passing hits", 0, 768, 768, 0, 256, 256 );
    hitTotal = this->book2D( "hitTotal", "total hits", 0, 768, 768, 0, 256, 256 );

    if ( m_expertMode.value() ) {
      m_hitEfficiencyPerRowColumn.emplace( this, "hitEfficiencyVsRowColumn", "hit efficiency per Row/Column per sensor",
                                           Axis1D{768, -0.5, 767.5}, Axis1D{256, -0.5, 255.5} );

      m_hotEfficiencyPerRowColumn.emplace( this, "hotEfficiencyVsRowColumn", "hot efficiency per Row/Column per sensor",
                                           Axis1D{768, -0.5, 767.5}, Axis1D{256, -0.5, 255.5} );

      m_xyResiduals.emplace( this, "xyResiduals", "residual in x,y",
                             Axis1D{600, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm},
                             Axis1D{600, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm} );

      m_xyResidualsProfile.emplace( this, "xyResidualsProfile", "residual in x,y",
                                    Axis1D{600, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm},
                                    Axis1D{600, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm} );
    } else {
      m_hitEfficiencyPerRowColumn.emplace( this, "hitEfficiencyVsRowColumn", "hit efficiency per Row/Column per sensor",
                                           Axis1D{48, -0.5, 767.5}, Axis1D{16, -0.5, 255.5} );

      m_hotEfficiencyPerRowColumn.emplace( this, "hotEfficiencyVsRowColumn", "hot efficiency per Row/Column per sensor",
                                           Axis1D{48, -0.5, 767.5}, Axis1D{16, -0.5, 255.5} );

      m_xyResiduals.emplace( this, "xyResiduals", "residual in x,y",
                             Axis1D{50, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm},
                             Axis1D{50, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm} );

      m_xyResidualsProfile.emplace( this, "xyResidualsProfile", "residual in x,y",
                                    Axis1D{50, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm},
                                    Axis1D{50, -5.0 * Gaudi::Units::mm, 5.0 * Gaudi::Units::mm} );
    }
  } );
};
