/*****************************************************************************\
* (c) Copyright 2024 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IFileAccess.h"
#include "LHCbAlgs/Consumer.h"
#include "TrackTools/GhostProbabilityModels.h"

namespace {
  template <typename KeyedObject>
  LHCb::MCParticle const* mcTruth( KeyedObject const* obj, LHCb::MCParticles const& mcparts,
                                   LHCb::LinksByKey const& links ) {
    LHCb::MCParticle const* mcp{nullptr};
    if ( obj ) {
      links.applyToLinks( obj->key(), [&mcp, &mcparts]( unsigned int, unsigned int mcpkey, float ) {
        if ( !mcp ) mcp = static_cast<LHCb::MCParticle const*>( mcparts.containedObject( mcpkey ) );
      } );
    }
    return mcp;
  }
} // namespace

template <typename ModelEvaluator, typename InputObjects>
class GhostProbTrainingTupleAlg
    : public LHCb::Algorithm::Consumer<void( InputObjects const&, LHCb::MCParticles const&, LHCb::LinksByKey const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>> {
public:
  using base_type =
      LHCb::Algorithm::Consumer<void( InputObjects const&, LHCb::MCParticles const&, LHCb::LinksByKey const& ),
                                LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg>>;
  using KeyValue = typename base_type::KeyValue;

  GhostProbTrainingTupleAlg( std::string const& name, ISvcLocator* pSvc )
      : base_type{name, pSvc, {KeyValue{"InputObjects", ""}, KeyValue{"MCParticles", ""}, KeyValue{"LinksToMC", ""}}} {}

  StatusCode initialize() override {
    auto sc = base_type::initialize();
    if ( sc.isFailure() ) return sc;
    if ( evaluate_model ) {
      // for loading weights file
      auto buffer = m_filesvc->read( m_weightsfilename.value() );
      sc &= m_model.load( buffer );
    }
    return sc;
  }

  void operator()( InputObjects const& objs, LHCb::MCParticles const& mcparts,
                   LHCb::LinksByKey const& links ) const override {
    auto       tuple = this->nTuple( ( m_modelname.value() == "" ? m_modelname.value() + "" : "/" ) + "TrainingTuple" );
    StatusCode sc    = StatusCode::SUCCESS;
    for ( auto const obj : objs ) {
      // write features first
      LHCb::Utils::unwind<0, ModelEvaluator::ModelType::InputVec::size>( [&]( auto k ) {
        auto const feature = m_model.features()->template get<k>();
        sc &= tuple->column( feature.name(), feature( obj ) );
      } );
      // further selection options after (MC truth, ...)
      auto                    mcp     = mcTruth( obj, mcparts, links );
      LHCb::MCParticle const* mother  = mcp ? mcp->mother() : nullptr;
      LHCb::MCParticle const* gmother = mother ? mother->mother() : nullptr;
      sc &= tuple->column( "MC_TRUEID", mcp ? mcp->particleID().pid() : 0 );
      sc &= tuple->column( "MC_MOTHER_ID", mother ? mother->particleID().pid() : 0 );
      sc &= tuple->column( "MC_GMOTHER_ID", gmother ? gmother->particleID().pid() : 0 );
      sc &= tuple->column( "MC_TRUE_P", mcp ? mcp->momentum().P() : 0.f );
      sc &= tuple->column( "MC_TRUE_PT", mcp ? mcp->momentum().Pt() : 0.f );
      sc &= tuple->column( "MC_TRUE_eta", mcp ? mcp->momentum().Eta() : 0.f );
      sc &= tuple->column( "MC_TRUE_phi", mcp ? mcp->momentum().Phi() : -10.f );
      // additional track info
      sc &= tuple->column( "Track_ghostProbability", obj->ghostProbability() );
      // if available evaluate (old) model
      if ( evaluate_model ) {
        auto invec = typename ModelEvaluator::ModelType::InputVec();
        LHCb::Utils::unwind<0, ModelEvaluator::ModelType::InputVec::size>( [&]( auto k ) {
          auto const feature = m_model.features()->template get<k>();
          invec( k )         = typename ModelEvaluator::ModelType::FType( feature( obj ) );
        } );
        auto output  = m_model.model()->evaluate( invec );
        auto out_arr = SIMDWrapper::to_array( output( 0 ) );
        sc &= tuple->column( "model_output", out_arr[0] );
      }
      // write out row
      sc.andThen( [&] { return tuple->write(); } ).ignore();
    }
  };

private:
  // properties
  Gaudi::Property<std::string> m_modelname{this, "ModelName", ""};
  Gaudi::Property<std::string> m_weightsfilename{this, "WeightsFileName", "",
                                                 [&]( auto& ) { evaluate_model = m_weightsfilename.value() != ""; }};
  bool                         evaluate_model;

  // services
  ServiceHandle<IFileAccess> m_filesvc{this, "FileAccessor", "ParamFileSvc", "Service used to retrieve file contents"};

  // data members
  ModelEvaluator m_model;
};

using Long_noUT = GhostProbTrainingTupleAlg<LHCb::GhostProbability::Long_noUT, LHCb::Tracks>;
DECLARE_COMPONENT_WITH_ID( Long_noUT, "GhostProb_Long_noUT_TrainingTupleAlg" )

using Long       = GhostProbTrainingTupleAlg<LHCb::GhostProbability::Long, LHCb::Tracks>;
using Downstream = GhostProbTrainingTupleAlg<LHCb::GhostProbability::Downstream, LHCb::Tracks>;
using Upstream   = GhostProbTrainingTupleAlg<LHCb::GhostProbability::Upstream, LHCb::Tracks>;
using Ttrack     = GhostProbTrainingTupleAlg<LHCb::GhostProbability::Ttrack, LHCb::Tracks>;
using Velo       = GhostProbTrainingTupleAlg<LHCb::GhostProbability::Velo, LHCb::Tracks>;
DECLARE_COMPONENT_WITH_ID( Long, "GhostProb_Long_TrainingTupleAlg" )
DECLARE_COMPONENT_WITH_ID( Downstream, "GhostProb_Downstream_TrainingTupleAlg" )
DECLARE_COMPONENT_WITH_ID( Upstream, "GhostProb_Upstream_TrainingTupleAlg" )
DECLARE_COMPONENT_WITH_ID( Ttrack, "GhostProb_Ttrack_TrainingTupleAlg" )
DECLARE_COMPONENT_WITH_ID( Velo, "GhostProb_Velo_TrainingTupleAlg" )
