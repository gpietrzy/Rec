/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/DetectorElement.h"
#include "MuonDet/MuonNamespace.h"
#include "MuonInterfaces/NNMuonTrack.h"

#include "Event/PrHits.h"
#include "Event/State.h"
#include "Event/StateVector.h"
#include "Event/Track.h"
#include "Gaudi/Accumulators.h"
#include "Kernel/LHCbID.h"
#include "LHCbAlgs/Transformer.h"
#include "TrackInterfaces/IPrFitterTool.h"
#include "TrackInterfaces/ITrackFitter.h"
#include "TrackInterfaces/ITrackMomentumEstimate.h"
#include <Magnet/DeMagnet.h>

#include <string>
#include <type_traits>
#include <vector>

namespace LHCb {

  using MuonHits = Pr::Hits<LHCb::Pr::HitType::Muon>;

  /**
   *  Simple algorithm to make "LHCb" tracks from MuonNNet tracks
   *  and to copy them to some location.
   *
   *  @author Stefania Vecchi (alias Jan Amoraal)
   *  @date   2009-12-03
   */
  struct MakeMuonTracksBase : Gaudi::Algorithm {
    using Algorithm::Algorithm;
    Event::v1::Tracks executionCore( NNMuonTracks const&, const DeMagnet&, DetectorElement const& lhcb ) const;

  protected:
    Gaudi::Property<bool>         m_skipBigClusters{this, "SkipBigClusters", false};
    Gaudi::Property<bool>         m_Bfield{this, "BField", false};
    Gaudi::Property<unsigned int> m_MaxNTiles{this, "MaxNTiles", 6};
    Gaudi::Property<bool>         m_fitTracks{this, "FitTracks", true};

    ToolHandle<ITrackFitter>           m_TrackMasterFitter{this, "Fitter", "TrackMasterFitter/Fitter"};
    ToolHandle<ITrackMomentumEstimate> m_fCalcMomentum{this, "TrackPtKick", "TrackPtKick", "momentum tool"};

    mutable Gaudi::Accumulators::AveragingCounter<> m_hitsPerTrack{this, "nb MuonHits per track"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_tilesPerTrack{this, "nb tiles used per track"};
    mutable Gaudi::Accumulators::Counter<>          m_trackRejecteBigCluster{this,
                                                                    "nb track not saved due to BIG cluster rejection"};
    mutable Gaudi::Accumulators::Counter<>          m_trackFitFailed{this, "nb track with Fit failure"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbTracks{this, "nb tracks created"};
    mutable Gaudi::Accumulators::AveragingCounter<> m_nbInputTracks{this, "nb input tracks"};

    bool m_isTMF{true};
  };

  struct MakeMuonTracks_TMF
      : Algorithm::Transformer<
            Event::v1::Tracks( NNMuonTracks const&, DeMagnet const&, DetectorElement const& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<MakeMuonTracksBase, DeMagnet, DetectorElement>> {
    MakeMuonTracks_TMF( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {KeyValue{"MuonTracksLocation", ""}, KeyValue{"DeMagnet", LHCb::Det::Magnet::det_path},
                        KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                       KeyValue{"TracksOutputLocation", TrackLocation::Muon} ) {}
    Event::v1::Tracks operator()( NNMuonTracks const& tracks, DeMagnet const& magnet,
                                  DetectorElement const& lhcb ) const override {
      return executionCore( tracks, magnet, lhcb );
    }
  };
  DECLARE_COMPONENT_WITH_ID( MakeMuonTracks_TMF, "MakeMuonTracks" )

  struct MakeMuonTracks_PrKF
      : Algorithm::Transformer<
            Event::v1::Tracks( NNMuonTracks const&, MuonHits const&, IPrFitterTool const&, DeMagnet const&,
                               DetectorElement const& ),
            LHCb::Algorithm::Traits::usesBaseAndConditions<MakeMuonTracksBase, DeMagnet, DetectorElement>> {
    MakeMuonTracks_PrKF( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       {KeyValue{"MuonTracksLocation", ""}, KeyValue{"MuonHitsLocation", ""},
                        KeyValue{"KalmanFilterTool", ""}, KeyValue{"DeMagnet", LHCb::Det::Magnet::det_path},
                        KeyValue{"StandardGeometryTop", LHCb::standard_geometry_top}},
                       KeyValue{"TracksOutputLocation", TrackLocation::Muon} ) {
      m_isTMF = false;
    }
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] { m_TrackMasterFitter.disable(); } );
    }
    Event::v1::Tracks operator()( NNMuonTracks const&, MuonHits const&, IPrFitterTool const&, DeMagnet const&,
                                  DetectorElement const& ) const override;
  };
  DECLARE_COMPONENT_WITH_ID( MakeMuonTracks_PrKF, "MakeMuonTracks_PrKF" )

  Event::v1::Tracks MakeMuonTracksBase::executionCore( NNMuonTracks const& mTracks, const DeMagnet& magnet,
                                                       DetectorElement const& lhcb ) const {
    // optimize usage of counters
    auto hitsPerTrack           = m_hitsPerTrack.buffer();
    auto tilesPerTrack          = m_tilesPerTrack.buffer();
    auto trackRejecteBigCluster = m_trackRejecteBigCluster.buffer();
    auto trackFitFailed         = m_trackFitFailed.buffer();

    m_nbInputTracks += mTracks.size();

    LHCb::Event::v1::Tracks tracks{};

    if ( msgLevel( MSG::DEBUG ) ) { debug() << " # of Muon tracks " << mTracks.size() << endmsg; }
    for ( const auto& t : mTracks ) {
      auto track = std::make_unique<Track>();
      auto hits  = t.getHits(); // std::vector<const MuonCluster*>
      if ( msgLevel( MSG::DEBUG ) ) { debug() << " # of Hits " << hits.size() << endmsg; }

      auto firstHit   = hits.front();
      auto z_firstHit = firstHit->Z();

      double          qOverP( 1. / 10000. ), sigmaQOverP( 0. );
      Gaudi::XYZPoint trackPos( t.bx() + t.sx() * z_firstHit, t.by() + t.sy() * z_firstHit, z_firstHit );
      LHCb::State     state( LHCb::StateVector( trackPos, Gaudi::XYZVector( t.sx(), t.sy(), 1.0 ), 1. / 10000. ) );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " State pos " << trackPos << " Slopes bx=" << t.bx() << " sx=" << t.sx() << " by=" << t.by()
                << " sy=" << t.sy() << endmsg;
      }
      if ( m_Bfield.value() ) {
        m_fCalcMomentum->calculate( magnet, &state, qOverP, sigmaQOverP ).ignore();
        state.setQOverP( qOverP );
      }

      state.setLocation( State::Location::Muon );
      track->addToStates( state );

      LHCb::State first_meas_state{state};
      first_meas_state.setLocation( State::Location::FirstMeasurement );
      track->addToStates( first_meas_state );

      // ========================
      auto lastHit   = hits.back();
      auto z_lastHit = lastHit->Z();

      trackPos = Gaudi::XYZPoint( t.bx() + t.sx() * z_lastHit, t.by() + t.sy() * z_lastHit, z_lastHit );
      LHCb::State laststate( LHCb::StateVector( trackPos, Gaudi::XYZVector( t.sx(), t.sy(), 1.0 ), qOverP ) );

      laststate.setLocation( State::Location::LastMeasurement );
      track->addToStates( laststate );
      // ========

      hitsPerTrack += hits.size();
      int  ntile          = 0;
      bool flagBIGcluster = false; // flag to identify tracks with too big clusters
      for ( auto const* hit : hits ) {
        auto const Tiles = hit->getPadTiles();
        if ( Tiles.size() > m_MaxNTiles ) flagBIGcluster = true;
        for ( auto const& tile : Tiles ) { track->addToLhcbIDs( LHCbID( tile ) ); }
        ntile += Tiles.size();
      }
      tilesPerTrack += ntile;
      if ( msgLevel( MSG::DEBUG ) ) { debug() << " Total # of LHCbID " << ntile << endmsg; }
      if ( m_skipBigClusters.value() && flagBIGcluster ) {
        ++trackRejecteBigCluster;
        if ( msgLevel( MSG::DEBUG ) ) { debug() << " Rejected track due to big clusters " << endmsg; }
      } else {
        if ( m_isTMF && m_fitTracks ) {
          // Try and improve the covariance information
          auto sc = ( *m_TrackMasterFitter )( *track, *lhcb.geometry(), LHCb::Tr::PID::Muon() );
          if ( sc.isFailure() ) { ++trackFitFailed; }
        }
        track->setPatRecStatus( Track::PatRecStatus::PatRecIDs );
        track->setType( Track::Types::Muon );
        tracks.insert( track.release() );
      }
    }
    m_nbTracks += tracks.size();
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " Saved " << tracks.size() << " Muon tracks of " << mTracks.size() << endmsg;
    }
    return tracks;
  }

  Event::v1::Tracks MakeMuonTracks_PrKF::operator()( NNMuonTracks const& mTracks, MuonHits const& muon_hits,
                                                     IPrFitterTool const& fitter, DeMagnet const& magnet,
                                                     DetectorElement const& lhcb ) const {
    auto tracks = executionCore( mTracks, magnet, lhcb );
    if ( m_fitTracks ) {
      auto [out_tracks] = fitter( tracks, muon_hits );
      m_nbTracks += out_tracks.size();
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << " Saved " << out_tracks.size() << " Muon tracks of " << mTracks.size() << endmsg;
      }
      return std::move( out_tracks );
    }
    return tracks;
  }
} // namespace LHCb
