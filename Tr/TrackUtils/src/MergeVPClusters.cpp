/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/VP/VPChannelID.h"
#include "VPDet/DeVP.h"

#include "Event/VPFullCluster.h"
#include "Event/VPLightCluster.h"
#include "Event/VPMicroCluster.h"

#include "Kernel/VPConstants.h"

#include "LHCbAlgs/MergingTransformer.h"

using LHCbIDsPerModule = std::array<std::set<LHCb::Detector::VPChannelID>, VP::NModules>;

/**
 *  Merge a list of "VPMicroClusters" or "VPLightClusters"
 *  to a new set of VPMicroClusters / VPLightClusters, in which only one cluster
 *  per VPChannelID is allowed.
 *
 *  Internally, a std::set is used to do the bookkeeping of testing whether
 *  a VPChannelID has been added already, or not.
 *
 *  Because of the multiple container types for VP clusters,
 *  there are several implementations for this algorithm, brought
 *  via template specialisations.
 *
 *  TODO: Implement a post-merge sorting based on the unique sensor ID?
 *
 *  @author Laurent Dufour
 */
namespace {
  template <class ClusterContainerType>
  void mergeVPClusters( ClusterContainerType& merge_to, LHCbIDsPerModule& encountered_channel_ids,
                        const ClusterContainerType& merge_from ) {
    //  probably there is something more elegant than this
    for ( const auto& vpCluster : merge_from ) {
      const auto moduleId      = vpCluster.channelID().module();
      auto [_, is_new_cluster] = encountered_channel_ids[moduleId].insert( vpCluster.channelID() );

      if ( is_new_cluster ) { merge_to.push_back( vpCluster ); }
    }
  }

  // Specialization for the MicroClusters (indexed container) - adding at the right index.
  template <>
  void mergeVPClusters( LHCb::VPMicroClusters& merge_to, LHCbIDsPerModule& encountered_channel_ids_per_module,
                        const LHCb::VPMicroClusters& merge_from ) {
    //  probably there is something more elegant than this
    for ( unsigned int moduleIdx = 0; moduleIdx < VP::NModules; ++moduleIdx ) {
      for ( const auto& cluster : merge_from.range( moduleIdx ) ) {
        auto [_, is_new_cluster] = encountered_channel_ids_per_module[moduleIdx].insert( cluster.channelID() );

        if ( is_new_cluster ) merge_to.insert( &cluster, &cluster + 1, cluster.channelID().module() );
      }
    }
  }
}; // namespace

namespace LHCb {
  template <class ClusterContainerType>
  struct VPClustersMergerT final : Algorithm::MergingTransformer<ClusterContainerType(
                                       Gaudi::Functional::vector_of_const_<ClusterContainerType> const& )> {

    mutable Gaudi::Accumulators::Counter<> m_n_clusters_input{this, "No. of input clusters (in total)"};
    mutable Gaudi::Accumulators::Counter<> m_n_clusters_output{this, "No. of output clusters"};

    VPClustersMergerT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<ClusterContainerType(
              Gaudi::Functional::vector_of_const_<ClusterContainerType> const& )>(
              name, pSvcLocator, {"InputClusters", {}}, {"OutputLocation", {}} ) {}

    ClusterContainerType
    operator()( Gaudi::Functional::vector_of_const_<ClusterContainerType> const& cluster_lists ) const override {
      ClusterContainerType outputClusters;

      LHCbIDsPerModule ids;

      for ( const auto& list : cluster_lists ) {
        if ( !list.size() ) continue;

        m_n_clusters_input += list.size();
        mergeVPClusters( outputClusters, ids, list );
      }

      m_n_clusters_output += ids.size();

      return outputClusters;
    }
  };

} // namespace LHCb

// This is disabled for VPFullClusters, as internally these use a
// std::pair< unsigned int, cluster > type, where it's not fully clear
// at the time of development what the intended use of the first number is
// and whether it's acutally used...
// DECLARE_COMPONENT_WITH_ID( LHCb::VPClustersMergerT<LHCb::VPFullClusters>, "VPFullClustersMerger" )

DECLARE_COMPONENT_WITH_ID( LHCb::VPClustersMergerT<std::vector<LHCb::VPFullCluster>>, "MergeVPFullClusterVector" )
DECLARE_COMPONENT_WITH_ID( LHCb::VPClustersMergerT<LHCb::VPLightClusters>, "MergeVPLightClusters" )
DECLARE_COMPONENT_WITH_ID( LHCb::VPClustersMergerT<LHCb::VPMicroClusters>, "MergeVPMicroClusters" )
