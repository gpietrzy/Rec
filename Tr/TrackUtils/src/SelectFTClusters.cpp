/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
/** @class selectFTClustersForTracks
 *
 *  Filters FTLiteClusters(Container) to only select
 *  those clusters that were used on the given track,
 *  or particle list.
 *
 *  Example use-case is in the persistency, in which
 *  (for reasons of applying better alignments) one wants to persist
 *  sufficient information to refit.
 *
 *  There is a separate class taking care of particles, as
 *  one needs to access the basic particles.
 *
 *  @see FTLiteCluster.h
 *  @author Laurent Dufour
 */

#include "Detector/FT/FTChannelID.h"

#include "Event/FTLiteCluster.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/Track.h"

#include "GaudiKernel/ISvcLocator.h"

#include "LHCbAlgs/MergingTransformer.h"

#include "LoKi/PhysExtract.h"

using FTLiteClusters = LHCb::FTLiteCluster::FTLiteClusters;

namespace {
  /** Changes the ids, determining the vpIDs on the track **/
  void addFTIds( LHCb::Track const* track, std::set<unsigned int>& ids ) {
    for ( const auto& id : track->lhcbIDs() ) {
      if ( id.isFT() ) ids.insert( id.lhcbID() );
    }
  }

  /** Changes the ids, determining the vpIDs on the proto **/
  void addFTIds( LHCb::ProtoParticle const* proto, std::set<unsigned int>& ids ) {
    if ( !( proto->track() ) ) return;

    addFTIds( proto->track(), ids );
  }

  /** Changes the ids, determining the vpIDs on the particle **/
  void addFTIds( LHCb::Particle const* particle, std::set<unsigned int>& ids ) {
    if ( !particle->isBasicParticle() || !( particle->proto() ) || !( particle->proto()->track() ) ) return;

    addFTIds( particle->proto(), ids );
  }

  /** Changes the ids and clusters, determining the VP clusters on the track **/
  void addFTClusters( LHCb::Track const* track, std::set<unsigned int>& ids, FTLiteClusters& clusters ) {
    for ( const auto& cluster : track->ftClusters() ) {
      auto [_, is_new_cluster] = ids.insert( cluster.channelID() );
      if ( is_new_cluster ) clusters.insert( &cluster, &cluster + 1, cluster.channelID().globalQuarterIdx() );
    }
  }

  /** Changes the ids and clusters, determining the vpIDs on the proto **/
  void addFTClusters( LHCb::ProtoParticle const* proto, std::set<unsigned int>& ids, FTLiteClusters& clusters ) {
    if ( !( proto->track() ) ) return;

    addFTClusters( proto->track(), ids, clusters );
  }

  /** Changes the ids and clusters, determining the vpIDs on the particle **/
  void addFTClusters( LHCb::Particle const* particle, std::set<unsigned int>& ids, FTLiteClusters& clusters ) {
    if ( !particle->isBasicParticle() || !( particle->proto() ) || !( particle->proto()->track() ) ) return;

    addFTClusters( particle->proto(), ids, clusters );
  }

  template <class InputType>
  InputType& getBasics( InputType& a ) {
    return a;
  } // for protos & tracks

  LHCb::Particle::Selection getBasics( LHCb::Particle::Range const& particles ) {
    // make sure the list of particles includes the children
    LHCb::Particle::Selection flat_particles;

    for ( const auto& rawParticle : particles ) {
      LoKi::Extract::particles( rawParticle, std::back_inserter( flat_particles ),
                                [&]( const auto& p ) { return p->isBasicParticle() && p->proto(); } );
    }

    return flat_particles;
  }

  FTLiteClusters getClusterSublist( std::set<unsigned int> const& ids, FTLiteClusters const& inputClusters ) {
    FTLiteClusters outputClusters;
    outputClusters.reserve( ids.size() );

    // now, it's time to search for the ClusterIDs.
    // exploit the indexing of the FTLiteCluster container
    // to do quicker searches
    for ( const auto& lhcbid : ids ) {
      const auto&  channel    = LHCb::LHCbID( lhcbid ).ftID();
      unsigned int quarterIdx = channel.globalQuarterIdx();

      //  could use ranges::make_subrange, but this is marginally
      //  different
      for ( const auto& cluster : inputClusters.range( quarterIdx ) ) {
        if ( cluster.channelID() == channel ) {
          outputClusters.insert( &cluster, &cluster + 1, cluster.channelID().globalQuarterIdx() );
          break;
        }
      }
    }

    return outputClusters;
  }
} // namespace

namespace LHCb {

  template <typename InputListType, typename ClusterContainer>
  struct SelectFTClustersT final
      : Algorithm::MergingTransformer<ClusterContainer( Gaudi::Functional::vector_of_const_<InputListType> const& )> {
    DataObjectReadHandle<ClusterContainer> m_inputClusters{this, "InputClusters", ""};

    mutable Gaudi::Accumulators::AveragingCounter<> m_clusterPerEvent{this, "Average clusters copied per event"};

    SelectFTClustersT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<ClusterContainer( Gaudi::Functional::vector_of_const_<InputListType> const& )>{
              name, pSvcLocator, {"Inputs", {}}, {"OutputLocation", {}}} {}

    ClusterContainer operator()( Gaudi::Functional::vector_of_const_<InputListType> const& lists ) const override {
      ClusterContainer* const pInputClusters = m_inputClusters.get();
      if ( !pInputClusters ) {
        this->error() << "Could not find the input cluster container" << endmsg;
        return ClusterContainer();
      }

      ClusterContainer const& inputClusters = *pInputClusters;

      // first, scout for LHCbIDs
      // these should be unique
      // and sorted
      std::set<unsigned int> ids;

      for ( const auto& list : lists ) {
        if ( list.empty() ) continue;
        const auto& flat_objects = getBasics( list );

        for ( const auto* object : flat_objects ) addFTIds( object, ids );
      }

      ClusterContainer outputClusters = getClusterSublist( ids, inputClusters );

      if ( outputClusters.size() < ids.size() ) this->error() << "Missing clusters from container" << endmsg;
      m_clusterPerEvent += outputClusters.size();

      return outputClusters;
    }
  };

  template <typename InputListType>
  struct SelectFTClustersFromTrackT final
      : Algorithm::MergingTransformer<FTLiteClusters( Gaudi::Functional::vector_of_const_<InputListType> const& )> {
    mutable Gaudi::Accumulators::AveragingCounter<> m_clusterPerEvent{this, "Average clusters copied per event"};

    SelectFTClustersFromTrackT( const std::string& name, ISvcLocator* pSvcLocator )
        : Algorithm::MergingTransformer<FTLiteClusters( Gaudi::Functional::vector_of_const_<InputListType> const& )>{
              name, pSvcLocator, {"Inputs", {}}, {"OutputLocation", {}}} {}

    FTLiteClusters operator()( Gaudi::Functional::vector_of_const_<InputListType> const& lists ) const override {
      std::set<unsigned int> ids;
      FTLiteClusters         outputClusters;

      for ( const auto& list : lists ) {
        if ( list.empty() ) continue;

        const auto& flat_particles = getBasics( list );
        for ( const auto* track : flat_particles ) { addFTClusters( track, ids, outputClusters ); }
      }

      m_clusterPerEvent += outputClusters.size();

      return outputClusters;
    }
  };

  using SelectFTClustersFromContainerForParticles      = SelectFTClustersT<LHCb::Particle::Range, FTLiteClusters>;
  using SelectFTClustersFromContainerForProtoParticles = SelectFTClustersT<LHCb::ProtoParticle::Range, FTLiteClusters>;
  using SelectFTClustersFromContainerForTracks         = SelectFTClustersT<LHCb::Track::Range, FTLiteClusters>;

  using SelectFTClustersFromParticles      = SelectFTClustersFromTrackT<LHCb::Particle::Range>;
  using SelectFTClustersFromProtoParticles = SelectFTClustersFromTrackT<LHCb::ProtoParticle::Range>;
  using SelectFTClustersFromTracks         = SelectFTClustersFromTrackT<LHCb::Track::Range>;

  DECLARE_COMPONENT_WITH_ID( SelectFTClustersFromContainerForParticles, "SelectFTClustersFromContainerForParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectFTClustersFromContainerForTracks, "SelectFTClustersFromContainerForTracks" )
  DECLARE_COMPONENT_WITH_ID( SelectFTClustersFromContainerForProtoParticles,
                             "SelectFTClustersFromContainerForProtoParticles" )

  DECLARE_COMPONENT_WITH_ID( SelectFTClustersFromParticles, "SelectFTClustersFromParticles" )
  DECLARE_COMPONENT_WITH_ID( SelectFTClustersFromTracks, "SelectFTClustersFromTracks" )
  DECLARE_COMPONENT_WITH_ID( SelectFTClustersFromProtoParticles, "SelectFTClustersFromProtoParticles" )
} // namespace LHCb