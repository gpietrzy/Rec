/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <algorithm>
#include <array>
#include <limits>
#include <map>
#include <memory>
#include <string>
#include <utility>
#include <vector>

#include "Event/Track.h"
#include "Event/TrackEnums.h"
#include "GaudiKernel/IFileAccess.h"
#include "GaudiKernel/ToolHandle.h"
#include "LHCbAlgs/Transformer.h"

#include "evaluator.h"
#include "model_calcer_wrapper.h"
#include "wrapped_calcer.h"

#include "Event/PrSeedTracks.h"
#include "Event/RelationTable.h"

using Tracks               = LHCb::Pr::Seeding::Tracks;
using Selection            = LHCb::Event::RelationTable1D<Tracks>;
using Track                = const LHCb::Event::ZipProxy<LHCb::Pr::Seeding::Tracks::SeedProxy<
    SIMDWrapper::Scalar, LHCb::Pr::ProxyBehaviour::Contiguous, const LHCb::Pr::Seeding::Tracks>>;
using OutputType           = std::tuple<Tracks, Selection>;
using base_t               = LHCb::Algorithm::MultiTransformer<OutputType( const Tracks& )>;
const auto FirstMeasurment = LHCb::Event::Enum::State::Location::FirstMeasurement;

class TtracksMVAFilter : public base_t {

public:
  TtracksMVAFilter( const std::string& name, ISvcLocator* pSvcLocator )
      : base_t( name, pSvcLocator, KeyValue{"InputLocation", {}},
                {KeyValue{"OutputTracks", ""}, KeyValue{"Relations", ""}} ) {}

  StatusCode initialize() override;

  float applyBDT( const Track& trk ) const;
  float applyTrackPairBDT( float ygap, float trackgap, float tiltY, float adtx, float adty, float yzIntersectionZ,
                           float yzIntersectionY ) const;

  OutputType operator()( const Tracks& tracksin ) const override { // begin operator

    auto const inputTrackContainer = tracksin.scalar();
    m_inputTracks += tracksin.size();

    auto output1 = std::tuple{Tracks(), LHCb::Event::RelationTable1D<LHCb::Pr::Seeding::Tracks>( &tracksin )};
    auto& [prseedingtracks1, tracksout] = output1;
    prseedingtracks1.reserve( tracksin.size() );

    if ( m_applySingleTrackFilter ) {
      for ( const auto& trk : inputTrackContainer ) { // begin loop over tracks
        // const auto trk = (*track);
        if ( trk.p().cast() < m_momentumThreshold ) continue;

        const float score = applyBDT( trk );

        if ( score < m_singleTrackMVAThreshold ) continue;

        tracksout.add( trk );
        prseedingtracks1.copy_back<SIMDWrapper::InstructionSet::Scalar>( trk );

      } // end loop over tracks
    }
    m_passed += tracksout.size();

    if ( !m_applyTrackPairFilter ) { return output1; }

    auto output2                         = std::tuple{Tracks(), Selection( tracksout.from() )};
    auto& [prseedingtracks2, tracksout2] = output2;
    prseedingtracks2.reserve( tracksout.size() );
    for ( std::size_t outer{0}; outer < tracksout.scalar().size(); ++outer ) { // begin outer loop
      const auto outerTrack      = tracksout.scalar()[outer].from();
      bool       outerTrackAdded = false;
      for ( auto inner = outer + 1; inner < tracksout.scalar().size(); ++inner ) { // begin inner loop
        const auto innerTrack = tracksout.scalar()[inner].from();
        if ( std::signbit( innerTrack.qOverP().cast() ) == ( std::signbit( outerTrack.qOverP().cast() ) ) )
          continue; // skip if they don't have opposite charge

        // prepare features
        const float outerPosY   = outerTrack.StatePos( FirstMeasurment ).Y().cast();
        const float innerPosY   = innerTrack.StatePos( FirstMeasurment ).Y().cast();
        const float outerPosX   = outerTrack.StatePos( FirstMeasurment ).X().cast();
        const float innerPosX   = innerTrack.StatePos( FirstMeasurment ).X().cast();
        const float outerSlopeY = outerTrack.StateDir( FirstMeasurment ).Y().cast();
        const float innerSlopeY = innerTrack.StateDir( FirstMeasurment ).Y().cast();
        const float outerSlopeX = outerTrack.StateDir( FirstMeasurment ).X().cast();
        const float innerSlopeX = innerTrack.StateDir( FirstMeasurment ).X().cast();

        const float ygap     = std::abs( innerPosY - outerPosY );
        const float xgap     = std::abs( innerPosX - outerPosX );
        const float trackgap = std::sqrt( ygap * ygap + xgap * xgap );
        const float tiltY    = std::copysign( 1, outerSlopeY * innerSlopeY );
        if ( tiltY < 0 && m_applyTiltYcut ) continue;
        const float adtx = std::abs( innerSlopeX - outerSlopeX );
        const float adty = std::abs( innerSlopeY - outerSlopeY );

        const float ty0 = outerSlopeY;
        const float ty1 = innerSlopeY;
        const float z0  = outerTrack.StatePos( FirstMeasurment ).Z().cast();
        const float z1  = innerTrack.StatePos( FirstMeasurment ).Z().cast();
        const float y0  = outerPosY;
        const float y1  = innerPosY;
        const float c0  = y0 - ty0 * z0;
        const float c1  = y1 - ty1 * z1;

        const float yzIntersectionZ = ( c1 - c0 ) / ( ty0 - ty1 );
        const float yzIntersectionY = ty0 * yzIntersectionZ + c1;

        if ( yzIntersectionZ < m_zMin ) continue;

        const float score = applyTrackPairBDT( ygap, trackgap, tiltY, adtx, adty, yzIntersectionZ, yzIntersectionY );

        if ( score > m_trackPairMVAThreshold ) { // add the tracks if above threshold -- we check in beginning of loops
                                                 // they are not already added
          tracksout2.addUniquely( innerTrack );
          if ( !outerTrackAdded ) { // we don't want to keep adding the same track, but we want to catch all candidate
                                    // pairs
            tracksout2.addUniquely( outerTrack );
            outerTrackAdded = true;
          }
        }
      } // end inner loop
    }   // end outer loop

    for ( auto trk : tracksout2.scalar() ) { // consolidate relation table
      auto trkIndex = trk.fromIndex().cast();
      prseedingtracks2.copy_back<SIMDWrapper::InstructionSet::Scalar>( tracksin.scalar()[trkIndex] );
    }
    m_passed2 += tracksout2.size();
    return output2;
  } // end operator

private:
  ServiceHandle<IFileAccess> m_fileSvc{this, "FileSvc", "ParamFileSvc", "Service used to retrieve file contents"};

  mutable Gaudi::Accumulators::StatCounter<> m_inputTracks{this, "#input tracks"};
  mutable Gaudi::Accumulators::StatCounter<> m_passed{this, "#passed single track MVA selection"};
  mutable Gaudi::Accumulators::StatCounter<> m_passed2{this, "#single tracks passed track pair MVA selection"};

  Gaudi::Property<bool>  m_applySingleTrackFilter{this, "applySingleTrackFilter", true};
  Gaudi::Property<bool>  m_applyTrackPairFilter{this, "applyTrackPairFilter", true};
  Gaudi::Property<float> m_singleTrackMVAThreshold{this, "singleTrackMVAThreshold", 0.3};
  Gaudi::Property<float> m_trackPairMVAThreshold{this, "trackPairMVAThreshold", 0.75};
  Gaudi::Property<float> m_momentumThreshold{this, "momentumThreshold", 2000.};
  Gaudi::Property<bool>  m_applyTiltYcut{this, "applyTiltYcut", true};
  Gaudi::Property<float> m_zMin{this, "zMin", 1500};

  Gaudi::Property<std::string> m_paramFilesLocationSingleTrackMVA{
      this, "ParamFilesLocationSingleTrackMVA", "paramfile://data/ttracks_catboost_single_track_MVA_filter-202306.cbm"};
  Gaudi::Property<std::string> m_paramFilesLocationTrackPairMVA{
      this, "ParamFilesLocationTrackPairMVA", "paramfile://data/ttracks_catboost_track_pair_MVA_filter-202306.cbm"};
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_StandAloneCatBoostModelSingleTrackMVA;
  std::vector<unsigned char>                             m_CatBoostModelBlobSingleTrackMVA;
  std::unique_ptr<NCatboostStandalone::TOwningEvaluator> m_StandAloneCatBoostModelTrackPairMVA;
  std::vector<unsigned char>                             m_CatBoostModelBlobTrackPairMVA;
};

StatusCode TtracksMVAFilter::initialize() {
  auto sc = base_t::initialize();
  if ( sc.isFailure() ) { return sc; }
  // initialise catboost model
  m_fileSvc.retrieve().orThrow( "Could not obtain IFileAccess Service" );
  auto const buffer = m_fileSvc->read( m_paramFilesLocationSingleTrackMVA );
  if ( !buffer ) {
    throw GaudiException( "Failed to read " + m_paramFilesLocationSingleTrackMVA, this->name(), StatusCode::FAILURE );
  }

  // get blob
  m_CatBoostModelBlobSingleTrackMVA.assign( buffer.value().begin(), buffer.value().end() );

  // load model
  try {
    m_StandAloneCatBoostModelSingleTrackMVA =
        std::make_unique<NCatboostStandalone::TOwningEvaluator>( m_CatBoostModelBlobSingleTrackMVA );
  } catch ( const std::runtime_error& e ) {
    error() << "Failed to load CatBoost model. Likely cases are "
            << "a malformed or missing file. Model file: " << m_paramFilesLocationSingleTrackMVA << std::endl;
    error() << "Message from Catboost:" << std::endl;
    error() << e.what();
    error() << endmsg;
    return StatusCode::FAILURE;
  }

  auto const buffer2 = m_fileSvc->read( m_paramFilesLocationTrackPairMVA );
  if ( !buffer2 ) {
    throw GaudiException( "Failed to read " + m_paramFilesLocationTrackPairMVA, this->name(), StatusCode::FAILURE );
  }

  // get blob
  m_CatBoostModelBlobTrackPairMVA.assign( buffer2.value().begin(), buffer2.value().end() );

  // load model
  try {
    m_StandAloneCatBoostModelTrackPairMVA =
        std::make_unique<NCatboostStandalone::TOwningEvaluator>( m_CatBoostModelBlobTrackPairMVA );
  } catch ( const std::runtime_error& e ) {
    error() << "Failed to load CatBoost model. Likely cases are "
            << "a malformed or missing file. Model file: " << m_paramFilesLocationTrackPairMVA << std::endl;
    error() << "Message from Catboost:" << std::endl;
    error() << e.what();
    error() << endmsg;
    return StatusCode::FAILURE;
  }

  return sc;
}

float TtracksMVAFilter::applyBDT( const Track& trk ) const {
  // prepare features
  const float txy2 =
      trk.StateDir( FirstMeasurment ).X().cast() * trk.StateDir( FirstMeasurment ).X().cast() +
      trk.StateDir( FirstMeasurment ).Y().cast() * trk.StateDir( FirstMeasurment ).Y().cast(); // to be used for pt
  const float pt = sqrt( txy2 / ( 1. + txy2 ) ) / fabs( trk.qOverP().cast() );

  const float p = trk.p().cast();

  const Gaudi::XYZVector slopes   = {trk.StateDir( FirstMeasurment ).X().cast(),
                                   trk.StateDir( FirstMeasurment ).Y().cast(),
                                   trk.StateDir( FirstMeasurment ).Z().cast()};
  const Gaudi::XYZVector momentum = slopes * ( p / slopes.R() );
  const float            pz       = momentum.z();

  const float eta = slopes.eta();

  const float y = trk.StatePos( FirstMeasurment ).Y().cast();

  const float r = trk.StatePos( FirstMeasurment ).Rho().cast();

  // respect the order
  std::vector<float> features = {y, eta, pt, pz, r};
  const float        score =
      m_StandAloneCatBoostModelSingleTrackMVA->Apply( features, NCatboostStandalone::EPredictionType::Probability );

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Single track Ttracks CatBoost features and output probability:" << std::endl;
    debug() << "pt: " << pt << std::endl;
    debug() << "pz: " << pz << std::endl;
    debug() << "eta: " << eta << std::endl;
    debug() << "y: " << y << std::endl;
    debug() << "r: " << r << std::endl;
    debug() << "score: " << score << std::endl;
  }
  return score;
}

float TtracksMVAFilter::applyTrackPairBDT( float ygap, float trackgap, float tiltY, float adtx, float adty,
                                           float yzIntersectionZ, float yzIntersectionY ) const {
  std::vector<float> features = {ygap, trackgap, tiltY, adtx, adty, yzIntersectionZ, yzIntersectionY};
  const float        score =
      m_StandAloneCatBoostModelTrackPairMVA->Apply( features, NCatboostStandalone::EPredictionType::Probability );
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Track pair Ttracks CatBoost features and output probability:" << std::endl;
    debug() << "ygap: " << ygap << std::endl;
    debug() << "trackgap: " << trackgap << std::endl;
    debug() << "tiltY: " << tiltY << std::endl;
    debug() << "adtx: " << adtx << std::endl;
    debug() << "adty: " << adty << std::endl;
    debug() << "yzIntersectionZ: " << yzIntersectionZ << std::endl;
    debug() << "yzIntersectionY: " << yzIntersectionY << std::endl;
    debug() << "score: " << score << std::endl;
  }
  return score;
}

DECLARE_COMPONENT( TtracksMVAFilter )
