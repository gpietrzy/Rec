/*******************************************************************************\
 * (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "LHCbAlgs/Consumer.h"

#include "Event/ODIN.h"
#include "Event/PrHits.h"

#include "Phoenix/Store.h"

#include <string>

namespace {
  using json   = nlohmann::json;
  using FTHits = LHCb::Pr::FT::Hits;
} // namespace

namespace nlohmann {

  /// converter from FTHit to json
  void to_json( json& j, FTHits const& ft_hits ) {
    j = json::array();
    for ( unsigned int i = 0; i < ft_hits.size(); i++ ) {
      if ( ft_hits.id( i ) == LHCb::Detector::FTChannelID{} ) continue;
      auto  FTChannelID   = ft_hits.id( i ).channelID();
      float x_min         = ft_hits.x( i );
      float z             = ft_hits.z( i );
      float dxDy          = ft_hits.dxDy( i );
      auto [y_min, y_max] = ft_hits.yEnd( i );
      float x_max         = x_min + ( y_max - y_min ) * dxDy;
      j += {{"channelID", FTChannelID}, {"type", "Line"}, {"pos", {x_min, y_min, z, x_max, y_max, z}}};
    }
  }

} // namespace nlohmann

/**
 * Get the FT Hits run3 Event data from the Pr Kernel via PrSciFiHits and store them in the Phoenix/Store.h
 * object, which will be used from the Phoenix sink to dump them into a final .json file along the other dumpers in this
 * directory. To be used in the LHCb web event display or the phoenix framework.
 *
 * 1. https://lhcb-web-display.app.cern.ch/#/
 * 2. https://github.com/HSF/phoenix
 *
 * See detailed documentation in:
 * LHCb/Vis/Phoenix/
 *
 * @author Andreas PAPPAS
 * @date 01-02-2022
 */
namespace LHCb::Phoenix {
  class DumpFTHitEvent final : public Algorithm::Consumer<void( FTHits const&, ODIN const& )> {

  public:
    DumpFTHitEvent( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( FTHits const&, ODIN const& ) const override;

  private:
    mutable Store m_storeFT{this, "FTHits_store", "Phoenix:FT"};
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpFTHitEvent::DumpFTHitEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer(
          name, pSvcLocator,
          {KeyValue{"FTHitsLocation", PrFTInfo::SciFiHitsLocation}, KeyValue{"ODIN", LHCb::ODINLocation::Default}} ) {}

void LHCb::Phoenix::DumpFTHitEvent::operator()( FTHits const& ft_hits, ODIN const& odin ) const {
  m_storeFT.storeEventData( {{"gps time", odin.gpsTime()},
                             {"run number", odin.runNumber()},
                             {"event number", odin.eventNumber()},
                             {"bunch crossing type", odin.bunchCrossingType()},
                             {"Content", {{"Hits", {{"FTHits", ft_hits}}}}}} );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpFTHitEvent )
