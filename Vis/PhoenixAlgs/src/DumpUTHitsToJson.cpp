/*******************************************************************************\
 * (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*******************************************************************************/

#include "LHCbAlgs/Consumer.h"

#include "Event/ODIN.h"
#include "Event/UTHit.h"
#include "PrKernel/UTHitHandler.h"

#include "Phoenix/Store.h"

#include <string>

namespace nlohmann {

  /// converter from UTHit to json
  void to_json( nlohmann::json& j, UT::HitHandler const& ut_hits ) {
    j = nlohmann::json::array();
    for ( auto const& utHit : ut_hits.hits() ) {
      float z = utHit.zAtYEq0();
      j += {{"channelID", (int)utHit.chanID()},
            {"type", "Line"},
            {"pos", {utHit.xMin(), utHit.yMin(), z, utHit.xMax(), utHit.yMax(), z}}};
    }
  }

} // namespace nlohmann

/**
 * Get the UT Hits run3 Event data and store them in the Phoenix/Store.h
 * object, which will be used from the Phoenix sink to dump them into a final
 * .json file along the other dumpers in this directory.
 * To be used in the LHCb web event display or the phoenix framework.
 *
 * 1. https://lhcb-web-display.app.cern.ch/#/
 * 2. https://github.com/HSF/phoenix
 *
 * See detailed documentation in:
 * LHCb/Vis/Phoenix/
 */
namespace LHCb::Phoenix {
  class DumpUTHitEvent final : public Algorithm::Consumer<void( UT::HitHandler const&, ODIN const& )> {

  public:
    DumpUTHitEvent( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( UT::HitHandler const&, ODIN const& ) const override;

  private:
    mutable Store m_storeUT{this, "UTHits_store", "Phoenix:UT"};
  };
} // namespace LHCb::Phoenix

LHCb::Phoenix::DumpUTHitEvent::DumpUTHitEvent( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator,
                {KeyValue{"UTHitsLocation", UTInfo::HitLocation}, KeyValue{"ODIN", LHCb::ODINLocation::Default}} ) {}

void LHCb::Phoenix::DumpUTHitEvent::operator()( UT::HitHandler const& ut_hits, ODIN const& odin ) const {
  m_storeUT.storeEventData( {{"gps time", odin.gpsTime()},
                             {"run number", odin.runNumber()},
                             {"event number", odin.eventNumber()},
                             {"bunch crossing type", odin.bunchCrossingType()},
                             {"Content", {{"Hits", {{"UTHits", ut_hits}}}}}} );
}

DECLARE_COMPONENT( LHCb::Phoenix::DumpUTHitEvent )
