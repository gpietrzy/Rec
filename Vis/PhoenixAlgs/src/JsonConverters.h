/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Event/PrVeloTracks.h>
#include <Event/ProtoParticle.h>
#include <Event/RecVertex.h>
#include <Event/Track_v1.h>
#include <nlohmann/json.hpp>

namespace {

  using Track  = LHCb::Event::v1::Track;
  using Tracks = LHCb::Event::v1::Tracks;
  using State  = LHCb::State;
  using json   = nlohmann::json;

} // namespace

namespace nlohmann {

  void to_json( json& j, State const* stateOfTrack );

  void to_json( json& j, Track const* track );

  void to_json( json& j, Tracks const& tracks );

  void to_json( json& j, LHCb::Pr::Velo::Tracks const& tracks );

  void to_json( json& j, LHCb::ProtoParticle const* proto );

  void to_json( json& j, LHCb::ProtoParticles const& protos );

  void to_json( json& j, LHCb::RecVertex const* v );

  void to_json( json& j, LHCb::RecVertices const& vertices );

} // namespace nlohmann
